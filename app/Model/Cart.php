<?php
App::uses('AppModel', 'Model');

class Cart extends AppModel {
    public $validate = array();
    
    public $hasMany = array(
        'CartItem' => array(
            'className' => 'CartItem',
            'foreignKey' => 'cart_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
}