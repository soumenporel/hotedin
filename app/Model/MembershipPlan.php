<?php
App::uses('AppModel', 'Model');
/**
 * Job Model
 *
 * @property User $User
 * @property Bid $Bid
 * @property PostJob $PostJob
 */
class MembershipPlan extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array();

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
	public $hasAndBelongsToMany = array(
        
        'MembershipItem' =>
            array(
                'className' => 'MembershipItem',
                'joinTable' => 'plan_items',
                'foreignKey' => 'plan_id',
                'associationForeignKey' => 'item_id',
                'unique' => true,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'plan_items'
            )
    
    );		

}

