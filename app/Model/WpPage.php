<?php
App::uses('AppModel', 'Model');

class WpPage extends AppModel {

    public $validate = array();

    // public function beforeSave($options = array()) {
    //     if (isset($this->data['WpPage']['page_url'])) 
    //     {
    //         $this->data[$this->alias]['slug'] = $this->createSlug($this->data['WpPage']['page_url']);
    //     }
    //     return true;
    // }

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['page_url'])) {
            if (empty($this->data[$this->alias]['id'])) {
                $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['page_url']);
            } else {
                $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['page_url'], $this->data[$this->alias]['id']);
            }
        }
        return true;
    }

    public $belongsTo = array(
        'Category' => array (
            'className' => 'Category',
            'foreignKey' => 'category_id'
        )
    );    
    
}
