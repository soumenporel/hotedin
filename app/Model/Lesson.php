<?php

App::uses('AppModel', 'Model');

/**
 * Lesson Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Lesson extends AppModel {

    public $hasMany = array(
        'Lecture' => array(
            'className' => 'Lecture',
            'foreignKey' => 'lesson_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => 'Lecture.sortby Asc',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );        

}
