<?php

App::uses('AppModel', 'Model');

/**
 * Job Model
 *
 * @property User $User
 * @property Bid $Bid
 * @property PostJob $PostJob
 */
class Subscription extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array();

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $hasMany = array(
      
    );
    public $belongsTo = array(
        'MembershipPlan' => array(
            'className' => 'MembershipPlan',
            'foreignKey' => 'memebership_plan_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
