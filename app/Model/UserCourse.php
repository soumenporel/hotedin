<?php

App::uses('AppModel', 'Model');

/**
 * Seo Model
 *
 */
class UserCourse extends AppModel {

    /**
     * Display field
     *
     * @var array
     */
    public $validate = array();
    
    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id'
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

}
