<?php

App::uses('AppModel', 'Model');
#App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property EmailNotification $EmailNotification
 * @property FavoriteList $FavoriteList
 * @property FavoriteShop $FavoriteShop
 * @property FavoriteTreasury $FavoriteTreasury
 * @property InboxMessage $InboxMessage
 * @property Preference $Preference
 * @property Privacy $Privacy
 * @property Security $Security
 * @property SentMessage $SentMessage
 * @property ShippingAddress $ShippingAddress
 * @property Shop $Shop
 */

class User extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'first_name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A username is required'
            )
        ),
        'user_pass' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'on' => 'create',
                'message' => 'A password is required'
            )
        ),
        'last_name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A firstname is required'
            )
        ),
        'email_address' => array(
          'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A Email address is required'
          ),
          'unique' => array(
              'rule' => 'isUnique',
              'required' => 'create',
              'message' => 'Not unique'
          )

        ),
    );

    /* public function beforeSave($options = array()) {
      if (isset($this->data[$this->alias]['password'])) {
      $this->data[$this->alias]['password'] = md5(
      $this->data[$this->alias]['password']
      );
      }
      return true;
      } */

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    //public $hasOne = 'UserImage';
    public $hasMany = array(

        'UserImage' => array(
            'className' => 'UserImage',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => array('UserImage.id'=>'ASC'),
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),

        'CompanyDetail' => array(
            'classname' => 'CompanyDetail',
            'foreignKey' => 'user_id',
            'dependent' => true
        ),

        'Post' => array(
        'className' => 'Post',
        'foreignKey' => 'user_id',
        'dependent' => false,
        'counterCache'=> true,
        'conditions' => '',
        'fields' => '',
        'order' => '',
        'limit' => '',
        'offset' => '',
        'exclusive' => '',
        'finderQuery' => '',
        'counterQuery' => ''
      ),

        'Subscription' => array(
           'className'  => 'Subscription',
           'foreignKey' => 'user_id'
        )

    );

    public $belongsTo = array(
        'EmployerMembershipPlan' => array(
           'className'  => 'EmployerMembershipPlan',
           'foreignKey' => 'membership_plan_id'
       ),
    );

     public $hasOne = array(
         'CompanyDetail' => array(
             'classname' => 'CompanyDetail',
             'dependent' => true
         )
    );

}
