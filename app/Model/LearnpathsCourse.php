<?php
App::uses('AppModel', 'Model');
/**
 * PostTag Model
 *
 */
class LearnpathsCourse extends AppModel {
/**
 * hasMany associations
 *
 * @var array
 */
	
	public $hasAndBelongsToMany = array(
        'Learnpath' =>
            array(
                'className' => 'Learnpath',
                'joinTable' => 'learnpaths_courses',
                'foreignKey' => 'post_id',
                'associationForeignKey' => 'learnpath_id',
                'unique' => true,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'learnpaths_courses'
            )
    );
	
}
