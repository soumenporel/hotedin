<?php
App::uses('AppModel', 'Model');
/**
 * Analytic Model
 *
 */
class Analytic extends AppModel 
{

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}
