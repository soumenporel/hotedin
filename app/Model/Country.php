<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 * @property Preference $Preference
 */
class Country extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed


            public $hasMany = array(
       
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'country',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
