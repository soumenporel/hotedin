<?php

App::uses('AppModel', 'Model');

class Language extends AppModel {

    public $hasMany = array(
        'LanguagePreference' => array(
            'className' => 'LanguagePreference',
            'foreignKey' => 'language_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
