<?php
App::uses('AppModel', 'Model');

class AwardImage extends AppModel {
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Award' => array(
            'className' => 'Award',
            'foreignKey' => 'award_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
        
    );
}