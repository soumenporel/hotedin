<?php
App::uses('AppModel', 'Model');
/**
 * SiteSetting Model
 *
 * @property User $User
 */
class UserAssignment extends AppModel {

public $validate = array();

public $belongsTo = array(
    'Assignment'=>array(
       'className'=>'Assignment',
       'foreignKey'=>'assignment_id'
    ),
    'User'=>array(
       'className'=>'User',
       'foreignKey'=>'user_id'
    ),
  );


}
