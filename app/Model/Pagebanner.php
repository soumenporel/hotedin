<?php

App::uses('AppModel', 'Model');

/**
 * Faq Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Pagebanner extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array();
       public $belongsTo = array(
        'WpPage' => array(
            'className' => 'WpPage',
            'foreignKey' => 'page_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed
}
