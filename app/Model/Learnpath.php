<?php

App::uses('AppModel', 'Model');

/**
 * Job Model
 *
 * @property User $User
 * @property Bid $Bid
 * @property PostJob $PostJob
 */
class Learnpath extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array();

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    
    public $hasAndBelongsToMany = array(
        'Post' =>
            array(
                'className' => 'Post',
                'joinTable' => 'learnpaths_courses',
                'foreignKey' => 'learnpath_id',
                'associationForeignKey' => 'post_id',
                'unique' => true,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'learnpaths_courses'
            )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['title'])) {
            if (empty($this->data[$this->alias]['id'])) {
                $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['title']);
            } else {
                $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['title'], $this->data[$this->alias]['id']);
            }
        }
        return true;
    }

}
