<?php

App::uses('AppModel', 'Model');

/**
 * Testimonial Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Testimonial extends AppModel {

    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'User_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );


}
