<?php
App::uses('AppModel', 'Model');
/**
 * State Model
 *
 * @property Preference $Preference
 */
class Order extends AppModel {

    public $belongsTo = array(
        'Post' => array(
           'className'  => 'Post',
           'foreignKey' => 'post_id'
        ),
         'User' => array(
           'className'  => 'User',
           'foreignKey' => 'user_id'
        ),
        
         'Bank' => array(
           'className'  => 'Bank',
           'foreignKey' => 'bank_id'
        )
     );
    
    public $hasMany = array(
        'OrderItem' => array(
            'className' => 'OrderItem',
            'foreignKey' => 'order_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
}
