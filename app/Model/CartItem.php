<?php
App::uses('AppModel', 'Model');

class CartItem extends AppModel {
    public $validate = array();
    
    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}