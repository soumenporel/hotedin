<?php
App::uses('AppModel', 'Model');

class Job extends AppModel {
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Jobtype' => array(
            'className' => 'Jobtype',
            'foreignKey' => 'job_type'
        ),
        'JobCategory' => array(
            'className' => 'JobCategory',
            'foreignKey' => 'category'
        )
    );
}
