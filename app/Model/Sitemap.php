<?php
App::uses('AppModel', 'Model');
/**
 * Sitemap Model
 *
 */
class Sitemap extends AppModel 
{

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}

