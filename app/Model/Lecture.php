<?php

App::uses('AppModel', 'Model');

/**
 * Lecture Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Lecture extends AppModel {

	public $hasMany = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'quiz_id',
            //'order' => 'Question.id DESC'
        ),
        'DownloadableFile' => array(
            'className' => 'LecturesLibrary',
            'foreignKey' => 'lecture_id',
            'conditions' => array('DownloadableFile.library_type' => 1)
        ),
        'ExternalLink' => array(
            'className' => 'LecturesLibrary',
            'foreignKey' => 'lecture_id',
            'conditions' => array('ExternalLink.library_type' => 2)
        ),
        'SourceCode' => array(
            'className' => 'LecturesLibrary',
            'foreignKey' => 'lecture_id',
            'conditions' => array('SourceCode.library_type' => 3)
        )
	);
        
            public $belongsTo = array(
       
        'Post' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
