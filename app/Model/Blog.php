<?php
App::uses('AppModel', 'Model');
#App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property EmailNotification $EmailNotification
 * @property FavoriteList $FavoriteList
 * @property FavoriteShop $FavoriteShop
 * @property FavoriteTreasury $FavoriteTreasury
 * @property InboxMessage $InboxMessage
 * @property Preference $Preference
 * @property Privacy $Privacy
 * @property Security $Security
 * @property SentMessage $SentMessage
 * @property ShippingAddress $ShippingAddress
 * @property Shop $Shop
 */
class Blog extends AppModel {



/**
 * Validation rules
 *
 * @var array
 */
	/*public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = md5(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}*/

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		// 'TaskImage' => array(
		// 	'className' => 'TaskImage',
		// 	'foreignKey' => 'task_id',
		// 	'dependent' => false,
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'exclusive' => '',
		// 	'finderQuery' => '',
		// 	'counterQuery' => ''
		// ),
            'BlogComment' => array(
			'className' => 'BlogComment',
			'foreignKey' => 'blog_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => array('BlogComment.id'=>'DESC'),
			'limit' => '3',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		
	);
 //        public $belongsTo = array(
	//     'Category' => array(
 //                'className' => 'Category',
 //                'foreignKey' => 'cat_id',
 //                'conditions' => '',
 //                'fields' => '',
 //                'order' => ''
 //            )
	// );
	
	public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['title'])) {
            if (empty($this->data[$this->alias]['id'])) {
                $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['title']);
            } else {
                $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['title'], $this->data[$this->alias]['id']);
            }
        }
        return true;
    }
    
      public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
