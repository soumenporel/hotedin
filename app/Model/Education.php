<?php
App::uses('AppModel', 'Model');

class Education extends AppModel {

	    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A name is required'
            )
        ),
        'email_address' => array(
          'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A Email address is required'
          )
          
        ),
        'message' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A message is required'
            )
        ),
    );
   
}