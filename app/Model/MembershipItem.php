<?php
App::uses('AppModel', 'Model');
/**
 * Job Model
 *
 * @property User $User
 * @property Bid $Bid
 * @property PostJob $PostJob
 */
class MembershipItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array();

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
	public $hasAndBelongsToMany = array(
        'MembershipPlan' =>
            array(
                'className' => 'MembershipPlan',
                'joinTable' => 'plan_items',
                'foreignKey' => 'item_id',
                'associationForeignKey' => 'plan_id',
                'unique' => true,
                'dependent' => true,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => 'plan_items'
            )
    );


}
