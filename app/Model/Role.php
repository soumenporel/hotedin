<?php
App::uses('AppModel', 'Model');
/**
 * Role Model
 *
 */
class Role extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $hasOne = array(
        'RolesAccess' => array(
            'className' => 'RolesAccess'
        )
    );
}
