<?php

App::uses('AppModel', 'Model');

/**
 * PromoCode Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class PromoCode extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array();

    //The Associations below have been created with all possible keys, those that are not needed can be removed
}
