<?php
App::uses('AppModel', 'Model');
/**
 * SiteSetting Model
 *
 * @property User $User
 */
class CmsPage extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
	);

	public function beforeSave($options = array()) {
        if (isset($this->data['CmsPage']['page_url'])) 
        {
            $this->data[$this->alias]['slug'] = $this->createSlug($this->data['CmsPage']['page_url']);
        }
        return true;
    }

}
