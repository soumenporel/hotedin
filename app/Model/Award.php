<?php
App::uses('AppModel', 'Model');

class Award extends AppModel {
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
      public $hasMany = array(
        'AwardImg' => array(
            'className' => 'Awardfile',
            'foreignKey' => 'award_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
      );
}