<?php
App::uses('AppModel', 'Model');
/**
 * EmailTemplate Model
 *
 */
class ReadLecture extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);
        
        public $belongsTo = array(
        		'User' => array(
                            'className' => 'User',
                            'foreignKey' => 'user_id',
                            'conditions' => '',
                            'fields' => '',
            ),
        		'Post' => array(
                            'className' => 'Post',
                            'foreignKey' => 'post_id'
                           
            )
      	);
	
	
}
