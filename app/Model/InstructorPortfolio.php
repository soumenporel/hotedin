<?php
App::uses('AppModel', 'Model');

class InstructorPortfolio extends AppModel {
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'counterCache' => true,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
      public $hasMany = array(
        'Portfoliofile' => array(
            'className' => 'Portfoliofile',
            'foreignKey' => 'portfolio_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
      );
}