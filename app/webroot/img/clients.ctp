<section class="home-slider inner-banner">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
         <div class="carousel-item no-shdow " style="background: url('/team4/studilmu/img/aboutus.png') no-repeat center center; background-size:cover;" >
         </div>
         <div class="carousel-item no-shdow" style="background: url('/team4/studilmu/img/aboutus.png') no-repeat center center; background-size:cover;" >
         </div>
         <div class="carousel-item active  no-shdow"  style="background: url('/team4/studilmu/img/aboutus.png') no-repeat center center; background-size:cover;" >
         </div>
      </div>
   </div>
</section>
<section class="pt-5 pb-5">
  <div class="container">
    <div class="row">
    <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative col-md-12"> Our clients</h1>
    <div class="col-md-6">
      <p>
        We like to work with ambitious start-ups as well as small and medium sized businesses. It doesn’t really matter to us what your product or service is, it’s about you, your mindset and your passion for what you do.
      </p>
      <p>
        Are you motivated, driven and determined to see your business succeed? Do you have a clear vision of where you want to be? Then let's talk. We love people like you because genuinely want to help our customers to succeed, so it’s important for us to work with people on the same wavelength.
      </p>
      <p>
        We love being able to take a company whose ad hoc marketing activities have delivered little or no response and giving them a powerful, effective strategy that really works. How do we do this? We become an extension of your company, reflecting your energy, excitement and passion back at your customers and prospects.
      </p>
    </div>
    <div class="col-md-6">
      <p>
        Everything we do is consistent and measured, carefully thought through, implemented creatively and intelligently. We love to give our clients reports, but if it’s not your thing, we can keep the stats to ourselves and just ensure everything’s on track.
      </p>
      <p>
        We are a marketing agency based in Arundel, West Sussex and we provide marketing strategy and support services to businesses across the South-East. We prefer to keep it local to Sussex, Hampshire, Surrey, Kent and London since it makes it easier to add a personal touch, with face-to-face monthly meetings and regular contact.
      </p>
      <p>
        Get in touch with us to find out more, call us today on 01903 650788.
      </p>
    </div>
  </div>
  </div>
</section>

<section>
  <section aria-labelledby="enterprise" role="region" class="container text-center section-with-space" id="enterprise">
     <h3 class="section-header">Need to train 5 or more people?</h3>
     <p class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-xs-12">We offer flexible, cost-effective group memberships for your business, school, or government organization. <a href="https://learning.linkedin.com/elearning-solutions-contact-us-lynda?utm_source=ldc-consumer&amp;utm_campaign=ldc-consumer-hp&amp;src=ldc-homepage&amp;veh=nmhp_body_contact_us">Contact Us</a></p>
     <ul class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-12 col-md-offset-0" id="customers">
        <li class="client-icon">
           <picture><img alt="Adobe" data-lazy-srcset="https://cdn.lynda.com/static/nonmember/images/client-logos/adobe.png, https://cdn.lynda.com/static/nonmember/images/client-logos/adobe-2x.png 2x" data-appear-top-offset="500" src="https://cdn.lynda.com/static/nonmember/images/client-logos/adobe.png"></picture>
        </li>
        <li class="client-icon">
           <picture><img alt="Full Sail University" data-lazy-srcset="https://cdn.lynda.com/static/nonmember/images/client-logos/fullsail.png, https://cdn.lynda.com/static/nonmember/images/client-logos/fullsail-2x.png 2x" data-appear-top-offset="500" src="https://cdn.lynda.com/static/nonmember/images/client-logos/fullsail.png"></picture>
        </li>
        <li class="client-icon">
           <picture><img alt="patagonia" data-lazy-srcset="https://cdn.lynda.com/static/nonmember/images/client-logos/patagonia.png, https://cdn.lynda.com/static/nonmember/images/client-logos/patagonia-2x.png 2x" data-appear-top-offset="500" src="https://cdn.lynda.com/static/nonmember/images/client-logos/patagonia.png"></picture>
        </li>
        <li class="client-icon">
           <picture><img alt="NBC" data-lazy-srcset="https://cdn.lynda.com/static/nonmember/images/client-logos/nbc.png, https://cdn.lynda.com/static/nonmember/images/client-logos/nbc-2x.png 2x" data-appear-top-offset="500" src="https://cdn.lynda.com/static/nonmember/images/client-logos/nbc.png"></picture>
        </li>
        <li class="client-icon">
           <picture><img alt="University of Southern California" data-lazy-srcset="https://cdn.lynda.com/static/nonmember/images/client-logos/usc.png, https://cdn.lynda.com/static/nonmember/images/client-logos/usc-2x.png 2x" data-appear-top-offset="500" src="https://cdn.lynda.com/static/nonmember/images/client-logos/usc.png"></picture>
        </li>
        <li class="client-icon">
           <picture><img alt="U.S. Office of Government Ethics" data-lazy-srcset="https://cdn.lynda.com/static/nonmember/images/client-logos/usge.png, https://cdn.lynda.com/static/nonmember/images/client-logos/usge-2x.png 2x" data-appear-top-offset="500" src="https://cdn.lynda.com/static/nonmember/images/client-logos/usge.png"></picture>
        </li>
     </ul>
  </section>
</section>


<section style="background-image: url('<?php echo $this->webroot; ?>img/fields.jpg');" class="hero">
   <h1 class="hero__title"><span class="hero__title--script">lets make</span> Great things happen</h1>
</section>
