  <!--  inner  slider   -->

      <section class="home-slider inner-banner without-shadow">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner" role="listbox">
                <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?> no-shdow " style="background: url('<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>') no-repeat center center; background-size:cover;" >

                    <div class="carousel-caption">
                    <div class="row align-items-center justify-content-between">
                      <?php echo $banner['Pagebanner']['desc']; ?>

                    </div>
                  </div>
                  </div>
                <?php $bi++; } ?>
              </div>
            </div>
      </section>

      <section class="mainWarp">
        <div class="container">
          <div class="row">
            <div class="col-lg-9">
              <div class="row">
                  <?php if(!empty($blog_list)){
		          foreach ($blog_list as $key => $bl) { echo '<pre>'; print_r($bl);?>
                <div class="col-lg-4">
                  <div class="blog-card">
                    <div class="blog-card-img" style="background-image: url('<?php  if(isset($bl['Blog']['image']) && $bl['Blog']['image'] !='') { echo $this->webroot.'blogs_image/'.$bl['Blog']['image']; } else { echo $this->webroot.'noimage.png'; } ?>')">
                      <!-- <img src="<?php if(isset($bl['Blog']['image']) && $bl['Blog']['image'] !='') { echo $this->webroot.'blogs_image/'.$bl['Blog']['image']; } else { echo $this->webroot.'noimage.png'; } ?>" alt=""> -->
                    </div>
                    <div class="date"><?php echo date('F d, Y',strtotime($bl['Blog']['creation_date'])); ?></div>
                    <div class="title"><?php echo $bl['Blog']['title']; ?> </div>
                    <div class="dec">
                      <?php  $converted_text=strip_tags($bl['Blog']['short_description']);
                              echo (substr($converted_text,0,180).'....'); ?>
                    </div>
                    <a href="<?php echo $this->webroot.'blogs/details/'.$bl['Blog']['slug']; ?>" class="btn btn-danger">Read More</a>
                  </div>
                </div>
                  <?php } } ?>
              </div>
            </div>
            <div class="col-lg-3">
                <?php //print_r($featured_blog); ?>
                 <?php //print_r($category_blog); ?>
                <?php foreach($category_blog as $cat){ ?>
              <div class="side-list-warp">
                <h4><?php echo $cat['BlogCategory']['category_name']; ?></h4>
                <ul class="side-list">
                    <?php foreach($cat['Blog'] as $blog){ ?>
                  <li>
                    <a href="<?php echo $this->webroot.'blogs/details/'.$blog['slug']; ?>">
                      <img src="<?php if(isset($blog['image']) && $blog['image'] !='') { echo $this->webroot.'blogs_image/'.$blog['image']; } else { echo $this->webroot.'noimage.png'; } ?>" alt="">
                      <div class="text"><?php echo $blog['title']; ?></div>
                    </a>
                  </li>
                    <?php } ?>
                </ul>
              </div>
                <?php } ?>

<!--                feature.........................................................-->
        <?php if(count($featured_blog)>0){ ?>
            <div class="side-list-warp">
                <h4>Featured Blogs</h4>
                <ul class="side-list">
                  <?php foreach($featured_blog as $bl){ ?>
                  <li>
                    <a href="<?php echo $this->webroot.'blogs/details/'.$bl['Blog']['slug']; ?>">
                      <img src="<?php if(isset($bl['Blog']['image']) && $bl['Blog']['image'] !='') { echo $this->webroot.'blogs_image/'.$bl['Blog']['image']; } else { echo $this->webroot.'noimage.png'; } ?>" alt="">
                      <div class="text"><?php echo $bl['Blog']['title']; ?></div>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
              </div>
        <?php } ?>
            </div>
          </div>
        </div>
      </section>
