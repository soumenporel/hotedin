<style>
    .header{position:relative;}
    .inner_header{background:#0c2440;}
    .cfi-chapter-container .header {width: auto;}
</style>

<!--<script src="<?php //echo $this->webroot . 'js/angular.min.js'; ?>"></script>-->
<script src="<?php echo $this->webroot . 'js/ng-1.5.5/angular.min.js'; ?>"></script>

<link href="<?php echo $this->webroot; ?>css/curriculum_test.css" rel="stylesheet" type="text/css">

<section class="profileedit" ng-app="myApp" ng-controller="myCtrl">
    <div class="container">
        <div class="row" style="background:#f6f6f6; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
            <div class="col-md-3 col-sm-3" style="padding:0;">
                <?php
                /*                 * *Course Sidebar* */
                echo $this->element('course_sidebar', $post_dtls, $course_status);
                ?>
            </div>
            <div class="col-md-9 col-sm-9" style="padding:0; border-left:#ddd solid 1px;">
                <div class="profile_second_part curriculum_part">
                    <div class="clearfix">
                        <div class="">
                            <h2 class="" style="display: inline-block; margin: 7px 0; width: auto;">Curriculum</h2>
<!--                            <div class="pull-right">
                                <a class="btn btn-danger btn-lg" target="_blank" href="<?php //echo $this->webroot.'learns/course_content/'.$post_dtls['Post']['slug'];?>">Preview</a>
                            </div>-->
                        </div>
                    </div>

                    <!-- Lecture template Start -->
                    <script type="text/ng-template" id="lectureTemplate">

                        <div class="cfi-content" ng-class="isEditLectureTitleShown(subRow) ? 'closed' : ''">
                        <span class="content ui-state-default">
                        <span class="cfi-item-type">{{ subRow.type }}</span>
                        <span class="cfi-item-number">1:</span>
                        <span class="cfi-item-title" data-purpose="lecture-title-shown">{{ subRow.title }}</span>
                        <i class="edit-handle fa fa-pencil" ng-click="toggleEditLectureTitle(subRow)"></i>
                        <span class="js-delete-handle delete-handle fa fa-trash" ng-click="deleteLectureFunction(subRow, $event)"></span>
                        <span class="action-buttons pull-right">

                        <a href="javascript:void(0)" data-purpose="add-content" class="add-content btn btn-primary btn-sm container-switch floating m0-10 m5-30-xs m5-30-md"
                        ng-if="!subRow.media" ng-click="toggleLectureAddContent(subRow)">
                        + Add Content
                        </a>
                        <span class="cfi-item-previewable" style="display: none;">(Preview enabled)</span>
                        <i class="fa  container-switch" ng-class="isIsHiddenShown(subRow) ? 'fa-chevron-up' : 'fa-chevron-down'" ng-click="toggleIsHidden(subRow)"></i>
                        <i class="fa fa-bars sort-handle"></i>
                        </span>
                        </span>
                        </div>
                        <div class="gray-container " ng-if="isIsHiddenShown(subRow) && !isAddResourceTabShown(subRow) && !isLectureAddContentShown(subRow)">

                        <div class="single-item" ng-if="subRow.media">
                        <div class="thumb" style="">
                        </div>
                        <div class="details tablet-m0 tablet-fn">
                        <span class="title">{{subRow.media}}</span><br>
                        <a class="edit-content-with-thumbnail" href="javascript:void(0)"
                        ng-click="toggleLectureAddContent(subRow)">
                        <i class="fa fa-pencil"></i>
                        Edit Content
                        </a>
                        <br>
                        </div>
                        </div>

                        <ul class="more">
                        <li class="desc">
                        <div class="lec-description"
                        ng-class="isShowEditContainerShown(subRow) ? 'hidden' : ''">
                        <p ng-click="toggleShowEditContainer(subRow)">{{ subRow.description }}</p>
                        </div>
                        <div class="edit-container"
                        ng-class="!isShowEditContainerShown(subRow) ? 'hidden' : ''">
                        <form class="clearfix">
                        <div class="form-group">
                        <textarea class="form-control" name="subRow.description" ng-model="subRow.description" placeholder="Add a description. Include what students will be able to do after completing the lecture."></textarea>
                        <input type="hidden" ng-model="subRow.lectureID" value="{{ subRow.lectureID }}" />
                        </div>
                        <div class="form-group pull-right">
                        <button type="button" class="btn btn-info" ng-click="toggleShowEditContainer(subRow)">Cancel</button>
                        <button type="button" class="btn btn-info" ng-click="saveLectureDescription(subRow.lectureID, subRow.description, subRow)">Save</button>
                        </div>
                        </form>
                        </div>
                        </li>

                        <li class="supplementary ">

                        <div class="downloadable-materials" ng-if="subRow.haveDownloadableFile">
                        <h4>Downloadable materials</h4>
                        <ul class="sup downloadable">
                        <li class="iconed File" data-type="asset" data-id=""
                        ng-repeat="downloadable in subRow.downloadablefile">
                        <span class="fa fa-download"></span>
                        <span class="title">{{ downloadable.downloadable_file }}</span><!-- <span> (8.4 kB)</span> -->
                        <a href="javascript:void(0)" class="fa fa-trash-o delete-sup"
                        ng-click="deleteFromResource(downloadable, subRow, $index, $event)"></a>
                        </li>
                        </ul>
                        </div>

                        <div class="external-resources" ng-if="subRow.haveExternalLink">
                        <h4>External Resources</h4>
                        <ul class="sup external">
                        <li class="iconed ExternalLink" data-type="asset" data-id=""
                        ng-repeat="external in subRow.externallink">
                        <span class="udi udi-external-link"></span>
                        <span class="title">{{ external.external_title }}</span>
                        <a href="javascript:void(0)" class="fa fa-trash-o delete-sup"
                        ng-click="deleteFromResource(external, subRow, $index, $event)"></a>
                        </li>
                        </ul>
                        </div>

                        <div class="sourcecode-resources" ng-if="subRow.haveSourceCodeFile">
                        <h4>Sourcecode</h4>

                        <ul class="sup external">
                        <li class="iconed ExternalLink" data-type="asset" data-id=""
                        ng-repeat="sourcecode in subRow.sourcecodefile">
                        <span class="udi udi-external-link"></span>
                        <span class="title">{{ sourcecode.sourcecode_file }}</span>
                        <a href="javascript:void(0)" class="fa fa-trash-o delete-sup"
                        ng-click="deleteFromResource(sourcecode, subRow, $index, $event)"></a>
                        </li>
                        </ul>
                        </div>
                        </li>
                        </ul>
                        <p class="bottom-btns" ng-class="isShowEditContainerShown(subRow) ? 'hidden' : ''">
                        <a class="btn btn-default desc-btn add" href="javascript:void(0)"
                        ng-if="!subRow.description"
                        ng-click="toggleShowEditContainer(subRow)">
                        Add Description
                        </a>

                        <a class="btn btn-default add-supplementary" href="javascript:void(0)"
                        ng-click="openAddResourceTab(subRow)">
                        Add Resources
                        </a>
                        </p>
                        <div id="replace-with-video-confirm-6342600" class="modal fade replace-with-video-confirm" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="udi udi-close"></span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Update this lecture to video?</h4>
                        </div>
                        <div class="modal-body">
                        The content of this lecture will be added as a downloadable resource, which you may choose to remove. You will be prompted to add a video file and re-publish as a video lecture.
                        </div>
                        <div class="modal-footer">
                        <div class="modal-footer--inner">
                        <button type="button" class="btn btn-danger replace-with-video" data-dismiss="modal">
                        Convert To Video
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>

                        <div class="gray-container has-header tablet-m0"
                        ng-if="isAddResourceTabShown(subRow)">
                        <div class="header">
                        Add Resources
                        <a class="close-btn container-switch fa fa-close" href="javascript:void(0)"
                        ng-click="toggleAddResourceTab(subRow)"></a>
                        </div>

                        <div class="asset-create-container" data-purpose="loader">
                        <div class="main create-asset-container add-supplementary ud-assetcreator" data-type="File">

                        <div my-tabs id="tabs">
                        <ul class="clearfix">
                        <li><a href="#downloadable-tabs">Downloadable File</a></li>
                        <li><a href="#library-tabs">Add From Library</a></li>
                        <li><a href="#external-tabs">External Resource</a></li>
                        <li><a href="#sourcecode-tabs">Source Code</a></li>
                        </ul>
                        <div id="downloadable-tabs">
                        <form>
                        <div class="form-group">
                        <input style="height:auto" type="file" class="form-control"
                        data-postid="<?php echo $post_dtls['Post']['id']; ?>"
                        data-lectureid="{{ subRow.lectureID }}"
                        file-model="uploadDownloadableFile"
                        ng-model="downloadableFile"
                        accept=".docx, .doc, .pdf,.jpg, .jpeg, .png, .gif, .mp4, .mkv, .flv, .pdf, .doc, .docx, .ppt, .mp3, .wav, .wma"  />
                        <span>{{ uploadProgress }}</span>
                        </div>
                        </form>
                        <div class="tips">
                        <p>
                        <b>Tip:</b>
                        Supported file DOCX, DOC, PDF
                        </p>
                        </div>
                        </div>
                        <div id="library-tabs">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                        <th class="col-md-4">
                        <span class="ng-scope">Filename</span>
                        </th>
                        <th class="col-md-2">
                        <span translate="">
                        <span class="ng-scope">Type</span>
                        </span>
                        </th>
                        <th class="col-md-3 orderable asc">
                        <span>Date Uploaded</span>
                        </th>
                        <th class="col-md-2">
                        <span translate="">
                        <span>Status</span>
                        </span>
                        </th>
                        <th class="col-md-1"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="asset in subRow.assets" ng-click="addFromLibrary(asset, subRow)">
                        <td>
                        <div>{{ asset.library_type === '1' ? asset.downloadable_file : asset.external_title }}</div>
                        </td>
                        <td>{{ asset.library_type === '1' ? 'File' : 'ExternalLink' }}</td>
                        <td>{{ asset.date | dateToISO | date:'MM/dd/yyyy' }}</td>
                        <td>
                        <div ng-switch="" on="asset.status">
                        <div ng-switch-when="1" class="ng-scope">
                        <span translate="">
                        <span class="ng-scope">Success</span>
                        </span>
                        </div>
                        </div>
                        </td>
                        <td class="center-align no-action" ng-click="$event.stopPropagation()">
                        <i class="fa fa-trash" ng-click="deleteFromLibrary(asset, $event)"></i>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </div>
                        <div id="external-tabs">
                        <form class="clearfix" ng-submit="uploadExternalLink(subRow)">
                        <div class="form-group">
                        <lable>Title</lable>
                        <input type="text" name="externalLinkTitle" ng-model="subRow.externalLinkTitle" class="form-control" placeholder="Enter Title" />
                        </div>
                        <div class="form-group">
                        <lable>URL</lable>
                        <input type="text" name="externalLinkUrl" ng-model="subRow.externalLinkUrl" class="form-control" placeholder="Enter URL" />
                        </div>
                        <button type="submit" class="btn btn-default pull-right">Add Link</button>
                        </form>
                        </div>
                        <div id="sourcecode-tabs">
                        <form>
                        <div class="form-group">
                        <input type="file" class="form-control"
                        data-postid="<?php echo $post_dtls['Post']['id']; ?>"
                        data-lectureid="{{ subRow.lectureID }}"
                        file-model="uploadSourceCodeFile"
                        ng-model="sourceCodeFile"
                        accept=".docx, .doc, .pdf" />
                        <span>{{ sourceCodeProgress }}</span>
                        </div>
                        </form>
                        <div class="tips">
                        <p>
                        <b>Tip:</b>
                        Supported file PDF, DOCX, DOC
                        </p>
                        </div>
                        </div>
                        </div>

                        </div>
                        </div>

                        </div>

                        <div class="gray-container has-header tablet-m0" ng-if="isLectureAddContentShown(subRow)">
                        <div class="header">
                        Select content
                        <a class="close-btn container-switch fa fa-close" href="javascript:void(0)"
                        ng-click="toggleLectureAddContent(subRow)"></a>
                        </div>
                        <div class="main types">
                        <form class="clearfix" ng-submit="uploadExternalLink(subRow)">
                        <div class="form-group">
                        <input type="file" class="form-control"
                        data-postid="<?php echo $post_dtls['Post']['id']; ?>"
                        data-lectureid="{{ subRow.lectureID }}"
                        file-model="uploadLectureContent"
                        ng-model="uploadlecture"
                        accept=".jpg, .jpeg, .png, .gif, .mp4, .mkv, .flv, .pdf, .doc, .docx, .ppt, .mp3, .wav, .wma" />
                        <span>{{progress}}</span>
                        </div>
                        <p style="font-size: 16px;">

                        <b> Free Member Status :</b>
                        <input
                        name="data[Lecture][status]"
                        ng-true-value="1"
                        ng-false-value="0"
                        ng-init="memberStatusC = 0"
                        ng-model="memberStatusC"
                        id="LectureStatus"
                        type="checkbox">
                        </p>
                        </form>
                        <div class="tips">
                        <p>
                        <b>Tip:</b>
                        Supported file jpg, jpeg, png, gif, mp4, mkv, flv, pdf, doc, docx, ppt, mp3, wav, wma
                        </p>
                        </div>
                        </div>
                        </div>

                        <div class="cfi-lecture cfi-edit-form editing " ng-class="isEditLectureTitleShown(subRow) ? 'opened' : 'closed'">
                        <span class="content ui-state-default">
                        <span class="cfi-item-type">Lecture</span>
                        <span class="cfi-item-number">1:</span>
                        <span class="cfi-item-title">
                        <form>
                        <div class="form-group row">
                        <label class="col-md-2 lecture_label"></label>
                        <span class="col-md-10">
                        <input name="lecture_title" class="form-control form_part form_text" ng-model="subRow.title" maxlength="80" value="" placeholder="Enter a title" type="text" />
                        <div class="number">{{80 - subRow.title.length}}</div>
                        </span>
                        </div>

                        <div class="form-group pull-right cancel_button">
                        <span class="col-md-12">
                        <button type="button" class="btn btn-primary" ng-click="toggleEditLectureTitle(subRow)" style="margin-right: 7px;">Cancel</button>
                        <button type="submit" class="btn btn-primary" ng-click="changeLectureTitle({id: subRow.lectureID, title: subRow.title}, subRow)">Save Lecture</button>
                        </span>
                        </div>

                        <div class="clearfix"></div>
                        </form>
                        </span>
                        </span>
                        </div>
                    </script>
                    <!-- Lecture template end -->

                    <!-- Quiz template Start -->
                    <script type="text/ng-template" id="quizTemplate">

                        <div class="cfi-content" ng-class="isEditQuizTitleDescShown(subRow) ? 'closed' : ''">
                        <span class="content ui-state-default">
                        <span class="cfi-item-type">{{ subRow.type }}</span>
                        <span class="cfi-item-number">1:</span>
                        <span class="cfi-item-title" data-purpose="lecture-title-shown">{{ subRow.title }}</span>
                        <i class="edit-handle fa fa-pencil" ng-click="toggleEditQuizTitleDesc(subRow)"></i>
                        <span class="js-delete-handle delete-handle fa fa-trash" ng-click="deleteQuizFunction(subRow, $event)"></span>
                        <span class="action-buttons pull-right">
                        <a href="javascript:void(0)"
                        data-purpose="add-content"
                        class="add-content btn btn-primary btn-sm container-switch floating m0-10 m5-30-xs m5-30-md"
                        ng-class="isSelectQuestionTypeShown(subRow)"
                        ng-click="toggleSelectQuestionType(subRow)">
                        + Add Questions
                        </a>
                        <span class="cfi-item-previewable" style="display: none;">(Preview enabled)</span>
                        <i class="fa  container-switch"
                        ng-class="isShowQuestionsShown(subRow) ? 'fa-chevron-up' : 'fa-chevron-down'"
                        ng-click="toggleShowQuestions(subRow)"
                        ng-if="subRow.haveQuestion != ''"></i>
                        <i class="fa fa-bars sort-handle"></i>
                        </span>
                        </span>
                        </div>

                        <div class="gray-container has-header select-question-type tablet-m0"
                        ng-if="isSelectQuestionTypeShown(subRow) && !isAddMultiChoiceQuestShown(subRow) && !isAddTFQuestShown(subRow)&& !isAddOrdQuestShown(subRow) && !isAddSEQuestShown(subRow)&& !isAddFBQuestShown(subRow) && !isAddMatchQuestShown(subRow)">
                        <div class="header sub-menu">
                        Select question type
                        <i class="close-btn container-switch fa fa-close"
                        ng-click="toggleSelectQuestionType(subRow)"></i>
                        </div>
                        <div class="main types sub-menu">
                        <a class="lecture-icons quiz-multi"
                        data-type="multiple-choice" href="javascript:void(0)"
                        ng-click="addQuizeQuestion(subRow)">
                        <span>
                        Multiple Choice
                        </span>
                        </a>
                        </div>
                         <div class="main types sub-menu">
                        <a class="lecture-icons quiz-multi"
                        data-type="true-false" href="javascript:void(0)"
                        ng-click="addQuizeQuestionTF(subRow)">
                        <span>
                        True/False
                        </span>
                        </a>
                       </div>
                        <div class="main types sub-menu">
                        <a class="lecture-icons quiz-multi"
                        data-type="ordering" href="javascript:void(0)"
                        ng-click="addQuizeQuestionOrd(subRow)">
                        <span>
                        Ordering
                        </span>
                        </a>
                        </div>
                        <div class="main types sub-menu">
                        <a class="lecture-icons quiz-multi"
                        data-type="sa" href="javascript:void(0)"
                        ng-click="addQuizeQuestionSE(subRow)">
                        <span>
                        Short Answer/Essay Question
                        </span>
                        </a>
                        </div>
                        <div class="main types sub-menu">
                        <a class="lecture-icons quiz-multi"
                        data-type="multiple-choice" href="javascript:void(0)"
                        ng-click="addQuizeQuestionFB(subRow)">
                        <span>
                        Fill-in-the-blank
                        </span>
                        </a>
                        </div>
                        <div class="main types sub-menu">
                         <a class="lecture-icons quiz-multi"
                        data-type="multiple-choice" href="javascript:void(0)"
                        ng-click="addQuizeQuestionMatch(subRow)">
                        <span>
                        Matching
                        </span>
                        </a>
                        </div>
                        </div>

                        <div class="gray-container has-header tablet-m0"
                        ng-if="isAddMultiChoiceQuestShown(subRow)">
                        <div class="header">
                        Add Multiple Choice
                        <a class="fa fa-close close-btn container-switch" href="javascript:void(0)"
                        ng-click="closeAddMultiChoiceQuest(subRow)"></a>
                        </div>
                        <div class="assessment-create-container">
                        <form class="" ng-submit="addEditQuestion(questionIndex, subRow,'multiple-choice')">
                        <div class="form-group" id="form-item-question">
                        <label>Question:*:</label>
                        <textarea class="form-control" name="subRow.questions[questionIndex].question" placeholder="Add your question herer"
                        ng-model="subRow.questions[questionIndex].question"></textarea>
                        </div>

                        <div id="form-item-answers">
                        <label>Answers:*:</label>

                        <div class="form-group clearfix row" ng-repeat="option in subRow.questions[questionIndex].options">
                        <div class="col-sm-1">
                        <input name="subRow.questions[questionIndex].answer" type="radio"
                        ng-model="subRow.questions[questionIndex].answer"
                        ng-checked="(subRow.questions[questionIndex].answer == option.answer) && option.answer != ''"
                        value="{{option.answer}}" />
                        </div>
                        <div class="col-sm-10">
                        <div class="row">
                        <textarea name="option.answer" class="form-control mb-2" placeholder="Add an answer"
                        ng-model="option.answer"
                        ng-class="{'last':$last}"
                        textinput-question-answer="addNewOptionRow(questionIndex, subRow, $index, $event)" required></textarea>
                        <input type="text" name="option.explanation" class="form-control" placeholder="Explain why this is or isn't the best answer."
                        ng-model="option.explanation" />
                        </div>
                        </div>
                        <div class="col-sm-1">
                        <i class="fa fa-trash" ng-click="deleteAnswerRow(questionIndex, subRow, $index, $event)"></i>
                        </div>
                        </div>

                        </div>

                        <input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

                        <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>

                        </div>
                        </form>
                        </div>
                        </div>


                         <div class="gray-container has-header tablet-m0"
                        ng-if="isAddTFQuestShown(subRow)">
                        <div class="header">
                        Add True False
                        <a class="fa fa-close close-btn container-switch" href="javascript:void(0)"
                        ng-click="closeAddTFQuest(subRow)"></a>
                        </div>
                        <div class="assessment-create-container">
                        <form class="" ng-submit="addEditQuestion(questionIndex, subRow, 'true-false')">
                        <div class="form-group" id="form-item-question">
                        <label>Question:*:</label>
                        <textarea class="form-control" name="subRow.questions[questionIndex].question" placeholder="Add your question herer"
                        ng-model="subRow.questions[questionIndex].question"></textarea>
                        </div>

                        <div id="form-item-answers">
                        <label>Answer:*:</label>

                        <div class="form-group clearfix row" ng-repeat="option in subRow.questions[questionIndex].options">
                        <div class="col-sm-1">
                        <input name="subRow.questions[questionIndex].answer" type="radio"
                        ng-model="subRow.questions[questionIndex].answer"
                        ng-checked="(subRow.questions[questionIndex].answer == option.answer) && option.answer != ''"
                        value="{{option.answer}}" />
                        </div>
                        <div class="col-sm-10">
                        <div class="row">
                        <textarea name="option.answer" class="form-control mb-2" placeholder="Answer Option"
                        ng-model="option.answer"
                        ng-class="{'last':$last}"
                        required></textarea>
                        <input type="text" name="option.explanation" class="form-control" placeholder="Explain why this is or isn't true answer."
                        ng-model="option.explanation" />
                        </div>
                        </div>

                        </div>

                        </div>

                        <input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

                        <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>

                        </div>
                        </form>
                        </div>
                        </div>

                         <div class="gray-container has-header tablet-m0"
                        ng-if="isAddOrdQuestShown(subRow)">
                        <div class="header">
                        Add Ordering
                        <a class="fa fa-close close-btn container-switch" href="javascript:void(0)"
                        ng-click="closeAddOrdQuest(subRow)"></a>
                        </div>
                        <div class="assessment-create-container">
                        <form class="" ng-submit="addEditQuestion(questionIndex, subRow, 'ordering')">
                        <div class="form-group" id="form-item-question">
                        <label>Question:*:</label>
                        <textarea class="form-control" name="subRow.questions[questionIndex].question" placeholder="Add your question herer"
                        ng-model="subRow.questions[questionIndex].question"></textarea>
                        </div>

                        <div id="form-item-answers">
                        <label>Answer:*:</label>

                        <div class="form-group clearfix row" ng-repeat="option in subRow.questions[questionIndex].options">
                        <div class="col-sm-1">
<!--                        <input name="subRow.questions[questionIndex].answer"
                        ng-model="subRow.questions[questionIndex].answer"
                        ng-checked="(subRow.questions[questionIndex].answer == option.answer) && option.answer != ''"
                        value="{{option.answer}}" />-->
                        </div>
                        <div class="col-sm-10">
                        <div class="row">
                        <textarea name="option.answer" class="form-control mb-2" placeholder="Answer"
                        ng-model="option.answer"
                        ng-class="{'last':$last}"
                        textinput-question-answer="addNewOptionRow(questionIndex, subRow, $index, $event)" required></textarea>
                        <input type="text" name="option.order" class="form-control" placeholder="order no"
                        ng-model="option.order" />
                        </div>
                        </div>
                        <div class="col-sm-1">
                        <i class="fa fa-trash" ng-click="deleteAnswerRow(questionIndex, subRow, $index, $event)"></i>
                        </div>
                        </div>

                        </div>

                        <input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

                        <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>

                        </div>
                        </form>
                        </div>
                        </div>

                         <div class="gray-container has-header tablet-m0"
                        ng-if="isAddSEQuestShown(subRow)">
                        <div class="header">
                        Add Short Answer/Essay
                        <a class="fa fa-close close-btn container-switch" href="javascript:void(0)"
                        ng-click="closeAddSEQuest(subRow)"></a>
                        </div>
                        <div class="assessment-create-container">
                        <form class="" ng-submit="addEditQuestion(questionIndex, subRow, 'shortans')">
                        <div class="form-group" id="form-item-question">
                        <label>Question:*:</label>
                        <textarea class="form-control" name="subRow.questions[questionIndex].question" placeholder="Add your question herer"
                        ng-model="subRow.questions[questionIndex].question"></textarea>
                        </div>

                        <div id="form-item-answers">
                        <label>Answer:*:</label>

                        <div class="form-group clearfix row" ng-repeat="option in subRow.questions[questionIndex].options">

                        <div class="col-sm-10">
                        <div class="row">
                        <textarea name="option.characterLimit" class="form-control mb-2" placeholder="Character limit"
                        ng-model="option.characterLimit"
                        ng-class="{'last':$last}"
                        required></textarea>

                        </div>
                        </div>

                        </div>

                        </div>

                        <input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

                        <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>

                        </div>
                        </form>
                        </div>
                        </div>

                         <div class="gray-container has-header tablet-m0"
                        ng-if="isAddFBQuestShown(subRow)">
                        <div class="header">
                        Add Fill in the Blank
                        <a class="fa fa-close close-btn container-switch" href="javascript:void(0)"
                        ng-click="closeAddFBQuest(subRow)"></a>
                        </div>
                        <div class="assessment-create-container">
                        <form class="" ng-submit="addEditQuestion(questionIndex, subRow, 'fillblank')">
                        <div class="form-group" id="form-item-question">
                        <label>Question:*:</label>
                        <textarea class="form-control" name="subRow.questions[questionIndex].question" placeholder="Add your question herer"
                        ng-model="subRow.questions[questionIndex].question"></textarea>
                        </div>

                        <div id="form-item-answers">
                        <label>Answer:*:</label>

                        <div class="form-group clearfix row" ng-repeat="option in subRow.questions[questionIndex].options">
                        <div class="col-sm-1">
                        <input name="subRow.questions[questionIndex].answer" type="radio"
                        ng-model="subRow.questions[questionIndex].answer"
                        ng-checked="(subRow.questions[questionIndex].answer == option.answer) && option.answer != ''"
                        value="{{option.answer}}" />
                        </div>
                        <div class="col-sm-10">
                        <div class="row">
                        <textarea name="option.answer" class="form-control mb-2" placeholder="Add an answer"
                        ng-model="option.answer"
                        ng-class="{'last':$last}"
                        textinput-question-answer="addNewOptionRow(questionIndex, subRow, $index, $event)" required></textarea>
                        <input type="text" name="option.explanation" class="form-control" placeholder="Explain why this is or isn't the best answer."
                        ng-model="option.explanation" />
                        </div>
                        </div>
                        <div class="col-sm-1">
                        <i class="fa fa-trash" ng-click="deleteAnswerRow(questionIndex, subRow, $index, $event)"></i>
                        </div>
                        </div>

                        </div>

                        <input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

                        <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>

                        </div>
                        </form>
                        </div>
                        </div>

                         <div class="gray-container has-header tablet-m0"
                        ng-if="isAddMatchQuestShown(subRow)">
                        <div class="header">
                        Add Matching
                        <a class="fa fa-close close-btn container-switch" href="javascript:void(0)"
                        ng-click="closeAddMatchQuest(subRow)"></a>
                        </div>
                        <div class="assessment-create-container">
                        <form class="" ng-submit="addEditQuestion(questionIndex, subRow, 'matching')">
                        <div class="form-group" id="form-item-question">
                        <label>Question:*:</label>
                        <textarea class="form-control" name="subRow.questions[questionIndex].question" placeholder="Add your question herer"
                        ng-model="subRow.questions[questionIndex].question"></textarea>
                        </div>

                        <div id="form-item-answers">
                        <label>Answer:*:</label>

                        <div class="form-group clearfix row" ng-repeat="option in subRow.questions[questionIndex].options">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                        <div class="row">
                        <textarea name="option.answer" class="form-control mb-2" placeholder="Question"
                        ng-model="option.answer"
                        ng-class="{'last':$last}"
                        textinput-question-answer="addNewOptionRow(questionIndex, subRow, $index, $event)" required></textarea>
                        <input type="text" name="option.match_ans" class="form-control" placeholder="Matching Answer"
                        ng-model="option.match_ans" />
                        </div>
                        </div>
                       <div class="col-sm-1">
                        <i class="fa fa-trash" ng-click="deleteAnswerRow(questionIndex, subRow, $index, $event)"></i>
                        </div>
                        </div>

                        </div>


                        <input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

                        <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>

                        </div>
                        </form>
                        </div>
                        </div>

                        <div class="gray-container quiz-instructor-view"
                        ng-if="isShowQuestionsShown(subRow) && !isSelectQuestionTypeShown(subRow)">
                        <div class="question-list-wrapper ">
                        <div class="top fxac fxdc-xs">
                        <h4>Questions</h4>
                        <a href="javascript:void(0)" class="btn btn-sm btn-default add mb10-xs mt10-xs add-question"
                        ng-click="toggleSelectQuestionType(subRow)">+ New Question</a>
                        </div>

                        <ul class="question-list ud-jq-sortable ui-sortable" data-placeholder="ui-state-highlight" data-handle="span.sort-handle" data-tolerance="pointer">
                        <li data-id="{{ question.id }}"
                        data-type="assessment"
                        data-assessmenttype="{{question.Qtype}}"
                        ng-repeat="question in subRow.questions">
                        <div class="ellipsis prompt">{{ question.question }}</div>

                        <span>{{question.Qtype}}</span>
                        <span class="edit-handle  fa fa-pencil"
                        ng-click="editQuizeQuestion($index, subRow)"></span>
                        <span class="delete-handle  fa fa-trash"
                        ng-click="deleteQuizeQuestion($index, subRow)"></span>
                        <!-- <span class="sort-handle fa fa-bars pull-right"></span> -->
                        </li>
                        </ul>
                        </div>
                        </div>

                        <div class="cfi-quiz cfi-edit-form editing " ng-class="isEditQuizTitleDescShown(subRow) ? 'opened' : 'closed'">
                        <span class="content ui-state-default">
                        <span class="cfi-item-type">Quiz</span>
                        <span class="cfi-item-number">1:</span>
                        <span class="cfi-item-title">
                        <form>
                        <div class="form-group row">
                        <label class="col-md-2 lecture_label"></label>
                        <span class="col-md-10">
                        <input name="lecture_title" class="form-control form_part form_text" ng-model="subRow.title" maxlength="80" value="" placeholder="Enter a title" type="text" />
                        <div class="number">{{80 - subRow.title.length}}</div>
                        </span>
                        </div>

                        <div class="form-group row">
                        <label class="col-md-2 lecture_label"></label>
                        <span class="col-md-10">
                        <textarea name="lecture_title" class="form-control form_part form_text" ng-model="subRow.description" value="" placeholder="Enter Description" type="text"></textarea>
                        </span>
                        </div>

                        <div class="form-group pull-right cancel_button">
                        <span class="col-md-12">
                        <button type="button" class="btn btn-primary" ng-click="toggleEditQuizTitleDesc(subRow)" style="margin-right: 7px;">Cancel</button>
                        <button type="submit" class="btn btn-primary"
                        ng-click="changeQuizTitleDesc({id: subRow.lectureID, title: subRow.title, description: subRow.description}, subRow)">Save Quiz</button>
                        </span>
                        </div>

                        <div class="clearfix"></div>
                        </form>
                        </span>
                        </span>
                        </div>

                    </script>
                    <!-- Quiz template End -->

                    <div style="padding:0px 0px;">
                        <div style="padding-bottom: 20px;" class="font-15">Add your course in segregated sections with lectures, practice questions, quizzes, exercises etc. below: </div>
                    <p id="msg" class="alert alert-danger" style="display: none;"></p>
                        <div>
                            <ul>
                                <li>
                                    <ul class="cfi-sortables-list">
                                        <li id="{{ row.lessonID}}" ng-repeat="row in totalStructure">
                                            <ul class="cfi-chapter-container ui-sortable" data-id="{{ row.lessonID}}">
                                                <li id="item-chapter-{{ row.lessonID}}" class="cfi-chapter cfi-sortable published" data-lessonid="{{ row.lessonID}}" data-class="chapter" data-canaddcontent="0">
                                                    <div class="cfi-content"
                                                         ng-show="!isEditSectionTitleShown(row)">
                                                        <span class="content ui-state-default">
                                                            <span class="cfi-item-type">Section</span>
                                                            <span class="cfi-item-number">1:</span>
                                                            <span class="cfi-item-title fwb" data-purpose="chapter-title-shown">{{ row.title}}</span>
                                                            <span class="edit-handle fa fa-pencil"
                                                                  data-purpose="chapter-title-edit"
                                                                  ng-show="!isEditSectionTitleShown(row)"
                                                                  ng-click="toggleEditSectionTitle(row)">
                                                            </span>

                                                            <span class="js-delete-handle delete-handle fa fa-trash"
                                                                  data-lessonid="{{ row.lessonID}}"
                                                                  ng-click="deleteThisSection($event)"
                                                                  ng-show="!isEditSectionTitleShown(row)">
                                                            </span>
                                                            <span class="action-buttons">
                                                                <span class="sort-handle outer"></span>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="cfi-chapter cfi-edit-form editing " ng-class="isEditSectionTitleShown(row) ? 'opened' : 'closed'">
                                                        <span class="content ui-state-default">
                                                            <span class="cfi-item-type">Section</span>
                                                            <span class="cfi-item-number">1:</span>
                                                            <span class="cfi-item-title">
                                                                <form>
                                                                    <div class="form-group row">
                                                                        <label class="col-md-2 lecture_label"></label>
                                                                        <span class="col-md-10">
                                                                            <input name="section_title" class="form-control form_part form_text" ng-model="row.title" maxlength="80" value="{{ row.title}}" placeholder="Enter a title" type="text">
<!--                                                                            <div class="number">{{ 80 - row.title.length}}</div>-->
                                                                        </span>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-md-2 lecture_label"></label>
                                                                        <span class="col-md-10">
                                                                            <textarea name="section_description" ng-model="row.description" class="form-control">{{ row.description}}</textarea>
                                                                            <input type="hidden" ng-model="row.lessonID" name="lesson_id">
                                                                        </span>
                                                                    </div>
                                                                    <div class="form-group pull-right cancel_button">
                                                                        <button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleEditSectionTitle(row)">Cancel</button>
                                                                        <button type="submit" class="btn btn-primary" ng-click="changeSectionTitleDesc(row.lessonID, row.title, row.description, row)">Save Section</button>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </form>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li id="item-lecture-{{subRow.lectureID}}"
                                                    ng-repeat="subRow in row.lectures" class="cfi-sortable published "
                                                    ng-class="(subRow.type == 'Quiz') ? 'cfi-quiz' : 'cfi-lecture'"
                                                    data-class="chapter" data-canaddcontent="1"
                                                    ng-include="subRow.type == 'Quiz' ? 'quizTemplate' : 'lectureTemplate'">
                                                </li>

                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <ul class="cfi-forms-list">
                                        <li>
                                            <ul class="add-forms-container">
                                                <div class="df ">

                                                    <li class="cfi-lecture cfi-add" ng-class="isAddLectureShown() || isAddQuizShown() ? 'hidden' : ''">
                                                        <a data-purpose="add-lecture-button" class="content ui-state-default add-lecture-btn" ng-click="addLecture = !addLecture">
                                                            <i class="fa fa-plus-square"></i>	Add Lecture
                                                        </a>
                                                    </li>

                                                    <li class="cfi-lecture cfi-add-form editing " ng-class="isAddLectureShown() ? 'opened' : 'closed'">
                                                        <form method="post"
                                                              ng-submit="addLectureFunction(newLecture.title)">
                                                            <div class="row">
                                                                <label class="col-md-2 lecture_label">New Lecture</label>
                                                                <span class="col-md-10">
                                                                    <div class="inputTypeField">
                                                                        <input type="text" name="lecture_title" ng-model="newLecture.title" maxlength="80" class="form-control form_part form_text" placeholder="Enter a title" required />
                                                                        <div class="number">{{80 - newLecture.title.length}}</div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                            <div class="form-group pull-right cancel_button">
                                                                <button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleAddLecture()">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Add Lecture</button>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </form>
                                                    </li>

                                                    <li class="cfi-quiz cfi-add" ng-class="isAddLectureShown() || isAddQuizShown() ? 'hidden' : ''">
                                                        <a data-purpose="add-quiz-button" ng-click="toggleAddQuiz()" class="content ui-state-default add-quiz-btn">
                                                            <i class="fa fa-plus-square"></i> Add Quiz
                                                        </a>
                                                    </li>

                                                    <li class="cfi-quiz cfi-add-form editing" ng-class="isAddQuizShown() ? 'opened' : 'closed'">
                                                        <span class="content ui-state-default">
                                                            <span class="cfi-item-type"></span>
                                                            <span class="cfi-item-title fx">
                                                                <form method="post" ng-submit="addQuizFunction({title: quiz.title, description: quiz.description})" class="mw700 p-3">
                                                                    <div class="form-group row">
                                                                        <label class="col-md-3 lecture_label col-form-label">New Quiz</label>
                                                                        <span class="col-md-9">
                                                                            <div class="inputTypeField">
                                                                                <input type="text" name="quiz_title" ng-model="quiz.title" maxlength="80" class="form-control form_part form_text" placeholder="Enter a title" required />
                                                                                <div class="number">{{80 - quiz.title.length}}</div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <span class="col-md-9 offset-md-3">
                                                                            <textarea name="quiz_description" ng-model="quiz.description" class="form-control" placeholder="Enter Description" required></textarea>
                                                                        </span>
                                                                    </div>
                                                                    <div class="form-group pull-right cancel_button">
                                                                        <span class="col-md-12">
                                                                            <button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleAddQuiz()">Cancel</button>
                                                                            <button type="submit" class="btn btn-primary">Add Quiz</button>
                                                                        </span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </form>
                                                            </span>
                                                        </span>
                                                    </li>

                                                </div>

                                                <!-- <div class="df">
                                                    <li class="cfi-coding-exercise cfi-add" ng-class="isAddLectureShown() || isAddQuizShown() ? 'hidden' : ''">
                                                        <a data-purpose="add-coding-exercise-button" class="content ui-state-default add-coding-exercise-btn" href="javascript: void(0)">
                                                            <i class="fa fa-plus-square"></i> Add Coding Exercise
                                                        </a>
                                                    </li>
                                                </div> -->

                                            </ul>
                                        </li>
                                        <li class="cfi-chapter cfi-add"
                                            ng-class="isAddNewSectionShown() ? 'hidden' : ''">
                                            <a class="content ui-state-default add-chapter-btn" data-purpose="add-chapter-button"
                                               ng-click="toggleAddNewSection()"> <i class="fa fa-plus-square"></i> Add Section </a>
                                        </li>

                                        <li class="cfi-chapter cfi-add-form editing"
                                            ng-class="isAddNewSectionShown() ? 'opened' : 'closed'">
                                            <span class="content ui-state-default">
                                                <span class="cfi-item-type"></span>
                                                <span class="cfi-item-title fx">
                                                    <form method="post" ng-submit="addSectionFunction()" class="mw700 p-3">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 col-form-label lecture_label">New Section</label>
                                                            <span class="col-md-9">
                                                                <input type="text" name="chapter_title" ng-model="chapter.title" class="form-control form_part form_text" placeholder="Enter a title" maxlength="80" required />
                                                                <div class="number">{{ 80 - chapter.title.length}}</div>
                                                            </span>
                                                        </div>
                                                        <div class="form-group row">
                                                            <span class="col-md-9 offset-md-3">
                                                                <label><strong>What will students be able to do at the end of this section?</strong></label>
                                                                <textarea name="chapter_description" ng-model="chapter.description" class="form-control" placeholder="Enter Description" required></textarea>
                                                            </span>
                                                        </div>
                                                        <div class="form-group pull-right cancel_button">
                                                            <span class="col-md-12">
                                                                <button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleAddNewSection()">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Add Section</button>
                                                            </span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </span>
                                            </span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
var serializeData = function (obj, prefix) {
    var str = [];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
            str.push(typeof v == "object" ?
                    serialize(v, k) :
                    encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
};

var app = angular.module('myApp', []);

app.config(function ($httpProvider) {
    // send all requests payload as query string
    $httpProvider.defaults.transformRequest = function (data) {
        if (data === undefined) {
            return data;
        }
        return serializeData(data);
    };

    // set all post requests content type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
});

app.service('lessonService', ['$http', '$q', function ($http, $q) {

            this.updateLectures = function (data) {
                var request = $http({
                    method: 'POST',
                    url: '<?php echo $this->webroot; ?>lessons/sprint_status_order_new',
                    data: data
                });
                return request.then(handleSuccess, handleError);
            };

            this.getAll = function (post_id) {
                var request = $http({
                    method: 'GET',
                    url: '<?php echo $this->webroot; ?>posts/getAllCurriculum/' + post_id
                });

                return request.then(handleSuccess, handleError);
            };

            function handleSuccess(response) {
                return response.data;
            }

            function handleError(response) {
                if (
                        !angular.isObject(response.data) ||
                        !response.data.message
                        ) {
                    return $q.reject("An unknown error occurred.");
                }
                // Otherwise, use expected error message.
                return $q.reject(response.data.message);
            }
        }]);

app.controller('myCtrl', ['$rootScope', '$scope', '$q', '$http', 'lessonService', '$filter', function ($rootScope, $scope, $q, $http, lessonService, $filter) {

                $scope.totalStructure = [];

                $scope.initializePage = function () {
                    lessonService.getAll('<?php echo $post_dtls['Post']['id']; ?>')
                            .then(function (res) {
                                if (res.ack == '1') {
                                    $scope.totalStructure = res.lessonsArray;
                                } else {
                                    $scope.totalStructure = [];
                                }
                            });
                };

                $scope.initializePage();

                $scope.changeSectionTitleDesc = function (lessonid, title, description, row) {
                    $http({
                        url: '<?php echo $this->webroot; ?>lessons/ajaxEditLesson/' + lessonid,
                        method: 'POST',
                        data: {
                            'data[Lesson][title]': title,
                            'data[Lesson][description]': description
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    console.log(res.res);
                                    $scope.toggleEditSectionTitle(row);
                                } else {
                                    console.log(res.res);
                                }
                            });
                };

                $scope.saveLectureDescription = function (lectureid, description, subRow) {
                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxEditLectureNew/' + lectureid,
                        method: 'POST',
                        data: {
                            'data[Lecture][description]': description,
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    console.log(res.res);
                                    $scope.toggleShowEditContainer(subRow);
                                } else {
                                    console.log(res.res);
                                }
                            });
                };

                $scope.addLectureFunction = function (title) {

                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxAddLecture/<?php echo $post_dtls['Post']['id']; ?>',
                        method: 'POST',
                        data: {
                            'data[lecture][title]': title,
                        }
                    })
                    .then(function (response) {
                        var res = response.data;
                        if (res.Ack == '1') {
                            console.log(res.res);
                            $scope.toggleAddLecture();
                            $scope.initializePage();
                            $scope.newLecture.title = '';
                        } else {
                            console.log(res.res);
                        }
                    });
                };

                $scope.deleteLectureFunction = function (subRow, $event) {
                    var el = $event.target;
                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxDeleteLectureNew',
                        method: 'POST',
                        data: {
                            'data[lecture_id]': subRow.lectureID,
                        }
                    })
                    .then(function (response) {
                        var res = response.data;
                        if (res.ack == '1') {
                            angular.element(el).closest('.cfi-sortable').remove();
                            $scope.update_position();
                        } else {
                            console.log(res.res);
                        }
                    });
                };

                $scope.deleteQuizFunction = function (subRow, $event) {
                    var el = $event.target;
                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxDeleteLectureNew',
                        method: 'POST',
                        data: {
                            'data[lecture_id]': subRow.lectureID,
                        }
                    })
                    .then(function (response) {
                        var res = response.data;
                        if (res.ack == '1') {
                            angular.element(el).closest('.cfi-sortable').remove();
                        } else {
                            console.log(res.res);
                        }
                    });
                };

                $scope.changeLectureTitle = function (obj, subRow) {
                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxEditLecture/' + obj.id,
                        method: 'POST',
                        data: {
                            'data[Lecture][title]': obj.title
                        }
                    })
                    .then(function (response) {
                        var res = response.data;
                        if (res.Ack == '1') {
                            $scope.toggleEditLectureTitle(subRow);
                        } else {
                            console.log(res.res);
                        }
                    });
                };

                $scope.addQuizFunction = function (obj) {

                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxAddQuiz/<?php echo $post_dtls['Post']['id']; ?>',
                        method: 'POST',
                        data: {
                            'data[quiz][name]': obj.title,
                            'data[quiz][objective]': obj.description
                        }
                    })
                    .then(function (response) {
                                                    var res = response.data;
                                                    if (res.Ack == '1') {
                                                        console.log(res.res);
                                                        $scope.toggleAddQuiz();
                                                        $scope.initializePage();
                                                        $scope.quiz = {};
                                                    } else {
                                                        console.log(res.res);
                                                    }
                                                });
                };

                $scope.changeQuizTitleDesc = function (obj, subRow) {
                    $http({
                        url: '<?php echo $this->webroot; ?>lectures/ajaxEditLecture/' + obj.id,
                        method: 'POST',
                        data: {
                            'data[Lecture][title]': obj.title,
                            'data[Lecture][description]': obj.description
                        }
                    })
                    .then(function (response) {
                        var res = response.data;
                        if (res.Ack == '1') {
                            $scope.toggleEditQuizTitleDesc(subRow);
                        } else {
                            console.log(res.res);
                        }
                    });
                };

                $scope.questionIndex = '';

                //MCQ .............................................
                $scope.addQuizeQuestion = function (subRow) {

                    if (subRow.questions.length > 0) {
                        $scope.questionIndex = subRow.questions.length;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                        //console.log(subRow.questions[$scope.questionIndex]);
                    } else {
                        $scope.questionIndex = 0;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                    }
                    $scope.toggleAddMultiChoiceQuest(subRow);
                };
                //True false .............................................
                $scope.addQuizeQuestionTF = function (subRow) {
alert(1);
                    if (subRow.questions.length > 0) {
                        $scope.questionIndex = subRow.questions.length;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    is_true: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                        console.log(subRow.questions[$scope.questionIndex]);
                    } else {
                        $scope.questionIndex = 0;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    is_true: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                    }
                    $scope.toggleAddTFQuest(subRow);
                };
                //ordering .............................................
                $scope.addQuizeQuestionOrd = function (subRow) {
                    alert('order');
                    if (subRow.questions.length > 0) {
                        $scope.questionIndex = subRow.questions.length;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                        //console.log(subRow.questions[$scope.questionIndex]);
                    } else {
                        $scope.questionIndex = 0;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                },
                                {
                                    answer: '',
                                    order: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                    }
                    $scope.toggleAddOrdQuest(subRow);
                };
                //short essay .............................................
                $scope.addQuizeQuestionSE = function (subRow) {

                    if (subRow.questions.length > 0) {
                        $scope.questionIndex = subRow.questions.length;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    characterLimit: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                        //console.log(subRow.questions[$scope.questionIndex]);
                    } else {
                        $scope.questionIndex = 0;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    characterLimit: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                    }
                    $scope.toggleAddSEQuest(subRow);
                };
                //FB...........
                $scope.addQuizeQuestionFB = function (subRow) {

                    if (subRow.questions.length > 0) {
                        $scope.questionIndex = subRow.questions.length;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                        //console.log(subRow.questions[$scope.questionIndex]);
                    } else {
                        $scope.questionIndex = 0;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                               {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                },
                                {
                                    answer: '',
                                    explanation: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                    }
                    $scope.toggleAddFBQuest(subRow);
                };
                //Matching...............................................
                $scope.addQuizeQuestionMatch = function (subRow) {

                    if (subRow.questions.length > 0) {
                        $scope.questionIndex = subRow.questions.length;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                        //console.log(subRow.questions[$scope.questionIndex]);
                    } else {
                        $scope.questionIndex = 0;
                        var newQuestion = {
                            id: '',
                            question: '',
                            options: [
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                },
                                {
                                    answer: '',
                                    match_ans: ''
                                }
                            ],
                            answer: ''
                        };

                        subRow.questions.push(newQuestion);
                    }
                    $scope.toggleAddMatchQuest(subRow);
                };


                $scope.editQuizeQuestion = function (index, subRow) {
                    $scope.questionIndex = index;
                    console.log(subRow.questions[index]);
                    $scope.toggleAddMultiChoiceQuest(subRow);
                    $scope.toggleShowQuestions(subRow);
                };

                $scope.addEditQuestion = function ($index, subRow,Qtype) {
                                                            $http({
                                                                url: '<?php echo $this->webroot; ?>lectures/ajaxAddQuizQuestion',
                                                                method: 'POST',
                                                                data: {
                                                                    'lecture_id': subRow.lectureID,
                                                                    'Qtype': Qtype,
                                                                    'data[Question][id]': subRow.questions[$index].id,
                                                                    'data[Question][question]': subRow.questions[$index].question,
                                                                    'data[Question][options]': angular.toJson(subRow.questions[$index].options),
                                                                    'quiz_ans': subRow.questions[$index].answer
                                                                }
                                                            })
                                                                    .then(function (response) {
                                                                        var res = response.data;
                                                                        if (res.Ack == '1') {
                                                                            console.log(res.res);
                                                                            $scope.initializePage();
                                                                        } else {
                                                                            console.log(res.res);
                                                                            subRow.questions.splice(index, $index);
                                                                        }

                                                                    });

                                                            $scope.closeAddMultiChoiceQuest(subRow);
                                                        }

                $scope.deleteQuizeQuestion = function ($index, subRow) {
                                                            $http({
                                                                url: '<?php echo $this->webroot; ?>lectures/ajaxDeleteQuizQuestion',
                                                                method: 'POST',
                                                                data: {
                                                                    'data[Question][id]': subRow.questions[$index].id
                                                                }
                                                            })
                                                                    .then(function (response) {
                                                                        var res = response.data;
                                                                        if (res.Ack == '1') {
                                                                            $scope.initializePage();
                                                                        } else {
                                                                            subRow.questions.splice(index, $index);
                                                                        }

                                                                    });
                                                        };

                $scope.uploadDownloadableFile = function (file, attrs, scope) {
                    var fd = new FormData();
                    fd.append('code_file', file[0]);
                    fd.append('lecture_id', attrs.lectureid);
                    fd.append('post_id', attrs.postid);

                    $http({
                        method: "POST",
                        url: '<?php echo $this->webroot; ?>lectures_libraries/ajax_add_downloadable_file',
                        data: fd,
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined},
                        uploadEventHandlers: {
                            progress: function(e) {
                                if (e.lengthComputable) {
                                   uploadProgress = Math.round(e.loaded * 100 / e.total);
                                   scope.uploadProgress = uploadProgress + '%';
                                   if (e.loaded == e.total) {
                                       scope.uploadProgress = "File upload finished!";
                                   }
                               }
                           }
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    $scope.initializePage();
                                }
                            });
                };

                $scope.uploadSourceCodeFile = function (file, attrs, scope) {
                    var fd = new FormData();
                    fd.append('code_file', file[0]);
                    fd.append('lecture_id', attrs.lectureid);
                    fd.append('post_id', attrs.postid);

                    $http({
                        method: "POST",
                        url: '<?php echo $this->webroot; ?>lectures_libraries/ajax_add_sourcecode_file',
                        data: fd,
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined},
                        uploadEventHandlers: {
                            progress: function(e) {
                                if (e.lengthComputable) {
                                   sourceCodeProgress = Math.round(e.loaded * 100 / e.total);
                                   scope.sourceCodeProgress = sourceCodeProgress + '%';
                                   if (e.loaded == e.total) {
                                       scope.sourceCodeProgress = "File upload finished!";
                                   }
                               }
                           }
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    $scope.initializePage();
                                }
                            });
                };

                $scope.uploadLectureContent = function (file, attrs, scope) {
                    var fd = new FormData();
                    fd.append('data[Lecture][media]', file[0]);
                    fd.append('lecture_id', attrs.lectureid);
                    fd.append('post_id', attrs.postid);
                    fd.append('status',scope.memberStatusC);
                    //console.log(scope.memberStatusC);
                    $http({
                        method: "POST",
                        url: '<?php echo $this->webroot; ?>lectures/ajaxAddLectureContent/' + attrs.lectureid,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined},
                        uploadEventHandlers: {
                            progress: function(e) {
                                if (e.lengthComputable) {
                                   progress = Math.round(e.loaded * 100 / e.total);
                                   scope.progress = progress + '%';
                                   //console.log("progress: " + progress + "%");
                                   if (e.loaded == e.total) {
                                       scope.progress = "File upload finished!";
                                     //console.log("File upload finished!");
                                     //console.log("Server will perform extra work now...");
                                   }
                               }
                           }
                        }
                    })
                    .then(function (response) {

                        var res = response.data;

                        //alert(res.Ack);
                        if (res.Ack =='1') {
                           // alert(1);

                            $scope.initializePage();
                        }
                        else
                        {
                            //alert(2);
                            $('#msg').html(res.res);
                           $('#msg').show();
                              setTimeout(function () {
                        $('#msg').hide();

                    }, 4000);

                         }
                        },
                    function (err){
                        console.log(err);
                    });
                };

                $scope.uploadExternalLink = function (subRow) {
                                                            $http({
                                                                url: '<?php echo $this->webroot; ?>lectures_libraries/ajax_add_external_resource',
                                                                method: 'POST',
                                                                data: {
                                                                    'lecture_id': subRow.lectureID,
                                                                    'post_id': '<?php echo $post_dtls['Post']['id']; ?>',
                                                                    'title': subRow.externalLinkTitle,
                                                                    'url': subRow.externalLinkUrl
                                                                }
                                                            })
                                                                    .then(function (response) {
                                                                        var res = response.data;
                                                                        if (res.asset.library_type == '1') {
                                                                            subRow.downloadablefile.push(res.asset);
                                                                            subRow.haveDownloadableFile = 1;
                                                                        }
                                                                        if (res.asset.library_type == '2') {
                                                                            subRow.externallink.push(res.asset);
                                                                            subRow.haveExternalLink = 1;
                                                                        }
                                                                    });
                                                            $scope.toggleAddResourceTab(subRow);
                                                        };

                $scope.openAddResourceTab = function (subRow) {
                                                            $http({
                                                                url: '<?php echo $this->webroot; ?>lectures_libraries/get_lectures_libraries',
                                                                method: 'GET',
                                                            })
                                                                    .then(function (response) {
                                                                        var res = response.data;
                                                                        if (res.Ack == '1') {
                                                                            subRow.assets = res.library;
                                                                        } else {
                                                                            subRow.assets = [];
                                                                        }
                                                                    });
                                                            $scope.toggleAddResourceTab(subRow);
                                                            //console.log(subRow);
                                                        };

                $scope.addFromLibrary = function (asset, subRow) {
                                                            $http({
                                                                url: '<?php echo $this->webroot; ?>lectures_libraries/update_lecture_library',
                                                                method: 'POST',
                                                                data: {
                                                                    'id': asset.id,
                                                                    'lecture_id': subRow.lectureID,
                                                                    'post_id': '<?php echo $post_dtls['Post']['id']; ?>'
                                                                }
                                                            })
                                                                    .then(function (response) {
                                                                        var res = response.data;
                                                                        if (res.Ack == '1') {
                                                                            if (res.asset.library_type == '1') {
                                                                                subRow.downloadablefile.push(res.asset);
                                                                                subRow.haveDownloadableFile = 1;
                                                                            }
                                                                            if (res.asset.library_type == '2') {
                                                                                subRow.externallink.push(res.asset);
                                                                                subRow.haveExternalLink = 1;
                                                                            }
                                                                        }
                                                                    });
                                                            $scope.toggleAddResourceTab(subRow);
                                                        };

                $scope.deleteFromLibrary = function (asset, $event) {
                                                            var el = $event.target;
                                                            $http({
                                                                url: '<?php echo $this->webroot; ?>lectures_libraries/delete_from_library',
                                                                method: 'POST',
                                                                data: {
                                                                    'id': asset.id
                                                                }
                                                            })
                                                                    .then(function (response) {
                                                                        var res = response.data;
                                                                        if (res.Ack == '1') {
                                                                            angular.element(el).closest('tr').remove();
                                                                        }
                                                                    });
                                                            $scope.toggleAddResourceTab(subRow);
                                                        };

                $scope.deleteFromResource = function (asset, subRow, $index, $event) {
                    var el = $event.target;
                    $http({
                        url: '<?php echo $this->webroot; ?>lectures_libraries/delete_from_resource',
                        method: 'POST',
                        data: {
                            'id': asset.id
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    if (asset.library_type == '1') {
                                        subRow.downloadablefile.splice($index, 1);
                                    }
                                    if (asset.library_type == '2') {
                                        subRow.externallink.splice($index, 1);
                                    }
                                    angular.element(el).closest('li').remove();
                                }
                            });
                };

                $scope.addSectionFunction = function () {
                    $http({
                        url: '<?php echo $this->webroot . 'lessons/ajaxAddSection/' . $post_dtls['Post']['id']; ?>',
                        method: 'POST',
                        data: {
                            'data[section][name]': $scope.chapter.title,
                            'data[section][objective]': $scope.chapter.description
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    $scope.toggleAddNewSection();
                                    $scope.chapter = {
                                        id: '',
                                        description: ''
                                    };
                                    $scope.initializePage();
                                }
                            });
                };

                $scope.deleteThisSection = function ($event) {
                    var sectionid = angular.element($event.currentTarget).attr('data-lessonid');
                    $http({
                        url: '<?php echo $this->webroot . 'lessons/ajax_delete_section'; ?>',
                        method: 'POST',
                        data: {
                            'section_id': sectionid,
                            'post_id': '<?php echo $post_dtls['Post']['id']; ?>'
                        }
                    })
                            .then(function (response) {
                                var res = response.data;
                                if (res.Ack == '1') {
                                    $scope.initializePage();
                                }
                            });
                };

                $scope.addNewOptionRow = function (questionIndex, subRow, $index, $event) {
                    var obj = {
                        answer: '',
                        explanation: ''
                    };
                    subRow.questions[questionIndex].options.push(obj);
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                };

                $scope.deleteAnswerRow = function (questionIndex, subRow, $index, $event) {
                    subRow.questions[questionIndex].options.splice($index, 1);
                };

                $scope.toggleAddNewSection = function () {
                    $scope.addNewSection = !$scope.addNewSection;
                };

                $scope.isAddNewSectionShown = function () {
                    return $scope.addNewSection;
                };

                $scope.toggleAddResourceTab = function (subRow) {
                    subRow.addResourceTab = !subRow.addResourceTab;
                };

                $scope.isAddResourceTabShown = function (subRow) {
                    return subRow.addResourceTab;
                };

                $scope.toggleEditLectureTitle = function (subRow) {
                    subRow.editLectureTitle = !subRow.editLectureTitle;
                };
                $scope.isEditLectureTitleShown = function (subRow) {
                    return subRow.editLectureTitle;
                };
                $scope.toggleEditSectionTitle = function (row) {
                    row.editSectionTitle = !row.editSectionTitle;
                };
                $scope.isEditSectionTitleShown = function (row) {
                    return row.editSectionTitle;
                };

                $scope.toggleIsHidden = function (subRow) {
                    subRow.isHidden = !subRow.isHidden;
                };
                $scope.isIsHiddenShown = function (subRow) {
                    return subRow.isHidden;
                };
                $scope.toggleShowEditContainer = function (subRow) {
                    subRow.showEditContainer = !subRow.showEditContainer;
                };
                $scope.isShowEditContainerShown = function (subRow) {
                    return subRow.showEditContainer;
                };
                $scope.toggleShowEditDeleteLecture = function (subRow) {
                    subRow.showEditDeleteLecture = !subRow.showEditDeleteLecture;
                };
                $scope.isShowEditDeleteLectureShown = function (subRow) {
                    return subRow.showEditDeleteLecture;
                };
                $scope.toggleEditQuizTitleDesc = function (subRow) {
                    subRow.editQuizTitleDesc = !subRow.editQuizTitleDesc;
                };
                $scope.isEditQuizTitleDescShown = function (subRow) {
                    return subRow.editQuizTitleDesc;
                };
                $scope.toggleSelectQuestionType = function (subRow) {
                    subRow.selectQuestionType = !subRow.selectQuestionType;
                }
                $scope.isSelectQuestionTypeShown = function (subRow) {
                    return subRow.selectQuestionType;
                };
                $scope.toggleAddMultiChoiceQuest = function (subRow) {
                    subRow.addMultiChoiceQuest = !subRow.addMultiChoiceQuest;
                };
                $scope.closeAddMultiChoiceQuest = function (subRow) {
                    subRow.addMultiChoiceQuest = false;
                    subRow.selectQuestionType = false;
                };
                $scope.isAddMultiChoiceQuestShown = function (subRow) {
                    return subRow.addMultiChoiceQuest;
                };

                $scope.toggleAddTFQuest = function (subRow) {
                    subRow.addTFQuest = !subRow.addTFQuest;
                };
                $scope.closeAddTFQuest = function (subRow) {
                    subRow.addTFQuest = false;
                    subRow.selectQuestionType = false;
                };
                $scope.isAddTFQuestShown = function (subRow) {
                    return subRow.addTFQuest;
                };

                $scope.toggleAddOrdQuest = function (subRow) {
                    subRow.addOrdQuest = !subRow.addOrdQuest;
                };
                $scope.closeAddOrdQuest = function (subRow) {
                    subRow.addOrdQuest = false;
                    subRow.selectQuestionType = false;
                };
                $scope.isAddOrdQuestShown = function (subRow) {
                    return subRow.addOrdQuest;
                };

                $scope.toggleAddSEQuest = function (subRow) {
                    subRow.addSEQuest = !subRow.addSEQuest;
                };
                $scope.closeAddSEQuest = function (subRow) {
                    subRow.addSEQuest = false;
                    subRow.selectQuestionType = false;
                };
                $scope.isAddSEQuestShown = function (subRow) {
                    return subRow.addSEQuest;
                };
                $scope.toggleAddFBQuest = function (subRow) {
                    subRow.addFBQuest = !subRow.addFBQuest;
                };
                $scope.closeAddFBQuest = function (subRow) {
                    subRow.addFBQuest = false;
                    subRow.selectQuestionType = false;
                };
                $scope.isAddFBQuestShown = function (subRow) {
                    return subRow.addFBQuest;
                };

                $scope.toggleAddMatchQuest = function (subRow) {
                    subRow.addMatchQuest = !subRow.addMatchQuest;
                };
                $scope.closeAddMatchQuest = function (subRow) {
                    subRow.addMatchQuest = false;
                    subRow.selectQuestionType = false;
                };
                $scope.isAddMatchQuestShown = function (subRow) {
                    return subRow.addMatchQuest;
                };


                $scope.toggleShowQuestions = function (subRow) {
                    subRow.showQuestions = !subRow.showQuestions;
                };

                $scope.isShowQuestionsShown = function (subRow) {
                    return subRow.showQuestions;
                };

                $scope.toggleLectureAddContent = function (subRow) {
                    subRow.lectureAddContent = !subRow.lectureAddContent;
                };

                $scope.isLectureAddContentShown = function (subRow) {
                    return subRow.lectureAddContent;
                };

                $scope.toggleAddLecture = function () {
                    $scope.addLecture = !$scope.addLecture;
                };
                $scope.isAddLectureShown = function () {
                    return $scope.addLecture;
                };
                $scope.toggleAddQuiz = function () {
                    $scope.addQuiz = !$scope.addQuiz;
                };
                $scope.isAddQuizShown = function () {
                    return $scope.addQuiz;
                };


//                $scope.update_position = function () {
//                    var els = angular.element('.cfi-chapter-container.ui-sortable');
//                    angular.forEach(els, function (el, i) {
//                        angular.element(el).find('.cfi-chapter .cfi-item-number').text(i + 1 + ':');
//                    });
//
//                    var els = angular.element('.cfi-sortable.cfi-quiz');
//                    angular.forEach(els, function (el, j) {
//                        angular.element(el).find('.cfi-item-number').text(j + 1 + ':');
//                    });
//
//                    var els = angular.element('.cfi-sortable.cfi-lecture');
//                    angular.forEach(els, function (el, j) {
//                        angular.element(el).find('.cfi-item-number').text(j + 1 + ':');
//                    });
//                };

        }]);

app.directive("uiSortable", ['lessonService', '$timeout', function (lessonService, $timeout) {
                return {
                    restrict: "C",
                    link: function (scope, element, attrs) {
                        element.sortable({
                            connectWith: ".ui-sortable",
                            handle: ".sort-handle",
                            //items: "> li:gt(0)",
                            create: function (event, ui) {
                                $timeout(function () {
                                    //scope.update_position();
                                    updatePosition();
                                }, 100);
                            },
                            update: function (event, ui) {
                                var values = element.sortable("toArray");
                                lessonService.updateLectures(values);
                                //scope.update_position();
                                updatePosition();
                            },
                            stop: function (event, ui) {
                                // var els = angular.element('.cfi-sortable.cfi-quiz');
                                // angular.forEach(els, function( el, i ){
                                //    angular.element(el).find('.cfi-item-number').text(i + 1 + ':');
                                // });
                                //updatePosition();
                                //scope.update_position();
                                updatePosition();
                            }
                        });

                        function updatePosition() {
                            var els = angular.element('.cfi-chapter-container.ui-sortable');
                            angular.forEach(els, function (el, i) {
                                angular.element(el).find('.cfi-chapter .cfi-item-number').text(i + 1 + ':');
                            });

                            var els = angular.element('.cfi-sortable.cfi-quiz');
                            angular.forEach(els, function (el, j) {
                                angular.element(el).find('.cfi-item-number').text(j + 1 + ':');
                            });

                            var els = angular.element('.cfi-sortable.cfi-lecture');
                            angular.forEach(els, function (el, j) {
                                angular.element(el).find('.cfi-item-number').text(j + 1 + ':');
                            });
                        }
                    }
                }
            }]);

app.directive('myTabs', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            var jqueryElm = $(elm[0]);
            elm.tabs({
                activate: function (evt, ui) {
                    if (ui.newPanel.attr('id') === 'library-tabs') {
                        //console.log('Hi');
                    }
                }
            });
        }
    };
});

app.directive('fileModel', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var uploadDownloadableFile = scope.$eval(attrs.fileModel);
            element.bind('change', function () {
                scope.$apply(function () {
                    var files = element[0].files;
                    if (files) {
                        uploadDownloadableFile(files, attrs, scope);
                    }
                });
            });
        }
    };
});

app.directive('textinputQuestionAnswer', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            //var uploadDownloadableFile = scope.$eval(attrs.textinputQuestionAnswer);
            element.on('click', function () {
                if (element.hasClass('last')) {
                    scope.$eval(attrs.textinputQuestionAnswer);
                }
            });
        }
    };
});


app.filter('dateToISO', function () {
    return function (input) {
        input = new Date(input).toISOString();
        return input;
    };
});


</script>
