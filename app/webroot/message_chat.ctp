
<section class="inner-wrapper" style="background:#EFEFF4;">
	<div class="container">
		<div class="row">
			<div class="col-md-11 middle-div">
				<div class="row">
					<div class="col-md-3 chat_left">
						<div class="chat_left_holder">
							<div class="chat_header_search">
								<form class="form-inline">
								  <div class="form-group">
								    <input type="text" class="form-control" id="exampleInputEmail2" placeholder="">
								  </div>
								  <button type="submit" class="btn btn-default">Go</button>
								</form>
							</div>
							<div class="chat_left_list">
								<?php 
									$login_id = $login_user['User']['id'];
									$login_name = $login_user['User']['first_name'].' '.$login_user['User']['last_name'];
									$login_image = $login_user['UserImage'][0]['originalpath'];
								foreach($offer_user as $user_key =>$user_val ) { 
										$user_id = $user_val['User']['id'];
										$user_name = $user_val['User']['first_name'].' '.$user_val['User']['last_name'];
										$user_image = "";

									?>
								<div class="media activs" onclick="check_button('<?php echo $user_id;?>','<?php echo $user_name;?>','<?php echo $user_image;?>','<?php echo $login_id;?>','<?php echo $login_name;?>','<?php echo $login_image;?>')" id="user_<?php echo $user_id; ?>">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								    <span class="pink">MarketPlace</span>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading"><?php echo $user_name; ?></h4>
								    <p>Hithere,Are you still looking for this...</p>
								  </div>
								  <!--<div class="media-right">
								    <span>4:40 AM</span>
								    <b>3</b>
								  </div>-->
								</div>
								<?php } ?>
								<!--<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								    <span class="sky">MarketPlace</span>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">We Love Shoes</h4>
								    <p>Hithere,Are you still looking for this...</p>
								  </div>
								  <div class="media-right">
								    <span>4:40 AM</span>
								    <b>3</b>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								    <span class="yellow">MarketPlace</span>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">We Love Shoes</h4>
								    <p>Hithere,Are you still looking for this...</p>
								  </div>
								  <div class="media-right">
								    <span>4:40 AM</span>
								    <b>3</b>
								  </div>
								</div>-->
							</div>
						</div>
					</div>
					<div class="col-md-6 chat_middle">
						<div class="chat_middle_holder">
							<div class="chat_mid_header">
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">Macbook Pro 17"</h4>
								    <p><i class="head_locatin"></i>5 miles - Lawrenceville</p>
								    <p><i class="head_times"></i>5 min ago</p>
								  </div>
								  <div class="media-right">
								    <span>$2,350</span>
								    <p>Negotiable</p>
								  </div>
								</div>
							</div>
							<div class="chat_body" id="chat_section">
								<!--<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								  </div>
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
								<div class="media media_me">
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
								<div class="sp_date">
									<span>THU, SEP 18, 2:43 PM</span>
								</div>
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								  </div>
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
								<div class="media media_me">
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>-->
							</div>
							<div class="chat_footer" id="footer_section">
								<!--<form class="form-inline">
								  <div class="form-group">
								    <input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
								  </div>
								  <div class="form-group">
								    <button type="submit" class="btn btn-default"><img src="<?php echo $this->webroot; ?>/images/flight-white.svg" style="width:25px"></button>
								  </div>
								  <div class="form-group">
								    <button type="submit" class="btn btn-default acpt_ofr">Accept Offer</button>
								  </div>
								</form>-->
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="spotlight">
							<div class="sponsord_add">
								<h3>Sponsored Ads</h3>
								<span>Based on your interests</span>
							</div>
							<div class="spotlight-holder">
								<div class="spotlight-holder-top">
									<span class="round"><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></span>
									<p>We Love Shoes <span class="small-desc">75 Products  |  19 followers</span></p>
								</div>
								<ul>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-1.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-2.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-3.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-4.png"></li>
								</ul>
								<div class="clearfix"></div>
								<p class="text-right follow-link"><a class="btn btn-default btn-sm" href="">Follow</a></p>
							</div>
							<div class="spotlight-holder">
								<div class="spotlight-holder-top">
									<span class="round"><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></span>
									<p>We Love Shoes <span class="small-desc">75 Products  |  19 followers</span></p>
								</div>
								<ul>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-1.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-2.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-3.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-4.png"></li>
								</ul>
								<div class="clearfix"></div>
								<p class="text-right follow-link"><a class="btn btn-default btn-sm" href="">Follow</a></p>
							</div>
							<div class="spotlight-holder">
								<div class="spotlight-holder-top">
									<span class="round"><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></span>
									<p>We Love Shoes <span class="small-desc">75 Products  |  19 followers</span></p>
								</div>
								<ul>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-1.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-2.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-3.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-4.png"></li>
								</ul>
								<div class="clearfix"></div>
								<p class="text-right follow-link"><a class="btn btn-default btn-sm" href="">Follow</a></p>
							</div>
							<p class="text-center">
								<a href="">See More</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="acpt_offer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Accept Offer</h4>
      </div>
      <form>
	      <div class="modal-body">
			  <div class="form-group">
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Meeting Tittle">
			  </div>
			  <div class="form-group">
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Location">
			  </div>
			  <div class="form-group">
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Price (If any)">
			  </div>
			  <div class="form-group">
			    <textarea class="form-control" id="exampleInputEmail1" placeholder="Meeting Details"></textarea>
			  </div>
			  <div class="form-group">
			  	<div class="row">
			  		<div class="col-md-6"><input type="date" class="form-control" id="exampleInputEmail1" placeholder="Date"></div>
			  		<div class="col-md-6"><input type="time" class="form-control" id="exampleInputEmail1" placeholder="Time"></div>
			  	</div>
			  </div>
			  <div class="form-group">
			   	<p>Who’s Going</p>
			   	<ul>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></li>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></li>
				</ul>
			  </div>
			  <div class="form-group">
			   	<p>Emergency Contact</p>
			   	<ul>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></li>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/plus_circle.svg"></li>
				</ul>
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	        <button type="button" class="btn btn-primary">Close Deal</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script>

	$(document).ready(function(){

	var session_id=<?php echo $_SESSION['user_id'];?>
	
	//create a new WebSocket object.
	var wsUri = "ws://localhost:25768/server.php"; 	
	websocket = new WebSocket(wsUri); 
	
	websocket.onopen = function(ev) { // connection is open 
		//$('#message_box').append("<div class=\"system_msg\">Connected!</div>"); //notify user
		console.log(Connected);
	}

	
	
	//#### Message received from server?
	websocket.onmessage = function(ev) {
                console.log(ev.data);
		var msg = JSON.parse(ev.data); //PHP sends Json data
		//alert(msg);
		var type = msg.type; //message type
		var umsg = msg.message; //message text
		var uname = msg.name; //user name
		var ucolor = msg.color; //color
		var ureceivename = msg.receive_name;
		var usenderid = msg.sender_id;
		var usreceiverid = msg.receiver_id;
		var r_name = msg.r_name;
		var r_image = msg.r_image;
		var s_name = msg.s_name;
		var s_image = msg.s_image;

		
		
		
                if(umsg!=null && uname!=null)
                {
                	
		
        
		 if(type == 'usermsg') 
		{
		
                if(ureceivename == session_id)
		 {		 
		        
                        if ($('#footer_section').length == 0){                                
                                check_button(usenderid,s_name,s_image,ureceivename,r_name,r_image);                                
                                $('#chat_section').append('<div class="media"><div class="media-left"><a href="javascript:void(0)"><img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></a></div><div class="media-body"><p>'+umsg+'</p></div></div>');
                        } else{
                                $('#chat_section').append('<div class="media"><div class="media-left"><a href="javascript:void(0)"><img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></a></div><div class="media-body"><p>'+umsg+'</p></div></div>');
                        }                

                        //$('#chat_section').append('<div class="media"><div class="media-left"><a href="javascript:void(0)"><img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></a></div><div class="media-body"><p>'+umsg+'</p></div></div>');       
                        
                        
         
                        
                        
		    

		 }
		 if(usenderid == session_id)
		 {		 
		       
                       /* $.ajax({
                            type: "post",
                            url: "<?php echo SITEURL; ?>ajax_add_chat.php",                            
                            data: {'from_id':usenderid,'to_id':ureceivename,'message':umsg,'color':ucolor},
                            success: function (data) {
                                ///alert(data);
                                
                            }
                        });*/
		    
		    
		    
                    $('#chat_section').append('<div class="media media_me"><div class="media-body"><p>'+umsg+'</p></div></div>');
                    
		 }
                 
            
		
		
	}
}
		
	};
	
	
	websocket.onerror	= function(ev){console.log(ev)};
	websocket.onclose 	= function(ev){console.log(ev)};
	
        
});


function check_button(id,name,image,sender_id,sender_name,sender_image)
	{

		//alert(id);
		
		
        
                        
                        //alert($('#main_chat_box_'+id).length);
                //var chat_str ='<div class="small_chat_box" id="main_chat_box_'+id+'"><div class="chat_head" id="chat_top_sec_'+id+'"><p>'+name+'</p><ul><li><a href="javascript:void(0)" class="fa fa-minus-square-o minz" id="min_'+id+'" onclick="minimize('+id+')"></a></li><li><a href="javascript:void(0)" class="fa fa-plus-square-o maxz" id="max_'+id+'" onclick="maximize('+id+')"></a></li></ul></div><div class="open_close" id="chat_main_sec_'+id+'"><div class="chat_body" id="chat_text_sec_'+id+'"><ul class="chat_convs" id="chat_text_body_'+id+'"></ul></div><div class="msg_text_box" id="chat_bottom_sec_'+id+'"><input type="text" name="message" id="message_'+id+'"><input type="hidden" name="receiver_name" id="receiver_name_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+id+'"><input type="hidden" name="sender_id" id="sender_id_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+sender_id+'"><input type="hidden" name="receiver_id" id="receiver_id_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+id+'"><input type="hidden" name="r_name" id="r_name_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+name+'"><input type="hidden" name="r_image" id="r_image_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+image+'"><input type="hidden" name="s_image" id="s_image_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+sender_image+'"><input type="hidden" name="s_name" id="s_name_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+sender_name+'"><button id="send-btn" onclick="send_message('+id+')"><i class="fa fa-send"></i></button></div></div></div>';
                //console.log(chat_str);

                var chat_body  = '<div class="media"><div class="media-left"><a href="javascript:void(0)"><img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></a></div><div class="media-body"><p>Hithere,Are you still looking for this...</p></div></div><div class="media media_me"><div class="media-body"><p>Hithere,Are you still looking for this...</p></div></div>';

                var chat_footer= '<form class="form-inline"><div class="form-group"><input type="text" class="form-control" name="message" id="message_'+id+'"></div><div class="form-group"><button type="button"  class="btn btn-default" id="send-btn" onclick="send_message('+id+')"><img src="<?php echo $this->webroot; ?>/images/flight-white.svg" style="width:25px"></button></div><div class="form-group"><button type="button" class="btn btn-default acpt_ofr">Accept Offer</button></div><input type="hidden" name="receiver_name" id="receiver_name_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+id+'"><input type="hidden" name="sender_id" id="sender_id_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+sender_id+'"><input type="hidden" name="receiver_id" id="receiver_id_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+id+'"><input type="hidden" name="r_name" id="r_name_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+name+'"><input type="hidden" name="r_image" id="r_image_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+image+'"><input type="hidden" name="s_image" id="s_image_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+sender_image+'"><input type="hidden" name="s_name" id="s_name_'+id+'" placeholder="" maxlength="10" style="width:20%" value="'+sender_name+'"></form>';
                //$("#chat_section").html(chat_body);
                $("#footer_section").html(chat_footer);
                
                
                        //alert($('#main_chat_box_'+id).length);
        
       /* $.ajax({
            type: "post",
            url: "<?php echo SITEURL; ?>ajax_get_chat.php",                            
            data: {'from_id':id,'to_id':sender_id},
            success: function (data) {
                if(data){
                $("#chat_text_sec_"+id).show();   
		    $('#chat_text_body_'+id).append(data);
		    }
                
            }
        });  */ 

	}

	function send_message(id)
        {
        //alert(id);
        var mymessage = $('#message_'+id).val(); //get message text
        $('#message_'+id).val('');
        //alert(mymessage);
	        var myname = $('#sender_name').val(); //get user name
	
	        if(myname == ""){ //empty name?
		        alert("Enter your Name please!");
		        return;
	        }
	        if(mymessage == ""){ //emtpy message?
		        alert("Enter Some message Please!");
		        return;
	        }

	        var receive_name = $('#receiver_name_'+id).val();
	        var sender_id =  $('#sender_id_'+id).val();
	        var receiver_id =  $('#receiver_id_'+id).val();
	        
	        var r_name = $('#r_name_'+id).val();
	        var r_image =  $('#r_image_'+id).val();
	        var s_name =  $('#s_name_'+id).val();
	        var s_image =  $('#s_image_'+id).val();
	
	        //prepare json data
	        var msg = {
	        message: mymessage,
	        name: s_name,
	        color : '000',
	        receive_name: receive_name,
	        sender_id: sender_id,
	        receiver_id: receiver_id,
	        r_name: r_name,
	        r_image: r_image,
	        s_image: s_image,
	        s_name: s_name
	
	        };
        console.log(msg);
	        //convert and send data to server
	        //websocket.send(JSON.stringify(msg));    
        }
</script>

<script>
	$('#acpt_offer').modal('show');
</script>











