<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class ExchangeRatesController extends AppController {

  public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
  var $uses = array('Post','TempCart','User','Order','Category','Rating');

  public function admin_index(){

    $is_admin = $this->Session->read('is_admin');
    if (!isset($is_admin) && $is_admin == '') {
        $this->redirect('/admin');
    }

    $this->loadModel('ExchangeRate');
    $usdExchangeRate = $this->ExchangeRate->find('all',array('conditions'=> array('ExchangeRate.base_currency'=>'usd'),'order'=>array('ExchangeRate.price ASC')));
    $eurExchangeRate = $this->ExchangeRate->find('all',array('conditions'=>array('ExchangeRate.base_currency'=>'eur'),'order'=>array('ExchangeRate.price ASC')));
    $gbpExchangeRate = $this->ExchangeRate->find('all',array('conditions'=>array('ExchangeRate.base_currency'=>'gbp'),'order'=>array('ExchangeRate.price ASC')));
    $cadExchangeRate = $this->ExchangeRate->find('all',array('conditions'=>array('ExchangeRate.base_currency'=>'cad'),'order'=>array('ExchangeRate.price ASC')));
    $audExchangeRate = $this->ExchangeRate->find('all',array('conditions'=>array('ExchangeRate.base_currency'=>'aud'),'order'=>array('ExchangeRate.price ASC')));
    $inrExchangeRate = $this->ExchangeRate->find('all',array('conditions'=>array('ExchangeRate.base_currency'=>'inr'),'order'=>array('ExchangeRate.price ASC')));

    $this->set(compact('usdExchangeRate','eurExchangeRate','gbpExchangeRate','cadExchangeRate','audExchangeRate','inrExchangeRate'));

  }

  public function ajaxSaveRate(){
    $data = array();

    //print_r($this->request->data);exit;

    $this->loadModel('ExchangeRate');

      // $details = array('exchange_rate'=>$this->request->data['value'],'discount_percentage'=>$this->request->data['promo']);
      // $condition = array('id'=>$this->request->data['id']);

      if($this->ExchangeRate->save($this->request->data)){
        $data['Ack'] = 1;
        $data['res'] = 'Exchange Rate Saved Successfully ';
      }else{
        $data['Ack'] = 0;
        $data['res'] = 'Error!';
      }

    echo json_encode($data);

    exit;
  }

  public function currencySet($baseCurrency,$price){

    $currencyList   = array("USD","GBP","EUR","AUD","CAD","INR");
    $currencySymbol = array("USD"=>"$","GBP"=>"£","EUR"=>"€","AUD"=>"A$","CAD"=>"C$","INR"=>"₹");

    // $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    // $query = json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip=' . $ip));

    // $symbolJson = file_get_contents('https://restcountries.eu/rest/v2/alpha/' . $query->geoplugin_countryCode);
    // $symbolDecoded = json_decode($symbolJson);
    // $symbolAray = $symbolDecoded->currencies;
    $symbol = $this->Session->read('symbol');
    $code = $this->Session->read('code');

    $this->loadModel('SpacialDiscount');
    $discountDetails = $this->SpacialDiscount->find('first',array('conditions'=>array('SpacialDiscount.id'=>1)));
    $discountStatus = $discountDetails['SpacialDiscount']['status'];

    if(in_array($code, $currencyList)){

      $this->loadModel('ExchangeRate');
      $exRate = $this->ExchangeRate->find('first',array('conditions'=>array('ExchangeRate.base_currency'=>$baseCurrency,'ExchangeRate.price'=>$price)));
      
      if($code==$baseCurrency){
        
        $data['currency'] = $baseCurrency;
        $data['symbol'] = $currencySymbol[$baseCurrency];
        
        if($discountStatus == 1){
          $baseCurrency = strtolower($baseCurrency);
          $data['price'] = $discountDetails['SpacialDiscount'][$baseCurrency.'_price'];;
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price'];          
        }
        
        $data['old_price'] = $exRate['ExchangeRate']['price'];    
      
      }else if($code == "USD"){

        $data['currency'] = $code;
        $data['symbol'] = $symbol;
        // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['usd_rate'];
        if($discountStatus == 1){
          $data['price'] = $discountDetails['SpacialDiscount']['usd_price'];
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['usd_rate'];          
        }
        $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['usd_rate'];

      }else if($code == "GBP"){

        $data['currency'] = $code;
        $data['symbol'] = $symbol;
        // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['gbp_rate'];
        if($discountStatus == 1){
          $data['price'] = $discountDetails['SpacialDiscount']['gbp_price'];
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['gbp_rate'];          
        }
        $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['gbp_rate'];

      }else if($code == "EUR"){

        $data['currency'] = $code;
        $data['symbol'] = $symbol;
        // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['euro_rate'];
        if($discountStatus == 1){
          $data['price'] = $discountDetails['SpacialDiscount']['eur_price'];
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['euro_rate'];          
        }
        $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['euro_rate'];

      }else if($code == "AUD"){

        $data['currency'] = $code;
        $data['symbol'] = $symbol;
        // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['aud_rate'];
        if($discountStatus == 1){
          $data['price'] = $discountDetails['SpacialDiscount']['aud_price'];
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['aud_rate'];          
        }
        $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['aud_rate'];

      }else if($code == "CAD"){
        
        $data['currency'] = $code;
        $data['symbol'] = $symbol;
        // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['cad_rate'];
        if($discountStatus == 1){
          $data['price'] = $discountDetails['SpacialDiscount']['cad_price'];
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['cad_rate'];          
        }
        $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['cad_rate'];

      }else if($code == "INR"){
        
        $data['currency'] = $code;
        $data['symbol'] = $symbol;
        // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['inr_rate'];
        if($discountStatus == 1){
          $data['price'] = $discountDetails['SpacialDiscount']['inr_price'];
        }else{
          $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['inr_rate'];          
        }
        $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['inr_rate'];

      }
    
    }else{

        $this->loadModel('ExchangeRate');
        $exRate = $this->ExchangeRate->find('first',array('conditions'=>array('ExchangeRate.base_currency'=>$baseCurrency,'ExchangeRate.price'=>$price)));
        
        if($baseCurrency == "USD"){

          $data['currency'] = 'USD';
          $data['symbol'] = '$';
          // $data['price'] = $exRate['ExchangeRate']['base_local_net_price'];
          if($discountStatus == 1){
            $data['price'] = $discountDetails['SpacialDiscount']['usd_price'];
          }else{
            $data['price'] = $exRate['ExchangeRate']['base_local_net_price'];          
          }
          $data['old_price'] = $exRate['ExchangeRate']['price'];

        }else{
          $data['currency'] = 'USD';
          $data['symbol'] = '$';
          // $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['usd_rate'];
          if($discountStatus == 1){
            $data['price'] = $discountDetails['SpacialDiscount']['usd_price'];
          }else{
            $data['price'] = $exRate['ExchangeRate']['base_local_net_price']*$exRate['ExchangeRate']['usd_rate'];          
          }  
          $data['old_price'] = $exRate['ExchangeRate']['price']*$exRate['ExchangeRate']['usd_rate'];  
        }
    
    }

    return $data;
    exit;
  }



}