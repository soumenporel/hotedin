<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class IndustriesController extends AppController {

	  public function admin_index() {


        $title_for_layout = 'Industry';
        $this->set(compact('title_for_layout'));
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $this->loadModel('Industry');
       
        $this->set('industries', $this->Industry->find('all'));
    }
    
     public function admin_add() {
      
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $title_for_layout = 'Industry Add';
        $this->set(compact('title_for_layout'));
        if ($this->request->is('post')) {
           
                $this->Industry->create();
                #pr($this->data);
                #exit;
                if ($this->Industry->save($this->request->data)) {
                   
                    $this->Session->setFlash(__('The industry has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'admin_index'));
                } else {
                    $this->Session->setFlash(__('The industry could not be saved. Please, try again.', 'default', array('class' => 'error')));
                }
            } 
        }
   

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->loadModel('IndustryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $title_for_layout = 'Industry Edit';
        $this->set(compact('title_for_layout'));
        if (!$this->Industry->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
           
            if ($this->Industry->save($this->request->data)) {
                $this->Session->setFlash(__('The industry has been saved.'));
                return $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(__('The industry could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Industry.' . $this->Industry->primaryKey => $id));
            $this->request->data = $this->Industry->find('first', $options);
           // print_r($this->request->data);die;
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
       
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Industry->id = $id;
        if (!$this->Industry->exists()) {
            throw new NotFoundException(__('Invalid industry'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Industry->delete()) {
            //$this->IndustryImage->delete()
            $this->Session->setFlash(__('The industry has been deleted.'));
        } else {
            $this->Session->setFlash(__('The industry could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }

}