<?php

App::uses('AppController', 'Controller');

/**
 * Faqs Controller
 *
 * @property Faq $Faq
 * @property PaginatorComponent $Paginator
 */
class PagebannersController extends AppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Banner.id' => 'desc'
        )
    );
    
    public function admin_index() {
        $this->loadModel('Pagebanner');
        $this->loadModel('WpPage');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->Pagebanner->recursive = 2;
        // $a = $this->Pagebanner->find('all');
        $this->Paginator->settings = $this->paginate;
        //$a=$this->Paginator->paginate();
        //print_r($a);die;
        $this->set('banners', $this->Paginator->paginate());
    }
    
    public function admin_add() {
        $this->loadModel('WpPage');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $pages = $this->WpPage->find('list', array('fields' => array('WpPage.id', 'WpPage.title')));
         $this->set(compact('pages'));
         //print_r($categories);die;
        if ($this->request->is('post')) {
            $this->request->data['Pagebanner']['status']=0; 
            if (isset($this->request->data['Pagebanner']['image']) && $this->request->data['Pagebanner']['image']['name'] != '') {
                $path = $this->request->data['Pagebanner']['image']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if ($ext) {
                    $uploadPath = Configure::read('UPLOAD_BANNER_PATH');
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array($ext, $extensionValid)) {
                        $imageName = rand() . '_' . (strtolower(trim($this->request->data['Pagebanner']['image']['name'])));
                        $full_image_path = $uploadPath . '/' . $imageName;
                        move_uploaded_file($this->request->data['Pagebanner']['image']['tmp_name'], $full_image_path);
                        $this->request->data['Pagebanner']['image'] = $imageName;
                    } else {
                        $this->Session->setFlash(__('Invalid Image Type.'));
                        return $this->redirect(array('action' => 'edit', $id));
                    }
                }
            } else {
                $this->request->data['Pagebanner']['image'] = $this->request->data['Pagebanner']['image'];
            }
            $this->Pagebanner->create();
            if ($this->Pagebanner->save($this->request->data)) {
                $this->Session->setFlash(__('The Banner has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Banner could not be saved. Please, try again.'));
            }
        }
        
       
    }
    
    public function admin_edit($id = null) {
        $this->loadModel('WpPage');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        //$this->loadModel('FaqCategory');
        if (!$this->Pagebanner->exists($id)) {
            throw new NotFoundException(__('Invalid Faq'));
        }
         $categories = $this->WpPage->find('list', array('fields' => array('WpPage.id', 'WpPage.title')));
         //print_r($categories);die;
        if ($this->request->is(array('post', 'put'))) {
            if (isset($this->request->data['Pagebanner']['image']) && $this->request->data['Pagebanner']['image']['name'] != '') {
                $path = $this->request->data['Pagebanner']['image']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if ($ext) {
                    $uploadPath = Configure::read('UPLOAD_BANNER_PATH');
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array($ext, $extensionValid)) {
                        $OldImg = $this->request->data['Pagebanner']['saved_image'];
                        $imageName = rand() . '_' . (strtolower(trim($this->request->data['Pagebanner']['image']['name'])));
                        $full_image_path = $uploadPath . '/' . $imageName;
                        move_uploaded_file($this->request->data['Pagebanner']['image']['tmp_name'], $full_image_path);
                        $this->request->data['Pagebanner']['image'] = $imageName;
                        if ($OldImg != '') {
                            unlink($uploadPath . '/' . $OldImg);
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid Image Type.'));
                        return $this->redirect(array('action' => 'edit', $id));
                    }
                }
            } else {
                unset($this->request->data['Pagebanner']['image']);
            }
            
            if ($this->Pagebanner->save($this->request->data)) {
                $this->Session->setFlash(__('The Banner has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Banner could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Pagebanner.' . $this->Pagebanner->primaryKey => $id));
            $this->request->data = $this->Pagebanner->find('first', $options);
        }
        $this->set(compact('categories'));
        
    }
    
    public function admin_delete($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Pagebanner->id = $id;
        if (!$this->Pagebanner->exists()) {
            throw new NotFoundException(__('Invalid Faq'));
        }

        $this->request->onlyAllow('post', 'delete');
        $banner_row = $this->Pagebanner->find('first', array('conditions' => array('Banner.id' => $id)));
        $uploadPath = Configure::read('UPLOAD_BANNER_PATH');
        $OldImg = $banner_row['Pagebanner']['image'];
        unlink($uploadPath . '/' . $OldImg);
        if ($this->Pagebanner->delete()) {
            
            $this->Session->setFlash(__('The Banner has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Banner could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function ajaxBannerSelect() {
        
        $data = array();

        // echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;

        if($this->Pagebanner->updateAll(array('status'=>$this->request->data['status']), array('Banner.id'=>$this->request->data['banner_id']))){
            // $this->Pagebanner->updateAll(array('status'=>0), array('Banner.id !='=>$this->request->data['banner_id']));
            $data['Ack'] = 1;
            $data['res'] = 'Banner Image Status Successfully Set';
        }else{
            $data['Ack'] = 0;
        }

        echo json_encode($data);
        exit;    
    }

}