<?php
App::uses('AppController', 'Controller');
/**
 * Analytics Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class UserBookmarksController extends AppController 
{

public $components = array('Paginator','Session');

  public function ajaxSaveBookmark(){
    
    $data = array();
     $bookmarks = $this->UserBookmark->find('all',array('conditions'=>array('UserBookmark.user_id'=>$this->request->data['user_id'],'UserBookmark.post_id'=>$this->request->data['post_id'],'UserBookmark.lecture_id'=>$this->request->data['lecture_id'])));
    if (count($bookmarks) == 0) {
     $this->UserBookmark->create();
    if($this->UserBookmark->save($this->request->data)){
      $data['Ack'] = 1;
    }else{
      $data['Ack'] = 0;
    }
    }
 else {
    $data['Ack'] = 2;    
    }
    echo json_encode($data);
    exit;
  }


}