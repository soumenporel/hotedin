<?php

App::uses('AppController', 'Controller');

/**
 * Faqs Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class LecturesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array('Lecture','Faqcategory','Lesson');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Lecture.id' => 'desc'
        )
    );

    public function admin_add($id = NULL) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $post_id = base64_decode($id);
        $ids = explode(',', $post_id);
        $post_id = $ids['0'];
        $lesson_id = $ids['1'];
        if ($this->request->is('post')) {
            $this->request->data['Lecture']['post_id'] = $post_id;
            $this->request->data['Lecture']['lesson_id'] = $lesson_id;

            //pr($this->request->data); exit;

                if (!empty($this->request->data['Lecture']['media']['name'])) {
                    $pathpart = pathinfo($this->request->data['Lecture']['media']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg','jpeg','png','mp4','mkv','flv','pdf','doc','docx','ppt','mp3');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "lecture_media";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Lecture']['media']['tmp_name'], $full_flg_path);
                        $this->request->data['Lecture']['media'] = $filename;
                        
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'add'));
                    }
                } else {
                    $filename = '';
                    $this->request->data['Lecture']['media'] = $filename;
                }

            $this->Lecture->create();
            if ($this->Lecture->save($this->request->data)) {
                $this->Session->setFlash(__('The Lecture has been saved.'));
                return $this->redirect(array('controller'=>'lessons','action' => 'view',$lesson_id));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Lecture could not be saved. Please, try again.'));
            }
        }
         
        $this->set(compact('lesson_id'));
    }

    public function admin_edit($id = NULL) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        
        if ($this->request->is('post')) {
            

            if (!empty($this->request->data['Lecture']['media']['name'])) {
                $pathpart = pathinfo($this->request->data['Lecture']['media']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('jpg','jpeg','png','mp4','mkv','flv','pdf','doc','docx','ppt');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "lecture_media";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Lecture']['media']['tmp_name'], $full_flg_path);
                    $this->request->data['Lecture']['media'] = $filename;
                    
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                    return $this->redirect(array('action' => 'add'));
                }
            } else {
                $filename = '';
                $this->request->data['Lecture']['media'] = $filename;
            }

            $this->Lecture->create();
            if ($this->Lecture->save($this->request->data)) {
                $this->Session->setFlash(__('The Lecture has been saved.'));
                return $this->redirect(array('controller'=>'lessons','action' => 'view',$lesson_id));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Lecture could not be saved. Please, try again.'));
            }
        
        } else{
            $this->request->data = $this->Lecture->find('first',array('conditions'=>array('Lecture.id'=>$id)));
        }
         
        $this->set(compact('lesson_id'));
    }

    public function admin_view($id = NULL){

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        //$title_for_layout = 'Post View';
        if (!$this->Lecture->exists($id)) {
            throw new NotFoundException(__('Invalid Lecture'));
        }
        $options = array('conditions' => array('Lecture.' . $this->Lecture->primaryKey => $id));
        $this->Lecture->recursive = 2;
        $lecture = $this->Lecture->find('first', $options);
        $fileName = '';
        $linkName = '';
        $codeName = ''; 
        
        if(!empty($lecture['DownloadableFile'])){ $fileName = array();
            foreach ($lecture['DownloadableFile'] as $key => $file){
                $fileName = $file['downloadable_file'];
            }
        }
        if(!empty($lecture['ExternalLink'])){ $linkName = array();
            foreach ($lecture['ExternalLink'] as $key => $link){
                $linkName = $link['link'];
            }
        }
        if(!empty($lecture['SourseCode'])){ $codeName = array(); 
            foreach ($lecture['SourseCode'] as $key => $code){
                $codeName[] = $code['file'];
            }
        }
        
        $post_id = $lecture['Lecture']['post_id'];

        $this->loadModel('LectureAsset');
        $this->loadModel('LectureLink');
        $this->loadModel('LectureCode');

        $asset  = $this->LectureAsset->find('all',array('conditions'=>array('LectureAsset.lecture_id !='=>$id,'LectureAsset.post_id'=>$post_id,'LectureAsset.downloadable_file !='=>$fileName)));
        $link   = $this->LectureLink->find('all',array('conditions'=>array('LectureLink.lecture_id !='=>$id,'LectureLink.post_id'=>$post_id,'LectureLink.link !='=>$linkName)));
        $code   = $this->LectureCode->find('all',array('conditions'=>array('LectureCode.lecture_id !='=>$id,'LectureCode.post_id'=>$post_id,'LectureCode.file !='=>$codeName)));

        $allValue = array_merge($asset, $link, $code);

        $this->set(compact('lecture','asset','link','code'));
    }

    public function admin_delete($id = NULL){

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Lecture->id = $id;
        if (!$this->Lecture->exists()) {
            throw new NotFoundException(__('Invalid Lecture'));
        }

        $this->loadModel('Lecture');
        $this->Lecture->recursive = 0;
        $lecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id'=>$id)));
        $info = array('post_id'=>$lecture['Lecture']['post_id'],'lesson_id'=>$lecture['Lecture']['lesson_id']);
        $ids = implode(',', $info);

        if ($this->Lecture->delete($id)) {
            $this->Session->setFlash(__('The Lecture has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Lecture could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('controller'=>'lessons', 'action' => 'view', $lecture['Lecture']['lesson_id']));
    }

    public function ajaxAddLecture($post_id = NULL){


        $data = array();
        $html = '';
        $lessons = $this->Lesson->find('all',array(
            'conditions' => array(
                'Lesson.post_id' => $post_id
            ),
            //'order'=>'Lesson.sortby ASC'
        ));
        if(!empty($lessons)){
            $lessons = end($lessons);
            
           // foreach ($lessons as $key => $value) {
           //     $no_of_lectures = $this->Lecture->find('count',array('conditions'=>array('Lecture.lesson_id'=>$value['Lesson']['id'])));
           //     if($no_of_lectures==0){
           //          $Lecture['Lecture']['post_id']      = $post_id;
           //          $Lecture['Lecture']['lesson_id']    = $value['Lesson']['id'];
           //          $Lecture['Lecture']['title']        = $this->request->data['lecture']['title'];
           //          $Lecture['Lecture']['type']         = 0;

           //          $this->Lecture->create();
           //          $this->Lecture->save($Lecture);

           //          $html = $this->curriculum_html($post_id);

           //          $data['Ack'] = 1;
           //          $data['html'] = $html;

           //          break;
           //     }
           // }
           // if($html==''){
                $Lecture['Lecture']['post_id']      = $post_id;
                $Lecture['Lecture']['lesson_id']    = $lessons['Lesson']['id'];
                $Lecture['Lecture']['title']        = $this->request->data['lecture']['title'];
                $Lecture['Lecture']['type']         = 0;

                $this->Lecture->create();
                $this->Lecture->save($Lecture);

                $html = $this->curriculum_html($post_id);

                $data['Ack'] = 1;
                $data['html'] = $html;        
           // }
        }
        else{
            $Lesson['Lesson']['post_id'] = $post_id;
            $Lesson['Lesson']['title'] = 'Unpublished Section';
            $this->Lesson->create();
            $this->Lesson->save($Lesson);
            $lessonID = $this->Lesson->id;

            $Lecture['Lecture']['post_id']      = $post_id;
            $Lecture['Lecture']['lesson_id']    = $lessonID;
            $Lecture['Lecture']['title']        = $this->request->data['lecture']['title'];
            $Lecture['Lecture']['type']         = 0;

            $this->Lecture->create();
            $this->Lecture->save($Lecture);

            //$html = $this->curriculum_html($post_id);

            $data['Ack'] = 1;
            $data['html'] = $html;

        }
        echo json_encode($data);
        exit;
    }
    public function ajaxAddQuiz($post_id = NULL){
        
        $data = array();
        $html = '';
        $lessons = $this->Lesson->find('all', array(
            'conditions' => array(
                'Lesson.post_id' => $post_id
            ),
            //'order'=>'Lesson.sortby ASC'
        ));
        if(!empty($lessons)){
            $lessons = end($lessons);
           // foreach ($lessons as $key => $value) {
           //     $no_of_lectures = $this->Lecture->find('count',array('conditions'=>array('Lecture.lesson_id'=>$value['Lesson']['id'])));
           //     if($no_of_lectures==0){
           //          $Lecture['Lecture']['post_id']      = $post_id;
           //          $Lecture['Lecture']['lesson_id']    = $value['Lesson']['id'];
           //          $Lecture['Lecture']['title']        = $this->request->data['quiz']['name'];
           //          $Lecture['Lecture']['description']  = $this->request->data['quiz']['objective'];
           //          $Lecture['Lecture']['type']         = 1;

           //          $this->Lecture->create();
           //          $this->Lecture->save($Lecture);

           //          $html = $this->curriculum_html($post_id);

           //          $data['Ack'] = 1;
           //          $data['html'] = $html;

           //          break;
           //     }
           // }
           // if($html==''){
                $Lecture['Lecture']['post_id']      = $post_id;
                $Lecture['Lecture']['lesson_id']    = $lessons['Lesson']['id'];
                $Lecture['Lecture']['title']        = $this->request->data['quiz']['name'];
                $Lecture['Lecture']['description']  = $this->request->data['quiz']['objective'];
                $Lecture['Lecture']['type']         = 1;

                $this->Lecture->create();
                $this->Lecture->save($Lecture);

                $html = $this->curriculum_html($post_id);

                $data['Ack'] = 1;
                $data['html'] = $html;        
           // }
        }
        else{
            $Lesson['Lesson']['post_id'] = $post_id;
            $Lesson['Lesson']['title'] = 'Unpublished Section';
            $this->Lesson->create();
            $this->Lesson->save($Lesson);
            $lessonID = $this->Lesson->id;

            $Lecture['Lecture']['post_id']      = $post_id;
            $Lecture['Lecture']['lesson_id']    = $lessonID;
            $Lecture['Lecture']['title']        = $this->request->data['quiz']['name'];
            $Lecture['Lecture']['description']  = $this->request->data['quiz']['objective'];
            $Lecture['Lecture']['type']         = 1;

            $this->Lecture->create();
            $this->Lecture->save($Lecture);

            $html = $this->curriculum_html($post_id);

            $data['Ack'] = 1;
            $data['html'] = $html;

        }
        echo json_encode($data);
        exit;
    }

    public function curriculum_html($post_id){
        $html = '';
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$post_id),'order'=>'Lesson.sortby Asc'));
        $serial_no = 1;
        foreach ($lessons as $key => $lesson) { 

            $html.='<li id="'.$lesson['Lesson']['id'].'" data-id="'.$lesson['Lesson']['id'].'" class="ui-state-default"><div id="id" class="ui-sortable-1">
                    <div class="first_section"><span class="section">Section '.$serial_no.': </span><span class="sectiontext" id="lesson_title-'.$lesson['Lesson']['id'].'" ><i class="fa fa-file-text-o" aria-hidden="true"></i>'.$lesson['Lesson']['title'].'<i class="fa fa-pencil edit-lesson" style="cursor: pointer;" > </i><i class="fa fa-trash" style="cursor: pointer;" ></i></span> 
                    <span class="action_buttons pull-right"><i class="fa fa-bars" style="cursor: move;" ></i></span></div><ul id="sortable1" class="connectedSortable">';
                        
                        $lecture_no = 1;
                        foreach ($lesson['Lecture'] as $key => $value) {
                            if($value['type']==0){
                                $html.='<li id="'.$value['id'].'" data-id="'.$lesson['Lesson']['id'].'-'.$value['id'].'" class="ui-state-default"> 
                                <div class="fileupload"><div class="row"><div class="col-md-6"><div class="lecturer"><i class="fa fa-check-circle" aria-hidden="true"></i> Lecture '.$lecture_no.' :</label><span class="introduction" id="lecture_title-'.$value['id'].'" ><i class="fa fa-file-text-o" aria-hidden="true"></i> '.$value['title'].' <i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash delete-lecture" style="cursor: pointer;"></i> </span>
                                </div></div><div class="col-md-6"><div class="upload_part pull-right"><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i>
                                Add Content</button><i class="fa fa-chevron-down pull-right"></i></div></div></div></div></li>';
                            }
                            if($value['type']==1){
                               $html.='<li id="'.$value['id'].'" data-id="'.$lesson['Lesson']['id'].'-'.$value['id'].'" class="ui-state-default"><div class="fileupload">
                                        <div class="row"><div class="col-md-6"><div class="lecturer"><i class="fa fa-check-circle" aria-hidden="true"></i> Quiz '.$lecture_no.' :</label><span class="introduction" id="lecture_title-'.$value['id'].'" ><i class="fa fa-file-text-o" aria-hidden="true"></i> '.$value['title'].'<i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash delete-lecture" style="cursor: pointer;"></i></span>
                                        </div></div>';
                                if(empty($value['Question'])){        
                                    $html.='<div class="col-md-6"><div class="upload_part pull-right"><button type="button" data-toggle="modal" data-target="#questions" class="btn btn-primary add_quiz_question"><i class="fa fa-plus"></i>Add Questions</button></div></div></div></div></li>';  
                                }
                                else{
                                    $html.='<i class="fa fa-chevron-down pull-right"></i>';   
                                }
                            }
                        $lecture_no = $lecture_no+1;
                        } 
                    $html.='</ul></div></li>';
            
            $serial_no = $serial_no+1; 
        }
        return $html;
    }

    public function ajaxAddLectureContent($lecture_id = NULL){
        $this->loadModel('Setting');
         $this->loadModel('Post');
          $this->loadModel('Lecture');
       $userid = $this->Session->read('userid');
       $data=array();
        $data['Ack'] = 0;
        $videoSetting = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.video_no', 'Setting.video_size')));     
      
       $video_no=$videoSetting['Setting']['video_no'];
       $video_size=$videoSetting['Setting']['video_size']*100000;
      
        if(!empty($this->request->data)){
            //pr($this->request->data); exit;
            if (!empty($this->request->data['Lecture']['media']['name'])) {
                $pathpart = pathinfo($this->request->data['Lecture']['media']['name']);
                $ext = $pathpart['extension'];
                //for checking video file
                 $extensionVideo= array('mp4','mkv','flv','wav','wma');
             
                $extensionValid = array('jpg','jpeg','png','gif','mp4','mkv','flv','mp3','pdf','doc','docx' ,'ppt','wav','wma');
                if (in_array(strtolower($ext), $extensionValid)) {
                    
                       if (in_array(strtolower($ext), $extensionVideo)) {
                 
                        $postIds=$this->Post->find('list',array('conditions'=>array('Post.user_id'=>$userid),'fields' => array('Post.id')));
                        //print_r($postIds);die;
                        $lectureCount=$this->Lecture->find('count',array('conditions'=>array('Lecture.post_id'=>$postIds)));
                       if($lectureCount>=$video_no)
                       {
                           $data['Ack'] = 0;
                           $data['res'] = 'you have exceed max video upload limit.No of upload should be less than '.$video_no;
                           echo json_encode($data);
                           exit;
                       }
                       elseif($this->request->data['Lecture']['media']['size']>$video_size)
                       {
                           $data['Ack'] = 0;
                           $data['res'] = $this->request->data['Lecture']['media']['size'].'you have exceed video size limit.video size should be less than '.$video_size.'MB';
                           echo json_encode($data);
                           exit;  
                       }
                       else
                       {
                            $uploadFolder = "lecture_media";
                            $uploadPath = WWW_ROOT . $uploadFolder;
                            $filename = uniqid() . '.' . $ext;
                            $full_flg_path = $uploadPath . '/' . $filename;
                            move_uploaded_file($this->request->data['Lecture']['media']['tmp_name'], $full_flg_path);
                            $this->request->data['Lecture']['media'] = $filename;
                            
                       }
                      }
                      else
                      {
                    $uploadFolder = "lecture_media";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Lecture']['media']['tmp_name'], $full_flg_path);
                    $this->request->data['Lecture']['media'] = $filename;
                   
                      }
                    
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = ' Invalid Media Type';
                    echo json_encode($data);
                           exit;  
                }
            } else {
                $filename = '';
                $this->request->data['Lecture']['media'] = $filename;
            }
            $this->request->data['Lecture']['id'] = $lecture_id;
            $this->request->data['Lecture']['status'] = $this->request->data['status'];
            if($this->Lecture->save($this->request->data)){
                $data['Ack'] = 1;
                $data['res'] = 'Lecture Media Successfully Saved';
                echo json_encode($data);
                           exit;  
            }
        }
        echo json_encode($data);
        exit;
    }

    public function ajaxLectureDetails(){
        $lecture_id = $this->request->data['lecture_id'];
        $lectureDetails = $this->Lecture->find('first',array('conditions'=>array('Lecture.id'=>$lecture_id),'fields'=>array('Lecture.id','Lecture.title','Lecture.description')));
        $data['id'] = $lectureDetails['Lecture']['id'];
        $data['title'] = $lectureDetails['Lecture']['title'];
        $data['description'] = $lectureDetails['Lecture']['description']; 
        echo json_encode($data);
        exit;
    }

    public function ajaxEditLecture($lecture_id = NULL){
        $data = array();
        $this->request->data['Lecture']['id'] = $lecture_id;
        if($this->Lecture->save($this->request->data)){
            $data['Ack'] = 1;
            $data['res'] = ' Section Saved Successfully ';
            $data['title'] = $this->request->data['Lecture']['title'];
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = 'Error!!!';
        }
        echo json_encode($data); 
        exit;
    }

    public function ajaxDeleteLecture(){

        $data = array();
        $html = '';
        $lecture_id = $this->request->data['lecture_id'];
        if ($this->Lecture->delete($lecture_id)) {
            $data['Ack'] = 1;
            $data['res'] = ' Lecture is Successfully deleted';
            $html = $this->curriculum_html($this->request->data['post_id']);
            $data['html'] = $html;
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = ' Error!! ';
        }
        echo json_encode($data); 
        exit;
    }

    public function ajaxDeleteLectureNew(){
        $this->autoRender = false;
        $data = array();
        $html = '';
        $lecture_id = $this->request->data['lecture_id'];
        if ($this->Lecture->delete($lecture_id)) {
            $this->loadModel('Question');
            $this->Question->deleteAll(array('Question.quiz_id' => $lecture_id), false);
            $data['ack'] = 1;
            $data['res'] = ' Lecture is Successfully deleted';
        }
        else{
            $data['ack'] = 0;
            $data['res'] = ' Error!! ';
        }
        echo json_encode($data);
    }

    public function ajaxAddQuizQuestion($post_id = NULL){
        $this->autoRender = false;
        $data = array();

       $obj = json_decode($this->request->data['Question']['options']);
        if($this->request->data['Qtype']=='matching'||$this->request->data['Qtype']=='ordering')
        {
            $score=count($obj);
        }
        else
        {
            $score=1; 
        }    
       

        if(!empty($this->request->data)){
            $question['Question']['id']         = $this->request->data['Question']['id'];
            $question['Question']['quiz_id']    = $this->request->data['lecture_id'];
            $question['Question']['question']   = $this->request->data['Question']['question'];
            $question['Question']['options']    = $this->request->data['Question']['options'];
            $question['Question']['answer']     = $this->request->data['quiz_ans'];
            $question['Question']['Qtype']     = $this->request->data['Qtype'];
            $question['Question']['marks']     = $score;
            $this->loadModel('Question');
            if($this->Question->save($question)){
                $data['Ack'] = 1;
                $data['res'] = 'Quiz question Successfully Saved';
                //$html = $this->curriculum_html($post_id);
                //$data['html'] = $html;
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Error!!';
            }
        }
        echo json_encode($data);
        
    }

    public function ajaxDeleteQuizQuestion() {
        $this->autoRender = false;
        $data = array();
        // pr($this->request->data);
        $this->loadModel('Question');
        $id = $this->request->data['Question']['id'];
        if ($this->Question->delete($id)) {
            $data['Ack'] = 1;
        } else {
            $data['Ack'] = 0;
        }
        echo json_encode($data);
    }

    public function ajaxLectureVideo(){
        $data = array();
        $html = '';
        $lecture_id = $this->request->data['lecture_id'];
        $lectureDetails = $this->Lecture->find('first',array('conditions'=>array('Lecture.id'=>$lecture_id)));
          if(!empty($lectureDetails)){
            $file = $lectureDetails['Lecture']['media'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = trim($lectureDetails['Lecture']['media'],$ext);
            
            $html.='<video id="yourVideoplayer" width="568" controls><source src="'.$this->webroot.'lecture_media/'.$lectureDetails['Lecture']['media'].'" type="video/'.$ext.'"  controlsList="nodownload">
                      <source src="'.$this->webroot.'lecture_media/'.$file_name.'.ogg" type="video/ogg"  controlsList="nodownload">Your browser does not support HTML5 video1.</video>';
                $data['Ack'] = 1;
                $data['html'] = $html; 
          }
          else{
            $data['Ack'] = 0;
          }
        echo json_encode($data);    
        exit;
    }

    public function ajaxLectureVideostatus(){
        
        $this->loadModel('VideoStatus');
       
        $lecture_id = $this->request->data('lecture_id');
        $user_id = $this->request->data('userId');

        $videoDetails = $this->VideoStatus->find('first',array('conditions'=>array('VideoStatus.user_id'=>$user_id)));
          if(!empty($videoDetails)){
            
            $this->VideoStatus->updateAll(array('user_id' => $user_id, 'lecture_id' => $lecture_id));
            exit;
            
          }
          else{
            
            $data1['VideoStatus']['user_id'] = $user_id;
            $data1['VideoStatus']['lecture_id'] = $lecture_id;
            $this->VideoStatus->create();
            $this->VideoStatus->save($data1);
            exit;
          }
         
        exit;
    }

    public function ajaxAdminAddQuiz($post_id = NULL){
        
        $data = array();
        $html = '';
        
        if(!empty($this->request->data)){
           
                $Lecture['Lecture']['post_id']      = $post_id;
                $Lecture['Lecture']['lesson_id']    = $this->request->data['quiz']['lesson_id'];
                $Lecture['Lecture']['title']        = $this->request->data['quiz']['name'];
                $Lecture['Lecture']['description']  = $this->request->data['quiz']['objective'];
                $Lecture['Lecture']['type']         = 1;

                $this->Lecture->create();
                $this->Lecture->save($Lecture);

                $data['Ack'] = 1;
                $data['msg'] = 'Quiz Has Been Saved Successfully';
        }
        else{
           
            $data['Ack'] = 1;
            $data['html'] = 'Error...';

        }
        echo json_encode($data);
        exit;
    }

    public function ajaxAdminAddQuistion(){
        
        $data = array();
        if(!empty($this->request->data)){
            $question['Question']['quiz_id']    = $this->request->data['quiz_id'];
            $question['Question']['question']   = $this->request->data['qus'];
            $question['Question']['options']    = json_encode($this->request->data['ops']);
            $question['Question']['answer']     = $this->request->data['ans'];
            $this->loadModel('Question');
            if($this->Question->save($question)){
                $data['Ack'] = 1;
                $data['res'] = 'Quiz question Successfully Saved';
            }
            else{
                $data['Ack'] = 0;
                $data['res'] = 'Error!!';
            }
        }
        echo json_encode($data);
        exit;
    }

    public function ajaxDeleteResourse(){
        
        $data = array();
        
        if($this->request->data['resourse_type']==1){
            $this->loadModel('LectureAsset');
            $this->LectureAsset->delete($this->request->data['resourse_id']);
            $data['Ack'] = 1;
            $data['res'] = 'DownloadableFile Has Been Deleted Successfully..';
        }else if($this->request->data['resourse_type']==2){
            $this->loadModel('LectureLink');
            $this->LectureLink->delete($this->request->data['resourse_id']);
            $data['Ack'] = 1;
            $data['res'] = 'External Link Has Been Deleted Successfully..';
        }else if($this->request->data['resourse_type']==3){
            $this->loadModel('LectureCode');
            $this->LectureCode->delete($this->request->data['resourse_id']);
            $data['Ack'] = 1;
            $data['res'] = 'Sourse Code Has Been Deleted Successfully..';
        }else{
            $data['Ack'] = 0;
        }
        
        echo json_encode($data);
        exit;
    }

    public function ajaxEditLectureNew($lecture_id = NULL){
        $data = array();
        $this->request->data['Lecture']['id'] = $lecture_id;
        if($this->Lecture->save($this->request->data)){
            $data['Ack'] = 1;
            $data['res'] = ' Lecture Saved Successfully ';
            $data['description'] = $this->request->data['Lecture']['description'];
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = 'Error!!!';
        }
        echo json_encode($data); 
        exit;
    }
    

}
