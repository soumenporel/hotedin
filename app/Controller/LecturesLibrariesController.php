<?php

App::uses('AppController', 'Controller');

/**
 * LecturesLibrariesController Controller
 */
class LecturesLibrariesController extends AppController {

	public $components = array('Paginator', 'Session');

	public function ajax_add_downloadable_file() {
		$this->autoRender = false;
        $data = array();
        $userid = $this->Session->read('userid');
        
        $code = $_FILES;
        $lecture = $this->request->data['lecture_id'];
        $post_id = $this->request->data['post_id'];
        if(!empty($code)){

            if (!empty($code['code_file']['name'])) {
                $pathpart = pathinfo($code['code_file']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('docx','doc','pdf');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "lecture_asset";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($code['code_file']['tmp_name'], $full_flg_path);
                    
                    
                    $lecture_code['LecturesLibrary']['lecture_id'] 			= $lecture;
                    $lecture_code['LecturesLibrary']['post_id'] 			= $post_id;
                    $lecture_code['LecturesLibrary']['user_id'] 			= $userid;
                    $lecture_code['LecturesLibrary']['library_type'] 		= 1;
                    $lecture_code['LecturesLibrary']['date'] 				= gmdate('Y-m-d H:i:s');
                    $lecture_code['LecturesLibrary']['downloadable_file'] 	= $filename;

                    $this->LecturesLibrary->create();
                    if($this->LecturesLibrary->save($lecture_code)){
                        
                        $newAsset = $this->LecturesLibrary->find('first', array(
                        	'conditions' => array(
                        		'LecturesLibrary.id' => $this->LecturesLibrary->id
                    		)
                    	));
                        $data['asset'] = $newAsset['LecturesLibrary'];
                        $data['Ack'] = 1;
                        $data['res'] = 'Lecture Downloadable File Has Been Saved successfuly.';
                    }
                    
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Invalid image type.';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Error';
            }
        
        }
        echo json_encode($data);
	}

	public function ajax_add_sourcecode_file() {
		$this->autoRender = false;
        $data = array();
        $userid = $this->Session->read('userid');
        
        $code = $_FILES;
        $lecture = $this->request->data['lecture_id'];
        $post_id = $this->request->data['post_id'];
        if(!empty($code)){

            if (!empty($code['code_file']['name'])) {
                $pathpart = pathinfo($code['code_file']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('docx','doc','pdf');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "lecture_asset";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($code['code_file']['tmp_name'], $full_flg_path);
                    
                    
                    $lecture_code['LecturesLibrary']['lecture_id'] 			= $lecture;
                    $lecture_code['LecturesLibrary']['post_id'] 			= $post_id;
                    $lecture_code['LecturesLibrary']['user_id'] 			= $userid;
                    $lecture_code['LecturesLibrary']['library_type'] 		= 3;
                    $lecture_code['LecturesLibrary']['date'] 				= gmdate('Y-m-d H:i:s');
                    $lecture_code['LecturesLibrary']['sourcecode_file'] 	= $filename;

                    $this->LecturesLibrary->create();
                    if($this->LecturesLibrary->save($lecture_code)){
                        
                        $newAsset = $this->LecturesLibrary->find('first', array(
                        	'conditions' => array(
                        		'LecturesLibrary.id' => $this->LecturesLibrary->id
                    		)
                    	));
                        $data['asset'] = $newAsset['LecturesLibrary'];
                        $data['Ack'] = 1;
                        $data['res'] = 'Lecture sourcecode File Has Been Saved successfuly.';
                    }
                    
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Invalid image type.';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Error';
            }
        
        }
        echo json_encode($data);
	}

	public function ajax_add_external_resource() {
		$this->autoRender = false;
        $data = array();
        $userid = $this->Session->read('userid');
        //pr($this->request->data); exit();
        $lecture_code['LecturesLibrary']['lecture_id'] 		= $this->request->data['lecture_id'];
        $lecture_code['LecturesLibrary']['post_id'] 		= $this->request->data['post_id'];
        $lecture_code['LecturesLibrary']['user_id'] 		= $userid;
        $lecture_code['LecturesLibrary']['library_type'] 	= 2;
        $lecture_code['LecturesLibrary']['date'] 			= gmdate('Y-m-d H:i:s');
        $lecture_code['LecturesLibrary']['external_title']	= $this->request->data['title'];
        $lecture_code['LecturesLibrary']['external_link']	= $this->request->data['url'];

        $this->LecturesLibrary->create();
        if($this->LecturesLibrary->save($lecture_code)){
        	$newAsset = $this->LecturesLibrary->find('first', array('conditions' => array('id' => $this->LecturesLibrary->id)));
			$data['asset'] = $newAsset['LecturesLibrary'];
            $data['Ack'] = 1;
        } else {
            $data['Ack'] = 0;
        }
        echo json_encode($data);
	}

	public function get_lectures_libraries() {
		$this->autoRender = false;
		$data = array();
		$userid = $this->Session->read('userid');
		$lectures_libraries = $this->LecturesLibrary->find('all', array(
			'conditions' => array(
				'LecturesLibrary.user_id' => $userid,
				'LecturesLibrary.post_id' => '',
				'LecturesLibrary.lecture_id' => ''
			)
		));
		$library = array();
		if (!empty($lectures_libraries)) {
			foreach ($lectures_libraries as $value) {
				$library[] = $value['LecturesLibrary'];
			}
			$data['library'] = $library;
			$data['Ack'] = 1;
		} else {
			$data['Ack'] = 0;
		}
		echo json_encode($data);
	}

	public function update_lecture_library() {
		$this->autoRender = false;
		$data = array();
		
		$asset['LecturesLibrary']['id'] 		= $this->request->data['id'];
		$asset['LecturesLibrary']['post_id'] 	= $this->request->data['post_id'];
		$asset['LecturesLibrary']['lecture_id'] = $this->request->data['lecture_id'];

		if ($this->LecturesLibrary->save($asset)) {
			$newAsset = $this->LecturesLibrary->find('first', array('conditions' => array('id' => $this->request->data['id'])));
			$data['asset'] = $newAsset['LecturesLibrary'];
			$data['Ack'] = 1;
		} else {
			$data['Ack'] = 0;
		}
		echo json_encode($data);
	}

	public function delete_from_library() {
		$this->autoRender = false;
		$data = array();
		
		$asset_id	= $this->request->data['id'];
		if ($this->LecturesLibrary->delete($asset_id)) {
			$data['Ack'] = 1;
		} else {
			$data['Ack'] = 0;
		}
		echo json_encode($data);
	}

	public function delete_from_resource() {
		$this->autoRender = false;
		$data = array();
		
		$asset['LecturesLibrary']['id'] 		= $this->request->data['id'];
		$asset['LecturesLibrary']['post_id'] 	= '';
		$asset['LecturesLibrary']['lecture_id'] = '';

		if ($this->LecturesLibrary->save($asset)) {
			$data['Ack'] = 1;
		} else {
			$data['Ack'] = 0;
		}
		echo json_encode($data);
	}

}