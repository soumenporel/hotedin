<?php

App::uses('AppController', 'Controller');

/**
 * Faqs Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class LocationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array('User','Country','State','City');
    
    public function admin_add() {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if($this->request->is('post')){
            if($this->User->save($this->request->data)){
                $this->Session->setFlash(__('The User Location has been saved.'));
                $this->redirect('/admin');
            }else{
                $this->Session->setFlash(__('User Location Can Not Saved'));
                return $this->redirect(array('controller'=>'locations', 'action' => 'add'));
            }
        }
        $all_users = $this->User->find('all',array(
            'conditions'=>array('User.is_admin !='=>1),
            'fields'=>array('User.id','User.first_name','User.last_name')
            ));
        $countries = $this->Country->find('all');
        $this->set(compact('countries','all_users'));
    }

}
