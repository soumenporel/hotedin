<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class DashboardsController extends AppController {

  public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
  var $uses = array('Post','TempCart','User','Order','Category','Rating','RecentActivity');

  public function admin_index($filter = NULL) {

    //$this->layout = false;
    $total_instructors = $this->User->find('count',array('conditions'=>array('User.admin_type'=>1)));
    $total_students = $this->User->find('count',array('conditions'=>array('User.admin_type'=>2)));
    
    $total_users = $this->User->find('count',array('conditions'=>array('User.is_admin !='=>1)));
    $posts = $this->Post->find('all');

    if ($filter != NULL) {
            if ($filter == 'weekly') {
                $instructor = array(
                    'User.admin_type' => 1,
                    'User.member_since > DATE_SUB(NOW(), INTERVAL 1 WEEK)'
                );

                $learner = array(
                    'User.admin_type' => 2,
                    'User.member_since > DATE_SUB(NOW(), INTERVAL 1 WEEK)'
                );
                
                $order = array(
                    'Order.payment_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)'
                );
                
                $total_course_array = array(
                    'Post.post_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)'
                );
                
                $live_course_array = array(
                    'Post.post_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)',
                    'Post.is_approve' => 1
                );
            } else if($filter == 'monthly') {
                $instructor = array(
                    'User.admin_type' => 1,
                    'User.member_since > DATE_SUB(NOW(), INTERVAL 1 MONTH)'
                );

                $learner = array(
                    'User.admin_type' => 2,
                    'User.member_since > DATE_SUB(NOW(), INTERVAL 1 MONTH)'
                );
                
                $order = array(
                    'Order.payment_date > DATE_SUB(NOW(), INTERVAL 1 MONTH)'
                );
                
                $total_course_array = array(
                    'Post.post_date > DATE_SUB(NOW(), INTERVAL 1 MONTH)'
                );
                
                $live_course_array = array(
                    'Post.post_date > DATE_SUB(NOW(), INTERVAL 1 MONTH)',
                    'Post.is_approve' => 1
                );
            } else if($filter == 'yesterday') {
                $instructor = array(
                    'User.admin_type' => 1,
                    'User.member_since BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())'    

                );

                $learner = array(
                    'User.admin_type' => 2,
                    'User.member_since BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())'
                );
                
                $order = array(
                    'Order.payment_date BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())'
                );
                
                $total_course_array = array(
                    'Post.post_date BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())'
                );
                
                $live_course_array = array(
                    'Post.post_date BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())',
                    'Post.is_approve' => 1
                );
            } else if($filter == 'yearly') {
                $instructor = array(
                    'User.admin_type' => 1,
                    'User.member_since > DATE_SUB(NOW(), INTERVAL 1 YEAR)'
                );

                $learner = array(
                    'User.admin_type' => 2,
                    'User.member_since > DATE_SUB(NOW(), INTERVAL 1 YEAR)'
                );
                
                $order = array(
                    'Order.payment_date > DATE_SUB(NOW(), INTERVAL 1 YEAR)'
                );
                
                $total_course_array = array(
                    'Post.post_date > DATE_SUB(NOW(), INTERVAL 1 YEAR)'
                );
                
                $live_course_array = array(
                    'Post.post_date > DATE_SUB(NOW(), INTERVAL 1 YEAR)',
                    'Post.is_approve' => 1
                );
            } else if($filter == 'last_month') {
                $instructor = array(
                    'User.admin_type' => 1,
                    'User.member_since BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)'
                );

                $learner = array(
                    'User.admin_type' => 2,
                    'User.member_since BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)'
                );
                
                $order = array(
                    'Order.payment_date BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)'
                );
                
                $total_course_array = array(
                    'Post.post_date BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)'
                );
                
                $live_course_array = array(
                    'Post.post_date BETWEEN DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)',
                    'Post.is_approve' => 1
                );
            } else if($filter == 'this_month') {
                $instructor = array(
                    'User.admin_type' => 1,
                    'User.member_since BETWEEN DATE_FORMAT( NOW( ) , "%Y-%m-01" ) AND DATE(NOW())'
                );

                $learner = array(
                    'User.admin_type' => 2,
                    'User.member_since BETWEEN DATE_FORMAT( NOW( ) , "%Y-%m-01" ) AND DATE(NOW())'
                );
                
                $order = array(
                    'Order.payment_date BETWEEN DATE_FORMAT( NOW( ) , "%Y-%m-01" ) AND DATE(NOW())'
                );
                
                $total_course_array = array(
                    'Post.post_date BETWEEN DATE_FORMAT( NOW( ) , "%Y-%m-01" ) AND DATE(NOW())'
                );
                
                $live_course_array = array(
                    'Post.post_date BETWEEN DATE_FORMAT( NOW( ) , "%Y-%m-01" ) AND DATE(NOW())',
                    'Post.is_approve' => 1
                );
            }else if($filter == 'all_time') {
                $instructor = array(
                    'User.admin_type' => 1,
                );

                $learner = array(
                    'User.admin_type' => 2,
                );
                
                $order = array(
                );
                
                $total_course_array = array(
                );
                
                $live_course_array = array(
                    'Post.is_approve' => 1
                );
            } 
        } else {
            $instructor = array(
                'User.admin_type' => 1,
                'User.member_since >' => date('Y-m-d') . ' 00:00:00',
                'User.member_since <' => date('Y-m-d H:i:s')
            );
            
            $learner = array(
                'User.admin_type' => 2,
                'User.member_since >' => date('Y-m-d') . ' 00:00:00',
                'User.member_since <' => date('Y-m-d H:i:s')
            );
            
            $order = array(
                'Order.payment_date >' => date('Y-m-d') . ' 00:00:00',
                'Order.payment_date <' => date('Y-m-d H:i:s')
            );
            
            $total_course_array = array(
                'Post.post_date >' => date('Y-m-d') . ' 00:00:00',
                'Post.post_date <' => date('Y-m-d H:i:s')
            );
            
            $live_course_array = array(
                'Post.post_date >' => date('Y-m-d') . ' 00:00:00',
                'Post.post_date <' => date('Y-m-d H:i:s'),
                'Post.is_approve' => 1
            );
        }

            //Transform POST into GET
            if (($this->request->is('post') || $this->request->is('put')) && isset($this->data['Filter'])) {
                //pr($this->request->params); exit;
                $filter = $this->request->params['pass'][0];
                $filter_url['controller'] = $this->request->params['controller'];
                $filter_url['action'] = $this->request->params['action'];
                $filter_url['page'] = 1;

                foreach ($this->data['Filter'] as $name => $value) {
                    if ($value) {
                        $filter_url[$name] = urlencode($value);
                    }
                }
                return $this->redirect($filter_url);
            } else {
                foreach ($this->params['named'] as $param_name => $value) {
                    if (!in_array($param_name, array('page', 'sort', 'direction', 'limit'))) {
                        if ($param_name == "start_date" && $param_name == "end_date") {
                            //$conditions[] = "DATE(Order.payment_date) BETWEEN '".$this->params['start_date']."' AND '".$this->params['end_date']."'";
                        } else {
                            //$conditions['Order.' . $param_name] = $value;
                        }
                        $this->request->data['Filter'][$param_name] = $value;
                        
                    }
                }
                $start_date = isset($this->params['named']['start_date']) ? $this->params['named']['start_date'] : '';
                $end_date = isset($this->params['named']['end_date']) ? $this->params['named']['end_date'] : '';
                   
                   if($start_date != '' && $end_date != '') {

                        $filter = "In Date";
                         
                        $instructor = array(
                            'User.admin_type' => 1,
                            'User.member_since BETWEEN "'.$start_date.'" AND "'.$end_date.'"'
                        );

                        $learner = array(
                            'User.admin_type' => 2,
                            'User.member_since BETWEEN "'.$start_date.'" AND "'.$end_date.'"'
                        );
                        
                        $order = array(
                            'Order.payment_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"'
                        );
                        
                        $total_course_array = array(
                            'Post.post_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"'
                        );
                        
                        $live_course_array = array(
                            'Post.post_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"',
                            'Post.is_approve' => 1
                        );

                        //pr($order); exit;
                    }
            }
        
        $todays_instructor_signup = $this->User->find('count', array(
            'conditions' => $instructor
        ));
        $todays_user_signup = $this->User->find('count', array(
            'conditions' => $learner
        ));
        
//        $sales = $this->Order->find('all', array(
//            'conditions' => $order
//        ));
        

        $options = array('conditions' => $order, 'order' => array('Order.id' => 'desc'), 'group' => 'Order.id', 'limit' => 5);
        $this->Paginator->settings = $options;
        $this->set('sales', $this->Paginator->paginate('Order'));
        
        $latest_orders = $this->Order->find('all', array(
            'order' => array('Order.id DESC'),
            'limit' => 5
        ));
        
        $total_course = $this->Post->find('count', array(
            'conditions' => $total_course_array,
            'group' => array('Post.id')
        ));
        
        $live_course = $this->Post->find('count', array(
            'conditions' => $live_course_array,
            'group' => array('Post.id')
        ));
        
        
        $recentActivities = $this->RecentActivity->find('all', array(
            'order' => array('RecentActivity.id DESC'),
            'limit' => 5
        ));

        //-------- today Graph Data ----------//

        $orderGraph = $this->Order->query("SELECT h.theHour, COUNT( payment_date ) AS numberOfItems
                    FROM (

                    SELECT 0 AS theHour
                    UNION ALL SELECT 1 
                    UNION ALL SELECT 2 
                    UNION ALL SELECT 3 
                    UNION ALL SELECT 4 
                    UNION ALL SELECT 5 
                    UNION ALL SELECT 6 
                    UNION ALL SELECT 7 
                    UNION ALL SELECT 8 
                    UNION ALL SELECT 9 
                    UNION ALL SELECT 10 
                    UNION ALL SELECT 11 
                    UNION ALL SELECT 12 
                    UNION ALL SELECT 13 
                    UNION ALL SELECT 14 
                    UNION ALL SELECT 15 
                    UNION ALL SELECT 16 
                    UNION ALL SELECT 17 
                    UNION ALL SELECT 18 
                    UNION ALL SELECT 19 
                    UNION ALL SELECT 20 
                    UNION ALL SELECT 22 
                    UNION ALL SELECT 23
                    UNION ALL SELECT 24
                    ) AS h
                    LEFT OUTER 
                    JOIN orders ON EXTRACT( HOUR 
                    FROM orders.payment_date ) = h.theHour
                    AND DATE( orders.payment_date ) = CURDATE()
                    GROUP 
                    BY h.theHour");
                
                $sales = $orderGraph;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['numberOfItems']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Today",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
                $this->set(compact('data'));    
        //--------- End Graph Data -----------//            

        $this->loadModel('Post');
        $total_course       = $this->Post->find('count');
        $total_draft        = $this->Post->find('count',array('conditions'=>array('Post.is_approve'=>2)));
        $total_approved     = $this->Post->find('count',array('conditions'=>array('Post.is_approve'=>1)));
        $total_inreview     = $this->Post->find('count',array('conditions'=>array('Post.is_approve'=>0)));
        $total_disapproved  = $this->Post->find('count',array('conditions'=>array('Post.is_approve'=>3)));

        $draftPars = floor(($total_draft/$total_course)*100);
        $approvedPars = floor(($total_approved/$total_course)*100);
        $inreviewPars = floor(($total_inreview/$total_course)*100);
        $disapprovedPars = floor(($total_disapproved/$total_course)*100);

        //exit;
        $this->loadModel('Category');
        $total_categories = $this->Category->find('count',array('conditions'=>array('Category.parent_id'=>0)));
        $total_subCategories = $this->Category->find('count',array('conditions'=>array('Category.parent_id !='=>0)));

        $totalView = $this->Post->find('list',array('conditions'=>array('is_approve'=>1),'fields'=>array('id','click_count')));
        $vCount = 0;
        foreach ($totalView as $key => $view) {
            $vCount = $vCount + $view;
        }
        
        $this->set(compact('total_instructors', 'draftPars' , 'vCount','total_categories', 'total_subCategories','approvedPars' ,'inreviewPars' ,'disapprovedPars' ,'total_students', 'total_course', 'total_users', 'todays_instructor_signup', 'todays_user_signup', 'live_course', 'filter', 'latest_orders','recentActivities','start_date','end_date'));
  }

  public function ajaxGraph($filter = NULL) {

    //$this->layout = false;
    $total_instructors = $this->User->find('count',array('conditions'=>array('User.admin_type'=>1)));
    $total_students = $this->User->find('count',array('conditions'=>array('User.admin_type'=>2)));
    
    $total_users = $this->User->find('count',array('conditions'=>array('User.is_admin !='=>1)));
    $posts = $this->Post->find('all');

    if ($filter != NULL) {
            if ($filter == 'weekly') {
                $order = array(
                    'Order.payment_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)'
                );
                $order = $this->Order->query("SELECT DATE( payment_date ) AS DATE, COUNT( id ) AS count
                FROM orders
                WHERE payment_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)
                GROUP BY DATE
                ORDER BY DATE");

                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['count']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Last 7 Days",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
            } else if($filter == 'monthly') {
                
                $order = $this->Order->query("SELECT DATE( payment_date ) AS DATE, COUNT( id ) AS count
                FROM orders
                WHERE payment_date > DATE_SUB(NOW(), INTERVAL 1 MONTH)
                GROUP BY DATE
                ORDER BY DATE");

                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['count']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Last 30 Days",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
            } else if($filter == 'yesterday') {
                
                
                //$order = array('Order.payment_date BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())');
                $order = $this->Order->query("SELECT h.theHour, COUNT( payment_date ) AS numberOfItems
                    FROM (

                    SELECT 0 AS theHour
                    UNION ALL SELECT 1 
                    UNION ALL SELECT 2 
                    UNION ALL SELECT 3 
                    UNION ALL SELECT 4 
                    UNION ALL SELECT 5 
                    UNION ALL SELECT 6 
                    UNION ALL SELECT 7 
                    UNION ALL SELECT 8 
                    UNION ALL SELECT 9 
                    UNION ALL SELECT 10 
                    UNION ALL SELECT 11 
                    UNION ALL SELECT 12 
                    UNION ALL SELECT 13 
                    UNION ALL SELECT 14 
                    UNION ALL SELECT 15 
                    UNION ALL SELECT 16 
                    UNION ALL SELECT 17 
                    UNION ALL SELECT 18 
                    UNION ALL SELECT 19 
                    UNION ALL SELECT 20 
                    UNION ALL SELECT 22 
                    UNION ALL SELECT 23
                    UNION ALL SELECT 24
                    ) AS h
                    LEFT OUTER 
                    JOIN orders ON EXTRACT( HOUR 
                    FROM orders.payment_date ) = h.theHour
                    AND DATE( orders.payment_date ) =  subdate(current_date, 1)
                    GROUP 
                    BY h.theHour");
                
                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['numberOfItems']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Yesterday",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );

                
            } else if($filter == 'yearly') {
                                
                //$order = array('Order.payment_date > DATE_SUB(NOW(), INTERVAL 1 YEAR)');
                $order = $this->Order->query("SELECT COUNT( u.id ) AS total, m.month FROM (

                    SELECT  'Jan' AS 
                    MONTH 
                    UNION SELECT  'Feb' AS 
                    MONTH 
                    UNION SELECT  'Mar' AS 
                    MONTH 
                    UNION SELECT  'Apr' AS 
                    MONTH 
                    UNION SELECT  'May' AS 
                    MONTH 
                    UNION SELECT  'Jun' AS 
                    MONTH 
                    UNION SELECT  'Jul' AS 
                    MONTH 
                    UNION SELECT  'Aug' AS 
                    MONTH 
                    UNION SELECT  'Sep' AS 
                    MONTH 
                    UNION SELECT  'Oct' AS 
                    MONTH 
                    UNION SELECT  'Nov' AS 
                    MONTH 
                    UNION SELECT  'Dec' AS 
                    MONTH
                    ) AS m
                    LEFT JOIN orders u ON MONTH( STR_TO_DATE( CONCAT( m.month, YEAR( CURDATE( ) ) -1 ) ,  '%M %Y' ) ) = MONTH( u.payment_date ) 
                    AND YEAR( u.payment_date ) = YEAR( CURDATE( ) ) -1
                    GROUP BY m.month
                    ORDER BY 1 +1");
                    
                    $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['total']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Yearly",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
                 
            } else if($filter == 'last_month') {
                
                $order = $this->Order->query("SELECT DATE( payment_date ) AS DATE, COUNT( id ) AS count
                FROM orders
                WHERE payment_date
                BETWEEN  DATE_SUB(DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY), INTERVAL 1 MONTH) AND DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())) DAY)
                GROUP BY DATE
                ORDER BY DATE");

                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['count']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Last Month",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );

            } else if($filter == 'this_month') {
                $order = $this->Order->query("SELECT DATE( payment_date ) AS DATE, COUNT( id ) AS count
                FROM orders
                WHERE payment_date
                BETWEEN  DATE_FORMAT( NOW( ) , '%Y-%m-01' ) AND DATE(NOW())
                GROUP BY DATE
                ORDER BY DATE");

                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['count']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"This Month",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
            } else if($filter == 'all_time') {
                $instructor = array(
                    'User.admin_type' => 1,
                );

                $learner = array(
                    'User.admin_type' => 2,
                );
                
                $order = array(
                );
                
                $total_course_array = array(
                );
                
                $live_course_array = array(
                    'Post.is_approve' => 1
                );
            } 
        } else if( $filter == NULL && !isset($this->request->data['Filter'])){
            //$order = array('Order.payment_date BETWEEN DATE_FORMAT( DATE_SUB( NOW( ) , INTERVAL 1 DAY ), "%Y-%m-%d") AND DATE(NOW())');
                $order = $this->Order->query("SELECT h.theHour, COUNT( payment_date ) AS numberOfItems
                    FROM (

                    SELECT 0 AS theHour
                    UNION ALL SELECT 1 
                    UNION ALL SELECT 2 
                    UNION ALL SELECT 3 
                    UNION ALL SELECT 4 
                    UNION ALL SELECT 5 
                    UNION ALL SELECT 6 
                    UNION ALL SELECT 7 
                    UNION ALL SELECT 8 
                    UNION ALL SELECT 9 
                    UNION ALL SELECT 10 
                    UNION ALL SELECT 11 
                    UNION ALL SELECT 12 
                    UNION ALL SELECT 13 
                    UNION ALL SELECT 14 
                    UNION ALL SELECT 15 
                    UNION ALL SELECT 16 
                    UNION ALL SELECT 17 
                    UNION ALL SELECT 18 
                    UNION ALL SELECT 19 
                    UNION ALL SELECT 20 
                    UNION ALL SELECT 22 
                    UNION ALL SELECT 23
                    UNION ALL SELECT 24
                    ) AS h
                    LEFT OUTER 
                    JOIN orders ON EXTRACT( HOUR 
                    FROM orders.payment_date ) = h.theHour
                    AND DATE( orders.payment_date ) = CURDATE()
                    GROUP 
                    BY h.theHour");
                
                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['numberOfItems']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Today",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
        }

            //Transform POST into GET
            if (($this->request->is('post') || $this->request->is('put')) && isset($this->data['Filter'])) {
                
                $order = $this->Order->query("SELECT DATE( payment_date ) AS DATE, COUNT( id ) AS count
                FROM orders
                WHERE payment_date
                BETWEEN '".$this->request->data['Filter']['start_date']."' AND '".$this->request->data['Filter']['end_date']."' 
                GROUP BY DATE
                ORDER BY DATE");

                $sales = $order;
                    //pr($sales);
                    $d1 = array();
                    $count = 1;
                    foreach ($sales as $key => $sale) {
                        $d1[] = array($key+1,$sale[0]['count']);
                        $count++;
                    }
                    $data[] = array(
                        "label"=>"Between Two Dates",
                        "data"=>$d1,
                        "lines"=>array(
                            "show"=>true,
                            "fill"=>true,
                            "fillColor"=>array(
                                "colors"=>array("rgba(255,255,255,.4)", "rgba(183,236,240,.4)")
                                )
                            )
                        );
            }

        echo json_encode($data);
        exit;
    }
}




