<?php

App::uses('AppController', 'Controller');

class HowitworksController extends AppController {


    public function admin_index() {

		$userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $managehomes = $this->Howitwork->find('all');
        $this->set('howitworks',$managehomes);

	}

    public function admin_add() {

        if ($this->request->is('post')) {
            $this->Howitwork->create();
            if(isset($this->request->data['Howitwork']['image']) && $this->request->data['Howitwork']['image']['name']!=''){
                    $ext = explode('/',$this->request->data['Howitwork']['image']['type']);
                    if($ext){
                        $uploadFolder = "howitworks_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['Howitwork']['image']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['Howitwork']['image']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['Howitwork']['image']['tmp_name'],$full_image_path);
                            $this->request->data['Howitwork']['image'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['Howitwork']['image'] = '';
            }
            if ($this->Howitwork->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        }
    }

    public function admin_edit($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->Howitwork->exists($id)) {
            throw new NotFoundException(__('Invalid content'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Howitwork->id = $id;
            if(isset($this->request->data['Howitwork']['image']) && $this->request->data['Howitwork']['image']['name']!=''){
                    $ext = explode('/',$this->request->data['Howitwork']['image']['type']);
                    if($ext){
                        $uploadFolder = "howitworks_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['Howitwork']['image']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['Howitwork']['image']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['Howitwork']['image']['tmp_name'],$full_image_path);
                            $this->request->data['Howitwork']['image'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['Howitwork']['image'] = $this->request->data['Howitwork']['hide_image'];
            }
            if ($this->Howitwork->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.', array('class' => 'success')));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));

            }
        } else {
            $options = array('conditions' => array('Howitwork.' . $this->Howitwork->primaryKey => $id));
            $this->request->data = $this->Howitwork->find('first', $options);
        }
    }

    public function admin_delete($id = null) {
        $is_admin = $this->Session->read('is_admin');
        $this->loadModel('Howitwork');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $this->Howitwork->id = $id;
        if (!$this->Howitwork->exists()) {
                throw new NotFoundException(__('Invalid'));
        }
        $howitwork_img=$this->Howitwork->find('first',array('conditions'=>array('Howitwork.id'=>$id)));
        $howitwork_img_unlink = $howitwork_img['Howitwork']['image'];
        $uploadFolder = "howitworks_image";
        $uploadPath = WWW_ROOT . $uploadFolder;
        if($howitwork_img_unlink!='' && file_exists($uploadPath . '/' . $howitwork_img_unlink)){
            unlink($uploadPath . '/' . $howitwork_img_unlink);
        }
        if ($this->Howitwork->delete($id)) {
                $this->Session->setFlash(__('The Howitwork has been deleted.'));
        } else {
                $this->Session->setFlash(__('The Howitwork could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
?>
