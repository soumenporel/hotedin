<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class FavoritiesController extends AppController {
    /* function beforeFilter() {
      parent::beforeFilter();
      } */

    /**
     * Components
     *
     * @var array
     */
    //public $name = 'Users';
    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    var $uses = array('User', 'Wishlist','Favorite');

    

    public function favorite_this_course() {
        $this->autoRender = false;
        $data = array();
        
        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            $conditions = array(
                'Favorite.post_id' => $postid,
                'Favorite.user_id' => $userid
            );
            if (!$this->Favorite->hasAny($conditions)) {
                $favorite['Favorite'] = array('post_id' => $postid, 'user_id' => $userid);
                $this->Favorite->create();
                if ($this->Favorite->save($favorite)) {
                    $data['Ack'] = 1;
                    $data['res'] = 'This Course Successfully Added to Fevorite';
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Failed to save!!!';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Already exists!!!';
            }

            echo json_encode($data);
        }
    }

    public function unfavorite_this_course() {
        $this->autoRender = false;
        $data = array();

        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            $conditions = array(
                'Favorite.post_id' => $postid,
                'Favorite.user_id' => $userid
            );
            if ($this->Favorite->hasAny($conditions)) {
                $favorite = array('Favorite.post_id' => $postid, 'Favorite.user_id' => $userid);
                if ($this->Favorite->deleteAll($favorite)) {
                    $data['Ack'] = 1;
                    $data['res'] = 'This Course Successfully Removed from Favorite';
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Failed to remove!!!';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'No Course exists!!!';
            }

            echo json_encode($data);
        }
    }

}
