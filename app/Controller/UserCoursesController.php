<?php

App::uses('AppController', 'Controller');

/**
 * UserCourses Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class UserCoursesController extends AppController {

    public $components = array('Paginator', 'Session');

    public function index() {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access Your Courses.', 'default', array('class' => 'error'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        //echo $userid; exit;
        $this->loadModel('Archive');
        $this->loadModel('Wishlist');
        $this->loadModel('Post');
        $this->UserCourse->recursive = 2;
        $this->Wishlist->recursive = 2;
        $this->Archive->recursive = 2;
        $this->Post->recursive = 2;
        
        $user_courses = $this->UserCourse->find('list', array('conditions' => array('UserCourse.user_id' => $userid),'fields'=>array('post_id')));
        $userPosts = $this->Post->find('all',array('conditions'=>array('Post.id'=>$user_courses)));
        //pr($userPosts[0]['PostImage']); exit;
        
        $useArchive = $this->Archive->find('list',array('conditions' => array('Archive.user_id' => $userid),'fields'=>array('post_id')));
        $archivePosts = $this->Post->find('all',array('conditions'=>array('Post.id'=>$useArchive)));
        
        $useWishlist = $this->Wishlist->find('list',array('conditions' => array('Wishlist.user_id' => $userid),'fields'=>array('post_id')));
        $wishlistPosts = $this->Post->find('all',array('conditions'=>array('Post.id'=>$useWishlist)));
        //pr($wishlistPosts); exit;

        //print_r($userPosts);die;
        $this->set(compact('userPosts','archivePosts','wishlistPosts'));
    }

    public function take_this_course_direct() {
        $data = array();
        if ($this->request->is('post')) {
            $this->loadModel('UserCourse');
            $userid = $this->Session->read('userid');
            $postid = $this->request->data['postid'];
            $userCourse['UserCourse'] = array('user_id' => $userid, 'post_id' => $postid);
            $this->UserCourse->create();
            if ($this->UserCourse->save($userCourse)) {
                $data['Ack'] = 1;
            } else {
                $data['Ack'] = 0;
            }
        }
        echo json_encode($data);

        exit;
    }

}
