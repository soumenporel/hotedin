<?php

App::uses('AppController', 'Controller');
App::import('Vendor', 'Dompdf', array('file' => 'dompdf/vendor/autoload.php'));

// reference the Dompdf namespace
use Dompdf\Dompdf;

/**
 * Learns Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class LearnsController extends AppController
{
    /* function beforeFilter() {
    parent::beforeFilter();
    } */

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    public $uses = array('Post', 'User', 'Lesson', 'Lecture');
    public $layout = 'default';

    public function questions($slug = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('CourseQuestion');
        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];
        $course_questions = $this->CourseQuestion->find('all', array('conditions' => array('CourseQuestion.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $postDetails['Post']['id'])));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('course_questions', 'postDetails', 'userid', 'rating', 'RatingExit', 'lectureCount', 'readListCount', 'lastLectureDetails'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $postDetails['Post']['id'], 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $postDetails['Post']['id'], 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);
    }

    public function course_content($slug = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        //print_r($lessons);die;
        $this->set(compact('lessons', 'postDetails'));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $userid)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'userdetails', 'readList', 'readListCount', 'lectureCount', 'lastLectureDetails'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

    }

    public function overview($slug = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'error'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $instructor_id = $postDetails['Post']['user_id'];
        $instructorDetails = $this->User->find('first', array('conditions' => array('User.id' => $instructor_id)));
        $this->loadModel('CourseQuestion');
        $questions = $this->CourseQuestion->find('all', array('conditions' => array('CourseQuestion.post_id' => $post_id), 'limit' => 3));
        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $this->loadModel('Announcement');
        $announcements = $this->Announcement->find('all', array('conditions' => array('Announcement.post_id' => $post_id), 'limit' => 3));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('lessons', 'postDetails', 'instructorDetails', 'questions', 'rating', 'RatingExit', 'userid', 'announcements', 'lectureCount', 'readList', 'readListCount', 'lastLectureDetails', 'user_courses'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

    }

    public function announcement($slug = null)
    {
        $userid = $this->Session->read('userid');

        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->Post->recursive = 2;
        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));

        $this->loadModel('Announcement');
        $this->Announcement->recursive = 2;
        $announcements = $this->Announcement->find('all', array('conditions' => array('Announcement.post_id' => $post_id)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'announcements', 'lectureCount', 'readListCount', 'lastLectureDetails'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

        $this->loadModel('UserNotification');
        $this->UserNotification->updateAll(
            array(
                'UserNotification.status' => 0,
            ),
            array(
                'UserNotification.user_id' => $userid,
                'UserNotification.activity_id' => $post_id,
            )
        );

    }

    public function instructor_announcement($slug = null)
    {
        $userid = $this->Session->read('userid');

        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->Post->recursive = 2;
        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

//        if($postDetails['Post']['user_id'] != $userid){
        //            if(empty($user_courses)){
        //                // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        //            }
        //        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));

        $this->loadModel('Announcement');
        $this->Announcement->recursive = 2;
        $announcements = $this->Announcement->find('all', array('conditions' => array('Announcement.post_id' => $post_id)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'announcements', 'lectureCount', 'readListCount', 'lastLectureDetails', 'post_id'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

        $this->loadModel('UserNotification');
        $this->UserNotification->updateAll(
            array(
                'UserNotification.status' => 0,
            ),
            array(
                'UserNotification.user_id' => $userid,
                'UserNotification.activity_id' => $post_id,
            )
        );

    }

    public function answers($slug = null, $qid = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                return $this->redirect($this->webroot . 'posts/details/' . $postDetails['Post']['slug']);
            }
        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $this->set(compact('lessons', 'postDetails'));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $this->loadModel('CourseQuestion');
        $this->loadModel('CourseAnswer');

        $question = $this->CourseQuestion->find('first', array('conditions' => array('CourseQuestion.id' => $qid)));
        $answers = $this->CourseAnswer->find('all', array('conditions' => array('CourseAnswer.question_id' => $qid)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'question', 'answers', 'lectureCount', 'readListCount', 'lastLectureDetails'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

    }

    public function bookmarks($slug = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->loadModel('UserBookmark');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $bookmarks = $this->UserBookmark->find('all', array('conditions' => array('UserBookmark.user_id' => $userid, 'UserBookmark.post_id' => $post_id)));
        //print_r($bookmarks); die;
        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('postDetails', 'userid', 'rating', 'RatingExit', 'bookmarks', 'lectureCount', 'readListCount', 'lastLectureDetails'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

    }

    public function video_html($slug = null, $id = null, $time_id = null)
    {

        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        $this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('VideoStatus');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);

        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            //echo $lectureID; exit;
            //$nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            //$nextID = $nextLecture['Lecture']['id'];
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }
        $nextID;

        $previous = $this->Lecture->find('first', array('conditions' => array('Lecture.id <' => $id, 'Lecture.post_id ' => $post_id)));

        $this->loadModel('UserBookmark');
        $marks = $this->UserBookmark->find('all', array('conditions' => array('UserBookmark.user_id' => $userid, 'UserBookmark.post_id' => $post_id, 'UserBookmark.lecture_id' => $id)));

        $bookmarkTime = '';
        if ($time_id != '') {
            $bookmark = $this->UserBookmark->find('first', array('conditions' => array('UserBookmark.id' => $time_id)));
            $bookmarkTime = $bookmark['UserBookmark']['time'];
        }

//        if($nextID!=''){
        //            $next = $this->Lecture->find('first',array('conditions'=>array('Lecture.id'=>$nextID)));
        //            if($next['Lecture']['media']!=''){
        //                $file = $next['Lecture']['media'];
        //                $ext = pathinfo($file, PATHINFO_EXTENSION);
        //                $imageExt = array('jpeg' , 'jpg');
        //                $videoExt = array('mp4','flv','mkv');
        //                $pdfExt = array('pdf');
        //                $docExt = array('doc','docx','ppt');
        //                if(in_array(strtolower($ext), $videoExt)){
        //                    $URL = $this->webroot."learns/video_html/".$postDetails['Post']['slug']."/".$nextID;
        //                }
        //                if(in_array(strtolower($ext), $pdfExt)){
        //                    $URL = $this->webroot."learns/read_pdf/".$postDetails['Post']['slug']."/".$nextID;
        //                }
        //                if(in_array(strtolower($ext), $imageExt)){
        //                    $URL = $this->webroot."learns/view_image/".$postDetails['Post']['slug']."/".$nextID;
        //                }
        //                if(in_array(strtolower($ext), $docExt)){
        //                    $URL = $this->webroot."learns/read_doc/".$postDetails['Post']['slug']."/".$nextID;
        //                }
        //            }else{
        //                if($next['Lecture']['type']==0){
        //                    $URL = $this->webroot."learns/description/".$postDetails['Post']['slug']."/".$nextID;
        //                }else{
        //                    $URL = $this->webroot."learns/previewquiz/".$postDetails['Post']['slug']."/".$nextID;
        //                }
        //            }
        //        }else{
        //            $URL = $this->webroot."learns/course_content/".$postDetails['Post']['slug'];
        //        }

        $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $media = $lectureDetails['Lecture']['media'];
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'bookmarkTime', 'marks', 'URL', 'lectureCount'));

    }

    public function reload_function()
    {

    }

    public function read_pdf($slug = null, $id = null)
    {
        //$this->layout = 'quiz';
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        //$this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);

        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            // $nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            // $nextID = $nextLecture['Lecture']['id'];
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }
        //echo $nextID; exit;
        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));

        $count = 1;
        foreach ($lessons as $key => $lesson) {
            if ($lesson['Lesson']['id'] == $lectureDetails['Lecture']['lesson_id']) {
                $lesCount = $count;
            }
            $count++;
        }

        $count = 1;
        foreach ($lessonDetails['Lecture'] as $key => $lecture) {
            if ($lecture['id'] == $lectureDetails['Lecture']['id']) {
                $lecCount = $count;
            }
            $count++;
        }

        $media = $lectureDetails['Lecture']['media'];
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'URL', 'lesCount', 'lecCount'));
    }

    public function view_image($slug = null, $id = null)
    {
        // $this->layout = 'quiz';
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        //$this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);

        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            // $nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            // $nextID = $nextLecture['Lecture']['id'];
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }

        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $count = 1;
        foreach ($lessons as $key => $lesson) {
            if ($lesson['Lesson']['id'] == $lectureDetails['Lecture']['lesson_id']) {
                $lesCount = $count;
            }
            $count++;
        }

        $count = 1;
        foreach ($lessonDetails['Lecture'] as $key => $lecture) {
            if ($lecture['id'] == $lectureDetails['Lecture']['id']) {
                $lecCount = $count;
            }
            $count++;
        }
        $media = $lectureDetails['Lecture']['media'];
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'URL', 'lesCount', 'lecCount'));
    }

    public function read_doc($slug = null, $id = null)
    {
        $this->layout = 'quiz';
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        //$this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);

        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            // $nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            // $nextID = $nextLecture['Lecture']['id'];
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }

        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $count = 1;
        foreach ($lessons as $key => $lesson) {
            if ($lesson['Lesson']['id'] == $lectureDetails['Lecture']['lesson_id']) {
                $lesCount = $count;
            }
            $count++;
        }

        $count = 1;
        foreach ($lessonDetails['Lecture'] as $key => $lecture) {
            if ($lecture['id'] == $lectureDetails['Lecture']['id']) {
                $lecCount = $count;
            }
            $count++;
        }

        $media = $lectureDetails['Lecture']['media'];
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'URL', 'lesCount', 'lecCount'));
    }

    public function description($slug = null, $id = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        //$this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);
        //echo $lessonLastLecture['id'];
        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            //$nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            //echo $nextID; exit;
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                //pr($nextLesson);
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }

        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        // $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$post_id),'order'=>'Lesson.sortby Asc'));

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $count = 1;
        foreach ($lessons as $key => $lesson) {
            if ($lesson['Lesson']['id'] == $lectureDetails['Lecture']['lesson_id']) {
                $lesCount = $count;
            }
            $count++;
        }

        $count = 1;
        foreach ($lessonDetails['Lecture'] as $key => $lecture) {
            if ($lecture['id'] == $lectureDetails['Lecture']['id']) {
                $lecCount = $count;
            }
            $count++;
        }

        $media = $lectureDetails['Lecture']['media'];
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'URL', 'lesCount', 'lecCount'));
    }

    public function previewquiz($slug = null, $id = null)
    {
        //$this->layout = 'quiz';
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        //$this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);
        //echo $lessonLastLecture['id'];
        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            //$nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            //echo $nextID; exit;
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                //pr($nextLesson);
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }

        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        $this->loadModel('Question');
        $questions = $this->Question->find('all', array('conditions' => array('Question.quiz_id' => $lectureDetails['Lecture']['id'])));

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $media = $lectureDetails['Lecture']['media'];
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'URL', 'questions'));
    }

    public function previewquiz1($slug = null, $id = null)
    {
        //$this->layout = 'quiz';
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->Lesson->recursive = 2;
        //$this->layout = false;
        $this->loadModel('Lecture');
        $this->loadModel('Lesson');

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if (empty($user_courses)) {
            // return $this->redirect($this->webroot.$postDetails['Post']['slug']);
        }

        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);
        //echo $lessonLastLecture['id'];
        $nextID = '';
        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
            //$nextLecture = $this->Lecture->find('first',array('conditions'=>array('Lecture.id >'=>$lectureID)));
            //echo $nextID; exit;
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                //pr($nextLesson);
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }

        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }
        $this->loadModel('ReadLecture');
        $lectureStatus = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)));
        if (empty($lectureStatus)) {
            $readLecture = array('user_id' => $userid, 'post_id' => $post_id, 'lecture_id' => $id, 'read_status' => 1);
            $this->ReadLecture->save($readLecture);
        } else {
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 0),
                array('ReadLecture.user_id' => $userid, 'post_id' => $post_id)
            );
            $this->ReadLecture->updateAll(
                array('ReadLecture.read_status' => 1),
                array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.lecture_id' => $id)
            );
        }

        $this->loadModel('Question');
        $questions = $this->Question->find('all', array('conditions' => array('Question.quiz_id' => $lectureDetails['Lecture']['id'])));

        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        $media = $lectureDetails['Lecture']['media'];

        $this->loadModel('QuizScore');

        $optionsQuizOld = array('conditions' => array('`QuizScore`.`quiz_id`' => $id, '`QuizScore`.`user_id`' => $userid));
        $Quizdetails = $this->QuizScore->find('first', $optionsQuizOld);
        $this->set(compact('lessons', 'postDetails', 'lectureDetails', 'media', 'userid', 'URL', 'questions', 'Quizdetails'));
    }

    public function start_quiz($id = null, $retest = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->loadModel('Question');
        $questions = $this->Question->find('first', array('conditions' => array('Question.id' => $id)));

        $nextQuestion = $this->Question->find('first', array('conditions' => array('Question.id >' => $questions['Question']['id'], 'Question.quiz_id ' => $questions['Question']['quiz_id'])));
        $nextLectureUrl = $this->nextID($questions['Question']['quiz_id']);
        if (empty($nextQuestion)) {
            //$URL = $nextLectureUrl;
            $URL = $this->webroot . 'learns/end_quiz/' . $questions['Question']['quiz_id'];
        } else {
            $URL = $this->webroot . 'learns/start_quiz/' . $nextQuestion['Question']['id'];
        }
        // echo $nextLectureUrl; exit;

        //for retest part......................................
        if ($retest == 1) {

            $this->loadModel('UserScore');
            $quiz_id = $questions['Question']['quiz_id'];
            $query = "delete from user_scores WHERE user_id = " . $userid . " and quiz_id = " . $quiz_id;
            $this->UserScore->query($query);
//            $this->UserScore->deleteAll(
            //            ['UserScore.user_id' => "'$userid'",'UserScore.quiz_id' => "'$quiz_id'"]
            //
            //        );

        }

        $options = $questions['Question']['options'];
        $Qtype = $questions['Question']['Qtype'];
        $optionsDecode = json_decode($options);
        shuffle($optionsDecode);
        //print_r($optionsDecode);die;

        $this->set(compact('questions', 'optionsDecode', 'URL', 'Qtype', 'userid'));
    }

    public function nextID($id = null)
    {

        $this->loadModel('Lecture');
        $this->loadModel('Lesson');
        $this->loadModel('Post');
        $lectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $id)));
        $lessonID = $lectureDetails['Lecture']['lesson_id'];
        $lessonDetails = $this->Lesson->find('first', array('conditions' => array('Lesson.id' => $lessonID)));
        $lessonLastLecture = end($lessonDetails['Lecture']);
        $post_id = $lectureDetails['Lecture']['post_id'];
        $postDetails = $this->Post->find('first', array('conditions' => array('Post.id' => $post_id)));

        $nextID = '';

        if ($lessonLastLecture['id'] != $id) {
            $currentKey = '';
            foreach ($lessonDetails['Lecture'] as $lectureKey => $val) {
                if ($val['id'] == $id) {
                    $currentKey = $lectureKey;
                }
            }
            $count = count($lessonDetails['Lecture']);
            $lectureID = $lessonDetails['Lecture'][$currentKey + 1]['id'];
            $nextID = $lectureID;
        } else {
            $nextLesson = $this->Lesson->find('first', array('conditions' => array('Lesson.id >' => $lessonID, 'Lesson.post_id ' => $post_id)));
            if (!empty($nextLesson)) {
                if (isset($nextLesson['Lecture']['0'])) {
                    $nextID = $nextLesson['Lecture']['0']['id'];
                }
            } else {
                $nextID = '';
            }
        }

        if ($nextID != '') {
            $next = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $nextID)));
            if ($next['Lecture']['media'] != '') {
                $file = $next['Lecture']['media'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $imageExt = array('jpeg', 'jpg');
                $videoExt = array('mp4', 'flv', 'mkv');
                $pdfExt = array('pdf');
                $docExt = array('doc', 'docx', 'ppt');
                if (in_array(strtolower($ext), $videoExt)) {
                    $URL = $this->webroot . "learns/video_html/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $pdfExt)) {
                    $URL = $this->webroot . "learns/read_pdf/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $imageExt)) {
                    $URL = $this->webroot . "learns/view_image/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
                if (in_array(strtolower($ext), $docExt)) {
                    $URL = $this->webroot . "learns/read_doc/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            } else {
                if ($next['Lecture']['type'] == 0) {
                    $URL = $this->webroot . "learns/description/" . $postDetails['Post']['slug'] . "/" . $nextID;
                } else {
                    $URL = $this->webroot . "learns/previewquiz/" . $postDetails['Post']['slug'] . "/" . $nextID;
                }
            }
        } else {
            $URL = $this->webroot . "learns/course_content/" . $postDetails['Post']['slug'];
        }

        return $URL;
    }

    public function discussion($slug = null)
    {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $this->loadModel('CourseDiscussion');
        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];
        $course_discussions = $this->CourseDiscussion->find('all', array('conditions' => array('CourseDiscussion.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $postDetails['Post']['id'])));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $postDetails['Post']['id']), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->set(compact('course_discussions', 'postDetails', 'userid', 'rating', 'RatingExit', 'lectureCount', 'readListCount', 'lastLectureDetails', 'post_id'));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $postDetails['Post']['id'], 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $postDetails['Post']['id'], 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

    }

    public function addscore()
    {
        $this->layout = false;
        $this->loadModel('UserScore');

        $optionsScore = array('conditions' => array('UserScore.user_id' => $this->request->data['user_id'], 'UserScore.question_id' => $this->request->data['question_id']));
        $existScore = $this->UserScore->find('first', $optionsScore);

        if (count($existScore) > 0) {
            $givenAns = $this->request->data['your_ans'];
            $score = $this->request->data['score'];
            $this->UserScore->updateAll(
                array('your_ans' => "'$givenAns'", 'score' => "'$score'"),
                array('id' => $existScore['UserScore']['id'])
            );

        } else {
            $this->UserScore->create();

            $this->UserScore->save($this->request->data);
        }
        echo '1';
        exit;
    }

    public function addordering($user_id = null, $question_id = null, $quiz_id = null)
    {
        $this->layout = false;
        $this->loadModel('TempOrder');
        $updateRecordsArray = $_POST['recordsArray'];

        $options = array('conditions' => array('TempOrder.user_id' => $user_id, 'TempOrder.question_id' => $question_id));
        $Details = $this->TempOrder->find('first', $options);
        $listingCounter = 1;

        if (count($Details) > 0) {
            foreach ($updateRecordsArray as $recordIDValue) {

                $pieces = explode("@", $recordIDValue);
                $query = "update temp_orders SET sequence = " . $listingCounter . " WHERE answer_id = " . $pieces[0];
                $this->TempOrder->query($query);
                $listingCounter = $listingCounter + 1;
            }

        } else {
            foreach ($updateRecordsArray as $recordIDValue) {
                $pieces = explode("@", $recordIDValue);
                $query = "insert into temp_orders SET sequence = " . $listingCounter . ",answer_id = " . $pieces[0] . ",user_id=" . $user_id . ",question_id = " . $question_id . ",quiz_id = " . $quiz_id . ",correct_order = " . $pieces[1];
                $this->TempOrder->query($query);
                $listingCounter = $listingCounter + 1;
            }
        }

        // $this->TempOrder->create();
        //$this->TempOrder->save($this->request->data);
        echo '1';
        exit;
    }

    public function addscoreordering()
    {
        $this->layout = false;
        $this->loadModel('UserScore');
        $this->loadModel('TempOrder');
        $score = 0;
        $user_id = $this->request->data['user_id'];
        $question_id = $this->request->data['question_id'];
        //$user_id=93;
        //$question_id=22;
        $options = array('conditions' => array('TempOrder.user_id' => $user_id, 'TempOrder.question_id' => $question_id));
        $Details = $this->TempOrder->find('all', $options);
        //print_r($Details);die;
        if (count($Details) > 0) {
            foreach ($Details as $Detail) {

                //print_r($Detail);die;
                if ($Detail['TempOrder']['correct_order'] == $Detail['TempOrder']['sequence']) {
                    $score = $score + 1;
                }
            }

        }
        $this->request->data['score'] = $score;

        $optionsScore = array('conditions' => array('UserScore.user_id' => $this->request->data['user_id'], 'UserScore.question_id' => $this->request->data['question_id']));
        $existScore = $this->UserScore->find('first', $optionsScore);

        if (count($existScore) > 0) {

            $this->UserScore->updateAll(
                array('score' => "'$score'"),
                array('id' => $existScore['UserScore']['id'])
            );

        } else {
            $this->UserScore->create();
            $this->UserScore->save($this->request->data);
        }
        echo '1';
        exit;
    }

    public function addmatching($user_id = null, $question_id = null, $quiz_id = null)
    {
        $this->layout = false;
        $this->loadModel('TempOrder');
        $updateRecordsArray = $_POST['recordsArray2'];
        $originalOrderArray = $_POST['recordsArray3'];

        $options = array('conditions' => array('TempOrder.user_id' => $user_id, 'TempOrder.question_id' => $question_id));
        $Details = $this->TempOrder->find('first', $options);
        $listingCounter = 0;

        if (count($Details) > 0) {
            foreach ($updateRecordsArray as $recordIDValue) {

                $query = "update temp_orders SET answer = '" . $recordIDValue . "' WHERE correct_answer = '" . $originalOrderArray[$listingCounter] . "'";

                $this->TempOrder->query($query);
                $listingCounter = $listingCounter + 1;
            }

        } else {
            foreach ($updateRecordsArray as $recordIDValue) {

                $query = "insert into temp_orders SET answer = '" . $recordIDValue . "',correct_answer = '" . $originalOrderArray[$listingCounter] . "',user_id=" . $user_id . ",question_id = " . $question_id . ",quiz_id = " . $quiz_id;

                $this->TempOrder->query($query);
                $listingCounter = $listingCounter + 1;
            }
        }

        // $this->TempOrder->create();
        //$this->TempOrder->save($this->request->data);
        echo '1';
        exit;
    }

    public function addscorematching()
    {
        $this->layout = false;
        $this->loadModel('UserScore');
        $this->loadModel('TempOrder');
        $score = 0;
        $user_id = $this->request->data['user_id'];
        $question_id = $this->request->data['question_id'];
        //$user_id=93;
        //$question_id=22;
        $options = array('conditions' => array('TempOrder.user_id' => $user_id, 'TempOrder.question_id' => $question_id));
        $Details = $this->TempOrder->find('all', $options);
        //print_r($Details);die;
        if (count($Details) > 0) {
            foreach ($Details as $Detail) {

                //print_r($Detail);die;
                if ($Detail['TempOrder']['correct_answer'] == $Detail['TempOrder']['answer']) {
                    $score = $score + 1;
                }
            }

        }
        $this->request->data['score'] = $score;

        $optionsScore = array('conditions' => array('UserScore.user_id' => $this->request->data['user_id'], 'UserScore.question_id' => $this->request->data['question_id']));
        $existScore = $this->UserScore->find('first', $optionsScore);

        if (count($existScore) > 0) {

            $this->UserScore->updateAll(
                array('score' => "'$score'"),
                array('id' => $existScore['UserScore']['id'])
            );

        } else {
            $this->UserScore->create();
            $this->UserScore->save($this->request->data);
        }
        echo '1';
        exit;
    }
    public function certification($quiz_id = null)
    {
        $this->layout = false;
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'error'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $username = $this->Session->read('username');
        $this->loadModel('Lecture');
        $optionsLecture = array('conditions' => array('Lecture.id' => $quiz_id));
        $LectureDetails = $this->Lecture->find('first', $optionsLecture);
        $course = $LectureDetails['Post']['post_title'];

        $number = rand(1000, 9999);

        //for pdf generate.............................................
        $fileName = date('Y-m-d') . '_' . $number . '.pdf';
        //$certificateImg='/team4/studilmu/cerificate.png';
        $certificateImg = 'http://192.168.1.118/team4/studilmu/1_cerificate2.png';
        $html = '<html>
<head>
<title></title>
<meta name="" content="">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</head>
<body style="font-family: \'Roboto\', sans-serif; width:810px;height:900px;background-image:url(\'http://192.168.1.118/team4/studilmu/cerificate1.png\');background-size:100%;overflow: hidden;margin: 0 auto;background-repeat:no-repeat;background-possion:top-left;">
        <div class="certificate_container">
		<span style="display: block;text-align:center;color:#4f81bd;margin-top:200px;font-size: 17px">Certificate number : ONCERT/' . date('Y') . '/' . $number . '</span>
		<span style="display: block;text-align:center;color:#4f81bd;margin-top:240px;font-size: 70px;font-family: \'Edwardian Script ITC\';">' . $username . '</span>
		<b style="display: block;text-align:center;color:#4f81bd;margin-top:125px;font-size: 35px;">' . $course . '</b>
		<b style="display: block;text-align:center;color:#4f81bd;margin-top:100px;font-size: 25px;"><span style="color:#000">Issued on</span> ' . date('F j,Y') . '</b>
	</div>

</body>
<style>
	@font-face {
		font-family: "Edwardian Script ITC";
		src: url("team4/studilmu/fonts/EdwardianScriptITC.eot");
		src: url("team4/studilmu/fonts/EdwardianScriptITC.eot?#iefix") format("embedded-opentype"),
		url("team4/studilmu/fonts/EdwardianScriptITC.woff") format("woff"),
		url("team4/studilmu/fonts/EdwardianScriptITC.ttf") format("truetype");
		font-weight: normal;
		font-style: normal;
	}

.certificate_container
{

}
body,html{margin:0;padding:0;margin-bottom:10px;}

</style>
</html>';
        //echo $html;
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        //$dompdf->stream($fileName);
        $output = $dompdf->output();
        file_put_contents("certificate/" . $fileName, $output);
        $this->loadModel('QuizScore');
        $optionsScore = array('conditions' => array('QuizScore.user_id' => $userid, 'QuizScore.quiz_id' => $quiz_id));
        $existScore = $this->QuizScore->find('first', $optionsScore);

        if (count($existScore) > 0) {

            $this->QuizScore->updateAll(
                array('file_name' => "'$fileName'"),
                array('id' => $existScore['QuizScore']['id'])
            );

        }
        $this->set(compact('username', 'course', 'number', 'fileName'));
    }

    public function htmltopdf()
    {

        $fileName = 'a.pdf';
        $html =
            '<html><body>' .
            '<p>Put your html here, or generate it with your favourite ' .
            'templating system.</p>' .
            '</body></html>';
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        //$dompdf->stream($fileName);
        $output = $dompdf->output();
        file_put_contents("certificate/file.pdf", $output);
        echo 1;
        //$dompdf->stream($fileName);

        exit;
    }

    public function assignment($slug = null)
    {

        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                //// return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        //print_r($lessons);die;
        $this->set(compact('lessons', 'postDetails'));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $userid)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->loadModel('Assignment');
        $this->loadModel('UserAssignment');
        if (isset($this->request->data) && !empty($this->request->data)) {

            $this->request->data1 = array();
            $ids = $this->request->data['ids'];
            $questions = $this->request->data['questions'];

            //print_r($questions);die;

            $marks = $this->request->data['markss'];
            $answers = $this->request->data['answers'];

            for ($i = 0; $i < count($questions); $i++) {
                $quesstionArr[$i]['id'] = $ids[$i];
                $quesstionArr[$i]['question'] = $questions[$i];
                $quesstionArr[$i]['marks'] = $marks[$i];
                $quesstionArr[$i]['answer'] = $answers[$i];
            }

            $data = json_encode($quesstionArr);
            $this->request->data1['UserAssignment']['answer'] = $data;
            $this->request->data1['UserAssignment']['q_id'] = json_encode($ids);
            $this->request->data1['UserAssignment']['question'] = json_encode($questions);

            $this->request->data1['UserAssignment']['marks'] = json_encode($marks);
            $this->request->data1['UserAssignment']['assignment_id'] = $this->request->data['assignment_id'];
            $this->request->data1['UserAssignment']['total_marks'] = $this->request->data['total_marks'];
            $this->request->data1['UserAssignment']['post_id'] = $post_id;
            $this->request->data1['UserAssignment']['user_id'] = $userid;
            $this->request->data1['UserAssignment']['status'] = 1;
            $this->UserAssignment->create();
            $this->UserAssignment->save($this->request->data1);

            $this->Session->setFlash('Assignment saved successfully.', 'default', array('class' => 'success'));

        }

        $assignments = $this->Assignment->find('all', array('conditions' => array('Assignment.post_id' => $post_id)));

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'userdetails', 'readList', 'readListCount', 'lectureCount', 'lastLectureDetails', 'assignments'));

    }

    public function assignment_submitted($slug = null)
    {

        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                //// return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        //print_r($lessons);die;
        $this->set(compact('lessons', 'postDetails'));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $userid)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->loadModel('Assignment');
        $this->loadModel('UserAssignment');

        $assignments = $this->Assignment->find('all', array('conditions' => array('Assignment.post_id' => $post_id)));

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'userdetails', 'readList', 'readListCount', 'lectureCount', 'lastLectureDetails', 'assignments'));

    }

    public function assignment_evalute($slug = null, $userassignment_id = null)
    {

        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'success'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        $postDetails = $this->Post->find('first', array('conditions' => array('Post.slug' => $slug)));
        $post_id = $postDetails['Post']['id'];

        $this->loadModel('UserCourse');
        $this->UserCourse->recursive = 0;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid, 'UserCourse.post_id' => $post_id)));

        if ($postDetails['Post']['user_id'] != $userid) {
            if (empty($user_courses)) {
                //// return $this->redirect($this->webroot.$postDetails['Post']['slug']);
            }
        }

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all', array('conditions' => array('Lesson.post_id' => $post_id), 'order' => 'Lesson.sortby Asc'));
        //print_r($lessons);die;
        $this->set(compact('lessons', 'postDetails'));

        $this->loadModel('Rating');
        $RatingExit = $this->Rating->find('first', array('conditions' => array('Rating.user_id' => $userid, 'Rating.post_id' => $post_id)));
        if (!empty($RatingExit)) {
            $rating = $RatingExit['Rating']['id'];
        } else {
            $rating = '';
        }

        $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $userid)));

        $this->loadModel('ReadLecture');
        $readList = $this->ReadLecture->find('list', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        $readListCount = count($readList);

        $this->loadModel('Lecture');
        $lectureCount = $this->Lecture->find('count', array('conditions' => array('Lecture.post_id' => $postDetails['Post']['id'])));

        $this->loadModel('Favorite');
        $favoritexist = $this->Favorite->find('first', array('conditions' => array('Favorite.post_id' => $post_id, 'Favorite.user_id' => $this->Session->read('userid'))));
        if (!empty($favoritexist)) {

            $favoritexist = 1;
        } else {
            $favoritexist = 0;
        }

        $this->set('favoritexist', $favoritexist);

        $this->loadModel('Archive');
        $archivexist = $this->Archive->find('first', array('conditions' => array('Archive.post_id' => $post_id, 'Archive.user_id' => $this->Session->read('userid'))));
        if (!empty($archivexist)) {

            $archivexist = 1;
        } else {
            $archivexist = 0;
        }

        $this->set('archivexist', $archivexist);

        $LastReadCourse = $this->ReadLecture->find('first', array('conditions' => array('ReadLecture.user_id' => $userid, 'ReadLecture.post_id' => $post_id, 'ReadLecture.read_status' => 1), 'fields' => array('ReadLecture.id', 'ReadLecture.lecture_id')));
        if (!empty($LastReadCourse)) {
            $this->Lecture->recursive = 2;
            $lastLectureDetails = $this->Lecture->find('first', array('conditions' => array('Lecture.id' => $LastReadCourse['ReadLecture']['lecture_id'])));
        }

        $this->loadModel('Assignment');
        $this->loadModel('UserAssignment');
        if (isset($this->request->data) && !empty($this->request->data)) {

            $this->request->data1 = array();
            $ids = $this->request->data['ids'];
            $questions = $this->request->data['questions'];

            //print_r($questions);die;

            $marks = $this->request->data['markss'];
            $answers = $this->request->data['answers'];
            $scores = $this->request->data['scores'];
            $total_score = 0;
            for ($i = 0; $i < count($questions); $i++) {
                $quesstionArr[$i]['id'] = $ids[$i];
                $quesstionArr[$i]['question'] = $questions[$i];
                $quesstionArr[$i]['marks'] = $marks[$i];
                $quesstionArr[$i]['answer'] = $answers[$i];
                $quesstionArr[$i]['score'] = $scores[$i];
                $total_score = $total_score + $scores[$i];
            }

            $data = json_encode($quesstionArr);

            $this->request->data1['UserAssignment']['answer'] = $data;

            $this->request->data1['UserAssignment']['scores'] = json_encode($scores);

            $this->request->data1['UserAssignment']['total_score'] = $total_score;

            $this->request->data1['UserAssignment']['status'] = 2;
            $this->UserAssignment->id = $userassignment_id;
            $this->UserAssignment->save($this->request->data1);

            $this->Session->setFlash('Assignment score saved successfully.', 'default', array('class' => 'success'));

        }

        $assignment = $this->UserAssignment->find('first', array('conditions' => array('UserAssignment.id' => $userassignment_id)));

        $this->set(compact('lessons', 'postDetails', 'userid', 'rating', 'RatingExit', 'userdetails', 'readList', 'readListCount', 'lectureCount', 'lastLectureDetails', 'assignment'));

    }

    public function end_quiz($quiz_id = null)
    {
        $certificate = 0;
        $this->loadModel('UserScore');
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'error'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $this->loadModel('UserScore');
        $sum = $this->UserScore->find('all', array(
            'conditions' => array(
                'UserScore.user_id' => $userid, 'UserScore.quiz_id' => $quiz_id),
            'fields' => array('sum(UserScore.score) as total_score',
            ),
        )
        );

        $your_score = $sum[0][0]['total_score'];

        $this->loadModel('Question');
        $marks = $this->Question->find('all', array(
            'conditions' => array(
                'Question.quiz_id' => $quiz_id),
            'fields' => array('sum(Question.marks) as total_marks',
            ),
        )
        );

        $total_marks = $marks[0][0]['total_marks'];
        $percent = ($your_score / $total_marks) * 100;
        $yourPercentage = round($percent, 1);
        if ($yourPercentage >= 80) {
            $certificate = 1;
        }

        $this->loadModel('QuizScore');
        $arr = array();
        $arr['QuizScore']['user_id'] = $userid;
        $arr['QuizScore']['quiz_id'] = $quiz_id;
        $arr['QuizScore']['score'] = $your_score;
        $arr['QuizScore']['total_marks'] = $total_marks;
        $arr['QuizScore']['percentage'] = $yourPercentage;
        $arr['QuizScore']['is_certificate'] = $certificate;
        $arr['QuizScore']['test_date'] = date('Y-m-d');

        $optionsScore = array('conditions' => array('QuizScore.user_id' => $userid, 'QuizScore.quiz_id' => $quiz_id));
        $existScore = $this->QuizScore->find('first', $optionsScore);

        if (count($existScore) > 0) {
            $retest = $existScore['QuizScore']['retest'];
            $currentretest = $retest + 1;
            $this->QuizScore->updateAll(
                array('score' => "'$your_score'", 'percentage' => "'$yourPercentage'", 'is_certificate' => "'$certificate'", 'retest' => "'$currentretest'"),
                array('id' => $existScore['QuizScore']['id'])
            );

        } else {
            $this->QuizScore->create();
            $this->QuizScore->save($arr);
        }
        $this->set(compact('your_score', 'total_marks', 'yourPercentage', 'certificate', 'quiz_id'));

    }

    public function quiz_result($quiz_id = null)
    {
        $certificate = 0;
        $this->loadModel('QuizScore');
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash('Please login to access profile.', 'default', array('class' => 'error'));
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
        $optionsScore = array('conditions' => array('QuizScore.user_id' => $userid, 'QuizScore.quiz_id' => $quiz_id));
        $existScore = $this->QuizScore->find('first', $optionsScore);

        if (count($existScore) > 0) {
            $your_score = $existScore['QuizScore']['score'];
            $total_marks = $existScore['QuizScore']['total_marks'];
            $yourPercentage = $existScore['QuizScore']['percentage'];
            $certificate = $existScore['QuizScore']['is_certificate'];

        } else {
            $your_score = 0;
            $total_marks = 0;
            $yourPercentage = 0;
            $certificate = 0;
        }
        $this->set(compact('your_score', 'total_marks', 'yourPercentage', 'certificate', 'quiz_id'));

    }

    //chk prev quiz details................................................

    public function isQuiz($quiz_id = null, $userid = null)
    {
        $this->layout = false;
        $this->loadModel('QuizScore');

        $options = array('conditions' => array('`QuizScore`.`quiz_id`' => $quiz_id, '`QuizScore`.`user_id`' => $userid));
        $Quizdetails = $this->QuizScore->find('first', $options);
        //print_r($Quizdetails);die;
        return $Quizdetails;
        exit;
    }

    //add discussion comment............................................
    public function addDiscussion()
    {
        $this->layout = false;
        $this->loadModel('CourseDiscussion');

        $this->request->data['post_time'] = date('Y-m-d H:i:s');
        $this->CourseDiscussion->create();

        $this->CourseDiscussion->save($this->request->data);

        $this->loadModel('User');
        $userid = $this->Session->read('userid');
        $options = array('conditions' => array('`User`.`id`' => $userid));
        $userdetails = $this->User->find('first', $options);
        if (isset($userdetails['User']['user_image']) && $userdetails['User']['user_image'] != '') {
            $img = $this->webroot . 'user_images/' . $userdetails['User']['user_image'];} else { $img = $this->webroot . 'img/profile_img.jpg';}

        $html = '<div class="row course-section mt-0">
          <div class="col-1 section-title">

              <img src="' . $img . '" class="instructor-avatar"> </div>
         <div class="col-11">
            <div class="instructor-title mb-2"> <a href="#" class="font-15" style="color: #d20000!important;">
            ' . $userdetails['User']['first_name'] . ' ' . $userdetails['User']['last_name'] . '</a>
                <span class="time-gray-text">Now</span> </div>


            <div class="instructor-description no-top-margine">
               <p class="font-14" style="margin-bottom: 0!important;">
               ' . $this->request->data['description'] . '</p>

            </div>
         </div>
		<div class="separator-b"></div>

      </div>';

        echo $html;
        exit;
    }

    public function assignmentStatus($assignment_id = null, $userid = null)
    {
        $this->layout = false;
        $this->loadModel('UserAssignment');

        $options = array('conditions' => array('`UserAssignment`.`assignment_id`' => $assignment_id, '`UserAssignment`.`user_id`' => $userid));
        $details = $this->UserAssignment->find('first', $options);
        if (count($details) == 0) {
            $details = 0;
        }
        return $details;
        exit;
    }

    public function assignmentUser($assignment_id = null)
    {
        $this->layout = false;
        $this->loadModel('UserAssignment');

        $options = array('conditions' => array('`UserAssignment`.`assignment_id`' => $assignment_id));
        $details = $this->UserAssignment->find('all', $options);

        return $details;
        exit;
    }

    public function addAnnouncement()
    {
        $this->layout = false;
        $this->loadModel('Announcement');

        $this->request->data['post_time'] = date('Y-m-d H:i:s');
        $this->Announcement->create();

        $this->Announcement->save($this->request->data);

        $html = '
            <div class=" d-flex">
                <div class="thread-head__basic-info " >
                    <div class="annoucement_comment_div font-14">
                        <p class="mb-1"><strong>' . $this->request->data['title'] . '</strong></p>
                        <p class="no-compress user_announcement_comment" >' . $this->request->data['announcement'] . '</p>
                    </div>
                </div>
            </div>';

        echo $html;
        exit;
    }

    public function ajaxEditAnnouncement()
    {
        $this->layout = false;
        $this->loadModel('Announcement');

        $this->Announcement->id = $this->request->data['id'];
        $this->Announcement->save($this->request->data);

        $html = 1;
        echo $html;
        exit;
    }

    public function ajaxDeleteComment()
    {
        $this->layout = false;
        $this->loadModel('Announcement');

        $this->Announcement->id = $this->request->data['id'];
        $this->Announcement->delete();

        $html = 1;
        echo $html;
        exit;
    }

    public function addstudentNote()
    {
        $this->layout = false;
        $this->loadModel('UserCourse');

        $this->UserCourse->id = $this->request->data['id'];
        $this->UserCourse->save($this->request->data);

        $html = 1;
        echo $html;
        exit;
    }
}
