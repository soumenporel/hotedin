<?php
App::uses('AppController', 'Controller');
/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class BlogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($catID=null) {
            /*$this->loadModel('BlogCategory');
             $this->loadModel('Pagebanner');
            	//$this->Category->recursive = 0;
        //$options = array('order' => array('Blog.id' => 'desc'));
        //$this->Paginator->settings = $options;
        $blogList=$this->Blog->find('all',array('order'=>array('Blog.id' => 'desc')));
        $this->set('blog_list', $blogList);

        $mod_blog = $this->Blog->find('first',array('order'=>array('Blog.modified_date'=>'desc'),'limit'=>'1'));

        $featured_blog = $this->Blog->find('all',array('conditions' => array('Blog.is_feature'=>1),'order'=>array('Blog.id'=>'desc'),'limit'=>'4'));
         $category_blog = $this->BlogCategory->find('all',array('conditions' => array('BlogCategory.show_in_homepage'=>1,'BlogCategory.status'=>1),'order'=>array('BlogCategory.id'=>'desc'),'limit'=>'4'));

          $page_id=7;
           $banners = $this->Pagebanner->find('all', array(
            'conditions' => array(
                'Pagebanner.page_id' => $page_id
            )
        ));*/
        $this->Paginator->settings = array(
            'conditions' => array('Blog.status' => 1),
            'order' => array(
            'Blog.modified_date' => 'desc',
            'limit' => 12
            )
        );
        $blogs = $this->Paginator->Paginate('Blog');

        $count = $this->Blog->find('count', array(
                                        'conditions' =>array('Blog.status' => 1)
                                        ));
        if($count < 12){
            $pageContent = '1 to '.$count;
        }else {
            $pageContent = '1 to 12';
        }


        $this->loadModel('Pagebanner');

        $pagebanner = $this->Pagebanner->find('first', array(
                                                  'conditions'=>array('Pagebanner.id'=>39),
                                                  array('Pagebanner.status' => 1)
                                                ));
        $this->loadModel('AppPromotion');
        $AppPromotions = $this->AppPromotion->find('all',array(
                                        'conditions' =>array('AppPromotion.active' => 1)
                                        ));

        $this->set(compact('blogs', 'pagebanner','AppPromotions','count', 'pageContent'));
	}

    public function details($slug=null) {
        $this->loadModel('BlogCategory');
        $title_for_layout = 'Blog Details';
        $this->loadModel('BlogComment');
        $userid = $this->Session->read('user_id');

        if ($this->request->is(array('post', 'put'))) {
            //$UserID=$this->Session->read('userid');
            $comment = $this->request->data['comment'];
            $blog_id=$this->request->data['blog_id'];
            $user_id=$userid;
            $name=$this->request->data['name'];
            $website=$this->request->data['website'];
            $email=$this->request->data['email'];
            //$noti['BlogComment']['user_id'] = $UserID;
            $noti['BlogComment']['blog_id'] = $blog_id;
            $noti['BlogComment']['user_id'] = $user_id;
            $noti['BlogComment']['name'] = $name;
            $noti['BlogComment']['email'] = $email;
            $noti['BlogComment']['comment'] = $comment;
            $noti['BlogComment']['website'] = $website;
            $this->BlogComment->create();
            $this->BlogComment->save($noti);
            $this->Session->setFlash(__('The blog comment submited successfully.'));
            return $this->redirect(array('action' => 'details/'.$slug));
        }else{
        	$this->Blog->recursive = 2;
            $options = array('conditions' => array('Blog.slug' => $slug));
            $blog_details = $this->Blog->find('first', $options);
            $blog_id=$blog_details['Blog']['id'];
            $related_options = array('conditions' => array('Blog.id !=' => $blog_id,'Blog.status'=>1), 'order' => array('Blog.modified_date' => 'desc'), 'limit' => 1);
            $latestupdateblogs = $this->Blog->find('first', $related_options);

            $featured_blog = $this->Blog->find('all',array('conditions' => array('Blog.is_feature'=>1),'order'=>array('Blog.id'=>'desc'),'limit'=>'4'));
            $category_blog = $this->BlogCategory->find('all',array('conditions' => array('BlogCategory.show_in_homepage'=>1,'BlogCategory.status'=>1),'order'=>array('BlogCategory.id'=>'desc'),'limit'=>'4'));
        }
        $this->set(compact('title_for_layout','blog_details','RelatedBlogs','BlogsComments','userid','latestupdateblogs','featured_blog','category_blog'));
	}

	public function admin_index() {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $title_for_layout = 'Blogs List';
            //$this->Category->recursive = 0;
            $options = array('order' => array('Blog.id' => 'desc'));
            $this->Paginator->settings = $options;
            $this->set('blog_list', $this->Paginator->paginate('Blog'));
            $this->set(compact('title_for_layout'));
	}

	public function admin_view($id = null) {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
                $this->redirect('/admin');
            }
            $title_for_layout = 'Blog View';
            if (!$this->Blog->exists($id)) {
                throw new NotFoundException(__('Invalid Blog'));
            }
            $options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
            $blog_view = $this->Blog->find('first', $options);

            $this->set(compact('title_for_layout','blog_view'));
	}

/**
 * add method
 *
 * @return void
 */

	public function admin_add() {
            $this->loadModel('BlogCategory');
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $title_for_layout = 'Blog Add';
            $categories = $this->BlogCategory->find('list', array('fields' => array('BlogCategory.id', 'BlogCategory.category_name')));
            if ($this->request->is('post')) {
                $options = array('conditions' => array('Blog.title'  => $this->request->data['Blog']['title']));
                $name = $this->Blog->find('first', $options);
                if(!$name){

                    if(isset($this->request->data['Blog']['image']) && $this->request->data['Blog']['image']['name']!=''){
                            $ext = explode('/',$this->request->data['Blog']['image']['type']);
                            if($ext){
                                $uploadFolder = "blogs_image";
                                $uploadPath = WWW_ROOT . $uploadFolder;
                                $extensionValid = array('jpg','jpeg','png','gif');
                                if(in_array($ext[1],$extensionValid)){

                                    $max_width = "600";
                                    $size = getimagesize($this->request->data['Blog']['image']['tmp_name']);

                                    $width = $size[0];
                                    $height = $size[1];
                                    $imageName = time().'_'.(strtolower(trim($this->request->data['Blog']['image']['name'])));
                                    $full_image_path = $uploadPath . '/' . $imageName;
                                    move_uploaded_file($this->request->data['Blog']['image']['tmp_name'],$full_image_path);
                                    $this->request->data['Blog']['image'] = $imageName;

                                    if ($width > $max_width){
                                        $scale = $max_width/$width;
                                        $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                    }else{
                                        $scale = 1;
                                        $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                    }/**/
                                    //unlink($uploadPath. '/' .$this->request->data['User']['hidprofile_img']);
                                } else{
                                    $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                                    return $this->redirect(array('action' => 'admin_add'));
                                }
                            }
                    }else{
                        $this->request->data['Blog']['image'] = '';
                    }

                    if(isset($this->request->data['Blog']['background_image']) && $this->request->data['Blog']['background_image']['name']!=''){
                            $ext = explode('/',$this->request->data['Blog']['background_image']['type']);
                            if($ext){
                                $uploadFolder = "blogs_image";
                                $uploadPath = WWW_ROOT . $uploadFolder;
                                $extensionValid = array('jpg','jpeg','png','gif');
                                if(in_array($ext[1],$extensionValid)){

                                    $max_width = "600";
                                    $size = getimagesize($this->request->data['Blog']['background_image']['tmp_name']);
                                    $width = $size[0];
                                    $height = $size[1];
                                    $bacimageName = time().'_'.(strtolower(trim($this->request->data['Blog']['background_image']['name'])));
                                    $full_image_path = $uploadPath . '/' . $bacimageName;
                                    move_uploaded_file($this->request->data['Blog']['background_image']['tmp_name'],$full_image_path);
                                    $this->request->data['Blog']['background_image'] = $bacimageName;

                                    if ($width > $max_width){
                                        $scale = $max_width/$width;
                                        $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                    }else{
                                        $scale = 1;
                                        $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                    }
                                } else{
                                    $this->Session->setFlash(__('Please upload image for background image of .jpg, .jpeg, .png or .gif format.'));
                                    return $this->redirect(array('action' => 'admin_add'));
                                }
                            }
                    }else{
                        $this->request->data['Blog']['background_image'] = '';
                    }
                    $this->request->data['Blog']['user_id'] = $userid;
                    $this->Blog->create();
                    if ($this->Blog->save($this->request->data)) {
                            $this->Session->setFlash(__('The blog has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Session->setFlash(__('The blog could not be saved. Please, try again.'));
                    }
                } else {
                        $this->Session->setFlash(__('The blog name already exists. Please, try again.'));
                }
            }
            $this->set(compact('title_for_layout','categories'));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function admin_edit($id = null) {
             $this->loadModel('BlogCategory');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $title_for_layout = 'Blog Edit';
	    if (!$this->Blog->exists($id)) {
                    throw new NotFoundException(__('Invalid Blog'));
            }
            $categories = $this->BlogCategory->find('list', array('fields' => array('BlogCategory.id', 'BlogCategory.category_name')));
            if ($this->request->is(array('post', 'put'))) {
                //$options = array('conditions' => array('Blog.title'  => $this->request->data['Blog']['title'], 'Blog.cat_id'=>$this->request->data['Blog']['cat_id']));
                //$name = $this->Blog->find('first', $options);
                $name = array();
                if(!$name){

                    if(isset($this->request->data['Blog']['image']) && $this->request->data['Blog']['image']['name']!=''){
                        $ext = explode('/',$this->request->data['Blog']['image']['type']);
                        if($ext){
                            $uploadFolder = "blogs_image";
                            $uploadPath = WWW_ROOT . $uploadFolder;
                            $extensionValid = array('jpg','jpeg','png','gif');
                            if(in_array($ext[1],$extensionValid)){

                                $max_width = "600";
                                $size = getimagesize($this->request->data['Blog']['image']['tmp_name']);

                                $width = $size[0];
                                $height = $size[1];
                                $imageName = time().'_'.(strtolower(trim($this->request->data['Blog']['image']['name'])));
                                $full_image_path = $uploadPath . '/' . $imageName;
                                move_uploaded_file($this->request->data['Blog']['image']['tmp_name'],$full_image_path);
                                $this->request->data['Blog']['image'] = $imageName;

                                if ($width > $max_width){
                                    $scale = $max_width/$width;
                                    $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                }else{
                                    $scale = 1;
                                    $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                }
                                if( $this->request->data['Blog']['hide_img']!='' && file_exists($uploadPath. '/' .$this->request->data['Blog']['hide_img'])){
                                    unlink($uploadPath. '/' .$this->request->data['Blog']['hide_img']);
                                }
                            } else{
                                $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                                return $this->redirect(array('action' => 'admin_edit/'.$id));
                            }
                        }
                    }else{
                        $this->request->data['Blog']['image'] = $this->request->data['Blog']['hide_img'];
                    }
                    if(isset($this->request->data['Blog']['background_image']) && $this->request->data['Blog']['background_image']['name']!=''){
                            $ext = explode('/',$this->request->data['Blog']['background_image']['type']);
                            if($ext){
                                $uploadFolder = "blogs_image";
                                $uploadPath = WWW_ROOT . $uploadFolder;
                                $extensionValid = array('jpg','jpeg','png','gif');
                                if(in_array($ext[1],$extensionValid)){

                                    $max_width = "600";
                                    $size = getimagesize($this->request->data['Blog']['background_image']['tmp_name']);
                                    $width = $size[0];
                                    $height = $size[1];
                                    $bacimageName = time().'_'.(strtolower(trim($this->request->data['Blog']['background_image']['name'])));
                                    $full_image_path = $uploadPath . '/' . $bacimageName;
                                    move_uploaded_file($this->request->data['Blog']['background_image']['tmp_name'],$full_image_path);
                                    $this->request->data['Blog']['background_image'] = $bacimageName;

                                    if ($width > $max_width){
                                        $scale = $max_width/$width;
                                        $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                    }else{
                                        $scale = 1;
                                        $uploaded = $this->resizeImage($full_image_path,$width,$height,$scale);
                                    }
                                } else{
                                    $this->Session->setFlash(__('Please upload image for background image of .jpg, .jpeg, .png or .gif format.'));
                                    return $this->redirect(array('action' => 'admin_add'));
                                }
                            }
                    }else{
                        $this->request->data['Blog']['background_image'] = $this->request->data['Blog']['hide_backimg'];
                    }
                    $this->request->data['Blog']['modified_date'] = date('Y-m-d H:i:s');
                    if ($this->Blog->save($this->request->data)) {
                        $this->Session->setFlash(__('The blog has been saved.'));
                        return $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('The blog could not be saved. Please, try again.'));
                        return $this->redirect(array('action' => 'admin_edit/'.$id));
                    }
                } else {
                    $this->Session->setFlash(__('The blog already exists. Please, try again.'));
                    return $this->redirect(array('action' => 'admin_edit/'.$id));
                }
            } else {

                $options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
                $this->request->data = $this->Blog->find('first', $options);
            }
            $this->set(compact('title_for_layout','categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('The category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}*/

	public function admin_delete($id = null) {
            $is_admin = $this->Session->read('is_admin');
            $this->loadModel('BlogComment');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $this->Blog->id = $id;
            if (!$this->Blog->exists()) {
                    throw new NotFoundException(__('Invalid Blog'));
            }
            $blog_img=$this->Blog->find('first',array('conditions'=>array('Blog.id'=>$id)));
            $blog_img_unlink = $blog_img['Blog']['image'];
            $uploadFolder = "blogs_image";
            $uploadPath = WWW_ROOT . $uploadFolder;
            if($blog_img_unlink!='' && file_exists($uploadPath . '/' . $blog_img_unlink)){
                unlink($uploadPath . '/' . $blog_img_unlink);
            }

            if ($this->Blog->delete($id)) {
                $this->BlogComment->deleteAll(array('BlogComment.blog_id' => $id), false);
                    $this->Session->setFlash(__('The blog has been deleted.'));
            } else {
                    $this->Session->setFlash(__('The blog could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'index'));
	}

	public function admin_getsubcat($id = null){
            $this->loadModel('Category');
            $options = array('conditions' => array('Category.parent_id'=>$id,'Category.active' => 1 ));
            $categories = $this->Category->find('all', $options);
            return $categories;
	}

        public function resizeImage($image,$width,$height,$scale) {

            list($imagewidth, $imageheight, $imageType) = getimagesize($image);
            $imageType = image_type_to_mime_type($imageType);
            $newImageWidth = ceil($width * $scale);
            $newImageHeight = ceil($height * $scale);
            $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
            switch($imageType) {
                    case "image/gif":
                            $source=imagecreatefromgif($image);
                            break;
                case "image/pjpeg":
                    case "image/jpeg":
                    case "image/jpg":
                            $source=imagecreatefromjpeg($image);
                            break;
                case "image/png":
                    case "image/x-png":
                            $source=imagecreatefrompng($image);
                            break;
            }
            imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

            switch($imageType) {
                    case "image/gif":
                            imagegif($newImage,$image);
                            break;
            case "image/pjpeg":
                    case "image/jpeg":
                    case "image/jpg":
                            imagejpeg($newImage,$image,90);
                            break;
                    case "image/png":
                    case "image/x-png":
                            imagepng($newImage,$image);
                            break;
            }

            chmod($image, 0777);
            return $image;
        }
}
