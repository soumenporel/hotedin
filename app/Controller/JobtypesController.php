<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class JobtypesController extends AppController {

    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Jobtype->recursive = 0;
        $this->set('categories', $this->Paginator->paginate());
    }

    public function admin_category() {
        $this->loadModel('JobCategory');
        $options = array('order' => array('JobCategory.name' => 'ASC'));
        $this->Paginator->settings = $options;
        $this->JobCategory->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('JobCategory'));
    }

    public function admin_addcategory() {
        $this->loadModel('JobCategory');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $title_for_layout = 'JobCategory Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('JobCategory.title' => $this->request->data['JobCategory']['title']));
            $name = $this->JobCategory->find('first', $options);
            if (!$name) {
                $this->JobCategory->create();
                if ($this->JobCategory->save($this->request->data)) {

                    $this->Session->setFlash(__('The country has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'category'));
                } else {
                    $this->Session->setFlash(__('The Job Category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The Job Category name already exists. Please, try again.'));
            }
        }
    }

    public function admin_deletecategory($id=null ) {
        $this->loadModel('JobCategory');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->JobCategory->id = $id;
        if (!$this->JobCategory->exists()) {
            throw new NotFoundException(__('Invalid Job Category'));
        }
        $this->request->onlyAllow('post', 'delete');


        if ($this->JobCategory->delete($id)) {
            $this->Session->setFlash(__('The Job Category has been deleted.' ,'default', array(), 'good'));
        } else {
            $this->Session->setFlash(__('The Job Category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'category'));
    }

    public function admin_editcategory($id=null) {
        $this->loadModel('JobCategory');
       $userid = $this->Session->read('adminuserid');
       $is_admin = $this->Session->read('is_admin');
       if (!isset($is_admin) && $is_admin == '') {
           $this->redirect('/admin');
       }
       if (!$this->JobCategory->exists($id)) {
           throw new NotFoundException(__('Invalid Job Category'));
       }
       if ($this->request->is(array('post', 'put'))) {
           $options = array('conditions' => array('JobCategory.title' => $this->request->data['JobCategory']['title'], 'JobCategory.id <>' => $id));
           $name = $this->JobCategory->find('first', $options);

           if (!$name) {
               if ($this->JobCategory->save($this->request->data)) {
                   $this->Session->setFlash(__('The country has been saved.'));
                   return $this->redirect(array('action' => 'category'));
               } else {
                   $this->Session->setFlash(__('The Job Category could not be saved. Please, try again.'));
               }
           } else {
               $this->Session->setFlash(__('The Job Category already exists. Please, try again.'));
           }
       } else {

           $options = array('conditions' => array('JobCategory.' . $this->JobCategory->primaryKey => $id));
           $this->request->data = $this->JobCategory->find('first', $options);

       }
    }

    public function admin_index() {
        $this->loadModel('Jobtype');
        $countries = $this->Jobtype->find('list');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Jobtype'])) {
            $Jobtype = $this->request->data['Jobtype'];
        } else {
            $Jobtype = '';
        }
        $QueryStr = '';
        if ($keywords != '') {
            $QueryStr.=" AND (Jobtype.name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (Jobtype.status = '" . $Newsearch_is_active . "')";
        }
        if ($Jobtype != '') {
            $QueryStr.=" AND (Jobtype.id=" . $Jobtype . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('Jobtype.name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Jobtype List';
        $this->Jobtype->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('Jobtype'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Jobtype'));
    }

     public function admin_add() {
        $this->loadModel('Jobtype');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $title_for_layout = 'Jobtype Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('Jobtype.name' => $this->request->data['Jobtype']['name']));
            $name = $this->Jobtype->find('first', $options);
            if (!$name) {



                //for header icon.....................................................
//                if (!empty($this->request->data['Jobtype']['imagelogo']['name'])) {
//                    $pathpart1 = pathinfo($this->request->data['Jobtype']['imagelogo']['name']);
//                    $ext1 = $pathpart1['extension'];
//                    $extensionValid1 = array('jpg', 'jpeg', 'png', 'gif', 'svg');
//                    if (in_array(strtolower($ext), $extensionValid)) {
//                        $uploadFolder1 = "img/cat_logo_img";
//                        $uploadPath1 = WWW_ROOT . $uploadFolder1;
//                        $filename1 = uniqid() . '.' . $ext1;
//                        $full_flg_path1 = $uploadPath1. '/' . $filename1;
//                        move_uploaded_file($this->request->data['Jobtype']['imagelogo']['tmp_name'], $full_flg_path1);
//
//                    } else {
//                        $this->Session->setFlash(__('Invalid image type for icon.'));
//                        return $this->redirect(array('action' => 'index'));
//                    }
//                } else {
//                    $imagelogo = 'noimage.png';
//                }


                $this->Jobtype->create();
                if ($this->Jobtype->save($this->request->data)) {

                    $this->Session->setFlash(__('The country has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The country could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The country name already exists. Please, try again.'));
            }
        }

    }

     public function admin_edit($id = null) {

         $this->loadModel('Jobtype');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if (!$this->Jobtype->exists($id)) {
            throw new NotFoundException(__('Invalid country'));
        }

        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('Jobtype.name' => $this->request->data['Jobtype']['name'], 'Jobtype.id <>' => $id));
            $name = $this->Jobtype->find('first', $options);

            if (!$name) {
                //echo "hello";exit;


                 //for header icon.....................................................
//                if (!empty($this->request->data['Jobtype']['imagelogo']['name'])) {
//                    $pathpart1 = pathinfo($this->request->data['Jobtype']['imagelogo']['name']);
//                    $ext1 = $pathpart1['extension'];
//                    $extensionValid1 = array('jpg', 'jpeg', 'png', 'gif', 'svg');
//                    if (in_array(strtolower($ext), $extensionValid)) {
//                        $uploadFolder1 = "img/cat_logo_img";
//                        $uploadPath1 = WWW_ROOT . $uploadFolder1;
//                        $filename1 = uniqid() . '.' . $ext1;
//                        $full_flg_path1 = $uploadPath1. '/' . $filename1;
//                        move_uploaded_file($this->request->data['Jobtype']['imagelogo']['tmp_name'], $full_flg_path1);
//
//                    } else {
//                        $this->Session->setFlash(__('Invalid image type for icon.'));
//                        return $this->redirect(array('action' => 'index'));
//                    }
//                } else {
//                    $imagelogo = $this->request->data['Jobtype']['hide_img1'];
//                }



                if ($this->Jobtype->save($this->request->data)) {
                    $this->Session->setFlash(__('The country has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The country could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The country already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;

            $options = array('conditions' => array('Jobtype.' . $this->Jobtype->primaryKey => $id));
            $this->request->data = $this->Jobtype->find('first', $options);

            //print_r($this->request->data);
        }

    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Jobtype->id = $id;
        if (!$this->Jobtype->exists()) {
            throw new NotFoundException(__('Invalid country'));
        }
        $this->request->onlyAllow('post', 'delete');


        if ($this->Jobtype->delete($id)) {
            $this->Session->setFlash(__('The country has been deleted.' ,'default', array(), 'good'));
        } else {
            $this->Session->setFlash(__('The country could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
