<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class SalarypersController extends AppController {
    
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Salaryper->recursive = 0;
        $this->set('categories', $this->Paginator->paginate());
    }

    public function admin_index() {
        $this->loadModel('Salaryper');
        $countries = $this->Salaryper->find('list');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Salaryper'])) {
            $Salaryper = $this->request->data['Salaryper'];
        } else {
            $Salaryper = '';
        }
        $QueryStr = '';
        if ($keywords != '') {
            $QueryStr.=" AND (Salaryper.name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (Salaryper.status = '" . $Newsearch_is_active . "')";
        }
        if ($Salaryper != '') {
            $QueryStr.=" AND (Salaryper.id=" . $Salaryper . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('Salaryper.name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Salaryper List';
        $this->Salaryper->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('Salaryper'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Salaryper'));
    }

     public function admin_add() {
        $this->loadModel('Salaryper');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $title_for_layout = 'Salaryper Add';
        if ($this->request->is('post')) {
           // $options = array('conditions' => array('Salaryper.name' => $this->request->data['Salaryper']['name']));
           // $name = $this->Salaryper->find('first', $options);
          //  if (!$name) {

              
                
                //for header icon.....................................................
//                if (!empty($this->request->data['Salaryper']['imagelogo']['name'])) {
//                    $pathpart1 = pathinfo($this->request->data['Salaryper']['imagelogo']['name']);
//                    $ext1 = $pathpart1['extension'];
//                    $extensionValid1 = array('jpg', 'jpeg', 'png', 'gif', 'svg');
//                    if (in_array(strtolower($ext), $extensionValid)) {
//                        $uploadFolder1 = "img/cat_logo_img";
//                        $uploadPath1 = WWW_ROOT . $uploadFolder1;
//                        $filename1 = uniqid() . '.' . $ext1;
//                        $full_flg_path1 = $uploadPath1. '/' . $filename1;
//                        move_uploaded_file($this->request->data['Salaryper']['imagelogo']['tmp_name'], $full_flg_path1);
//                       
//                    } else {
//                        $this->Session->setFlash(__('Invalid image type for icon.'));
//                        return $this->redirect(array('action' => 'index'));
//                    }
//                } else {
//                    $imagelogo = 'noimage.png';
//                }
                
               
                $this->Salaryper->create();
                if ($this->Salaryper->save($this->request->data)) {
                  
                    $this->Session->setFlash(__('The job has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The job could not be saved. Please, try again.'));
                }
//            } else {
//                $this->Session->setFlash(__('The job name already exists. Please, try again.'));
//            }
        }
       
    }
    
     public function admin_edit($id = null) {
         
         $this->loadModel('Salaryper');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        if (!$this->Salaryper->exists($id)) {
            throw new NotFoundException(__('Invalid country'));
        }
       
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('Salaryper.name' => $this->request->data['Salaryper']['name'], 'Salaryper.id <>' => $id));
            $name = $this->Salaryper->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

            
                 //for header icon.....................................................
//                if (!empty($this->request->data['Salaryper']['imagelogo']['name'])) {
//                    $pathpart1 = pathinfo($this->request->data['Salaryper']['imagelogo']['name']);
//                    $ext1 = $pathpart1['extension'];
//                    $extensionValid1 = array('jpg', 'jpeg', 'png', 'gif', 'svg');
//                    if (in_array(strtolower($ext), $extensionValid)) {
//                        $uploadFolder1 = "img/cat_logo_img";
//                        $uploadPath1 = WWW_ROOT . $uploadFolder1;
//                        $filename1 = uniqid() . '.' . $ext1;
//                        $full_flg_path1 = $uploadPath1. '/' . $filename1;
//                        move_uploaded_file($this->request->data['Salaryper']['imagelogo']['tmp_name'], $full_flg_path1);
//                       
//                    } else {
//                        $this->Session->setFlash(__('Invalid image type for icon.'));
//                        return $this->redirect(array('action' => 'index'));
//                    }
//                } else {
//                    $imagelogo = $this->request->data['Salaryper']['hide_img1'];
//                }
                
             
               
                if ($this->Salaryper->save($this->request->data)) {
                    $this->Session->setFlash(__('The job has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The job could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The job already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
           
            $options = array('conditions' => array('Salaryper.' . $this->Salaryper->primaryKey => $id));
            $this->request->data = $this->Salaryper->find('first', $options);

            //print_r($this->request->data);
        }
       
    }
    
    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Salaryper->id = $id;
        if (!$this->Salaryper->exists()) {
            throw new NotFoundException(__('Invalid country'));
        }
        $this->request->onlyAllow('post', 'delete');
       
 
        if ($this->Salaryper->delete($id)) {
            $this->Session->setFlash(__('The job has been deleted.' ,'default', array(), 'good'));
        } else {
            $this->Session->setFlash(__('The job could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}