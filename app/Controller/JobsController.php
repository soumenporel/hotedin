<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class JobsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Jobs';

    public $data2='' ;
    public $components = array('Paginator', 'Session');
    var $uses = array('User', 'Country', 'State', 'City', 'PostImage', 'Setting', 'Post', 'Chat', 'Skill','Lecture','CmsPage','Salaryper','Jobtype');

    /**
     * index method
     *
     * @return void
     */
    public function addnewpost() {
        $this->layout = false;
        //pr($_POST); exit;
        $userid = $this->Session->read('user_id');
        $this->request->data['Post']['user_id'] = $userid;
        $this->request->data['Post']['category_id'] = $_POST['category_id'];
        $this->request->data['Post']['subcategory_id'] = $_POST['subcategory_id'];
        $this->request->data['Post']['post_title'] = $_POST['post_title'];
        $this->request->data['Post']['location'] = $_POST['location'];
        $this->request->data['Post']['city'] = $_POST['city'];
        $this->request->data['Post']['state'] = $_POST['state'];
        $this->request->data['Post']['address'] = $_POST['address'];
        $this->request->data['Post']['country'] = $_POST['country'];
        $this->request->data['Post']['zip_code'] = $_POST['zip_code'];
        $this->request->data['Post']['post_description'] = $_POST['post_description'];
        $this->request->data['Post']['post_date'] = date('Y-m-d h:m:s');
        $this->request->data['Post']['is_approve'] = 0;
        $this->Post->create();
        if ($this->Post->save($this->request->data)) {
            $this->Session->write('lastPostId', $this->Post->getInsertID());
            echo $this->Post->getInsertID();
        } else {
            echo 0;
        }
        exit;
        $this->autoRender = false;
    }

    public function addnewoffer() {
        $this->layout = false;
        //pr($_POST); exit;
        //$userid = $this->Session->read('user_id');

        if ($this->Session->check('LastPostId')) {

            $LastPostId = $this->Session->read('LastPostId');
        } else {
            $LastPostId = $this->Session->read('getoffer_editid');
        }
        //$this->request->data['Post']['user_id'] = $userid;
        $category_id = $this->request->data['Post']['category_id'] = $_POST['category_id'];
        $subcategory_id = $this->request->data['Post']['subcategory_id'] = $_POST['subcategory_id'];
        $title = $this->request->data['Post']['post_title'] = $_POST['post_title'];
        $location = $this->request->data['Post']['location'] = $_POST['location'];
        $city = $this->request->data['Post']['city'] = $_POST['city'];
        $state = $this->request->data['Post']['state'] = $_POST['state'];
        $address = $this->request->data['Post']['address'] = $_POST['address'];
        $country = $this->request->data['Post']['country'] = $_POST['country'];
        $zip_code = $this->request->data['Post']['zip_code'] = $_POST['zip_code'];
        $description = $this->request->data['Post']['post_description'] = $_POST['post_description'];
        $date = $this->request->data['Post']['post_date'] = gmdate('Y-m-d h:i:s');
        $type = $this->request->data['Post']['type'] = $_POST['type'];


        $update = $this->Post->updateAll(array('Post.category_id' => "'$category_id'", 'Post.subcategory_id' => "'$subcategory_id'", 'Post.post_title' => "'$title'", 'Post.location' => "'$location'", 'Post.city' => "'$city'", 'Post.state' => "'$state'", 'Post.address' => "'$address'", 'Post.country' => "'$country'", 'Post.zip_code' => "'$zip_code'", 'Post.post_description' => "'$description'", 'Post.post_date' => "'$date'", 'Post.type' => "'$type'"), array('Post.id' => $LastPostId));
        if ($update) {
            echo $LastPostId;
        } else {
            echo 0;
        }

        //$this->request->data['Post']['post_id'] = $_POST['post_id'];
        //$this->request->data['Post']['is_approve'] = 0;
        //$this->Post->create();
        //pr($this->request->data);exit;
        /* if ($this->Post->save($this->request->data)){
          $this->Session->write('lastPostId', $this->Post->getInsertID());
          echo $this->Post->getInsertID();
          } else {
          echo 0;
          } */

        exit;
        $this->autoRender = false;
    }

    public function getsubcat() {

        $id = $_REQUEST['id'];
        $this->loadModel('Category');
        $options_subcat = array('conditions' => array('Category.status' => 1, 'Category.parent_id' => $id), 'order' => array('Category.category_name' => 'asc'), 'fields' => array('Category.id', 'Category.category_name'));
        $subcatg = $this->Category->find('list', $options_subcat);
        //$this->set('selectbox',$subcatg);
        //$subcategory="";

        echo '<option>--Select a SubCategory (Required)--</option>';

        foreach ($subcatg as $key => $subcat) {
            echo '<option value="' . $key . '" >' . $subcat . '</option>';
        }

        exit;
    }

    public function ajaximage() {
        $this->layout = false;
        $this->loadModel('PostImage');
        //$uploadFolder = "postimg/";
        $uploadFolder = "img/post_img/";
        $path = WWW_ROOT . $uploadFolder;

        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "Jpg", "Png", "Gif", "Bmp", "Jpeg");
        if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

            //pr($_POST); pr($_FILES); exit;
            if (isset($_FILES['photoimg1']) && !empty($_FILES['photoimg1'])) {
                $_FILES['photoimg'] = $_FILES['photoimg1'];
            } else if (isset($_FILES['photoimg2']) && !empty($_FILES['photoimg2'])) {
                $_FILES['photoimg'] = $_FILES['photoimg2'];
            } else if (isset($_FILES['photoimg3']) && !empty($_FILES['photoimg3'])) {
                $_FILES['photoimg'] = $_FILES['photoimg3'];
            } else if (isset($_FILES['photoimg4']) && !empty($_FILES['photoimg4'])) {
                $_FILES['photoimg'] = $_FILES['photoimg4'];
            } else if (isset($_FILES['photoimg5']) && !empty($_FILES['photoimg5'])) {
                $_FILES['photoimg'] = $_FILES['photoimg5'];
            }
            //pr($_FILES); exit;
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];
            if (strlen($name)) {
                list($txt, $ext) = explode(".", $name);
                if (in_array($ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $actual_image_name = time() . substr(str_replace(" ", "_", $txt), 5) . "." . $ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        //echo $path . $actual_image_name; exit;
                        if (move_uploaded_file($tmp, $path . $actual_image_name)) {

                            $postId = $this->Session->read('lastPostId');
                            //mysqli_query($db, "UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
                            $this->request->data['post_id'] = $postId;
                            $this->request->data['originalpath'] = $actual_image_name;
                            $this->request->data['resizepath'] = $actual_image_name;

                            $this->PostImage->create();
                            if ($this->PostImage->save($this->request->data)) {
                                echo "<img src=" . $this->webroot . "img/post_img/" . $actual_image_name . "  class='preview'>";
                            }
                        } else
                            echo "failed";
                    } else
                        echo "Image file size max 1 MB";
                } else
                    echo "Invalid file format..";
            } else
                echo "Please select image..!";

            exit;
        }
        $this->autoRender = false;
    }

    public function ajaximage_offer() {
        //echo "hello";exit;
        $this->layout = false;
        $this->loadModel('PostImage');
        //$uploadFolder = "postimg/";
        $uploadFolder = "img/post_img/";
        $path = WWW_ROOT . $uploadFolder;

        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "Jpg", "Png", "Gif", "Bmp", "Jpeg");
        if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

            //pr($_POST); pr($_FILES); exit;
            if (isset($_FILES['photoimg_1']) && !empty($_FILES['photoimg_1'])) {
                $_FILES['photoimg'] = $_FILES['photoimg_1'];
            } else if (isset($_FILES['photoimg_2']) && !empty($_FILES['photoimg_2'])) {
                $_FILES['photoimg'] = $_FILES['photoimg_2'];
            } else if (isset($_FILES['photoimg_3']) && !empty($_FILES['photoimg_3'])) {
                $_FILES['photoimg'] = $_FILES['photoimg_3'];
            } else if (isset($_FILES['photoimg_4']) && !empty($_FILES['photoimg_4'])) {
                $_FILES['photoimg'] = $_FILES['photoimg_4'];
            } else if (isset($_FILES['photoimg_5']) && !empty($_FILES['photoimg_5'])) {
                $_FILES['photoimg'] = $_FILES['photoimg_5'];
            }
            //pr($_FILES['photoimg']);
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];

            //echo $name; exit;
            if (strlen($name)) {
                list($txt, $ext) = explode(".", $name);
                if (in_array($ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $actual_image_name = time() . substr(str_replace(" ", "_", $txt), 5) . "." . $ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        //echo $path . $actual_image_name; exit;
                        if (move_uploaded_file($tmp, $path . $actual_image_name)) {

                            if ($this->Session->check('LastPostId')) {

                                $postId = $this->Session->read('LastPostId');
                            } else {
                                $postId = $this->Session->read('getoffer_editid');
                            }

                            $postId = $this->Session->read('LastPostId');
                            //mysqli_query($db, "UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
                            $this->request->data['post_id'] = $postId;
                            $this->request->data['originalpath'] = $actual_image_name;
                            $this->request->data['resizepath'] = $actual_image_name;

                            //pr($this->request->data);

                            $this->PostImage->create();
                            if ($this->PostImage->save($this->request->data)) {
                                echo "<img src=" . $this->webroot . "img/post_img/" . $actual_image_name . "  class='preview'>";
                            }
                        } else
                            echo "failed";
                    } else
                        echo "Image file size max 1 MB";
                } else
                    echo "Invalid file format..";
            } else
                echo "Please select image..!";

            exit;
        }
        $this->autoRender = false;
    }

    public function addnewpostbudget_offer() {

        $this->layout = false;
        //pr($_POST);
        $userid = $this->Session->read('user_id');
        if ($this->Session->check('LastPostId')) {

            $LastPostId = $this->Session->read('LastPostId');
        } else {
            $LastPostId = $this->Session->read('getoffer_editid');
        }
        //echo $userid;
        $price = $_POST['budget'];
        $price_condition = $_POST['price_condition'];
        $product_condition = $_POST['product_condition'];
        $is_complete = 1;
        //pr($pid); exit;
        //$this->Post->id = $this->Session->read('lastPostId');
        //$pid['Post']['id'] = $this->Session->read('lastPostId');
        /* if ($this->Post->save($pid)){
          $this->Session->delete('lastPostId');
          $this->Session->setFlash(__('Post Added Successfully.'));
          echo 1;
          } else {
          echo 0;
          } */
        $update = $this->Post->updateAll(array('Post.price' => "'$price'", 'Post.price_condition' => "'$price_condition'", 'Post.product_condition' => "'$product_condition'", 'Post.is_complete' => "'$is_complete'"), array('Post.id' => $LastPostId));
        if ($update) {
            echo 1;

            if ($this->Session->check('post_owner') || $this->Session->check('notification_to_id') || $this->Session->check('post_title')) {



                $to_id = $this->Session->read('post_owner');
                $noti_post_id = $this->Session->read('notification_to_id');
                $LastPostId;
                $from_id = $userid;
                $post_title = $this->Session->read('post_title');
                $date = gmdate('Y-m-d h:i:s');
                $message = "Have posted a new offer on your post " . $post_title;
                $type = "offer";
                $is_read = 0;
                $link = Configure::read('SITE_URL') . 'posts/offer_details/' . $LastPostId;

                $this->request->data['from_id'] = $from_id;
                $this->request->data['to_id'] = $to_id;
                $this->request->data['post_id'] = $noti_post_id;
                $this->request->data['offer_id'] = $LastPostId;
                $this->request->data['date'] = $date;
                $this->request->data['message'] = $message;
                $this->request->data['link'] = $link;
                $this->request->data['is_read'] = $is_read;
                $this->request->data['type'] = $type;

                $this->loadModel('OfferNotification');
                $this->OfferNotification->recursive = 2;
                $this->OfferNotification->create();
                if ($this->OfferNotification->save($this->request->data)) {
                    $this->Session->delete('LastPostId');
                    $this->Session->delete('post_owner');
                    $this->Session->delete('notification_to_id');
                    $this->Session->delete('post_title');
                }
            }
        } else {
            echo 0;
        }
        exit;
        $this->autoRender = false;
    }

    public function addnewpostbudget() {
        $this->layout = false;
        //pr($_POST);
        $userid = $this->Session->read('lastPostId');
        //echo $userid;
        $pid['Post']['price'] = $_POST['budget'];
        $pid['Post']['price_condition'] = $_POST['price_condition'];
        $pid['Post']['product_condition'] = $_POST['product_condition'];
        //pr($pid); exit;
        //$this->Post->id = $this->Session->read('lastPostId');
        $pid['Post']['id'] = $this->Session->read('lastPostId');
        if ($this->Post->save($pid)) {
            echo $this->Session->read('lastPostId');
            $this->Session->delete('lastPostId');
            $this->Session->setFlash(__('Post Added Successfully.'));
            //echo 1;
        } else {
            echo 0;
        }
        exit;
        $this->autoRender = false;
    }

    public function editoldpost() {
        $this->layout = false;
        //pr($_POST); exit;
        $userid = $this->Session->read('user_id');

        $this->request->data['Post']['category_id'] = $_POST['category_id'];
        $this->request->data['Post']['subcategory_id'] = $_POST['subcategory_id'];
        $this->request->data['Post']['post_title'] = $_POST['post_title'];
        $this->request->data['Post']['location'] = $_POST['location'];
        $this->request->data['Post']['city'] = $_POST['city'];
        $this->request->data['Post']['state'] = $_POST['state'];
        $this->request->data['Post']['address'] = $_POST['address'];
        $this->request->data['Post']['country'] = $_POST['country'];
        $this->request->data['Post']['zip_code'] = $_POST['zip_code'];
        $this->request->data['Post']['post_description'] = $_POST['post_description'];
        $this->Post->id = $_POST['id'];
        if ($this->Post->save($this->request->data)) {
            $this->Session->write('lastPostId', $this->Post->getInsertID());
            echo 1;
        } else {
            echo 0;
        }
        exit;
        $this->autoRender = false;
    }

    public function ajaximageedit() {

        $this->layout = false;
        $this->loadModel('PostImage');
        //$uploadFolder = "postimg/";
        $uploadFolder = "img/post_img/";
        $path = WWW_ROOT . $uploadFolder;

        $valid_formats = array("jpg", "png", "gif", "bmp");
        if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

            //pr($_POST); pr($_FILES); exit;
            if (isset($_FILES['ephotoimg1']) && !empty($_FILES['ephotoimg1'])) {
                $_FILES['photoimg'] = $_FILES['ephotoimg1'];
            } else if (isset($_FILES['ephotoimg2']) && !empty($_FILES['ephotoimg2'])) {
                $_FILES['photoimg'] = $_FILES['ephotoimg2'];
            } else if (isset($_FILES['ephotoimg3']) && !empty($_FILES['ephotoimg3'])) {
                $_FILES['photoimg'] = $_FILES['ephotoimg3'];
            } else if (isset($_FILES['ephotoimg4']) && !empty($_FILES['ephotoimg4'])) {
                $_FILES['photoimg'] = $_FILES['ephotoimg4'];
            } else if (isset($_FILES['ephotoimg5']) && !empty($_FILES['ephotoimg5'])) {
                $_FILES['photoimg'] = $_FILES['ephotoimg5'];
            }
            //pr($_FILES); exit;
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];
            if (strlen($name)) {
                list($txt, $ext) = explode(".", $name);
                if (in_array($ext, $valid_formats)) {
                    if ($size < (1024 * 1024)) {
                        $actual_image_name = time() . substr(str_replace(" ", "_", $txt), 5) . "." . $ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        //echo $path . $actual_image_name; exit;
                        if (move_uploaded_file($tmp, $path . $actual_image_name)) {

                            $postId = $this->Session->read('lastPostId');
                            //mysqli_query($db, "UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
                            $this->request->data['post_id'] = $postId;
                            $this->request->data['originalpath'] = $actual_image_name;
                            $this->request->data['resizepath'] = $actual_image_name;

                            $this->PostImage->create();
                            if ($this->PostImage->save($this->request->data)) {
                                echo "<img src=" . $this->webroot . "img/post_img/" . $actual_image_name . "  class='preview'>";
                            }
                        } else
                            echo "failed";
                    } else
                        echo "Image file size max 1 MB";
                } else
                    echo "Invalid file format..";
            } else
                echo "Please select image..!";

            exit;
        }
        $this->autoRender = false;



        /*
          $this->layout = false;
          $this->loadModel('PostImage');
          $uploadFolder = "postimg/";
          $path = WWW_ROOT . $uploadFolder;

          $valid_formats = array("jpg", "png", "gif", "bmp");
          if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

          //pr($this->request->data); pr($_FILES); exit;

          if(isset($_FILES['photoimg1']) && !empty($_FILES['photoimg1'])){
          $_FILES['photoimg'] = $_FILES['photoimg1'];
          $oldPostImgId = $_POST['epimgid1'];
          $oldpid = $_POST['epostids1'];
          } else if(isset($_FILES['photoimg2']) && !empty($_FILES['photoimg2'])){
          $_FILES['photoimg'] = $_FILES['photoimg2'];
          $oldPostImgId = $_POST['epimgid2'];
          $oldpid = $_POST['epostids2'];
          } else if(isset($_FILES['photoimg3']) && !empty($_FILES['photoimg3'])){
          $_FILES['photoimg'] = $_FILES['photoimg3'];
          $oldPostImgId = $_POST['epimgid3'];
          $oldpid = $_POST['epostids3'];
          } else if(isset($_FILES['photoimg4']) && !empty($_FILES['photoimg4'])){
          $_FILES['photoimg'] = $_FILES['photoimg4'];
          $oldPostImgId = $_POST['epimgid4'];
          $oldpid = $_POST['epostids4'];
          } else if(isset($_FILES['photoimg5']) && !empty($_FILES['photoimg5'])){
          $_FILES['photoimg'] = $_FILES['photoimg5'];
          $oldPostImgId = $_POST['epimgid5'];
          $oldpid = $_POST['epostids5'];
          }
          //pr($_FILES); exit;
          $name = $_FILES['photoimg']['name'];
          $size = $_FILES['photoimg']['size'];
          if (strlen($name)) {
          list($txt, $ext) = explode(".", $name);
          if (in_array($ext, $valid_formats)) {
          if ($size < (1024 * 1024)) {
          $actual_image_name = time() . substr(str_replace(" ", "_", $txt), 5) . "." . $ext;
          $tmp = $_FILES['photoimg']['tmp_name'];
          //echo $path . $actual_image_name; exit;
          if (move_uploaded_file($tmp, $path . $actual_image_name)) {
          if($oldPostImgId != ""){
          $this->PostImage->id = $oldPostImgId; $this->PostImage->delete();
          }

          //mysqli_query($db, "UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
          $this->request->data['post_id'] = $oldpid;
          $this->request->data['originalpath'] = $actual_image_name;
          $this->request->data['resizepath'] = $actual_image_name;

          $this->PostImage->create();
          if ($this->PostImage->save($this->request->data)){
          echo "<img src=". $this->webroot ."postimg/" . $actual_image_name . "  class='preview'>";
          }

          } else
          echo "failed";
          } else
          echo "Image file size max 1 MB";
          } else
          echo "Invalid file format..";
          } else
          echo "Please select image..!";

          exit;
          }
          $this->autoRender=false;

         */
    }

    public function deletePostImage() {
        $this->layout = false;

        //$options = array('conditions' => array('PostImage.id'  => $_POST['id']));
        //$img = $this->PostImage->find('first', $options);

        $this->PostImage->recursive = -1;
        $this->request->data = $this->PostImage->read(null, $_POST['id']);

        //pr($this->request->data); pr($_POST);exit;
        //pr($this->request->data); exit;
        if ($this->PostImage->delete($_POST['id'])) {
            //if($this->request->data['PostImage']['resizepath'] !=""){
            //        unlink(USER_IMAGES . $this->request->data['User']['photo']);
            //}
            echo 1;
        } else {
            echo 0;
        }

        $this->autoRender = false;
    }

    public function curriculum($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }



        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$post_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        $course_status = $this->course_status($post_id);

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$post_id),'order'=>'Lesson.sortby Asc'));

        $this->set(compact('post_dtls','lessons','course_status'));
    }

    public function curriculum_test($slug = NULL) {
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }



        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$post_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        $course_status = $this->course_status($post_id);

        $this->loadModel('Lesson');
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$post_id),'order'=>'Lesson.sortby Asc'));

         $this->CmsPage->recursive = 2;

        $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 60)));
        $this->set(compact('post_dtls','lessons','course_status','topContent'));
    }

    public function getAllCurriculum($post_id = NULL) {
        $this->autoRender = false;
        $this->loadModel('Lesson');

        $lessons = $this->Lesson->find('all',array(
            'recursive' => 2,
            'conditions' => array(
                'Lesson.post_id' => $post_id
            ),
            //'order' => 'Lesson.sortby ASC'
        ));
        //pr($lessons); exit();
        if(!empty($lessons)){
            $lessonsArray = array();
            foreach ($lessons as $lesson) {
                $lectureArray = array();

                foreach ($lesson['Lecture'] as $key => $value) {
                    if(!empty($value['Question'])) {
                        foreach ($value['Question'] as $qvalue) {
                            $QuestionArray[] = array(
                                'id' => $qvalue['id'],
                                'question' => $qvalue['question'],
                                'options' => json_decode($qvalue['options']),
                                'answer' => $qvalue['answer'],
                                'Qtype' => $qvalue['Qtype']
                            );
                        }
                        $haveQuestion = 1;
                    } else {
                        $QuestionArray = array();
                        $haveQuestion = '';
                    }

                    if (!empty($value['DownloadableFile'])) {
                        foreach ($value['DownloadableFile'] as $dvalue) {
                            $downloadableArray[] = $dvalue;
                        }
                        $haveDownloadableFile = 1;
                    } else {
                        $downloadableArray = array();
                        $haveDownloadableFile = 0;
                    }

                    if (!empty($value['ExternalLink'])) {
                        foreach ($value['ExternalLink'] as $evalue) {
                            $externalLinkArray[] = $evalue;
                        }
                        $haveExternalLink = 1;
                    } else {
                        $externalLinkArray = array();
                        $haveExternalLink = 0;
                    }

                    if (!empty($value['SourceCode'])) {
                        foreach ($value['SourceCode'] as $svalue) {
                            $sourcecodeArray[] = $svalue;
                        }
                        $haveSourceCode = 1;
                    } else {
                        $sourcecodeArray = array();
                        $haveSourceCode = 0;
                    }
                      //print_r($value);die;
                    $type = $value['type'] == '1' ? 'Quiz' : 'Lecture';
                    $lectureArray[] = array(
                        'lectureID' => $value['id'],
                        'title' => $value['title'],
                        'description' => $value['description'],
                        'media' => $value['media'],
                        'type' => $type,
                        'questions' => $QuestionArray,
                        'haveQuestion' => $haveQuestion,
                        'downloadablefile' => $downloadableArray,
                        'haveDownloadableFile' => $haveDownloadableFile,
                        'externallink' => $externalLinkArray,
                        'haveExternalLink' => $haveExternalLink,
                        'sourcecodefile' => $sourcecodeArray,
                        'haveSourceCodeFile' => $haveSourceCode
                    );

                }

                $lessonsArray[] = array(
                    'lessonID' => $lesson['Lesson']['id'],
                    'title' => $lesson['Lesson']['title'],
                    'description' => $lesson['Lesson']['description'],
                    'lectures' => $lectureArray
                );
            }
            $data['lessonsArray'] = $lessonsArray;
            $data['ack'] = 1;
        } else {
            $data['ack'] = 0;
        }
        echo json_encode($data);
    }

    public function test_video($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $course_id = $post_dtls['Post']['id'];

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$course_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        if(isset($this->request->data) && !empty($this->request->data)){


            $saveType = '';
            if(isset($this->request->data['save-next']) && $this->request->data['save-next']=='Save & Next' ){
                $saveType = 'save-next';
            }

            if (!empty($this->request->data['video']['name'])) {
                $pathpart = pathinfo($this->request->data['video']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('mp4','vtt','flv','mkv','mov');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "test_video";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['video']['tmp_name'], $full_flg_path);


                    if(isset($this->request->data['last_video']) && $this->request->data['last_video']!=''){
                        unlink($uploadPath.'/'.$this->request->data['last_video']);
                    }

                    $data['TestVideo']['video'] = $filename;
                    $data['TestVideo']['microphone'] = $this->request->data['microphone'];
                    $data['TestVideo']['camera'] = $this->request->data['camera'];
                    $data['TestVideo']['lighting'] = $this->request->data['lighting'];
                    $data['TestVideo']['editing_software'] = $this->request->data['editing_software'];
                    $data['TestVideo']['post_id'] = $post_id;

                    if(isset($this->request->data['id']) && !empty($this->request->data['id'])){
                        $data['TestVideo']['id'] = $this->request->data['id'];
                    }

                    $this->loadModel('TestVideo');
                    $this->TestVideo->id = $post_dtls['Post']['id'];
                    $this->TestVideo->save($data);

                    if($saveType == 'save-next'){
                        return $this->redirect(array('action' => "course_landing_page",$post_dtls['Post']['slug']));
                    }

                    $this->Session->setFlash('Test Video Saved Successful.', 'default', array('class' => 'success'));

                } else {

                    $this->Session->setFlash(__('Invalid image type.'));
                    return $this->redirect(array('action' => "test_video",$post_dtls['Post']['slug']));
                }
            } else {

                $filename = '';
            }
        }

        $course_status = $this->course_status($post_id);
        $this->loadModel('TestVideo');
        $test_video = $this->TestVideo->find('all',array('conditions'=>array('TestVideo.post_id'=>$post_id)));

        $TestVideo_condition = $this->TestVideo->find('count',array(
            'conditions' => array(
                'TestVideo.post_id' => $post_id,
                'TestVideo.status' => 0
            )));
        $testVideo = $this->TestVideo->find('first',array(
            'conditions' => array(
                'TestVideo.post_id' => $post_id,
            )));
         $this->loadModel('CmsPage');
        $this->CmsPage->recursive = 2;

        $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 59)));
        $this->set(compact('post_dtls','test_video','course_status','TestVideo_condition','testVideo','topContent'));

    }

    public function automatic_message($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$post_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        if(isset($this->request->data) && !empty($this->request->data)){
            $data = json_encode($this->request->data);
            $this->Post->id = $post_dtls['Post']['id'];
            $this->Post->saveField('automatic_message',$data);
            $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        }

        $course_status = $this->course_status($post_id);

        $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 62)));

        $this->set(compact('post_dtls','course_status','topContent'));

    }

    public function price_coupons($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$post_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }


        if(isset($this->request->data) && !empty($this->request->data)){
           $data['Post']['currency_type'] = $this->request->data['currency_type'];
           $data['Post']['price'] = $this->request->data['price'];
           $this->Post->id = $post_dtls['Post']['id'];
           $this->Post->save($data);
           $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        }

        $course_status = $this->course_status($post_id);

        $this->set(compact('post_dtls','course_status'));

    }

    public function editoldpostbudget() {
        $this->layout = false;
        //pr($_POST);
        $userid = $this->Session->read('lastPostId');
        //echo $userid;
        $pid['Post']['id'] = $_POST['id'];
        $pid['Post']['price'] = $_POST['budget'];
        $pid['Post']['price_condition'] = $_POST['price_condition'];
        $pid['Post']['product_condition'] = $_POST['product_condition'];
        //pr($pid); exit;
        $this->Post->id = $_POST['id'];
        if ($this->Post->save($pid)) {
            $this->Session->delete('lastPostId');
            $this->Session->setFlash(__('Post Updated Successfully.'));
            echo 1;
        } else {
            echo 0;
        }
        exit;
        $this->autoRender = false;
    }

    public function fetchpostdata() {

        $this->layout = false;

        $options = array('conditions' => array('Post.id' => $_POST['pid']));
        $post = $this->Post->find('first', $options);
        //pr($post);exit;
        $this->Session->write('lastPostId', $_POST['pid']);



        echo json_encode($post);
        exit;
        $this->autoRender = false;
    }

    public function fetchaddofferdata() {

        $this->layout = false;

        $options = array('conditions' => array('Post.id' => $_POST['pid']));
        $post = $this->Post->find('first', $options);
        //pr($post);exit;
        //$this->Session->write('lastPostId', $_POST['pid']);



        echo json_encode($post);
        exit;
        $this->autoRender = false;
    }

    public function fetchofferdata() {

        $this->layout = false;
        //$this->Post->recursive=2;

        $options = array('conditions' => array('Post.id' => $_POST['pid']));
        $post = $this->Post->find('first', $options);
        //pr($post);exit;

        echo json_encode($post);
        exit;
        $this->autoRender = false;
    }

    /*
      public function admin_uploadUser($id = null)

      {

      $this->autoRender=false;

      $this->loadModel('Multimageupload');

      $imagename = $_FILES['file']['name'];

      $uploadPath= Configure::read('UPLOAD_USER_IMG_PATH');

      //echo $imagename = $_FILES['file']['profile_image']['name'];

      $options = array('conditions' => array('User.id' => $id));

      $user = $this->User->find('first', $options);

      if(!empty($user)){

      $this->Multimageupload->create();



      move_uploaded_file($_FILES['file']['tmp_name'], $uploadPath.'/'.(date('Ymd_his').'__'.$imagename));

      $imageName1 = date('Ymd_his').'__'.$imagename;

      $userupdate['Multimageupload']['user_id']=$id;

      $userupdate['Multimageupload']['image_upload']=$imageName1;

      $this->Multimageupload->save($userupdate);

      }

     */

    public function index() {
        $this->Post->recursive = 0;
        $this->set('posts', $this->Paginator->paginate());
    }

    public function admin_index() {
        $conditions = array();
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $users = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.is_admin' => '0')));

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['user'])) {
            $User = $this->request->data['user'];
        } else {
            $User = '';
        }
        //$QueryStr = '1';
        if ($keywords != '') {
            $conditions['Post.post_title LIKE'] = '%'.$keywords.'%';
        }
        if ($Newsearch_is_active != '') {
            $conditions['Post.is_approve'] = $Newsearch_is_active;
        }
        if ($User != '') {
            $conditions['Post.user_id'] = $User;
        }
        $options = array('conditions' => $conditions, 'order' => array('Post.id' => 'desc'), 'group' => 'Post.id');
        $this->Paginator->settings = $options;
        $title_for_layout = 'Post List';
        $this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Post'));
        $this->set(compact('title_for_layout', 'keywords', 'Newsearch_is_active', 'users', 'User'));
    }
    public function admin_coursestatus($status = NULL){
        $is_approve = $status;
        $conditions = array();
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $users = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.is_admin' => '0')));

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['user'])) {
            $User = $this->request->data['user'];
        } else {
            $User = '';
        }
        //$QueryStr = '1';
        if ($keywords != '') {
            $conditions['Post.post_title LIKE'] = '%'.$keywords.'%';
        }
        if ($Newsearch_is_active != '') {
            $conditions['Post.is_approve'] = $Newsearch_is_active;
        }
        if ($User != '') {
            $conditions['Post.user_id'] = $User;
        }
        $conditions['Post.is_approve'] = $is_approve;
        $options = array('conditions' => $conditions, 'order' => array('Post.id' => 'desc'), 'group' => 'Post.id');
        $this->Paginator->settings = $options;
        $title_for_layout = 'Post List';
        $this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Post'));
        $this->set(compact('title_for_layout', 'keywords', 'Newsearch_is_active', 'users', 'User'));

    }

public function admin_coursebycat($catid=NULL) {
        $conditions = array();
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $users = $this->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.is_admin' => '0')));

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['user'])) {
            $User = $this->request->data['user'];
        } else {
            $User = '';
        }
        //$QueryStr = '1';
        if ($keywords != '') {
            $conditions['Post.post_title LIKE'] = '%'.$keywords.'%';
        }
        if ($Newsearch_is_active != '') {
            $conditions['Post.is_approve'] = $Newsearch_is_active;
        }
        if ($User != '') {
            $conditions['Post.user_id'] = $User;
        }
        $conditions['Post.category_id'] = $catid;
        $options = array('conditions' => $conditions, 'order' => array('Post.id' => 'desc'));
        $this->Paginator->settings = $options;
        $title_for_layout = 'Post List';
        $this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Post'));
        $this->set(compact('title_for_layout', 'keywords', 'Newsearch_is_active', 'users', 'User'));
    }

    public function admin_subposts($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Sub Post List';
        //$this->Post->recursive = 0;
        $this->set('posts', $this->Paginator->paginate('Post', array('Post.id' => $id)));
        $this->set(compact('title_for_layout', 'id'));
    }

    public function admin_exportsub($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $posts = $this->Post->find('all');

        $output = '';
        $output .='Name, Status';
        $output .="\n";

        if (!empty($posts)) {
            foreach ($posts as $category) {
                $isactive = ($category['Post']['active'] == 1 ? 'Active' : 'Inactive');

                $output .='"' . $category['Post']['name'] . '","' . $isactive . '"';
                $output .="\n";
            }
        }
        $filename = "posts" . time() . ".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->Post->exists($id)) {
            throw new NotFoundException(__('Invalid Post'));
        }
        $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
        $this->set('category', $this->Post->find('first', $options));
    }

    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Post View';
        if (!$this->Post->exists($id)) {
            throw new NotFoundException(__('Invalid Post'));
        }
        $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
        $this->Post->recursive = 2;
        $post = $this->Post->find('first', $options);
        $post_id = $post['Post']['id'];
        $this->loadModel('Lesson');
        $this->Paginator->settings = array(
            'conditions' => array(
                'Lesson.post_id' => $post_id
            ),
            'limit' => 10
        );

        $lesson = $this->Paginator->paginate('Lesson');

        //for video count........
        $this->loadModel('ReadLecture');
        $this->loadModel('UserCourse');
        $freeVideo=0;
        $options_video = array('conditions' => array('ReadLecture.post_id'=> $id),'group' => 'ReadLecture.user_id','recursive'=>1);
        $read_videos = $this->ReadLecture->find('all', $options_video);
        $totalView=count($read_videos);
        $freevideoView=0;
        /*if($totalView>0)
        {
        foreach($read_videos as $key=>$val)
        {
           if($val['User']['membership_plan_id']=='NULL'||$val['User']['membership_plan_id']==7)
           {
              $freevideoView=$freevideoView+1;
           }
        }
        }*/

        $options_student = array('conditions' => array('UserCourse.post_id'=> $id,),'group' => 'UserCourse.user_id','recursive'=>-1);
        $students = $this->UserCourse->find('all', $options_student);
        $totalStudents=count($students);

        $this->set(compact('title_for_layout', 'post','lesson','totalView','freevideoView','totalStudents'));
    }



    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if ($this->request->is('post')) {
            $this->Post->create();
            if ($this->Post->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->Post->User->find('list');
        $this->set(compact('users'));
    }

    // public function admin_add($id = NULL) {
    //     $user_id = base64_decode($id);
    //     $this->loadModel('PostImage');

    //     $userid = $this->Session->read('adminuserid');
    //     $is_admin = $this->Session->read('is_admin');
    //     $this->request->data1 = array();
    //     if (!isset($is_admin) && $is_admin == '') {
    //         $this->redirect('/admin');
    //     }
    //     $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.admin_type !=' => '0','User.is_admin !=' => '1','User.admin_type ' => 1 )));
    //     $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.category_name')));
    //     $posts = $this->Post->find('list', array('fields' => array('Post.id', 'Post.post_title')));


    //     $title_for_layout = 'Post Add';

    //     if ($this->request->is('post')) {
    //         // echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;
    //         $user_id = $this->request->data['Post']['user_id'];
    //         $options = array('conditions' => array('Post.post_title' => $this->request->data['Post']['post_title']));
    //         $name = $this->Post->find('first', $options);

    //         if (!$name) {

    //             if (!empty($this->request->data['Post']['image']['name'])) {
    //                 $pathpart = pathinfo($this->request->data['Post']['image']['name']);
    //                 $ext = $pathpart['extension'];
    //                 $extensionValid = array('jpg','jpeg','png','gif','mp4','mkv','flv');
    //                 if (in_array(strtolower($ext), $extensionValid)) {
    //                     $uploadFolder = "img/post_img";
    //                     $uploadPath = WWW_ROOT . $uploadFolder;
    //                     $filename = uniqid() . '.' . $ext;
    //                     $full_flg_path = $uploadPath . '/' . $filename;
    //                     move_uploaded_file($this->request->data['Post']['image']['tmp_name'], $full_flg_path);
    //                     $this->request->data1['PostImage']['originalpath'] = $filename;
    //                     $this->request->data1['PostImage']['resizepath'] = $filename;
    //                 } else {
    //                     $this->Session->setFlash(__('Invalid image type.'));
    //                     return $this->redirect(array('action' => 'index'));
    //                 }
    //             } else {
    //                 $filename = '';
    //             }

    //             // foreach ($this->request->data['Post']['video'] as $key => $video) {

    //             //     if (!empty($video['name'])) {
    //             //         $pathpart = pathinfo($video['name']);
    //             //         $ext = $pathpart['extension'];
    //             //         $extensionValid = array('mp4','mkv','flv');
    //             //         if (in_array(strtolower($ext), $extensionValid)) {
    //             //             $uploadFolder = "img/post_video";
    //             //             $uploadPath = WWW_ROOT . $uploadFolder;
    //             //             $filename = uniqid() . '.' . $ext;
    //             //             $full_flg_path = $uploadPath . '/' . $filename;
    //             //             move_uploaded_file($video['tmp_name'], $full_flg_path);
    //             //             $videoData['originalpath'] = $filename;
    //             //             $videoData['resizepath'] = $filename;
    //             //             $this->request->data['PostVideo'][] = $videoData;
    //             //         }
    //             //     }
    //             // }


    //             $this->request->data['Post']['post_date'] = gmdate('Y-m-d h:m:s');
    //             $this->Post->create();
    //             if ($this->Post->saveAssociated($this->request->data)) {

    //                 $this->loadModel('User');
    //                 $this->User->id = $user_id;
    //                 $this->User->saveField('admin_type','2');

    //                 $this->request->data1['PostImage']['post_id'] = $this->Post->id;
    //                 $this->PostImage->save($this->request->data1);
    //                 $this->Session->setFlash(__('Course Basic Details has been saved. Add Resourses for this Course.'));
    //                 return $this->redirect(array('action' => 'addResourse',$this->Post->id));

    //             } else {
    //                 $this->Session->setFlash(__('The Post could not be saved. Please, try again.'));
    //             }

    //         } else {
    //             $this->Session->setFlash(__('The Post name already exists. Please, try again.'));
    //         }
    //     }

    //     $this->set(compact('title_for_layout', 'countries', 'posts','users', 'categories'));
    // }

    public function admin_add($id = NULL) {
        //echo 1; die;
        $user_id = base64_decode($id);


        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        $this->request->data2 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.admin_type !=' => '0','User.admin_type' => '1')));
        $categories = $this->Post->Category->find('list', array('conditions'=>array('Category.parent_id'=>0), 'fields' => array('Category.id', 'Category.category_name')));
       $jobtypes = $this->Jobtype->find('list', array('fields' => array('Jobtype.id', 'Jobtype.name')));
       $salarypers = $this->Salaryper->find('list', array('fields' => array('Salaryper.id', 'Salaryper.name')));
        $countries = $this->Country->find('list', array('fields' => array('Country.id', 'Country.name')));

        $title_for_layout = 'Job Add';
        if ($this->request->is('post')) {

            $user_id = $this->request->data['Post']['user_id'];
            //$options = array('conditions' => array('Post.post_title' => $this->request->data['Post']['post_title']));
           // $name = $this->Post->find('first', $options);
            //if (!$name) {

//                if (!empty($this->request->data['Post']['image']['name'])) {
//                    $pathpart = pathinfo($this->request->data['Post']['image']['name']);
//                    $ext = $pathpart['extension'];
//                    $extensionValid = array('jpg','jpeg','png','gif','mp4','mkv','flv');
//                    if (in_array(strtolower($ext), $extensionValid)) {
//                        $uploadFolder = "img/post_img";
//                        $uploadPath = WWW_ROOT . $uploadFolder;
//                        $filename = uniqid() . '.' . $ext;
//                        $full_flg_path = $uploadPath . '/' . $filename;
//                        move_uploaded_file($this->request->data['Post']['image']['tmp_name'], $full_flg_path);
//                        $this->request->data1['PostImage']['originalpath'] = $filename;
//                        $this->request->data1['PostImage']['resizepath'] = $filename;
//                    } else {
//                        $this->Session->setFlash(__('Invalid image type.'));
//                        return $this->redirect(array('action' => 'index'));
//                    }
//                } else {
//                    $filename = '';
//                }

                //for uploading audio file........................................................


                $this->request->data['Post']['post_date'] = gmdate('Y-m-d h:m:s');
                $this->Post->create();
                if ($this->Post->save($this->request->data)) {

                 //   $this->loadModel('User');
                  //  $this->User->id = $user_id;
                  //  $this->User->saveField('admin_type','1');

//                    $this->request->data1['PostImage']['post_id'] = $this->Post->id;
//                    $this->PostImage->save($this->request->data1);


                    $this->Session->setFlash(__('The Job has been saved.'));

                    //return $this->redirect(array('action' => 'coursestatus',$this->request->data['Post']['is_approve']));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The Job could not be saved. Please, try again.'));
                }
//            } else {
//                $this->Session->setFlash(__('The Post name already exists. Please, try again.'));
//            }
        }

        $this->loadModel('Tag');
        $tags = $this->Tag->find('list',array('fields'=>array('Tag.id','Tag.name')));


        $this->set(compact('title_for_layout', 'countries', 'tags','users', 'categories', 'jobtypes','salarypers'));
    }

    public function admin_addResourse($id = NULL){
        $post_id = $id;
        $this->set(compact('post_id'));
    }

    public function addVideo(){

        $this->loadModel('PostVideo');

        $data = array();

        if (!empty($_FILES)) {

            if (!empty($_FILES['file']['name'])) {
                $pathpart = pathinfo($_FILES['file']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('mp4','mkv','flv');
                if (in_array(strtolower($ext), $extensionValid)) {

                    $uploadFolder = "img/post_video";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;

                    move_uploaded_file($_FILES['file']['tmp_name'], $full_flg_path);
                    $videoData['originalpath'] = $filename;
                    $videoData['resizepath'] = $filename;
                    $videoData['post_id'] = $this->request->data['post_id'];
                    $this->request->data['PostVideo'][] = $videoData;

                    if ($this->PostVideo->save($this->request->data)) {

                        $data['Ack'] = 1;
                        $data['res'] = ' Video Is Successfully Uploaded ';

                    }else{
                        $data['Ack'] = 0;
                        $data['res'] = ' Video Can not save , Try again ';
                    }

                }else{

                    $data['Ack'] = 0;
                    $data['res'] = ' Video Extantion Did not support ';

                }

            }else{
                $data['Ack'] = 0;
                $data['res'] = 'No Video file.';
            }

        }else{
            $data['Ack'] = 0;
            $data['res'] = 'Error!!';
        }

        echo json_encode($data);
        exit;
    }

    public function add_course() {
        $this->loadModel('PostImage');
        $this->loadModel('CmsPage');
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

         $this->loadModel('CmsPage');
        $this->CmsPage->recursive = 2;

        $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 53)));
        $midContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 54)));
        $bottomContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 55)));

        $this->set(compact('topContent', 'midContent', 'bottomContent'));
        if(isset($this->request->data['post_title'])){
            $data['Post']['post_title'] = $this->request->data['post_title'];
            $data['Post']['post_date']  = gmdate('Y-m-d h:m:s');
            $data['Post']['is_approve'] = 2;
            $data['Post']['user_id'] = $userid;

            $this->Post->create();
            if ($this->Post->save($data)) {

                $this->loadModel('RecentActivity');
                $postID = $this->Post->getLastInsertId();
                $activity['RecentActivity'] = array('post_id'=>$postID,'activity'=>1,'message'=>'New Course is posted','date'=>gmdate('Y-m-d h:m:s'));
                $this->RecentActivity->create();
                $this->RecentActivity->save($activity);

                $this->User->id = $userid;
                $this->User->saveField('admin_type','1');

                $post_id = $this->Post->id;
                $postDetails = $this->Post->find('first',array('conditions'=>array('Post.id'=>$post_id)));
                $post_slug = $postDetails['Post']['slug'];
                return $this->redirect(array('controller'=>'posts', 'action' => 'course_goals',$post_slug));
                $this->Session->setFlash('The Post could not be saved. Please, try again.', 'default', array('class' => 'success'));
            }
        }
    }

    public function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
    }




    public function jobsearch() {

        $this->loadModel('Job');
        $this->loadModel('CompanyDetail');
        $this->loadModel('Pagebanner');

        $pagebanner = $this->Pagebanner->find('first', array(
                                                  'conditions'=>array('Pagebanner.id'=>47),
                                                  array('Pagebanner.status' => 1)
                                                ));
        $this->set(compact('pagebanner'));
        if($this->request->is('post')) {

            $title = $this->request->data['title'];
            $unfilteredjobs = $this->Job->find('all', array(
                            'conditions' => array(
                                'OR' => array('Job.title LIKE'=>'%'.$title.'%'),
                                        array('Job.country_name Like' => '%'.$this->request->data['country'].'%')
                            ),
                                'order' => array('Job.created' => 'desc')
                            ));

            if($this->request->data['location']) {
                $ip = $this->get_client_ip();
                $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
                $lat = $new_arr[0]['geoplugin_latitude'];
                $lon = $new_arr[0]['geoplugin_longitude'];

                foreach ($unfilteredjobs as $unfilteredjob) {
                    $distance = $this->distance($lat, $lon, $unfilteredjob['Job']['lat'],$unfilteredjob['Job']['lon'], "M");
                    if($this->request->data['location'] > $distance) {
                        $totals[] = $unfilteredjob;
                    }

                }
            }else {
                $totals = $unfilteredjobs;
            }

        }else {


            $totals = $this->Job->find('all', array(
                'order' => array('Job.created' => 'desc')
            ));
        }
        foreach ($totals as $total) {

            $this->loadModel('Country');
            $countrys = $this->Country->find('first', array(
                'conditions' => array('Country.id' => $total['Job']['country'])
            ));
            $total['Job']['country'] = $countrys['Country']['name'];
            $wholes[] = $total;
        }


        $this->set(compact('wholes', 'country'));


    }


    public function course_search($category_slug = NULL){
        $userid = $this->Session->read('userid');

        $this->loadModel('Category');
        if(isset($this->request->data) && !empty($this->request->data)){
            $title = $this->request->data['search'];
            $options = array('conditions'=>array('Post.post_title LIKE' => '%'.$title.'%','Post.is_approve'=>1,'Post.privacy'=>0),'order' => array('Post.price' => 'desc'), 'group' => 'Post.id','limit'=>6);
        }
        else{
            $options = array('conditions'=>array('Post.is_approve'=>1,'Post.privacy'=>0),'order' => array('Post.price' => 'desc'), 'group' => 'Post.id','limit'=>6);
        }

        if($category_slug!=''){
            $catDetails = $this->Category->find('first',array('conditions'=>array('Category.slug'=>$category_slug)));
            if($catDetails['Category']['parent_id']==0){
                $category_id = $catDetails['Category']['id'];
                $options = array('conditions'=>array('Post.is_approve'=>1,'Post.privacy'=>0,'Post.category_id'=>$category_id),'order' => array('Post.price' => 'desc'), 'group' => 'Post.id','limit'=>6);
            }
            if($catDetails['Category']['parent_id']!=0){
                $subcategory_id = $catDetails['Category']['id'];
                $options = array('conditions'=>array('Post.is_approve'=>1,'Post.privacy'=>0,'Post.subcategory_id'=>$subcategory_id),'order' => array('Post.price' => 'desc'), 'group' => 'Post.id','limit'=>6);
            }
        }

        $max_price_post = $this->Post->find('all',array('limit'=>1,'order'=>array('Post.price DESC')));
        $max_price = $max_price_post[0]['Post']['price'];
        $this->Paginator->settings = $options;
        $this->Post->recursive = 1;
        $courses = $this->Paginator->paginate('Post');
        //$contain = array('Post' => array('conditions' => array('Post.is_approve'=>1)));
        $categories = $this->Category->find('all',array('fields'=>array('Category.id','Category.category_name','Category.post_count'),'order' => array('Category.post_count' => 'desc'),'conditions'=>array('Category.parent_id'=>0)));
        $users = $this->User->find('all',array('conditions'=>array('User.admin_type'=>1,'User.is_admin != 1'), 'fields'=>array('User.id','User.first_name','User.last_name')));
        $this->set(compact('categories','users','courses','max_price'));

    }

    public function ajaxCourseSearch(){
        $data = $conditions = array();
        $html = '';

        if ($this->request->is('ajax')) {
            $contain = '';
            if(isset($this->request->data['order_filter'])){
                if($this->request->data['order_filter']=='by_review'){
                    $order_by = array('Post.click_count DESC');
                }
                if($this->request->data['order_filter']=="by_rating"){
                    $contain = array('Review' => array('order' => 'Review.ratting DESC'));
                }
                if($this->request->data['order_filter']=="by_date"){
                    $order_by = array('Post.post_date DESC');
                }
                if($this->request->data['order_filter']=="by_low_price"){
                    $order_by = array('Post.price ASC');
                }
                if($this->request->data['order_filter']=="by_high_price"){
                    $order_by = array('Post.price DESC');
                }
                $sort = $this->request->data['order_filter'];
            }
            else{
                $order_by = '';
                $sort = '';
            }

            //$options = array('Post.user_id'=>$userid);
            $order = '';

            if(!empty($order_by)){
                $order = $order_by;
            }

            if(isset($this->request->data['slider_range'])){
                $conditions['Post.price between ? and ?'] = array($this->request->data['slider_range'][0],$this->request->data['slider_range'][1]);
                $order['Post.price'] = 'DESC';
            }

            if(isset($this->request->data['free_price']) && $this->request->data['free_price'] != NULL) {
                $conditions['Post.price'] = 0;
            }

            if(isset($this->request->data['skill_level']) && $this->request->data['skill_level'] != NULL ){
                if($this->request->data['skill_level']!=0){
                    $conditions['Post.skill_level'] = $this->request->data['skill_level'];
                }
            }

            if(isset($this->request->data['category_id']) && $this->request->data['category_id'] != NULL) {
                $conditions['Post.category_id'] = $this->request->data['category_id'];
            }

            if(isset($this->request->data['sub_category_id']) && $this->request->data['sub_category_id'] != NULL){
               $conditions['Post.subcategory_id'] = $this->request->data['sub_category_id'];
            }

            if(isset($this->request->data['user_id']) && $this->request->data['user_id'] != NULL) {
                $conditions['Post.user_id'] = $this->request->data['user_id'];
            }

            if(isset($this->request->data['show']) && $this->request->data['show'] != NULL) {
                $limit = $this->request->data['show'];
            }
            else{
               $limit = '';
            }

            if(isset($this->request->data['short_by']) && $this->request->data['short_by'] != NULL) {
                $conditions['Post.lead_status'] = $this->request->data['short_by'];
            }
            $conditions['Post.is_approve'] = 1;
            $conditions['Post.privacy'] = 0;
            //$conditions['Post.status'] = 1;'Post.privacy'=>0
            //pr($conditions); exit;
            $this->Post->recursive = 1;
            $courses = $this->Post->find('all', array(
                'conditions' => $conditions,
                'order' => $order,
                'limit' => $limit,
                'contain' => $contain
                //'page' => $this->request->data['page']
            ));

            if (!empty($courses)) {
                $data['ack'] = 1;
                foreach ($courses as $key => $course) {
                    if($course['Post']['post_description']!=''){
                        if (strlen($course['Post']['post_description']) > 100)
                         {
                             $stringCut = substr($course['Post']['post_description'], 0, 100);
                             $string = substr($stringCut, 0, strrpos($stringCut, ' '))."....";
                         }
                         else
                         {
                             $string=$course['Post']['post_description'];
                         }
                    }
                $html .='<div class="item  col-xs-4 col-lg-4"><div class="coursename thumbnail" style="cursor:pointer;" onclick="javascript:window.location.href='."'".$this->webroot.'posts/course_details/'.$course['Post']['slug']."'".'" >
                <div class="courseimage"><img class="img-responsive" src="'.$this->webroot.'img/post_img/'.$course['PostImage']['0']['originalpath'].'"></div><div class="course_bottom_part">
                        <div class="name-p">'.$course['Post']['post_title'].'</div><div class="course_para">'.$string.'</div>';

                        if(!empty($course['User']))
                        {

                        $html .= '<div class="writer_name">By '.$course['User']['first_name'].' '.$course['User']['last_name'].'</div>';

                        }

                        $html .='<div class="rating">Rating:<span class="starimg"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                <i class="fa fa-star"></i></span></div><div class="price"><div class="pull-left"><span class="pricenin">$'.round($course['Post']['price']).'</span>';
                            if(isset($course['Post']['old_price']) && $course['Post']['old_price']!=''){
                            $html .='<span class="doller25">$'.$course['Post']['old_price'].'</span>';
                            }
                            $html .='</div><div class="pull-right"><span class="enrolled"><img src="'.$this->webroot.'img/groupusers.png'.'">57.2k Enrolled</span></div>
                            <div class="clearfix"></div></div></div></div></div>';

                }

                $data['data'] = $html;
                $data['sort'] = $sort;
            } else {
                $data['ack'] = 0;
            }
        }
        echo json_encode($data);
        exit;
    }

    public function user_courses(){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

        $conditions['Post.user_id'] = $userid;
        $options = array('conditions' => $conditions, 'order' => array('Post.id' => 'desc'), 'group' => 'Post.id','limit'=>6);
        $this->Paginator->settings = $options;
        $this->Post->recursive = 1;
        $this->set('courses', $this->Paginator->paginate('Post'));
    }

    // public function course_landing_page($slug = NULL){
    //     $this->loadModel('PostImage');
    //     $userid = $this->Session->read('userid');

    //     if (!isset($userid) || $userid == '') {
    //         $this->redirect('/');
    //     }
    //     if (!$this->User->exists($userid)) {
    //         throw new NotFoundException(__('Invalid user'));
    //     }
    //     $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
    //     $course_id = $post_dtls['Post']['id'];
    //     $categoryID = $post_dtls['Post']['category_id'];
    //     $subcategories = $this->Post->Category->find('list',array('fields'=>array('Category.id','Category.category_name'),'conditions'=>array('Category.parent_id'=>$categoryID) ));

    //     $this->loadModel('AccessInstructor');
    //     $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$course_id,'AccessInstructor.user_id'=>$userid)));
    //     if(!empty($accessInstructor)){
    //         if($accessInstructor['AccessInstructor']['edit']==0){
    //             return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
    //         }
    //     }

    //     $post_dtls = $this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));

    //     $this->request->data1 = array();
    //     $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.is_admin' => '0')));
    //     $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.category_name'),'conditions'=>array('Category.parent_id'=>0)));


    //     $this->loadModel('Category');
    //     $allCat = $this->Category->find('all');
    //     $posts = $this->Post->find('list', array('fields' => array('Post.id', 'Post.post_title')));
    //     $course_id = $post_dtls['Post']['id'];
    //     $course_status = $this->course_status($course_id);


    //     $title_for_layout = 'Post Add';
    //     if (!empty($this->request->data)) {

    //             echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;

    //             $data['Post']['id'] = $this->request->data['Post']['id'];
    //             $data['Post']['user_id'] = $userid;
    //             $data['Post']['post_title'] = $this->request->data['Post']['post_title'];
    //             $data['Post']['post_subtitle'] = $this->request->data['Post']['post_subtitle'];
    //             $data['Post']['post_description'] = $this->request->data['Post']['post_description'];
    //             $data['Post']['skill_level'] = $this->request->data['Post']['skill_level'];
    //             $data['Post']['category_id'] = $this->request->data['Post']['category_id'];
    //             $data['Post']['subcategory_id'] = $this->request->data['Post']['subcategory_id'];
    //             $data['Post']['language'] = $this->request->data['Post']['language'];
    //             $data['Post']['post_date'] = gmdate('Y-m-d h:m:s');
    //             $data['Post']['landing_page_status'] = 1;
    //             $data['Tag'] = $this->request->data['Tag'];

    //             //$this->Post->create();
    //             if ($this->Post->save($data)) {
    //                 $post_id = $this->Post->id;

    //                 // $this->loadModel('User');
    //                 // $this->User->id = $userid;
    //                 // $this->User->saveField('admin_type','1');
    //                 // $this->request->data1['PostImage']['post_id'] = $this->Post->id;
    //                 // $this->PostImage->save($this->request->data1);
    //                 if (isset($this->request->data['Post']['image']) && !empty($this->request->data['Post']['image'])) {

    //                     foreach ($this->request->data['Post']['image'] as $value) {

    //                         $path = $value['name'];
    //                         $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
    //                         if ($ext) {
    //                             $uploadFolder = "img/post_img";
    //                             $uploadPath = WWW_ROOT . $uploadFolder;
    //                             $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
    //                             if (in_array($ext, $extensionValid)) {
    //                                 $imageName = rand() . '_' . (strtolower(trim($value['name'])));

    //                                 $full_image_path = $uploadPath . '/' . $imageName;
    //                                 move_uploaded_file($value['tmp_name'], $full_image_path);
    //                                 $this->request->data1['PostImage']['originalpath'] = $imageName;
    //                                 $this->request->data1['PostImage']['resizepath'] = $imageName;
    //                                 $this->request->data1['PostImage']['post_id'] = $post_id;
    //                                 $this->PostImage->create();
    //                                 $this->PostImage->save($this->request->data1);
    //                             }
    //                         }
    //                     }
    //                 }

    //                 if (isset($this->request->data['Post']['video']) && !empty($this->request->data['Post']['video'])) {

    //                     foreach ($this->request->data['Post']['video'] as $valueVideo) {

    //                         $path1 = $valueVideo['name'];
    //                         $ext1 = strtolower(pathinfo($path1, PATHINFO_EXTENSION));
    //                         if ($ext1) {
    //                             $uploadFolder1 = "img/post_video";
    //                             $uploadPath1 = WWW_ROOT . $uploadFolder1;
    //                             $extensionValid1 = array('mp4','mkv','flv');
    //                             if (in_array($ext1, $extensionValid1)) {
    //                                 $imageName1 = rand() . '_' . (strtolower(trim($valueVideo['name'])));

    //                                 $full_image_path1 = $uploadPath1 . '/' . $imageName1;
    //                                 //echo $imageName1.'<br>';
    //                                 //echo $full_image_path1; exit;
    //                                 move_uploaded_file($valueVideo['tmp_name'], $full_image_path1);
    //                                 $video['PostVideo']['originalpath'] = $imageName1;
    //                                 $video['PostVideo']['resizepath'] = $imageName1;
    //                                 $video['PostVideo']['post_id'] = $post_id;
    //                                 $this->loadModel('PostVideo');
    //                                 $this->PostVideo->create();
    //                                 $this->PostVideo->save($video);
    //                             }
    //                         }
    //                     }
    //                 }
    //                 $this->Session->setFlash('The Post has been saved.', 'default', array('class' => 'success'));
    //                 $postDetails = $this->Post->find('first',array('conditions'=>array('Post.id'=>$this->request->data['Post']['id'])));
    //                 $post_slug = $postDetails['Post']['slug'];
    //                 return $this->redirect(array('controller'=>'posts', 'action' => 'course_landing_page',$post_slug));
    //             } else {
    //                 $this->Session->setFlash('The Post could not be saved. Please, try again.', 'default', array('class' => 'error'));
    //             }
    //     }
    //     $this->request->data = $post_dtls;

    //     $this->loadModel('Tag');
    //     $tags = $this->Tag->find('list',array('fields'=>array('Tag.id','Tag.name')));

    // $this->set(compact('title_for_layout', 'subcategories','countries','allCat', 'tags', 'posts', 'users', 'categories','post_dtls','course_status'));

    // }


    public function course_landing_page($slug = NULL){
        $this->loadModel('PostImage');
        $this->loadModel('Skill');
        $this->loadModel('CourseSkill');
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $course_id = $post_dtls['Post']['id'];
        $categoryID = $post_dtls['Post']['category_id'];
        $subcategories = $this->Post->Category->find('list',array('fields'=>array('Category.id','Category.category_name'),'conditions'=>array('Category.parent_id'=>$categoryID) ));

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$course_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        $post_dtls = $this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));

        $this->request->data1 = array();
        $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.is_admin' => '0')));
        $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.category_name'),'conditions'=>array('Category.parent_id'=>0)));


        $this->loadModel('Category');
        $allCat = $this->Category->find('all');
        $posts = $this->Post->find('list', array('fields' => array('Post.id', 'Post.post_title')));
        $course_id = $post_dtls['Post']['id'];
        $course_status = $this->course_status($course_id);


        $title_for_layout = 'Job Add';
        if (!empty($this->request->data)) {

                $saveType = '';
                if(isset($this->request->data['save-next']) && $this->request->data['save-next']=='Save & Next' ){
                    $saveType = 'save-next';
                }

                // echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;

                $data['Post']['id'] = $this->request->data['Post']['id'];
                $data['Post']['user_id'] = $userid;
                $data['Post']['post_title'] = $this->request->data['Post']['post_title'];
                $data['Post']['post_subtitle'] = $this->request->data['Post']['post_subtitle'];
                $data['Post']['post_description'] = $this->request->data['Post']['post_description'];
                $data['Post']['skill_level'] = $this->request->data['Post']['skill_level'];
                $data['Post']['category_id'] = $this->request->data['Post']['category_id'];
                $data['Post']['subcategory_id'] = $this->request->data['Post']['subcategory_id'];
                $data['Post']['language'] = $this->request->data['Post']['language'];
                $data['Post']['post_date'] = gmdate('Y-m-d h:m:s');
                $data['Post']['landing_page_status'] = 1;
                $data['Tag'] = $this->request->data['Tag'];

                //$this->Post->create();
                if ($this->Post->save($data)) {
                    $post_id = $this->Post->id;

                    // $this->loadModel('User');
                    // $this->User->id = $userid;
                    // $this->User->saveField('admin_type','1');
                    // $this->request->data1['PostImage']['post_id'] = $this->Post->id;
                    // $this->PostImage->save($this->request->data1);
                    if (isset($this->request->data['Post']['image']) && !empty($this->request->data['Post']['image'])) {

                        foreach ($this->request->data['Post']['image'] as $value) {

                            $path = $value['name'];
                            $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                            if ($ext) {
                                $uploadFolder = "img/post_img";
                                $uploadPath = WWW_ROOT . $uploadFolder;
                                $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                                if (in_array($ext, $extensionValid)) {
                                    $imageName = rand() . '_' . (strtolower(trim($value['name'])));

                                    $full_image_path = $uploadPath . '/' . $imageName;
                                    move_uploaded_file($value['tmp_name'], $full_image_path);
                                    $this->request->data1['PostImage']['originalpath'] = $imageName;
                                    $this->request->data1['PostImage']['resizepath'] = $imageName;
                                    $this->request->data1['PostImage']['post_id'] = $post_id;
                                    $this->PostImage->create();
                                    $this->PostImage->save($this->request->data1);
                                }
                            }
                        }
                    }

                    if (isset($this->request->data['Post']['video']) && !empty($this->request->data['Post']['video'])) {

                        // pr($this->request->data['Post']['video']); exit;

                        foreach ($this->request->data['Post']['video'] as $valueVideo) {

                            $path1 = $valueVideo['name'];
                            $ext1 = strtolower(pathinfo($path1, PATHINFO_EXTENSION));
                            if ($ext1) {
                                $uploadFolder1 = "img/post_video";
                                $uploadPath1 = WWW_ROOT . $uploadFolder1;
                                // echo $uploadPath1; exit;
                                $extensionValid1 = array('mp4','mkv','flv');
                                if (in_array($ext1, $extensionValid1)) {
                                    $imageName1 = rand() . '_' . (strtolower(trim($valueVideo['name'])));

                                    $full_image_path1 = $uploadPath1 . '/' . $imageName1;
                                    // echo $imageName1.'<br>';
                                    // echo $full_image_path1; exit;
                                    move_uploaded_file($valueVideo['tmp_name'], $full_image_path1);
                                    $video['PostVideo']['originalpath'] = $imageName1;
                                    $video['PostVideo']['resizepath'] = $imageName1;
                                    $video['PostVideo']['post_id'] = $post_id;
                                    $this->loadModel('PostVideo');
                                    $this->PostVideo->create();
                                    $this->PostVideo->save($video);
                                }
                            }
                        }
                    }

                     if(count($this->request->data['skilldata'])>0){
                    $this->request->data3=array();
                     foreach($this->request->data['skilldata'] as $key=>$val)
                     {
                          $optionsSkill = array('conditions' => array('Skill.id' => $val));
                           $skillRow = $this->Skill->find('first', $optionsSkill);
                           $this->request->data3['CourseSkill']['skill_id']=$val;
                           $this->request->data3['CourseSkill']['skill_name']=$skillRow['Skill']['skill_name'];
                           $this->request->data3['CourseSkill']['post_id']=$post_id;
                           $this->CourseSkill->create();
                           $this->CourseSkill->save($this->request->data3);
                     }
                    }
                    $this->Session->setFlash('The Post has been saved.', 'default', array('class' => 'success'));
                    $postDetails = $this->Post->find('first',array('conditions'=>array('Post.id'=>$this->request->data['Post']['id'])));
                    $post_slug = $postDetails['Post']['slug'];

                    if($saveType == 'save-next'){
                        return $this->redirect(array('action' => "automatic_message",$postDetails['Post']['slug']));
                    }

                    return $this->redirect(array('controller'=>'posts', 'action' => 'course_landing_page',$post_slug));
                } else {
                    $this->Session->setFlash('The Post could not be saved. Please, try again.', 'default', array('class' => 'error'));
                }
        }
        $this->request->data = $post_dtls;

        $this->loadModel('Tag');
        $tags = $this->Tag->find('list',array('fields'=>array('Tag.id','Tag.name')));
 $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 61)));




        $skill = $this->Skill->find('list',array('fields'=>array('id','skill_name')));
    $this->set(compact('title_for_layout', 'subcategories','countries','allCat', 'tags', 'posts', 'users', 'categories','post_dtls','course_status','topContent','skill'));

    }

    public function overview($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

    }

    public function course_setting(){

    }

    public function course_goals($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $course_id = $post_dtls['Post']['id'];
        $courseGoals = json_decode($post_dtls['Post']['course_goals']);


        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$course_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        $course_slug = $slug;

        $course_status = $this->course_status($course_id);

        $tmp = array('key'=>'value');

         $this->loadModel('CmsPage');
        $this->CmsPage->recursive = 2;

        $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 56)));
        $midContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 57)));
        $bottomContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 58)));

        $this->set(compact('courseGoals','course_id','course_slug','post_dtls','tmp','course_status','topContent', 'midContent', 'bottomContent'));
    }

    public function approve_status($slug = NULL){
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $post_dtls = $this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        $this->Post->id = $post_id;
        if($this->Post->saveField('is_approve',0)){
            $this->redirect('/posts/course_goals/'.$slug);
            $this->Session->setFlash('Course Is Successfully Submited For Review', 'default',array('class'=>'success'));
        }

    }



    public function course_details($slug = NULL){
        $this->loadModel('PostImage');
        $userid = $this->Session->read('userid');
        // if (!isset($userid) || $userid == '') {
        //     $this->redirect('/');
        // }
        // if (!$this->User->exists($userid)) {
        //     throw new NotFoundException(__('Invalid user'));
        // }

        $post_dtls = $this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug),'fields'=>array('id')));
        $id = $post_dtls['Post']['id'];
        $this->User->recursive = 2;

        $this->loadModel('UserCourse');
        $totalEnrolled = $this->UserCourse->find('count',array('conditions'=>array('UserCourse.post_id'=>$id)));


        $this->loadModel('Lesson');
        $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$id)));

        $this->loadModel('Lecture');
        $totalLecture = $this->Lecture->find('count',array('conditions'=>array('Lecture.post_id'=>$id)));

        $this->set('coursedetail', $this->Post->find('first', array('conditions' => array('Post.id' => $id))));

        //$this->set('all_reviews', $this->Rating->find('all',array('conditions' => array('Rating.post_id' => $id)))) ;
        $otherCourses = $this->Post->find('all',array('limit'=>6));
        $this->set(compact('otherCourses','lessons','totalEnrolled','totalLecture'));

        $coursecount=$this->Post->find('first', array('conditions' => array('Post.id' => $id)));
        // $coursecount['Post']['click_count']=$coursecount['Post']['click_count']+1;
        // $count_update['Post']['click_count'] = $coursecount['Post']['click_count'];
        // $count_update['Post']['id'] = $id;
        // $this->Post->id = $id;
        // $this->Post->saveField('click_count',$count_update['Post']['click_count']);




        if ($userid) {

            $this->loadModel('UserCourse');
        $courseExist = $this->UserCourse->find('count',array('conditions'=>array('UserCourse.user_id'=>$userid,'UserCourse.post_id'=>$id)));
        if($courseExist){
          $status = TRUE;
        }else{
            $status = FALSE;
        }


        } else {
            $status = FALSE;


        }

$this->set('courseStatus',$status);


        $this->set('existOnCart', $existOnCart);

        $this->loadModel('Wishlist');
        $whishlistexist = $this->Wishlist->find('first', array('conditions' => array('Wishlist.post_id' => $id, 'Wishlist.user_id' => $this->Session->read('userid'))));
        if (!empty($whishlistexist)) {

            $whishlistexist = 1;
        } else {
            $whishlistexist = 0;
        }

        $this->set('whishlistexist', $whishlistexist);

        $this->set(compact('userid'));



    }

    public function details($slug = NULL){

       $this->loadModel('PostImage');
        $userid = $this->Session->read('userid');
        // if (!isset($userid) || $userid == '') {
        //     $this->redirect('/');
        // }
        // if (!$this->User->exists($userid)) {
        //     throw new NotFoundException(__('Invalid user'));
        // }

        $post_dtls = $this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug),'fields'=>array('id','type','slug')));
        $id = $post_dtls['Post']['id'];
        $postDetails=$post_dtls;
        if($post_dtls['Post']['type']==2){
            $this->redirect('/posts/vendor_course/'.$post_dtls['Post']['slug']);
        }
        $this->User->recursive = 2;
        $this->Post->recursive = 2;

        $this->loadModel('UserCourse');
        $totalEnrolled = $this->UserCourse->find('count',array('conditions'=>array('UserCourse.post_id'=>$id)));


        $this->loadModel('Lesson');
        $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$id)));

        $this->loadModel('Lecture');
        $totalLecture = $this->Lecture->find('count',array('conditions'=>array('Lecture.post_id'=>$id)));

        // $courseDetails = $this->Post->find('first', array('conditions' => array('Post.id' => $id)));

        // $MetaTagskeywords = 'Learnfly | '.$courseDetails['Post']['post_title'];

        $this->set('coursedetail', $this->Post->find('first', array('conditions' => array('Post.id' => $id))));

        //$this->set('all_reviews', $this->Rating->find('all',array('conditions' => array('Rating.post_id' => $id)))) ;
        $otherCourses = $this->Post->find('all',array('limit'=>6));

        $this->Post->recursive = 2;
        $postOption = array('limit'=>6,'conditions' => array('`Post`.`course_of_the_day`' => 1, '`Post`.`is_approve`' => 1,'Post.id !='=>$id));
        $courses = $this->Post->find('all', $postOption);

        $this->set(compact('otherCourses','lessons','totalEnrolled','totalLecture','courses','postDetails'));

        $coursecount=$this->Post->find('first', array('conditions' => array('Post.id' => $id)));
        // $coursecount['Post']['click_count']=$coursecount['Post']['click_count']+1;
        // $count_update['Post']['click_count'] = $coursecount['Post']['click_count'];
        // $count_update['Post']['id'] = $id;
        // $this->Post->id = $id;
        // $this->Post->saveField('click_count',$count_update['Post']['click_count']);


        if ($userid) {

         $this->loadModel('UserCourse');
        $courseExist = $this->UserCourse->find('count',array('conditions'=>array('UserCourse.user_id'=>$userid,'UserCourse.post_id'=>$id)));
        if($courseExist){
          $status = TRUE;
        }else{
            $status = FALSE;
        }
             //for membership check.................
            $this->loadModel('Subscription');
           $this->loadModel('MembershipPlan');
           $subscribeDetail = $this->Subscription->find('all',array('conditions'=>array('Subscription.user_id'=>$userid)));
          if(count($subscribeDetail)>0)
          {
           $courseTakenCount = $this->UserCourse->find('count',array('conditions'=>array('UserCourse.user_id'=>$userid)));
           $courseTotalCount=$subscribeDetail[0]['MembershipPlan']['no_of_post'];
           if($courseTakenCount<$courseTotalCount)
           {
               $today=date('Y-m-d');
               $subscriptionEnd=$subscribeDetail[0]['Subscription']['to_date'];
               if(strtotime($today)<=strtotime($subscriptionEnd))
               {
               $Subscribestatus = TRUE;
               }
               else
               {
               $Subscribestatus = TRUE;
               }
           }
           if($courseExist){
                $Subscribestatus = TRUE;
           }else{
               $Subscribestatus = TRUE;
           }
          }
          else
          {
              $Subscribestatus = FALSE;
          }

        $this->loadModel('Wishlist');
        $whishlistexist = $this->Wishlist->find('first', array('conditions' => array('Wishlist.post_id' => $id, 'Wishlist.user_id' => $this->Session->read('userid'))));
        if (!empty($whishlistexist)) {

            $whishlistexist = 1;
        } else {
            $whishlistexist = 0;
        }

        $this->set('whishlistexist', $whishlistexist);

        $this->set(compact('userid'));

        } else {
            $status = FALSE;
             $Subscribestatus = FALSE;

        }

        $this->set('courseStatus',$status);

        $this->loadModel('UserCourse');
        $this->loadModel('User');
        $this->UserCourse->recursive = 2;
        $user_courses = $this->UserCourse->find('all', array('conditions' => array('UserCourse.user_id' => $userid,'UserCourse.post_id' => $id)));
        $this->set('user_courses',$user_courses);
    }

    public function admin_addsubcategory($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Sub Post Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('Post.name' => $this->request->data['Post']['name'], 'Post.parent_id' => $this->request->data['Post']['parent_id']));
            $name = $this->Post->find('first', $options);
            if (!$name) {
                $this->Post->create();
                if ($this->Post->save($this->request->data)) {
                    $this->Session->setFlash(__('The sub category has been saved.'));
                    return $this->redirect(array('action' => 'subposts', $id));
                } else {
                    $this->Session->setFlash(__('The sub category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The sub category name already exists. Please, try again.'));
            }
        }
        $options = array('conditions' => array('Post.id' => $id));
        $categoryname = $this->Post->find('list', $options);
        if ($categoryname) {
            $categoryname = $categoryname[$id];
        } else {
            $categoryname = '';
        }
        $this->set(compact('title_for_layout', 'categoryname', 'id'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->Post->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Post->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
            $this->request->data = $this->Post->find('first', $options);
        }
        $users = $this->Post->User->find('list');
        $this->set(compact('users'));
    }

    // public function admin_edit($id = null) {
    //     $this->loadModel('PostImage');
    //     $userid = $this->Session->read('adminuserid');
    //     $is_admin = $this->Session->read('is_admin');
    //     $this->request->data1 = array();

    //     if (!isset($is_admin) && $is_admin == '') {
    //         $this->redirect('/admin');
    //     }
    //     $posts = $this->Post->find('first', array('conditions' => array('Post.id' => $id)));
    //     //echo $id;exit;
    //     if (!$this->Post->exists($id)) {
    //         throw new NotFoundException(__('Invalid category'));
    //     }
    //     if ($this->request->is(array('post', 'put'))) {



    //         $options = array('conditions' => array('Post.post_title' => $this->request->data['Post']['post_title'], 'Post.id <>' => $id));
    //         $name = $this->Post->find('first', $options);

    //         if (!$name) {

    //             if (!empty($this->request->data['Post']['image']['name'])) {
    //                 $pathpart = pathinfo($this->request->data['Post']['image']['name']);
    //                 $ext = $pathpart['extension'];
    //                 $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'mp4' , 'flv' ,'ogg' , 'mkv');
    //                 if (in_array(strtolower($ext), $extensionValid)) {
    //                     $uploadFolder = "img/post_img";
    //                     $uploadPath = WWW_ROOT . $uploadFolder;
    //                     $filename = uniqid() . '.' . $ext;
    //                     $full_flg_path = $uploadPath . '/' . $filename;
    //                     move_uploaded_file($this->request->data['Post']['image']['tmp_name'], $full_flg_path);
    //                     $this->request->data1['PostImage']['originalpath'] = $filename;
    //                     $this->request->data1['PostImage']['resizepath'] = $filename;
    //                     $this->request->data1['PostImage']['id'] = $this->request->data['Post']['postimage_id'];
    //                     $this->request->data1['PostImage']['post_id'] = $id;
    //                     $this->PostImage->save($this->request->data1);
    //                 } else {
    //                     $this->Session->setFlash(__('Invalid image type.'));
    //                     return $this->redirect(array('action' => 'index'));
    //                 }
    //             }

    //             $this->request->data['Post']['post_date'] = date('Y-m-d h:m:s');
    //             $user_id = $this->request->data['Post']['user_id'];
    //             $post_status = $posts['Post']['is_approve'];
    //             $current_status = $this->request->data['Post']['is_approve'];
    //             $post_title = $this->request->data['Post']['post_title'];

    //                 if($post_status=='0'){ $status = 'In Review';}
    //                 if($post_status=='1'){ $status = 'Approved';}
    //                 if($post_status=='2'){ $status = 'Declined';}
    //                 if($post_status=='3'){ $status = 'Disapproved';}
    //                 if($post_status=='4'){ $status = 'On Hold';}

    //                 if($post_status!=$current_status){

    //                     $this->loadModel('Setting');
    //                     $contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
    //                     if ($contact_email) {
    //                         $adminEmail = $contact_email['Setting']['site_email'];
    //                     } else {
    //                         $adminEmail = 'superadmin@abc.com';
    //                     }
    //                     $this->loadModel('User');
    //                     $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
    //                     $user_name = $userdetails['User']['first_name'].' '.$userdetails['User']['last_name'];

    //                     $this->loadModel('EmailTemplate');
    //                     $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 6)));

    //                     $msg_body = str_replace(array('[USERNAME]', '[COURSETITLE]', '[COURSESTATUS]'), array($user_name, $post_title, $status), $EmailTemplate['EmailTemplate']['content']);

    //                     $from = $contact_email['Setting']['site_name'] . ' <' . $adminEmail . '>';
    //                     $Subject_mail = $EmailTemplate['EmailTemplate']['subject'];
    //                     $this->php_mail($userdetails['User']['email_address'], $from, $Subject_mail, $msg_body);
    //                 }

    //             if ($this->Post->save($this->request->data)) {
    //                 if($current_status==1){
    //                     $this->loadModel('RecentActivity');
    //                     $postID = $this->request->data['Post']['id'];
    //                     $activity['RecentActivity'] = array('post_id'=>$postID,'activity'=>2,'message'=>'Course Status is Approved','date'=>gmdate('Y-m-d h:m:s'));
    //                     $this->RecentActivity->create();
    //                     $this->RecentActivity->save($activity);
    //                 }
    //                 $this->Session->setFlash(__('The job has been saved.'));
    //                 return $this->redirect(array('action' => 'coursestatus',$current_status));
    //                 //return $this->redirect(array('action' => 'index'));
    //             } else {
    //                 $this->Session->setFlash(__('The job could not be saved. Please, try again.'));
    //             }
    //         } else {
    //             $this->Session->setFlash(__('The job already exists. Please, try again.'));
    //         }
    //     } else {

    //         $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
    //         $this->request->data = $this->Post->find('first', $options);
    //     }

    //     $this->loadModel('Tag');
    //     $tags = $this->Tag->find('list',array('fields'=>array('Tag.id','Tag.name')));

    //     $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.admin_type' => '1' )));

    //     $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.category_name')));

    //     $skills = $this->Post->Skill->find('list',array('fields'=>array('id','skill_name')));

    //     $countries = $this->Post->Country->find('list', array('fields' => array('id','name')));

    //     $states = $this->Post->State->find('list', array('fields' => array('id','name')));

    //     $cities = $this->Post->City->find('list', array('fields' => array('id','name')));

    //     $location = $this->Post->CourseLocation->find('list', array('conditions'=>array('CourseLocation.user_id'=>$posts['User']['id']),'fields' => array('id','address')));

    //     $this->set(compact('posts', 'tags', 'users', 'categories', 'skills', 'countries', 'states', 'cities', 'location'));
    // }

     public function admin_edit($id = null) {
        $this->loadModel('PostImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();

        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $posts = $this->Post->find('first', array('conditions' => array('Post.id' => $id)));
        //echo $id;exit;
        if (!$this->Post->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {




                $this->request->data['Post']['modified_date'] = date('Y-m-d h:m:s');
                $user_id = $this->request->data['Post']['user_id'];

                $post_title = $this->request->data['Post']['post_title'];




                    //echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;
                if ($this->Post->save($this->request->data)) {

                    $this->Session->setFlash(__('The job has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                    //return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The job could not be saved. Please, try again.'));
                }

        } else {

            $options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
            $this->request->data = $this->Post->find('first', $options);
        }

        $this->loadModel('Tag');
        $tags = $this->Tag->find('list',array('fields'=>array('Tag.id','Tag.name')));

        $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.first_name'), 'conditions' => array('User.admin_type !=' => '0','User.admin_type' => '1')));

        $this->loadModel('Category');
        $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.category_name')));

        $countries = $this->Post->Country->find('list', array('fields' => array('id','name')));

        $jobtypes = $this->Jobtype->find('list', array('fields' => array('Jobtype.id', 'Jobtype.name')));
       $salarypers = $this->Salaryper->find('list', array('fields' => array('Salaryper.id', 'Salaryper.name')));

        $this->set(compact('posts', 'tags', 'users', 'categories',  'countries','jobtypes','salarypers'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Post->id = $id;
        if (!$this->Post->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Post->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Post->id = $id;
        if (!$this->Post->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->Post->delete($id)) {
            $this->Session->setFlash(__('The job has been deleted.'));
        } else {
            $this->Session->setFlash(__('The job could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    ///////////////////////////////AK///////////
    public function admin_export() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $options = array('Post.id !=' => 0);
        $cats = $this->Post->find('all', array('conditions' => $options));
        $output = '';
        $output .='Post Name, Parent Name, Is Active';
        $output .="\n";
//pr($cats);exit;
        if (!empty($cats)) {
            foreach ($cats as $cat) {
                $isactive = ($cat['Post']['active'] == 1) ? 'Yes' : 'No';

                $output .='"' . $cat['Post']['name'] . '","' . $cat['Parent']['name'] . '","' . $isactive . '"';
                $output .="\n";
            }
        }
        $filename = "posts" . time() . ".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    public function list_post($id = null) {
        $this->loadModel('Category');
        $this->loadModel('Post');
        $this->loadModel('Country');
        $this->loadModel('UserBlock');

        $userid = $this->Session->read('user_id');
        $blocked_by_users = array();
        if (!empty($userid)) {
            $blocked_by_users = $this->UserBlock->find('list', array('conditions' => array('UserBlock.user_to' => $userid), 'fields' => array('UserBlock.user_by')));
        }

        if ($id == '') {

            if (!empty($blocked_by_users)) {
                $options_post = array('conditions' => array('Post.id !=' => '', 'Post.is_approve' => 1, 'NOT' => array('Post.user_id' => $blocked_by_users)));
            } else {
                $options_post = array('conditions' => array('Post.id !=' => '', 'Post.is_approve' => 1));
            }

            $posts = $this->Post->find('all', $options_post);

            $options_cont = array('conditions' => array('Country.id' => $posts[0]['Category']['country_id']));
            $countries = $this->Country->find('all', $options_cont);
        } else {
            if (!empty($blocked_by_users)) {
                $options_post = array('conditions' => array('Post.category_id' => $id, 'Post.is_approve' => 1, 'NOT' => array('Post.user_id' => $blocked_by_users)));
            } else {
                $options_post = array('conditions' => array('Post.category_id' => $id, 'Post.is_approve' => 1));
            }

            $posts = $this->Post->find('all', $options_post);

            if (!empty($posts[0])) {
                $options_cont = array('conditions' => array('Country.id' => $posts[0]['Category']['country_id']));
            } else {
                $options_cont = array('conditions' => array());
            }
            $countries = $this->Country->find('all', $options_cont);
        }

        $options = array('conditions' => array('Category.id !=' => '', 'Category.status' => 1));
        $categories = $this->Category->find('all', $options);




        $this->set(compact('categories', 'posts', 'countries'));
    }


    public function isfeature($slug = NULL)
    {

        $this->Post->updateAll(
            array('featured' =>1),
            array('Post.id' => $slug)
        );

         $this->redirect('/posts/list_course');

    }


    public function list_course() {
        $userid = $this->Session->read('userid');
        if(!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        $option = array(
            'conditions' => array(
                'Post.user_id' => $userid
            ),
            'group' => array('Post.id')
        );

        // $userdetails = $this->Post->User->find('all', array('fields' => array('User.id', 'User.admin_type','User.featured'), 'conditions' => array('User.id' => $userid)));

        // $admin_type = $userdetails[0]['User']['admin_type'];
        // $featured_status = $userdetails[0]['User']['featured'];

        // if ( $admin_type==1 || $admin_type==2 && $admin_type==1)
        // {
        //     throw new NotFoundException(__('Invalid user'));
        // }

        $this->Post->recursive = 2;
        $courses = $this->Post->find('all', $option);
        $this->set(compact('courses','featured_status'));
    }

    public function course_filter($slug = NULL,$page = NULL){
        $this->loadModel('Category');
        $categoryDetails = $this->Category->find('first',array('conditions'=>array('Category.slug'=>$slug),'fields'=>array('Category.id','Category.category_name','Category.slug','Category.category_description')));
        $cat_id = $categoryDetails['Category']['id'];


        $order_filter = '';
        $price_filter = '';
        $skill_filter = '';

        // //------ Start Filtering ------//
        // if(!empty($this->request->data)){
        //     //echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;

        //     $contain = '';
        //     if(isset($this->request->data['order_filter'])){
        //         if($this->request->data['order_filter']=='by_review'){
        //             $order_by = array('Post.click_count DESC');
        //             $order_filter = 'by_review';
        //         }
        //         if($this->request->data['order_filter']=="by_rating"){
        //             $contain = array('Review' => array('order' => 'Review.ratting DESC'));
        //             $order_filter = 'by_rating';
        //         }
        //         if($this->request->data['order_filter']=="by_date"){
        //             $order_by = array('Post.post_date DESC');
        //             $order_filter = 'by_date';
        //         }
        //         if($this->request->data['order_filter']=="by_low_price"){
        //             $order_by = array('Post.price ASC');
        //             $order_filter = 'by_low_price';
        //         }
        //         if($this->request->data['order_filter']=="by_high_price"){
        //             $order_by = array('Post.price DESC');
        //             $order_filter = 'by_high_price';
        //         }
        //         $sort = $this->request->data['order_filter'];
        //     }
        //     else{
        //         $order_by = '';
        //         $sort = '';
        //     }

        //     //$options = array('Post.user_id'=>$userid);
        //     $order = '';

        //     if(!empty($order_by)){
        //         $order = $order_by;
        //     }


        //     if(isset($this->request->data['price']) && $this->request->data['price'] != NULL) {
        //         if($this->request->data['price'] == 1){
        //             $conditions['Post.price'] = 0;
        //             $price_filter = '1';
        //         }
        //         if($this->request->data['price'] == 0){
        //             $conditions['Post.price !='] = 0;
        //             $price_filter = '0';
        //         }
        //     }

        //     if(isset($this->request->data['skill_level']) && $this->request->data['skill_level'] != NULL ){
        //         if($this->request->data['skill_level']!=0){
        //             $conditions['Post.skill_level'] = $this->request->data['skill_level'];
        //             $skill_filter = $this->request->data['skill_level'];
        //         }
        //     }



        //     if(isset($this->request->data['show']) && $this->request->data['show'] != NULL) {
        //         $limit = $this->request->data['show'];
        //     }
        //     else{
        //        $limit = 2;
        //     }

        //     if(isset($this->request->data['short_by']) && $this->request->data['short_by'] != NULL) {
        //         $conditions['Post.lead_status'] = $this->request->data['short_by'];
        //     }
        //     $conditions['Post.is_approve'] = 1;
        //     // $conditions['Post.privacy'] = 0;
        //     $conditions['Post.category_id'] = $cat_id;

        //     $this->Post->recursive = 1;
        //     $options = array(
        //         'conditions' => $conditions,
        //         'order' => $order,
        //         'limit' => 4,
        //         'contain' => $contain,
        //         'page' => 1
        //     );
        // }else {
        //     $options = array(
        //     'conditions' => array(
        //         'Post.category_id' => $cat_id,
        //         'Post.is_approve' => 1
        //         ),
        //     'limit' => 4,
        //     'page' =>1
        //     );
        // }
        // //------ End Filtering ------//

        $options = array(
        'conditions' => array(
            'Post.category_id' => $cat_id,
            'Post.is_approve' => 1
            ),
        'limit' => 4,
        'page' =>1
        );

        $this->Paginator->settings = $options;
        $this->set('posts', $this->Paginator->paginate('Post'));

        $topCategories = $this->Category->find('list', array('fields'=>array('Category.id','Category.category_name'),'conditions'=>array('Category.parent_id'=>0),'limit'=>5));
        //print_r($topCategories);die;'conditions'=>array('Category.parent_id'=>0,'Category.category_of_the_day'=>1),'limit'=>5));

        $panelCategories = $this->Category->find('all', array('conditions'=>array('Category.parent_id'=>0),'limit'=>5));
        //print_r($panelCategories);die;


        // //echo '<pre>'; print_r($order_filter); echo '</pre>';
        $optionsAll = array(
        'conditions' => array(
            'Post.category_id' => $cat_id,
            'Post.is_approve' => 1
            ),
        'limit' => 10,
        'page' =>1
        );

        $this->Paginator->settings = $optionsAll;
        $this->set('allPosts', $this->Paginator->paginate('Post'));
        // $allPosts = $this->Post->find('all',array('conditions'=>array('Post.category_id' => $cat_id,'Post.is_approve' => 1),'order'=>array('Post.id'=>'DESC')));

        $this->set(compact('categoryDetails', 'panelCategories','topCategories','allPosts','order_filter','price_filter','skill_filter','cat_id'));
    }

 public function sortcoursefilter()
    {
     $this->layout = false;
     $data = array();
        $searchCount=0;
        $val='';
        //$cat_id=$this->request->data['cat_id'];
        $cat_id=47;
         //$this->request->data['order_filter']=='newest';
        // $ids=array_intersect($ids_skill,$ids_degree,$ids_course);
       // print_r($ids);die;

      if(!empty($this->request->data)){
        //     //echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;

        //     $contain = '';
             if(isset($this->request->data['order_filter'])){
                 if($this->request->data['order_filter']=='newest'){
                     $order_by = array('Post.id DESC');

                 }
        if($this->request->data['order_filter']=='oldest'){
                     $order_by = array('Post.id ASC');

                 }

                 if($this->request->data['order_filter']=='titleAsc'){
                     $order_by = array('Post.post_title ASC');

                 }
        if($this->request->data['order_filter']=='titleDesc'){
                     $order_by = array('Post.post_title DESC');

                 }

            }
             else{
                 $order_by = '';
                 $sort = '';
             }

             $allPosts = $this->Post->find('all',array('conditions'=>array('Post.category_id' => $cat_id,'Post.is_approve' => 1),'order' => $order_by,'limit' => 20));
             $searchCount=count($allPosts);
            foreach ($allPosts as $key => $blog)
            {
                if($blog['Post']['skill_level']==1)
                     {
                         $level='Beginer';
                     }
                     elseif($blog['Post']['skill_level']==2)
                     {
                         $level='Intermediate';
                     }
                     else
                     {
                        $level='Expert';

                     }

                $val .=' <li>
                    <a href="'.$this->webroot.'posts/details/'.$blog['Post']['slug'].'" class="d-flex">
                      <div class="downCorseimg mr-3 p-relative">
                        <img src="'.$this->webroot.'img/post_img/'.$blog['PostImage']['0']['originalpath'].'" alt="">
                        <div class="preview">preview</div>
                      </div>
                      <div class="downCorseimgMenu">
                        <div>
                          <h3>'.$blog['Post']['post_title'].'</h3>
                          <div class="font-12">'.substr($blog['Post']['post_description'],0,150).'</div>
                          <div class="mt-2">

                              <span class="font-12 mr-2">'.$level.'</span>
                              <span class="font-12 mr-2"><span>'.date('M d,Y',strtotime($blog['Post']['post_date'])).'</span></span>
                              <span class="font-12 mr-2">Views <span>'.$blog['Post']['click_count'].'</span></span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </li>';
            }

        }

        if($val!='')  { $data['html'] = $val;  $data['ACK'] = 1; $data['searchCount'] = $searchCount;} else { $data['html'] = '<div class="col-md-6  no_search">No instructor found</div>'; $data['ACK'] = 0; $data['searchCount'] = $searchCount;}

        echo json_encode($data);
        exit;
    }

   /* public function details($slug = NULL){

        $this->loadModel('Post');
        $userid = $this->Session->read('userid');



        $this->Post->recursive = 2;
        if(isset($slug) && $slug !=''){
           $post_dtls = $this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));

           $this->Post->id = $post_dtls['Post']['id'];
           $click_count = $post_dtls['Post']['click_count'];
           $click_count = $click_count + 1;
           $this->Post->saveField('click_count',$click_count);

           $post_related = $this->Post->find('all',array('conditions'=>array('Post.slug !='=>$slug,'Post.category_id'=>$post_dtls['Post']['category_id']),'limit' => 2,'order' => 'Post.id DESC',));
        }

        $this->set(compact('post_dtls','post_related'));
    } */



    public function post_details($id = null) {

        //echo $id;exit;
        //date_default_timezone_set("Asia/Kolkata");

        $this->loadModel('PostImage');
        $this->loadModel('PostComment');
        $this->loadModel('UserImage');
        $this->loadModel('PostFavorite');
        $this->loadModel('PostView');
        $this->loadModel('PostLike');

        $this->PostComment->recursive = 2;
        $this->Post->recursive = 2;

        $userid = $this->Session->read('user_id');
        if ($userid == '') {
            $userid = 0;
            $ip_address = $_SERVER['REMOTE_ADDR'];
            $options_view = array('conditions' => array('PostView.ip_address' => $ip_address, 'PostView.post_id' => $id, 'PostView.type' => 'post'));
        } else {
            $options_view = array('conditions' => array('PostView.user_id' => $userid, 'PostView.post_id' => $id, 'PostView.type' => 'post'));
        }


        $postview_count = $this->PostView->find('count', $options_view);
        if ($postview_count == 0) {

            $this->request->data['PostView']['user_id'] = $userid;
            $this->request->data['PostView']['post_id'] = $id;
            $this->request->data['PostView']['ip_address'] = $ip_address;
            $this->request->data['PostView']['date'] = gmdate('Y-m-d h:i:s');
            $this->request->data['PostView']['type'] = 'post';

            $this->PostView->create();
            $this->PostView->save($this->request->data['PostView']);
        }






        if ($this->request->is('post')) {

            $post_id = $this->request->data['post_id'];

            $this->request->data['PostComment']['user_id'] = $userid;
            $this->request->data['PostComment']['post_id'] = $this->request->data['post_id'];
            $this->request->data['PostComment']['date'] = gmdate('Y-m-d h:i:s');
            $this->request->data['PostComment']['message'] = $this->request->data['message'];
            //pr($this->request->data);

            $this->PostComment->create();
            if ($this->PostComment->save($this->request->data)) {

                return $this->redirect('/posts/post_details/' . $post_id);
            }
        }




        $options_post = array('conditions' => array('Post.id' => $id));
        $posts = $this->Post->find('first', $options_post);

        $post_id = $posts['Post']['id'];

        $options_postimg = array('conditions' => array('PostImage.post_id' => $post_id));
        $postimgs = $this->PostImage->find('all', $options_postimg);


        $options_comment = array('conditions' => array('PostComment.post_id' => $id));
        $postcomment = $this->PostComment->find('all', $options_comment);

        $postcomment_count = $this->PostComment->find('count', $options_comment);

        $options_offer = array('conditions' => array('Post.post_id' => $id, 'Post.type' => 'offer'));
        $postoffer = $this->Post->find('all', $options_offer);

        $postoffer_count = $this->Post->find('count', $options_offer);

        $options_make_offer = array('conditions' => array('Post.post_id' => $id, 'Post.user_id' => $userid, 'Post.type' => 'offer'));
        $post_make_offer = $this->Post->find('count', $options_make_offer);




        $options_fav = array('conditions' => array('PostFavorite.post_id' => $id, 'PostFavorite.type' => 'post'));
        $postfav_count = $this->PostFavorite->find('count', $options_fav);


        $options_like = array('conditions' => array('PostLike.post_id' => $id, 'PostLike.type' => 'post', 'PostLike.status' => 1));
        $postlike_count = $this->PostLike->find('count', $options_like);

        $options_likeuser = array('conditions' => array('PostLike.user_id' => $userid, 'PostLike.post_id' => $id, 'PostLike.type' => 'post', 'PostLike.status' => 1));
        $postlikeuser = $this->PostLike->find('count', $options_likeuser);

        $options_favuser = array('conditions' => array('PostFavorite.user_id' => $userid, 'PostFavorite.post_id' => $id, 'PostFavorite.type' => 'post'));
        $postfavuser_count = $this->PostFavorite->find('count', $options_favuser);

        $options_view1 = array('conditions' => array('PostView.post_id' => $id, 'PostView.type' => 'post'));
        $postview_totcount = $this->PostView->find('count', $options_view1);




        //pr($posts['PostImage']);


        $this->set(compact('posts', 'postimgs', 'postcomment_count', 'postcomment', 'postfav_count', 'postfavuser_count', 'postview_totcount', 'postoffer', 'postoffer_count', 'post_make_offer', 'postlike_count', 'postlikeuser'));
    }

    public function getedit_offerid() {

        $offer_id = $_REQUEST['offer_id'];
        $this->Session->write('getoffer_editid', $offer_id);
        exit;

        //$this->Session->write('getoffer_id', $id);
        //return $this->redirect('/posts/offer_details/'.$id);
    }

    public function makeoffer() {

        $this->Session->delete('LastPostId');
        $this->Session->delete('post_owner');
        $this->Session->delete('notification_to_id');
        $this->Session->delete('post_title');

        $post_id = $_REQUEST['post_id'];
        $userid = $this->Session->read('user_id');
        $is_complete = 0;
        $is_approve = 0;
        $post_date = gmdate('Y-m-d h:i:s');

        $this->request->data['post_date'] = $post_date;
        $this->request->data['is_complete'] = $is_complete;
        $this->request->data['user_id'] = $userid;
        $this->request->data['post_id'] = $post_id;
        $this->request->data['is_approve'] = $is_approve;


        $options_offer = array('conditions' => array('Post.post_id' => $post_id, 'Post.user_id' => $userid));
        $offer_count = $this->Post->find('count', $options_offer);
        $offer = $this->Post->find('first', $options_offer);
        $offer_title = $offer['Post']['post_title'];
        //echo $offer_title


        $post_own = array('conditions' => array('Post.id' => $post_id));
        $post_owner = $this->Post->find('first', $post_own);
        $post_owner_user = $post_owner['Post']['user_id'];

        //echo
        //pr($offer);exit;
        //$offer['Post']['id'];
        if ($offer_count == 0) {

            $this->Post->create();
            $this->Post->save($this->request->data);
            $this->Session->write('LastPostId', $this->Post->getInsertID());
            $this->Session->write('post_owner', $post_owner_user);
            $this->Session->write('notification_to_id', $post_id);
            $this->Session->write('post_title', $offer_title);
        } else {
            $this->Session->write('LastPostId', $offer['Post']['id']);

            $this->Post->updateAll(array('Post.post_date' => "'$post_date'"), array('Post.id' => $LastPostId));
            $this->Session->write('post_owner', $post_owner_user);
            $this->Session->write('notification_to_id', $post_id);
            $this->Session->write('post_title', $offer_title);
        }
        exit;
    }

    public function offer_details($id = null) {
        //date_default_timezone_set("Asia/Kolkata");

        $this->loadModel('PostImage');
        $this->loadModel('PostComment');
        $this->loadModel('UserImage');
        $this->loadModel('PostFavorite');
        $this->loadModel('PostView');
        $this->loadModel('Category');
        $this->loadModel('PostLike');

        $this->PostComment->recursive = 2;
        $this->Post->recursive = 2;



        $userid = $this->Session->read('user_id');


        if ($userid == '') {
            $userid = 0;
            $ip_address = $_SERVER['REMOTE_ADDR'];
            $options_view = array('conditions' => array('PostView.ip_address' => $ip_address, 'PostView.post_id' => $id, 'PostView.type' => 'offer'));
        } else {
            $options_view = array('conditions' => array('PostView.user_id' => $userid, 'PostView.post_id' => $id, 'PostView.type' => 'offer'));
        }


        $postview_count = $this->PostView->find('count', $options_view);
        if ($postview_count == 0) {

            $this->request->data['PostView']['user_id'] = $userid;
            $this->request->data['PostView']['post_id'] = $id;
            $this->request->data['PostView']['ip_address'] = $ip_address;
            $this->request->data['PostView']['date'] = gmdate('Y-m-d h:i:s');
            $this->request->data['PostView']['type'] = 'offer';

            $this->PostView->create();
            $this->PostView->save($this->request->data['PostView']);
        }




        if ($this->request->is('post')) {
            $post_id = $this->request->data['post_id'];

            $this->request->data['user_id'] = $userid;
            $this->request->data['post_id'] = $this->request->data['post_id'];
            $this->request->data['date'] = gmdate('Y-m-d h:i:s');
            $this->request->data['message'] = $this->request->data['message'];


            $this->PostComment->create();
            if ($this->PostComment->save($this->request->data)) {

                return $this->redirect('/posts/offer_details/' . $post_id);
            }
        }




        $options_post = array('conditions' => array('Post.id' => $id));
        $posts = $this->Post->find('first', $options_post);

        $post_id = $posts['Post']['id'];


        $post_details = $this->Post->find('first', array('conditions' => array('Post.id' => $posts['Post']['post_id'])));
        $offer_details = $this->Post->find('first', array('conditions' => array('Post.id' => $post_id)));
        $this->set(compact('post_details', 'offer_details'));
        //$options_cat_select= array('conditions' => array('Category.id' => $post_cat_id));
        //$cate_name = $this->Category->find('first', $options_cat_select);
        //$cate_name['Category']['category_name'];

        $options_offer1 = array('conditions' => array('Post.id' => $posts['Post']['post_id']));
        $postoffer1 = $this->Post->find('first', $options_offer1);

        $options_postimg = array('conditions' => array('PostImage.post_id' => $post_id));
        $postimgs = $this->PostImage->find('all', $options_postimg);


        $options_comment = array('conditions' => array('PostComment.post_id' => $id));
        $postcomment = $this->PostComment->find('all', $options_comment);

        $postcomment_count = $this->PostComment->find('count', $options_comment);

        $options_offer = array('conditions' => array('Post.post_id' => $id, 'Post.type' => 'offer'));
        $postoffer = $this->Post->find('all', $options_offer);

        $postoffer_count = $this->Post->find('count', $options_offer);



        $options_fav = array('conditions' => array('PostFavorite.post_id' => $id, 'PostFavorite.type' => 'offer'));
        $postfav_count = $this->PostFavorite->find('count', $options_fav);

        $options_favuser = array('conditions' => array('PostFavorite.user_id' => $userid, 'PostFavorite.post_id' => $id, 'PostFavorite.type' => 'offer'));
        $postfavuser_count = $this->PostFavorite->find('count', $options_favuser);


        $options_offerlike = array('conditions' => array('PostLike.post_id' => $id, 'PostLike.type' => 'offer', 'PostLike.status' => 1));
        $offerlike_count = $this->PostLike->find('count', $options_offerlike);

        $options_offerlikeuser = array('conditions' => array('PostLike.user_id' => $userid, 'PostLike.post_id' => $id, 'PostLike.type' => 'offer', 'PostLike.status' => 1));
        $offerlikeuser = $this->PostLike->find('count', $options_offerlikeuser);

        $options_view1 = array('conditions' => array('PostView.post_id' => $id, 'PostView.type' => 'offer'));
        $postview_totcount = $this->PostView->find('count', $options_view1);

        //$posts['User']['id'];
        $options_rating = array('conditions' => array('Post.user_id' => $posts['User']['id'], 'Post.type' => 'offer'));
        $postrat_count = $this->Post->find('all', $options_rating);
        /* foreach($postrat_count as $count)
          {


          $options_rating1= array('conditions' => array('PostLike.post_id' => $count['Post']['id'], 'PostLike.type' => 'offer', 'PostLike.status' => 1));
          $postrat_count1 = $this->PostLike->find('all', $options_rating1);

          } */



        $this->set(compact('posts', 'postimgs', 'postcomment_count', 'postcomment', 'postfav_count', 'postfavuser_count', 'postview_totcount', 'postoffer', 'postoffer_count', 'postoffer1', 'offerlike_count', 'offerlikeuser', 'postrat_count'));
    }

    public function post_view($id = null) {

        $this->loadModel('PostView');


        $userid = $this->Session->read('user_id');
        if ($userid == '') {
            $userid = 0;
        }

        $ip_address = $_SERVER['REMOTE_ADDR'];


        $options_view = array('conditions' => array('PostView.ip_address' => $ip_address, 'PostView.post_id' => $id, 'PostView.type' => 'post'));
        $postview_count = $this->PostView->find('count', $options_view);


        if ($postview_count == 0) {
            //echo $postview_count;exit;
            $this->request->data['user_id'] = $userid;
            $this->request->data['post_id'] = $id;
            $this->request->data['ip_address'] = $ip_address;
            $this->request->data['date'] = gmdate('Y-m-d h:i:s');

            //pr($this->request->data);exit;

            $this->PostView->create();
            $this->PostView->save($this->request->data);
        } else {

        }
        //$this->set(compact('postview_totcount'));
    }

    /* public function offer_view($id = null)
      {

      $this->loadModel('PostView');


      $userid = $this->Session->read('user_id');
      if($userid=='')
      {
      $userid=0;
      }

      $ip_address=$_SERVER['REMOTE_ADDR'];


      $options_view= array('conditions' => array('PostView.ip_address' => $ip_address,'PostView.post_id' => $id));
      $postview_count = $this->PostView->find('count', $options_view);


      if($postview_count==0)
      {
      //echo $postview_count;exit;
      $this->request->data['user_id']=$userid;
      $this->request->data['post_id']=$id;
      $this->request->data['ip_address']=$ip_address;
      $this->request->data['type']='offer';
      $this->request->data['date']=gmdate('Y-m-d h:i:s');

      //pr($this->request->data);exit;

      $this->PostView->create();
      $this->PostView->save($this->request->data);

      }
      else
      {

      }
      //$this->set(compact('postview_totcount'));


      } */

    public function user_image($userid) {
        $user_id = base64_decode($userid);
        $this->loadModel('UserImage');

        $options_img = array('conditions' => array('UserImage.user_id' => $user_id));
        $img_user = $this->UserImage->find('first', $options_img);
        return $img_user['UserImage']['originalpath'];
    }

    public function offer_image($postid) {
        $post_id = base64_decode($postid);
        $this->loadModel('PostImage');

        $options_img = array('conditions' => array('PostImage.post_id' => $post_id));
        $img_offer = $this->PostImage->find('first', $options_img);
        //foreach($img_offer as $offer)
        //{
        return $img_offer['PostImage']['originalpath'];
        //}
    }

    public function delete_post($id = null) {
        $this->loadModel('PostComment');
        $this->Post->id = $id;
        if (!$this->Post->exists()) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->Post->delete($id)) {
            $this->PostComment->deleteAll(['PostComment.post_id' => $id]);
            return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));

            $this->Session->setFlash(__('The job has been deleted.'));
        } else {
            $this->Session->setFlash(__('The job could not be deleted. Please, try again.'));
        }
    }

    public function post_fav($post_id = null) {
        //date_default_timezone_set("Asia/Kolkata");

        $post_id = $_REQUEST['post_id'];

        $this->loadModel('PostFavorite');
        $userid = $this->Session->read('user_id');

        $options_user = array('conditions' => array('PostFavorite.user_id' => $userid, 'PostFavorite.post_id' => $post_id, 'PostFavorite.type' => 'post'));
        $existuser = $this->PostFavorite->find('first', $options_user);


        $this->request->data['user_id'] = $userid;
        $this->request->data['post_id'] = $post_id;
        $this->request->data['type'] = 'post';
        $this->request->data['date'] = gmdate('Y-m-d h:i:s');

        if (empty($existuser)) {

            $this->PostFavorite->create();

            if ($this->PostFavorite->save($this->request->data)) {

                //return $this->redirect('/posts/post_details/'.$post_id);
            }
        } else {

            //return $this->redirect('/posts/post_details/'.$post_id);
        }

        $options_fav = array('conditions' => array('PostFavorite.post_id' => $post_id, 'PostFavorite.type' => 'post'));
        echo $postfav_count = $this->PostFavorite->find('count', $options_fav);

        exit;
    }

    public function offer_fav() {
        //date_default_timezone_set("Asia/Kolkata");
        $post_id = $_REQUEST['post_id'];

        $this->loadModel('PostFavorite');
        $userid = $this->Session->read('user_id');

        $options_user = array('conditions' => array('PostFavorite.user_id' => $userid, 'PostFavorite.post_id' => $post_id, 'PostFavorite.type' => 'offer'));
        $existuser = $this->PostFavorite->find('first', $options_user);


        $this->request->data['user_id'] = $userid;
        $this->request->data['post_id'] = $post_id;
        $this->request->data['type'] = 'offer';
        $this->request->data['date'] = gmdate('Y-m-d h:i:s');

        if (empty($existuser)) {

            $this->PostFavorite->create();

            if ($this->PostFavorite->save($this->request->data)) {



                //return $this->redirect('/posts/offer_details/'.$post_id);
            }
        } else {

            //return $this->redirect('/posts/offer_details/'.$post_id);
        }

        $options_fav = array('conditions' => array('PostFavorite.post_id' => $post_id, 'PostFavorite.type' => 'offer'));
        echo $postfav_count = $this->PostFavorite->find('count', $options_fav);

        exit;
    }

    public function post_like() {
        //date_default_timezone_set("Asia/Kolkata");

        $post_id = $_REQUEST['post_id'];
        $this->loadModel('User');

        $this->loadModel('PostLike');
        $userid = $this->Session->read('user_id');

        $options_user = array('conditions' => array('PostLike.user_id' => $userid, 'PostLike.post_id' => $post_id, 'PostLike.type' => 'post'));
        $existuser = $this->PostLike->find('first', $options_user);

        //$existuser['PostLike']['status'];

        $options_post = array('conditions' => array('Post.id' => $post_id));
        $existpost = $this->Post->find('first', $options_post);


        $this->request->data['PostLike']['user_id'] = $userid;
        $this->request->data['PostLike']['post_id'] = $post_id;
        $this->request->data['PostLike']['type'] = 'post';
        $this->request->data['PostLike']['status'] = 1;
        $this->request->data['PostLike']['date'] = gmdate('Y-m-d h:i:s');
        $rtng['type'] = '';
        $rtng['rate'] = '0';
        $rtng['count'] = '0';

        if (empty($existuser)) {

            $this->PostLike->create();

            if ($this->PostLike->save($this->request->data)) {

                $rate = $existpost['User']['rating'] + 1;
                $usr['User']['id'] = $existpost['User']['id'];
                $rating = $usr['User']['rating'] = $rate;
                //$this->User->save($usr);
                $this->User->updateAll(array('User.rating' => $rating), array('User.id' => $existpost['User']['id']));
                $rtng['type'] = 1;

                //return $this->redirect('/posts/post_details/'.$post_id);
            }
        } else {

            if ($existuser['PostLike']['status'] == 1) {

                $update = $this->PostLike->updateAll(array('PostLike.status' => 0), array('PostLike.id' => $existuser['PostLike']['id']));
                $rate = $existpost['User']['rating'] - 1;
                if ($rate < 0) {
                    $rate = 0;
                }
                $usr['User']['id'] = $existpost['User']['id'];
                $rating = $usr['User']['rating'] = $rate;
                //$this->User->save($usr);
                $this->User->updateAll(array('User.rating' => $rating), array('User.id' => $existpost['User']['id']));
                $rtng['type'] = 0;
            } else {
                $update = $this->PostLike->updateAll(array('PostLike.status' => 1), array('PostLike.id' => $existuser['PostLike']['id']));
                $rate = $existpost['User']['rating'] + 1;
                $usr['User']['id'] = $existpost['User']['id'];
                $rating = $usr['User']['rating'] = $rate;
                //$this->User->save($usr);
                $this->User->updateAll(array('User.rating' => $rating), array('User.id' => $existpost['User']['id']));
                $rtng['type'] = 1;
            }
        }
        $rtng['rate'] = $rate;

        $options_like = array('conditions' => array('PostLike.post_id' => $post_id, 'PostLike.type' => 'post', 'PostLike.status' => 1));
        $postlike_count = $this->PostLike->find('count', $options_like);
        //echo $existuser['PostLike']['status'];
        $rtng['count'] = $postlike_count;
        echo json_encode($rtng);

        exit;
    }

    public function offer_like() {
        //date_default_timezone_set("Asia/Kolkata");
        //$this->autoRender=false;

        $post_id = $_REQUEST['post_id'];

        $this->loadModel('PostLike');
        $this->loadModel('User');
        $userid = $this->Session->read('user_id');

        $options_user = array('conditions' => array('PostLike.user_id' => $userid, 'PostLike.post_id' => $post_id, 'PostLike.type' => 'offer'));
        $existuser = $this->PostLike->find('first', $options_user);



        $options_post = array('conditions' => array('Post.id' => $post_id));
        $existpost = $this->Post->find('first', $options_post);
        //echo $existpost['User']['id'];exit;

        $this->request->data['PostLike']['user_id'] = $userid;
        $this->request->data['PostLike']['post_id'] = $post_id;
        $this->request->data['PostLike']['type'] = 'offer';
        $this->request->data['PostLike']['status'] = 1;
        $this->request->data['PostLike']['date'] = gmdate('Y-m-d h:i:s');
        $rtng['type'] = '';
        $rtng['rate'] = '0';
        $rtng['count'] = '0';
        if (empty($existuser)) {

            $this->PostLike->create();

            if ($this->PostLike->save($this->request->data)) {
                $rate = $existpost['User']['rating'] + 1;
                $usr['User']['id'] = $existpost['User']['id'];
                $rating = $usr['User']['rating'] = $rate;
                //$this->User->save($usr);
                $this->User->updateAll(array('User.rating' => $rating), array('User.id' => $existpost['User']['id']));
                $rtng['type'] = 1;
                //return $this->redirect('/posts/post_details/'.$post_id);
            }
        } else {



            if ($existuser['PostLike']['status'] == 1) {

                $update = $this->PostLike->updateAll(array('PostLike.status' => 0), array('PostLike.id' => $existuser['PostLike']['id']));
                $rate = $existpost['User']['rating'] - 1;
                if ($rate < 0) {
                    $rate = 0;
                }
                $usr['User']['id'] = $existpost['User']['id'];
                $rating = $usr['User']['rating'] = $rate;
                //$this->User->save($usr);
                $this->User->updateAll(array('User.rating' => $rating), array('User.id' => $existpost['User']['id']));
                $rtng['type'] = 0;
            } else {
                $update = $this->PostLike->updateAll(array('PostLike.status' => 1), array('PostLike.id' => $existuser['PostLike']['id']));
                $rate = $existpost['User']['rating'] + 1;
                $usr['User']['id'] = $existpost['User']['id'];
                $rating = $usr['User']['rating'] = $rate;
                $this->User->updateAll(array('User.rating' => $rating), array('User.id' => $existpost['User']['id']));
                $rtng['type'] = 1;
            }
        }
        $rtng['rate'] = $rate;
        $options_like = array('conditions' => array('PostLike.post_id' => $post_id, 'PostLike.type' => 'offer', 'PostLike.status' => 1));
        $postlike_count = $this->PostLike->find('count', $options_like);
        $rtng['count'] = $postlike_count;
        echo json_encode($rtng);


        exit;
    }

    public function getgmttime() {
        $offset = $_REQUEST['offset'];
        $this->Session->write('timezone', $offset);
        //$this->Session->read('timezone');
        exit;
    }

    public function message_chat($postId, $offer_id) {
        $userid = $this->Session->read('user_id');
        if (empty($userid)) {
            $this->redirect('/');
            exit;
        }
        $this->Post->recursive = 2;
        $options = array('conditions' => array('Post.post_id' => $postId));
        $offer_user = $this->Post->find('all', $options);
        //pr($offer_user);
        //exit;
        $this->set('offer_user', $offer_user);
        $this->loadModel('User');
        $login_user = $this->User->find('first', array('conditions' => array('User.id' => $userid)));

        $this->Post->recursive = 1;
        $post_details = $this->Post->find('first', array('conditions' => array('Post.id' => $postId)));
        $offer_details = $this->Post->find('first', array('conditions' => array('Post.id' => $offer_id)));

        $chats = $this->Chat->find('all', array('conditions' => array('Chat.offer_id' => $offer_id)));

        $s = $this->Chat->updateAll(array('Chat.is_read' => 1), array('Chat.offer_id' => $offer_id, 'Chat.receiver_id' => $userid));
        //var_dump($s);
        //exit;
        //pr($chats);
        //exit;

        $this->set('login_user', $login_user);
        $this->set(compact('post_details', 'offer_details', 'chats'));
    }

    //////////////////////////AK///////////////////////


    public function admin_import_csv() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if ($this->request->is(array('post', 'put'))) {
            $ret = $this->Post->csvImport($this->request->data['csv']['tmp_name'], $userid);
            if (empty($ret['errors'])) {
                $this->Session->setFlash($ret['messages']);
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->setFlash($ret['errors']);
                return $this->redirect(array('action' => 'index'));
            }
        }
    }

    public function import_csv() {
        $userid = $this->Session->read('userid');
        if(!isset($userid) || $userid == '') {
            $this->redirect('/');
        }

        if ($this->request->is(array('post', 'put'))) {
            $ret = $this->Post->csvImport($this->request->data['csv']['tmp_name'], $userid);
            if (empty($ret['errors'])) {
                $this->Session->setFlash($ret['messages'], 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'list_course'));
            } else {
                $this->setFlash($ret['errors'], 'default', array('class' => 'error'));
                return $this->redirect(array('action' => 'list_course'));
            }
        }
    }

    public function csv_upload() {
        $userid = $this->Session->read('userid');
        if(!isset($userid) || $userid == '') {
            $this->redirect('/');
        }

         $userdetails = $this->Post->User->find('all', array('fields' => array('User.id', 'User.admin_type'), 'conditions' => array('User.id' => $userid)));

         $admin_type=$userdetails[0]['User']['admin_type'];

        if ( $admin_type==1 || $admin_type==2 && $admin_type==1)
        {
            throw new NotFoundException(__('Invalid user'));
        }


        if ($this->request->is(array('post', 'put'))) {
            $ret = $this->Post->csvImport($this->request->data['csv']['tmp_name'], $userid);
            if (empty($ret['errors'])) {
                $this->Session->setFlash($ret['messages'], 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'list_course'));
            } else {
                $this->setFlash($ret['errors'], 'default', array('class' => 'error'));
                return $this->redirect(array('action' => 'list_course'));
            }
        }
    }

    public function admin_featured_course() {
        $conditions = array();
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $conditions['Post.featured'] = 1;
        $options = array('conditions' => $conditions, 'order' => array('Post.id' => 'desc'), 'group' => 'Post.id');
        $this->Paginator->settings = $options;
        $title_for_layout = 'Post List';
        $this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Post'));

    }

    public function course_schedule() {
        $userid = $this->Session->read('userid');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }

        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $params = array(
            'conditions' => array(
                'Post.user_id' => $userid
            ),
            'recursive' => -1,
            'order' => array('Post.id' => 'DESC')
        );
        $courses = $this->Post->find('all', $params);
        $startdates = array();
        foreach ($courses as $course) {
            $startdates[] = array(
                'id' => $course['Post']['id'],
                'title' => $course['Post']['post_title'],
                'start' => date('Y-m-d', strtotime($course['Post']['startdate'])),
                'end' => date('Y-m-d', strtotime($course['Post']['enddate']))
            );
        }
        $startdates = json_encode($startdates);

        $this->set(compact('courses', 'startdates'));
    }

    public function ajaxAddGoals(){
        $res = array();
        $post_id = $this->request->data['post_id'];
        unset($this->request->data['post_id']);
        $data = json_encode($this->request->data);
        $this->Post->id = $post_id;
        if($this->Post->saveField('course_goals', $data)){
            $res['Ack'] = 1;
            $res['res'] = 'Goals Saved';
        }
        else{
            $res['Ack'] = 0;
            $res['res'] = 'Error...';
        }
        echo json_encode($res);
        exit;
    }

    public function course_status($post_id){
        $course_status = array();
        $post_details = $this->Post->find('first',array('conditions'=>array('Post.id'=>$post_id)));

        $this->loadModel('TestVideo');
        $this->loadModel('Lesson');
        $this->loadModel('Lecture');

        $TestVideo_condition = $this->TestVideo->find('count',array(
            'conditions' => array(
                'TestVideo.post_id' => $post_id
                // ,
                // 'TestVideo.status' => 1
            )));
        $lesson_condition = $this->Lesson->find('count',array(
            'conditions' => array(
                'Lesson.post_id' => $post_id,
            )));
        $lecture_condition = $this->Lecture->find('count',array(
            'conditions' => array(
                'Lecture.post_id' => $post_id,
            )));

        if($post_details['Post']['course_goals']!=''){
            $course_status['course_goals'] = 1;
        }
        if($post_details['Post']['automatic_message']!=''){
            $course_status['automatic_message'] = 1;
        }
        // if($post_details['Post']['price']!=''){
            $course_status['price_coupons'] = 1;
        // }
        if($post_details['Post']['landing_page_status']!= 0 ){
            $course_status['landing_page_status'] = 1;
        }
        if ($TestVideo_condition>0){
            $course_status['test_video'] = 1;
        }
        if ($lesson_condition>0 && $lecture_condition>0){
            $course_status['curriculum'] = 1;
        }

        return $course_status;
    }

    //wishlist add........................................
      public function addtowishlist() {
           $this->loadModel('Wishlist');
        $this->layout = false;
        $this->autoRender = false;
        //pr($_POST); exit;
        $userid = $this->Session->read('user_id');
        $this->request->data['Wishlist']['user_id'] = $userid;
        $this->request->data['Wishlist']['post_id'] = $_POST['post_id'];

        $this->Wishlist->create();
        if ($this->Wishlist->save($this->request->data)) {

            echo 1;
        } else {
            echo 0;
        }
        exit;

    }

     public function removefromwishlist() {
           $this->loadModel('Wishlist');
           $this->autoRender = false;
        $this->layout = false;
         $userid = $this->Session->read('user_id');
        //pr($_POST); exit;
       $this->Wishlist->deleteAll(array('Wishlist.user_id' => $userid,'Wishlist.post_id' => $_POST['post_id']));

        echo 1;
        exit;

    }

     public function addassignment($slug = NULL){
        $userid = $this->Session->read('userid');
        $this->loadModel('Assignment');

        if (!isset($userid) || $userid == '') {
            $this->redirect('/');
        }
        if (!$this->User->exists($userid)) {
            throw new NotFoundException(__('Invalid user'));
        }

        $post_dtls=$this->Post->find('first',array('conditions'=>array('Post.slug'=>$slug)));
        $post_id = $post_dtls['Post']['id'];

        $this->loadModel('AccessInstructor');
        $accessInstructor = $this->AccessInstructor->find('first',array('conditions'=>array('AccessInstructor.post_id'=>$post_id,'AccessInstructor.user_id'=>$userid)));
        if(!empty($accessInstructor)){
            if($accessInstructor['AccessInstructor']['edit']==0){
                return $this->redirect(array('controller'=>'posts', 'action' => 'overview',$slug));
            }
        }

        if(isset($this->request->data) && !empty($this->request->data)){
            $this->request->data1=array();
            $ids=$this->request->data['ids'];
            $questions=$this->request->data['questions'];

            $marks=$this->request->data['markss'];
            for($i=0;$i<count($questions);$i++)
            {
                $quesstionArr[$i]['id']=$ids[$i];
                $quesstionArr[$i]['question']=$questions[$i];
                $quesstionArr[$i]['marks']=$marks[$i];
            }

            $data = json_encode($quesstionArr);
            $this->request->data1['Assignment']['question']=$data;
            $this->request->data1['Assignment']['subject']=$this->request->data['subject'];
            $this->request->data1['Assignment']['total_marks']=$this->request->data['total_marks'];
            $this->request->data1['Assignment']['post_id']=$post_id;
            $this->Assignment->create();
            $this->Assignment->save($this->request->data1);

            $this->Session->setFlash(__('Assignment saved successfully.', 'default', array('class' => 'success')));

        }

        $course_status = $this->course_status($post_id);

        $topContent = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => 62)));

        $this->set(compact('post_dtls','course_status','topContent'));

    }
}
