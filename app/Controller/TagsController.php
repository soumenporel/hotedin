<?php

App::uses('AppController', 'Controller');

/**
 * Tags Controller
 *
 * @property Faq $Faq
 * @property PaginatorComponent $Paginator
 */
class TagsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array('Tag','Post');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Tag.id' => 'desc'
        )
    );

    public function admin_index() {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        //$this->Tag->recursive = 2;
        $this->Paginator->settings = $this->paginate;
        $this->set('tags', $this->Paginator->paginate('Tag'));
    }

    
    public function admin_view($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if (!$this->Tag->exists($id)) {
            throw new NotFoundException(__('Invalid Tag'));
        }
        $options = array('conditions' => array('Tag.' . $this->Tag->primaryKey => $id));
        $this->set('tag', $this->Tag->find('first', $options));
    }
    
    public function admin_add() {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $posts = $this->Post->find('list',array('fields'=>array('Post.post_title')));
        $this->set(compact('posts'));
        if ($this->request->is('post')) {
            $this->Tag->create();
            if ($this->Tag->save($this->request->data)) {
                $this->Session->setFlash(__('The Tag has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Tag could not be saved. Please, try again.'));
            }
        }
    }

    public function ajaxAddTag() {

        $data = array();
        
        if (!empty($this->request->data)) {
            $this->Tag->create();
            $tag['Tag']['name'] = $this->request->data['tag'];
            if ($this->Tag->save($tag)) {
               $data['Ack'] = 1;
               $data['res'] = 'Tag is Saved';
               $data['id'] = $this->Tag->getInsertID();
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Tag is Can not be Saved';
            }
        }

        echo json_encode($data);
        exit;
    }



    public function admin_edit($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        //$this->loadModel('FaqCategory');
        if (!$this->Tag->exists($id)) {
            throw new NotFoundException(__('Invalid Tag'));
        }
        
        $posts = $this->Post->find('list',array('fields'=>array('Post.post_title')));
        $this->set(compact('posts'));
        
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Tag->save($this->request->data)) {
                $this->Session->setFlash(__('The Tag has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Tag could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Tag.' . $this->Tag->primaryKey => $id));
            $this->request->data = $this->Tag->find('first', $options);
            //$faqcategories = $this->FaqCategory->find('all');
        }
        $this->set(compact('users', 'faqcategories'));
    }

    public function admin_delete($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Tag->id = $id;
        if (!$this->Tag->exists()) {
            throw new NotFoundException(__('Invalid Tag'));
        }

        $this->request->onlyAllow('post', 'delete');
        if ($this->Tag->delete()) {
            $this->Session->setFlash(__('The Tag has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Tag could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    
    
}
