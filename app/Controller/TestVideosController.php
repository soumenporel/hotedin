<?php

App::uses('AppController', 'Controller');

/**
 * TestVideos Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class TestVideosController extends AppController {
	public function ajaxTestVideoStatus(){
		$data = array();
		$this->loadModel('TestVideo'); 
		$video_id = $this->request->data['video_id'];
		$this->TestVideo->id = $video_id ;
		if($this->TestVideo->saveField('status', $this->request->data['status'])){
			$data['Ack'] = 1;
			if($this->request->data['status']==0){
				$stat = 'Video is Successfully Deactivated' ;
			}else{
				$stat = 'Video is Successfully Activated' ;
			}
			$data['mas'] = $stat;
			$data['stat'] = $this->request->data['status'];
		}
		else{
			$data['Ack'] = 0;
		}
		echo json_encode($data);
		exit;       
  	}

}	