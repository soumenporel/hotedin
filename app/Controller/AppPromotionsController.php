<?php

App::uses('AppController', 'Controller');

class AppPromotionsController extends AppController {

    public function admin_index() {

		$userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $apps = $this->AppPromotion->find('all');
        $this->set('apps',$apps);

	}

    public function admin_add() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if ($this->request->is('post')) {

            $arr = $this->request->data;
            $string = implode(',', $arr['dat']);
            $this->AppPromotion->create();
            $this->request->data['AppPromotion']['points'] = $string;
            if(isset($this->request->data['AppPromotion']['image']) && $this->request->data['AppPromotion']['image']['name']!=''){
                    $ext = explode('/',$this->request->data['AppPromotion']['image']['type']);
                    if($ext){
                        $uploadFolder = "appPromotions_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['AppPromotion']['image']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['AppPromotion']['image']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['AppPromotion']['image']['tmp_name'],$full_image_path);
                            $this->request->data['AppPromotion']['image'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['AppPromotion']['image'] = '';
            }

            if(isset($this->request->data['AppPromotion']['android']) && $this->request->data['AppPromotion']['android']['name']!=''){
                    $ext = explode('/',$this->request->data['AppPromotion']['android']['type']);
                    if($ext){
                        $uploadFolder = "appPromotions_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['AppPromotion']['android']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['AppPromotion']['android']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['AppPromotion']['android']['tmp_name'],$full_image_path);
                            $this->request->data['AppPromotion']['android'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload android playstore image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['AppPromotion']['android'] = '';
            }

            if(isset($this->request->data['AppPromotion']['ios']) && $this->request->data['AppPromotion']['ios']['name']!=''){
                    $ext = explode('/',$this->request->data['AppPromotion']['ios']['type']);
                    if($ext){
                        $uploadFolder = "appPromotions_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['AppPromotion']['ios']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['AppPromotion']['ios']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['AppPromotion']['ios']['tmp_name'],$full_image_path);
                            $this->request->data['AppPromotion']['ios'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload ios store image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['AppPromotion']['ios'] = '';
            }


            if ($this->AppPromotion->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        }

    }

    public function admin_edit($id= null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->AppPromotion->exists($id)) {
            throw new NotFoundException(__('Invalid content'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->AppPromotion->id = $id;
            if(isset($this->request->data['AppPromotion']['image']) && $this->request->data['AppPromotion']['image']['name']!=''){
                    $ext = explode('/',$this->request->data['AppPromotion']['image']['type']);
                    if($ext){
                        $uploadFolder = "appPromotions_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['AppPromotion']['image']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['AppPromotion']['image']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['AppPromotion']['image']['tmp_name'],$full_image_path);
                            $this->request->data['AppPromotion']['image'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['AppPromotion']['image'] = $this->request->data['AppPromotion']['hide_image'];
            }

            if(isset($this->request->data['AppPromotion']['android']) && $this->request->data['AppPromotion']['android']['name']!=''){
                    $ext = explode('/',$this->request->data['AppPromotion']['android']['type']);
                    if($ext){
                        $uploadFolder = "appPromotions_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['AppPromotion']['android']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['AppPromotion']['android']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['AppPromotion']['android']['tmp_name'],$full_image_path);
                            $this->request->data['AppPromotion']['android'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload android playstore image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['AppPromotion']['android'] = $this->request->data['AppPromotion']['hide_android'];
            }

            if(isset($this->request->data['AppPromotion']['ios']) && $this->request->data['AppPromotion']['ios']['name']!=''){
                    $ext = explode('/',$this->request->data['AppPromotion']['ios']['type']);
                    if($ext){
                        $uploadFolder = "appPromotions_image";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['AppPromotion']['ios']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['AppPromotion']['ios']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['AppPromotion']['ios']['tmp_name'],$full_image_path);
                            $this->request->data['AppPromotion']['ios'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload ios store image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['AppPromotion']['ios'] = $this->request->data['AppPromotion']['hide_ios'];
            }
            $arr = $this->request->data;
            $string = implode(',', $arr['dat']);
            $this->request->data['AppPromotion']['points'] = $string;
            if ($this->AppPromotion->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.', array('class' => 'success')));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));

            }
        } else {
            $options = array('conditions' => array('AppPromotion.' . $this->AppPromotion->primaryKey => $id));
            $this->request->data = $this->AppPromotion->find('first', $options);
        }
    }

    public function admin_delete($id = null) {
        $is_admin = $this->Session->read('is_admin');
        $this->loadModel('AppPromotion');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $this->AppPromotion->id = $id;
        if (!$this->AppPromotion->exists()) {
                throw new NotFoundException(__('Invalid'));
        }
        $howitwork_img=$this->AppPromotion->find('first',array('conditions'=>array('AppPromotion.id'=>$id)));
        $howitwork_img_unlink = $howitwork_img['AppPromotion']['image'];
        $uploadFolder = "howitworks_image";
        $uploadPath = WWW_ROOT . $uploadFolder;
        if($howitwork_img_unlink!='' && file_exists($uploadPath . '/' . $howitwork_img_unlink)){
            unlink($uploadPath . '/' . $howitwork_img_unlink);
        }
        if ($this->AppPromotion->delete($id)) {
                $this->Session->setFlash(__('The AppPromotion has been deleted.'));
        } else {
                $this->Session->setFlash(__('The AppPromotion could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));

    }


}
