<?php
App::uses('AppController', 'Controller');
/**
 * Analytics Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class AnnouncementsController extends AppController 
{

public $components = array('Paginator','Session');

  public function ajaxSaveAnnouncement(){
    
    $data = array();
    
    $image = $_FILES;
      if (!empty($image['announcement_media']['name'])) {
          $pathpart = pathinfo($image['announcement_media']['name']);
          $ext = $pathpart['extension'];
          $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
          if (in_array(strtolower($ext), $extensionValid)) {
              $uploadFolder = "announcement_media";
              $uploadPath = WWW_ROOT . $uploadFolder;
              $filename = uniqid() . '.' . $ext;
              $full_flg_path = $uploadPath . '/' . $filename;
                if(move_uploaded_file($image['announcement_media']['tmp_name'], $full_flg_path)){
                  $this->request->data['Announcement']['media'] = $filename;
                  $this->request->data['Announcement']['post_time'] = date('Y-m-d H:i:s');
                  $this->Announcement->create();
                    if($this->Announcement->save($this->request->data)){
                      $data['Ack'] = 1;
                      $data['res'] = 'Announcement has been saved successfully';
                    }
                }
          } else {
              $data['Ack'] = 0;
              $data['res'] = 'Invalide Image Type';
          }
      } else {
          $data['Ack'] = 0;
          $data['res'] = 'No Image';
      }
     echo json_encode($data);
     exit;
  }

}