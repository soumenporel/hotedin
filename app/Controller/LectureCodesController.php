<?php

App::uses('AppController', 'Controller');

/**
 * LectureCodes Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class LectureCodesController extends AppController {

  

    public function ajaxAddLectureCode(){
        
        $data = array();
        $code = $_FILES;
        $lecture = $this->request->data['lacture_id'];
        $post_id = $this->request->data['post_id'];
        if(!empty($code)){

            if (!empty($code['code_file']['name'])) {
                $pathpart = pathinfo($code['code_file']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('docx','doc','pdf');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "lecture_asset";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($code['code_file']['tmp_name'], $full_flg_path);
                    
                    $lecture_code['LectureCode']['file'] = $filename;
                    $lecture_code['LectureCode']['lecture_id'] = $lecture;
                    $lecture_code['LectureCode']['post_id'] = $post_id;
                    $lecture_code['LectureCode']['date'] = gmdate('Y-m-d');
                    $this->LectureCode->create();
                    if($this->LectureCode->save($lecture_code)){
                        $data['Ack'] = 1;
                        $data['res'] = 'Lecture Source Code Has Been Saved successfuly.';
                    }
                    
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Invalid image type.';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Error';
            }
        
        }
        echo json_encode($data);
        exit;
    }

    public function ajaxAddLectureCodeLibrary(){
        
        $data = array();
        $lecture = $this->request->data['lecture_id'];
        $post_id = $this->request->data['post_id'];
       
        $lecture_code['LectureCode']['file'] = $this->request->data['name'];
        $lecture_code['LectureCode']['lecture_id'] = $lecture;
        $lecture_code['LectureCode']['post_id'] = $lecture;
        $lecture_code['LectureCode']['date'] = gmdate('Y-m-d');
        $this->LectureCode->create();
        if($this->LectureCode->save($lecture_code)){
            $data['Ack'] = 1;
            $data['res'] = 'Lecture Source Code Has Been Saved successfuly.';
        }
        else {
        $data['Ack'] = 0;
        $data['res'] = 'Error!!!';
        }
            
        echo json_encode($data);
        exit;
    }
}

?>    