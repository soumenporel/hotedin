<?php
App::uses('AppController', 'Controller');
/**
 * Analytics Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class AnnouncementCommentsController extends AppController 
{

public $components = array('Paginator','Session');

  public function ajaxSaveAnnouncementComment(){

    $data = array();

    $value['announcement_id']   = $this->request->data['a_id'];
    $value['user_id']           = $this->request->data['userid'];
    $value['comment']           = $this->request->data['a_text'];
    $value['comment_time']      = gmdate('Y-m-d H:i:s');

    if($this->AnnouncementComment->save($value)){
      $data['Ack'] = 1;
      $data['res'] = 'announcement comment is saved';
    }else{
      $data['Ack'] = 0;
      $data['res'] = 'Error...';
    }
    
    echo json_encode($data);
     exit;
  }

  public function ajaxDeleteComment(){
    $data = array();

    if( $this->AnnouncementComment->delete($this->request->data['c_id']) ){
      $data['Ack'] = 1;
      $data['res'] = ' Comment in deleted successfully ';
    }else{
      $data['Ack'] = 0;
      $data['res'] = ' Error... ';
    }
    echo json_encode($data);
    exit;
  }

  public function ajaxEditComment(){

    $data = array();

    $value['id']                = $this->request->data['c_id'];
    $value['announcement_id']   = $this->request->data['a_id'];
    $value['user_id']           = $this->request->data['userid'];
    $value['comment']           = $this->request->data['a_text'];
    //$value['comment_time']      = gmdate('Y-m-d H:i:s');

    if($this->AnnouncementComment->save($value)){
      $data['Ack'] = 1;
      $data['res'] = 'announcement comment is saved';
    }else{
      $data['Ack'] = 0;
      $data['res'] = 'Error...';
    }
    
    echo json_encode($data);
     exit;
  }

}