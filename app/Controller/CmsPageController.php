<?php

App::uses('AppController', 'Controller');

/**
 * CmsPages Controller
 *
 * @property CmsPage $CmsPage
 * @property PaginatorComponent $Paginator
 */
class CmsPageController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->CmsPage->recursive = 0;
        $this->set('content', $this->Paginator->paginate());
    }

    public function display($slug = NULL) {
        $cmsPage = $this->CmsPage->find('first', array(
            'conditions' => array(
                'CmsPage.slug' => $slug
            )
        ));
        if (!$cmsPage) {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
        $this->set(compact('cmsPage'));
    }

    public function preview($slug = NULL) {
        $cmsPage = $this->CmsPage->find('first', array(
            'conditions' => array(
                'CmsPage.slug' => $slug
            )
        ));
        if (!$cmsPage) {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
        $this->set(compact('cmsPage'));
    }

    public function admin_index() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $is_active = $this->request->data['search_is_active'];
        } else {
            $is_active = '';
        }

        $QueryStr = '1';
        if ($keywords != '') {
            $QueryStr.=" AND (CmsPage.page_title LIKE '%" . $keywords . "%')";
        }
        if ($is_active != '') {
            $QueryStr.=" AND (CmsPage.status = '" . $is_active . "')";
        }
        $condition = array($QueryStr);

        $title_for_layout = 'CmsPage List';

        $options = array(
            'conditions' => $condition,
            'order' => array(
                'CmsPage.order' => 'ASC'
            )
        );

        $this->Paginator->settings = $options;

        $this->CmsPage->recursive = 0;
        $this->set('contents', $this->Paginator->paginate());
        $this->set(compact('title_for_layout', 'keywords', 'is_active'));
    }

    public function admin_export() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $contents = $this->CmsPage->find('all');

        $output = '';
        $output .='Page Heading, CmsPage';
        $output .="\n";

        if (!empty($contents)) {
            foreach ($contents as $content) {
                $output .='"' . html_entity_decode($content['CmsPage']['page_heading']) . '","' . strip_tags($content['CmsPage']['content']) . '"';
                $output .="\n";
            }
        }
        $filename = "contents" . time() . ".csv";
        header('CmsPage-type: application/csv');
        header('CmsPage-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($page_name = null) {
        $this->loadModel('Contact');
        $this->loadModel('User');
        if (isset($page_name) && $page_name != '') {
            $options = array('conditions' => array('CmsPage.page_title' => $page_name));
            $content = $this->CmsPage->find('first', $options);
            if ($this->request->is(array('post', 'put'))) {
                $this->request->data['Contact']['contact_date'] = date('Y-m-d');
                $UserEmail = $this->request->data['Contact']['email'];
                $UserID = $this->request->data['Contact']['user_id'];
                if ($UserID == '') {
                    $options = array('conditions' => array('User.email' => $UserEmail));
                    $userEmailDetails = $this->User->find('first', $options);
                    //pr($userEmailDetails);
                    if (count($userEmailDetails) > 0) {
                        $this->request->data['Contact']['user_id'] = $userEmailDetails['User']['id'];
                    } else {
                        $this->request->data['Contact']['user_id'] = 0;
                    }
                }
                if ($this->request->data['Contact']['type'] == 2) {
                    $link_data = $this->request->data['Contact']['link'];
                    $ExpLink_data = end(explode('/', $link_data));
                    if ($ExpLink_data != '') {
                        $task_id = base64_decode($ExpLink_data);
                    } else {
                        $task_id = 0;
                    }
                    $this->request->data['Contact']['task_id'] = $task_id;
                } else {
                    $this->request->data['Contact']['task_id'] = 0;
                }

                if ($this->Contact->save($this->request->data)) {
                    $this->Session->setFlash(__('Thank you, we\'ll get back to you shortly.'));
                    return $this->redirect(array('action' => 'view/' . $page_name));
                } else {
                    $this->Session->setFlash(__('Your details could not be saved. Please, try again.'));
                }
            }
            if ($content) {
                $title_for_layout = $content['CmsPage']['page_heading'];
                $page_title = $content['CmsPage']['page_title'];
                $this->set(compact('title_for_layout', 'content', 'page_title'));
            }
        } else {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
    }

    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'CmsPage View';
        if (!$this->CmsPage->exists($id)) {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
        $options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
        $content = $this->CmsPage->find('first', $options);
        $this->set(compact('title_for_layout', 'content'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->Category->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->request->data['CmsPage']['slug'] = str_replace(' ', '-',  strtolower($this->request->data['CmsPage']['page_title']));
            $this->CmsPage->create();
            if ($this->CmsPage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $users = $this->Category->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_edit($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->CmsPage->exists($id)) {
            throw new NotFoundException(__('Invalid content'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->CmsPage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.', array('class' => 'success')));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
            $this->request->data = $this->CmsPage->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Category->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->CmsPage->id = $id;
        if (!$this->CmsPage->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->CmsPage->delete($id)) {
            $this->Session->setFlash(__('The content has been deleted.'));
        } else {
            $this->Session->setFlash(__('The content could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function how_it_works() {

    }

    public function faq() {
        $this->loadModel('Faq');
        $this->loadModel('FaqCategory');
        $title_for_layout = 'Faq';
        $CatOptions = array('conditions' => array('FaqCategory.active' => 1, 'FaqCategory.parent_id' => 0));
        $FaqCat = $this->FaqCategory->find('all', $CatOptions);
        //$FaqOptions = array('conditions' => array('Faq.is_active' => 1));
        //$FaqData = $this->Faq->find('all', $FaqOptions);
        $this->set(compact('title_for_layout', 'FaqCat'));
    }

    public function faq_category_wise($id = null) {
        $this->loadModel('Faq');
        $FaqOptions = array('conditions' => array('Faq.is_active' => 1, 'Faq.faq_category_id' => $id));
        $FaqData = $this->Faq->find('all', $FaqOptions);
        return $FaqData;
        exit;
    }

    public function blog() {

    }

    public function single_blog() {

    }

    public function admin_ajaxorder() {
        $this->layout = false;
        $order = 1;
        foreach($this->request->data['sort_order'] as $id) {
            $data['CmsPage']['id'] = $id;
            $data['CmsPage']['order'] = $order;
            $this->CmsPage->save($data);
            $order++;
        }
        die();
    }

    public function bulkAction(){
        $data = array();
        if(!empty($this->request->data)){
            if($this->request->data['action_type']==1){
                //delete
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->CmsPage->id = $value;
                    $this->CmsPage->delete();
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Deleted';
            }
            if($this->request->data['action_type']==2){
                //approve
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->CmsPage->id = $value;
                    $this->CmsPage->saveField('status', 1);
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Approved';

            }
            if($this->request->data['action_type']==3){
                //disapprove
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->CmsPage->id = $value;
                    $this->CmsPage->saveField('status', 0);
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Disapprove';

            }
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = 'Error..';
        }
      echo json_encode($data);
      exit;
    }

    public function ajaxCMSPrewiew(){
         $id = $this->request->data['page_id'];
         $options = array('conditions' => array('CmsPage.id' => $id));
         $data = $this->CmsPage->find('first', $options);
         if(!empty($data)){
            $res['html'] = $data['CmsPage']['page_description'];
            $res['Ack'] = 1;
         }
         else
         {
            $res['Ack'] = 0;
         }
         echo json_encode($res); exit;
    }

        public function aboutus() {

            $this->loadModel('Client');
            $Clients = $this->Client->find('all', array(
                                                'conditions'=>array('Client.feature'=>1),
                                                'limit' => 5
                                                ));
            $this->loadModel('Blog');
            $blogs = $this->Blog->find('all',array(
                                        'conditions' => array('is_feature'=>3),
                                        'limit' => 3
                                    ));
            $this->loadModel('ManageHomepage');
            $homecontent3 = $this->ManageHomepage->find('first', array(
                                                      'conditions'=>array('id'=>4)
                                                    ));
            $this->loadModel('Pagebanner');
            $pagebanner = $this->Pagebanner->find('first', array(
                                                      'conditions'=>array('Pagebanner.id'=>6),
                                                      array('Pagebanner.status' => 1)
                                                    ));

            $this->loadModel('WpPages');
            $wppages1 = $this->WpPages->find('first', array(
                                                      'conditions'=>array('id'=>4)
                                                    ));
            $this->loadModel('WpPages');
            $wppages2 = $this->WpPages->find('first', array(
                                            'conditions'=>array('id'=>17)
                                            ));
            $this->loadModel('WpPages');
            $wppages3 = $this->WpPages->find('first', array(
                                          'conditions'=>array('id'=>19)
                                        ));

            $this->set(compact('Clients', 'pagebanner', 'blogs','homecontent3','wppages1', 'wppages2', 'wppages3'));
    }

    public function employer() {
        $this->loadModel('Client');
        $Clients = $this->Client->find('all', array(
                                            'conditions'=>array('Client.feature'=>1),
                                            'limit' => 5
                                            ));
        $this->loadModel('Blog');
        $blogs = $this->Blog->find('all',array(
                                    'conditions' => array('is_feature'=>3),
                                    'limit' => 3
                                ));
        $this->loadModel('ManageHomepage');
        $homecontent3 = $this->ManageHomepage->find('first', array(
                                                  'conditions'=>array('id'=>4)
                                                ));

        $homecontent = $this->ManageHomepage->find('first', array(
                                                'conditions'=>array('id'=>1)
                                                ));

        $this->loadModel('WpPages');
        $this->loadModel('Pagebanner');

        $pagebanner = $this->Pagebanner->find('first', array(
                                                  'conditions'=>array('Pagebanner.id'=>44),
                                                  array('Pagebanner.status' => 1)
                                                ));

        $wppages1 = $this->WpPages->find('first', array(
                                        'conditions'=>array('id'=>20)
                                        ));
        $wppages2 = $this->WpPages->find('first', array(
                                        'conditions'=>array('id'=>21)
                                        ));

        $this->loadModel('EmployerMembershipPlan');
        $plans = $this->EmployerMembershipPlan->find('all');

        $this->set(compact('Clients', 'pagebanner','blogs','homecontent3','wppages1', 'wppages2', 'homecontent','plans'));

    }



}
