<?php
App::uses('AppController', 'Controller');

/**
 * Trainingproviders Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SolutionsController extends AppController {
    
    public $uses = array('User','CmsPage','Pagebanner');
    public $components = array('Paginator');
    
    public function business() {

        if($this->request->is('post')){

            $this->loadModel('Setting');                
            $contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
            if ($contact_email) {
                $adminEmail = $contact_email['Setting']['site_email'];
            } else {
                $adminEmail = 'superadmin@abc.com';
            }
            
            $options = array('conditions' => array('User.id' => $this->User->getLastInsertId()));
            $lastInsetred = $this->User->find('first', $options);

            $this->loadModel('EmailTemplate');
            $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 10)));
            $siteurl = Configure::read('SITE_URL');
            
            $NAME = $this->request->data['full_name'] ;
            $COMPANYNAME = $this->request->data['company_name'] ;
            $COMPANYEMAIL = $this->request->data['company_email'] ;
            $OFFICENUMBER = $this->request->data['office_number'] ;
            $MOBILENUMBER = $this->request->data['mobile_number'] ;
            $INDUSTRY = $this->request->data['industry'];
            $COMPANYSIZE = $this->request->data['company_size'] ;
            $HOWMANYPEOPLE = $this->request->data['how_many_people'] ;

            $msg_body = str_replace(array('[NAME]','[COMPANYNAME]', '[COMPANYEMAIL]', '[OFFICENUMBER]','[MOBILENUMBER]', '[INDUSTRY]', '[COMPANYSIZE]', '[HOWMANYPEOPLE]'), array($NAME, $COMPANYNAME, $COMPANYEMAIL,$OFFICENUMBER, $MOBILENUMBER, $INDUSTRY, $COMPANYSIZE, $HOWMANYPEOPLE), $EmailTemplate['EmailTemplate']['content']);

    
            
            $from = 'noreply@studilmu.com';
            $Subject_mail = $EmailTemplate['EmailTemplate']['subject'];

            $Email = new CakeEmail('gmail');
            $Email->from(array('noreply@studilmu.com' => 'studilmu.com'))
                    ->emailFormat('html')
                    ->to($adminEmail)
                    ->subject($Subject_mail);
                    
            
            if($Email->send($msg_body)){
                $this->Session->setFlash('Business Inqury Has Been Saved Successfuly.', 'default', array('class' => 'success'));
            }else{
                $this->Session->setFlash(__('Unabale To Send Mail.Please Try Again', 'default', array('class' => 'error')));
            }

        }

        $bannerContent = $this->CmsPage->find('first', array(
            'conditions' => array(
                'CmsPage.id' => 42
            )
        ));

        $bannerMenu  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>43)));

        $pageContent = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>44)));

        $pageHeader  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>50)));

        $this->loadModel('Testimonial');
        $this->Testimonial->recursive = 2;
        $optionstestimonial = array('conditions' => array('`Testimonial`.status' => 1), 'limit' => 3 ,'group' => '`Testimonial`.`id` desc');
        $testimonialdata = $this->Testimonial->find('all', $optionstestimonial);

        $this->loadModel('Faq');
        $this->Faq->recursive = 2;
        $faqs = $this->Faq->find('all',array('conditions'=>array('Faq.faqcategory_id'=>3)));
        
        //for industry..................................................
        $this->loadModel('Industry');
        $industries = $this->Industry->find('all');
        
         $page_id=8; 
           $banners = $this->Pagebanner->find('all', array(
            'conditions' => array(
                'Pagebanner.page_id' => $page_id
            )
        ));
        $this->set(compact('testimonialdata','faqs','bannerContent','bannerMenu','pageContent','pageHeader','industries','banners'));
        
    }

    public function university() {

        if($this->request->is('post')){
            // pr($this->request->data); exit;
            $this->loadModel('Setting');                
            $contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
            if ($contact_email) {
                $adminEmail = $contact_email['Setting']['site_email'];
            } else {
                $adminEmail = 'superadmin@abc.com';
            }
            
            $options = array('conditions' => array('User.id' => $this->User->getLastInsertId()));
            $lastInsetred = $this->User->find('first', $options);

            $this->loadModel('EmailTemplate');
            $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 12)));
            $siteurl = Configure::read('SITE_URL');
            
            $NAME = $this->request->data['full_name'] ;
            $INSTITUTE = $this->request->data['institution_name'] ;
            $COMPANYEMAIL = $this->request->data['company_email'] ;
            $OFFICENUMBER = $this->request->data['office_number'] ;
            $HOWMANYPEOPLE = $this->request->data['how_many_people'] ;

            $msg_body = str_replace(array('[NAME]','[INSTITUTE]', '[COMPANYEMAIL]', '[OFFICENUMBER]','[HOWMANYPEOPLE]'), array($NAME, $INSTITUTE, $COMPANYEMAIL,$OFFICENUMBER, $HOWMANYPEOPLE), $EmailTemplate['EmailTemplate']['content']);

    
            
            $from = 'noreply@studilmu.com';
            $Subject_mail = $EmailTemplate['EmailTemplate']['subject'];

            $Email = new CakeEmail('gmail');
            $Email->from(array('noreply@studilmu.com' => 'studilmu.com'))
                    ->emailFormat('html')
                    ->to($adminEmail)
                    ->subject($Subject_mail);

            if($Email->send($msg_body)){
                $this->Session->setFlash('University Inqury Has Been Saved Successfuly.', 'default', array('class' => 'success'));
            }else{
                $this->Session->setFlash(__('Unabale To Send Mail.Please Try Again', 'default', array('class' => 'error')));
            }        

                    
        }

        $bannerContent = $this->CmsPage->find('first', array(
            'conditions' => array(
                'CmsPage.id' => 47
            )
        ));

        $bannerMenu  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>48)));

        $pageContent = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>49)));

        $pageHeader  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>52)));

        $this->loadModel('Testimonial');
        $this->Testimonial->recursive = 2;
        $optionstestimonial = array('conditions' => array('`Testimonial`.status' => 1), 'limit' => 3 ,'group' => '`Testimonial`.`id` desc');
        $testimonialdata = $this->Testimonial->find('all', $optionstestimonial);

        $this->loadModel('Faq');
        $this->Faq->recursive = 2;
        $faqs = $this->Faq->find('all',array('conditions'=>array('Faq.faqcategory_id'=>5)));
         $page_id=9; 
           $banners = $this->Pagebanner->find('all', array(
            'conditions' => array(
                'Pagebanner.page_id' => $page_id
            )
        ));
        $this->set(compact('testimonialdata','faqs','bannerContent','pageContent','bannerMenu','pageHeader','banners'));
        
    }

    public function government() {


        if($this->request->is('post')){
            
            $this->loadModel('Setting');                
            $contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
            if ($contact_email) {
                $adminEmail = $contact_email['Setting']['site_email'];
            } else {
                $adminEmail = 'superadmin@abc.com';
            }
            
            $options = array('conditions' => array('User.id' => $this->User->getLastInsertId()));
            $lastInsetred = $this->User->find('first', $options);

            $this->loadModel('EmailTemplate');
            $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 11)));
            $siteurl = Configure::read('SITE_URL');
            
            $NAME = $this->request->data['full_name'] ;
            $GOVERNMENT = $this->request->data['government_name'] ;
            $COMPANYEMAIL = $this->request->data['company_email'] ;
            $OFFICENUMBER = $this->request->data['office_number'] ;
            $HOWMANYPEOPLE = $this->request->data['how_many_people'] ;

            $msg_body = str_replace(array('[NAME]','[GOVERNMENT]', '[COMPANYEMAIL]', '[OFFICENUMBER]','[MOBILENUMBER]'), array($NAME, $GOVERNMENT, $COMPANYEMAIL,$OFFICENUMBER, $MOBILENUMBER), $EmailTemplate['EmailTemplate']['content']);

    
            
            $from = 'noreply@studilmu.com';
            $Subject_mail = $EmailTemplate['EmailTemplate']['subject'];

            $Email = new CakeEmail('gmail');
            $Email->from(array('noreply@studilmu.com' => 'studilmu.com'))
                    ->emailFormat('html')
                    ->to($adminEmail)
                    ->subject($Subject_mail)
                    ->send($msg_body);
                    
        }

        $bannerContent = $this->CmsPage->find('first', array(
            'conditions' => array(
                'CmsPage.id' => 45
            )
        ));

        // $bannerMenu  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>43)));

        $pageContent = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>46)));

        $pageHeader  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>51)));

        $this->loadModel('Testimonial');
        $this->Testimonial->recursive = 2;
        $optionstestimonial = array('conditions' => array('`Testimonial`.status' => 1), 'limit' => 3 ,'group' => '`Testimonial`.`id` desc');
        $testimonialdata = $this->Testimonial->find('all', $optionstestimonial);

        $this->loadModel('Faq');
        $this->Faq->recursive = 2;
        $faqs = $this->Faq->find('all',array('conditions'=>array('Faq.faqcategory_id'=>4)));
         $page_id=10; 
           $banners = $this->Pagebanner->find('all', array(
            'conditions' => array(
                'Pagebanner.page_id' => $page_id
            )
        ));
        $this->set(compact('testimonialdata','faqs','bannerContent','pageContent','pageHeader','banners'));
        
    }
    
       public function membershippackage() {

        if($this->request->is('post')){
            // pr($this->request->data); exit;
            $this->loadModel('Setting');                
            $contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
            if ($contact_email) {
                $adminEmail = $contact_email['Setting']['site_email'];
            } else {
                $adminEmail = 'superadmin@abc.com';
            }
            
            $options = array('conditions' => array('User.id' => $this->User->getLastInsertId()));
            $lastInsetred = $this->User->find('first', $options);

            $this->loadModel('EmailTemplate');
            $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 12)));
            $siteurl = Configure::read('SITE_URL');
            
            $NAME = $this->request->data['full_name'] ;
            $INSTITUTE = $this->request->data['institution_name'] ;
            $COMPANYEMAIL = $this->request->data['company_email'] ;
            $OFFICENUMBER = $this->request->data['office_number'] ;
            $HOWMANYPEOPLE = $this->request->data['how_many_people'] ;

            $msg_body = str_replace(array('[NAME]','[INSTITUTE]', '[COMPANYEMAIL]', '[OFFICENUMBER]','[HOWMANYPEOPLE]'), array($NAME, $INSTITUTE, $COMPANYEMAIL,$OFFICENUMBER, $HOWMANYPEOPLE), $EmailTemplate['EmailTemplate']['content']);

    
            
            $from = 'noreply@studilmu.com';
            $Subject_mail = $EmailTemplate['EmailTemplate']['subject'];

            $Email = new CakeEmail('gmail');
            $Email->from(array('noreply@studilmu.com' => 'studilmu.com'))
                    ->emailFormat('html')
                    ->to($adminEmail)
                    ->subject($Subject_mail);

            if($Email->send($msg_body)){
                $this->Session->setFlash('University Inqury Has Been Saved Successfuly.', 'default', array('class' => 'success'));
            }else{
                $this->Session->setFlash(__('Unabale To Send Mail.Please Try Again', 'default', array('class' => 'error')));
            }        

                    
        }

        $bannerContent = $this->CmsPage->find('first', array(
            'conditions' => array(
                'CmsPage.id' => 47
            )
        ));

        $bannerMenu  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>48)));

        $pageContent = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>49)));

        $pageHeader  = $this->CmsPage->find('first',array('conditions'=>array('CmsPage.id'=>52)));

        $this->loadModel('Testimonial');
        $this->Testimonial->recursive = 2;
        $optionstestimonial = array('conditions' => array('`Testimonial`.status' => 1), 'limit' => 3 ,'group' => '`Testimonial`.`id` desc');
        $testimonialdata = $this->Testimonial->find('all', $optionstestimonial);

        $this->loadModel('Faq');
        $this->Faq->recursive = 2;
        $faqs = $this->Faq->find('all',array('conditions'=>array('Faq.faqcategory_id'=>5)));
         $page_id=19; 
           $banners = $this->Pagebanner->find('all', array(
            'conditions' => array(
                'Pagebanner.page_id' => $page_id
            )
        ));
           
        $this->loadModel('MembershipPlan');
        $this->MembershipPlan->recursive = 2;
        $mPlans = $this->MembershipPlan->find('all');
        //print_r($mPlans);die;

        $this->loadModel('MembershipItem');
        $this->loadModel('PlanItem');
        $this->MembershipItem->recursive = 2;
        $planItems = $this->MembershipItem->find('all',array('order'=>array('MembershipItem.order'=>'ASC')));
        $userid = $this->Session->read('userid');
        if(isset($userid))
        {
        $userDetails = $this->User->find('first',array('conditions'=>array('User.id'=>$userid))); 
        }
        else
        {
          $userDetails = array();  
        }
        
        $this->set(compact('testimonialdata','faqs','bannerContent','pageContent','bannerMenu','pageHeader','banners','mPlans','planItems','userDetails'));
        
    }
    
}