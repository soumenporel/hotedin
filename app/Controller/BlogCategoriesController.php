<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class BlogCategoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
   public $components = array('Paginator');
    var $uses = array('Blog', 'BlogCategory','BlogComment');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->BlogCategory->recursive = 0;
        $this->set('categories', $this->Paginator->paginate());
    }

    public function admin_index() {
       
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Country'])) {
            $Country = $this->request->data['Country'];
        } else {
            $Country = '';
        }
        $QueryStr = '';
        if ($keywords != '') {
            $QueryStr.=" AND (BlogCategory.category_name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (BlogCategory.status = '" . $Newsearch_is_active . "')";
        }
        if ($Country != '') {
            $QueryStr.=" AND (BlogCategory.country_id=" . $Country . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('BlogCategory.category_name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Category List';
        $this->BlogCategory->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('BlogCategory'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Country'));
    }

    public function admin_list_subcategory() {
        $this->loadModel('Country');
        $countries = $this->Country->find('list');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Country'])) {
            $Country = $this->request->data['Country'];
        } else {
            $Country = '';
        }
        $QueryStr = "(BlogCategory.parent_id != '" . 0 . "')";
        if ($keywords != '') {
            $QueryStr.=" AND (BlogCategory.category_name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (BlogCategory.status = '" . $Newsearch_is_active . "')";
        }
        if ($Country != '') {
            $QueryStr.=" AND (BlogCategory.country_id=" . $Country . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('BlogCategory.category_name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Category List';
        $this->BlogCategory->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('BlogCategory'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Country'));
    }

    public function admin_subcategories($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Sub Category List';
        $options = array('conditions' => array('BlogCategory.id' => $id));
        $categoryname = $this->BlogCategory->find('list', $options);
        if ($categoryname) {
            $categoryname = $categoryname[$id];
        } else {
            $categoryname = '';
        }
        //$this->BlogCategory->recursive = 0;
        $this->set('categories', $this->Paginator->paginate('BlogCategory', array('BlogCategory.parent_id' => $id)));
        $this->set(compact('title_for_layout', 'categoryname', 'id'));
    }

    public function admin_exportsub($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $categories = $this->BlogCategory->find('all');

        $output = '';
        $output .='Name, Status';
        $output .="\n";

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $isactive = ($category['BlogCategory']['active'] == 1 ? 'Active' : 'Inactive');

                $output .='"' . $category['BlogCategory']['name'] . '","' . $isactive . '"';
                $output .="\n";
            }
        }
        $filename = "categories" . time() . ".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->BlogCategory->exists($id)) {
            throw new NotFoundException(__('Invalid Category'));
        }
        $options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
        $this->set('category', $this->BlogCategory->find('first', $options));
    }

    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Category View';
        if (!$this->BlogCategory->exists($id)) {
            throw new NotFoundException(__('Invalid Category'));
        }
        $options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
        $category = $this->BlogCategory->find('first', $options);
        #pr($category);
        if ($category) {
            $options = array('conditions' => array('BlogCategory.id' => $category['BlogCategory']['parent_id']));
            $categoryname = $this->BlogCategory->find('list', $options);
            #pr($categoryname);
            if ($categoryname) {
                $categoryname = $category['BlogCategory']['category_name'];
            } else {
                $categoryname = '';
            }
        }
        $this->set(compact('title_for_layout', 'category', 'categoryname'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if ($this->request->is('post')) {
            $this->BlogCategory->create();
            if ($this->BlogCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->BlogCategory->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_add() {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        //print_r($country);
        $title_for_layout = 'Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('BlogCategory.category_name' => $this->request->data['BlogCategory']['category_name']));
            $name = $this->BlogCategory->find('first', $options);
            if (!$name) {

                if (!empty($this->request->data['BlogCategory']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['BlogCategory']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['BlogCategory']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $filename = '';
                }
               
                $this->BlogCategory->create();
                if ($this->BlogCategory->save($this->request->data)) {
                   
                    $this->Session->setFlash(__('The category has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category name already exists. Please, try again.'));
            }
        }
        $this->set(compact('title_for_layout'));
    }

    public function admin_add_sub_category() {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->BlogCategory->Country->find('list');
        $categories = $this->BlogCategory->find('list', array('fields' => array('BlogCategory.id', 'BlogCategory.category_name'),'conditions'=>array('BlogCategory.parent_id'=>0)));
        //print_r($country);
        $title_for_layout = 'Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('BlogCategory.category_name' => $this->request->data['BlogCategory']['category_name']));
            $name = $this->BlogCategory->find('first', $options);
            if (!$name) {

                if (!empty($this->request->data['BlogCategory']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['BlogCategory']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['BlogCategory']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['BlogCategory']['parent_id'] = $this->request->data['BlogCategory']['categories'] ? $this->request->data['BlogCategory']['categories'] : 0;
                $this->BlogCategory->create();
                if ($this->BlogCategory->save($this->request->data)) {
                    $this->request->data1['CategoryImage']['category_id'] = $this->BlogCategory->id;
                    //pr($this->request->data1);
                    //exit;
                    $this->BlogCategoryImage->save($this->request->data1);
                    $this->Session->setFlash(__('The category has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category name already exists. Please, try again.'));
            }
        }
        $this->set(compact('parents', 'title_for_layout', 'countries', 'categories'));
    }

    public function admin_addsubcategory($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Sub Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('BlogCategory.name' => $this->request->data['BlogCategory']['name'], 'BlogCategory.parent_id' => $this->request->data['BlogCategory']['parent_id']));
            $name = $this->BlogCategory->find('first', $options);
            if (!$name) {
                $this->BlogCategory->create();
                if ($this->BlogCategory->save($this->request->data)) {
                    $this->Session->setFlash(__('The sub category has been saved.'));
                    return $this->redirect(array('action' => 'subcategories', $id));
                } else {
                    $this->Session->setFlash(__('The sub category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The sub category name already exists. Please, try again.'));
            }
        }
        $options = array('conditions' => array('BlogCategory.id' => $id));
        $categoryname = $this->BlogCategory->find('list', $options);
        if ($categoryname) {
            $categoryname = $categoryname[$id];
        } else {
            $categoryname = '';
        }
        $this->set(compact('title_for_layout', 'categoryname', 'id'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->BlogCategory->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->BlogCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
            $this->request->data = $this->BlogCategory->find('first', $options);
        }
        $users = $this->BlogCategory->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_edit($id = null) {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        if (!$this->BlogCategory->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('BlogCategory.category_name' => $this->request->data['BlogCategory']['category_name'], 'BlogCategory.id <>' => $id));
            $name = $this->BlogCategory->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

            
                
                 //for header icon.....................................................
            
              
                if ($this->BlogCategory->save($this->request->data)) {
                    $this->Session->setFlash(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
            $is_parent = $this->BlogCategory->find('count', array('conditions' => array( 'BlogCategory.id' => $id)));
            $options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
            $this->request->data = $this->BlogCategory->find('first', $options);

            //print_r($this->request->data);
        }
       
    }

    public function admin_edit_subcat($id = null) {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->BlogCategory->Country->find('list');
        $categories = $this->BlogCategory->find('list', array('fields' => array('BlogCategory.id', 'BlogCategory.category_name'), 'conditions' => array('BlogCategory.id <>' => $id,'BlogCategory.parent_id '=>0)));
        //echo $id;exit;
        if (!$this->BlogCategory->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('BlogCategory.category_name' => $this->request->data['BlogCategory']['category_name'], 'BlogCategory.id <>' => $id));
            $name = $this->BlogCategory->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

                if (!empty($this->request->data['BlogCategory']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['BlogCategory']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['BlogCategory']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                        $this->request->data1['CategoryImage']['id'] = $this->request->data['BlogCategory']['categoryimage_id'];
                        $this->request->data1['CategoryImage']['category_id'] = $id;
                        $this->BlogCategoryImage->save($this->request->data1);
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->request->data['BlogCategory']['image'] = $this->request->data['BlogCategory']['hide_img'];
                }

                $this->request->data['BlogCategory']['parent_id'] = $this->request->data['BlogCategory']['categories'];
                if ($this->BlogCategory->save($this->request->data)) {
                    $this->Session->setFlash(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
            $is_parent = $this->BlogCategory->find('count', array('conditions' => array('BlogCategory.parent_id' => 0, 'BlogCategory.id' => $id)));
            $options = array('conditions' => array('BlogCategory.' . $this->BlogCategory->primaryKey => $id));
            $this->request->data = $this->BlogCategory->find('first', $options);

            //print_r($this->request->data);
        }
        $this->set(compact('is_parent', 'countries', 'categories'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->BlogCategory->id = $id;
        if (!$this->BlogCategory->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->BlogCategory->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->BlogCategory->id = $id;
        if (!$this->BlogCategory->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
       
        if ($this->BlogCategory->delete($id)) {
            $this->Session->setFlash(__('The category has been deleted.' ,'default', array(), 'good'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    ///////////////////////////////AK///////////
    public function admin_export() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $options = array('BlogCategory.id !=' => 0);
        $cats = $this->BlogCategory->find('all', array('conditions' => $options));
        $output = '';
        $output .='Category Name, Parent Name, Is Active';
        $output .="\n";
//pr($cats);exit;
        if (!empty($cats)) {
            foreach ($cats as $cat) {
                $isactive = ($cat['BlogCategory']['active'] == 1) ? 'Yes' : 'No';

                $output .='"' . $cat['BlogCategory']['name'] . '","' . $cat['Parent']['name'] . '","' . $isactive . '"';
                $output .="\n";
            }
        }
        $filename = "categories" . time() . ".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    //////////////////////////AK///////////////////////

    public function ajaxSubCategory(){

        $data = array();
        $html = '';
        $category_id = $this->request->data['category_id'];
        $subCategories = $this->BlogCategory->find('all',array('conditions'=>array('BlogCategory.parent_id'=>$category_id)));
        if(!empty($subCategories)){
            $html .= '<option value="">(Sub-Category)</option>';
            foreach ($subCategories as $key => $subCategory) {
                 $subCategory['BlogCategory']['category_name'];
                 $html .= '<option value="'.$subCategory['BlogCategory']['id'].'">'.$subCategory['BlogCategory']['category_name'].'</option>';
             }
            $data['Ack'] = 1;
            $data['res'] = $html; 
        }
        else{
            $data['Ack'] = 0;
        }
        echo json_encode($data);      
        exit;
    }
}
