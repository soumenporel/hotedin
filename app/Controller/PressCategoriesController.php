<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class PressCategoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    var $uses = array('PressRelease', 'PressCategory');
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->PressCategory->recursive = 0;
        $this->set('categories', $this->Paginator->paginate());
    }

    public function admin_index() {
       
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Country'])) {
            $Country = $this->request->data['Country'];
        } else {
            $Country = '';
        }
        $QueryStr = '';
        if ($keywords != '') {
            $QueryStr.=" AND (PressCategory.category_name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (PressCategory.status = '" . $Newsearch_is_active . "')";
        }
        if ($Country != '') {
            $QueryStr.=" AND (PressCategory.country_id=" . $Country . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('PressCategory.category_name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Category List';
        $this->PressCategory->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('PressCategory'));
        $this->set(compact('title_for_layout', 'keywords', 'Newsearch_is_active'));
    }

    public function admin_list_subcategory() {
        $this->loadModel('Country');
        $countries = $this->Country->find('list');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Country'])) {
            $Country = $this->request->data['Country'];
        } else {
            $Country = '';
        }
        $QueryStr = "(PressCategory.parent_id != '" . 0 . "')";
        if ($keywords != '') {
            $QueryStr.=" AND (PressCategory.category_name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (PressCategory.status = '" . $Newsearch_is_active . "')";
        }
        if ($Country != '') {
            $QueryStr.=" AND (PressCategory.country_id=" . $Country . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('PressCategory.category_name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Category List';
        $this->PressCategory->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('PressCategory'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Country'));
    }

    public function admin_subcategories($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Sub Category List';
        $options = array('conditions' => array('PressCategory.id' => $id));
        $categoryname = $this->PressCategory->find('list', $options);
        if ($categoryname) {
            $categoryname = $categoryname[$id];
        } else {
            $categoryname = '';
        }
        //$this->PressCategory->recursive = 0;
        $this->set('categories', $this->Paginator->paginate('PressCategory', array('PressCategory.parent_id' => $id)));
        $this->set(compact('title_for_layout', 'categoryname', 'id'));
    }

    public function admin_exportsub($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $categories = $this->PressCategory->find('all');

        $output = '';
        $output .='Name, Status';
        $output .="\n";

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $isactive = ($category['PressCategory']['active'] == 1 ? 'Active' : 'Inactive');

                $output .='"' . $category['PressCategory']['name'] . '","' . $isactive . '"';
                $output .="\n";
            }
        }
        $filename = "categories" . time() . ".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->PressCategory->exists($id)) {
            throw new NotFoundException(__('Invalid Category'));
        }
        $options = array('conditions' => array('PressCategory.' . $this->PressCategory->primaryKey => $id));
        $this->set('category', $this->PressCategory->find('first', $options));
    }

    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Category View';
        if (!$this->PressCategory->exists($id)) {
            throw new NotFoundException(__('Invalid Category'));
        }
        $options = array('conditions' => array('PressCategory.' . $this->PressCategory->primaryKey => $id));
        $category = $this->PressCategory->find('first', $options);
        #pr($category);
        if ($category) {
            $options = array('conditions' => array('PressCategory.id' => $category['PressCategory']['parent_id']));
            $categoryname = $this->PressCategory->find('list', $options);
            #pr($categoryname);
            if ($categoryname) {
                $categoryname = $category['PressCategory']['category_name'];
            } else {
                $categoryname = '';
            }
        }
        $this->set(compact('title_for_layout', 'category', 'categoryname'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if ($this->request->is('post')) {
            $this->PressCategory->create();
            if ($this->PressCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->PressCategory->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_add() {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->PressCategory->Country->find('list');
        $categories = $this->PressCategory->find('list', array('fields' => array('PressCategory.id', 'PressCategory.category_name'),'conditions'=>array('PressCategory.parent_id '=>0)));
        //print_r($country);
        $title_for_layout = 'Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('PressCategory.category_name' => $this->request->data['PressCategory']['category_name']));
            $name = $this->PressCategory->find('first', $options);
            if (!$name) {

                if (!empty($this->request->data['PressCategory']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['PressCategory']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['PressCategory']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $filename = '';
                }
                
                //for header icon.....................................................
                if (!empty($this->request->data['PressCategory']['imagelogo']['name'])) {
                    $pathpart1 = pathinfo($this->request->data['PressCategory']['imagelogo']['name']);
                    $ext1 = $pathpart1['extension'];
                    $extensionValid1 = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder1 = "img/cat_logo_img";
                        $uploadPath1 = WWW_ROOT . $uploadFolder1;
                        $filename1 = uniqid() . '.' . $ext1;
                        $full_flg_path1 = $uploadPath1. '/' . $filename1;
                        move_uploaded_file($this->request->data['PressCategory']['imagelogo']['tmp_name'], $full_flg_path1);
                       
                    } else {
                        $this->Session->setFlash(__('Invalid image type for icon.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $imagelogo = 'noimage.png';
                }
                
                $this->request->data['PressCategory']['parent_id'] = $this->request->data['PressCategory']['categories'] ? $this->request->data['PressCategory']['categories'] : 0;
                $this->request->data['PressCategory']['imagelogo'] = $imagelogo;
                $this->PressCategory->create();
                if ($this->PressCategory->save($this->request->data)) {
                    $this->request->data1['CategoryImage']['category_id'] = $this->PressCategory->id;
                    //pr($this->request->data1);
                    //exit;
                    $this->PressCategoryImage->save($this->request->data1);
                    $this->Session->setFlash(__('The category has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category name already exists. Please, try again.'));
            }
        }
        $this->set(compact('parents', 'title_for_layout', 'countries', 'categories'));
    }

    public function admin_add_sub_category() {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->PressCategory->Country->find('list');
        $categories = $this->PressCategory->find('list', array('fields' => array('PressCategory.id', 'PressCategory.category_name'),'conditions'=>array('PressCategory.parent_id'=>0)));
        //print_r($country);
        $title_for_layout = 'Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('PressCategory.category_name' => $this->request->data['PressCategory']['category_name']));
            $name = $this->PressCategory->find('first', $options);
            if (!$name) {

                if (!empty($this->request->data['PressCategory']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['PressCategory']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['PressCategory']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['PressCategory']['parent_id'] = $this->request->data['PressCategory']['categories'] ? $this->request->data['PressCategory']['categories'] : 0;
                $this->PressCategory->create();
                if ($this->PressCategory->save($this->request->data)) {
                    $this->request->data1['CategoryImage']['category_id'] = $this->PressCategory->id;
                    //pr($this->request->data1);
                    //exit;
                    $this->PressCategoryImage->save($this->request->data1);
                    $this->Session->setFlash(__('The category has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category name already exists. Please, try again.'));
            }
        }
        $this->set(compact('parents', 'title_for_layout', 'countries', 'categories'));
    }

    public function admin_addsubcategory($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Sub Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('PressCategory.name' => $this->request->data['PressCategory']['name'], 'PressCategory.parent_id' => $this->request->data['PressCategory']['parent_id']));
            $name = $this->PressCategory->find('first', $options);
            if (!$name) {
                $this->PressCategory->create();
                if ($this->PressCategory->save($this->request->data)) {
                    $this->Session->setFlash(__('The sub category has been saved.'));
                    return $this->redirect(array('action' => 'subcategories', $id));
                } else {
                    $this->Session->setFlash(__('The sub category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The sub category name already exists. Please, try again.'));
            }
        }
        $options = array('conditions' => array('PressCategory.id' => $id));
        $categoryname = $this->PressCategory->find('list', $options);
        if ($categoryname) {
            $categoryname = $categoryname[$id];
        } else {
            $categoryname = '';
        }
        $this->set(compact('title_for_layout', 'categoryname', 'id'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->PressCategory->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->PressCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('PressCategory.' . $this->PressCategory->primaryKey => $id));
            $this->request->data = $this->PressCategory->find('first', $options);
        }
        $users = $this->PressCategory->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_edit($id = null) {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
      
        if (!$this->PressCategory->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('PressCategory.category_name' => $this->request->data['PressCategory']['category_name'], 'PressCategory.id <>' => $id));
            $name = $this->PressCategory->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

             
                
                
                if ($this->PressCategory->save($this->request->data)) {
                    $this->Session->setFlash(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
           
            $options = array('conditions' => array('PressCategory.' . $this->PressCategory->primaryKey => $id));
            $this->request->data = $this->PressCategory->find('first', $options);

            //print_r($this->request->data);
        }
        
    }

    public function admin_edit_subcat($id = null) {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->PressCategory->Country->find('list');
        $categories = $this->PressCategory->find('list', array('fields' => array('PressCategory.id', 'PressCategory.category_name'), 'conditions' => array('PressCategory.id <>' => $id,'PressCategory.parent_id '=>0)));
        //echo $id;exit;
        if (!$this->PressCategory->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('PressCategory.category_name' => $this->request->data['PressCategory']['category_name'], 'PressCategory.id <>' => $id));
            $name = $this->PressCategory->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

                if (!empty($this->request->data['PressCategory']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['PressCategory']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['PressCategory']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                        $this->request->data1['CategoryImage']['id'] = $this->request->data['PressCategory']['categoryimage_id'];
                        $this->request->data1['CategoryImage']['category_id'] = $id;
                        $this->PressCategoryImage->save($this->request->data1);
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->request->data['PressCategory']['image'] = $this->request->data['PressCategory']['hide_img'];
                }

                $this->request->data['PressCategory']['parent_id'] = $this->request->data['PressCategory']['categories'];
                if ($this->PressCategory->save($this->request->data)) {
                    $this->Session->setFlash(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
            $is_parent = $this->PressCategory->find('count', array('conditions' => array('PressCategory.parent_id' => 0, 'PressCategory.id' => $id)));
            $options = array('conditions' => array('PressCategory.' . $this->PressCategory->primaryKey => $id));
            $this->request->data = $this->PressCategory->find('first', $options);

            //print_r($this->request->data);
        }
        $this->set(compact('is_parent', 'countries', 'categories'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->PressCategory->id = $id;
        if (!$this->PressCategory->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->PressCategory->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->PressCategory->id = $id;
        if (!$this->PressCategory->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
       
        if ($this->PressCategory->delete($id)) {
            $this->Session->setFlash(__('The category has been deleted.' ,'default', array(), 'good'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    ///////////////////////////////AK///////////
    public function admin_export() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $options = array('PressCategory.id !=' => 0);
        $cats = $this->PressCategory->find('all', array('conditions' => $options));
        $output = '';
        $output .='Category Name, Parent Name, Is Active';
        $output .="\n";
//pr($cats);exit;
        if (!empty($cats)) {
            foreach ($cats as $cat) {
                $isactive = ($cat['PressCategory']['active'] == 1) ? 'Yes' : 'No';

                $output .='"' . $cat['PressCategory']['name'] . '","' . $cat['Parent']['name'] . '","' . $isactive . '"';
                $output .="\n";
            }
        }
        $filename = "categories" . time() . ".csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    //////////////////////////AK///////////////////////

    public function ajaxSubCategory(){

        $data = array();
        $html = '';
        $category_id = $this->request->data['category_id'];
        $subCategories = $this->PressCategory->find('all',array('conditions'=>array('PressCategory.parent_id'=>$category_id)));
        if(!empty($subCategories)){
            $html .= '<option value="">(Sub-Category)</option>';
            foreach ($subCategories as $key => $subCategory) {
                 $subCategory['PressCategory']['category_name'];
                 $html .= '<option value="'.$subCategory['PressCategory']['id'].'">'.$subCategory['PressCategory']['category_name'].'</option>';
             }
            $data['Ack'] = 1;
            $data['res'] = $html; 
        }
        else{
            $data['Ack'] = 0;
        }
        echo json_encode($data);      
        exit;
    }
}
