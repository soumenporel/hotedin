<?php
App::uses('AppController', 'Controller');

class SaveLatersController extends AppController {
    
    public function cart_to_savelater() {
        $this->autoRender = false;
        $data = array();
        
        $this->loadModel('Cart');
        $this->loadModel('CartItem');
        $this->loadModel('Wishlist');
        
        if ($this->request->is('post')) {
            $cartid = $this->request->data['cartid'];
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];
            $saveLater = array();
            if (!empty($cartid)) {
                $conditions = array(
                    'SaveLater.post_id' => $postid,
                    'SaveLater.user_id' => $userid
                );

                if (!$this->SaveLater->hasAny($conditions)) {
                    if ($this->CartItem->deleteAll(array('CartItem.cart_id' => $cartid, 'CartItem.post_id' => $postid), false)) {
                        $saveLater['SaveLater'] = array(
                            'user_id' => $userid,
                            'post_id' => $postid
                        );

                        if ($this->SaveLater->save($saveLater)) {
                            $this->CartItem->recursive = 2;
                            $cartItems = $this->CartItem->find('all', array(
                                'conditions' => array(
                                    'CartItem.cart_id' => $cartid
                                )
                            ));

                            $this->SaveLater->recursive = 2;
                            $saveLater = $this->SaveLater->find('all', array(
                                'conditions' => array(
                                    'SaveLater.user_id' => $userid
                                )
                            ));

                            $this->Wishlist->recursive = 2;
                            $wishlist = $this->Wishlist->find('all', array(
                                'conditions' => array(
                                    'Wishlist.user_id' => $userid
                                )
                            ));

                            $price = 0;
                            $old_price = 0;
                            foreach ($cartItems as $cartItem) {
                                $price += $cartItem['Post']['price'];
                                $old_price += $cartItem['Post']['old_price'];
                            }

                            $data['ack'] = 1;
                            $data['html']['CartItems'] = $cartItems;
                            $data['html']['SaveLater'] = $saveLater;
                            $data['html']['Wishlist'] = $wishlist;
                            $data['html']['cnt'] = count($cartItems);
                            $data['html']['price'] = $price;
                            $data['html']['old_price'] = $old_price;
                            $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price) : 0;
                        } else {
                            $data['ack'] = 0;
                            $data['msg'] = 'DB Error!!!';
                        }
                    } else {
                        $data['ack'] = 0;
                        $data['msg'] = 'DB Error!!!';
                    }
                } else {
                    $data['ack'] = 0;
                    $data['msg'] = 'Already exist!!!';
                }
            }

        }
        
        echo json_encode($data);
    }
    
    public function remove_save_later() {
        $this->autoRender = false;
        $data = array();
        
        $this->loadModel('Cart');
        $this->loadModel('CartItem');
        $this->loadModel('Wishlist');
        
        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];
            
            if (!empty($userid)) {
                if ($this->SaveLater->deleteAll(array('SaveLater.user_id' => $userid, 'SaveLater.post_id' => $postid), false)) {
                    $cart = $this->Cart->find('first', array('conditions' => array('Cart.user_id' => $userid)));
                    $this->CartItem->recursive = 2;
                    $cartItems = $this->CartItem->find('all', array(
                        'conditions' => array(
                            'CartItem.cart_id' => $cart['Cart']['id']
                        )
                    ));

                    $this->SaveLater->recursive = 2;
                    $saveLater = $this->SaveLater->find('all', array(
                        'conditions' => array(
                            'SaveLater.user_id' => $userid
                        )
                    ));

                    $this->Wishlist->recursive = 2;
                    $wishlist = $this->Wishlist->find('all', array(
                        'conditions' => array(
                            'Wishlist.user_id' => $userid
                        )
                    ));

                    $price = 0;
                    $old_price = 0;
                    foreach ($cartItems as $cartItem) {
                        $price += $cartItem['Post']['price'];
                        $old_price += $cartItem['Post']['old_price'];
                    }

                    $data['ack'] = 1;
                    $data['html']['CartItems'] = $cartItems;
                    $data['html']['SaveLater'] = $saveLater;
                    $data['html']['Wishlist'] = $wishlist;
                    $data['html']['cnt'] = count($cartItems);
                    $data['html']['price'] = $price;
                    $data['html']['old_price'] = $old_price;
                    $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price): 0;
                } else {
                    $data['ack'] = 0;
                    $data['msg'] = 'DB Error!!!';
                }
            } 
        }
        
        echo json_encode($data);
    }
    
}