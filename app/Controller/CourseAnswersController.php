<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class CourseAnswersController extends AppController {

	public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');

	public function ajaxAddAnswer(){
		
		$data = array();
		$ans['answers'] = $this->request->data['ans'];
		$ans['question_id'] = $this->request->data['question_id'];
		$ans['post_id'] = $this->request->data['postID'];
		$ans['user_id'] = $this->request->data['userID'];
		$ans['post_time'] = gmdate("y-m-d");

		if($this->CourseAnswer->save($ans)){
			$data['Ack'] = 1;
			$data['res'] = 'New Answers is posted';
		}
		else{
			$data['Ack'] = 0;
		}

		echo json_encode($data);
		exit;
	}
	
}