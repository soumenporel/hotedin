<?php
App::uses('AppController', 'Controller');

class ManageHomepagesController extends AppController {

	public function admin_index() {

		$userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $managehomes = $this->ManageHomepage->find('all');
        $this->set('managehomes',$managehomes);
        
	}

	public function admin_add() {

        if ($this->request->is('post')) {
            $this->ManageHomepage->create();
            if ($this->ManageHomepage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        }
    }

    public function admin_edit($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->ManageHomepage->exists($id)) {
            throw new NotFoundException(__('Invalid content'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->ManageHomepage->id = $id;
            if ($this->ManageHomepage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.', array('class' => 'success')));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));

            }
        } else {
            $options = array('conditions' => array('ManageHomepage.' . $this->ManageHomepage->primaryKey => $id));
            $this->request->data = $this->ManageHomepage->find('first', $options);
        }
    }

}


?>