<?php
App::uses('AppController', 'Controller');

/**
 * Trainingproviders Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BankpaymentsController extends AppController {
    
    public $components = array('Paginator');
    
    public function admin_index() {
       
        $this->loadModel('Order');
        $this->loadModel('Bank'); 
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
                     
        $options = array('conditions' => array('Order.payment_type' => 'Bank'), 'order' => array('Order.id' => 'desc'));
        
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('Order');
       
        $this->set(compact('users'));
    }
    
   
    public function admin_edit($id = NULL) {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
         $this->loadModel('Order');
        $this->loadModel('MembershipPlan');
         $this->loadModel('Bank');
       $this->loadModel('Subscription');
        $this->loadModel('User');
        $title_for_layout = 'Status Edit';
        $this->set(compact('title_for_layout'));
        
        if (!$this->Order->exists($id)) {
            throw new NotFoundException(__('Invalid Order'));
        }
        
         $options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));
            $orderdata = $this->Order->find('first', $options);
          $userid= $orderdata['Order']['user_id'];
          $planid= $orderdata['Order']['plan_id'];
        if ($this->request->is(array('post', 'put'))) {
         
            
            if ($this->Order->save($this->request->data)) {
               if($this->request->data['Order']['status']=='Success')
               {
                    
                   $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
     $duration=$planDetails['MembershipPlan']['duration'];
     $durationUnit=$planDetails['MembershipPlan']['duration_in'];
     $from_date=date('Y-m-d');
     if($durationUnit=='Days')
     {
        $to_date=date('Y-m-d', strtotime("+".$duration."days"));
     }
     else
     {
       $to_date=date('Y-m-d', strtotime("+".$duration."months"));   
     }
     
                   $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
                    if(!empty($subDetails)){
                        
                        $sub['Subscription'] = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );

                        $this->Subscription->id = $subDetails['Subscription']['id'];
                        $this->Subscription->save($sub);

                    }else{

                        $sub['Subscription'] = array(
                'user_id' => $userid,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );

                        $this->Subscription->create();
                        $this->Subscription->save($sub);
                    }

                    $this->User->id = $userid;
                    $this->User->saveField('membership_plan_id', $planid);
               }
                $this->Session->setFlash(__('Status updated.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The status could not be updated. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Order.' . $this->Order->primaryKey => $id));
            $user=$this->request->data = $this->Order->find('first', $options);
        $this->set(compact('user'));
            
        }
        
       
    }
    
    
    public function admin_delete($id = null) {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('Order');
        $this->loadModel('Bank');
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid Order'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Order->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash(__('The Order has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Order could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    
}