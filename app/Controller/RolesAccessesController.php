<?php

App::uses('AppController', 'Controller');

/**
 * RolesAccesses Controller
 *
 * @property RolesAccess $RolesAccess
 * @property PaginatorComponent $Paginator
 */
class RolesAccessesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $this->RolesAccess->recursive = 0;
        $this->set('rolesAccesses', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->RolesAccess->exists($id)) {
            throw new NotFoundException(__('Invalid roles access'));
        }
        $options = array('conditions' => array('RolesAccess.' . $this->RolesAccess->primaryKey => $id));
        $this->set('rolesAccess', $this->RolesAccess->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            
            $this->request->data['RolesAccess']['accessibility'] = serialize($this->request->data['RolesAccess']['accessibility']);
            
            $this->RolesAccess->create();
            if ($this->RolesAccess->save($this->request->data)) {
                $this->Session->setFlash('The roles access has been saved.','default', array('class' => 'success'));
                $this->redirect('/admin');
            } else {
                $this->Session->setFlash('The roles access could not be saved. Please, try again.','default', array('class' => 'error'));
            }
        }
        $roles = $this->RolesAccess->Role->find('list');
        $this->set(compact('roles'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {

        $this->loadModel('User');
        $users = $this->User->find('all',array('conditions'=>array('User.is_admin != '=>1),'group'=>array('User.id')));

        $role_access = $this->RolesAccess->find('first',array('conditions'=>array('RolesAccess.id'=>$id)));
        $role_id = $role_access['RolesAccess']['role_id'];
        $members = $this->User->find('all',array('conditions'=>array('User.is_admin != '=>1,'User.role'=>$role_id)));
        $members_ids = array();
        foreach ($members as $key => $value) {
            $members_ids[] = $value['User']['id'];
        }

        if (!$this->RolesAccess->exists($id)) {
            throw new NotFoundException(__('Invalid roles access'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['RolesAccess']['accessibility'] = serialize($this->request->data['RolesAccess']['accessibility']);
            
            if ($this->RolesAccess->save($this->request->data)) {
                
                if(!empty($this->request->data['User']['user_id'])){
                    $this->User->updateAll(
                        array('User.role' => 0),
                        array('User.id' => $members_ids)
                    );
                    $this->User->updateAll(
                        array('User.role' => $this->request->data['RolesAccess']['role_id']),
                        array('User.id' => $this->request->data['User']['user_id'])
                    );
                }
                
                $this->Session->setFlash('The roles access has been saved.','default', array('class' => 'success'));
                return $this->redirect(array('controller'=>'roles','action' => 'group_n_permission'));
            } else {
                $this->Session->setFlash('The roles access could not be saved. Please, try again.','default', array('class' => 'error'));
            }
        } else {
            $options = array('conditions' => array('RolesAccess.' . $this->RolesAccess->primaryKey => $id));
            $this->request->data = $this->RolesAccess->find('first', $options);
        }

        $roles = $this->RolesAccess->Role->find('list');
        $this->set(compact('roles','users','members_ids'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->RolesAccess->id = $id;
        if (!$this->RolesAccess->exists()) {
            throw new NotFoundException(__('Invalid roles access'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->RolesAccess->delete()) {
            $this->Session->setFlash('The roles access has been deleted.','default', array('class' => 'success'));
        } else {
            $this->Session->setFlash('The roles access could not be deleted. Please, try again.','default', array('class' => 'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
