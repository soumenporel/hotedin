<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Trainingproviders Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class NormalUsersController extends AppController {
    
    public $components = array('Paginator');
    
    public function admin_index() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if (isset($this->request->data['user_type'])) {
            $user_type = $this->request->data['user_type'];
        } else {
            $user_type = '';
        }
        $QueryStr = array('User.is_admin !=' => 1, 'User.admin_type !=' => 0);
        if ($user_type != '') {
            $QueryStr = array('User.is_admin !=' => 1, 'User.admin_type' => $user_type);
        }
        
        $this->loadModel('User');
        
        $options = array('conditions' => $QueryStr, 'order' => array('User.id' => 'desc'));
        
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('User');
        $this->set(compact('users','user_type'));
    }
    
    public function admin_add() {
         $this->loadModel('MembershipPlan');
         $this->loadModel('Subscription');
         $this->loadModel('UserImage');
         $this->loadModel('Role');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('User');
      
        $roles = $this->Role->find('list');
        $this->MembershipPlan->recursive=-1;
        $membership_plan_ids = $this->MembershipPlan->find('all');
       
        //print_r($roles);die;
        $this->request->data1 = array();
        $title_for_layout = 'User Add';
        $this->set(compact('title_for_layout', 'countries','roles','membership_plan_ids'));
        if ($this->request->is('post')) {
            $options = array('conditions' => array('User.email_address' => $this->request->data['User']['email_address']));
            $emailexists = $this->User->find('first', $options);
            if (!$emailexists) {
                if (!empty($this->request->data['User']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['User']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "user_images/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['UserImage']['originalpath'] = $filename;
                        $this->request->data1['UserImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['User']['user_pass'] = md5($this->request->data['User']['user_pass']);
                $this->request->data['User']['member_since'] = date('Y-m-d h:m:s');
                $this->User->create();
                #pr($this->data);
                #exit;
                if ($this->User->save($this->request->data)) {
                
                    if($this->request->data['User']['membership_plan_id']!=''&&$this->request->data['User']['membership_plan_id']>0){
                    //subscription.....................................
                       //for getting plan duration............................................
     $planid=$this->request->data['User']['membership_plan_id'];
     $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
     $duration=$planDetails['MembershipPlan']['duration'];
     $durationUnit=$planDetails['MembershipPlan']['duration_in'];
     $from_date=date('Y-m-d');
     if($durationUnit=='Days')
     {
        $to_date=date('Y-m-d', strtotime("+".$duration."days"));
     }
     else
     {
       $to_date=date('Y-m-d', strtotime("+".$duration."months"));   
     }
     
     $userid= $this->User->id;
    $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
    
        if(!empty($subDetails)){
            
            $sub['Subscription'] = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );
            $this->Subscription->id = $subDetails['Subscription']['id'];
            $this->Subscription->save($sub);
            
        
        }else{
            
            $sub['Subscription'] = array(
                'user_id' => $userid,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );

            $this->Subscription->create();
            $this->Subscription->save($sub);
        }
      }
                   
      //sbuscription add ends here...........................................
                    $this->request->data1['UserImage']['user_id'] = $this->User->id;
                    $this->UserImage->save($this->request->data1);
                    $this->Session->setFlash(__('The user has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The user could not be saved. Please, try again.', 'default', array('class' => 'error')));
                }
            } else {
                $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array('class' => 'error')));
            }
        }
        $this->loadModel('Language');
       
//        $state = $this->User->State->find('list');
//        $city = $this->User->City->find('list');
//        $languages = $this->Language->find('list',array('fields'=>array('Language.id','Language.full_name')));
//        $this->set(compact('state','city','languages'));
    }
    
    public function admin_edit($id = NULL) {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('UserImage');
        $this->loadModel('User');
         $this->loadModel('Role');
          $this->loadModel('MembershipPlan');
         $this->loadModel('Subscription');
        $this->request->data1 = array();
        
        $title_for_layout = 'User Edit';
        $this->set(compact('title_for_layout'));
        if (!$this->User->exists($id)) {
           
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            // pr($this->request->data); exit;
            if (!empty($this->request->data['User']['image']['name'])) {
                $pathpart = pathinfo($this->request->data['User']['image']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "user_images/";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                    $this->request->data1['UserImage']['originalpath'] = $filename;
                    $this->request->data1['UserImage']['resizepath'] = $filename;
                    if (isset($this->request->data['User']['userimage_id']) && $this->request->data['User']['userimage_id'] != '') {
                        $this->request->data1['UserImage']['id'] = $this->request->data['User']['userimage_id'];
                    }
                    $this->request->data1['UserImage']['user_id'] = $id;
                    //pr($this->request->data1);
                    //exit;
                    $this->UserImage->save($this->request->data1);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = '';
            }

            if (isset($this->request->data['User']['user_pass']) && $this->request->data['User']['user_pass'] != '') {
                //$this->request->data['User']['txt_password'] = $this->request->data['User']['user_pass'];
                $this->request->data['User']['user_pass'] = md5($this->request->data['User']['user_pass']);
            } else {
                $this->request->data['User']['user_pass'] = $this->request->data['User']['hidpw'];
            }

            
            if ($this->User->save($this->request->data)) {
                $user_id = $this->request->data['User']['id'];
                $userid=$this->request->data['User']['id'];
            if($this->request->data['User']['membership_plan_id']!=''&&$this->request->data['User']['membership_plan_id']>0){
                    //subscription.....................................
                       //for getting plan duration............................................
     $planid=$this->request->data['User']['membership_plan_id'];
     $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
     $duration=$planDetails['MembershipPlan']['duration'];
     $durationUnit=$planDetails['MembershipPlan']['duration_in'];
     $from_date=date('Y-m-d');
     if($durationUnit=='Days')
     {
        $to_date=date('Y-m-d', strtotime("+".$duration."days"));
     }
     else
     {
       $to_date=date('Y-m-d', strtotime("+".$duration."months"));   
     }
     
    
    $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
    
        if(!empty($subDetails)){
            
            $sub['Subscription'] = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );
            $this->Subscription->id = $subDetails['Subscription']['id'];
            $this->Subscription->save($sub);
            
        
        }else{
            
            $sub['Subscription'] = array(
                'user_id' => $userid,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );

            $this->Subscription->create();
            $this->Subscription->save($sub);
        }
      }
                $this->Session->setFlash(__('The user has been saved.'));
                
                $msg_body='<div>Hello '.$this->request->data['User']['first_name'].'</div></br></br>
                    <div>Your account has been activated.Now you can login and upload courses.</div>
                    <div>Thanx <br/> Team Studilmu</div>';
                if($this->request->data['User']['activated']==1&&$this->request->data['User']['admin_type']==1){
                $Email = new CakeEmail();
                $Email->emailFormat('html');
                //$Email->from('admin@studilmu.com');
                //$Email->to($this->request->data['User']['email_address']);
                $Email->from('admin@studilmu.com');
                $Email->to($this->request->data['User']['email_address']);
                $Email->subject('Account activated : Studilium');
                $Email->send($msg_body); 
                }
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('User.id'=> $id));
            $a = $this->request->data = $this->User->find('first', $options);
            
        }
        
        $this->loadModel('Language');
        $roles = $this->Role->find('list');
        $this->loadModel('MembershipPlan');
       $this->MembershipPlan->recursive = -1;
        $membership_plan_ids = $this->MembershipPlan->find('all');
        $this->set(compact('roles','membership_plan_ids'));
    }
    
    public function admin_view($id = NULL) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->loadModel('User');
        
        $title_for_layout = 'User View';
        $this->set(compact('title_for_layout'));
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }
    
    public function admin_delete($id = null) {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('User');
        $this->loadModel('UserImage');
        
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash(__('The user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    
}