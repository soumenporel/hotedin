<?php

App::uses('AppController', 'Controller');

/**
 * Settings Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class SalesReportsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','Session');

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        //$userid = $this->Session->read('userid');
        //if (!isset($userid) && $userid == '') {
        //    $this->redirect('/admin');
        //}
		
        $this->loadModel('Order');
        $this->Order->recursive = 2;
        $orders = $this->Order->find('all');
        
        
        
        
        $conditions = array();
        //Transform POST into GET
        if (($this->request->is('post') || $this->request->is('put')) && isset($this->data['Filter'])) {
            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            $filter_url['page'] = 1;

            foreach ($this->data['Filter'] as $name => $value) {
                if ($value) {
                    $filter_url[$name] = urlencode($value);
                }
            }
            return $this->redirect($filter_url);
        } else {
            foreach ($this->params['named'] as $param_name => $value) {
                if (!in_array($param_name, array('page', 'sort', 'direction', 'limit'))) {
                    if ($param_name == "start_date" && $param_name == "end_date") {
                        //$conditions[] = "DATE(Order.payment_date) BETWEEN '".$this->params['start_date']."' AND '".$this->params['end_date']."'";
                    } else {
                        //$conditions['Order.' . $param_name] = $value;
                    }
                    $this->request->data['Filter'][$param_name] = $value;
                    
                }
            }
            $start_date = isset($this->params['named']['start_date']) ? $this->params['named']['start_date'] : '';
            $end_date = isset($this->params['named']['end_date']) ? $this->params['named']['end_date'] : '';
            if($start_date != '' && $end_date != '') {
                $conditions[] = "DATE(Order.payment_date) BETWEEN '$start_date' AND '$end_date'";
            }
        }


        $options = array('conditions' => $conditions, 'limit' => 8, 'recursive' => 3);
        $this->Paginator->settings = $options;
        $this->set('orders', $this->Paginator->paginate('Order'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
		$this->loadModel('OrderItem');
		$this->loadModel('Setting');
        //if (!$this->CartItem->exists($id)) {
            //throw new NotFoundException(__('Invalid Shopping cart'));
        //}
		$settingvalue=$this->Setting->find('all');
		//print_r ($settingvalue);exit;
		$this->OrderItem->recursive=2;
        $options = array('conditions' => array('OrderItem.order_id'=>$id));
		$orderitemvalue=$this->OrderItem->find('all', $options);
		
		
        $this->set(compact('orderitemvalue','settingvalue'));
	}
	
	public function admin_delete($id = null,$itemid=null) {
        //$is_admin = $this->Session->read('is_admin');
        //if (!isset($is_admin) && $is_admin == '') {
         //   $this->redirect('/admin');
        //}
		//echo $id.$itemid;exit;
		$this->loadModel('CartItem');
		//echo $this->CartItem->cart_id;exit;
        //$this->CartItem->cart_id = $id;
        

        //$this->request->onlyAllow('post', 'delete');
		
        if ($this->CartItem->deleteAll(array(
                        'CartItem.post_id' => $itemid,
                       'CartItem.cart_id' => $id
                   ), false)) 
		{
            $this->Session->setFlash(__('The cart has been deleted.'));
        } else {
            $this->Session->setFlash(__('The cart could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }

    public function admin_cancel($id = null) {
        
        $this->loadModel('Order');
        $this->Order->id = $id;
        if($this->Order->saveField('status', 'Canceled')){
            $this->Session->setFlash(__('The Order has been Canceled.'));
        }
        else{
            $this->Session->setFlash(__('The Order could not be Canceled. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));        
    }
}