<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class BanksController extends AppController {

	  public function admin_index() {


        $title_for_layout = 'Bank';
        $this->set(compact('title_for_layout'));
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $this->loadModel('Bank');
       
        $this->set('banks', $this->Bank->find('all'));
    }
    
     public function admin_add() {
      
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $title_for_layout = 'Bank Add';
        $this->set(compact('title_for_layout'));
        if ($this->request->is('post')) {
           
                $this->Bank->create();
                #pr($this->data);
                #exit;
                if ($this->Bank->save($this->request->data)) {
                   
                    $this->Session->setFlash(__('The bank has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'admin_index'));
                } else {
                    $this->Session->setFlash(__('The bank could not be saved. Please, try again.', 'default', array('class' => 'error')));
                }
            } 
        }
   

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->loadModel('BankImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $title_for_layout = 'Bank Edit';
        $this->set(compact('title_for_layout'));
        if (!$this->Bank->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
           
            if ($this->Bank->save($this->request->data)) {
                $this->Session->setFlash(__('The bank has been saved.'));
                return $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(__('The bank could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Bank.' . $this->Bank->primaryKey => $id));
            $this->request->data = $this->Bank->find('first', $options);
           // print_r($this->request->data);die;
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
       
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Bank->id = $id;
        if (!$this->Bank->exists()) {
            throw new NotFoundException(__('Invalid bank'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Bank->delete()) {
            //$this->BankImage->delete()
            $this->Session->setFlash(__('The bank has been deleted.'));
        } else {
            $this->Session->setFlash(__('The bank could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }

}