<?php

App::uses('AppController', 'Controller');

/**
 * Roles Controller
 *
 * @property Role $Role
 * @property PaginatorComponent $Paginator
 */
class RolesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    
    /**
     * admin_group_n_permission method
     */

    public function admin_group_n_permission() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $this->loadModel('RolesAccess');

        if ($this->request->is('post')) {
            $role = array();

            $role['Role'] = array(
                'name' => $this->request->data['Role']['name']
            );

            $role['RolesAccess'] = array(
                'accessibility' => serialize($this->request->data['Role']['accessibility'])
            );

            unset($this->request->data);

            $this->Role->create();
            if ($this->Role->saveAssociated($role)) {
                $this->Session->setFlash('Group & Permission has been saved.','default', array('class' => 'success'));
                $this->redirect(array(
                    'controller' => 'roles',
                    'action' => 'group_n_permission'
                ));

            } else {
                $this->Session->setFlash('Group & Permission could not be saved. Please, try again.','default', array('class' => 'error'));
            }
        }
        
        $this->RolesAccess->recursive = 0;
        $rolesAccesses = $this->Paginator->paginate('Role');

        $this->set(compact('rolesAccesses'));
    }
    
    /**
     * admin_manage_admins method
     */

    public function admin_manage_admins() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $this->loadModel('User');
        $this->loadModel('Country');

        if($this->request->is('post')){
            //pr($this->request->data); exit;
            $role_id = $this->request->data['User']['role'];
            $roleDetails = $this->Role->find('first',array('conditions'=>array('Role.id'=>$role_id)));
            $roleName = $roleDetails['Role']['name'];
            $this->User->create();
            $this->request->data['User']['pending'] = 0;
            $this->request->data['User']['admin_type'] = 2;
            $this->request->data['User']['user_pass'] = md5($this->request->data['User']['user_pass']);
            $this->loadModel('EmailTemplate');
            $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 9)));
            //pr($EmailTemplate); exit;
            if($this->User->save($this->request->data)){

                    $user_name = $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
                    $message = 'Hi.'.$user_name.' You have been added as in '.$roleName.' admin access group. Click on the following link if you accept our request';
                    $this->loadModel('EmailTemplate');
                    $this->loadModel('Setting');

                    $adminEmail = '';
                    $contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
                        if ($contact_email) {
                            $adminEmail = $contact_email['Setting']['site_email'];
                        } else {
                            $adminEmail = 'superadmin@abc.com';
                        }
                    $EmailTemplate = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 9)));
                    $siteurl = Configure::read('SITE_URL');
                    $LOGINLINK = $siteurl . 'users/login';
                    $ACTIVATELINK = $siteurl . 'roles/activate_admin_accesss/' . base64_encode($this->User->getLastInsertId());
                    $email_address = $this->request->data['User']['email_address'];
                    $mailSubject = 'Group Member Access Activation Mail'; 
                    $msg_body = str_replace(array('[USERNAME]', '[EMAILADDRESS]' , '[SUBJECT]' , '[MESSAGE]' , '[ACTIVATELINK]'), array($user_name, $email_address, $mailSubject, $message,$ACTIVATELINK),$EmailTemplate['EmailTemplate']['content']);


                    $from = $contact_email['Setting']['site_name'] . ' <' . $adminEmail . '>';
                    $Subject_mail = $EmailTemplate['EmailTemplate']['subject'];
                    $this->php_mail($this->request->data['User']['email_address'], $from, $Subject_mail, $msg_body);

                $this->Session->setFlash('Saved successfully.','default', array('class' => 'success'));
                $this->redirect(array('controller' => 'roles','action' => 'admin_group_members'));
            }
        }

        $roles = $this->Role->find('list');
        $countries = $this->Country->find('list');
        $this->set(compact('roles', 'countries'));
    }

    public function activate_admin_accesss($userid) {
        $id = base64_decode($userid);
        $this->loadModel('User');
        $userDetails = $this->User->find('first', array('conditions' => array('User.id' => $id)));

        if (empty($userDetails)) {
            $msg = 'Invalid account.';
            $this->Session->setFlash($msg, 'default', array('class' => 'error'));
        } else {
            $data['User']['id'] = $id;
            $data['User']['pending'] = 1;
            $data['User']['activated'] = 1;
            if ($this->User->save($data)) {
                $msg = 'Account activated.';
                $this->Session->write('userid', $id);
                $this->Session->setFlash($msg, 'default', array('class' => 'success'));
                return $this->redirect('/admin');
            }
        }
        $this->set('msg', $msg);
    }


    public function admin_group_members() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->loadModel('User');

        /*$groupMembers = $this->User->find('all', array(
            'conditions' => array(
                'User.role !=' => 0
            )
        ));*/
        $this->User->recursive = 0;
        $this->Paginator->settings = array(
            'User' => array(
                'conditions' => array(
                    'User.role !=' => 0
                )
            )
        );
        $groupMembers = $this->Paginator->paginate('User');
        //pr($groupMembers); exit(); 
        $this->set(compact('groupMembers'));
    }
    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Role->recursive = 0;
        $this->set('roles', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Role->exists($id)) {
            throw new NotFoundException(__('Invalid role'));
        }
        $options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
        $this->set('role', $this->Role->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Role->create();
            if ($this->Role->save($this->request->data)) {
                $this->Session->setFlash(__('Role Has Been Saved', 'default', array('class' => 'success')));
                $this->redirect('/admin');
            } else {
                $this->Session->setFlash(__('The role could not be saved. Please, try again.','default', array('class' => 'error') ));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Role->exists($id)) {
            throw new NotFoundException(__('Invalid role'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Role->save($this->request->data)) {
                $this->Flash->success(__('The role has been saved.'));
                $this->redirect('/admin');
            } else {
                $this->Flash->error(__('The role could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
            $this->request->data = $this->Role->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    // public function admin_delete($id = null) {
    //     $this->loadModel('Role');
    //     $this->Role->id = $id;
    //     if (!$this->Role->exists()) {
    //         throw new NotFoundException(__('Invalid role'));
    //     }
    //     //$this->request->allowMethod('post', 'delete');
    //     if ($this->Role->delete()) {
    //         $this->Session->setFlash(__('The Member has been deleted.', 'default', array('class' => 'success')));
    //     } else {
    //         $this->Session->setFlash(__('The member could not be deleted. Please, try again.','default', array('class' => 'error') ));
    //     }
    //     return $this->redirect(array('action' => 'admin_group_members'));
    // }

    public function admin_delete($id = null) {
       $this->loadModel('Role');
       $this->loadModel('User');
        $this->Role->id = $id;
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        // $this->loadModel('User');
        // $this->loadModel('UserImage');
        
        $this->Role->id = $id;
        if (!$this->Role->exists()) {
            throw new NotFoundException(__('Invalid Role'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Role->delete()) {
            $this->User->updateAll(array('User.role'=>0,'User.pending'=>0), array('User.role'=>$id));
            $this->Session->setFlash(__('The Group has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Group could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_group_n_permission'));
    }

    public function admin_add_group_member(){

        $this->loadModel('User');
        $users = $this->User->find('all',array('conditions'=>array('User.is_admin != '=>1)));
        if(!empty($this->request->data)){
            //pr($this->request->data); exit;
            $role['Role']['name'] = $this->request->data['Role']['name'];
            $this->Role->create();
            $this->Role->save($role);
            $roleId = $this->Role->getLastInsertId();;
            $this->loadModel('RolesAccess');
            $roleAccess['RolesAccess']['role_id'] = $roleId;
            $roleAccess['RolesAccess']['accessibility'] = serialize($this->request->data['RolesAccess']['accessibility']);
            $this->RolesAccess->create();
            $this->RolesAccess->save($roleAccess);

            $this->loadModel('User');
            foreach ($this->request->data['User']['user_id'] as $key => $value) {
                $this->User->id = $value;
                $this->User->saveField("role",$roleId);
            }

            $this->Session->setFlash('The Group has been created.', 'default', array('class' => 'success'));
        }

        $this->set(compact('users'));
    }

    public function admin_remove($id = NULL){

        $this->loadModel('User');
        if ($this->User->updateAll(array('User.role'=>0,'User.pending'=>0), array('User.id'=>$id))) {
            $this->Session->setFlash(__('The Group Member removed from group.'));
        } else {
            $this->Session->setFlash(__('The Group Member could not removed from group. Please, try again.'));
        }

        return $this->redirect(array('action' => 'admin_group_members'));
    }



}
