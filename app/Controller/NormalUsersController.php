<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Trainingproviders Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class NormalUsersController extends AppController {
    
    public $components = array('Paginator');
    

    public function candidateRegistration(){

        $this->loadModel('User');
        $this->loadModel('Country');
        $this->loadModel('Skill');
        $this->loadModel('Experience');
        $this->loadModel('Education');
        $this->loadModel('Pagebanner');

          $pagebanner = $this->Pagebanner->find('first', array(
                                                  'conditions'=>array('Pagebanner.id' => 50),
                                                  array('Pagebanner.status' => 1)
                                                ));

        $this->set(compact('pagebanner'));
        $countries =$this->Country->find('all');
        $this->set(compact('countries'));

        $skills =$this->Skill->find('all');
        $this->set(compact('skills'));

        $data = array();

        if($this->request->is('post')){


        // pr($this->request->data);
        // exit;

             

            $emailexists = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['User']['email_address'])));

            if(!$emailexists){

                /*User image*/
                if($this->request->data['User']['user_image'] != ''){

                    $file= $this->request->data['User']['user_image'];
                 move_uploaded_file($file['tmp_name'], WWW_ROOT. 'user_images/' . $file['name']);
                $this->request->data['User']['user_image'] = $file['name'];
                }else{

                $this->request->data['User']['user_image'] = '';

                }
                /*User Image end*/

                /*Video CV*/
                if($this->request->data['User']['video'] != ''){
                 $videoFile= $this->request->data['User']['video'];
                 move_uploaded_file($videoFile['tmp_name'], WWW_ROOT. 'user_video/' . $videoFile['name']);
                $this->request->data['User']['video'] = $videoFile['name'];
            }else{

                $this->request->data['User']['video'] = '';
            }
                /*video CV end*/

                /*Document*/
                if($this->request->data['User']['document'] != ''){
                      $documentFile= $this->request->data['User']['document'];
                 move_uploaded_file($documentFile['tmp_name'], WWW_ROOT. 'user_cv/' . $documentFile['name']);
                $this->request->data['User']['document'] = $documentFile['name'];
            }else{
                $this->request->data['User']['document'] = '';
            }
               /*document end*/

               /*add category*/

               if($this->request->data['User']['category']!= ''){

                $this->request->data['User']['category'] = $this->request->data['User']['category'];
               }else{
                $this->request->data['User']['category'] = '';
               }
               /*end category*/

               /*add professionalHeadline*/

               if($this->request->data['User']['professionalHeadline']!= ''){

                $this->request->data['User']['professionalHeadline'] = $this->request->data['User']['professionalHeadline'];
               }else{
                $this->request->data['User']['professionalHeadline'] = '';
               }
               /*end professionalHeadline*/

               /*add state2*/

               if($this->request->data['User']['state2']!= ''){

                $this->request->data['User']['state2'] = $this->request->data['User']['state2'];
               }else{
                $this->request->data['User']['state2'] = '';
               }
               /*end state2*/

               /*add keySkills*/

               if($this->request->data['User']['keySkills']!= ''){

                $this->request->data['User']['keySkills'] = $this->request->data['User']['keySkills'];
               }else{
                $this->request->data['User']['keySkills'] = '';
               }
               /*end keySkills*/

               /*add profileSummary*/

               if($this->request->data['User']['profileSummary']!= ''){

                $this->request->data['User']['profileSummary'] = $this->request->data['User']['profileSummary'];
               }else{
                $this->request->data['User']['profileSummary'] = '';
               }
               /*end profileSummary*/

               /*add jobSought*/

               if($this->request->data['User']['jobSought']!= ''){

                $this->request->data['User']['jobSought'] = $this->request->data['User']['jobSought'];
               }else{
                $this->request->data['User']['jobSought'] = '';
               }
               /*end jobSought*/

                  /*add totalExperience*/

               if($this->request->data['User']['totalExperience']!= ''){

                $this->request->data['User']['totalExperience'] = $this->request->data['User']['totalExperience'];
               }else{
                $this->request->data['User']['totalExperience'] = '';
               }
               /*end totalExperience*/

                $this->request->data['User']['admin_type'] = '2';
                $this->request->data['User']['user_pass'] = md5($this->request->data['User']['user_pass']);
                $this->request->data['User']['activated'] = '1';
                $this->User->save($this->request->data);

                $getUserId =$this->User->getInsertID();

                $this->request->data['Experience']['user_id'] = $getUserId;

                   /*add started*/

               if($this->request->data['Experience']['startedDate']!= ''){

                $this->request->data['Experience']['startedDate'] = $this->request->data['Experience']['startedDate'];
               }else{
                 $this->request->data['Experience']['startedDate'] = '';
               }

               /*end started*/

                  /*add finished*/

               if($this->request->data['Experience']['finishedDate']!= ''){

                $this->request->data['Experience']['finishedDate'] = $this->request->data['Experience']['finishedDate'];
                 $this->request->data['Experience']['isChecked'] = '0';
               }else{
                $this->request->data['Experience']['finishedDate'] = '';
                $this->request->data['Experience']['isChecked'] = '1';
               }
               /*end finished*/

                   /*add companyName*/

               if($this->request->data['Experience']['companyName']!= ''){

                $this->request->data['Experience']['companyName'] = $this->request->data['Experience']['companyName'];
               }else{
                $this->request->data['Experience']['companyName'] = '';
               }
               /*end companyName*/


                   /*add positionTitle*/

               if($this->request->data['Experience']['positionTitle']!= ''){

                $this->request->data['Experience']['positionTitle'] = $this->request->data['Experience']['positionTitle'];
               }else{
                $this->request->data['Experience']['positionTitle'] = '';
               }
               /*end postionTitle*/


                   /*add description*/

               if($this->request->data['Experience']['description']!= ''){

                $this->request->data['Experience']['description'] = $this->request->data['Experience']['description'];
               }else{
                $this->request->data['Experience']['description'] = '';
               }
               /*end description*/

                $this->Experience->create();
                $this->Experience->save($this->request->data);

                 $this->request->data['Education']['user_id'] = $getUserId;
                 $getDate =$this->request->data['Education']['startedDate'];

                   /*add started*/

               if($this->request->data['Education']['startedDate']!= ''){

               $this->request->data['Education']['startedDate'] = $this->request->data['Education']['startedDate'];
               }else{
               $this->request->data['Education']['startedDate'] = '';
               }

               /*end started*/

                  /*add finished*/

               if($this->request->data['Education']['finishedDate']!= ''){

                $this->request->data['Education']['finishedDate'] = $this->request->data['Education']['finishedDate'];
                $this->request->data['Education']['isChecked'] = '0';
               }else{
                $this->request->data['Education']['finishedDate'] = '';
                 $this->request->data['Education']['isChecked'] = '1';
               }
               /*end finished*/

                   /*add institution*/

               if($this->request->data['Education']['institution']!= ''){

                $this->request->data['Education']['institution'] = $this->request->data['Education']['institution'];
               }else{
                $this->request->data['Education']['institution'] = '';
               }
               /*end institution*/


                   /*add degree*/

               if($this->request->data['Education']['degree']!= ''){

                $this->request->data['Education']['degree'] = $this->request->data['Education']['degree'];
               }else{
                $this->request->data['Education']['degree'] = '';
               }
               /*end degree*/


                   /*add description*/

               if($this->request->data['Education']['description']!= ''){

                $this->request->data['Education']['description'] = $this->request->data['Education']['description'];
               }else{
                $this->request->data['Education']['description'] = '';
               }
               /*end description*/

                $this->Education->create();
                $this->Education->save($this->request->data);

                $this->Session->setFlash('The registration has been done successfully.You need to login','default',array('class'=>'success'));

                 return $this->redirect(array('controller'=>'users','action' => 'login'));
            }
        }


    }


    public function admin_index($user_type=null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if (isset($this->request->data['user_type'])) {
            $user_type = $this->request->data['user_type'];
        } 
        $QueryStr = array('User.is_admin !=' => 1);
        if ($user_type != '') {
            $QueryStr = array('User.is_admin !=' => 1, 'User.admin_type' => $user_type);
        }
        
        $this->loadModel('User');
        
        //print_r($QueryStr);die;
        
        $options = array('conditions' => $QueryStr, 'order' => array('User.id' => 'desc'));
        
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('User');
        $type=$user_type;
        $this->set(compact('users','user_type','type'));
    }
    
    public function admin_add($type=NULL) {
         $this->loadModel('MembershipPlan');
         $this->loadModel('Subscription');
         $this->loadModel('UserImage');
         $this->loadModel('Role');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('User');
      
        $roles = $this->Role->find('list');
        $this->MembershipPlan->recursive=-1;
        $membership_plan_ids = $this->MembershipPlan->find('all');
       
        //print_r($roles);die;
        $this->request->data1 = array();
        $title_for_layout = 'User Add';
        $this->set(compact('title_for_layout', 'countries','roles','membership_plan_ids'));
        if ($this->request->is('post')) {
            $options = array('conditions' => array('User.email_address' => $this->request->data['User']['email_address']));
            $emailexists = $this->User->find('first', $options);
            if (!$emailexists) {
                if (!empty($this->request->data['User']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['User']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "user_images/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['UserImage']['originalpath'] = $filename;
                        $this->request->data1['UserImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['User']['user_pass'] = md5($this->request->data['User']['user_pass']);
                $this->request->data['User']['member_since'] = date('Y-m-d h:m:s');
                $this->User->create();
                #pr($this->data);
                #exit;
                if ($this->User->save($this->request->data)) {
                
                    if($this->request->data['User']['membership_plan_id']!=''&&$this->request->data['User']['membership_plan_id']>0){
                    //subscription.....................................
                       //for getting plan duration............................................
     $planid=$this->request->data['User']['membership_plan_id'];
     $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
     $duration=$planDetails['MembershipPlan']['duration'];
     $durationUnit=$planDetails['MembershipPlan']['duration_in'];
     $from_date=date('Y-m-d');
     if($durationUnit=='Days')
     {
        $to_date=date('Y-m-d', strtotime("+".$duration."days"));
     }
     else
     {
       $to_date=date('Y-m-d', strtotime("+".$duration."months"));   
     }
     
     $userid= $this->User->id;
    $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
    
        if(!empty($subDetails)){
            
            $sub['Subscription'] = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );
            $this->Subscription->id = $subDetails['Subscription']['id'];
            $this->Subscription->save($sub);
            
        
        }else{
            
            $sub['Subscription'] = array(
                'user_id' => $userid,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );

            $this->Subscription->create();
            $this->Subscription->save($sub);
        }
      }
                   
      //sbuscription add ends here...........................................
                    $this->request->data1['UserImage']['user_id'] = $this->User->id;
                    $this->UserImage->save($this->request->data1);
                    $this->Session->setFlash(__('The user has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The user could not be saved. Please, try again.', 'default', array('class' => 'error')));
                }
            } else {
                $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array('class' => 'error')));
            }
        }
        $this->loadModel('Language');
       
//        $state = $this->User->State->find('list');
//        $city = $this->User->City->find('list');
//        $languages = $this->Language->find('list',array('fields'=>array('Language.id','Language.full_name')));
        $this->set(compact('type'));
    }
    
    public function admin_edit($id = NULL) {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('UserImage');
        $this->loadModel('User');
         $this->loadModel('Role');
          $this->loadModel('MembershipPlan');
         $this->loadModel('Subscription');
        $this->request->data1 = array();
        
        $title_for_layout = 'User Edit';
        $this->set(compact('title_for_layout'));
        if (!$this->User->exists($id)) {
           
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            // pr($this->request->data); exit;
            if (!empty($this->request->data['User']['image']['name'])) {
                $pathpart = pathinfo($this->request->data['User']['image']['name']);
                $ext = $pathpart['extension'];
                $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = "user_images/";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                    $this->request->data1['UserImage']['originalpath'] = $filename;
                    $this->request->data1['UserImage']['resizepath'] = $filename;
                    if (isset($this->request->data['User']['userimage_id']) && $this->request->data['User']['userimage_id'] != '') {
                        $this->request->data1['UserImage']['id'] = $this->request->data['User']['userimage_id'];
                    }
                    $this->request->data1['UserImage']['user_id'] = $id;
                    //pr($this->request->data1);
                    //exit;
                    $this->UserImage->save($this->request->data1);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = '';
            }

            if (isset($this->request->data['User']['user_pass']) && $this->request->data['User']['user_pass'] != '') {
                //$this->request->data['User']['txt_password'] = $this->request->data['User']['user_pass'];
                $this->request->data['User']['user_pass'] = md5($this->request->data['User']['user_pass']);
            } else {
                $this->request->data['User']['user_pass'] = $this->request->data['User']['hidpw'];
            }

            
            if ($this->User->save($this->request->data)) {
                $user_id = $this->request->data['User']['id'];
                $userid=$this->request->data['User']['id'];
            if($this->request->data['User']['membership_plan_id']!=''&&$this->request->data['User']['membership_plan_id']>0){
                    //subscription.....................................
                       //for getting plan duration............................................
     $planid=$this->request->data['User']['membership_plan_id'];
     $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
     $duration=$planDetails['MembershipPlan']['duration'];
     $durationUnit=$planDetails['MembershipPlan']['duration_in'];
     $from_date=date('Y-m-d');
     if($durationUnit=='Days')
     {
        $to_date=date('Y-m-d', strtotime("+".$duration."days"));
     }
     else
     {
       $to_date=date('Y-m-d', strtotime("+".$duration."months"));   
     }
     
    
    $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
    
        if(!empty($subDetails)){
            
            $sub['Subscription'] = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );
            $this->Subscription->id = $subDetails['Subscription']['id'];
            $this->Subscription->save($sub);
            
        
        }else{
            
            $sub['Subscription'] = array(
                'user_id' => $userid,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'memebership_plan_id' => $planid
            );

            $this->Subscription->create();
            $this->Subscription->save($sub);
        }
      }
                $this->Session->setFlash(__('The user has been saved.'));
                
                $msg_body='<div>Hello '.$this->request->data['User']['first_name'].'</div></br></br>
                    <div>Your account has been activated.Now you can login and upload courses.</div>
                    <div>Thanx <br/> Team Studilmu</div>';
                if($this->request->data['User']['activated']==1&&$this->request->data['User']['admin_type']==1){
                $Email = new CakeEmail();
                $Email->emailFormat('html');
                //$Email->from('admin@studilmu.com');
                //$Email->to($this->request->data['User']['email_address']);
                $Email->from('admin@studilmu.com');
                $Email->to($this->request->data['User']['email_address']);
                $Email->subject('Account activated : Studilium');
                $Email->send($msg_body); 
                }
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('User.id'=> $id));
            $a = $this->request->data = $this->User->find('first', $options);
            
        }
        
        $this->loadModel('Language');
        $roles = $this->Role->find('list');
        $this->loadModel('MembershipPlan');
       $this->MembershipPlan->recursive = -1;
        $membership_plan_ids = $this->MembershipPlan->find('all');
        $this->set(compact('roles','membership_plan_ids'));
    }
    
    public function admin_view($id = NULL) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->loadModel('User');
        
        $title_for_layout = 'User View';
        $this->set(compact('title_for_layout'));
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }
    
    public function admin_delete($id = null) {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('User');
        $this->loadModel('UserImage');
        
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash(__('The user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    
    public function admin_send_push_form() {
        
        $this->layout=false;
        $this->render=false;
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if($this->request->data['push_msg']!="")
        {
       $message=$this->request->data['push_msg'];
        $this->loadModel('User');
     // $url = 'http://111.93.169.90/team4/studilmu/webservice/push/testpush';
        $url = 'http://111.93.169.90/team4/studilmu/webservice/push/sendpush';
    $fields = array(
        //'registration_ids' => $device_token_id,
        'message' => $message
    );
//    $headers = array(
//        'Authorization: key=AIzaSyC8WIl4ZgFUnpAbI1tjB7c2ShAz9ZwZSD4',
//        'Content-Type: application/json'
//    );

    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));


    // Execute post
    $result = curl_exec($ch);
    // print_r($result);die;
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
   
   
    $this->Session->setFlash('Notification send successfully.', 'default', array('class' => 'message success'));
        }          
                $this->redirect(array("action" => "index", 2));
    }

    
}