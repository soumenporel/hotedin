<?php
App::uses('AppController', 'Controller');
require_once(ROOT . '/app/Vendor' . DS . 'payu.php');
require_once(ROOT . '/app/Vendor' . DS . 'stripe-php' . DS . 'init.php');

class CheckoutsController extends AppController {
    public $components = array('Session');
    
    public function index($id = NULL) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->Session->setFlash(__('Please login to access MembershipPlan.', 'default', array('class' => 'error')));
            return $this->redirect(array('controller'=>'homepages','action' => 'index'));
        }
 $this->loadModel('Order');
        //$this->Session->write('plan_id',);
        
        if ($this->request->is('post')) {
           
                // if ($this->request->data['User']['user_pass'] == $this->request->data['User']['cpass']) {
                    $this->request->data['Order']['payment_type'] = 'Bank';
                    $this->request->data['Order']['user_id'] = $userid;
                    $this->request->data['Order']['status'] = "Pending";
                    $this->request->data['Order']['plan_id'] = $id;
                    $this->request->data['Order']['payment_date'] = date('Y-m-d H:i:s');
                     if (!empty($this->request->data['Order']['document']['name'])) {
                    $pathpart = pathinfo($this->request->data['Order']['document']['name']);
                    $ext = $pathpart['extension'];
                    //$extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    //if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "bank_docs/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Order']['document']['tmp_name'], $full_flg_path);
                        $this->request->data['Order']['document'] = $filename;
                      
//                    } else {
//                        $this->Session->setFlash(__('Invalid image type.'));
//                    }
                } else {
                    $filename = '';
                }
                    if ($this->Order->save($this->request->data)) {
                        
                    $this->Session->setFlash('After verification your membership will be activated', 'default', array('class' => 'success'));
                   return $this->redirect(array('controller' => 'homepages','action' => 'index'));    
                    } else {
                        $this->Session->setFlash(__('Sorry your details could not be saved. Please, try again.', 'default', array('class' => 'error')));
                    }
                
           
        }
        
        $mid = $id;
        $this->loadModel('MembershipPlan');
        $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$mid)));
        
        $this->Session->write('plan_id',$planDetails['MembershipPlan']['id']);
        $this->Session->write('amount',$planDetails['MembershipPlan']['price']);
        
         $this->loadModel('Bank');
        $bank_ids = $this->Bank->find('all');
        //print_r($bank_ids);die;
        $this->set(compact('planDetails','userid','bank_ids'));   
    }

    public function payu_return(){

        //echo '<pre>'; print_r($_POST); echo '</pre>';
        if($_POST['status']!='success'){
            $this->redirect('/checkouts/error_payment');
        }
        $this->autoRender = false;
        $this->layout = false;
        
        $this->loadModel('Order');
        $this->loadModel('Cart');
        $this->loadModel('CartItem');
        $this->loadModel('UserCourse');
        
        $userid = $_POST['firstname'];
        
        $this->Cart->recursive = 3;
        $cart = $this->Cart->find('first', array(
            'conditions' => array(
                'Cart.user_id' => $userid
            ) 
        ));
        
        $order['Order'] = array(
            'user_id' => $userid,
            'transaction_id' => $_POST['txnid'],
            'quantity' => count($cart['CartItem']),
            'amount' => $_POST['amount'],
            'payment_date' => date('Y-m-d H:i:s'),
            'status' => 'Success'
        );
        
        foreach ($cart['CartItem'] as $cartItem) {
                    
            $order['OrderItem'][] = array(
                'post_id' => $cartItem['post_id'],
                'quantity' => 1,
                'price' => $cartItem['Post']['price'],
                'old_price' => $cartItem['Post']['old_price']
            );

            $userCourse['UserCourse'] = array('user_id' => $userid, 'post_id' => $cartItem['post_id']);
            $this->UserCourse->create();
            $this->UserCourse->save($userCourse);

            $this->CartItem->deleteAll(array(
                'CartItem.post_id' => $cartItem['post_id'],
                'CartItem.cart_id' => $cart['Cart']['id']
            ), false);
            
        }
        $this->Order->saveAssociated($order);
        $this->redirect('/checkouts/thankyou');
    }

    public function takethis_course_payu_return() {
        //echo '<pre>'; print_r($_POST); echo '</pre>'; exit;
        if($_POST['status']!='success'){
            $this->redirect('/checkouts/error_payment');
        }
        $this->autoRender = false;
        $this->layout = false;
        
        //$custom = explode('&&', $_POST['custom']);
        
        // $userid = $custom[0];
        // $postid = $custom[1];

        $userid = $_POST['firstname'];
        $postid = $_POST['lastname'];
                
        $this->loadModel('Post');
        $this->loadModel('Order');
        $this->loadModel('UserCourse');
        
        $this->Post->recursive = 2;
        $cart = $this->Post->find('first', array('conditions' => array(
            'Post.id' => $postid
        )));
        
        $order['Order'] = array(
            'user_id' => $userid,
            'transaction_id' => $_POST['txnid'],
            'quantity' => 1,
            'amount' => $_POST['amount'],
            'payment_date' => date('Y-m-d H:i:s'),
            'status' => 'Success'
        );


        $order['OrderItem'][] = array(
            'post_id' => $cart['Post']['id'],
            'quantity' => 1,
            'price' => $cart['Post']['price'],
            'old_price' => $cart['Post']['old_price']
        );
        
        $this->Order->create();
        $this->Order->saveAssociated($order);
        
        $userCourse['UserCourse'] = array('user_id' => $userid, 'post_id' => $postid);
        $this->UserCourse->create();
        $this->UserCourse->save($userCourse);
        $this->redirect('/checkouts/thankyou');
        
    }


    
    public function ccpayment() {
        //pr($_POST); exit;
        $this->autoRender = false;
        $data = array();
        
        if($this->request->is('post')){
            
            $this->loadModel('Order');
            $this->loadModel('Cart');
            $this->loadModel('CartItem');
            $this->loadModel('SavedCard');
            
            $userid = $this->Session->read('userid');
            App::import(
                'Vendor',
                'PaypalClass',
                array('file' => 'PaypalCC' . DS . 'paypal.class.php')
            );
            
            $ccfname    = $this->request->data['ccfname'];
            $cclname    = $this->request->data['cclname'];
            $ccnum      = $this->request->data['ccnum'];
            $ccexp1      = $this->request->data['ccexp'];
            $cccvv      = $this->request->data['cccvv'];
            $cccountry  = $this->request->data['cccountry'];
            $ccpcode    = $this->request->data['ccpcode'];
            $amt        = $this->request->data['amt'];
            $remberThisCard = $this->request->data['remberThisCard'];
            $ccexp      = explode('/',$ccexp1);
            $requestParams = array(
                'IPADDRESS' => $_SERVER['REMOTE_ADDR'],          // Get our IP Address
                'PAYMENTACTION' => 'Sale'
            );
            $creditCardDetails = array(
                'CREDITCARDTYPE' => 'Visa',
                'ACCT' => $ccnum,
                'EXPDATE' => $ccexp[0].'20'.$ccexp[1],          // Make sure this is without slashes (NOT in the format 07/2017 or 07-2017)
                'CVV2' => $cccvv
            );
            $payerDetails = array(
                'FIRSTNAME' => $ccfname,
                'LASTNAME' => $cclname,
                'COUNTRYCODE' => 'IN',
                //'STATE' => 'NY',
                //'CITY' => 'New York',
                //'STREET' => '14 Argyle Rd.',
                'ZIP' => $ccpcode
            );
            $orderParams = array(
                'AMT' => $amt,               // This should be equal to ITEMAMT + SHIPPINGAMT
                //'ITEMAMT' => '496',
                //'SHIPPINGAMT' => '4',
                'CURRENCYCODE' => 'USD'       // USD for US Dollars
            );
            $item = array(
                //'L_NAME0' => 'iPhone',
                //'L_DESC0' => 'White iPhone, 16GB',
                //'L_AMT0' => '496',
                //'L_QTY0' => '1'
            );
            
            $this->Cart->recursive = 2;
            $cart = $this->Cart->find('first', array('conditions' => array(
                'Cart.user_id' => $userid
            )));
            
            $paypal = new Paypal();
            $response = $paypal->request('DoDirectPayment', $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item);
            if (is_array($response) && $response['ACK'] == 'Success') {
                
                $order['Order'] = array(
                    'user_id' => $userid,
                    'transaction_id' => $response['TRANSACTIONID'],
                    'quantity' => count($cart['CartItem']),
                    'amount' => $amt,
                    'payment_date' => date('Y-m-d H:i:s'),
                    'status' => $response['ACK']
                );
                
                $this->loadModel('UserCourse');
                
                foreach ($cart['CartItem'] as $cartItem) {
                    
                    $order['OrderItem'][] = array(
                        'post_id' => $cartItem['post_id'],
                        'quantity' => 1,
                        'price' => $cartItem['Post']['price'],
                        'old_price' => $cartItem['Post']['old_price']
                    );
                    
                    $userCourse['UserCourse'] = array('user_id' => $userid, 'post_id' => $cartItem['post_id']);
                    $this->UserCourse->create();
                    $this->UserCourse->save($userCourse);
                    
                    $this->CartItem->deleteAll(array(
                        'CartItem.post_id' => $cartItem['post_id'],
                        'CartItem.cart_id' => $cart['Cart']['id']
                    ), false);
                }
                
                $this->Order->create();
                if($this->Order->saveAssociated($order)) {
                    
                    if($remberThisCard) {
                        $savedCard = $this->SavedCard->find('first', array('SavedCard.user_id' => $userid));
                        $this->loadModel('User');
                        $userDetails = $this->User->find('first',array('conditions'=>array('User.id'=>$userid)));
                        if(!empty($savedCard)){
                            $savedCardArray['SavedCard'] = array(
                                'id' => $savedCard['SavedCard']['id'],
                                'user_id' => $userid,
                                'first_name' => $ccfname,
                                'last_name' => $cclname,
                                'expire' => $ccexp1,
                                'country' => $cccountry,
                                'zip' => $ccpcode,
                                'cc_number' => $ccnum,
                                'status' => $userDetails['User']['save_card']
                            );
                        } else {
                            $savedCardArray['SavedCard'] = array(
                                'user_id' => $userid,
                                'first_name' => $ccfname,
                                'last_name' => $cclname,
                                'expire' => $ccexp1,
                                'country' => $cccountry,
                                'zip' => $ccpcode,
                                'cc_number' => $ccnum,
                                'status' => $userDetails['User']['save_card']
                            );
                            $this->SavedCard->create();
                        }
                        $this->SavedCard->save($savedCardArray);
                    }
                    
                    $data['ack'] = 1;
                }
                
            } else {
                $data['ack'] = 0;
                $data['response'] = $response;
            }
        }
        
        echo json_encode($data);
    }
    
    public function thankyou() {
        
       
    
    }
    
    public function thankyou_paypal() {
        
        //echo $custom = $_POST['custom'];
        $this->loadModel('Order');
        $this->loadModel('User');
        $this->loadModel('Subscription');
        $this->loadModel('CartItem');
        $this->loadModel('UserCourse');
        $custom = $_POST['custom'];
        //$custom = '0|3|99|r|r|r2@gmail.com|698d51a19d8a121ce581499d7b701668';
        $customArray = explode('|', $custom);
         //print_r($customArray);
      $userid = $customArray['0'];
      $planid = $customArray['1'];
       $amount = $customArray['2'];
        $first_name = $customArray['3'];
        $last_name = $customArray['4'];
        $email_address = $customArray['5'];
        $user_pass = $customArray['6'];
        $this->request->data1=array();
          if($userid==null||$userid==0)
        {
         
          $this->request->data1['User']['first_name'] = $first_name; 
          $this->request->data1['User']['last_name']  = $last_name; 
          $this->request->data1['User']['email_address']  = $email_address; 
           $this->request->data1['User']['user_pass']  = $user_pass;
           $this->User->create();
           if ($this->User->save($this->request->data1)) {
               $latestuserId = $this->User->getLastInsertId();
               $this->Session->write('userid',$latestuserId);
              $userid = $latestuserId;
           }
        }
 
   
        if($userid>0){
        $order['Order'] = array(
            'user_id' => $userid,
            'transaction_id' => $_POST['payer_id'],
            'quantity' => 1,
            'amount' => $amount,
            'payment_date' => date('Y-m-d H:i:s'),
            'status' => 'Success',
            'plan_id' => $planid
        );
        
        
        $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
        if(!empty($subDetails)){
            
            $this->Subscription->id = $subDetails['Subscription']['id'];
            $this->Subscription->saveField('memebership_plan_id', $planid);
        
        }else{
            
            $sub['Subscription'] = array(
                'user_id' => $userid,
                'memebership_plan_id' => $planid,
            );

            $this->Subscription->create();
            $this->Subscription->save($sub);
        }

        $this->User->id = $userid;
        $this->User->saveField('membership_plan_id', $planid);

        $this->Order->saveAssociated($order);
        }
        
       
    
    }
    public function canceledpayment() {
    
    }
    public function error_payment(){

    }
    
    public function takethis_course($slug = NULL) {

        $this->loadModel('Post');
        $cart = $this->Post->find('first', array(
            'conditions' => array(
                'Post.slug' => $slug
            )
        ));

        $surl = Router::url(array('controller' => 'checkouts', 'action' => 'takethis_course_payu_return'),TRUE);
        $furl = Router::url(array('controller' => 'checkouts', 'action' => 'takethis_course_payu_return'),TRUE); 
        if ( !empty($this->request->data['payuSubmit']) ){
            //echo '<pre>'; print_r($this->request->data); echo '</pre>'; exit;
            pay_page( array ('key' => 'gtKFFx', 'txnid' => uniqid( 'txnid_' ), 'amount' => $this->request->data['amount'],
            'firstname' => $this->request->data['firstname'],'lastname' => $this->request->data['lastname'], 'email' => $this->request->data['email'], 'phone' => $this->request->data['phone'],
            'productinfo' => 'Product Info', 'surl' => $surl, 'furl' => $furl), 
            'eCwWELxi',$surl,$furl);
        }

        $userid = $this->Session->read('userid');
        if (empty($userid)) {
            $this->redirect('/');
        }
        
        $this->loadModel('SavedCard');
        
        
        
        $price = $cart['Post']['price'];
        $old_price = $cart['Post']['old_price'];
       
        $savedCard = $this->SavedCard->find('first', array('conditions' => array(
           'SavedCard.user_id' => $userid,
           'SavedCard.status' => 1
       )));
        
       $this->set(compact('price', 'old_price', 'cart', 'savedCard', 'userid')); 
    }
    
    public function takethis_course_ccpayment() {
        $this->autoRender = false;
        $data = array();
        
        if($this->request->is('post')){
            
            $userid = $this->Session->read('userid');
            $this->loadModel('SavedCard');
            App::import(
                'Vendor',
                'PaypalClass',
                array('file' => 'PaypalCC' . DS . 'paypal.class.php')
            );
            
            $ccfname    = $this->request->data['ccfname'];
            $cclname    = $this->request->data['cclname'];
            $ccnum      = $this->request->data['ccnum'];
            $ccexp1      = $this->request->data['ccexp'];
            $cccvv      = $this->request->data['cccvv'];
            $cccountry  = $this->request->data['cccountry'];
            $ccpcode    = $this->request->data['ccpcode'];
            $amt        = $this->request->data['amt'];
            $postid     = $this->request->data['postid'];
            $remberThisCard = $this->request->data['remberThisCard'];
            $ccexp = explode('/',$ccexp1);
            $requestParams = array(
                'IPADDRESS' => $_SERVER['REMOTE_ADDR'],          // Get our IP Address
                'PAYMENTACTION' => 'Sale'
            );
            $creditCardDetails = array(
                'CREDITCARDTYPE' => 'Visa',
                'ACCT' => $ccnum,
                'EXPDATE' => $ccexp[0].'20'.$ccexp[1],          // Make sure this is without slashes (NOT in the format 07/2017 or 07-2017)
                'CVV2' => $cccvv
            );
            $payerDetails = array(
                'FIRSTNAME' => $ccfname,
                'LASTNAME' => $cclname,
                'COUNTRYCODE' => $cccountry,
                //'STATE' => 'NY',
                //'CITY' => 'New York',
                //'STREET' => '14 Argyle Rd.',
                'ZIP' => $ccpcode
            );
            $orderParams = array(
                'AMT' => $amt,               // This should be equal to ITEMAMT + SHIPPINGAMT
                //'ITEMAMT' => '496',
                //'SHIPPINGAMT' => '4',
                'CURRENCYCODE' => 'USD'       // USD for US Dollars
            );
            $item = array(
                //'L_NAME0' => 'iPhone',
                //'L_DESC0' => 'White iPhone, 16GB',
                //'L_AMT0' => '496',
                //'L_QTY0' => '1'
            );
            
            $this->loadModel('Post');
            $this->Post->recursive = 2;
            $cart = $this->Post->find('first', array('conditions' => array(
                'Post.id' => $postid
            )));
            
            $paypal = new Paypal();
            $response = $paypal->request('DoDirectPayment', $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item);
            if (is_array($response) && $response['ACK'] == 'Success') {
                $this->loadModel('Order');
                $order['Order'] = array(
                    'user_id' => $userid,
                    'transaction_id' => $response['TRANSACTIONID'],
                    'quantity' => 1,
                    'amount' => $amt,
                    'payment_date' => date('Y-m-d H:i:s'),
                    'status' => $response['ACK']
                );
                
                
                $order['OrderItem'][] = array(
                    'post_id' => $cart['Post']['id'],
                    'quantity' => 1,
                    'price' => $cart['Post']['price'],
                    'old_price' => $cart['Post']['old_price']
                );
                
                $this->Order->create();
                if($this->Order->saveAssociated($order)) {
                    $this->loadModel('UserCourse');
                    $userCourse['UserCourse'] = array('user_id' => $userid, 'post_id' => $postid);
                    $this->UserCourse->create();
                    $this->UserCourse->save($userCourse);
                    
                    
                    if($remberThisCard) {
                        $savedCard = $this->SavedCard->find('first', array('SavedCard.user_id' => $userid));
                        $this->loadModel('User');
                        $userDetails = $this->User->find('first',array('conditions'=>array('User.id'=>$userid)));
                        if(!empty($savedCard)){
                            $savedCardArray['SavedCard'] = array(
                                'id' => $savedCard['SavedCard']['id'],
                                'user_id' => $userid,
                                'first_name' => $ccfname,
                                'last_name' => $cclname,
                                'expire' => $ccexp1,
                                'country' => $cccountry,
                                'zip' => $ccpcode,
                                'cc_number' => $ccnum,
                                'status' => $userDetails['User']['save_card']
                            );
                        } else {
                            $savedCardArray['SavedCard'] = array(
                                'user_id' => $userid,
                                'first_name' => $ccfname,
                                'last_name' => $cclname,
                                'expire' => $ccexp1,
                                'country' => $cccountry,
                                'zip' => $ccpcode,
                                'cc_number' => $ccnum,
                                'status' => $userDetails['User']['save_card']
                            );
                            $this->SavedCard->create();
                        }
                        $this->SavedCard->save($savedCardArray);
                    }
                    
                    
                    $data['ack'] = 1;
                }
                
            } else {
                $data['ack'] = 0;
                $data['response'] = $response;
            }
        }
        
        echo json_encode($data);
    }
    
    public function notify_url() {
        
        $this->autoRender = false;
        $this->layout = false;
        
        $this->loadModel('Order');
        $this->loadModel('User');
        $this->loadModel('Subscription');
        $this->loadModel('CartItem');
        $this->loadModel('UserCourse');
        $this->request->data1=array();
        $this->request->data=array();
       
        $custom = $_POST['custom'];
        $customArray = explode('|', $custom);
        $userid = $customArray['0'];
        $planid = $customArray['1'];
        $amount = $customArray['2'];
        $first_name = $customArray['3'];
        $last_name = $customArray['4'];
        $email_address = $customArray['5'];
        $user_pass = $customArray['6'];
        
//        $userid = $this->Session->read('userid');
//        $planid = $this->Session->read('plan_id');
//        $amount = $this->Session->read('amount');
//        
       /* if($userid==null||$userid==0)
        {
           $this->request->data1['User']['first_name'] = $first_name; 
          $this->request->data1['User']['last_name']  = $last_name; 
          $this->request->data1['User']['email_address']  = $email_address; 
           $this->request->data1['User']['user_pass']  = $user_pass;
           $this->User->create();
           if ($this->User->save($this->request->data1)) {
               $latestuserId = $this->User->getLastInsertId();
               //$this->Session->write('userid',$latestuserId);
               $userid = $latestuserId;
           }
        }
   
        if($userid>0){
        $order['Order'] = array(
            'user_id' => $userid,
            'transaction_id' => $_POST['payer_id'],
            'quantity' => 1,
            'amount' => $amount,
            'payment_date' => date('Y-m-d H:i:s'),
            'status' => 'Success',
            'plan_id' => $planid
        );
        
        
        $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
        if(!empty($subDetails)){
            
            $this->Subscription->id = $subDetails['Subscription']['id'];
            $this->Subscription->saveField('memebership_plan_id', $planid);
        
        }else{
            
            $sub['Subscription'] = array(
                'user_id' => $userid,
                'memebership_plan_id' => $planid,
            );

            $this->Subscription->create();
            $this->Subscription->save($sub);
        }

        $this->User->id = $userid;
        $this->User->saveField('membership_plan_id', $planid);

        $this->Order->saveAssociated($order);
        } */
        exit();
        
    }
    
    public function takethis_course_notify_url() {
        $this->autoRender = false;
        $this->layout = false;
        
        $custom = explode('&&', $_POST['custom']);
        
        $userid = $custom[0];
        $postid = $custom[1];
                
        $this->loadModel('Post');
        $this->loadModel('Order');
        $this->loadModel('UserCourse');
        
        $this->Post->recursive = 2;
        $cart = $this->Post->find('first', array('conditions' => array(
            'Post.id' => $postid
        )));
        
        $order['Order'] = array(
            'user_id' => $userid,
            'transaction_id' => $_POST['txn_id'],
            'quantity' => 1,
            'amount' => $_POST['mc_gross'],
            'payment_date' => date('Y-m-d H:i:s'),
            'status' => 'Success'
        );


        $order['OrderItem'][] = array(
            'post_id' => $cart['Post']['id'],
            'quantity' => 1,
            'price' => $cart['Post']['price'],
            'old_price' => $cart['Post']['old_price']
        );
        
        $this->Order->create();
        $this->Order->saveAssociated($order);
        
        $userCourse['UserCourse'] = array('user_id' => $userid, 'post_id' => $postid);
        $this->UserCourse->create();
        $this->UserCourse->save($userCourse);
        
    }
    
    //for stripe test...........................................
    public function bank($id = NULL) {
        $userid = $this->Session->read('userid');
               
    }

    public function paymentTest(){
        
        $userid = $this->Session->read('userid');
        $planid = $this->Session->read('plan_id');
        $this->loadModel('MembershipPlan');
        $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
        $amount = $planDetails['MembershipPlan']['price']*100;


        \Stripe\Stripe::setApiKey("sk_test_ySVhuMSBmrIakQ4bZLHMSBdU");

        // Token is created using Stripe.js or Checkout!
        // Get the payment token ID submitted by the form:
        $token = $this->request->data['stripeToken'];

        // Charge the user's card:
        $charge = \Stripe\Charge::create(array(
          "amount" => $amount,
          "currency" => "IDR",
          "description" => "Example charge",
          "source" => $token,
        ));

        $this->loadModel('User');
        $this->User->id = $userid;
        $this->User->saveField('membership_plan_id',$planDetails['MembershipPlan']['id']);
        
        // $customer = \Stripe\Customer::create(array(
        //   "email" => $this->request->data['stripeEmail'],
        // ));
        // $r = json_decode($customer->__toJson());
        // //pr(json_decode($r)); exit;
        // $subscription = \Stripe\Subscription::create(array(
        //   "customer" => $r->id,
        //   "items" => array(
        //     array(
        //       "plan" => "4_premium_10",
        //     ),
        //   )
        // ));

        $this->redirect('/homepages/index');
        exit;
    }
    
    
    //checkout payment gateway.................................................
    public function paymentTestNew(){
        $update=0;
        $this->loadModel('Order');
        $this->loadModel('User');
        $this->loadModel('Subscription');
        $this->loadModel('CartItem');
        $this->loadModel('UserCourse');
        require_once(ROOT . '/app/Veritrans/Veritrans.php');
        //echo 1; die;
       $userid = $this->Session->read('userid');
       $planid = $this->Session->read('plan_id');
        $this->loadModel('MembershipPlan');
        $planDetails = $this->MembershipPlan->find('first',array('conditions'=>array('MembershipPlan.id'=>$planid)));
        $amount = $planDetails['MembershipPlan']['price'];
        //$amount = 10;

        //Set Your server key
Veritrans_Config::$serverKey = "VT-server-UFV8hSy3rKogLEi9dWBdDC9m";

// Uncomment for production environment
// Veritrans_Config::$isProduction = true;

// Enable sanitization
Veritrans_Config::$isSanitized = true;

// Enable 3D-Secure
Veritrans_Config::$is3ds = true;

// Required
$transaction_details = array(
  'order_id' => rand(),
  'gross_amount' => $amount, // no decimal allowed for creditcard
);

$enable_payments = array('credit_card');


// Fill transaction details
$transaction = array(
   'enabled_payments' => $enable_payments,
    'transaction_details' => array(
        'order_id' => rand(),
        'gross_amount' => $amount // no decimal allowed
        )
    );


//echo '9';die;
$snapToken = Veritrans_Snap::getSnapToken($transaction);
if($snapToken!='')
{
    ?>
<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="VT-client-sfSEdha18mn8L6At"></script>
<script type="text/javascript">
 snap.pay('<?=$snapToken?>', {
            
          // Optional
          onSuccess: function(result){
                //alert(1);
                 $.ajax({
                url: '<?php echo $this->webroot . 'users/addSubscription' ?>',
                type: 'post',
                dataType: 'json',
               
                success: function(data) {
                    //alert(data.Ack);
                  if(data.Ack == 1){
                    window.location.href = "<?php echo $this->webroot; ?>checkouts/thankyou";
                  }
                  else
                  {
                     window.location.href = "<?php echo $this->webroot; ?>checkouts/error_payment"; 
                  }
                }
            });
           
          },
          // Optional
          onPending: function(result){
              // alert(2);
               window.location.href = "<?php echo $this->webroot; ?>checkouts/error_payment";
           
          },
          // Optional
          onError: function(result){
               //alert(3);
           window.location.href = "<?php echo $this->webroot; ?>checkouts/error_payment";
            
          }
          
        });  
</script>
<?php

//        $order['Order'] = array(
//            'user_id' => $userid,
//            'transaction_id' => '123456',
//            'quantity' => 1,
//            'amount' => $amount,
//            'payment_date' => date('Y-m-d H:i:s'),
//            'status' => $resultStaus,
//            'plan_id' => $planid
//        );
//    $subDetails = $this->Subscription->find('first',array('conditions'=>array('Subscription.user_id'=>$userid)));
//    
//        if(!empty($subDetails)){
//            
//            $this->Subscription->id = $subDetails['Subscription']['id'];
//            $this->Subscription->saveField('memebership_plan_id', $planid);
//            $update=1;
//        
//        }else{
//            
//            $sub['Subscription'] = array(
//                'user_id' => $userid,
//                'memebership_plan_id' => $planid
//            );
//
//            $this->Subscription->create();
//            $this->Subscription->save($sub);
//           $update=1;
//        }
//        if($update==1)
//        {
//                $this->User->id = $userid;
//                $this->User->saveField('membership_plan_id', $planid);
//                 $this->Order->saveAssociated($order);
//                //$this->redirect('/checkouts/thankyou');
//
//        }   

}

else
{
    echo "Payment gateway error";
}

       
       
    }
}