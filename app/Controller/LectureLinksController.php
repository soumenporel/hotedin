<?php

App::uses('AppController', 'Controller');

/**
 * LectureAssets Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class LectureLinksController extends AppController {

  

    public function ajaxAddLectureAsset(){
        $this->autoRender = false;
        $data = array();
        //pr($this->request->data); exit();
        $lectureID = $this->request->data['lacture_id'];
        $resourse['LectureLink']['lecture_id'] = $lectureID;
        $resourse['LectureLink']['link'] = $this->request->data['title'];
        $resourse['LectureLink']['url'] = $this->request->data['url'];
        $resourse['LectureLink']['post_id'] = $this->request->data['post_id'];
        $resourse['LectureLink']['date'] = gmdate('Y-m-d');
        if($this->LectureLink->save($resourse['LectureLink'])){
            $data['Ack'] = 1;
        }
        else{
            $data['Ack'] = 0;
        }
        echo json_encode($data);
        exit;
    }

    public function ajaxAddLectureLink(){

        //pr($this->request->data['lecture_id']);
        $data = array();
        $lectureID = $this->request->data['lecture_id'];
        $resourse['LectureLink']['lecture_id'] = $lectureID;
        $resourse['LectureLink']['link'] = $this->request->data['name'];
        $resourse['LectureLink']['url'] = $this->request->data['url'];
        $resourse['LectureLink']['post_id'] = $this->request->data['post_id'];
        $resourse['LectureLink']['date'] = gmdate('Y-m-d');
        if($this->LectureLink->save($resourse['LectureLink'])){
            $data['Ack'] = 1;
        }
        else{
            $data['Ack'] = 0;
        }
        echo json_encode($data);
        exit;
    }
}

?>    