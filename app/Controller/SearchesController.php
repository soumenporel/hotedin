<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SearchesController extends AppController {
    /* function beforeFilter() {
      parent::beforeFilter();
      } */

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Searches';
    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    var $uses = array('User', 'Country', 'State', 'City', 'Newsletter', 'Setting', 'Content', 'Activity', 'Contact', 'Category', 'Post', 'Homeslider', 'Skill', 'Comment', 'Partner','Venue','Wishlist','TempCart','Rating','InstructorSkill','Skill','Portfolio','Portfoliofile','Award','Awardfile','CourseSkill','PostImage');
    public $helpers = array('GoogleMap');

     public function index($keyword=null){
       
     //search by name,overview or company..........................
    $this->Session->write('ssname',$keyword);
      $sslug = $keyword;
    
         $this->User->recursive = 2;
         $order = array('User.id DESC'); 
         if($keyword=='')
         {
             $this->Session->write('ssname','');
             $users = array();
         }
        else {
              
            $str = explode(' ',$keyword);
        
                if(!empty($str[0]) && !empty($str[1])) {
                   $users = $this->User->find('all',array('conditions'=>array('AND' => array(
             'User.first_name LIKE' => $str[0].'%',
             'User.last_name LIKE' => $str[1].'%'),'User.admin_type' => 1),'limit' => 20));

               }
               else{
                   //$users = $this->User->find('all',array('joins'=>$joins,'conditions'=>array('User.first_name LIKE' => '%'.$str[0].'%')));
               $users = $this->User->find('all',array('conditions'=>array('OR' => array(
             'User.first_name LIKE' => $str[0].'%',
             'User.last_name LIKE' => $str[0].'%'),'User.admin_type' => 1),'limit' => 20));
               }
         }
         
            if(count($users)>0)
           {
              
                foreach ($users as $user) 
               {
                  $ids[]= $user['User']['id'];
               }
               
                               
              $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids),'order' => $order,'limit'=>20));  
              $searchCount=count($users);
               
            }
        else {
            $results=array();
            $searchCount=0;
        }
        
        //posts.............................................................
        $ids1=array();
        $posts = $this->Post->find('all',array('conditions'=>array(
              'Post.post_title LIKE' => '%'.$keyword.'%',
             ),'fields' => array('Post.id')));
               
                    //print_r($users);
                     if(count($posts)>0)
                    {

                         foreach ($posts as $post) 
                        {
                           $ids1[]= $post['Post']['id'];
                        }


                       

                     }
                     
                     $posts1 = $this->CourseSkill->find('all',array('conditions'=>array(
              'CourseSkill.skill_name LIKE' => '%'.$keyword.'%',
             ),'fields' => array('CourseSkill.post_id')));
               
                    //print_r($users);
                     if(count($posts1)>0)
                    {

                         foreach ($posts1 as $post1) 
                        {
                           $ids1[]= $post1['CourseSkill']['post_id'];
                        }
                    }
                   
        if(!empty($ids1))
        {
            $ids1=array_unique($ids1); 
         $results1 = $this->Post->find('all',array('conditions'=>array('Post.id' => $ids1),'limit' => 20)); 
       $searchCount1= count($results1);
        }
        else {
            $results1 = array();
            $searchCount1=0;
        }
      
      /*$skills = $this->Skill->find('all',array('fields'=>array('Skill.id','Skill.skill_name')));
      $subjects = $this->User->find('all',array('fields'=>array('User.subject'),'conditions'=>array('not' => array('User.subject' => null)),'group' => 'User.subject','recursive' => -1));
      $degrees = $this->User->find('all',array('fields'=>array('User.degree'),'conditions'=>array('not' => array('User.degree' => null)),'group' => 'User.degree','recursive' => -1));
      $courses = $this->Post->find('all',array('fields'=>array('Post.id','Post.post_title'),'recursive' => -1)); */
      
      //print_r($skills);
      //print_r($subjects);
      //print_r($degrees);
      //print_r($courses);
      $this->set(compact('results','sslug','searchCount','results1','searchCount1'));
      
    }
    
     public function searchskill($keyword=null){
       
     //search by name,overview or company..........................
    $this->Session->write('ssname',$keyword);
      $sslug = $keyword;
    
        
         if($keyword=='')
         {
             $this->Session->write('ssname','');
             $results = array();
         }
        else {
                       
               $results = $this->InstructorSkill->find('all',array('conditions'=>array(
             'InstructorSkill.skill_name LIKE' => $keyword.'%'),'limit' => 20));
               
              $results1=$this->CourseSkill->find('all',array('conditions'=>array(
      'CourseSkill.skill_name LIKE' => $keyword.'%'),'limit' => 20));
      
        }
       
      
    //print_r($results);
    //print_r($results1);die;
      $this->set(compact('results','sslug','results1'));
      
    }
    
    public function index1($keyword=null){
       
    //search by name,overview or company..........................
    $this->Session->write('ssname',$keyword);
      $sslug = $keyword;
    
         $this->User->recursive = 2;
         $order = array('User.id DESC'); 
         if($keyword=='')
         {
             $this->Session->write('ssname','');
             $users = $this->User->find('all',array('conditions'=>array('User.admin_type' => 1),'order' => $order,'limit'=>20));
         }
        else {
             $str = explode(' ',$keyword);
        
                if(!empty($str[0]) && !empty($str[1])) {
                   $users = $this->User->find('all',array('conditions'=>array('AND' => array(
             'User.first_name LIKE' => $str[0].'%',
             'User.last_name LIKE' => $str[1].'%'),'User.admin_type' => 1)));

               }
               else{
                   //$users = $this->User->find('all',array('joins'=>$joins,'conditions'=>array('User.first_name LIKE' => '%'.$str[0].'%')));
               $users = $this->User->find('all',array('conditions'=>array('OR' => array(
             'User.first_name LIKE' => $str[0].'%',
             'User.last_name LIKE' => $str[0].'%'),'User.admin_type' => 1)));
               }
         }
         
            if(count($users)>0)
           {
              
                foreach ($users as $user) 
               {
                  $ids[]= $user['User']['id'];
               }
               
                               
              $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids),'order' => $order,'limit'=>20));  
              $searchCount=count($users);
               
            }
        else {
            $results=array();
            $searchCount=0;
        }
      
      /*$skills = $this->Skill->find('all',array('fields'=>array('Skill.id','Skill.skill_name')));
      $subjects = $this->User->find('all',array('fields'=>array('User.subject'),'conditions'=>array('not' => array('User.subject' => null)),'group' => 'User.subject','recursive' => -1));
      $degrees = $this->User->find('all',array('fields'=>array('User.degree'),'conditions'=>array('not' => array('User.degree' => null)),'group' => 'User.degree','recursive' => -1));
      $courses = $this->Post->find('all',array('fields'=>array('Post.id','Post.post_title'),'recursive' => -1)); */
      
      //print_r($skills);
      //print_r($subjects);
      //print_r($degrees);
      //print_r($courses);
      $this->set(compact('results','sslug','searchCount'));
      
    }
    
     public function ajaxFilterSkill($searchterm=null){
    
        //$ssname= $this->Session->read('ssname');
       
//for experience rating.............................................................
     
       
//for rating.............................................................
      $data = array();

          

        //$searchterm = $this->request->data['searchval'];
       
   
                     $this->User->recursive = 2;

                   
            
                    $users = $this->InstructorSkill->find('all',array('conditions'=>array(
              'InstructorSkill.skill_id' => $searchterm,
             ),'fields' => array('InstructorSkill.user_id')));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids[]= $user['InstructorSkill']['user_id'];
                        }


                       $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids),'limit' => 20));  

                     }

              
             //print_r($results);die;
         
        //overall data................................................
          /* foreach ($results as $key => $res) { //print_r($res);
            $cat_name = $this->requestAction('/searches/catname/' . $res['Freelancer']['category_id']);
               $total=round($res[0]['rat'],1);
              $val .='<div class="col-md-6 col-xs-12 col-portfolio-list">
                  <a href="'.$this->webroot.'users/view/'.base64_encode($res['User']['id']).'">
                  <figure>
                  <div class="skill-top-area-pct">
                      <div class="port-pic">
                          <img src="';
                           if(isset($res['FreelancerImage'][0]['originalpath'])) 
                            { 
                              $val.= $this->webroot.'user_images/'.$res['FreelancerImage'][0]['originalpath']; 
                            } 
                            else {
                               $val.= $this->webroot.'img/avtar-pic.jpg';  
                            }
                           $val.='" alt="#">
                      </div>
                      <figcaption>
                         <div class="tp-hd-pr-nm-pr-rt"><h3 class="pr-rt-prf-nm-cl">';
                           if(isset($res['User']['first_name']) && isset($res['User']['last_name'])) 
                            { 
                              $val.= $res['User']['first_name'].' '.$res['User']['last_name']; 
                            } 
                            $val.='</h3><h3 class="pr-rt-prf">';
                            $val.='$'.$res['Freelancer']['wages'].'/Hr</h3></div>';
                                //if($total>0){
                                         $val.= '<div><div class="star str">';
                         
                                        for($x=1;$x<=$total;$x++) {
                                            $val.= '<img src="'.$this->webroot.'img/star-on.png" />';
                                        }
                                        if (strpos($total,'.')) {
                                            $val.= '<img src="'.$this->webroot.'img/star-half.png" />';
                                            $x++;
                                        }
                                        while ($x<=5) {
                                            $val.= '<img src="'.$this->webroot.'img/star-off.png" />';
                                            $x++;
                                        }
                            
                            $val.= '<span class="rt-img-nm">'.$total.'/5</span>
                        </div></div>';
                                //}
                        $val.= '<h4 class="userCategory">';
                              
                                      $val.= '<span>'.$cat_name.'</span>'; 
                               
                          $val.='</h4> </figcaption> </div>
                          <div><h4 class="user_skill">';
                              if(isset($ssname)&&$ssname!='')
                          {
                           $val.='<span>'.$ssname.'</span>'; 
                          }
                              if(count($res['Skill']>0)) { 
                                  foreach($res['Skill'] as $key =>$v) { //print_r($val);
                                      if(isset($ssname)&&$ssname!=''){
                                      if($v['skill_name']!=$ssname){
                                      $val.= '<span>'.$v['skill_name'].'</span>'; 
                                      }
                                      }
                                } } 

                         $val.='</div>
                                            </figure>
                                            </a>
                                        </div>'; 
          }
         if($val!='')  { $data['html'] = $val; $data['ACK'] = 1; } else { $data['html'] = '<div class="col-md-12  no_search">No search found</div>'; $data['ACK'] = 0;}
     */
      echo json_encode($data);
      exit;
    }
    
    public function ajaxFilterCourse($searchterm=null){
    
        //$ssname= $this->Session->read('ssname');
       
//for experience rating.............................................................
     
       
//for rating.............................................................
      $data = array();

          

        //$searchterm = $this->request->data['searchval'];
       
   
                     $this->User->recursive = 2;

                   
            
                    $users = $this->Post->find('all',array('conditions'=>array(
              'Post.id' => $searchterm,
             ),'fields' => array('Post.user_id')));
               
                    //print_r($users);
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids[]= $user['Post']['user_id'];
                        }


                       $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids),'limit' => 20));  

                     }

              
             print_r($results);die;
         
        //overall data................................................
          /* foreach ($results as $key => $res) { //print_r($res);
            $cat_name = $this->requestAction('/searches/catname/' . $res['Freelancer']['category_id']);
               $total=round($res[0]['rat'],1);
              $val .='<div class="col-md-6 col-xs-12 col-portfolio-list">
                  <a href="'.$this->webroot.'users/view/'.base64_encode($res['User']['id']).'">
                  <figure>
                  <div class="skill-top-area-pct">
                      <div class="port-pic">
                          <img src="';
                           if(isset($res['FreelancerImage'][0]['originalpath'])) 
                            { 
                              $val.= $this->webroot.'user_images/'.$res['FreelancerImage'][0]['originalpath']; 
                            } 
                            else {
                               $val.= $this->webroot.'img/avtar-pic.jpg';  
                            }
                           $val.='" alt="#">
                      </div>
                      <figcaption>
                         <div class="tp-hd-pr-nm-pr-rt"><h3 class="pr-rt-prf-nm-cl">';
                           if(isset($res['User']['first_name']) && isset($res['User']['last_name'])) 
                            { 
                              $val.= $res['User']['first_name'].' '.$res['User']['last_name']; 
                            } 
                            $val.='</h3><h3 class="pr-rt-prf">';
                            $val.='$'.$res['Freelancer']['wages'].'/Hr</h3></div>';
                                //if($total>0){
                                         $val.= '<div><div class="star str">';
                         
                                        for($x=1;$x<=$total;$x++) {
                                            $val.= '<img src="'.$this->webroot.'img/star-on.png" />';
                                        }
                                        if (strpos($total,'.')) {
                                            $val.= '<img src="'.$this->webroot.'img/star-half.png" />';
                                            $x++;
                                        }
                                        while ($x<=5) {
                                            $val.= '<img src="'.$this->webroot.'img/star-off.png" />';
                                            $x++;
                                        }
                            
                            $val.= '<span class="rt-img-nm">'.$total.'/5</span>
                        </div></div>';
                                //}
                        $val.= '<h4 class="userCategory">';
                              
                                      $val.= '<span>'.$cat_name.'</span>'; 
                               
                          $val.='</h4> </figcaption> </div>
                          <div><h4 class="user_skill">';
                              if(isset($ssname)&&$ssname!='')
                          {
                           $val.='<span>'.$ssname.'</span>'; 
                          }
                              if(count($res['Skill']>0)) { 
                                  foreach($res['Skill'] as $key =>$v) { //print_r($val);
                                      if(isset($ssname)&&$ssname!=''){
                                      if($v['skill_name']!=$ssname){
                                      $val.= '<span>'.$v['skill_name'].'</span>'; 
                                      }
                                      }
                                } } 

                         $val.='</div>
                                            </figure>
                                            </a>
                                        </div>'; 
          }
         if($val!='')  { $data['html'] = $val; $data['ACK'] = 1; } else { $data['html'] = '<div class="col-md-12  no_search">No search found</div>'; $data['ACK'] = 0;}
     */
      echo json_encode($data);
      exit;
    }
    
    public function ajaxFilterDegree($searchterm=null){
    
        //$ssname= $this->Session->read('ssname');
       
//for experience rating.............................................................
     
       
//for rating.............................................................
      $data = array();

          

        //$searchterm = $this->request->data['searchval'];
       
   
                     $this->User->recursive = 2;

                   
            
                    $users = $this->User->find('all',array('conditions'=>array(
              'User.degree' => $searchterm,
             ),'fields' => array('User.id')));
               
                    //print_r($users);
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids[]= $user['User']['id'];
                        }


                       $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids),'limit' => 20));  

                     }

              
             print_r($results);die;
         
        //overall data................................................
          /* foreach ($results as $key => $res) { //print_r($res);
            $cat_name = $this->requestAction('/searches/catname/' . $res['Freelancer']['category_id']);
               $total=round($res[0]['rat'],1);
              $val .='<div class="col-md-6 col-xs-12 col-portfolio-list">
                  <a href="'.$this->webroot.'users/view/'.base64_encode($res['User']['id']).'">
                  <figure>
                  <div class="skill-top-area-pct">
                      <div class="port-pic">
                          <img src="';
                           if(isset($res['FreelancerImage'][0]['originalpath'])) 
                            { 
                              $val.= $this->webroot.'user_images/'.$res['FreelancerImage'][0]['originalpath']; 
                            } 
                            else {
                               $val.= $this->webroot.'img/avtar-pic.jpg';  
                            }
                           $val.='" alt="#">
                      </div>
                      <figcaption>
                         <div class="tp-hd-pr-nm-pr-rt"><h3 class="pr-rt-prf-nm-cl">';
                           if(isset($res['User']['first_name']) && isset($res['User']['last_name'])) 
                            { 
                              $val.= $res['User']['first_name'].' '.$res['User']['last_name']; 
                            } 
                            $val.='</h3><h3 class="pr-rt-prf">';
                            $val.='$'.$res['Freelancer']['wages'].'/Hr</h3></div>';
                                //if($total>0){
                                         $val.= '<div><div class="star str">';
                         
                                        for($x=1;$x<=$total;$x++) {
                                            $val.= '<img src="'.$this->webroot.'img/star-on.png" />';
                                        }
                                        if (strpos($total,'.')) {
                                            $val.= '<img src="'.$this->webroot.'img/star-half.png" />';
                                            $x++;
                                        }
                                        while ($x<=5) {
                                            $val.= '<img src="'.$this->webroot.'img/star-off.png" />';
                                            $x++;
                                        }
                            
                            $val.= '<span class="rt-img-nm">'.$total.'/5</span>
                        </div></div>';
                                //}
                        $val.= '<h4 class="userCategory">';
                              
                                      $val.= '<span>'.$cat_name.'</span>'; 
                               
                          $val.='</h4> </figcaption> </div>
                          <div><h4 class="user_skill">';
                              if(isset($ssname)&&$ssname!='')
                          {
                           $val.='<span>'.$ssname.'</span>'; 
                          }
                              if(count($res['Skill']>0)) { 
                                  foreach($res['Skill'] as $key =>$v) { //print_r($val);
                                      if(isset($ssname)&&$ssname!=''){
                                      if($v['skill_name']!=$ssname){
                                      $val.= '<span>'.$v['skill_name'].'</span>'; 
                                      }
                                      }
                                } } 

                         $val.='</div>
                                            </figure>
                                            </a>
                                        </div>'; 
          }
         if($val!='')  { $data['html'] = $val; $data['ACK'] = 1; } else { $data['html'] = '<div class="col-md-12  no_search">No search found</div>'; $data['ACK'] = 0;}
     */
      echo json_encode($data);
      exit;
    }
    
    public function ajaxFilterSubject($searchterm=null){
    
        //$ssname= $this->Session->read('ssname');
       
//for experience rating.............................................................
     
       
//for rating.............................................................
      $data = array();

          

        //$searchterm = $this->request->data['searchval'];
       
   
                     $this->User->recursive = 2;

                   
            
                    $users = $this->User->find('all',array('conditions'=>array(
              'User.subject' => $searchterm,
             ),'fields' => array('User.id')));
               
                    //print_r($users);
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids[]= $user['User']['id'];
                        }


                       $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids),'limit' => 20));  

                     }

              
             print_r($results);die;
         
        //overall data................................................
          /* foreach ($results as $key => $res) { //print_r($res);
            $cat_name = $this->requestAction('/searches/catname/' . $res['Freelancer']['category_id']);
               $total=round($res[0]['rat'],1);
              $val .='<div class="col-md-6 col-xs-12 col-portfolio-list">
                  <a href="'.$this->webroot.'users/view/'.base64_encode($res['User']['id']).'">
                  <figure>
                  <div class="skill-top-area-pct">
                      <div class="port-pic">
                          <img src="';
                           if(isset($res['FreelancerImage'][0]['originalpath'])) 
                            { 
                              $val.= $this->webroot.'user_images/'.$res['FreelancerImage'][0]['originalpath']; 
                            } 
                            else {
                               $val.= $this->webroot.'img/avtar-pic.jpg';  
                            }
                           $val.='" alt="#">
                      </div>
                      <figcaption>
                         <div class="tp-hd-pr-nm-pr-rt"><h3 class="pr-rt-prf-nm-cl">';
                           if(isset($res['User']['first_name']) && isset($res['User']['last_name'])) 
                            { 
                              $val.= $res['User']['first_name'].' '.$res['User']['last_name']; 
                            } 
                            $val.='</h3><h3 class="pr-rt-prf">';
                            $val.='$'.$res['Freelancer']['wages'].'/Hr</h3></div>';
                                //if($total>0){
                                         $val.= '<div><div class="star str">';
                         
                                        for($x=1;$x<=$total;$x++) {
                                            $val.= '<img src="'.$this->webroot.'img/star-on.png" />';
                                        }
                                        if (strpos($total,'.')) {
                                            $val.= '<img src="'.$this->webroot.'img/star-half.png" />';
                                            $x++;
                                        }
                                        while ($x<=5) {
                                            $val.= '<img src="'.$this->webroot.'img/star-off.png" />';
                                            $x++;
                                        }
                            
                            $val.= '<span class="rt-img-nm">'.$total.'/5</span>
                        </div></div>';
                                //}
                        $val.= '<h4 class="userCategory">';
                              
                                      $val.= '<span>'.$cat_name.'</span>'; 
                               
                          $val.='</h4> </figcaption> </div>
                          <div><h4 class="user_skill">';
                              if(isset($ssname)&&$ssname!='')
                          {
                           $val.='<span>'.$ssname.'</span>'; 
                          }
                              if(count($res['Skill']>0)) { 
                                  foreach($res['Skill'] as $key =>$v) { //print_r($val);
                                      if(isset($ssname)&&$ssname!=''){
                                      if($v['skill_name']!=$ssname){
                                      $val.= '<span>'.$v['skill_name'].'</span>'; 
                                      }
                                      }
                                } } 

                         $val.='</div>
                                            </figure>
                                            </a>
                                        </div>'; 
          }
         if($val!='')  { $data['html'] = $val; $data['ACK'] = 1; } else { $data['html'] = '<div class="col-md-12  no_search">No search found</div>'; $data['ACK'] = 0;}
     */
      echo json_encode($data);
      exit;
    }
    
     public function searchUser()
    {
        $ids_skill=array();
        $ids_degree=array();
        $ids_course=array();
        $ids_subject=array();
        $ids_location=array();
        $ids=array();
        $data = array();
        $searchCount=0;
        $val='';
        // $ids=array_intersect($ids_skill,$ids_degree,$ids_course);
       // print_r($ids);die; 
         
       $skill= $this->request->data['skill'];
       $degree= $this->request->data['degree'];
       $course= $this->request->data['course'];
       $subject= $this->request->data['subject'];
       $location= $this->request->data['location'];
       $lat= $this->request->data['search_lat'];
       $lng= $this->request->data['search_lng'];
      if($skill==''&&$degree==''&&$course==''&&$subject==''&&$location==''&&$lat==''&&$lng=='')
      {
          $users = $this->User->find('all',array('fields'=>'User.id'));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids[]= $user['User']['id'];
                        }
                      
                     }
      }
      else
      {
       /*$location= 'Kolkata%2C+West+Bengal%2C+India';
       $lat= '22.572646';
       $lng= '88.36389499999996'; */
       if($skill!='')
       {
          
               $users = $this->InstructorSkill->find('all',array('conditions'=>array(
              'InstructorSkill.skill_id' => $skill,
             ),'fields' => array('InstructorSkill.user_id')));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids_skill[]= $user['InstructorSkill']['user_id'];
                        }
                      
                     }
                     
                     
       }
       
       
       if($degree!='')
       {
          
               $users = $this->User->find('all',array('conditions'=>array(
              'User.degree' => $degree,
             ),'fields'=>'User.degree','User.id'));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids_degree[]= $user['User']['id'];
                        }
                      
                     }
       }
       
       if($subject!='')
       {
          
               $users = $this->User->find('all',array('conditions'=>array(
              'User.subject' => $subject,
             ),'fields'=>'User.subject','User.id'));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids_subject[]= $user['User']['id'];
                        }
                      
                     }
       }
       
       if($course!='')
       {
          
               $users = $this->Post->find('all',array('conditions'=>array(
              'Post.id' => $course,
             ),'fields' => array('Post.user_id')));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids_course[]= $user['Post']['user_id'];
                        }
                      
                     }
                     
                     
       }
       if($location!='')
       {
           $conditions = array();
          if(!empty($lat) && !empty($lng)) {
                $this->User->virtualFields = array(
                    'distance' => "(3959 * acos(cos(radians('".$lat."')) * cos(radians(User.user_lat)) * cos( radians(User.user_lon) - radians('".$lng."')) + sin(radians('".$lat."')) * sin(radians(User.user_lat))))"
                );
                 $conditions['User.distance < '] = '50';
            
               $users = $this->User->find('all',array('conditions'=>$conditions,'fields'=>'User.id'));
               
                     if(count($users)>0)
                    {

                         foreach ($users as $user) 
                        {
                           $ids_location[]= $user['User']['id'];
                        }
                      
                     }
             }
             
             //print_r($ids_location);die;
       }
        
       if($skill!='')
       {
           $ids= array();
           $ids=$ids_skill;
           if($degree!='')
           {
              $ids= array_intersect($ids,$ids_degree);
           }
           if($course!='')
           {
              $ids= array_intersect($ids,$ids_course);
           }
           if($subject!='')
           {
              $ids= array_intersect($ids,$ids_subject);
           }
           if($location!='')
           {
              $ids= array_intersect($ids,$ids_location);
           }
          
       } 
       elseif($degree!='')
       {
            $ids= array();
           $ids=$ids_degree;
          
           if($course!='')
           {
              $ids= array_intersect($ids,$ids_course);
           }
           if($subject!='')
           {
              $ids= array_intersect($ids,$ids_subject);
           }
            if($location!='')
           {
              $ids= array_intersect($ids,$ids_location);
           }
       }
       elseif($subject!='')
       {
            $ids= array();
           $ids=$ids_subject;
          
            if($course!='')
           {
              $ids= array_intersect($ids,$ids_course);
           }
            if($location!='')
           {
              $ids= array_intersect($ids,$ids_location);
           }
           
       }
       elseif($course!='')
       {
           $ids= array();
           $ids=$ids_course;
            if($location!='')
           {
              $ids= array_intersect($ids,$ids_location);
           }
                     
       }
       
       elseif($location!='')
       {
           $ids= array();
           $ids=$ids_location;
           
                     
       }
      }  
      
      //for sort by.............................................................
       if(isset($this->request->data['order_filter'])){
                if($this->request->data['order_filter']=='newest'){
                    $order_by = array('User.id DESC');
                }
                if($this->request->data['order_filter']=="popularity"){
                    $order_by = array('User.popularity DESC');
                }
                if($this->request->data['order_filter']=="review"){
                    $order_by = array('User.avg_rating DESC'); 
                }
//                if($this->request->data['order_filter']=="language"){
//                    $order_by = array('User.price ASC'); 
//               }
               
                $sort = $this->request->data['order_filter'];
            }
            else{
                $order_by = '';
                $sort = '';
            }
            
             $order = '';

            if(!empty($order_by)){
                $order = $order_by;
            }
       
       if(!empty($ids))
        {
           
           if($this->Session->read('ssname')!=null&&$this->Session->read('ssname')!='')
            {
                $ssname = $this->Session->read('ssname');
                 $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids,'OR' => array(
      'User.first_name LIKE' => $ssname.'%',
      'User.last_name LIKE' => $ssname.'%'),'User.admin_type' => 1),'order' => $order,'limit' => 20));  
            }
            else
            {
                $results = $this->User->find('all',array('conditions'=>array('User.id' => $ids,'User.admin_type' => 1),'order' => $order,'limit' => 20));  
            }
             $searchCount=count($results);
            foreach ($results as $result) 
            {
                if($result['User']['user_image']!=null||$result['User']['user_image']!='')
                {
                    $img=$this->webroot.'user_images/'.$result['User']['user_image'];
                }
                else
                {
                    $img=$this->webroot.'img/noimage.png'; 
                }
                 
                $val .=' <div class="col-md-6">
                        <div class="instructors">
                                <div class="media">
                                  <div class="media-left">
                                    <a href="#">
                                      <img src="'.$img.'" alt="" width="190" height="190">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">'.$result['User']['first_name'].' '.$result['User']['last_name'].'</h4>
                                    
                                    <p>'.$result['User']['email_address'].'</p>
                                   <p>'.$result['User']['address'].'</p>
                                  </div>
                                </div>
                        </div>
                </div>';  
            }
            
        }
        
        if($val!='')  { $data['html'] = $val;  $data['ACK'] = 1; $data['searchCount'] = $searchCount;} else { $data['html'] = '<div class="col-md-6  no_search">No instructor found</div>'; $data['ACK'] = 0; $data['searchCount'] = $searchCount;}
     
        echo json_encode($data);
        exit;
    }
    
     function skillsuggest($keyword = null)
    {
       $this->layout = false;
      
        $display_json = array();
        $json_arr = array();

        $QueryStr='1';
        if($keyword!=''){
            $QueryStr.=" AND (Skill.skill_name LIKE '%".$keyword."%')";
        }
        

        $users = $this->Skill->find('all', array(
            'conditions' => array($QueryStr)));

        foreach ($users as $user) {
            $json_arr["label"] = $user['Skill']['skill_name'];
            $json_arr["link"] = $user['Skill']['id'];
            array_push($display_json, $json_arr);
        }

        $jsonWrite = json_encode($display_json); //encode that search data
        echo $jsonWrite;
        exit;
    }
    
        function degreesuggest($keyword = null)
    {
       $this->layout = false;
      
        $display_json = array();
        $json_arr = array();

        $QueryStr='1';
        if($keyword!=''){
            $QueryStr.=" AND (User.degree LIKE '%".$keyword."%')";
        }
        

        $users = $this->User->find('all', array(
            'conditions' => array($QueryStr),'fields'=>'User.degree','group' => 'User.degree'));

        foreach ($users as $user) {
            $json_arr["label"] = $user['User']['degree'];
            $json_arr["link"] = $user['User']['degree'];
            array_push($display_json, $json_arr);
        }

        $jsonWrite = json_encode($display_json); //encode that search data
        echo $jsonWrite;
        exit;
    }
    
          function subjectsuggest($keyword = null)
    {
       $this->layout = false;
      
        $display_json = array();
        $json_arr = array();

        $QueryStr='1';
        if($keyword!=''){
            $QueryStr.=" AND (User.subject LIKE '%".$keyword."%')";
        }
        

        $users = $this->User->find('all', array(
            'conditions' => array($QueryStr),'fields'=>'User.subject','group' => 'User.subject'));

        foreach ($users as $user) {
            $json_arr["label"] = $user['User']['subject'];
            $json_arr["link"] = $user['User']['subject'];
            array_push($display_json, $json_arr);
        }

        $jsonWrite = json_encode($display_json); //encode that search data
        echo $jsonWrite;
        exit;
    }
    
     function coursesuggest($keyword = null)
    {
       $this->layout = false;
      
        $display_json = array();
        $json_arr = array();

        $QueryStr='1';
        if($keyword!=''){
            $QueryStr.=" AND (Post.post_title LIKE '%".$keyword."%')";
        }
        

        $users = $this->Post->find('all', array(
            'conditions' => array($QueryStr)));

        foreach ($users as $user) {
            $json_arr["label"] = $user['Post']['post_title'];
            $json_arr["link"] = $user['Post']['id'];
            array_push($display_json, $json_arr);
        }

        $jsonWrite = json_encode($display_json); //encode that search data
        echo $jsonWrite;
        exit;
    }
    
    function followinstrutor() {
        $this->layout = false;
        $this->autoRender = false;
        $userid = $this->Session->read('userid');

        $this->loadModel('FollowInstructor');

        $this->request->data = array();

        if ($this->request->is('post')) {

            $insid = $_REQUEST['id'];


            $options = array('conditions' => array('FollowInstructor.user_id' => $userid, 'FollowInstructor.instructor_id' => $insid));
            $skillexistscheck = $this->FollowInstructor->find('first', $options);

            $date = gmdate("Y-m-d");


            $this->request->data['FollowInstructor']['user_id'] = $userid;
            $this->request->data['FollowInstructor']['instructor_id'] = $insid;
            $this->request->data['FollowInstructor']['added_date'] = $date;

            if (!$skillexistscheck) {
                $this->FollowInstructor->create();

                if ($this->FollowInstructor->save($this->request->data)) {

                    $last_id = $this->FollowInstructor->getInsertID();

                    echo $insid;
                }
            } else {
                echo "0";
            }
        }
        //echo json_encode($ret);
        exit;
    }

    function unfollowinstrutor($id = null) {
        $this->loadModel('FollowInstructor');
        $this->layout = false;
        $this->autoRender = false;
        $insid = $_REQUEST['id'];
        $userid = $this->Session->read('userid');
        $ret = array();
        if ($this->FollowInstructor->deleteAll(['FollowInstructor.user_id' => $userid, 'FollowInstructor.instructor_id' => $insid])) {
            echo $insid;
        } else {
            echo "0";
        }
        //echo json_encode($ret);
        exit;
    }

    function favinstrutor() {
        $this->layout = false;
        $this->autoRender = false;
        $userid = $this->Session->read('userid');

        $this->loadModel('FavouriteInstructor');

        $this->request->data = array();

        if ($this->request->is('post')) {

            $insid = $_REQUEST['id'];


            $options = array('conditions' => array('FavouriteInstructor.user_id' => $userid, 'FavouriteInstructor.instructor_id' => $insid));
            $skillexistscheck = $this->FavouriteInstructor->find('first', $options);

            $date = gmdate("Y-m-d");


            $this->request->data['FavouriteInstructor']['user_id'] = $userid;
            $this->request->data['FavouriteInstructor']['instructor_id'] = $insid;
            $this->request->data['FavouriteInstructor']['added_date'] = $date;

            if (!$skillexistscheck) {
                $this->FavouriteInstructor->create();

                if ($this->FavouriteInstructor->save($this->request->data)) {

                    $last_id = $this->FavouriteInstructor->getInsertID();

                    echo $insid;
                }
            } else {
                echo "0";
            }
        }
        //echo json_encode($ret);
        exit;
    }

    function unfavinstrutor($id = null) {
        $this->loadModel('FavouriteInstructor');
        $this->layout = false;
        $this->autoRender = false;
        $insid = $_REQUEST['id'];
        $userid = $this->Session->read('userid');
        $ret = array();
        if ($this->FavouriteInstructor->deleteAll(['FavouriteInstructor.user_id' => $userid, 'FavouriteInstructor.instructor_id' => $insid])) {
            echo $insid;
        } else {
            echo "0";
        }
        //echo json_encode($ret);
        exit;
    }

}
