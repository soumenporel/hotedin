<?php

App::uses('AppController', 'Controller');

class PostjobsController extends AppController {


    public function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
    }

    public function add() {
        $this->loadModel('Pagebanner');

        $pagebanner = $this->Pagebanner->find('first', array(
                                                  'conditions'=>array('Pagebanner.id'=>48),
                                                  array('Pagebanner.status' => 1)
                                                ));
        $this->set(compact('pagebanner'));
        $userid = $this->Session->read('userid');
        if(!$userid) {
            $this->redirect(array('controller' =>'Users', 'action' => 'login'));
        }
        $this->loadModel('Jobtype');
        $jobtypes = $this->Jobtype->find('all');

        $this->loadModel('JobCategory');
        $jobCategories = $this->JobCategory->find('all');

        $this->loadModel('Country');
        $countries = $this->Country->find('all');

        $this->loadModel('CompanyDetail');
        $companydetails = $this->CompanyDetail->find('first', array(
            'conditions' => array('CompanyDetail.user_id' => $userid)
        ));

        $this->loadModel('User');
        $user = $this->User->find('first', array(
            'conditions' => array('User.id' => $userid)
        ));
        $this->loadModel('EmployerMembershipPlan');
        $plans = $this->EmployerMembershipPlan->find('all');

        $this->set(compact('jobtypes', 'jobCategories', 'countries', 'companydetails', 'user', 'plans'));

    }

    public function getStates() {
        if ($this->request->is('post')) {
            $this->loadModel('State');
            $states = $this->State->find('all', array(
                'conditions' => array('State.country_id' => $this->request->data['val'])
            ));
            echo json_encode($states);
        }
        exit;
    }

    public function preview() {
        $userid = $this->Session->read('userid');
        if(!$userid) {
            $this->redirect(array('controller' =>'Users', 'action' => 'login'));
        }
        $this->request->data['Job']['user_id'] = $userid;
        $arr = $this->request->data;

        $this->loadModel('Jobtype');
        $this->loadModel('JobCategory');

        $jobtype = $this->Jobtype->find('first', array(
            'conditions' => array('Jobtype.id' => $arr['Job']['job_type'])
        ));
        $jobtypes = $jobtype['Jobtype']['name'];

        $category = $this->JobCategory->find('first', array(
            'conditions' => array('JobCategory.id' => $arr['Job']['category'])
        ));
        $categorys = $category['JobCategory']['title'];
        $this->set(compact('arr', 'jobtypes', 'categorys'));



    }

    public function submit() {

        $userid = $this->Session->read('userid');
        if(!$userid) {
            $this->redirect(array('controller' =>'Users', 'action' => 'login'));
        }
        if ($this->request->is('post')) {
            $this->loadModel('Job');
            $ip = $this->get_client_ip();
            $new_arr[]= unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
            $lat = $new_arr[0]['geoplugin_latitude'];
            $lon = $new_arr[0]['geoplugin_longitude'];
            $this->request->data['Job']['lon'] = $lon;
            $this->request->data['Job']['lat'] = $lat;
            $this->loadModel('Country');
            $country = $this->Country->find('first', array(
                'conditions' => array('Country.id' => $this->request->data['Job']['country'])
            ));
            $this->request->data['Job']['country_name'] = $country['Country']['name'];
            $this->request->data['Job']['user_id'] = $userid;
            $this->request->data['Job']['created'] = date('Y-m-d');
            $this->Job->create();
            if ($this->Job->save($this->request->data)) {
                $this->Session->setFlash('Job Posted!');
                return $this->redirect( array('controller' =>'Users', 'action' => 'dashboard'));
            }
        }
    }



}

?>
