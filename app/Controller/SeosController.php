<?php

App::uses('AppController', 'Controller');

/**
 * Seos Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class SeosController extends AppController {

    public $components = array('Paginator', 'Session');

    public function admin_listing() {
        $seos = $this->Seo->find('all');

        $this->Seo->recursive = 0;
        $this->set('seos', $this->Paginator->paginate());
    }

    public function admin_add() {
        if ($this->request->is('post')) {

            if ($this->Seo->save($this->request->data)) {
                $this->Session->setFlash(__('New post saved successfully to the database'));
                return $this->redirect(array('action' => 'listing'));
            } else {

                $this->Session->setFlash('Unable to add user. Please, try again.');
            }
        }
    }

    public function admin_edit($id = null) {

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Seo->save($this->request->data)) {
                $this->Session->setFlash(__('New post saved successfully to the database'));
                return $this->redirect(array('action' => 'listing'));
            } else {

                $this->Session->setFlash('Unable to add user. Please, try again.');
            }
        } else {
            $options = array('conditions' => array('Seo.id' => $id));
            $this->request->data = $this->Seo->find('first', $options);
        }
    }

    public function admin_view($id = NULL) {

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->Seo->exists($id)) {
            throw new NotFoundException(__('Invalid Seo Keywords'));
        }
        $options = array('conditions' => array('Seo.' . $this->Seo->primaryKey => $id));
        $seo = $this->Seo->find('first', $options);
                
        $this->set(compact('seo'));

    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }
        $this->Seo->id = $id;
        if (!$this->Seo->exists()) {
            throw new NotFoundException(__('Invalid Seo Keywords'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Seo->delete()) {
            $this->Session->setFlash(__('The Seo Keywords has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Seo Keywords could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'listing'));
    }

}
