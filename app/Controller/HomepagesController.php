 <?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class HomepagesController extends AppController {

    public $components = array('Paginator', 'Session');
    var $uses = array('User', 'Newsletter', 'Setting', 'Content', 'Activity', 'Contact', 'Category', 'Post', 'Homeslider', 'Comment', 'Partner','Wishlist','TempCart','Rating');

    public function index() {

        $this->loadModel('Banner');
        $banners = $this->Banner->find('all'
             , array(
             'conditions' => array(
                 'Banner.status' => 1
             ),'order' => array('Banner.order ASC'),
         )
            );


        $this->loadModel('ManageHomepage');

        $homecontent = $this->ManageHomepage->find('first', array(
                                          'conditions'=>array('id'=>1)
                                        ));
        $homecontent1 = $this->ManageHomepage->find('first', array(
                                          'conditions'=>array('id'=>2)
                                        ));

        $homecontent2 = $this->ManageHomepage->find('first', array(
                                          'conditions'=>array('id'=>3)
                                        ));

        $homecontent3 = $this->ManageHomepage->find('first', array(
                                          'conditions'=>array('id'=>4)
                                        ));

        $this->loadModel('Blog');
        $blogs = $this->Blog->find('all',array(
            'conditions' => array('is_feature'=>3),
            'limit' => 3
        ));

        $this->loadModel('Testimonial');
        $Testimonials = $this->Testimonial->find('all', array(
                                          'conditions'=>array('Testimonial.status'=>1)
                                      ));

        $this->loadModel('Client');
        $Clients = $this->Client->find('all', array(
                                            'conditions'=>array('Client.feature'=>1),
                                            'limit' => 5
                                            ));

        $this->loadModel('Howitwork');
        $howitworks = $this->Howitwork->find('all');

        $this->loadModel('AppPromotion');
        $AppPromotions = $this->AppPromotion->find('all',array(
                                        'conditions' =>array('AppPromotion.active' => 1)
                                        ));


        $this->set(compact('banners', 'AppPromotions', 'howitworks', 'homecontent', 'homecontent1', 'homecontent2', 'homecontent3', 'Clients', 'blogs', 'Testimonials'));

    }

    public function itemList($id){
        $this->loadModel('PlanItem');

        $options = array('fields'=>array('PlanItem.id','PlanItem.plan_id'),'conditions' => array('`PlanItem`.`item_id`' => $id));
        $itemList = $this->PlanItem->find('list', $options);
        return $itemList;
        exit;
    }

    public function postdata($id) {
        $this->loadModel('Post');
        $this->Post->recursive = 2;
        $options = array('conditions' => array('`Post`.`is_approve`' => 1,  '`Post`.`category_id`' => $id),'limit'=>8,'order'=>array('Post.id'=>'DESC'));
        $courseoftheday = $this->Post->find('all', $options);
        //print_r($courseoftheday);die;
        return $courseoftheday;
        exit;
    }

    public function postdatanewsletter() {
        $email = $this->request->data['email'];
        $this->loadModel('Newsletter');
        $options = array('conditions' => array('`Newsletter`.`email`' => $email));
        $courseoftheday = $this->Newsletter->find('first', $options);
        if (empty($courseoftheday)) {
            $this->request->data['Newsletter']['email'] = $email;
            $this->Newsletter->create();
            $this->Newsletter->save($this->request->data);
            echo "1";
        } else {
            echo "0";
        }
        exit;
    }

    public function subcategory($id) {
        $this->loadModel('Category');
        $options = array('conditions' => array('`Category`.`parent_id`' => $id, '`Category`.`category_of_the_day`' => 1, '`Category`.`show_in_homepage`' => 1));
        $this->Category->recursive = 0;
        $courseoftheday = $this->Category->find('all', $options);
        if (!empty($courseoftheday)) {
            return $courseoftheday;
            exit;
        } else {
            $courseoftheday = array();
            return $courseoftheday;
            exit;
        }
    }

    public function homepage_autocomplete() {
        $this->autoRender = false;
        $data = array();
        $this->loadModel('Post');
        $this->loadModel('User');
        $this->loadModel('Tag');
        $this->loadModel('PostTag');

        if ($this->request->is('post')) {
            $q = $this->request->data['q'];

            $db = $this->Post->getDataSource();
            $subQuery = $db->buildStatement(
                array(
                    'fields'     => array('PT.post_id'),
                    'table'      => 'post_tags',
                    'alias'      => 'PT',
                    'joins'      => array(
                        array(
                            'table' => 'posts',
                            'alias' => 'P',
                            'type' => 'inner',
                            'conditions' => array(
                                'P.id = PT.post_id'
                            )
                        ),
                        array(
                            'table' => 'tags',
                            'alias' => 'T',
                            'type' => 'inner',
                            'conditions' => array(
                                'T.id = PT.tag_id'
                            )
                        )
                    ),
                    'conditions' => array(
                        'T.name LIKE' => '%'.$q.'%'
                    ),
                    'group'      => array("PT.post_id")
                ),
                $this->Post
            );
            $subQueryExpression = $db->expression($subQuery);
            //print_r($subQueryExpression); exit;
            $courses = $this->Post->query($subQueryExpression->value);

            $post_ids = '';
            foreach ($courses as $key => $course) {
                $post_ids[] = $course['PT']['post_id'];
            }

            $this->Post->recursive = 0;
            $data['Course'] = $this->Post->find('all', array(
                'fields' => array('Post.slug', 'Post.post_title'),
                'conditions' => array(
                    'Post.post_title LIKE' => '%' . $q . '%'
                ),
                'limit' => 4
            ));
            $data['Taged_Course'] = $this->Post->find('all', array(
                'fields' => array('Post.slug', 'Post.post_title'),
                'conditions' => array(
                    'Post.id' => $post_ids
                ),
                'limit' => 4
            ));


            $this->User->recursive = 0;
            $data["Users"] = $this->User->find('all', array(
                'fields' => array('User.first_name', 'User.last_name'),
                'conditions' => array(
                    "concat(' ',User.first_name,User.last_name) LIKE '%$q%'"
                ),
                'limit' => 4
            ));
        }

        echo json_encode($data);
    }

     public function autosuggest($keyword = null) {

        //$str = explode(' ',$keyword);

        $str=$keyword;
        $this->layout = false;

        $this->loadModel('User');
        $this->loadModel('Post');
        $this->loadModel('Skill');

        $display_json = array();



       if(!empty($str)) {

        $users = $this->User->find('all',array('conditions'=>array('OR' => array(
      'User.first_name LIKE' => $str.'%',
      'User.last_name LIKE' => $str.'%'),'User.admin_type' => 1),'limit' => 4));

         $posts = $this->Post->find('all',array('conditions'=>array(
      'Post.post_title LIKE' => $str.'%'),'limit' => 4));

          $skills = $this->Skill->find('all',array('conditions'=>array(
      'Skill.skill_name LIKE' => $str.'%'),'limit' => 4));
        }
        /*if(!empty($str[0]) && !empty($str[1]) && !empty($str[2])){
            $users = $this->User->find('all',array('joins'=>$joins,'conditions'=>array('User.first_name LIKE' => '%'.$str[0].'%','User.biography LIKE' => '%'.$str[1].'%','UserEmployment.company LIKE' => '%'.$str[2].'%')));
        } */

        //$users = $this->User->find('all',array('joins'=>$joins,'conditions'=>array('OR'=>array('User.first_name LIKE' => '%'.$str[0].'%','User.biography LIKE' => '%'.$str[1].'%','UserEmployment.company LIKE' => '%'.$str[2].'%'))));
        foreach ($posts as $post) {

            $val= $this->webroot.'img/all-course-icon.png';
            $json_arr["label"] = $post['Post']['post_title'];
            //$json_arr["link"] = $user['User']['first_name']." ".$user['User']['last_name'];
            //$json_arr["link"] = Router::url(array('controller' => 'Users', 'action' => 'view', base64_encode($user['User']['id'])), true);
            $json_arr["link"] = Router::url(array('controller' => 'posts', 'action' => 'details', $post['Post']['slug']), true);

            $json_arr["icon"] = $val;
            //$json_arr["desc"] = $company;
            //$json_arr["cat"] = $cat_name;
            array_push($display_json, $json_arr);
        }

        foreach ($users as $user) {

           //print_r($user);die;
            $html='';

           if($user['User']['user_image']!=''||$user['User']['user_image']!=NULL)
             {
               $val= $this->webroot.'user_images/'.$user['User']['user_image'];
             }
             else {
                $val= 'img/profile_img.jpg';
             }

             $html.='<span class="searchImg"><img src="'.$val.'" alt="#" width="50" height="50"/></span>';
             $html.='<span class="searchName">'.$user['User']['first_name']." ".$user['User']['last_name'].'</span>';


            $json_arr["label"] = $user['User']['first_name']." ".$user['User']['last_name'];
            //$json_arr["link"] = $user['User']['first_name']." ".$user['User']['last_name'];
            //$json_arr["link"] = Router::url(array('controller' => 'Users', 'action' => 'view', base64_encode($user['User']['id'])), true);
            $json_arr["link"] = Router::url(array('controller' => 'Users', 'action' => 'user_profile', $user['User']['id']), true);

            $json_arr["icon"] = $val;
            //$json_arr["desc"] = $company;
            //$json_arr["cat"] = $cat_name;
            array_push($display_json, $json_arr);
        }



     foreach ($skills as $skill) {

            $val= $this->webroot.'img/skills.png';
            $json_arr["label"] = $skill['Skill']['skill_name'];
            //$json_arr["link"] = $user['User']['first_name']." ".$user['User']['last_name'];
            //$json_arr["link"] = Router::url(array('controller' => 'Users', 'action' => 'view', base64_encode($user['User']['id'])), true);
            $json_arr["link"] = Router::url(array('controller' => 'searches', 'action' => 'searchskill', $skill['Skill']['skill_name']), true);

            $json_arr["icon"] = $val;
            //$json_arr["desc"] = $company;
            //$json_arr["cat"] = $cat_name;
            array_push($display_json, $json_arr);
        }


        $jsonWrite = json_encode($display_json); //encode that search data
        echo $jsonWrite;
        exit;

    }


    public function viewers($id) {
        $this->loadModel('ReadLecture');

       $totalView=0;
        $options_video = array('conditions' => array('ReadLecture.post_id'=> $id),'group' => 'ReadLecture.user_id','recursive'=>1);
        $read_videos = $this->ReadLecture->find('all', $options_video);
        $totalView=count($read_videos);
        return $totalView;
        exit;
    }

}
