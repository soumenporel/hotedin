<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class  PressReleasesController extends AppController {

    public $components = array('Paginator', 'Session');

    public function index() {
         $this->loadModel('PressCategory');
        $title_for_layout = 'Press List';
        
        $options = array('order' => array('PressRelease.id' => 'desc'));
        $this->Paginator->settings = $options;
        $this->set('press_list', $this->Paginator->paginate('PressRelease'));
         $featured_blog = $this->PressRelease->find('all',array('conditions' => array('PressRelease.is_feature'=>1),'order'=>array('PressRelease.id'=>'desc'),'limit'=>'4'));
         $category_blog = $this->PressCategory->find('all',array('conditions' => array('PressCategory.show_in_homepage'=>1,'PressCategory.status'=>1),'order'=>array('PressCategory.id'=>'desc'),'limit'=>'4'));
        
        $this->set(compact('title_for_layout','featured_blog','category_blog'));
        
    }

    public function details($slug=null){
    	//echo $slug; 
    	if(isset($slug) && $slug !=''){
    		$options = array('conditions' => array('PressRelease.slug' => $slug));
        	$press_details = $this->PressRelease->find('first', $options);
    	}
    	$this->set(compact('press_details'));
    	
    }

    public function admin_add() {
        $this->loadModel('PressCategory');
        $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $title_for_layout = 'Press Add';
            $categories = $this->PressCategory->find('list', array('fields' => array('PressCategory.id', 'PressCategory.category_name')));
            if ($this->request->is('post')) {
                
                $this->PressRelease->create();
                if ($this->PressRelease->save($this->request->data)) {
                        $this->Session->setFlash(__('The press has been saved.'));
                        return $this->redirect(array('action' => 'index'));
                } else {
                        $this->Session->setFlash(__('The press could not be saved. Please, try again.'));
                }
            }
            $this->set(compact('title_for_layout','categories'));
    }

    public function admin_index() {	
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $title_for_layout = 'Press List';
        //$this->Category->recursive = 0;
        $options = array('order' => array('PressRelease.id' => 'desc'));
        $this->Paginator->settings = $options;
        $this->set('press_list', $this->Paginator->paginate('PressRelease'));
        $this->set(compact('title_for_layout'));
	}

	public function admin_edit($id = null) {
        $this->loadModel('PressCategory');
        $is_admin = $this->Session->read('is_admin');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $title_for_layout = 'Press Edit';
    	if (!$this->PressRelease->exists($id)) {
                throw new NotFoundException(__('Invalid Press'));
        }
        $categories = $this->PressCategory->find('list', array('fields' => array('PressCategory.id', 'PressCategory.category_name')));
        if ($this->request->is(array('post', 'put'))) {
            //$options = array('conditions' => array('Blog.title'  => $this->request->data['Blog']['title'], 'Blog.cat_id'=>$this->request->data['Blog']['cat_id']));
            //$name = $this->Blog->find('first', $options);
            $name = array();
           
            if ($this->PressRelease->save($this->request->data)) {
                $this->Session->setFlash(__('The press has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The press could not be saved. Please, try again.'));
                return $this->redirect(array('action' => 'admin_edit/'.$id));
            }
            
        } else {
           
            $options = array('conditions' => array('PressRelease.' . $this->PressRelease->primaryKey => $id));
            $this->request->data = $this->PressRelease->find('first', $options);
        }
        $this->set(compact('title_for_layout','categories'));
	}

	public function admin_delete($id = null) {
        $is_admin = $this->Session->read('is_admin');
        
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $this->PressRelease->id = $id;
        if (!$this->PressRelease->exists()) {
                throw new NotFoundException(__('Invalid PressRelease'));
        }
        
        if ($this->PressRelease->delete($id)) {
            
            $this->Session->setFlash(__('The press has been deleted.'));
        } else {
            $this->Session->setFlash(__('The press could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
	}

    
}
