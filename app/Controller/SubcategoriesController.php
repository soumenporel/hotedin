<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class SubcategoriesController extends AppController {

    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    var $uses = array('User', 'Category','Post', 'Country');
    
    /**
     * Components
     *
     * @var array
     */
    
    /**
     * index method
     *
     * @return void
     */
    
    public function admin_index() {
        $this->loadModel('Country');
        $countries = $this->Country->find('list');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Country'])) {
            $Country = $this->request->data['Country'];
        } else {
            $Country = '';
        }
        $QueryStr = "(Category.parent_id = '" . 0 . "') AND (Category.show_in_homepage = '" . 1 . "')";
        if ($keywords != '') {
            $QueryStr.=" AND (Category.category_name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (Category.status = '" . $Newsearch_is_active . "')";
        }
        if ($Country != '') {
            $QueryStr.=" AND (Category.country_id=" . $Country . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('Category.category_name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Category List';
        $this->Category->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('Category'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Country'));
    }

    public function admin_list_subcategory() {
        $this->loadModel('Country');
        $countries = $this->Country->find('list');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $Newsearch_is_active = $this->request->data['search_is_active'];
        } else {
            $Newsearch_is_active = '';
        }
        if (isset($this->request->data['Country'])) {
            $Country = $this->request->data['Country'];
        } else {
            $Country = '';
        }
        $QueryStr = "(Category.parent_id != '" . 0 . "') AND (Category.show_in_homepage = '" . 1 . "')";
        if ($keywords != '') {
            $QueryStr.=" AND (Category.category_name LIKE '%" . $keywords . "%')";
        }
        if ($Newsearch_is_active != '') {
            $QueryStr.=" AND (Category.status = '" . $Newsearch_is_active . "')";
        }
        if ($Country != '') {
            $QueryStr.=" AND (Category.country_id=" . $Country . ")";
        }
        $options = array('conditions' => array($QueryStr), 'order' => array('Category.category_name' => 'ASC'));

        $this->Paginator->settings = $options;
        $title_for_layout = 'Category List';
        $this->Category->recursive = 1;
        $this->set('categories', $this->Paginator->paginate('Category'));
        $this->set(compact('title_for_layout', 'countries', 'keywords', 'Newsearch_is_active', 'Country'));
    }

    
    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Category View';
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid Category'));
        }
        $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
        $category = $this->Category->find('first', $options);
        #pr($category);
        if ($category) {
            $options = array('conditions' => array('Category.id' => $category['Category']['parent_id']));
            $categoryname = $this->Category->find('list', $options);
            #pr($categoryname);
            if ($categoryname) {
                $categoryname = $category['Category']['category_name'];
            } else {
                $categoryname = '';
            }
        }
        $this->set(compact('title_for_layout', 'category', 'categoryname'));
    }

    public function admin_add() {
        $this->loadModel('Country');
        $this->loadModel('Category');
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->Category->Country->find('list');
        $categories = $this->Category->find('list', array('fields' => array('Category.id', 'Category.category_name')));
        //print_r($country);
        $title_for_layout = 'Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('Category.category_name' => $this->request->data['Category']['category_name']));
            $name = $this->Category->find('first', $options);
            if (!$name) {

                if (!empty($this->request->data['Category']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['Category']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['Category']['parent_id'] = $this->request->data['Category']['categories'] ? $this->request->data['Category']['categories'] : 0;
                $this->Category->create();
                $this->request->data['Category']['show_in_homepage'] = 1;
                $this->request->data['Category']['status'] = 1;
                
                if ($this->Category->save($this->request->data)) {
                    
                    //------ Create CMS Page For This Category ---------//
                    // $data['title']          = $this->request->data['Category']['category_name'];
                    // $data['page_url']       = $this->request->data['Category']['category_name'];
                    // $data['created']        = date('Y-m-d H:m:s');
                    // $data['header_menu']    = 1;
                    // $this->loadModel('WpPage');
                    // $this->WpPage->save($data);
                    //------ End Of Create CMS Page For This Category ---------//                    
                    
                    $this->request->data1['CategoryImage']['category_id'] = $this->Category->id;
                    $this->CategoryImage->save($this->request->data1);
                    $this->Session->setFlash(__('The category has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category name already exists. Please, try again.'));
            }
        }
        $this->set(compact('parents', 'title_for_layout', 'countries', 'categories'));
    }

    public function admin_add_sub_category() {
        $this->loadModel('Country');
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->Category->Country->find('list');
        $categories = $this->Category->find('list', array('fields' => array('Category.id','Category.category_name'),'conditions' => array('Category.parent_id'=>0,'Category.show_in_homepage'=>1)));
        //print_r($country);
        $title_for_layout = 'Category Add';
        if ($this->request->is('post')) {
            $options = array('conditions' => array('Category.category_name' => $this->request->data['Category']['category_name']));
            $name = $this->Category->find('first', $options);
            if (!$name) {

                if (!empty($this->request->data['Category']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['Category']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['Category']['parent_id'] = $this->request->data['Category']['categories'] ? $this->request->data['Category']['categories'] : 0;
                $this->Category->create();
                $this->request->data['Category']['show_in_homepage'] = 1;
                $this->request->data['Category']['status'] = 1;
                if ($this->Category->save($this->request->data)) {

                    //------ Create CMS Page For This Category ---------//
                    // $data['title']          = $this->request->data['Category']['category_name'];
                    // $data['page_url']       = $this->request->data['Category']['category_name'];
                    // $data['created']        = date('Y-m-d H:m:s');
                    // $data['header_menu']    = 1;
                    // $this->loadModel('WpPage');
                    // $this->WpPage->save($data);
                    //------ End Of Create CMS Page For This Category ---------// 

                    $this->request->data1['CategoryImage']['category_id'] = $this->Category->id;
                    $this->CategoryImage->save($this->request->data1);
                    $this->Session->setFlash(__('The category has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'admin_list_subcategory'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category name already exists. Please, try again.'));
            }
        }
        $this->set(compact('parents', 'title_for_layout', 'countries', 'categories'));
    }

    
    public function edit($id = null) {
        $userid = $this->Session->read('userid');
        if (!isset($userid) && $userid == '') {
            $this->redirect('/admin');
        }
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $users = $this->Category->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_edit($id = null) {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->Category->Country->find('list');
        $categories = $this->Category->find('list', array('fields' => array('Category.id', 'Category.category_name'), 'conditions' => array('Category.id <>' => $id)));
        
        $catDetails = $this->Category->find('first',array('conditions'=>array('Category.id'=>$id),'fields'=>array('Category.slug')));
        $catSlug = $catDetails['Category']['slug'];
        $this->loadModel('WpPage');
        $cmsPage = $this->WpPage->find('first',array('conditions'=>array('WpPage.slug'=>$catSlug),'fields'=>array('WpPage.id')));

        //echo $id;exit;
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('Category.category_name' => $this->request->data['Category']['category_name'], 'Category.id <>' => $id));
            $name = $this->Category->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

                if (!empty($this->request->data['Category']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['Category']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                        $this->request->data1['CategoryImage']['id'] = $this->request->data['Category']['categoryimage_id'];
                        $this->request->data1['CategoryImage']['category_id'] = $id;
                        $this->CategoryImage->save($this->request->data1);
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->request->data['Category']['image'] = $this->request->data['Category']['hide_img'];
                }

                $this->request->data['Category']['parent_id'] = $this->request->data['Category']['categories'];
                if ($this->Category->save($this->request->data)) {

                    //------ Edit CMS Page For This Category ---------//
                    // $data['id']             = $cmsPage['WpPage']['id'];
                    // $data['title']          = $this->request->data['Category']['category_name'];
                    // $data['page_url']       = $this->request->data['Category']['category_name'];
                    // $this->loadModel('WpPage');
                    // $this->WpPage->save($data);
                    //------ Edit Of Create CMS Page For This Category ---------//

                    $this->Session->setFlash(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
            $is_parent = $this->Category->find('count', array('conditions' => array('Category.parent_id' => 0, 'Category.id' => $id)));
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);

            //print_r($this->request->data);
        }
        $this->set(compact('is_parent', 'countries', 'categories'));
    }

    public function admin_edit_subcat($id = null) {
        $this->loadModel('CategoryImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        $this->request->data1 = array();
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $countries = $this->Category->Country->find('list');
        $categories = $this->Category->find('list', array('fields' => array('Category.id', 'Category.category_name'), 'conditions' => array('Category.id <>' => $id,'Category.parent_id '=>0,'Category.show_in_homepage'=>1)));
        
        $catDetails = $this->Category->find('first',array('conditions'=>array('Category.id'=>$id),'fields'=>array('Category.slug')));
        $catSlug = $catDetails['Category']['slug'];
        $this->loadModel('WpPage');
        $cmsPage = $this->WpPage->find('first',array('conditions'=>array('WpPage.slug'=>$catSlug),'fields'=>array('WpPage.id')));
  

        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //echo "hello";exit;
            $options = array('conditions' => array('Category.category_name' => $this->request->data['Category']['category_name'], 'Category.id <>' => $id));
            $name = $this->Category->find('first', $options);

            if (!$name) {
                //echo "hello";exit;

                if (!empty($this->request->data['Category']['image']['name'])) {
                    $pathpart = pathinfo($this->request->data['Category']['image']['name']);
                    $ext = $pathpart['extension'];
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif', 'svg');
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder = "img/cat_img";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                        $this->request->data1['CategoryImage']['originalpath'] = $filename;
                        $this->request->data1['CategoryImage']['resizepath'] = $filename;
                        $this->request->data1['CategoryImage']['id'] = $this->request->data['Category']['categoryimage_id'];
                        $this->request->data1['CategoryImage']['category_id'] = $id;
                        $this->CategoryImage->save($this->request->data1);
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->request->data['Category']['image'] = $this->request->data['Category']['hide_img'];
                }

                $this->request->data['Category']['parent_id'] = $this->request->data['Category']['categories'];
                if ($this->Category->save($this->request->data)) {

                    //------ Edit CMS Page For This Category ---------//
                    // $data['id']             = $cmsPage['WpPage']['id'];
                    // $data['title']          = $this->request->data['Category']['category_name'];
                    // $data['page_url']       = $this->request->data['Category']['category_name'];
                    // $this->loadModel('WpPage');
                    // $this->WpPage->save($data);
                    //------ Edit Of Create CMS Page For This Category ---------//

                    $this->Session->setFlash(__('The category has been saved.'));
                    return $this->redirect(array('action' => 'admin_list_subcategory'));
                } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The category already exists. Please, try again.'));
            }
        } else {
            //echo "hello";exit;
            $is_parent = $this->Category->find('count', array('conditions' => array('Category.parent_id' => 0, 'Category.id' => $id)));
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);

            //print_r($this->request->data);
        }
        $this->set(compact('is_parent', 'countries', 'categories'));
    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        $options = array('conditions' => array('Category.parent_id' => $id));
        $cat = $this->Category->find('list', $options);
        #pr($cat);
        #exit;
        if ($cat) {
            foreach ($cat as $k => $v) {
                $options1 = array('conditions' => array('Category.parent_id' => $k));
                $subcat = $this->Category->find('list', $options1);
                if ($subcat) {
                    foreach ($subcat as $k1 => $v1) {
                        $this->Category->delete($k1);
                    }
                }
                $this->Category->delete($k);
            }
        }
        
        if ($this->Category->delete($id)) {
            $this->Session->setFlash(__('The category has been deleted.' ,'default', array(), 'good'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
}
