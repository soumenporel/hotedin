<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class CertificatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Certificates';

    public $data2='' ;
    public $components = array('Paginator', 'Session');
    var $uses = array('User', 'Post', 'QuizScore', 'Lecture');

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $options = array('conditions' => array('`QuizScore`.`is_certificate`' => 1), 'order' => array('QuizScore.id' => 'desc'));
        $this->Paginator->settings = $options;
        $title_for_layout = 'Certificate List';
        $certificates=$this->Paginator->paginate('QuizScore');
        for($i=0;$i<count($certificates);$i++)
        {
          
            $optionsLecture = array('conditions' => array('Lecture.id' => $certificates[$i]['QuizScore']['quiz_id']));
            $LectureDetails = $this->Lecture->find('first', $optionsLecture);
            $course=$LectureDetails['Post']['post_title'];
            $certificates[$i]['course_name']=$course; 
            $certificates[$i]['quiz_name']=$LectureDetails['Lecture']['title'];
            
             $this->User->recursive=-1;
             $optionsUser = array('conditions' => array('User.id' => $certificates[$i]['QuizScore']['user_id']));
             $UserDetails = $this->User->find('first', $optionsUser);
             $certificates[$i]['user_name']=$UserDetails['User']['first_name']." ".$UserDetails['User']['last_name'];
        }
       
       
        $this->set('posts', $certificates);
        $this->set(compact('title_for_layout', 'posts'));
    }
}
