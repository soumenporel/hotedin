<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class Clientscontroller extends AppController {

	  public function admin_index() {


        $title_for_layout = 'Client';
        $this->set(compact('title_for_layout'));
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $this->loadModel('Client');
       
        $this->set('clients', $this->Client->find('all'));
    }
    
     public function admin_add() {
      
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $title_for_layout = 'Client Add';
        $this->set(compact('title_for_layout'));
        if ($this->request->is('post')) {
           if (isset($this->request->data['Client']['image']) && $this->request->data['Client']['image']['name'] != '') {
                $path = $this->request->data['Client']['image']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if ($ext) {
                    $uploadPath = Configure::read('UPLOAD_CLIENT_PATH');
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array($ext, $extensionValid)) {
                        $imageName = rand() . '_' . (strtolower(trim($this->request->data['Client']['image']['name'])));
                        $full_image_path = $uploadPath . '/' . $imageName;
                        move_uploaded_file($this->request->data['Client']['image']['tmp_name'], $full_image_path);
                        $this->request->data['Client']['image'] = $imageName;
                    } else {
                        $this->Session->setFlash(__('Invalid Image Type.'));
                        return $this->redirect(array('action' => 'edit', $id));
                    }
                }
            } else {
                $this->request->data['Client']['image'] = $this->request->data['Client']['image'];
            }
                $this->Client->create();
                #pr($this->data);
                #exit;
                if ($this->Client->save($this->request->data)) {
                   
                    $this->Session->setFlash(__('The client has been saved.', 'default', array('class' => 'success')));
                    return $this->redirect(array('action' => 'admin_index'));
                } else {
                    $this->Session->setFlash(__('The client could not be saved. Please, try again.', 'default', array('class' => 'error')));
                }
            } 
        }
   

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->loadModel('ClientImage');
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $title_for_layout = 'Client Edit';
        $this->set(compact('title_for_layout'));
        if (!$this->Client->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
           if (isset($this->request->data['Client']['image']) && $this->request->data['Client']['image']['name'] != '') {
                $path = $this->request->data['Client']['image']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if ($ext) {
                    $uploadPath = Configure::read('UPLOAD_CLIENT_PATH');
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array($ext, $extensionValid)) {
                        $OldImg = $this->request->data['Client']['saved_image'];
                        $imageName = rand() . '_' . (strtolower(trim($this->request->data['Client']['image']['name'])));
                        $full_image_path = $uploadPath . '/' . $imageName;
                        move_uploaded_file($this->request->data['Client']['image']['tmp_name'], $full_image_path);
                        $this->request->data['Client']['image'] = $imageName;
                        if ($OldImg != '') {
                            unlink($uploadPath . '/' . $OldImg);
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid Image Type.'));
                        return $this->redirect(array('action' => 'edit', $id));
                    }
                }
            } else {
                unset($this->request->data['Client']['image']);
            }
            if ($this->Client->save($this->request->data)) {
                $this->Session->setFlash(__('The client has been saved.'));
                return $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(__('The client could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Client.' . $this->Client->primaryKey => $id));
            $this->request->data = $this->Client->find('first', $options);
           // print_r($this->request->data);die;
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
       
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Client->id = $id;
        if (!$this->Client->exists()) {
            throw new NotFoundException(__('Invalid client'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Client->delete()) {
            //$this->ClientImage->delete()
            $this->Session->setFlash(__('The client has been deleted.'));
        } else {
            $this->Session->setFlash(__('The client could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }

}