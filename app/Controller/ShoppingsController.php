<?php

App::uses('AppController', 'Controller');

/**
 * Settings Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class ShoppingsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','Session');

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        //$userid = $this->Session->read('userid');
        //if (!isset($userid) && $userid == '') {
        //    $this->redirect('/admin');
        //}
		
		$this->loadModel('Cart');
		$this->Cart->recursive = 2;
		$Shoppings = $this->Cart->find('all');
		
        $this->set('Shoppings', $Shoppings);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
		$this->loadModel('CartItem');
        //if (!$this->CartItem->exists($id)) {
            //throw new NotFoundException(__('Invalid Shopping cart'));
        //}
		$this->CartItem->recursive=2;
        $options = array('conditions' => array('CartItem.cart_id'=>$id));
		$cartitemvalue=$this->CartItem->find('all', $options);
		//pr($cartitemvalue);exit;
        $this->set('cartitem',$cartitemvalue);
	}
	
	public function admin_delete($id = null,$itemid=null) {
        //$is_admin = $this->Session->read('is_admin');
        //if (!isset($is_admin) && $is_admin == '') {
         //   $this->redirect('/admin');
        //}
		//echo $id.$itemid;exit;
		$this->loadModel('CartItem');
		//echo $this->CartItem->cart_id;exit;
        //$this->CartItem->cart_id = $id;
        

        //$this->request->onlyAllow('post', 'delete');
		
        if ($this->CartItem->deleteAll(array(
                        'CartItem.post_id' => $itemid,
                       'CartItem.cart_id' => $id
                   ), false)) 
		{
            $this->Session->setFlash(__('The cart has been deleted.'));
        } else {
            $this->Session->setFlash(__('The cart could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }
}