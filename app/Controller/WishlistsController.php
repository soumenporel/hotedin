<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class WishlistsController extends AppController {
    /* function beforeFilter() {
      parent::beforeFilter();
      } */

    /**
     * Components
     *
     * @var array
     */
    //public $name = 'Users';
    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    var $uses = array('User', 'Wishlist');

    public function index() {
        $userid = $this->Session->read('userid');
        $wishlist = $this->Wishlist->find('all', array('conditions' => array('Wishlist.user_id' => $userid), 'recursive' => 2));
        $this->set(compact('wishlist'));
    }

    public function ajaxremoveWishlist() {

        $wishlist_id = $this->request->data['id'];

        $this->Wishlist->id = $wishlist_id;
        if (!$this->Wishlist->exists()) {
            throw new NotFoundException(__('Invalid contact'));
        }

        $this->request->onlyAllow('post', 'delete');
        if ($this->Wishlist->delete()) {
            echo '1';
        } else {
            echo '0';
        }
        exit;
    }

    public function cart_to_wishlist() {
        $this->autoRender = false;
        $data = array();

        $this->loadModel('Cart');
        $this->loadModel('CartItem');
        $this->loadModel('SaveLater');

        if ($this->request->is('post')) {
            $cartid = $this->request->data['cartid'];
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];
            $saveLater = array();
            if (!empty($cartid)) {
                $conditions = array(
                    'Wishlist.post_id' => $postid,
                    'Wishlist.user_id' => $userid
                );

                if (!$this->Wishlist->hasAny($conditions)) {

                    if ($this->CartItem->deleteAll(array('CartItem.cart_id' => $cartid, 'CartItem.post_id' => $postid), false)) {
                        $saveLater['Wishlist'] = array(
                            'user_id' => $userid,
                            'post_id' => $postid
                        );

                        if ($this->Wishlist->save($saveLater)) {
                            $this->CartItem->recursive = 2;
                            $cartItems = $this->CartItem->find('all', array(
                                'conditions' => array(
                                    'CartItem.cart_id' => $cartid
                                )
                            ));

                            $this->SaveLater->recursive = 2;
                            $saveLater = $this->SaveLater->find('all', array(
                                'conditions' => array(
                                    'SaveLater.user_id' => $userid
                                )
                            ));

                            $this->Wishlist->recursive = 2;
                            $wishlist = $this->Wishlist->find('all', array(
                                'conditions' => array(
                                    'Wishlist.user_id' => $userid
                                )
                            ));

                            $price = 0;
                            $old_price = 0;
                            foreach ($cartItems as $cartItem) {
                                $price += $cartItem['Post']['price'];
                                $old_price += $cartItem['Post']['old_price'];
                            }

                            $data['ack'] = 1;
                            $data['html']['CartItems'] = $cartItems;
                            $data['html']['SaveLater'] = $saveLater;
                            $data['html']['Wishlist'] = $wishlist;
                            $data['html']['cnt'] = count($cartItems);
                            $data['html']['price'] = $price;
                            $data['html']['old_price'] = $old_price;
                            $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price) : 0;
                        } else {
                            $data['ack'] = 0;
                            $data['msg'] = 'DB Error!!!';
                        }
                    } else {
                        $data['ack'] = 0;
                        $data['msg'] = 'DB Error!!!';
                    }
                } else {
                    $data['ack'] = 0;
                    $data['msg'] = 'Already exists!!!';
                }
            }
        }

        echo json_encode($data);
    }

    public function remove_wishlist() {
        $this->autoRender = false;
        $data = array();

        $this->loadModel('Cart');
        $this->loadModel('CartItem');
        $this->loadModel('SaveLater');

        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            if (!empty($userid)) {
                if ($this->Wishlist->deleteAll(array('Wishlist.user_id' => $userid, 'Wishlist.post_id' => $postid), false)) {
                    $cart = $this->Cart->find('first', array('conditions' => array('Cart.user_id' => $userid)));
                    $this->CartItem->recursive = 2;
                    $cartItems = $this->CartItem->find('all', array(
                        'conditions' => array(
                            'CartItem.cart_id' => $cart['Cart']['id']
                        )
                    ));

                    $this->SaveLater->recursive = 2;
                    $saveLater = $this->SaveLater->find('all', array(
                        'conditions' => array(
                            'SaveLater.user_id' => $userid
                        )
                    ));

                    $this->Wishlist->recursive = 2;
                    $wishlist = $this->Wishlist->find('all', array(
                        'conditions' => array(
                            'Wishlist.user_id' => $userid
                        )
                    ));

                    $price = 0;
                    $old_price = 0;
                    foreach ($cartItems as $cartItem) {
                        $price += $cartItem['Post']['price'];
                        $old_price += $cartItem['Post']['old_price'];
                    }

                    $data['ack'] = 1;
                    $data['html']['CartItems'] = $cartItems;
                    $data['html']['SaveLater'] = $saveLater;
                    $data['html']['Wishlist'] = $wishlist;
                    $data['html']['cnt'] = count($cartItems);
                    $data['html']['price'] = $price;
                    $data['html']['old_price'] = $old_price;
                    $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price) : 0;
                } else {
                    $data['ack'] = 0;
                    $data['msg'] = 'DB Error!!!';
                }
            }
        }

        echo json_encode($data);
    }

    public function add_to_wishlist() {
        $this->autoRender = false;
        $data = array();

        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            $conditions = array(
                'Wishlist.post_id' => $postid,
                'Wishlist.user_id' => $userid
            );
            if (!$this->Wishlist->hasAny($conditions)) {
                $wishlist['Wishlist'] = array('post_id' => $postid, 'user_id' => $userid);
                $this->Wishlist->create();
                if ($this->Wishlist->save($wishlist)) {
                    $data['Ack'] = 1;
                    $data['res'] = 'This Course Successfully Added to Wishlist';
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Failed to save!!!';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Already exists!!!';
            }

            echo json_encode($data);
        }
    }

    public function remove_from_wishlist() {
        $this->autoRender = false;
        $data = array();

        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            $conditions = array(
                'Wishlist.post_id' => $postid,
                'Wishlist.user_id' => $userid
            );
            if ($this->Wishlist->hasAny($conditions)) {
                $wishlist = array('Wishlist.post_id' => $postid, 'Wishlist.user_id' => $userid);
                if ($this->Wishlist->deleteAll($wishlist)) {
                    $data['Ack'] = 1;
                    $data['res'] = 'This Course Successfully Removed from Wishlist';
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Failed to remove!!!';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'No Course exists!!!';
            }

            echo json_encode($data);
        }
    }

}
