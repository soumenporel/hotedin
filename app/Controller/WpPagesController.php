<?php
App::uses('AppController', 'Controller');

class WpPagesController extends AppController {

    //public $uses = array('Faq','Faqcategory','WpPage');
	public function display($slug = NULL) {

	 $this->loadModel('Pagebanner');
		$cmsPage = $this->WpPage->find('first', array(
            'conditions' => array(
                'WpPage.slug' => $slug
            )
        ));
          $page_id=$cmsPage['WpPage']['id'];
           $banners = $this->Pagebanner->find('all', array(
            'conditions' => array(
                'Pagebanner.page_id' => $page_id
            )
        ));
//        if (!$cmsPage) {
//            throw new NotFoundException(__('Invalid CmsPage'));
//        }
        $this->set(compact('cmsPage','banners'));

         if($slug=='faq')
       {

          $this->loadModel('Faq');
          $this->loadModel('Faqcategory');
             $blogs=$this->Faq->find('all',array('order'=>array('Faq.order' => 'asc')));
            $this->set('blogs', $blogs);
             return $this->render('faq');

       }
       elseif($slug=='contact-us') {
           return $this->render('contactus');

       }
//       elseif($slug=='about-us') {
//           return $this->render('aboutus');
//
//       }
       elseif($slug=='our-clients') {
            $this->loadModel('Client');

             $clients=$this->Client->find('all',array('order'=>array('Client.id' => 'desc')));
            $this->set('clients', $clients);
           return $this->render('clients');

       }
       elseif($slug=='in-the-news-n-media') {
           return $this->render('news');

       }
	}

	public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->CmsPage->recursive = 0;
        $this->set('content', $this->Paginator->paginate());
    }

    // public function display($slug = NULL) {
    //     $cmsPage = $this->CmsPage->find('first', array(
    //         'conditions' => array(
    //             'CmsPage.slug' => $slug
    //         )
    //     ));
    //     if (!$cmsPage) {
    //         throw new NotFoundException(__('Invalid CmsPage'));
    //     }
    //     $this->set(compact('cmsPage'));
    // }

    public function preview($slug = NULL) {
        $cmsPage = $this->WpPage->find('first', array(
            'conditions' => array(
                'WpPage.slug' => $slug
            )
        ));
        if (!$cmsPage) {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
        $this->set(compact('cmsPage'));
    }

    public function admin_index() {

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $is_active = $this->request->data['search_is_active'];
        } else {
            $is_active = '';
        }


        $QueryStr = "(WpPage.header_menu = 0)";
        if ($keywords != '') {
            $QueryStr.=" AND (WpPage.title LIKE '%" . $keywords . "%')";
        }
        if ($is_active != '') {
            $QueryStr.=" AND (WpPage.status = '" . $is_active . "')";
        }
        $condition = array($QueryStr);

        $title_for_layout = 'CmsPage List';

        $options = array(
            'conditions' => $condition,
            'order' => array(
                'WpPage.order' => 'ASC'
            )
        );

        $this->Paginator->settings = $options;

        $this->WpPage->recursive = 0;

        $this->set('contents',$this->Paginator->paginate());
        $this->set(compact('title_for_layout', 'keywords', 'is_active'));
    }

    public function admin_menu_pages() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $is_active = $this->request->data['search_is_active'];
        } else {
            $is_active = '';
        }

        $QueryStr = "(WpPage.header_menu = 1 )";;
        if ($keywords != '') {
            $QueryStr.=" AND (WpPage.title LIKE '%" . $keywords . "%')";
        }
        if ($is_active != '') {
            $QueryStr.=" AND (WpPage.status = '" . $is_active . "')";
        }
        $condition = array($QueryStr);

        $title_for_layout = 'CmsPage List';

        $options = array(
            'conditions' => $condition,
            'order' => array(
                'WpPage.order' => 'ASC'
            )
        );

        $this->Paginator->settings = $options;

        $this->WpPage->recursive = 0;
        $this->set('contents', $this->Paginator->paginate());
        $this->set(compact('title_for_layout', 'keywords', 'is_active'));
    }

    public function admin_export() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $contents = $this->CmsPage->find('all');

        $output = '';
        $output .='Page Heading, CmsPage';
        $output .="\n";

        if (!empty($contents)) {
            foreach ($contents as $content) {
                $output .='"' . html_entity_decode($content['CmsPage']['page_heading']) . '","' . strip_tags($content['CmsPage']['content']) . '"';
                $output .="\n";
            }
        }
        $filename = "contents" . time() . ".csv";
        header('CmsPage-type: application/csv');
        header('CmsPage-Disposition: attachment; filename=' . $filename);
        echo $output;
        exit;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($page_name = null) {
        $this->loadModel('Contact');
        $this->loadModel('User');
        if (isset($page_name) && $page_name != '') {
            $options = array('conditions' => array('CmsPage.page_title' => $page_name));
            $content = $this->CmsPage->find('first', $options);
            if ($this->request->is(array('post', 'put'))) {
                $this->request->data['Contact']['contact_date'] = date('Y-m-d');
                $UserEmail = $this->request->data['Contact']['email'];
                $UserID = $this->request->data['Contact']['user_id'];
                if ($UserID == '') {
                    $options = array('conditions' => array('User.email' => $UserEmail));
                    $userEmailDetails = $this->User->find('first', $options);
                    //pr($userEmailDetails);
                    if (count($userEmailDetails) > 0) {
                        $this->request->data['Contact']['user_id'] = $userEmailDetails['User']['id'];
                    } else {
                        $this->request->data['Contact']['user_id'] = 0;
                    }
                }
                if ($this->request->data['Contact']['type'] == 2) {
                    $link_data = $this->request->data['Contact']['link'];
                    $ExpLink_data = end(explode('/', $link_data));
                    if ($ExpLink_data != '') {
                        $task_id = base64_decode($ExpLink_data);
                    } else {
                        $task_id = 0;
                    }
                    $this->request->data['Contact']['task_id'] = $task_id;
                } else {
                    $this->request->data['Contact']['task_id'] = 0;
                }

                if ($this->Contact->save($this->request->data)) {
                    $this->Session->setFlash(__('Thank you, we\'ll get back to you shortly.'));
                    return $this->redirect(array('action' => 'view/' . $page_name));
                } else {
                    $this->Session->setFlash(__('Your details could not be saved. Please, try again.'));
                }
            }
            if ($content) {
                $title_for_layout = $content['CmsPage']['page_heading'];
                $page_title = $content['CmsPage']['page_title'];
                $this->set(compact('title_for_layout', 'content', 'page_title'));
            }
        } else {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
    }

    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'CmsPage View';
        if (!$this->WpPage->exists($id)) {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
        $options = array('conditions' => array('WpPage.' . $this->WpPage->primaryKey => $id));
        $content = $this->WpPage->find('first', $options);
        $this->set(compact('title_for_layout', 'content'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
        $users = $this->Category->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $string = implode(',', $this->request->data['dat']);
            $this->request->data['WpPage']['points'] = $string;
            $this->request->data['WpPage']['slug'] = str_replace(' ', '-',  strtolower($this->request->data['WpPage']['title']));
            if(isset($this->request->data['WpPage']['image']) && $this->request->data['WpPage']['image']['name']!=''){
                    $ext = explode('/',$this->request->data['WpPage']['image']['type']);
                    if($ext){
                        $uploadFolder = "cms_page";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array($ext[1],$extensionValid)){

                            $max_width = "600";
                            $size = getimagesize($this->request->data['WpPage']['image']['tmp_name']);

                            $width = $size[0];
                            $height = $size[1];
                            $imageName = time().'_'.(strtolower(trim($this->request->data['WpPage']['image']['name'])));
                            $full_image_path = $uploadPath . '/' . $imageName;
                            move_uploaded_file($this->request->data['WpPage']['image']['tmp_name'],$full_image_path);
                            $this->request->data['WpPage']['image'] = $imageName;
                        } else{
                            $this->Session->setFlash(__('Please upload image of .jpg, .jpeg, .png or .gif format.'));
                            return $this->redirect(array('action' => 'admin_add'));
                        }
                    }
            }else{
                $this->request->data['WpPage']['image'] = '';
            }
            $this->WpPage->create();
            if ($this->WpPage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $users = $this->Category->User->find('list');
        $this->set(compact('users'));
    }

    public function admin_edit($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->WpPage->exists($id)) {
            throw new NotFoundException(__('Invalid content'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $string = implode(',', $this->request->data['dat']);
            $this->request->data['WpPage']['points'] = $string;
            if (isset($this->request->data['WpPage']['image']) && $this->request->data['WpPage']['image']['name'] != '') {
              $path = $this->request->data['WpPage']['image']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if ($ext) {
                    $uploadFolder = "cms_page";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                    if (in_array($ext, $extensionValid)) {
                        $OldImg = $this->request->data['WpPage']['hide_img'];
                        $imageName = rand() . '_' . (strtolower(trim($this->request->data['WpPage']['image']['name'])));
                        $full_image_path = $uploadPath . '/' . $imageName;
                        move_uploaded_file($this->request->data['WpPage']['image']['tmp_name'], $full_image_path);
                        $this->request->data['WpPage']['image'] = $imageName;
                        if ($OldImg != '') {
                            unlink($uploadPath . '/' . $OldImg);
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid Image Type.'));
                        return $this->redirect(array('action' => 'edit', $id));
                    }
                }
            } else {
                $this->request->data['WpPage']['image'] = $this->request->data['hide_img'];
            }
            if ($this->WpPage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.', array('class' => 'success')));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('WpPage.' . $this->WpPage->primaryKey => $id));
            $this->request->data = $this->WpPage->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Category->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->WpPage->id = $id;
        if (!$this->WpPage->exists()) {
            throw new NotFoundException(__('Invalid CMS Page'));
        }
        if ($this->WpPage->delete($id)) {
            $this->Session->setFlash(__('The CMS Page has been deleted.'));
        } else {
            $this->Session->setFlash(__('The CMS Page could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function how_it_works() {

    }

    public function faq() {
        $this->loadModel('Faq');
        $this->loadModel('FaqCategory');
        $title_for_layout = 'Faq';
        $CatOptions = array('conditions' => array('FaqCategory.active' => 1, 'FaqCategory.parent_id' => 0));
        $FaqCat = $this->FaqCategory->find('all', $CatOptions);
        //$FaqOptions = array('conditions' => array('Faq.is_active' => 1));
        //$FaqData = $this->Faq->find('all', $FaqOptions);
        $this->set(compact('title_for_layout', 'FaqCat'));
    }

    public function faq_category_wise($id = null) {
        $this->loadModel('Faq');
        $FaqOptions = array('conditions' => array('Faq.is_active' => 1, 'Faq.faq_category_id' => $id));
        $FaqData = $this->Faq->find('all', $FaqOptions);
        return $FaqData;
        exit;
    }

    public function blog() {

    }

    public function single_blog() {

    }

    public function admin_ajaxorder() {
        $this->layout = false;
        $order = 1;
        foreach($this->request->data['sort_order'] as $id) {
            $data['WpPage']['id'] = $id;
            $data['WpPage']['order'] = $order;
            $this->WpPage->save($data);
            $order++;
        }
        die();
    }

    public function bulkAction(){
        $data = array();
        if(!empty($this->request->data)){
            if($this->request->data['action_type']==1){
                //delete
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->WpPage->id = $value;
                    $this->WpPage->delete();
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Deleted';
            }
            if($this->request->data['action_type']==2){
                //approve
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->WpPage->id = $value;
                    $this->WpPage->saveField('status', 1);
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Approved';

            }
            if($this->request->data['action_type']==3){
                //disapprove
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->WpPage->id = $value;
                    $this->WpPage->saveField('status', 0);
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Disapprove';

            }
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = 'Error..';
        }
      echo json_encode($data);
      exit;
    }

    public function ajaxCMSPrewiew(){
         $id = $this->request->data['page_id'];
         $options = array('conditions' => array('CmsPage.id' => $id));
         $data = $this->CmsPage->find('first', $options);
         if(!empty($data)){
            $res['html'] = $data['CmsPage']['page_description'];
            $res['Ack'] = 1;
         }
         else
         {
            $res['Ack'] = 0;
         }
         echo json_encode($res); exit;
    }



}
