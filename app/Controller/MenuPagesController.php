<?php
App::uses('AppController', 'Controller');

class MenuPagesController extends AppController {

	public $components = array('Paginator');
     var $uses = array('WpPage');
        
    public function admin_index() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if (isset($this->request->data['keyword'])) {
            $keywords = $this->request->data['keyword'];
        } else {
            $keywords = '';
        }
        if (isset($this->request->data['search_is_active'])) {
            $is_active = $this->request->data['search_is_active'];
        } else {
            $is_active = '';
        }
        
        $QueryStr = "(WpPage.header_menu = 1 )";
        if ($keywords != '') {
            $QueryStr.=" AND (WpPage.title LIKE '%" . $keywords . "%')";
        }
        if ($is_active != '') {
            $QueryStr.=" AND (WpPage.status = '" . $is_active . "')";
        }
        $condition = array($QueryStr);

        $title_for_layout = 'CmsPage List';
        
        $options = array(
            'conditions' => $condition,
            'order' => array(
                'WpPage.order' => 'ASC'
            )
        );
        
        $this->Paginator->settings = $options;
        
        $this->WpPage->recursive = 0;
        $this->set('contents', $this->Paginator->paginate());
        $this->set(compact('title_for_layout', 'keywords', 'is_active'));
    }

      

   
    public function admin_view($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'CmsPage View';
        if (!$this->WpPage->exists($id)) {
            throw new NotFoundException(__('Invalid CmsPage'));
        }
        $options = array('conditions' => array('WpPage.' . $this->WpPage->primaryKey => $id));
        $content = $this->WpPage->find('first', $options);
        $this->set(compact('title_for_layout', 'content'));
    }

    

    public function admin_add() {
        $this->loadModel('Category');
        if ($this->request->is('post')) {
            $this->request->data['WpPage']['slug'] = str_replace(' ', '-',  strtolower($this->request->data['WpPage']['title']));
            $this->request->data['WpPage']['header_menu'] = 1;

            $this->WpPage->create();
            if ($this->WpPage->save($this->request->data)) {
                $catId = $this->request->data['WpPage']['category_id'];
                $this->Category->id = $catId;
                $this->Category->saveField('page_status', 1);
                $this->Session->setFlash(__('The content has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        }

        $this->loadModel('Category');
        $categories = $this->Category->find('list',array('conditions'=>array('Category.show_in_homepage'=>1,'Category.page_status'=>0),'fields'=>array('Category.id','Category.category_name')));
        $this->set(compact('categories'));
    }

    public function admin_edit($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->WpPage->exists($id)) {
            throw new NotFoundException(__('Invalid content'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->WpPage->save($this->request->data)) {
                $this->Session->setFlash(__('The content has been saved.', array('class' => 'success')));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('WpPage.' . $this->WpPage->primaryKey => $id));
            $this->request->data = $this->WpPage->find('first', $options);
        }
    }

    

    public function admin_delete($id = null) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $pageDetails = $this->WpPage->find('first',array('conditions'=>array('WpPage.id'=>$id),'fields'=>array('WpPage.category_id')));
        $this->WpPage->id = $id;
        if (!$this->WpPage->exists()) {
            throw new NotFoundException(__('Invalid CMS Page'));
        }
        if ($this->WpPage->delete($id)) {
            $this->loadModel('Category');
            $catID = $pageDetails['WpPage']['category_id'];
            $this->Category->id = $catID;
            $this->Category->saveField('page_status', 0);
            $this->Session->setFlash(__('The content has been deleted.'));
        } else {
            $this->Session->setFlash(__('The content could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    
    
    public function admin_ajaxorder() {
        $this->layout = false;
        $order = 1;
        foreach($this->request->data['sort_order'] as $id) {
            $data['WpPage']['id'] = $id;
            $data['WpPage']['order'] = $order;
            $this->WpPage->save($data);
            $order++;
        }
        die();
    }

    public function bulkAction(){
        $data = array();
        if(!empty($this->request->data)){
            if($this->request->data['action_type']==1){
                //delete
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->WpPage->id = $value;
                    $this->WpPage->delete();
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Deleted';
            }
            if($this->request->data['action_type']==2){
                //approve
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->WpPage->id = $value;
                    $this->WpPage->saveField('status', 1);
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Approved';

            }
            if($this->request->data['action_type']==3){
                //disapprove
                foreach ($this->request->data['cms_ids'] as  $value) {
                    $this->WpPage->id = $value;
                    $this->WpPage->saveField('status', 0);
                }
                $data['Ack'] = 1;
                        $data['res'] = 'All Selected CMS Pages are Disapprove';

            }
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = 'Error..';
        }
      echo json_encode($data);
      exit;      
    }

    public function ajaxCMSPrewiew(){
         $id = $this->request->data['page_id'];
         $options = array('conditions' => array('CmsPage.id' => $id));
         $data = $this->CmsPage->find('first', $options);
         if(!empty($data)){
            $res['html'] = $data['CmsPage']['page_description'];
            $res['Ack'] = 1;
         }
         else
         {
            $res['Ack'] = 0;
         }
         echo json_encode($res); exit;
    }



}
