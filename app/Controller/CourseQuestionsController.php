<?php

App::uses('AppController', 'Controller');

/**
 * Privacies Controller
 *
 * @property Privacy $Privacy
 * @property PaginatorComponent $Paginator
 */
class CourseQuestionsController extends AppController {

	public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');

	public function ajaxAddQuestion(){
		
		$data = array();
		$question['title'] = $this->request->data['title'];
		$question['description'] = $this->request->data['description'];
		$question['post_id'] = $this->request->data['postID'];
		$question['user_id'] = $this->request->data['userID'];
		$question['post_time'] = gmdate("y-m-d");

		if($this->CourseQuestion->save($question)){
			$id = $this->CourseQuestion->getLastInsertID();
			$data['Ack'] = 1;
			$data['res'] = 'New question is posted';
			$data['id'] = $id;
		}
		else{
			$data['Ack'] = 0;
		}

		echo json_encode($data);
		exit;
	}
	
}