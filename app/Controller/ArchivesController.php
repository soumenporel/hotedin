<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class ArchivesController extends AppController {
    /* function beforeFilter() {
      parent::beforeFilter();
      } */

    /**
     * Components
     *
     * @var array
     */
    //public $name = 'Users';
    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    var $uses = array('User', 'Wishlist','Favorite','Archive');

    

    public function archive_this_course() {
        $this->autoRender = false;
        $data = array();
        
        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            $conditions = array(
                'Archive.post_id' => $postid,
                'Archive.user_id' => $userid
            );
            if (!$this->Archive->hasAny($conditions)) {
                $archive['Archive'] = array('post_id' => $postid, 'user_id' => $userid);
                $this->Archive->create();
                if ($this->Archive->save($archive)) {
                    $data['Ack'] = 1;
                    $data['res'] = 'This Course Successfully Added to Fevorite';
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Failed to save!!!';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'Already exists!!!';
            }

            echo json_encode($data);
        }
    }

    public function unarchive_this_course() {
        $this->autoRender = false;
        $data = array();

        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];

            $conditions = array(
                'Archive.post_id' => $postid,
                'Archive.user_id' => $userid
            );
            if ($this->Archive->hasAny($conditions)) {
                $archive = array('Archive.post_id' => $postid, 'Archive.user_id' => $userid);
                if ($this->Archive->deleteAll($archive)) {
                    $data['Ack'] = 1;
                    $data['res'] = 'This Course Successfully Removed from Archive';
                } else {
                    $data['Ack'] = 0;
                    $data['res'] = 'Failed to remove!!!';
                }
            } else {
                $data['Ack'] = 0;
                $data['res'] = 'No Course exists!!!';
            }

            echo json_encode($data);
        }
    }

}
