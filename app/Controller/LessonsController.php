<?php

App::uses('AppController', 'Controller');

/**
 * Faqs Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class LessonsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Lesson','Faqcategory');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Lesson.id' => 'desc'
        )
    );
    
    public function admin_add($post_id = NULL) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $post_id = base64_decode($post_id);
        if ($this->request->is('post')) {
            $this->request->data['Lesson']['post_id'] = $post_id;
            $this->Lesson->create();
            if ($this->Lesson->save($this->request->data)) {
                $this->loadModel('Post');
                $postStatus = $this->Post->find('first',array('conditions'=>array('Post.id'=>$post_id),'fields'=>array('Post.id','Post.is_approve')));
                $this->Session->setFlash(__('The Lesson has been saved.'));
                return $this->redirect(array('controller'=>'posts','action' => 'view',$postStatus['Post']['id']));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Lesson could not be saved. Please, try again.'));
            }
        }
    }

    public function admin_edit($post_id) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $lesson = $this->Lesson->find('first',array('conditions'=>array('Lesson.id'=>$post_id)));
        
        if (!empty($this->request->data)) {
            
            if ($this->Lesson->save($this->request->data)) {
                $this->Session->setFlash(__('The Lesson has been saved.'));
                return $this->redirect(array('controller'=>'posts','action' => 'view',$lesson['Lesson']['post_id']));
            } else {
                // return $this->redirect(array('controller' => 'posts','action' => 'view',));
                exit;
                $this->Session->setFlash(__('The Lesson could not be saved. Please, try again.'));
            }
        
        }else{
            $this->Lesson->recursive = 0;
            $this->request->data = $this->Lesson->find('first',array('conditions'=>array('Lesson.id'=>$post_id)));
        }
    }

    public function admin_view($id = NULL) {

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Post View';
        if (!$this->Lesson->exists($id)) {
            throw new NotFoundException(__('Invalid Lesson'));
        }
        $options = array('conditions' => array('Lesson.' . $this->Lesson->primaryKey => $id));
        $this->Lesson->recursive = 2;
        $lesson = $this->Lesson->find('first', $options);
        $lesson_id = $lesson['Lesson']['id'];

        $this->Paginator->settings = array(
            'conditions' => array(
                'Lecture.lesson_id' => $lesson_id,
                'Lecture.type'      => 0 
            ),
            'limit' => 10
        );

        $lecture = $this->Paginator->paginate('Lecture');

         $this->Paginator->settings = array(
            'conditions' => array(
                'Lecture.lesson_id' => $lesson_id,
                'Lecture.type'      => 1 
            ),
            'limit' => 10
        );

        $quiz = $this->Paginator->paginate('Lecture');


        $this->set(compact('title_for_layout', 'lesson','lecture','quiz'));

    }

    public function ajaxAddSection($course_id = NULL){
        $this->autoRender = false;
        $res = array();
        $data['Lesson']['post_id']      = $course_id;
        $data['Lesson']['title']        = $this->request->data['section']['name'];
        $data['Lesson']['description']  = $this->request->data['section']['objective'];

        $this->Lesson->create();
        if ($this->Lesson->save($data)) {
            $id = $this->Lesson->id;
            $res['Ack'] = 1;
            //$res['title'] = $this->request->data['section']['name'];
            //$res['id'] = $id;
        } else {
            $res['Ack'] = 0;
        }
		// $options = array('conditions' => array('Lesson.post_id' =>$course_id));
  //       $this->Lesson->recursive = 2;
  //       $lesson = $this->Lesson->find('all', $options);
		// $html = $this->curriculum_html($course_id);
				
		// $res['html'] =$html;
        echo json_encode($res);
        exit;
    }

    public function ajax_delete_section() {
        $this->autoRender = false;
        $data = array();
        $section_id = $this->request->data['section_id'];
        $post_id = $this->request->data['post_id'];

        $this->loadModel('Lecture');

        $lecture_ids = $this->Lecture->find('list', array(
            'fields' => array('Lecture.id'),
            'conditions' => array(
                'Lecture.lesson_id' => $section_id
            )
        ));

        $nextLessonId = $this->Lesson->find('first', array(
            'conditions' => array(
                'Lesson.id !=' => $section_id,
                'Lesson.post_id' => $post_id
            )
        ));

        $updateAll = $this->Lecture->updateAll(
            array('Lecture.lesson_id' => $nextLessonId['Lesson']['id']),
            array('Lecture.id' => $lecture_ids)
        );

        if ($updateAll) {
            if ($this->Lesson->delete($section_id)) {
                $data['Ack'] = 1;
            } else {
                $data['Ack'] = 0;
            }
            
        } else {
            $data['Ack'] = 0;
        }
        echo json_encode($data);
    }

    public function sprint_status_order_new() {
        $this->autoRender = false;
        $postdata = $this->request->data;

        //pr($postdata);

        $this->loadModel('Lecture');
        $lesson_id = str_replace('item-chapter-', '', $postdata[0]);

        foreach ($postdata as $key => $value) {
            $data = array();
            if($key != 0) {
                $lecture_id = str_replace('item-lecture-', '', $value);
                $data['Lecture']['id'] = $lecture_id;
                $data['Lecture']['lesson_id'] = $lesson_id;
                $data['Lecture']['sortby'] = $key - 1;
                $this->Lecture->save($data);
            }
        }

    }

    function sprint_status_order() {
        $data=json_decode($this->request->data['arr_sprints_id']);
        $postdata=$data;
        //pr($postdata);
	   //exit;
	   //$arraydata=explode(',',$postdata);
        $i=0;
        $this->loadModel('Lecture');
        foreach($postdata as $key=>$val1) {
            $existarray=explode('-',$val1);
            $data1=array();
            $data2=array();
            if(count($existarray) == 1) {
                $lesson_id = $existarray['0'];
            }
            if(count($existarray) > 1) {	

                $data1['Lecture']['id']=$existarray['1'];
                $data1['Lecture']['lesson_id']=$lesson_id;
                $data1['Lecture']['sortby']=$i;
                $this->Lecture->save($data1);
            } else {
                $data2['Lesson']['id']=$existarray['0'];
                $data2['Lesson']['sortby']=$i;
                $this->Lesson->save($data2);
            }
            $i++;
        }
        echo "1";
        exit;
    }

    public function curriculum_html($post_id){
        $html = '';
        $this->Lesson->recursive = 2;
        $lessons = $this->Lesson->find('all',array('conditions'=>array('Lesson.post_id'=>$post_id),'order'=>'Lesson.sortby Asc'));
        $serial_no = 1;
        foreach ($lessons as $key => $lesson) { 

            $html.='<li id="'.$lesson['Lesson']['id'].'" data-id="'.$lesson['Lesson']['id'].'" class="ui-state-default"><div id="id" class="ui-sortable-1">
                    <div class="first_section"><span class="section">Section '.$serial_no.': </span><span class="sectiontext" id="lesson_title-'.$lesson['Lesson']['id'].'" ><i class="fa fa-file-text-o" aria-hidden="true"></i>'.$lesson['Lesson']['title'].'<i class="fa fa-pencil edit-lesson" style="cursor: pointer;" > </i><i class="fa fa-trash" style="cursor: pointer;" ></i></span> 
                    <span class="action_buttons pull-right"><i class="fa fa-bars" style="cursor: move;" ></i></span></div><ul id="sortable1" class="connectedSortable">';
                        
                        $lecture_no = 1;
                        foreach ($lesson['Lecture'] as $key => $value) {
                            if($value['type']==0){
                                $html.='<li id="'.$value['id'].'" data-id="'.$lesson['Lesson']['id'].'-'.$value['id'].'" class="ui-state-default"> 
                                <div class="fileupload"><div class="row"><div class="col-md-6"><div class="lecturer"><i class="fa fa-check-circle" aria-hidden="true"></i> Lecture '.$lecture_no.' :</label><span class="introduction" id="lecture_title-'.$value['id'].'" ><i class="fa fa-file-text-o" aria-hidden="true"></i> '.$value['title'].' <i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash delete-lecture" style="cursor: pointer;"></i> </span>
                                </div></div><div class="col-md-6"><div class="upload_part pull-right"><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i>
                                Add Content</button><i class="fa fa-chevron-down pull-right"></i></div></div></div></div></li>';
                            }
                            if($value['type']==1){
                               $html.='<li id="'.$value['id'].'" data-id="'.$lesson['Lesson']['id'].'-'.$value['id'].'" class="ui-state-default"><div class="fileupload">
                                        <div class="row"><div class="col-md-6"><div class="lecturer"><i class="fa fa-check-circle" aria-hidden="true"></i> Quiz '.$lecture_no.' :</label><span class="introduction" id="lecture_title-'.$value['id'].'" ><i class="fa fa-file-text-o" aria-hidden="true"></i> '.$value['title'].'<i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash delete-lecture" style="cursor: pointer;"></i></span>
                                        </div></div>';
                                if(empty($value['Question'])){        
                                    $html.='<div class="col-md-6"><div class="upload_part pull-right"><button type="button" data-toggle="modal" data-target="#questions" class="btn btn-primary add_quiz_question"><i class="fa fa-plus"></i>Add Questions</button></div></div></div></div></li>';  
                                }
                                else{
                                    $html.='<i class="fa fa-chevron-down pull-right"></i>';   
                                }
                            }
                        $lecture_no = $lecture_no+1;
                        } 
                    $html.='</ul></div></li>';
            
            $serial_no = $serial_no+1; 
        }
        return $html;
    }

    public function ajaxLessonDetails(){
        $lesson_id = $this->request->data['lesson_id'];
        $lessonDetails = $this->Lesson->find('first',array('conditions'=>array('Lesson.id'=>$lesson_id),'fields'=>array('Lesson.title','Lesson.description')));
        $data['id'] = $lessonDetails['Lesson']['id'];
        $data['title'] = $lessonDetails['Lesson']['title'];
        $data['description'] = $lessonDetails['Lesson']['description']; 
        echo json_encode($data);
        exit;
    }

    public function ajaxEditLesson($lesson_id = NULL){
        $data = array();
        $this->request->data['Lesson']['id'] = $lesson_id;
        if($this->Lesson->save($this->request->data)){
            $data['Ack'] = 1;
            $data['res'] = ' Section Saved Successfully ';
            $data['title'] = $this->request->data['Lesson']['title'];
        }
        else{
            $data['Ack'] = 0;
            $data['res'] = 'Error!!!';
        }
        echo json_encode($data); 
        exit;
    }
    
}
