<?php
App::uses('AppController', 'Controller');

/**
 * Cart Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class CartsController extends AppController {
    public $components = array('Paginator', 'Session');
    public function index() {
        
        $this->loadModel('SaveLater');
        $this->loadModel('Wishlist');
        $userid = $this->Session->read('userid');
        if (!empty($userid)) {
            $this->Cart->recursive = 3;
            $cartItems = $this->Cart->find('first', array(
                'conditions' => array(
                    'Cart.user_id' => $this->Session->read('userid')
                )
            ));
            
            $this->SaveLater->recursive = 3;
            $saveLater = $this->SaveLater->find('all', array(
                'conditions' => array(
                    'SaveLater.user_id' => $this->Session->read('userid')
                )
            ));
            
            $this->Wishlist->recursive = 3;
            $wishlist = $this->Wishlist->find('all', array(
                'conditions' => array(
                    'Wishlist.user_id' => $this->Session->read('userid')
                )
            ));
        } else {
            $this->loadModel('Post');
            $this->Post->recursive = 3;
            $cartItems['CartItem'] = $this->Post->find('all', array(
                'conditions' => array(
                    "Post.id" => $this->Session->read('Cart')
                ) 
            ));
        }
        
        $this->set(compact('cartItems', 'userid', 'saveLater', 'wishlist'));
    }
    
    public function added($id = NULL) {
        if ($id == NULL) {
            $this->redirect(array(
                'controller' => 'posts',
                'action' => 'course_search'
            ));
        }
        
        $cart = $this->Session->read('Cart');
        $this->loadModel('Post');
        $this->loadModel('PostImage');
        
        $options_post = array('conditions' => array('Post.id' => $id));
        $post = $this->Post->find('first', $options_post);
        $this->set('post', $post);
    }
    
    public function addtocart() {
        $this->autoRender = false ;
        $data = array();
        if ($this->request->is('post')) {
            
            $userid = $this->request->data['userid'];
            $courseid = $this->request->data['courseid'];
            
            $checkCart = $this->Cart->find('first', array(
                'conditions' => array(
                    'Cart.user_id' => $userid
                )
            ));
            
            if (!empty($checkCart)) {
                
                $cart['Cart'] = array(
                    'id' => $checkCart['Cart']['id']
                );
                $cart['CartItem'][] = array(
                    'post_id' => $courseid
                );
                
            } else {
                
                $cart['Cart'] = array(
                    'user_id' => $userid
                );
                $cart['CartItem'][] = array(
                    'post_id' => $courseid
                );
                $this->Cart->create();
                
            }
            
            if ($this->Cart->saveAssociated($cart)) {
                
                
                $data['ack'] = 1;
                $data['url'] = Router::url(array(
                    'controller' => 'carts',
                    'action' => 'added',
                    $courseid
                ), true);
            } else {
                $data['ack'] = 0;
                $data['msg'] = 'Failed to insert in cart';
            }
        }
        echo json_encode($data);
    }
    
    public function addtocart_session() {
        $this->autoRender = false ;
        $data = array();
        if ($this->request->is('post')) {
            $courseid = $this->request->data['courseid'];
            $cart = $this->Session->read('Cart');
            
            if (!empty($cart)) {
                $cart = $cart;
            } else {
                $cart = array();
            }
            
            if (!in_array($courseid, $cart)) {
                $cart[] = $courseid;
                $this->Session->write('Cart', $cart);
                $cart = $this->Session->read('Cart');
                $data['ack'] = 1;
                $data['url'] = Router::url(array(
                    'controller' => 'carts',
                    'action' => 'added',
                    $courseid
                ), true);
            } else {
                $data['ack'] = 0;
                $data['msg'] = "Already exist in cart!!!";
            }
        }
        echo json_encode($data);
    }
    
    public function removefromcart() {
        $this->autoRender = false ;
        $data = array();
        if ($this->request->is('post')) {
            $cartid = $this->request->data['cartid'];
            $postid = $this->request->data['postid'];
            $userid = $this->Session->read('userid');
            $this->loadModel('CartItem');
            $this->loadModel('SaveLater');
            $this->loadModel('Wishlist');
            if ($this->CartItem->deleteAll(array('CartItem.cart_id' => $cartid, 'CartItem.post_id' => $postid), false)) {
                
                $this->CartItem->recursive = 2;
                $cartItems = $this->CartItem->find('all', array(
                    'conditions' => array(
                        'CartItem.cart_id' => $cartid
                    )
                ));

                $this->SaveLater->recursive = 2;
                $saveLater = $this->SaveLater->find('all', array(
                    'conditions' => array(
                        'SaveLater.user_id' => $userid
                    )
                ));

                $this->Wishlist->recursive = 2;
                $wishlist = $this->Wishlist->find('all', array(
                    'conditions' => array(
                        'Wishlist.user_id' => $userid
                    )
                ));

                $price = 0;
                $old_price = 0;
                foreach ($cartItems as $cartItem) {
                    $price += $cartItem['Post']['price'];
                    $old_price += $cartItem['Post']['old_price'];
                }

                $data['ack'] = 1;
                $data['html']['CartItems'] = $cartItems;
                $data['html']['SaveLater'] = $saveLater;
                $data['html']['Wishlist'] = $wishlist;
                $data['html']['cnt'] = count($cartItems);
                $data['html']['price'] = $price;
                $data['html']['old_price'] = $old_price;
                $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price): 0;
            } else {
                $data['ack'] = 0;
                $data['msg'] = "Delete failed!!!";
            }
        }
        
        echo json_encode($data);
    }
    
    public function removefromcart_session() {
        $this->autoRender = false ;
        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $cart = $this->Session->read('Cart');
            
            $key = array_search($postid, $cart);
            if ($key !== FALSE) {
                unset($cart[$key]);
                $this->Session->write('Cart', $cart);
                
                $cart = $this->Session->read('Cart');
                
                $this->loadModel('Post');
                $posts = $this->Post->find('all', array(
                    'conditions' => array('Post.id' => $cart)
                ));
                $price = 0;
                $old_price = 0;
                foreach ($posts as $post) {
                    $price += $post['Post']['price'];
                    $old_price += $post['Post']['old_price'];
                }
                
                $data['cnt'] = count($cart);
                $data['price'] = $price;
                $data['old_price'] = $old_price;
                $data['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price): 0;
                $data['ack'] = 1;
            } else {
                $data['ack'] = 0;
                $data['msg'] = 'Invalid course id!!!';
            }
        }
        
        echo json_encode($data);
    }
    
    public function save_later_to_cart() {
        $this->autoRender = false;
        $data = array();
        
        
        $this->loadModel('CartItem');
        $this->loadModel('SaveLater');
        $this->loadModel('Wishlist');
        
        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];
            $saveLater = array();
            if (!empty($userid)) {
                
                $cart = $this->Cart->find('first', array('conditions' => array(
                    'Cart.user_id' => $userid
                )));
                $cartid = $cart['Cart']['id'];
                
                $conditions = array(
                    'CartItem.post_id' => $postid,
                    'CartItem.cart_id' => $cartid
                );

                if (!$this->CartItem->hasAny($conditions)) {
                    if ($this->SaveLater->deleteAll(array('SaveLater.post_id' => $postid, 'SaveLater.user_id' => $userid), false)) {

                        $cartitem['CartItem'] = array(
                            'cart_id' => $cartid,
                            'post_id' => $postid
                        );

                        if ($this->CartItem->save($cartitem)) {
                            $this->CartItem->recursive = 2;
                            $cartItems = $this->CartItem->find('all', array(
                                'conditions' => array(
                                    'CartItem.cart_id' => $cartid
                                )
                            ));

                            $this->SaveLater->recursive = 2;
                            $saveLater = $this->SaveLater->find('all', array(
                                'conditions' => array(
                                    'SaveLater.user_id' => $userid
                                )
                            ));

                            $this->Wishlist->recursive = 2;
                            $wishlist = $this->Wishlist->find('all', array(
                                'conditions' => array(
                                    'Wishlist.user_id' => $userid
                                )
                            ));

                            $price = 0;
                            $old_price = 0;
                            foreach ($cartItems as $cartItem) {
                                $price += $cartItem['Post']['price'];
                                $old_price += $cartItem['Post']['old_price'];
                            }

                            $data['ack'] = 1;
                            $data['html']['CartItems'] = $cartItems;
                            $data['html']['SaveLater'] = $saveLater;
                            $data['html']['Wishlist'] = $wishlist;
                            $data['html']['cnt'] = count($cartItems);
                            $data['html']['price'] = $price;
                            $data['html']['old_price'] = $old_price;
                            $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price) : 0;
                        } else {
                            $data['ack'] = 0;
                            $data['msg'] = 'DB Error!!!';
                        }
                    } else {
                        $data['ack'] = 0;
                        $data['msg'] = 'DB Error!!!';
                    }
                } else {
                    $data['ack'] = 0;
                    $data['msg'] = 'Already exists!!!';
                }
            }

        }
        
        echo json_encode($data);
    }
    
    public function wishlist_to_cart() {
        $this->autoRender = false;
        $data = array();
        
        
        $this->loadModel('CartItem');
        $this->loadModel('SaveLater');
        $this->loadModel('Wishlist');
        
        if ($this->request->is('post')) {
            $postid = $this->request->data['postid'];
            $userid = $this->request->data['userid'];
            $saveLater = array();
            if (!empty($userid)) {
                $cart = $this->Cart->find('first', array('conditions' => array(
                    'Cart.user_id' => $userid
                )));
                $cartid = $cart['Cart']['id'];
                
                $conditions = array(
                    'CartItem.post_id' => $postid,
                    'CartItem.cart_id' => $cartid
                );

                if (!$this->CartItem->hasAny($conditions)) {
                    if ($this->Wishlist->deleteAll(array('Wishlist.post_id' => $postid, 'Wishlist.user_id' => $userid), false)) {
                        
                        $cartitem['CartItem'] = array(
                            'cart_id' => $cartid,
                            'post_id' => $postid
                        );

                        if ($this->CartItem->save($cartitem)) {
                            $this->CartItem->recursive = 2;
                            $cartItems = $this->CartItem->find('all', array(
                                'conditions' => array(
                                    'CartItem.cart_id' => $cartid
                                )
                            ));

                            $this->SaveLater->recursive = 2;
                            $saveLater = $this->SaveLater->find('all', array(
                                'conditions' => array(
                                    'SaveLater.user_id' => $userid
                                )
                            ));

                            $this->Wishlist->recursive = 2;
                            $wishlist = $this->Wishlist->find('all', array(
                                'conditions' => array(
                                    'Wishlist.user_id' => $userid
                                )
                            ));

                            $price = 0;
                            $old_price = 0;
                            foreach ($cartItems as $cartItem) {
                                $price += $cartItem['Post']['price'];
                                $old_price += $cartItem['Post']['old_price'];
                            }

                            $data['ack'] = 1;
                            $data['html']['CartItems'] = $cartItems;
                            $data['html']['SaveLater'] = $saveLater;
                            $data['html']['Wishlist'] = $wishlist;
                            $data['html']['cnt'] = count($cartItems);
                            $data['html']['price'] = $price;
                            $data['html']['old_price'] = $old_price;
                            $data['html']['percentage'] = ($old_price > 0) ? 100 - ceil(($price * 100) / $old_price): 0;
                        } else {
                            $data['ack'] = 0;
                            $data['msg'] = 'DB Error!!!';
                        }
                    } else {
                        $data['ack'] = 0;
                        $data['msg'] = 'DB Error!!!';
                    }
                } else {
                    $data['ack'] = 0;
                    $data['msg'] = 'Already exists!!!';
                }
            }

        }
        
        echo json_encode($data);
    }
}