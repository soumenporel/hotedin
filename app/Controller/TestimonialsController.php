<?php

App::uses('AppController', 'Controller');

/**
 * Testimonials Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 */
class TestimonialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array('Testimonial','User','Post');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Testimonial.id' => 'desc'
        )
    );
    
    public function admin_add() {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if ($this->request->is('post')) {
            $this->request->data['Testimonial']['date'] = gmdate("Y-m-d h:i:s");
            $this->Testimonial->create();
            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The Testimonial has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Testimonial could not be saved. Please, try again.'));
            }
        }
        $users = $this->User->find('list',array('fields'=>array('User.id','User.first_name')));
        $posts = $this->Post->find('list',array('fields'=>array('Post.id','Post.post_title')));
        $this->set(compact('users','posts'));
    }

    public function admin_edit($id = NULL) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid Testimonial'));
        }
        if ($this->request->is( array('post','put'))) {
            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The Testimonial has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Testimonial could not be saved. Please, try again.'));
            }
        }
        else{
            $this->request->data = $this->Testimonial->find('first',array('conditions'=>array('Testimonial.id'=>$id)));
        }
        $users = $this->Testimonial->User->find('list',array('fields'=>array('User.id','User.first_name')));
        $posts = $this->Testimonial->Post->find('list',array('fields'=>array('Post.id','Post.post_title')));
        $this->set(compact('users','posts'));
    }

    public function admin_view($id = NULL) {

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid Testimonial'));
        }
        $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
        $testimonial = $this->Testimonial->find('first', $options);
                
        $this->set(compact('testimonial'));

    }

    public function admin_index() {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
       
        $this->set('testimonials',$this->Paginator->paginate('Testimonial'));
    }

    public function admin_delete($id = NULL) {
        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }
        $this->Testimonial->id = $id;
        if (!$this->Testimonial->exists()) {
            throw new NotFoundException(__('Invalid Testimonial'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Testimonial->delete()) {
            $this->Session->setFlash(__('The Testimonial has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Testimonial could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    
}
