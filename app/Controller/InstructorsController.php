<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class InstructorsController extends AppController {
    /* function beforeFilter() {
      parent::beforeFilter();
      } */

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Instructors';
    public $components = array('Session', 'RequestHandler', 'Paginator', 'Cookie');
    var $uses = array('User', 'Country', 'State', 'City', 'Newsletter', 'Setting', 'Content', 'Activity', 'Contact', 'Category', 'Post', 'Homeslider', 'Skill', 'Comment', 'Partner', 'Venue', 'Wishlist', 'TempCart', 'Rating', 'InstructorSkill', 'Skill', 'Portfolio', 'Portfoliofile', 'Award', 'Awardfile');
    public $helpers = array('GoogleMap');

    public function index() {

        $userid = $this->Session->read('user_id');
        $results = $this->User->find('all', array('conditions' => array('User.admin_type' => 1)));
        $this->set(compact('results'));
    }

    function followinstrutor() {
        $this->layout = false;
        $this->autoRender = false;
        $userid = $this->Session->read('userid');

        $this->loadModel('FollowInstructor');

        $this->request->data = array();

        if ($this->request->is('post')) {

            $insid = $_REQUEST['id'];


            $options = array('conditions' => array('FollowInstructor.user_id' => $userid, 'FollowInstructor.instructor_id' => $insid));
            $skillexistscheck = $this->FollowInstructor->find('first', $options);

            $date = gmdate("Y-m-d");


            $this->request->data['FollowInstructor']['user_id'] = $userid;
            $this->request->data['FollowInstructor']['instructor_id'] = $insid;
            $this->request->data['FollowInstructor']['added_date'] = $date;

            if (!$skillexistscheck) {
                $this->FollowInstructor->create();

                if ($this->FollowInstructor->save($this->request->data)) {

                    $last_id = $this->FollowInstructor->getInsertID();

                    echo $insid;
                }
            } else {
                echo "0";
            }
        }
        //echo json_encode($ret);
        exit;
    }

    function unfollowinstrutor($id = null) {
        $this->loadModel('FollowInstructor');
        $this->layout = false;
        $this->autoRender = false;
        $insid = $_REQUEST['id'];
        $userid = $this->Session->read('userid');
        $ret = array();
        if ($this->FollowInstructor->deleteAll(['FollowInstructor.user_id' => $userid, 'FollowInstructor.instructor_id' => $insid])) {
            echo $insid;
        } else {
            echo "0";
        }
        //echo json_encode($ret);
        exit;
    }

    function favinstrutor() {
        $this->layout = false;
        $this->autoRender = false;
        $userid = $this->Session->read('userid');

        $this->loadModel('FavouriteInstructor');

        $this->request->data = array();

        if ($this->request->is('post')) {

            $insid = $_REQUEST['id'];


            $options = array('conditions' => array('FavouriteInstructor.user_id' => $userid, 'FavouriteInstructor.instructor_id' => $insid));
            $skillexistscheck = $this->FavouriteInstructor->find('first', $options);

            $date = gmdate("Y-m-d");


            $this->request->data['FavouriteInstructor']['user_id'] = $userid;
            $this->request->data['FavouriteInstructor']['instructor_id'] = $insid;
            $this->request->data['FavouriteInstructor']['added_date'] = $date;

            if (!$skillexistscheck) {
                $this->FavouriteInstructor->create();

                if ($this->FavouriteInstructor->save($this->request->data)) {

                    $last_id = $this->FavouriteInstructor->getInsertID();

                    echo $insid;
                }
            } else {
                echo "0";
            }
        }
        //echo json_encode($ret);
        exit;
    }

    function unfavinstrutor($id = null) {
        $this->loadModel('FavouriteInstructor');
        $this->layout = false;
        $this->autoRender = false;
        $insid = $_REQUEST['id'];
        $userid = $this->Session->read('userid');
        $ret = array();
        if ($this->FavouriteInstructor->deleteAll(['FavouriteInstructor.user_id' => $userid, 'FavouriteInstructor.instructor_id' => $insid])) {
            echo $insid;
        } else {
            echo "0";
        }
        //echo json_encode($ret);
        exit;
    }

}
