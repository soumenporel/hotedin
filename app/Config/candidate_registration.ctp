<section class="candidate-registration-section py-5">
	  	<div class="container">
	  		<h1 class="candidate-registration-section-heading">Candidate Registration</h1>
	  		<p class="candidate-para px-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>

	  	</div>
	  </section>

	  <section class="candidate-registration-form py-5">
	  	<div class="container">
	  		<div class="row">
	  			

	  			<div class="col-sm-12 col-md-7">
	  				<form class="row" action="candidateRegistration" method="post" id="UserSignupForm" enctype ="multipart/form-data">
	  				<div class="employer-box">
	  					<h3>User Account</h3>
	  					  
	  					  	<div class="row">
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Username<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="username" name="data[User][username]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Password<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="password"  id="password" name="data[User][user_pass]">
	  					  			</div>		
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Confirm Password<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				 <input id="confpass" type="password" class="form-control" name="conf_pass" required>
	  					  			</div>		
	  					  	</div>
	    				
	  				</div>
	  					<div class="clearfix"></div>
	  				<div class="employer-box">
	  					<h3>Account information</h3>
	  					  
	  					  	<div class="row">
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">First Name<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="firstName" name="data[User][first_name]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Last Name<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="lastName" name="data[User][last_name]">
	  					  			</div>	
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Email Address<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="email"  id="emailAddress" name="data[User][email_address]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Phone Number</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="phone" name="data[User][Phone_number]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Website</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="website" name="data[User][website_link]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">linkedin Link</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="linkedIn" name="data[User][linkedin_link]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Your Photo</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<div class="candidate-file-panel">
	  					  					<span>Drop files here</span>
	  					  					<div class="upload_file_container">
    											<label class="blue-btn">Select file!</label>
    										<input type="file" name="data[User][user_image]" />
    									<!-- <input type="file" name="photo" /> -->
											</div>
	  					  				</div>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Upload Video CV</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<div class="candidate-file-panel">
	  					  					<span>Drop files here</span>
	  					  					<div class="upload_file_container">
    											<label class="blue-btn">Select file!</label>
    										<input type="file" name="data[User][video]" />
											</div>
	  					  				</div>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Document</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<div class="candidate-file-panel">
	  					  					<span>Drop files here</span>
	  					  					<div class="upload_file_container">
    											<label class="blue-btn">Select file!</label>
    										<input type="file" name="data[User][document]" />
											</div>
	  					  				</div>
	  					  			</div>

	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Video Resume</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text" name="data[User][cv]">
	  					  			</div>
	  					  	</div>
	    				  
	  				</div>
	  				<div class="clearfix"></div>
	  				<div class="employer-box">
	  					<h3>Address</h3>
	  					  
	  					  	<div class="row">
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Address<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="address" name="data[User][address]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">City<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][city]">
	  					  			</div>	
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">State</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  id="state" name="data[User][state]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Country<span>*</span></label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				   <select class="" name="data[User][country]">
                                      <?php foreach ($countries as $country) { ?>
                                          <option value="<?php echo $country['Country']['name'] ?>"><?php echo $country['Country']['name'] ?></option>
                                      <?php } ?>
                                  </select>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Zip-code/Postal code</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][post_code]">
	  					  			</div>
	  		
	  					  	</div>
	  					</div>

	  					<div class="clearfix"></div>
	  					<div class="employer-box">
	  					<h3>Resume</h3>
	  					  
	  					  	<div class="row">
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Category</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][category]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Professional Headline</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][professionalHeadline]">
	  					  			</div>	
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">State</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][state2]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Key Skills</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				 <select class="" name="data[User][keySkills]">
                                      <?php foreach ($skills as $skill) { ?>
                                          <option value="<?php echo $skill['Skill']['skill_name'] ?>"><?php echo $skill['Skill']['skill_name'] ?></option>
                                      <?php } ?>
                                  </select>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Profile Summary</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<textarea class="candidate-textarea" name="data[User][profileSummary]"></textarea>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Job Sought</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][jobSought]">
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Experience</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<input type="text"  name="data[User][totalExperience]">
	  					  			</div>
	  					  			<!--add form-->
                           <div class="show-form">
                              <h3 class="pl-3">Experience   (Add Experience)</h3>
                              <div class="row">
                                 <div class="col-sm-12 col-md-4">
                                    <label for="email">Started*</label>
                                 </div>
                                 <div class="col-sm-12 col-md-8">
                                    <input type="text"  name="data[User][started]">
                                 </div>
                                 <div class="col-sm-12 col-md-4">
                                    <label for="email">Finished*</label>
                                 </div>
                                 <div class="col-sm-12 col-md-8">
                                    <input type="text"  name="data[User][finished]">
                                 </div>
                                 <div class="col-sm-12 col-md-4">
                                    <label for="email">Company Name</label>
                                 </div>
                                 <div class="col-sm-12 col-md-8">
                                    <input type="text"  name="data[User][companyName]">
                                 </div>
                                 <div class="col-sm-12 col-md-4">
                                    <label for="email">Position Title *</label>
                                 </div>
                                 <div class="col-sm-12 col-md-8">
                                    <input type="text"  name="data[User][positionTitle]">
                                 </div>
                                 <div class="col-sm-12 col-md-4">
                                    <label for="email">Description</label>
                                 </div>
                                 <div class="col-sm-12 col-md-8">
                                    <textarea class="employer-textarea-hide" name="data[User][description]"></textarea>
                                 </div>
                              </div>
                           </div>
                           <!--end ad form-->
                           <div class="col-sm-12 col-md-4">
                              <label for="email">Experience</label>
                           </div>
                           <div class="col-sm-12 col-md-8">
                              <button type="button" id="hide" class="btn btn-default add-btn"> <i class="icon ion-android-add-circle"></i> Add Experience</button>
                           </div>
	  					  			<!--<div class="col-sm-12 col-md-4">
	  					  				<label for="email">Education</label>
	  					  			</div>
	  					  			<div class="col-sm-12 col-md-8">
	  					  				<button type="file" class="btn btn-default add-btn"><i class="icon ion-android-add-circle"></i> Add Education</button>
	  					  			</div> -->
	  		
	  					  	</div>
	  					</div>
	  					<input type="submit" class="candidate-register" placeholder="Register" name="">



	  					</form>
	  				</div>

	  			<div class="col-sm-12 col-md-5">
	  				<div class="youtube-box">
	  					<h2 class="youtube-heding mb-4">YouTube Account</h2>
	  					<p class="px-3">You will need a <strong>YouTube account</strong> to save your video <strong>Click record </strong>(red circle) to start. Click stop (square) to end. Options to share

						or delete with appear. Click the share icon (left) and add your title and description, select 'Unlisted' from the options so
						it's not searched on YouTube. <strong>Click Upload</strong> Once uploaded, click 'COPY LINK' Paste into 
						<strong>'Video Resume' box below</strong></p>
						<h2  class="youtube-heding mb-4 border-0 py-0">Record your video here</h2>
						<div class="recorder-box py-3">
							<h4 class="text-center text-white pt-5">Webcam Video Recorder</h4>
							<h5 style="color: #d6d6d6" class="text-center">Please grant access to your Camera</h5>
							<p style="color: #d6d6d6" class="px-3 text-gra text-center">
								Cam Recorder is a free online tool that allows you to record videos with your webcam. All you need is a web camera and an up-to-date web browser.After recording, you can directly upload your video to Youtube or save it to disk.
							</p>
							<a href="#"><div class="candidate-video-btn"></div></a>
						</div>
	  				</div>
	  			</div>

	  				
	  				

	  				
	    		 
	  			</div>
	  		</div>
	  </section>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/custom-file-input.js"></script>
    <script>
    	$(window).scroll(function(){
		  var sticky = $('.sticky'),
		      scroll = $(window).scrollTop();

		  if (scroll >= 50) sticky.addClass('fixed');
		  else sticky.removeClass('fixed');
		});
    </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                dots:true,
                nav: false,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          </script>
<script>
    $(document).ready(function () {
      $("#UserSignupForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	'data[User][username]': {
                validators: {
                    notEmpty: {
                        message: 'Username is required and cannot be empty'
                    }
                }
            },
            'data[User][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty.'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address. '
                    }
                }

            },
          'data[User][user_pass]': {
            validators: {
              notEmpty: {
                message: 'The password is required and cannot be empty.'
                },
                identical: {
                    field: 'conf_pass',
                    message: 'The password and its confirm password are not the same'
                }
            }
        },
          'conf_pass': {
            validators: {
              notEmpty: {
                message: 'The password is required and cannot be empty.'
                },
                identical: {
                    field: 'data[User][user_pass]',
                    message: 'The password and its confirm password are not the same'
                }
            }
          },
          'data[User][first_name]': {
                validators: {
                    notEmpty: {
                        message: 'First Name is required and cannot be empty'
                    }
                }
            },
            'data[User][last_name]': {
                validators: {
                    notEmpty: {
                        message: 'Last Name is required and cannot be empty'
                    }
                }
            },
            'data[User][address]': {
                validators: {
                    notEmpty: {
                        message: 'Address is required and cannot be empty'
                    }
                }
            },
            'data[User][city]': {
                validators: {
                    notEmpty: {
                        message: 'City is required and cannot be empty'
                    }
                }
            },
            'data[User][started]': {
                validators: {
                    notEmpty: {
                        message: 'Started is required and cannot be empty'
                    }
                }
            },
            'data[User][finished]': {
                validators: {
                    notEmpty: {
                        message: 'Finished is required and cannot be empty'
                    }
                }
            },
            'data[User][positionTitle]': {
                validators: {
                    notEmpty: {
                        message: 'Position title is required and cannot be empty'
                    }
                }
            }
        }
      });
    });
</script>
<script>
	   $("#hide").click(function(){
         $(".show-form").slideToggle();
         });
</script>