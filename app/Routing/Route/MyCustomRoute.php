<?php
App::uses('WpPage', 'Model');
App::uses('CakeRoute', 'Routing/Route');
App::uses('ClassRegistry', 'Utility');

class MyCustomRoute extends CakeRoute {

	public function parse($url) {
		$params = parent::parse($url);
		if (empty($params)) {
			return false;
		}

		$this->WpPage = ClassRegistry::init('WpPage');
		$slug = $params['slug'];
		
		$WpPage = $this->WpPage->find('first', array(
			'conditions' => array(
				'WpPage.slug' => $slug
			)
		));
		if ($WpPage) {
			$params['pass'] = array($slug);
			return $params;
		}
		return false;
	}
}