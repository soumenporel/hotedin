<style>
    #content {
        overflow: inherit;
    }
</style>

<!--page heading start-->

<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> My Dashboard </li>
    </ul>
</div>
<!-- page heading end-->

<div class="wrapper">

</div>
<?php //pr(date('Y-m-d'));?>
<!--body wrapper start-->
<div class="wrapper">

    <!-- <div class="row">
        <div class="col-md-9">
            <form action="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'date_range')); ?>" class="form-inline" id="FilterAdminIndexForm" method="post" accept-charset="utf-8">
                <div style="display:none;">
                    <input type="hidden" name="_method" value="POST">
                </div>
                <div class="form-group">
                    <input name="data[Filter][start_date]" class="form-control dateTimePicker " placeholder="Start Date" value="<?php if(isset($start_date) && !empty($start_date)){ echo $start_date; }?>" type="text" id="FilterStartDate">
                </div>
                <div class="form-group">
                    <input name="data[Filter][end_date]" class="form-control dateTimePicker " placeholder="End Date" value="<?php if(isset($end_date) && !empty($end_date)){ echo $end_date; }?>" type="text" id="FilterEndDate">
                </div>
                <input class="btn btn-primary" type="submit" value="Search">
                <div class="submit actions">
                    <a href="<?php echo $this->webroot;?>admin/dashboards/index">Reset</a>
                </div>
            </form>
        </div>   
        <div class="col-md-3">
            <div class="btn-group select pull-right" style="width: 15.2%;">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" style="width: 259%;margin-left: -59px;">
                    <span style="width: 70px;" class="dropdown-label"><?php
                        if ($filter != '') {
                            echo ucfirst($filter);
                        } else {
                            echo 'Filter';
                        }
                        ?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left text-sm">
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index')); ?>">Today</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'yesterday')); ?>">Yesterday</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'weekly')); ?>">Last 7 Days</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'monthly')); ?>">Last 30 Days</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'yearly')); ?>">Yearly</a>
                    </li>
                    
                     <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'last_month')); ?>">Last Month</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'this_month')); ?>">This Month</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'all_time')); ?>">All Time</a>
                    </li>
                </ul>
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-md-6">
            <!--statistics start-->
            <div class="row state-overview">
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <div class="panel purple">
                        <div class="symbol">
                            <i class="fa fa-tag"></i>
                        </div>
                        <div class="state-value">
                            <div class="value"><?php echo $total_categories; ?></div>
                            <div class="title">Total Categories</div>
                        </div>
                    </div>
                </div>
<!--                <div class="col-md-6 col-xs-12 col-sm-6">
                    <div class="panel red">
                        <div class="symbol">
                            <i class="fa fa-tags"></i>
                        </div>
                        <div class="state-value">
                            <div class="value"><?php echo $total_subCategories; ?></div>
                            <div class="title">Total Subcategories</div>
                        </div>
                    </div>
                </div>-->
            </div>
            <div class="row state-overview">
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <div class="panel blue">
                        <div class="symbol">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <div class="state-value">
                            <div class="value"><?php echo $total_course; ?></div>
                            <div class="title"> Total Jobs</div>
                        </div>
                    </div>
                </div>
<!--                <div class="col-md-6 col-xs-12 col-sm-6">
                    <div class="panel green">
                        <div class="symbol">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <div class="state-value">
                            <div class="value"><?php echo $live_course; ?></div>
                            <div class="title"> Live Jobs</div>
                        </div>
                    </div>
                </div>-->
            </div>
            <!--statistics end-->
        </div>

        <div class="col-md-6">
            <div class="panel deep-purple-box">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div id="graph-donut" class="revenue-graph"></div>

                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <ul class="bar-legend">
                                <li><span class="blue"></span> Open rate</li>
                                <li><span class="green"></span> Click rate</li>
                                <li><span class="purple"></span> Share rate</li>
                                <li><span class="red"></span> Unsubscribed rate</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!--statistics start-->
            <div class="row state-overview">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="panel purple">
                        <div class="symbol">
                            <i class="fa fa-eye"></i>
                        </div>
                        <div class="state-value">
                            <div class="value"><?php echo $vCount; ?></div>
                            <div class="title">Total Number of Views</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--statistics end-->
        </div>    
    </div>    
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row revenue-states">
                        <div class="col-md-9">
                            <!-- <h4>Monthly revenue report</h4>
                            <div class="icheck">
                                <div class="square-red single-row">
                                    <div class="checkbox ">
                                        <input type="checkbox" checked>
                                        <label>Online</label>
                                    </div>
                                </div>
                                <div class="square-blue single-row">
                                    <div class="checkbox ">
                                        <input type="checkbox">
                                        <label>Offline </label>
                                    </div>
                                </div>
                            </div> -->
                            <form action="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'date_range')); ?>" class="form-inline" id="Filter" method="post" accept-charset="utf-8">
                                <div style="display:none;">
                                    <input type="hidden" name="_method" value="POST">
                                </div>
                                <div class="form-group">
                                    <input name="data[Filter][start_date]" class="form-control input-sm dateTimePicker " placeholder="Start Date" value="<?php if(isset($start_date) && !empty($start_date)){ echo $start_date; }?>" type="text" id="FilterStartDate" required>
                                </div>
                                <div class="form-group">
                                    <input name="data[Filter][end_date]" class="form-control input-sm dateTimePicker " placeholder="End Date" value="<?php if(isset($end_date) && !empty($end_date)){ echo $end_date; }?>" type="text" id="FilterEndDate" required>
                                </div>
                                <input class="btn btn-primary" type="submit" value="Search">
                               <!--  <div class="submit actions">
                                    <a href="<?php echo $this->webroot;?>admin/dashboards/index">Reset</a>
                                </div> -->
                            </form>
                        </div>
                        <div class="col-md-3">
                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle pull-right" >
                                <span style="width: 70px;" class="dropdown-label"><?php
                                    if ($filter != '') {
                                        echo ucfirst($filter);
                                    } else {
                                        echo 'Filter';
                                    }
                                    ?></span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu text-left text-sm pull-right">
                                <li class="graph_filter" data-term='' >
                                    <a href="javascript:void(0)">Today</a>
                                </li>
                                <li class="graph_filter" data-term='yesterday' >
                                    <a href="javascript:void(0)">Yesterday</a>
                                </li>
                                <li class="graph_filter" data-term='weekly' >
                                    <a href="javascript:void(0)">Last 7 Days</a>
                                </li>
                                <li class="graph_filter" data-term='monthly' >
                                    <a href="javascript:void(0)">Last 30 Days</a>
                                </li>
                                <li class="graph_filter" data-term='yearly' >
                                    <a href="javascript:void(0)">Yearly</a>
                                </li>
                                
                                 <li class="graph_filter" data-term='last_month' >
                                    <a href="javascript:void(0)">Last Month</a>
                                </li>
                                <li class="graph_filter" data-term='this_month' >
                                    <a href="javascript:void(0)">This Month</a>
                                </li>
                                <li class="graph_filter" data-term='all_time' >
                                    <a href="javascript:void(0)">All Time</a>
                                </li>
                            </ul>
                            <!-- <ul class="revenue-nav">
                                <li><a href="#">weekly</a></li>
                                <li><a href="#">monthly</a></li>
                                <li class="active"><a href="#">yearly</a></li>
                            </ul> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="clearfix">
                                <div id="main-chart-legend" class="pull-right">
                                </div>
                            </div>

                            <div id="main-chart">
                                <div id="main-chart-container" class="main-chart">
                                </div>
                            </div>
                            <!-- <ul class="revenue-short-info">
                                <li>
                                    <h1 class="red">15%</h1>
                                    <p>Server Load</p>
                                </li>
                                <li>
                                    <h1 class="purple">30%</h1>
                                    <p>Disk Space</p>
                                </li>
                                <li>
                                    <h1 class="green">84%</h1>
                                    <p>Transferred</p>
                                </li>
                                <li>
                                    <h1 class="blue">28%</h1>
                                    <p>Temperature</p>
                                </li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


    <div class="row">
        
        

        <div class="col-md-8">
            <div class="panel green-box" style="height:155px;" >
                <div class="panel-body extra-pad">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $draftPars;?>">
                                    <span class="percent"><?php echo $draftPars;?>% <span class="sm">Draft Jobs</span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $approvedPars;?>">
                                    <span class="percent"><?php echo $approvedPars;?>% <span class="sm">Approved Jobs</span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $inreviewPars;?>">
                                    <span class="percent"><?php echo $inreviewPars;?>% <span class="sm">In-review Jobs</span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $disapprovedPars;?>">
                                    <span class="percent"><?php echo $disapprovedPars;?>% <span class="sm">Disapproved Jobs</span></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        
        <div class="col-md-12">
            
            <!--statistics start-->
            <div class="panel">
                <header class="panel-heading">
                    Latest Sign up
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $todays_instructor_signup; ?></div>
                                    <div class="title"><?php echo $filter ? ucfirst($filter) : 'Todays' ?> Employer Sign Up</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $todays_user_signup; ?></div>
                                    <div class="title"><?php echo $filter ? ucfirst($filter) : 'Todays' ?> Jobseeker Sign Up</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!--            <div class="panel">
                <header class="panel-heading">
                    <?php echo $filter ? $filter : 'today'; ?> Sales
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <?php
                    //pr($sales);
                    ?>
                    <div style="overflow-x: auto;">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="numeric">Order ID</th>
                                    <th class="numeric">Name</th>
                                    <th class="numeric">Transaction ID</th>
                                    <th class="numeric">Quantity</th>
                                    <th class="numeric">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($sales as $sale){
                                ?>
                                <tr>
                                    <td><?php echo $sale['Order']['id']; ?></td>
                                    <td><?php echo $sale['User']['first_name'] . ' ' . $sale['User']['last_name']; ?></td>
                                    <td class="numeric"><?php echo $sale['Order']['transaction_id']; ?></td>
                                    <td class="numeric"><?php echo $sale['Order']['quantity']; ?></td>
                                    <th><?php echo $sale['Order']['payment_date']; ?></th>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <div class="paging">
                            <?php   //$paginator->options(array('url' => $this->passedArgs));
                                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));

                                    echo $this->Paginator->numbers(array('separator' => ''));
                                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </div>
                    </div>
                </div>
            </div>-->

            <div class="panel">
                <header class="panel-heading">
                    Jobs
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $total_course; ?></div>
                                    <div class="title"><?php echo $filter ? ucfirst($filter) : 'Todays' ?> Jobs</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $live_course; ?></div>
                                    <div class="title">Live Jobs</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-xs-12 col-sm-4">
                    <div class="panel">
                        <header class="panel-heading">
                            Recent Activity
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <a class="fa fa-times" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <ul class="to-do-list ui-sortable" id="sortable-todo">
                                <?php
                                
                                foreach($recentActivities as $recentActivity){
                                ?>
                                <li class="clearfix">
                                    <p class="todo-title">
                                        <?php
                                        if($recentActivity['RecentActivity']['activity'] == '2') {
                                            
                                            echo 'Approved Jobs ' . $recentActivity['Post']['post_title'];
                                        } else if($recentActivity['RecentActivity']['activity'] == '1') {
                                            echo 'Posted Jobs ' . $recentActivity['Post']['post_title'];
                                        }
                                        ?>
                                    </p>
                                    <div class="todo-actionlist pull-right clearfix">
                                        <a href="#" class="todo-remove"><i class="fa fa-times"></i></a>
                                    </div>
                                </li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

<!--                <div class="col-md-8 col-xs-12 col-sm-8">
                    <div class="panel">
                        <header class="panel-heading">
                            Latest Order
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div style="overflow-x: auto;">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                        <tr>
                                            <th class="numeric">Order ID</th>
                                            <th class="numeric">Name</th>
                                            <th class="numeric">Transaction ID</th>
                                            <th class="numeric">Quantity</th>
                                            <th class="numeric">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($latest_orders as $latest_order){
                                        ?>
                                        <tr>
                                            <td><?php echo $latest_order['Order']['id']; ?></td>
                                            <td><?php echo $latest_order['User']['first_name'] . ' ' . $latest_order['User']['last_name']; ?></td>
                                            <td class="numeric"><?php echo $latest_order['Order']['transaction_id']; ?></td>
                                            <td class="numeric"><?php echo $latest_order['Order']['quantity']; ?></td>
                                            <th><?php echo $latest_order['Order']['payment_date']; ?></th>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>


            <!--statistics end-->
        </div>
            
    </div>
</div>

<link href="<?php echo $this->webroot; ?>adminFiles/css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script>
$(function(){

    Morris.Donut({
    element: 'graph-donut',
    data: [
        {value: 40, label: 'New Visit', formatted: 'at least 70%' },
        {value: 30, label: 'Unique Visits', formatted: 'approx. 15%' },
        {value: 20, label: 'Bounce Rate', formatted: 'approx. 10%' },
        {value: 10, label: 'Up Time', formatted: 'at most 99.99%' }
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (x, data) { return data.formatted; }
    });

    $('.dateTimePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    var data = [];

    //var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        // xaxis: {
        //     tickSize: 1,
        //     tickFormatter: function(v, a) {return months[v]
        //     }
        // },
        // Tooltip
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        //lines: {
        //     show: true,
        //     fill: true
        //
        //},
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };

    var data = <?php echo json_encode($data); ?>;
    var plot = $.plot($("#main-chart #main-chart-container"), data, options);

    $('.graph_filter').click(function(){
    var filter_type = $(this).data('term');
    $('#FilterStartDate').val('');
    $('#FilterEndDate').val('');
    console.log(filter_type);

        $.ajax({
            url: "<?php echo $this->webroot;?>dashboards/ajaxGraph/"+filter_type,
            dataType:'json',
            type: 'POST',     
            success: function(result){
                console.log(result);
                $.plot($("#main-chart #main-chart-container"), result, options);
            }
        });

    });

    $('#Filter').submit(function(){
        var form=$("#Filter");

        $.ajax({
            url: "<?php echo $this->webroot;?>dashboards/ajaxGraph/",
            data:form.serialize(),
            dataType:'json',
            type: 'POST',     
            success: function(result){
                console.log(result);
                $.plot($("#main-chart #main-chart-container"), result, options);
            }
        });
        return false;
    });
});

</script>
