<style>
    #content {
        overflow: inherit;
    }
</style>

<!--page heading start-->

<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> My Dashboard </li>
    </ul>
</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        
        <div class="col-md-9">
            <form action="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'date_range')); ?>" class="form-inline" id="FilterAdminIndexForm" method="post" accept-charset="utf-8">
                <div style="display:none;">
                    <input type="hidden" name="_method" value="POST">
                </div>
                <div class="form-group">
                    <input name="data[Filter][start_date]" class="form-control dateTimePicker " placeholder="Start Date" value="<?php if(isset($start_date) && !empty($start_date)){ echo $start_date; }?>" type="text" id="FilterStartDate">
                </div>
                <div class="form-group">
                    <input name="data[Filter][end_date]" class="form-control dateTimePicker " placeholder="End Date" value="<?php if(isset($end_date) && !empty($end_date)){ echo $end_date; }?>" type="text" id="FilterEndDate">
                </div>
                <input class="btn btn-primary" type="submit" value="Search">
                <div class="submit actions">
                    <a href="<?php echo $this->webroot;?>admin/dashboards/index">Reset</a>
                </div>
            </form>
        </div>   
        <div class="col-md-3">
            <div class="btn-group select pull-right" style="width: 15.2%;">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" style="width: 259%;margin-left: -59px;">
                    <span style="width: 70px;" class="dropdown-label"><?php
                        if ($filter != '') {
                            echo ucfirst($filter);
                        } else {
                            echo 'Filter';
                        }
                        ?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left text-sm">
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index')); ?>">Today</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'yesterday')); ?>">Yesterday</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'weekly')); ?>">Last 7 Days</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'monthly')); ?>">Last 30 Days</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'yearly')); ?>">Yearly</a>
                    </li>
                    
                     <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'last_month')); ?>">Last Month</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'this_month')); ?>">This Month</a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'dashboards', 'action' => 'index', 'all_time')); ?>">All Time</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel green-box">
                <div class="panel-body extra-pad">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $draftPars;?>">
                                    <span class="percent"><?php echo $draftPars;?>% <span class="sm">Draft Course</span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $approvedPars;?>">
                                    <span class="percent"><?php echo $approvedPars;?>% <span class="sm">Approved Course</span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $inreviewPars;?>">
                                    <span class="percent"><?php echo $inreviewPars;?>% <span class="sm">In-review Course</span></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="knob">
                                <span class="chart" data-percent="<?php echo $disapprovedPars;?>">
                                    <span class="percent"><?php echo $disapprovedPars;?>% <span class="sm">Disapproved Course</span></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        
        <div class="col-md-12">
            
            <!--statistics start-->
            <div class="panel">
                <header class="panel-heading">
                    Latest Sign up
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $todays_instructor_signup; ?></div>
                                    <div class="title"><?php echo $filter ? ucfirst($filter) : 'Todays' ?> Instructor's Sign Up</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $todays_user_signup; ?></div>
                                    <div class="title"><?php echo $filter ? ucfirst($filter) : 'Todays' ?> Learner's Sign Up</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <header class="panel-heading">
                    <?php echo $filter ? $filter : 'today'; ?> Sales
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <?php
                    //pr($sales);
                    ?>
                    <div style="overflow-x: auto;">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="numeric">Order ID</th>
                                    <th class="numeric">Name</th>
                                    <th class="numeric">Transaction ID</th>
                                    <th class="numeric">Quantity</th>
                                    <th class="numeric">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($sales as $sale){
                                ?>
                                <tr>
                                    <td><?php echo $sale['Order']['id']; ?></td>
                                    <td><?php echo $sale['User']['first_name'] . ' ' . $sale['User']['last_name']; ?></td>
                                    <td class="numeric"><?php echo $sale['Order']['transaction_id']; ?></td>
                                    <td class="numeric"><?php echo $sale['Order']['quantity']; ?></td>
                                    <th><?php echo $sale['Order']['payment_date']; ?></th>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <div class="paging">
                            <?php   //$paginator->options(array('url' => $this->passedArgs));
                                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));

                                    echo $this->Paginator->numbers(array('separator' => ''));
                                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <header class="panel-heading">
                    Courses
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $total_course; ?></div>
                                    <div class="title"><?php echo $filter ? ucfirst($filter) : 'Todays' ?> Course</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $live_course; ?></div>
                                    <div class="title">Live Course</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-xs-12 col-sm-4">
                    <div class="panel">
                        <header class="panel-heading">
                            Recent Activity
                            <span class="tools pull-right">
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                                <a class="fa fa-times" href="javascript:;"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <ul class="to-do-list ui-sortable" id="sortable-todo">
                                <?php
                                
                                foreach($recentActivities as $recentActivity){
                                ?>
                                <li class="clearfix">
                                    <p class="todo-title">
                                        <?php
                                        if($recentActivity['RecentActivity']['activity'] == '2') {
                                            
                                            echo 'Approved Course ' . $recentActivity['Post']['post_title'];
                                        } else if($recentActivity['RecentActivity']['activity'] == '1') {
                                            echo 'Posted Course ' . $recentActivity['Post']['post_title'];
                                        }
                                        ?>
                                    </p>
                                    <div class="todo-actionlist pull-right clearfix">
                                        <a href="#" class="todo-remove"><i class="fa fa-times"></i></a>
                                    </div>
                                </li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 col-xs-12 col-sm-8">
                    <div class="panel">
                        <header class="panel-heading">
                            Latest Order
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <div style="overflow-x: auto;">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                        <tr>
                                            <th class="numeric">Order ID</th>
                                            <th class="numeric">Name</th>
                                            <th class="numeric">Transaction ID</th>
                                            <th class="numeric">Quantity</th>
                                            <th class="numeric">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($latest_orders as $latest_order){
                                        ?>
                                        <tr>
                                            <td><?php echo $latest_order['Order']['id']; ?></td>
                                            <td><?php echo $latest_order['User']['first_name'] . ' ' . $latest_order['User']['last_name']; ?></td>
                                            <td class="numeric"><?php echo $latest_order['Order']['transaction_id']; ?></td>
                                            <td class="numeric"><?php echo $latest_order['Order']['quantity']; ?></td>
                                            <th><?php echo $latest_order['Order']['payment_date']; ?></th>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--statistics end-->
        </div>
            
    </div>
</div>

<link href="<?php echo $this->webroot; ?>adminFiles/css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script>
$(function(){
    $('.dateTimePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    });
});
</script>
