<?php
//pr($Shoppings);?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<div class="contents index">
    <h2 style="width:400px;float:left;"><?php echo __('Shopping Cart'); ?></h2>
    
    
    
<!--<form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">   
            <table style=" border:none;">
                <tr>
                    <td>Keyword</td>
                    <td><input type="text" name="keyword" value="<?php //echo isset($keywords)?$keywords:'';?>" placeholder="Search by Keyword."></td>
                    <td>Activity Status</td>
                    <td><select name="search_is_active" id="search_is_active">
                            <option value="" >Select Option</option>
                            <option value="1" <?php //echo (isset($is_active) && $is_active=='1')?'selected':'';?>>Active</option>
                            <option value="0" <?php //echo (isset($is_active) && $is_active=='0')?'selected':'';?>>Inactive</option>
                        </select></td>
                    <td><input type="submit" name="search" value="Search"></td>
                </tr> 
                      
            </table>
        </form>-->

    <table cellpadding="0" cellspacing="0"  id="sortable">
        <thead>
            <tr>
                <th>
                    <?php echo __('id'); ?>
                </th>
                <th>
                    <?php echo __('first_name'); ?>
                </th>
                <th>
                    <?php echo __('last_name'); ?>
                </th>
				<th>
                    <?php echo __('quantity'); ?>
                </th>
                <th>
                    <?php echo __('Total Price'); ?>
                </th>
                <th>
                    <?php echo __('Actions'); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($Shoppings as $key => $Shopping) :
			
			
			$price = 0;
			foreach($Shopping['CartItem'] as $crt) {
				$price += $crt['Post']['price'];
			}
			if(count($Shopping>0))
			{
			
            ?>
            <tr data-id="<?php echo $Shopping['Cart']['id']; ?>">
				<td>
                    <?php echo ++$key; ?>&nbsp;
                </td>
				<td>
                    <?php echo h($Shopping['User']['first_name']);?>
                </td>
                <td>
                    <?php echo h($Shopping['User']['last_name']);?>
                </td>
                
				<td>
                    <?php echo count($Shopping['CartItem']);?>
                </td>
				<td>
                    <?php echo '$'.$price; ?>
                </td>
                <td>
					<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')),
                        array('action' => 'view', $Shopping['Cart']['id']),
                        array('class' => 'btn btn-success btn-xs', 'escape'=>false)); ?>
                    
                </td>
            </tr>
            <?php 
			} endforeach;
            ?>
        </tbody>
    </table>
</div>