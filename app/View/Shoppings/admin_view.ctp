<?php //foreach($cartitem as $cartit) {
//pr($cartit);	}?>
<table cellpadding="0" cellspacing="0"  id="sortable">
	<thead>
		<tr>
			<th>
				<?php echo __('id'); ?>
			</th>
			<th>
				<?php echo __('Image'); ?>
			</th>
			<th>
				<?php echo __('Course Name'); ?>
			</th>
			
			<th>
				<?php echo __('Course Price'); ?>
			</th>
			
			<th>
				<?php echo __('Posted By'); ?>
			</th>
			<th>
				<?php echo __('Action'); ?>
			</th>				 
		</tr>
	</thead>
	<tbody>
		<?php
		foreach  ($cartitem as $key => $cartit) :
		
		?>
		<tr>
			<td>
				<?php echo ++$key; ?>&nbsp;
			</td>
			<td>
				<?php
				if(isset($cartit['Post']['PostImage']['0']['originalpath']) and !empty($cartit['Post']['PostImage']['0']['originalpath']))
				{
				?>
				<img alt="" src="<?php echo $this->webroot;?>img/post_img/<?php echo $cartit['Post']['PostImage']['0']['originalpath'];?>" style=" height:80px; width:80px;">
				<?php
				}
				else{
				?>
			   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

			   <?php } ?>
			</td>
			<td>
				<?php echo h($cartit['Post']['post_title']);?>
			</td>
			<td>
				<?php echo '$'.h($cartit['Post']['price']);?>
			</td>
			
			<td>
				<?php
				if(isset($cartit['Post']['User']['first_name']) and !empty($cartit['Post']['User']['first_name']))
				{ ?>
				<?php echo ($cartit['Post']['User']['first_name'].' '.$cartit['Post']['User']['last_name']); ?>
				<?php } else { echo 'No Data Found'; } ?>
			</td>
			<td>
				<?php
				echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-times')),
				array('action' => 'delete', $cartit['CartItem']['cart_id'],$cartit['CartItem']['post_id']),
				array('class' => 'btn btn-danger btn-xs', 'escape'=>false));
				?>
			</td>
		</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>