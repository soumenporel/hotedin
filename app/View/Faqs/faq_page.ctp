<style>
.ui-widget {
    width: 672px !important;
}
</style>
<section class="inner_content">
    <div class="container">

        <div class="cart-area">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-sm-offset-2">
                    <div class="cart-area-left">
                        <div class="media">
                            <div class="media-body">
                            	<h3>FAQs</h3>
                            	<div id="accordion">
                                	<?php foreach ($content as $key => $value) { ?>
								  <h3><?php echo $value['Faq']['title']; ?></h3>
								  <div>
								    <p><?php echo $value['Faq']['description']; ?></p>
								  </div>
								  <?php } ?>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#accordion" ).accordion({
      collapsible: true
    });
  } );
  </script>
