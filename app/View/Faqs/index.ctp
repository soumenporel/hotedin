<!--login bg-->
<section class="faq-section py-5 mb-5">
  <div class="container">
  </div>
</section>

  <div class="container">
      <div class="row d-flex justify-content-center">
          <h1  class="faq-section-heading text-center w-100 mb-3">Frequently Asked Questions</h1>

          <div class="col-sm-10">
              <?php $flag = 0; ?>

              <!--start accordion-->
              <div id="accordion">
                  <?php foreach ($faqs as $faq) { ?>
                      <?php $flag++ ?>
                      <?php if($flag < 2) { ?>
                      <div class="card faq">
                        <div class="card-header" id="headingOne">
                          <h5 class="mb-0"><span>Q</span>
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $flag ?>" aria-expanded="true" aria-controls="collapse<?php echo $flag ?>">
                              <?php echo $faq['Faq']['title'] ?>
                            </button>
                          </h5>
                        </div>

                        <div id="collapse<?php echo $flag ?>" class="collapse show" aria-labelledby="heading<?php echo $flag ?>" data-parent="#accordion">
                          <div class="card-body">
                            <?php echo $faq['Faq']['description']; ?>
                          </div>
                        </div>
                      </div>
                    <?php }else { ?>
                      <div class="card faq">
                        <div class="card-header" id="headingOne">
                          <h5 class="mb-0"><span>Q</span>
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $flag ?>" aria-expanded="false" aria-controls="collapse<?php echo $flag ?>">
                              <?php echo $faq['Faq']['title'] ?>
                            </button>
                          </h5>
                        </div>

                        <div id="collapse<?php echo $flag ?>" class="collapse" aria-labelledby="heading<?php echo $flag ?>" data-parent="#accordion">
                          <div class="card-body">
                            <?php echo $faq['Faq']['description']; ?>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  <?php } ?>
              </div>
              <!--end accordion-->
          </div>
      </div>
  </div>
