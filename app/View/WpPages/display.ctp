<!--<section class="inner_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="heading_part">
					<h2><?php
					echo $cmsPage['WpPage']['title'];
					?></h2>
					<?php
					echo $cmsPage['WpPage']['content'];
					?>
				</div>
			</div>
		</div>
	</div>
</section>-->
                                        
<section class="home-slider inner-banner">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
      </ol>
     <div class="carousel-inner" role="listbox">
                <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?> no-shdow " style="background: url('<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>') no-repeat center center; background-size:cover;" >        
        
                    <div class="carousel-caption">
                    <div class="row align-items-center justify-content-between">
                      <?php echo $banner['Pagebanner']['desc']; ?>

                    </div>
                  </div>
                  </div>
                <?php $bi++; } ?>
              </div>
   </div>
</section>

<?php
    echo $cmsPage['WpPage']['content'];
    ?>
<style media="screen">

.bg-gray {
  background: #f5f5f5 none repeat scroll 0 0;
}
.press-box-new {
  position: relative;
}
.press-box-new h1 {
  bottom: 0;
  color: #fff;
  height: 20px;
  left: 0;
  margin: auto;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  z-index: 99;
}
.expert {
  background: #fff none repeat scroll 0 0;
  border: 1px solid #ddd;
}
.bottom-border {
  border-bottom: 1px solid #ddd;
}
.right-border {
  border-right: 1px solid #ddd;
}
.experttext {
  font-size: 24px;
}
.expert p {
  font-size: 16px;
  line-height: 21px;
  padding: 15px;
}
.mr-top-20 {
  margin-top: 20px;
}

</style>

