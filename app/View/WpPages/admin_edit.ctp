<div class="contents form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('WpPage',array('type' => 'file','enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Content'); ?></legend>
	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('title');
		echo $this->Form->input('heading');
        echo $this->Form->input('page_url');
        echo $this->Form->input('points',array('type' => 'hidden'));
        ?>
        <span>Points :</span><br>No. 1<input num="1" id="addpoints" type="text" name="dat[1]" value="">
          <button type="button" id="points" name="button">Add More</button>
        <?php
		echo $this->Form->input('show_in_header');
        echo $this->Form->input('show_in_footer');
        echo $this->Form->input('status');
        echo $this->Form->input('image', array('type' => 'file'));
        echo $this->Form->input('image', array('type' => 'hidden', 'name' => 'hide_img'));
	?>
        <textarea name="data[WpPage][content]" id="Contentcontent" style="width:900px; height:600px;" class="validate[required]" ><?php echo $this->request->data['WpPage']['content']; ?></textarea>
        <!-- <textarea name="data[CmsPage][page_description_sp]" id="Contentcontent_sp" style="width:900px; height:600px;" class="validate[required]" ><?php //echo $this->request->data['CmsPage']['page_description_sp']; ?></textarea> -->
        <?php if($this->request->data['WpPage']['id']==5){
         echo $this->Form->input('image', array('type' => 'hidden', 'name' => 'saved_image'));
         echo $this->Form->input('image', array('type' => 'file','label'=>'Bottom banner'));
       ?>
        <textarea name="data[WpPage][bannercontent]" id="Bannercontent" style="width:900px; height:600px;">
        <?php echo $this->request->data['WpPage']['bannercontent']; ?>
        </textarea>
        <?php } ?>
    </fieldset>

     <?php
        if($this->request->data['WpPage']['image'] != '') {
        ?>
        <div style="width: 100%;">
            <img src="<?php echo $this->webroot.'cms_page/'.$this->request->data['WpPage']['image'] ?>" style="width: 100%;" />
        </div>
        <?php
        }
        ?>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('Contentcontent',
            {
                width: "95%"
            });
    CKEDITOR.replace('Bannercontent',
            {
                width: "95%"
            });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
      var count = 1;
      var points = $('#WpPagePoints').val();
      var splitedPoints = points.split(',');
      var flag = 0
      for(var i in splitedPoints) {
          flag++;
          if(flag < 2){
              $('#addpoints').val(splitedPoints[i]);
          } else {
              var j = parseInt(i)+1;
              var htm = '<br>No. '+j+'<input id="addpoints" type="text" name="dat['+j+']" value="'+splitedPoints[i]+'">';
                    $('#addpoints').after(htm);
                    $('#addpoints').attr('id','');
          }

      }


        $('#points').click(function(event) {
            count++;
            var htm = '<br>No. '+count+'<input id="addpoints" type="text" name="dat['+count+']" value="">';
            $('#addpoints').after(htm);
            $('#addpoints').attr('id','');
        });

    });
    </script>
