<section style="background: #f9f9f9" class="pt-5 pb-4">
   <div class="container">
     <?php
					echo $cmsPage['WpPage']['content'];
					?>
   </div>
</section>

<style media="screen">
.about-banner{background:url('https://learnfly.com/img/abouts-banners-1.jpg') #0C2440 center no-repeat; padding-top: 110px; padding-bottom: 140px;}
.abouts-banner h1{color:#fff; font-size: 33px; line-height: 34px; margin-top: 0; margin-bottom: 0; font-weight:700; position: relative;}
.abouts-banner h1:before{content: ''; position: absolute; width: 40%; height: 2px; background: #f8d24b; bottom:-10px; left: 0;}
.abouts-banner h1 span{color:#f8d24b; font-size:41px;}
.read-show{display: none;}
.read-show .he{padding-top:15px;}

.about-cate{position:relative; overflow:hidden;}
.about-cate img{transform: scale(1); transition: .4s ease-in-out;}
.about-figurecaption{font-size: 28px; color: #fff; position: absolute; width: 100%; height: 100%; top: 0; left: 0; right: 0; bottom: 0; margin:auto; text-align: center; padding: 140px 0;}
.about-cate:hover img{transform: scale(1.2); transition: .4s ease-in-out;}


.existence{background: #fff; border:#ddd solid 1px; margin-top: 20px;}
.existence-box{border-bottom:1px #ddd solid;}
.existence .existence-icon{border-right:#ddd solid 1px;}
.existence h3{color:#0493e2;}
.existence h3 span{color: #0C2440; font-weight: 700; }
.existence h4{font-size:15px; line-height:25px; padding:10px;}

.inputs-inp{margin-top: 15px;}
.inputs-inp input{padding: 8px; background: #f9f9f9; border: #eee solid 1px; width: 100%;}
.inputs-inp textarea{padding: 8px; background: #f9f9f9; border: #eee solid 1px; height: 198px; width: 100%;}
.submit-btn{border: 0; padding:5px 20px; border-radius: 3px; background: #08bccc; color: #fff; font-size: 17px;}
.submit-btn:hover{background:#f8d24b; color: #0C2440;}

.contact-info{background: #f9f9f9; padding-bottom: 15px; border: #eee solid 1px;}
.contact-info h1{color: #0C2440; font-size: 20px; padding: 8px; margin: 0;}
.contact-form h2{color: #0C2440; font-size: 20px; padding: 8px 15px; margin: 0; font-weight: 700;}
.contact-box{padding: 8px; border-bottom:#eee solid 1px;}
.span-1{font-size: 18px; color: #104A80}
.span-2{font-size: 20px; color: #104A80; font-weight: 700;}
</style>
