<section class="home-slider inner-banner">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
         <div class="carousel-item no-shdow " style="background: url('/team4/studilmu/img/faq_new.png') no-repeat center center; background-size:cover;" >
         </div>
         <div class="carousel-item no-shdow" style="background: url('/team4/studilmu/img/faq_new.png') no-repeat center center; background-size:cover;" >
         </div>
         <div class="carousel-item active  no-shdow"  style="background: url('/team4/studilmu/img/faq_new.png') no-repeat center center; background-size:cover;" >
         </div>
      </div>
   </div>
</section>

<section class="py-5 faq">
   <div class="container">
      <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"> Frequently Asked Question<span class="font-weight-bold" style="color:#f00; "> (FAQ)</span>
         <em class="dvdr">
         <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
         </em>
      </h1>
      <div id="accordion" role="tablist" aria-multiselectable="true">
         <div class="card mb-3 w-100">
            <div class="card-header border-bottom-0" role="tab" id="headingOne">
               <h4 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3" class="d-block text-blue">
                  <span class="font-14">Business Page FAQ</span> <i class="ion-ios-plus-outline float-right"></i>
                  </a>
               </h4>
            </div>
            <div id="collapse3" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
               <div class="card-block pt-0">
                  <p class=" font-14 text-gray"></p>
                  <p>
                     Business Page FAQ
                  </p>
                  <p></p>
               </div>
            </div>
         </div>
         <div class="card mb-3  w-100">
            <div class="card-header border-bottom-0" role="tab" id="headingOne">
               <h4 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6" class="d-block text-blue">
                  <span class="font-14">New Business FAQ</span> <i class="ion-ios-plus-outline float-right"></i>
                  </a>
               </h4>
            </div>
            <div id="collapse6" class="collapse " role="tabpanel" aria-labelledby="headingOne">
               <div class="card-block pt-0">
                  <p class=" font-14 text-gray"></p>
                  <p>
                     New Business FAQ
                  </p>
                  <p></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
