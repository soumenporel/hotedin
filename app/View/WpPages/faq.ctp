<section class="home-slider inner-banner">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
      </ol>
     <div class="carousel-inner" role="listbox">
                <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?> no-shdow " style="background: url('<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>') no-repeat center center; background-size:cover;" >        
        
                    <div class="carousel-caption">
                    <div class="row align-items-center justify-content-between">
                      <?php echo $banner['Pagebanner']['desc']; ?>

                    </div>
                  </div>
                  </div>
                <?php $bi++; } ?>
              </div>
   </div>
</section>

<section class="py-5 faq">
   <div class="container">
      <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"> Frequently Asked Question<span class="font-weight-bold" style="color:#f00; "> (FAQ)</span>
         <em class="dvdr">
         <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
         </em>
      </h1>
      <div id="accordion" role="tablist" aria-multiselectable="true">
       <?php 
       $i=1;
           foreach($blogs as $blog)
           {  
               
         ?>
         <div class="card mb-3 w-100">
            <div class="card-header border-bottom-0" role="tab" id="headingOne">
               <h4 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="<?php if($i==1){ echo 'true' ;} else{ echo 'false'; }?>" aria-controls="collapse3" class="d-block text-blue">
                  <span class="font-14"><?php echo($blog['Faq']['title'])?></span> <i class="ion-ios-plus-outline float-right"></i>
                  </a>
               </h4>
            </div>
            <div id="collapse<?php echo $i; ?>" class="collapse <?php if($i==1){ echo 'show' ;} ?>" role="tabpanel" aria-labelledby="headingOne">
               <div class="card-block pt-0">
                  <p class=" font-14 text-gray"></p>
                  <p>
                     <?php echo($blog['Faq']['description'])?>
                  </p>
                  <p></p>
               </div>
            </div>
         </div>
           <?php $i++; } ?>
      </div>
   </div>
</section>
