<section class="margin-top-50">
   <div class="container">
      <div class="row text-center margin-top-30">
         <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 padding-btm-20">
            <h2 class="coursetext">
               Learnfly
               <strong class="text-upperacse textDarkSkyBlue"> Press Releases</strong>
            </h2>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12 margin-top-30">
            <div class="press-box">
               <a href="/press_releases/details/us-study-reveals-rising-stress-at-work-driven-by-politics-artificial-intelligence-and-pressure-to-master-new-skills">
               <span>August 08, 2017</span>
               <strong>US Study Reveals Rising Stress at Work Driven by Politics, Artificial Intelligence and Pressure to Master New Skills.</strong>
               </a>
            </div>
         </div>
         <div class="col-sm-12 margin-top-30">
            <div class="press-box">
               <a href="/press_releases/details/u-s-study-reveals-rising-stress-at-work-driven-by-politics-artificial-intelligence-and-pressure-to-master-new-skills">
               <span>August 08, 2017</span>
               <strong>U.S. Study Reveals Rising Stress at Work Driven by Politics, Artificial Intelligence and Pressure to Master New Skills.</strong>
               </a>
            </div>
         </div>
         <div class="col-sm-12 margin-top-30 margin-bottom-50">
            <div class="press-pagination">
               <ul>
                  <p>
                     Page 1 of 1, showing 2 records out of 2 total, starting on record 1, ending on 2
                  </p>
                  <div class="paging">
                     <li><span class="prev disabled">&lt; previous</span>							</li>
                     <li>							</li>
                     <li><span class="next disabled">Next &gt;</span>						</li>
                  </div>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
