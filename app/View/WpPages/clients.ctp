<section class="home-slider inner-banner">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
                <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?> no-shdow " style="background: url('<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>') no-repeat center center; background-size:cover;" >        
        
                    <div class="carousel-caption">
                    <div class="row align-items-center justify-content-between">
                      <?php echo $banner['Pagebanner']['desc']; ?>

                    </div>
                  </div>
                  </div>
                <?php $bi++; } ?>
              </div>
   </div>
</section>
<section class="pt-5 pb-5">
  <div class="container">
    <div class="row">
   <?php
        echo $cmsPage['WpPage']['content'];
        ?>
  </div>
  </div>
</section>

<section class="pb-5">
  <section class="container">
    <div class="flexslider">
       <ul class="slides d-flex">
           
           <?php 
                foreach($clients as $blog)
           {
             ?>
            <li class="d-flex align-items-center">
            <img src="<?php echo $this->webroot.'client/'.$blog['Client']['image']; ?>" alt="<?php echo $blog['Client']['name']; ?>">
          </li>
         
           <?php
                }
            ?>
           <?php 
                foreach($clients as $blog)
           {
             ?>
            <li class="d-flex align-items-center">
            <img src="<?php echo $this->webroot.'client/'.$blog['Client']['image']; ?>" alt="<?php echo $blog['Client']['name']; ?>">
          </li>
         
           <?php
                }
            ?>
           <?php 
                foreach($clients as $blog)
           {
             ?>
            <li class="d-flex align-items-center">
            <img src="<?php echo $this->webroot.'client/'.$blog['Client']['image']; ?>" alt="<?php echo $blog['Client']['name']; ?>">
          </li>
         
           <?php
                }
            ?>
         
       </ul>
     </div>
  </section>
</section>


<section style="background-image: url('<?php echo $this->webroot; ?>banner/<?php echo $cmsPage['WpPage']['image']; ?>');" class="hero">
   <?php
        echo $cmsPage['WpPage']['bannercontent'];
        ?>
</section>
<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 70,
    itemMargin: 15,
    controlNav: false,
  });
  console.log("hi");
});
</script>
