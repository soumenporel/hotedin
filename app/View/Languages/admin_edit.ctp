<div class="categories form">
<?php echo $this->Form->create('Language',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Language Management'); ?></legend>
	<?php
	
		echo $this->Form->input('id');
		echo $this->Form->input('short_name',array('required'=>'required'));
		echo $this->Form->input('full_name',array('required'=>'required'));
				
	?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
