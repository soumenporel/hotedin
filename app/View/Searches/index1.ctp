<section class="listing_result">
    <div class="container">
        <div class="row" style="background: #f6f6f6; border-left: 1px solid #dedede; border-right: 1px solid #dedede;">
        		<div class="col-md-3 col-sm-3" style="padding:0">
        			<div class="publicprofile filter">
        				<h3><i class="fa fa-filter"></i> Filter
        				<div class="checkbox">
							<label>
                                                            <input type="checkbox" id='chkFilter'> Clear all
							</label>
						</div>
						</h3>
        				<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Keyword</label>
                                                    <input type="hidden" id='search_keyword' name="data['keywords']" value="" />
						    <input type="text" class="form-control" id="skill_input1" placeholder="Search by Keyword">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Course Name</label>
                                                     <input type="hidden" id='search_course' name="data['keywords']" value="" />
						    <input type="text" class="form-control" id="course_input1" placeholder="Search by Course Name">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Degree</label>
						    <input type="hidden" id='search_degree' name="data['degrees']" value="" />
						    <input type="text" class="form-control" id="degree_input1" placeholder="Search by Degree">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Specialisation/Subject</label>
                                                    <input type="hidden" id='search_subject' name="data['keywords']" value="" />
						    <input type="text" class="form-control" id="subject_input1" placeholder="Specialisation/Subject">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Location</label>
                                                    
                                                    <input type="hidden" name="data[search_lat]" value="" id="search_lat"/>
                                                    <input type="hidden" name="data[search_lng]" value="" id="search_lng"/>
						    <input type="text" class="form-control" id="search_location" placeholder="Location">
						  </div>
<!--						  <button type="submit" class="btn btn-danger">Done</button>-->
						</form>
        			</div>
        		</div>
        		<div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
        			<div class="profile_second_part">
        				<div class="search-result-head">
                                            <h2>Total <span id='searchCount'><?php echo $searchCount; ?></span> result found </h2>
        					<form class="form-inline pull-right">
							  <div class="form-group">
							    <label for="exampleInputName2">Sort By</label>
                                                            <select name="" class="form-control" id='order_filter' onchange="searchAjax();">
                                                                <option value="newest">Newest</option>
                                                                <option value="popularity">Popularity</option>							    	
							    	<option value="review">Reviews</option>
							    	
<!--                                                                <option value="language">Language</option>-->
							    </select>
							  </div>
							</form>
        				</div>
						<div class="search-result-body">
							<div class="row filter_content">
                                                            <?php
                                                            foreach ($results as $result) 
                                                            {
                                                                 if($result['User']['user_image']!=null||$result['User']['user_image']!='')
                                                                {
                                                                    $img=$this->webroot.'user_images/'.$result['User']['user_image'];
                                                                }
                                                                else
                                                                {
                                                                    $img=$this->webroot.'img/noimage.png'; 
                                                                }
                                                            ?>  
                                                            <div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
                                                                                       
										      <img src="<?php echo $img;?>" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading"><?php echo $result['User']['first_name']." ".$result['User']['last_name'];?></h4>
										    <p><?php echo $result['User']['first_name']." ".$result['User']['last_name'];?></p>
                                                                                    <p><?php echo $result['User']['email_address'];?></p>
                                                                                   <p><?php echo $result['User']['address'];?></p>
                                                                                   
                                                                                    <p class="ratings">Ratings  
                                                                                        <?php for($i=0;$i<$result['User']['avg_rating'];$i++){ ?>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <?php } ?>
                                                                                    </p>
                                                                                    <?php if(isset($userid) && $userid!='' ){?>
                                                                                    <p id="skillContainer_<?php echo $result['User']['id']; ?>">
                                                                                          <a href="javascript:void(0);" class="follow"  onclick="follwIns('<?php echo $result['User']['id']; ?>')"><i class="fa fa-plus"></i> Follow</a> 
                                                                                    </p> 
                                                                                    <p id="favContainer_<?php echo $result['User']['id']; ?>" >
                                                                                          <a href="javascript:void(0);" class="favorite" onclick="favIns('<?php echo $result['User']['id']; ?>')"><i class="fa fa-heart"></i> Favorite</a>
                                                                                    </p>
                                                                                    <?php } ?>
<!--                                                                                    <p>
                                                                                          <a href="" class="follow disable"><i class="fa fa-minus"></i> Unfollow</a> 
                                                                                          <a href="" class="favorite disable"><i class="fa fa-heart-o"></i> Favorite</a>
                                                                                    </p>
                                                                                   -->
<!--                                                                                    <span id="skillContainer_<?php echo $result['User']['id']; ?>"><button type="button" class="btn btn-primary" onclick="follwIns('<?php echo $result['User']['id']; ?>')">Follow</button></span>


                                                                                    <span id="favContainer_<?php echo $result['User']['id']; ?>"><button type="button" class="btn btn-success" onclick="favIns('<?php echo $result['User']['id']; ?>')">Favourite</button></span>-->
										  </div>
										</div>
									</div>
								</div>
                                                             <?php
                                                            }
                                                            
                                                            ?>
								
								
								
							</div>
						</div>
        			</div>
        		</div>
        </div>
    </div>
</section>

<script>
$(function () {
    
    //for skill...................................................................
            $('#skill_input1').autocomplete({
                minLength: 1,
                source: function (request, response) {
                    var keyword = $('#skill_input1').val();
                  
                    var url = '<?php echo $this->webroot .'users/skillsuggest/'?>' + keyword;
                    $.getJSON(url, response);
                }
               
                
            });
            
            $('#skill_input1').data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    
                    var $li = $('<li>');
                    var label='skill';
                   
                    $li.addClass('suggestrow skillsuggestrow');
                    $li.attr('data-value', item.link);
             
                    $li.attr('data-label', item.label);
                    $li.append('<a href="javascript:void(0);" onclick="searchFn(\''+label+'\',\''+item.link+'\');">');
                    $li.find('a').append('<div class="suggestDiv"><span class="suggestName">'+item.label+'</span></div>');    

                    return $li.appendTo(ul);
                  };
                  
                  //for degree..........................................
                  $('#degree_input1').autocomplete({
                minLength: 1,
                source: function (request, response) {
                    var keyword = $('#degree_input1').val();
                  
                    var url = '<?php echo $this->webroot .'searches/degreesuggest/'?>' + keyword;
                    $.getJSON(url, response);
                }
               
                
            });
          
      
            $('#degree_input1').data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    
                    var $li = $('<li>');
                    var label='degree';
                   
                    $li.addClass('suggestrow skillsuggestrow');
                    $li.attr('data-value', item.link);
             
                    $li.attr('data-label', item.label);
                    $li.append('<a href="javascript:void(0);" onclick="searchFn(\''+label+'\',\''+item.link+'\');">');
                    $li.find('a').append('<div class="suggestDiv"><span class="suggestName">'+item.label+'</span></div>');    

                    return $li.appendTo(ul);
                  };
     
              //for subject..........................................
                  $('#subject_input1').autocomplete({
                minLength: 1,
                source: function (request, response) {
                    var keyword = $('#subject_input1').val();
                  
                    var url = '<?php echo $this->webroot .'searches/subjectsuggest/'?>' + keyword;
                    $.getJSON(url, response);
                }
               
                
            });
          
      
            $('#subject_input1').data( "ui-autocomplete")._renderItem = function( ul, item ) {
    
                    var $li = $('<li>');
                    var label='subject';
                   
                    $li.addClass('suggestrow skillsuggestrow');
                    $li.attr('data-value', item.link);
             
                    $li.attr('data-label', item.label);
                    $li.append('<a href="javascript:void(0);" onclick="searchFn(\''+label+'\',\''+item.link+'\');">');
                    $li.find('a').append('<div class="suggestDiv"><span class="suggestName">'+item.label+'</span></div>');    

                    return $li.appendTo(ul);
                  };
      
         //for course.........................................
                  $('#course_input1').autocomplete({
                minLength: 1,
                source: function (request, response) {
                    var keyword = $('#course_input1').val();
                  
                    var url = '<?php echo $this->webroot .'searches/coursesuggest/'?>' + keyword;
                    $.getJSON(url, response);
                }
               
                
            });
          
      
            $('#course_input1').data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    
                    var $li = $('<li>');
                    var label='course';
                   
                    $li.addClass('suggestrow skillsuggestrow');
                    $li.attr('data-value', item.link);
             
                    $li.attr('data-label', item.label);
                    $li.append('<a href="javascript:void(0);" onclick="searchFn(\''+label+'\',\''+item.link+'\');">');
                    $li.find('a').append('<div class="suggestDiv"><span class="suggestName">'+item.label+'</span></div>');    

                    return $li.appendTo(ul);
                  };
                  
                  //for checkbox filtering...................................
                  $('input[type="checkbox"]'). click(function(){
                if($(this). prop("checked") == true){
                //alert("Checkbox is checked." );
                 $('#search_keyword').val('');

                $('#search_degree').val('');

                $('#search_course').val('');

                $('#search_subject').val('');
                
                $('#search_lat').val('');

                $('#search_lng').val('');

                $('#search_location').val('');
                
                $('#skill_input1').val('');

                $('#course_input1').val('');

                $('#degree_input1').val('');

                $('#subject_input1').val('');
               
                searchAjax();
                

           }
                else if($(this). prop("checked") == false){
                //alert("Checkbox is unchecked." );
                }
            });
      
        });
        
        
        function searchFn(type,link)
        {
           // alert(link);
            //alert(type);
           if(type=='skill')
           {
             $('#search_keyword').val(link);
           }
           if(type=='degree')
           {
             $('#search_degree').val(link);
           }
           if(type=='course')
           {
             $('#search_course').val(link);
           }
           if(type=='subject')
           {
             $('#search_subject').val(link);
           }
          searchAjax();
        }
        
        function searchAjax()
        {
          //alert(1);
          var keyword=$('#search_keyword').val();
          var degree=$('#search_degree').val();
          var course=$('#search_course').val();
          var subject=$('#search_subject').val();
          var location=$('#search_location').val();
          var search_lat=$('#search_lat').val();
          var search_lng=$('#search_lng').val();
          var order_filter = $("#order_filter").val();
          
          $.ajax({
            type: "POST",             // Type of request to be send, called as method
            dataType: 'json',
            url: "<?php echo $this->webroot;?>searches/searchUser",
            data: {
                skill:keyword,
                degree:degree,
                course:course,
                subject:subject,
                location:location,
                search_lat:search_lat,
                search_lng:search_lng,
                order_filter:order_filter
            },
            beforeSend: function () {
            },
            success: function (result) {
             console.log(result);
                    //alert(result.ACK);
                    
                    $('#searchCount').html(result.searchCount);
                    $('.filter_content').empty();
                    $('.filter_content').append(result.html);
            }
        }); 
        }
        
    function follwIns(id)
    {
        //alert(id);
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>searches/followinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data);
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#skillContainer_' + id).html('<a href="javascript:void(0);" class="follow disable"  onclick="unfollwIns(' + data + ')"><i class="fa fa-minus"></i> UnFollow</a>');
                   
                }

            }
        });
    }


    function unfollwIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>searches/unfollowinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#skillContainer_' + id).html('<a href="javascript:void(0);" class="follow"  onclick="follwIns(' + data + ')"><i class="fa fa-plus"></i>Follow</a>');
                    
                }

            }
        });
    }



    function favIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>searches/favinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#favContainer_' + id).html(' <a href="javascript:void(0);" class="favorite disable" onclick="unfavwIns('+data+')"><i class="fa fa-heart-o"></i> Favorite</a>');
                }

            }
        });
    }


    function unfavwIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>searches/unfavinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#favContainer_' + id).html(' <a href="javascript:void(0);" class="favorite" onclick="favIns('+data+')"><i class="fa fa-heart"></i> Favorite</a>');
                   
                }

            }
        });
    }
      
 </script>
 
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAr9mFfvbx8XxKEJPE3By2AghiC8c2e-5s&libraries=places&callback=initMap"
        async defer></script>

<script>
      function initMap() {
          var autocomplete = new google.maps.places.Autocomplete($("#search_location")[0], {});
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
              var place = autocomplete.getPlace();
               
              $('#search_lat').val(place.geometry.location.lat());
              $('#search_lng').val(place.geometry.location.lng());
              searchAjax();
      
          });
      }

      // function loadScript() {
      //     var script = document.createElement("script");
      //     script.type = "text/javascript";
      //     script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAr9mFfvbx8XxKEJPE3By2AghiC8c2e-5s&libraries=places";
      //     document.body.appendChild(script);
      // }

      //window.onload = loadScript;
</script>