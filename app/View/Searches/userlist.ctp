<section class="listing_result">
    <div class="container">
        <div class="row" style="background: #f6f6f6; border-left: 1px solid #dedede; border-right: 1px solid #dedede;">
        		<div class="col-md-3 col-sm-3" style="padding:0">
        			<div class="publicprofile filter">
        				<h3><i class="fa fa-filter"></i> Filter
        				<div class="checkbox">
							<label>
								<input type="checkbox"> Clear all
							</label>
						</div>
						</h3>
        				<form>
						  <div class="form-group">
						    <label for="exampleInputEmail1">Keyword</label>
						    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search by Keyword">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Course Name</label>
						    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Search by Course Name">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Degree</label>
						    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="By Degree">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Specialisation/Subject</label>
						    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Specialisation/Subject">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Location</label>
						    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Location">
						  </div>
						  <button type="submit" class="btn btn-danger">Done</button>
						</form>
        			</div>
        		</div>
        		<div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
        			<div class="profile_second_part">
        				<div class="search-result-head">
        					<h2>Total 300 result found </h2>
        					<form class="form-inline pull-right">
							  <div class="form-group">
							    <label for="exampleInputName2">Short By</label>
							    <select name="" class="form-control">
							    	<option>Popularity</option>
							    	<option>Language</option>
							    	<option>Reviews</option>
							    	<option>Newest</option>
							    </select>
							  </div>
							</form>
        				</div>
						<div class="search-result-body">
							<div class="row">
								<div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
										      <img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ritwik Instructor</h4>
										    <p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
										  </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
										      <img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ritwik Instructor</h4>
										    <p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
										  </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
										      <img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ritwik Instructor</h4>
										    <p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
										  </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
										      <img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ritwik Instructor</h4>
										    <p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
										  </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
										      <img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ritwik Instructor</h4>
										    <p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
										  </div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="instructors">
										<div class="media">
										  <div class="media-left">
										    <a href="#">
										      <img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
										    </a>
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ritwik Instructor</h4>
										    <p>ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
										  </div>
										</div>
									</div>
								</div>
							</div>
						</div>
        			</div>
        		</div>
        </div>
    </div>
</section>