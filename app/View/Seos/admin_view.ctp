<div class="testimonials view">
    <h2><?php echo __('SEO Keywords'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($seo['Seo']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Page Name'); ?></dt>
        <dd>
            <?php echo h($seo['Seo']['page_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Meta keyword'); ?></dt>
        <dd>
            <?php echo h($seo['Seo']['meta_keyword']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('meta_description'); ?></dt>
        <dd>
            <?php echo h($seo['Seo']['meta_description']); ?>
            &nbsp;
        </dd>
        <!-- <dt><?php echo __('Date'); ?></dt>
        <dd>
            <?php echo h($testimonial['Testimonial']['date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
            <?php echo $testimonial['Testimonial']['status']=='1' ? 'Active' : 'Inactive'; ?>
            &nbsp;
        </dd> -->
    </dl>
</div>