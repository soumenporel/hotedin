<div class="categories form">
<?php echo $this->Form->create('Category',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Categories'); ?></legend>
	<?php
		echo $this->Form->input('category_name',array('required'=>'required'));
		echo $this->Form->input('categories',array('empty' => '(choose one)','class'=>'selectbox2'));
		echo $this->Form->input('category_description',array('id'=>'cat_desc'));
		//echo $this->Form->input('country_id');
		echo $this->Form->input('is_principal',array('label'=>false,'hidden'=>'hidden'));
		echo $this->Form->input('status');
		echo $this->Form->input('show_in_homepage',array('label'=>'Show home page'));
                echo $this->Form->input('show_top',array('label'=>'Show top')); 
                echo $this->Form->input('top_order');?>
		<?php
		//echo $this->Form->input('category_of_the_day',array('label' => 'Top Categories','hidden'=>false));
		echo $this->Form->input('image',array('type'=>'file','required'=>'required'));
                echo $this->Form->input('imagelogo',array('type'=>'file','label'=>'Category Icon'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('cat_desc',
            {
                width: "95%"
            });
</script>