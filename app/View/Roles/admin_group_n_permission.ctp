<div class="rolesAccesses index">
    <h2><?php echo __('Create Group & Permission'); ?></h2>
    <div class="rolesAccesses form">
        <?php echo $this->Form->create('Role'); ?>
        <fieldset>
            <?php
            echo $this->Form->input('name');
        //echo $this->Form->input('accessibility');
            echo $this->Form->input('accessibility', array(
                'type' => 'select',
                'multiple' => 'checkbox',
                'options' => array(
                    'cms' => 'CMS_Pages',
                    'faq' => 'FAQ',
                    'banner' => 'Banner',
                    'home_slider' => 'Home_Slider',
                    'email_template' => 'Email_Template',
                    'normal_users' => 'Users',
                    'user_location' => 'Add User Location',
                    'categories' => 'Categories',
                    'course' => 'Course',
                    'contact_us' => 'Contact US',
                    'seo_keyword'=> 'SEO Keyword',
                    'language_management' => 'Language Management',
                    'language_resourse' => 'Language Resourse',
                    'sitemap' => 'Sitemaps',
                    'analytics' => 'Analytics',
                    'seo_url' => 'SEO URL',
                    'newsletter' => 'Newsletter',
                    'sub_admins' => 'Sub Admin'
                // 'setting' => 'Setting',
                // 'home_slider' => 'Home Slider',
                // 'list_slider' => 'List Slider',
                // 'add_slider' => 'Add Slider',
                // 'banner' => 'Banner',
                // 'list_banner' => 'List Banner',
                // 'add_banner' => 'Add Banner',
                // 'partners' => 'Partners',
                // 'list_partners' => 'List Partners',
                // 'add_partners' => 'Add Partners',
                // 'cms' => 'CMS',
                // 'list_cms' => 'List CMS',
                // 'add_cms' => 'Add CMS',
                // 'faq' => 'FAQ',
                // 'list_faq' => 'List FAQ',
                // 'add_faq' => 'Add FAQ',
                // 'categories' => 'Categories',
                // 'list_categories' => 'List Categories',
                // 'add_categories' => 'Add Categories',
                // 'properties' => 'Properties',
                // 'list_properties' => 'List Properties',
                // 'add_properties' => 'Add Properties',
                // 'email_template' => 'Email Template',
                // 'newsletters' => 'Newsletters',
                // 'users' => 'Users',
                // 'list_users' => 'List Users',
                // 'add_users' => 'Add Users',
                // 'contact' => 'Contact',
                // 'list_contact' => 'List Contact',
                // 'social' => 'Social',
                // 'list_social' => 'List Social',
                // 'menu' => 'Menu',
                // 'list_menu' => 'List Menu',
                // 'add_menu' => 'Add Menu',
                // 'header_menu' => 'Header Menu',
                // 'footer_menu' => 'Footer Menu',
                // 'testimonial' => 'Testimonial',
                // 'list_testimonial' => 'List Testimonial',
                // 'add_testimonial' => 'Add Testimonial',
                // 'news' => 'News',
                // 'list_news' => 'List News',
                // 'add_news' => 'Add News',
                // 'news_comments' => 'News Comments',
                // 'membership' => 'Membership',
                // 'plans' => 'Plans',
                // 'users_plan' => 'Users Plan'
                    )
                ));
                ?>
            </fieldset>
            <?php echo $this->Form->end(__('Submit')); ?>
        </div>
    </div>

    <div class="rolesAccesses index">
        <h2><?php echo __('Groups & Permissions'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                    <th><?php echo h('Groups'); ?></th>
                    <th><?php echo h('Permissions'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rolesAccesses as $rolesAccess): 
                    //echo '<pre>'; print_r($rolesAccess); echo '</pre>'; exit;
                ?>
                    <tr>
                        <td><?php echo h($rolesAccess['Role']['id']); ?>&nbsp;</td>
                        <td>
                            <?php echo $this->Html->link($rolesAccess['Role']['name'], array('controller' => 'roles', 'action' => 'view', $rolesAccess['Role']['id'])); ?>
                        </td>
                        <td>
                            <?php
                            echo implode(', ', unserialize($rolesAccess['RolesAccess']['accessibility']));
                            ?>
                        </td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('View'), array('controller'=>'roles_accesses','action' => 'view', $rolesAccess['RolesAccess']['id'])); ?>
                            <?php echo $this->Html->link(__('Edit'), array('controller'=>'roles_accesses', 'action' => 'edit', $rolesAccess['RolesAccess']['id'])); ?>
                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rolesAccess['Role']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $rolesAccess['Role']['id']))); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p>
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));
                ?>	</p>
                <div class="paging">
                    <?php
                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                    ?>
                </div>
            </div>