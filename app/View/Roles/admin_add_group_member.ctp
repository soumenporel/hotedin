<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
<style>
.dropdown-menu .open{
	    max-height: 50px;
}
</style>
<form action="<?php echo $this->webroot;?>admin/roles/add_group_member" id="RolesAccessAdminAddForm" method="post" accept-charset="utf-8">
	<div style="display:none;">
		<input type="hidden" name="_method" value="POST">
	</div>    
	<fieldset>
		<legend>Add Group Member Access</legend>
		        <div class="input text">
		        	<label for="RoleName">Group Name</label>
		        	<input name="data[Role][name]" maxlength="255" type="text" id="RoleName">
		        </div>
				<div class="input select">
					<label for="PostUserId">User</label>
					<select name="data[User][user_id][]" required="required" id="UserId" class="selectpicker" multiple>
						<?php foreach ($users as $key => $user) {?>
							<option value="<?php echo $user['User']['id'];?>"><?php echo $user['User']['first_name'].' '.$user['User']['last_name'];?></option>
						<?php } ?>
					</select>
				</div>
				<div class="input select">
					<label for="RolesAccessAccessibility">Accessibility</label>
					<input type="hidden" name="data[RolesAccess][accessibility]" value="" id="RolesAccessAccessibility">

						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="cms" id="RolesAccessAccessibilityCms">
							<label for="RolesAccessAccessibilityCms">CMS_Pages</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="faq" id="RolesAccessAccessibilityFaq">
							<label for="RolesAccessAccessibilityFaq">FAQ</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="banner" id="RolesAccessAccessibilityBanner">
							<label for="RolesAccessAccessibilityBanner">Banner</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="home_slider" id="RolesAccessAccessibilityHomeSlider">
							<label for="RolesAccessAccessibilityHomeSlider">Home_Slider</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="email_template" id="RolesAccessAccessibilityEmailTemplate">
							<label for="RolesAccessAccessibilityEmailTemplate">Email_Template</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="normal_users" id="RolesAccessAccessibilityNormalUsers">
							<label for="RolesAccessAccessibilityNormalUsers">Users</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="user_location" id="RolesAccessAccessibilityUserLocation">
							<label for="RolesAccessAccessibilityUserLocation">Add User Location</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="categories" id="RolesAccessAccessibilityCategories">
							<label for="RolesAccessAccessibilityCategories">Categories</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="course" id="RolesAccessAccessibilityCourse">
							<label for="RolesAccessAccessibilityCourse">Course</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="contact_us" id="RolesAccessAccessibilityContactUs">
							<label for="RolesAccessAccessibilityContactUs">Contact US</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="seo_keyword" id="RolesAccessAccessibilitySeoKeyword">
							<label for="RolesAccessAccessibilitySeoKeyword">SEO Keyword</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="language_management" id="RolesAccessAccessibilityLanguageManagement">
							<label for="RolesAccessAccessibilityLanguageManagement">Language Management</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="language_resourse" id="RolesAccessAccessibilityLanguageResourse">
							<label for="RolesAccessAccessibilityLanguageResourse">Language Resourse</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="sitemap" id="RolesAccessAccessibilitySitemap">
							<label for="RolesAccessAccessibilitySitemap">Sitemaps</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="analytics" id="RolesAccessAccessibilityAnalytics">
							<label for="RolesAccessAccessibilityAnalytics">Analytics</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="seo_url" id="RolesAccessAccessibilitySeoUrl">
							<label for="RolesAccessAccessibilitySeoUrl">SEO URL</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="newsletter" id="RolesAccessAccessibilityNewsletter">
							<label for="RolesAccessAccessibilityNewsletter">Newsletter</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" name="data[RolesAccess][accessibility][]" value="sub_admins" id="RolesAccessAccessibilitySubAdmins">
							<label for="RolesAccessAccessibilitySubAdmins">Sub Admin</label>
						</div>
				</div>
    </fieldset>
    <div class="submit">
    	<input type="submit" value="Submit">
    </div>
</form>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap-select.js"></script>