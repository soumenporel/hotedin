<!--  inner  slider   -->

      <section class="home-slider inner-banner">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
               <div class="carousel-inner" role="listbox">
                <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?> no-shdow " style="background: url('<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>') no-repeat center center; background-size:cover;" >        
        
                    <div class="carousel-caption">
                    <div class="row align-items-center justify-content-between">
                      <?php echo $banner['Pagebanner']['desc']; ?>

                    </div>
                  </div>
                  </div>
                <?php $bi++; } ?>
              </div>
                <?php //echo $bannerContent['CmsPage']['page_description']; ?>
              </div>
            </div>
      </section>

     <section class="py-5 pricing">
      <div class="container">
        <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">Membership<span class="font-weight-bold" style="color:#f00; "> Package</span>
          <em class="dvdr">
            <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
          </em>
        </h1>
          <!--<div class="row">
              <div class="col-lg-4">
                  <h1 class="zilla">Pricing</h1>
              </div>
          </div>-->
          <?php //echo '<pre>'; print_r($userdetails['User']); echo '</pre>'; ?>
          <table class="table table-bordered table-responsive d-block d-lg-table">
              <thead>
                  <th><h1 class="zilla font-weight-light">Membership Package</h1></th>
                  <th class="bg-danger text-white text-center"><h1 class="zilla font-weight-light text-uppercase">Free</h1></th>
                  <th class="text-center p-0 price-th">
                      <h4 class="zilla font-weight-light m-0 p-3 bg-light-sky text-white">Basic Membership</h4>
                      <table  class="w-100">
                          <tr>
                              <td class="border-top-0 border-bottom-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['0']['MembershipPlan']['duration'].' '.$mPlans['0']['MembershipPlan']['duration_in']; ?></p></td>
                              <td class="border-top-0 border-bottom-0 border-right-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['1']['MembershipPlan']['duration'].' '.$mPlans['1']['MembershipPlan']['duration_in']; ?></p></td>
                          </tr>
                      </table>
                  </th>
                  <th class="text-center p-0 price-th">
                      <h4 class="zilla font-weight-light m-0 p-3 bg-light-blue text-white">Premium Membership</h4>
                      <table  class="w-100">
                          <tr>
                              <td class="border-top-0 border-bottom-0 border-left-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['2']['MembershipPlan']['duration'].' '.$mPlans['2']['MembershipPlan']['duration_in']; ?></p></td>
                              <td class="border-top-0 border-bottom-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['3']['MembershipPlan']['duration'].' '.$mPlans['3']['MembershipPlan']['duration_in']; ?></p></td>
                          </tr>
                      </table>
                  </th>
              </thead>
              <tbody>
                  <?php foreach ($planItems as $key => $item) { ?>
                  <tr>
                      <td><h5><?php echo $item['MembershipItem']['name']; ?></h5></td>
                      <?php
                        $planList = $this->requestAction('/homepages/itemList/' . $item['MembershipItem']['id']);
                      ?>
                      <td class="text-center"><h4><i class="text-danger <?php echo (in_array($mPlans['4']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['0']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['1']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['2']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?> "></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['3']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <?php } ?>
           
                  <tr class="gray-bg">
                      <td><h4>Membership Fee</h4></td>
                      <td class="text-center"><h4 class="text-success"> Free </h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4 class="text-success">Rp <?php echo number_format($mPlans['0']['MembershipPlan']['price'],0,"",".");?></h4></td>
                                  <td class="text-center border-0 w-50"><h4 class="text-success">Rp <?php echo number_format($mPlans['1']['MembershipPlan']['price'],0,"",".");?></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4 class="text-success">Rp <?php echo number_format($mPlans['2']['MembershipPlan']['price'],0,"",".");?></h4></td>
                                  <td class="text-center border-0 w-50"><h4 class="text-success">Rp <?php  $p3 = number_format($mPlans['3']['MembershipPlan']['price'],0,"","."); echo $p3; ?></h4></td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <?php if(!empty($userDetails)){?>
                  <tr class="<?php if($userid && $userdetails['User']['admin_type']==2){ echo ''; }else{ echo 'showRow'; } ?>" >
                    <td></td>
                    <td class="text-center"><h4 class="text-success">  </h4></td>
                    <td class="p-0">
                      <table class="w-100">
                        <tr>
                          <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                            <h4 class="text-success">
                              <?php if(($mPlans['0']['MembershipPlan']['price'] > $userDetails['MembershipPlan']['price']) || ($userDetails['MembershipPlan']['price']=='')){ ?>
                                <a href="<?php echo $this->webroot; ?>checkouts/index/<?php echo $mPlans['0']['MembershipPlan']['id'];?>" class="btn bg-light-sky text-white">Select</a>
                              <?php }else{ ?>
                              Can't Downgrade
                              <?php } ?>
                            </h4>
                          </td>
                          <td class="text-center border-0 w-50">
                            <h4 class="text-success">
                              <?php if(($mPlans['1']['MembershipPlan']['price'] > $userDetails['MembershipPlan']['price']) || ($userDetails['MembershipPlan']['price']=='')){ ?>
                                <a href="<?php echo $this->webroot; ?>checkouts/index/<?php echo $mPlans['1']['MembershipPlan']['id'];?>" class="btn bg-light-sky text-white" >Select</a>
                              <?php }else{ ?>
                              Can't Downgrade
                              <?php } ?>
                            </h4>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td class="p-0">
                      <table  class="w-100">
                        <tr>
                          <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                            <h4 class="text-success">
                              <?php if(($mPlans['2']['MembershipPlan']['price'] > $userDetails['MembershipPlan']['price']) || ($userDetails['MembershipPlan']['price']=='')){ ?>
                                <a href="<?php echo $this->webroot; ?>checkouts/index/<?php echo $mPlans['2']['MembershipPlan']['id'];?>" class="btn btn-primary" >Select</a>
                              <?php }else{ ?>
                              Can't Downgrade
                              <?php } ?>
                            </h4>
                          </td>
                          <td class="text-center border-0 w-50">
                            <h4 class="text-success">
                              <?php if(($mPlans['3']['MembershipPlan']['price'] > $userDetails['MembershipPlan']['price']) || ($userDetails['MembershipPlan']['price']=='')){ ?>
                                <a href="<?php echo $this->webroot; ?>checkouts/index/<?php echo $mPlans['3']['MembershipPlan']['id'];?>" class="btn btn-primary" >Select</a>
                              <?php }else{ ?>
                              <a>Can't Downgrade</a>
                              <?php } ?>
                            </h4>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <?php } ?>
              </tbody>
          </table>
      </div>
    </section>

      <section class="py-5 skill-bg">
        <div class="container">
          <?php echo $pageHeader['CmsPage']['page_description']; ?>
          <div class="row">
            <div class="col-lg-6">
              <form method="post" action="#" >
                <div class="bg-white-transparent p-3 mw-500">
                    <h3 class="zilla font-weight-light mb-3 text-white">Company Information</h3>
                    <div class="form-group row">
                      <label for="example-text-input" class="col-4 col-form-label font-14 text-white">Full Name</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="full_name" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Company Name</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="company_name" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input"  class="col-4 col-form-label font-14 text-white">Company Email</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="company_email" type="email" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input"  class="col-4 col-form-label font-14 text-white">Office Number</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="office_number" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Mobile Number</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="mobile_number" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Industry</label>
                      <div class="col-8">
                        <div class="form-group">
                          <select class="form-control nullBorder" name="industry" required >
                            <option value="" >Choose from here</option>
                             <?php foreach($industries as $industry) { ?>
                                <option value="<?php echo $industry['Industry']['id']; ?>"><?php echo $industry['Industry']['name']; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Company Size</label>
                      <div class="col-8">
                        <div class="form-group">
                          <select class="form-control nullBorder" name="company_size" required >
                            <option value="" >Choose from here</option>
                            <option value="10 - 50 persons" >10 - 50 persons</option>
                            <option value="50 - 100 persons" >50 - 100 persons</option>
                            <option value="100 - 500 persons" >100 - 500 persons</option>
                            <option value="500 - 1000 persons">500 - 1000 persons</option>
                            <option value="More than 1000 persons">More than 1000 persons</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">How Many People Do You Need To Train</label>
                      <div class="col-8">
                        <div class="form-group">
                          <select class="form-control nullBorder" name="how_many_people" required >
                            <option value="" >Choose from here</option>
                            <?php for($i=5;$i<=100;$i=$i+5){ ?>
                             <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                            <?php } ?>
                            
                            <option value="More Than 100" >More Than 100</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group text-right">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                </div>
              </form>
            </div>
            <div class="col-lg-6">
              <?php echo $pageContent['CmsPage']['page_description']; ?>
            </div>
          </div>
        </div>
      </section>

      <!--  testimonials  -->

      <section class="testimonials py-5 bg-faded text-center">
        <div class="container">
            <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"><span class="font-weight-bold" style="color:#f00; "> Testimonials</span>
              <em class="dvdr">
                <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
              </em>
            </h1>
            <div id="carouseltestimonials" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                  <?php $aCount = 1; foreach ($testimonialdata as $key => $testimonial) { ?>
                    <div class="carousel-item <?php if($aCount==1){ echo 'active'; } ?>">
                      <div class="row justify-content-center">
                          <div class="col-lg-8 col-test">
                              <h5 class="font-weight-light"><?php echo $testimonial['Testimonial']['description']; ?></h5>
                              <!-- <div class="test-pic my-3 mx-auto">
                                  <img src="<?php echo $this->webroot;?>user_images/<?php echo $testimonial['User']['user_image']?>" alt="">
                              </div> -->
                              <h5 class="font-weight-bold"><?php echo $testimonial['User']['first_name'].' '.$testimonial['User']['last_name']; ?></h5>
                          </div>
                      </div>
                    </div>
                  <?php $aCount++; } ?>
                </div>
                <ol class="carousel-indicators mt-3">
                  <?php $ti = 0; foreach ($testimonialdata as $key => $testimonial) { ?>
                    <li data-target="#carouseltestimonials" data-slide-to="<?php echo $ti; ?>" class="<?= ($ti==0) ? 'active': ''; ?>"></li>
                  <?php $ti++; } ?>

                </ol>
              </div>
        </div>
      </section>


      <!--   contact  info  -->

      <section class="py-5 contact-info contact-info-govt">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-lg-7 text-white text-center py-lg-3">
                      <h1 class="zilla font-weight-light">Experience A Better learning Platform</h1>
                      <p class="font-weight-light">If you are not willing to learn, no one can help you. If you are determined to learn, no one can stop you. </p>
                      <a href="<?php echo $this->webroot; ?>contact-us" class="btn btn-danger btn-lg">Contact Us Now</a>
                  </div>
              </div>
          </div>
      </section>


      <!--   faq   -->


      <section class="py-5 faq">
        <div class="container">
            <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"> Frequently Asked Question<span class="font-weight-bold" style="color:#f00; "> (FAQ)</span>
              <em class="dvdr">
                <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
              </em>
            </h1>
            <div id="accordion" role="tablist" aria-multiselectable="true">
                <?php
                  $fCount = 1;
                  foreach ($faqs as $key => $faq) { ?>

                  <div class="card mb-3">
                      <div class="card-header border-bottom-0" role="tab" id="headingOne">
                          <h4 class="mb-0">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['Faq']['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['Faq']['id']; ?>" class="d-block text-blue">
                                <span class="font-14"><?php echo $faq['Faq']['title']; ?></span> <i class="ion-ios-plus-outline float-right"></i>
                              </a>
                          </h4>
                      </div>
                      <div id="collapse<?php echo $faq['Faq']['id']; ?>" class="collapse <?php if($fCount==1){ echo 'show'; } ?>" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-block pt-0">
                          <p class=" font-14 text-gray"><?php echo $faq['Faq']['description']; ?></p>
                        </div>
                      </div>
                  </div>

                <?php
                  $fCount++;
                  } ?>
            </div>
        </div>
      </section>
