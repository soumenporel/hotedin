<!--  inner  slider   -->

      <section class="home-slider inner-banner">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
               <div class="carousel-inner" role="listbox">
                <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?> no-shdow " style="background: url('<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>') no-repeat center center; background-size:cover;" >        
        
                    <div class="carousel-caption">
                    <div class="row align-items-center justify-content-between">
                      <?php echo $banner['Pagebanner']['desc']; ?>

                    </div>
                  </div>
                  </div>
                <?php $bi++; } ?>
              </div>
                <?php //echo $bannerContent['CmsPage']['page_description']; ?>
              </div>
            </div>
      </section>

      <!--    banner bottom area  -->
      <?php echo $bannerMenu['CmsPage']['page_description']; ?>

      <section class="py-5 skill-bg">
        <div class="container">
          <?php echo $pageHeader['CmsPage']['page_description']; ?>
          <div class="row">
            <div class="col-lg-6">
              <form method="post" action="#" >
                <div class="bg-white-transparent p-3 mw-500">
                    <h3 class="zilla font-weight-light mb-3 text-white">Company Information</h3>
                    <div class="form-group row">
                      <label for="example-text-input" class="col-4 col-form-label font-14 text-white">Full Name</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="full_name" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Company Name</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="company_name" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input"  class="col-4 col-form-label font-14 text-white">Company Email</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="company_email" type="email" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input"  class="col-4 col-form-label font-14 text-white">Office Number</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="office_number" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Mobile Number</label>
                      <div class="col-8">
                        <input class="form-control nullBorder" name="mobile_number" type="text" required >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Industry</label>
                      <div class="col-8">
                        <div class="form-group">
                          <select class="form-control nullBorder" name="industry" required >
                            <option value="" >Choose from here</option>
                             <?php foreach($industries as $industry) { ?>
                                <option value="<?php echo $industry['Industry']['id']; ?>"><?php echo $industry['Industry']['name']; ?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">Company Size</label>
                      <div class="col-8">
                        <div class="form-group">
                          <select class="form-control nullBorder" name="company_size" required >
                            <option value="" >Choose from here</option>
                            <option value="10 - 50 persons" >10 - 50 persons</option>
                            <option value="50 - 100 persons" >50 - 100 persons</option>
                            <option value="100 - 500 persons" >100 - 500 persons</option>
                            <option value="500 - 1000 persons">500 - 1000 persons</option>
                            <option value="More than 1000 persons">More than 1000 persons</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="example-search-input" class="col-4 col-form-label font-14 text-white">How Many People Do You Need To Train</label>
                      <div class="col-8">
                        <div class="form-group">
                          <select class="form-control nullBorder" name="how_many_people" required >
                            <option value="" >Choose from here</option>
                            <?php for($i=5;$i<=100;$i=$i+5){ ?>
                             <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                            <?php } ?>
                            
                            <option value="More Than 100" >More Than 100</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group text-right">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                </div>
              </form>
            </div>
            <div class="col-lg-6">
              <?php echo $pageContent['CmsPage']['page_description']; ?>
            </div>
          </div>
        </div>
      </section>

      <!--  testimonials  -->

      <section class="testimonials py-5 bg-faded text-center">
        <div class="container">
            <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"><span class="font-weight-bold" style="color:#f00; "> Testimonials</span>
              <em class="dvdr">
                <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
              </em>
            </h1>
            <div id="carouseltestimonials" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                  <?php $aCount = 1; foreach ($testimonialdata as $key => $testimonial) { ?>
                    <div class="carousel-item <?php if($aCount==1){ echo 'active'; } ?>">
                      <div class="row justify-content-center">
                          <div class="col-lg-8 col-test">
                              <h5 class="font-weight-light"><?php echo $testimonial['Testimonial']['description']; ?></h5>
                              <!-- <div class="test-pic my-3 mx-auto">
                                  <img src="<?php echo $this->webroot;?>user_images/<?php echo $testimonial['User']['user_image']?>" alt="">
                              </div> -->
                              <h5 class="font-weight-bold"><?php echo $testimonial['User']['first_name'].' '.$testimonial['User']['last_name']; ?></h5>
                          </div>
                      </div>
                    </div>
                  <?php $aCount++; } ?>
                </div>
                <ol class="carousel-indicators mt-3">
                  <?php $ti = 0; foreach ($testimonialdata as $key => $testimonial) { ?>
                    <li data-target="#carouseltestimonials" data-slide-to="<?php echo $ti; ?>" class="<?= ($ti==0) ? 'active': ''; ?>"></li>
                  <?php $ti++; } ?>

                </ol>
              </div>
        </div>
      </section>


      <!--   contact  info  -->

      <section class="py-5 contact-info contact-info-govt">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-lg-7 text-white text-center py-lg-3">
                      <h1 class="zilla font-weight-light">Experience A Better learning Platform</h1>
                      <p class="font-weight-light">If you are not willing to learn, no one can help you. If you are determined to learn, no one can stop you. </p>
                      <a href="<?php echo $this->webroot; ?>contact-us" class="btn btn-danger btn-lg">Contact Us Now</a>
                  </div>
              </div>
          </div>
      </section>


      <!--   faq   -->


      <section class="py-5 faq">
        <div class="container">
            <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"> Frequently Asked Question<span class="font-weight-bold" style="color:#f00; "> (FAQ)</span>
              <em class="dvdr">
                <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
              </em>
            </h1>
            <div id="accordion" role="tablist" aria-multiselectable="true">
                <?php
                  $fCount = 1;
                  foreach ($faqs as $key => $faq) { ?>

                  <div class="card mb-3">
                      <div class="card-header border-bottom-0" role="tab" id="headingOne">
                          <h4 class="mb-0">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['Faq']['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['Faq']['id']; ?>" class="d-block text-blue">
                                <span class="font-14"><?php echo $faq['Faq']['title']; ?></span> <i class="ion-ios-plus-outline float-right"></i>
                              </a>
                          </h4>
                      </div>
                      <div id="collapse<?php echo $faq['Faq']['id']; ?>" class="collapse <?php if($fCount==1){ echo 'show'; } ?>" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-block pt-0">
                          <p class=" font-14 text-gray"><?php echo $faq['Faq']['description']; ?></p>
                        </div>
                      </div>
                  </div>

                <?php
                  $fCount++;
                  } ?>
            </div>
        </div>
      </section>
