<div class="rolesAccesses view">
    <h2><?php echo __('Roles Access'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($rolesAccess['RolesAccess']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Role'); ?></dt>
        <dd>
            <?php echo $this->Html->link($rolesAccess['Role']['name'], array('controller' => 'roles', 'action' => 'view', $rolesAccess['Role']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Accessibility'); ?></dt>
        <dd>
            <?php  $accessibility = unserialize($rolesAccess['RolesAccess']['accessibility']); 
                    foreach ($accessibility as $key => $value) {
                        echo $value.'<br>';
                    }
                    //echo '<pre>'; print_r($accessibility); echo '</pre>';
            ?>
            &nbsp;
        </dd>
    </dl>
</div>
