<div class="rolesAccesses form">
    <?php echo $this->Form->create('RolesAccess'); ?>
    <fieldset>
        <legend><?php echo __('Add Roles Access'); ?></legend>
        <?php
        echo $this->Form->input('role_id', array('class'=>'selectbox2'));
        //echo $this->Form->input('accessibility');
        echo $this->Form->input('accessibility', array(
            'type' => 'select',
            'multiple' => 'checkbox',
            'options' => array(
                
                'setting'=>'Setting',
                'cms' => 'CMS_Pages',
                'content' => 'Content',
                'faq' => 'FAQ',
                'banner' => 'Banner',
                'blog' => 'Blog',
                'email_template' => 'Email_Template',
                'normal_users' => 'Users',
                'categories' => 'Categories',
                'course' => 'Course',
                'discussionforum' => 'Discussions',
                'membershipplan' => 'Membership plan',
                'membershipitem' => 'Membership item',
                'contact_us' => 'Contact US',
                'seo_keyword'=> 'SEO Keyword',
                
                'sitemap' => 'Sitemaps',
                'analytics' => 'Analytics',
                'seo_url' => 'SEO URL',
                'newsletter' => 'Newsletter',
                'social_media' => 'Social media',
                'testimonial' => 'Testimonial',
                'bank' => 'Bank',
                'bank_payment' => 'Bank Payment',
                'rating' => 'Rating'
              
            )
        ));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>

<style>
	select {padding:5px;}
</style>