<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
<style>
.dropdown-menu .open{
        max-height: 50px;
}
</style>
<?php
$accessibility = unserialize($this->request->data['RolesAccess']['accessibility']);
?>
<div class="rolesAccesses form">
    <?php echo $this->Form->create('RolesAccess'); ?>
    <fieldset>
        <legend><?php echo __('Edit Roles Access'); ?></legend>
        <h3>Edit Group Members of <?php echo $this->request->data['Role']['name']; ?> Group</h3>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('role_id',array('hidden'=>'hidden','label'=>false)); ?>
        <div class="input select">
            <label for="PostUserId">User</label>
            <select name="data[User][user_id][]" required="required" id="UserId" class="selectpicker" multiple>
                <?php foreach ($users as $key => $user) {?>
                    <option value="<?php echo $user['User']['id'];?>" <?php if(in_array($user['User']['id'], $members_ids)){ echo 'selected'; }?> ><?php echo $user['User']['first_name'].' '.$user['User']['last_name'];?></option>
                <?php } ?>
            </select>
        </div>
        <?php 
        echo $this->Form->input('accessibility', array(
            'type' => 'select',
            'class'=>'bid_pad',
            'multiple' => 'checkbox',
            'selected' => $accessibility,
            'options' => array(
                
                'setting'=>'Setting',
                'cms' => 'CMS_Pages',
                'content' => 'Content',
                'faq' => 'FAQ',
                'banner' => 'Banner',
                'blog' => 'Blog',
                'email_template' => 'Email_Template',
                'normal_users' => 'Users',
                'categories' => 'Categories',
                'course' => 'Course',
                'discussionforum' => 'Discussions',
                'membershipplan' => 'Membership plan',
                'membershipitem' => 'Membership item',
                'contact_us' => 'Contact US',
                'seo_keyword'=> 'SEO Keyword',
                
                'sitemap' => 'Sitemaps',
                'analytics' => 'Analytics',
                'seo_url' => 'SEO URL',
                'newsletter' => 'Newsletter',
                'social_media' => 'Social media',
                'testimonial' => 'Testimonial',
                 'bank' => 'Bank',
                'bank_payment' => 'Bank Payment',
                'rating' => 'Rating'
                
            )
        ));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>


<style>
	select {padding:5px;}
</style>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap-select.js"></script>