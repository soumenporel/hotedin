<section class="inner_content">
    <div class="container">

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="heading_part">
                    <h2>Checkout</h2>
                </div>
            </div>
        </div>
        <div class="shopping-cart">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="shopping-cart-left">
                        <h3><span id="cartCnt">1.</span> Billing</h3>

                        <div class="shopping-cart-area">
                            <form class="form-inline">
                                <div class="form-group">
                                    <label class="pull-left">
                                        <input type="radio" name="newCardRadio" id="newCardRadio" value="1" />
                                        Pay with new card
                                    </label>
                                    <img class="img-responsive pull-left" src="/team4/learnfly/images/paypal-pic.jpg">
                                </div>
                            </form>


                            <form class="form-horizontal hidden" id="ccform">
                                <div class="form-group">
                                    <label for="ccfname" class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-5">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <input type="Name on Card" class="form-control" id="ccfname" name="ccfname" placeholder="First name" value="<?php echo!empty($savedCard['SavedCard']['first_name']) ? $savedCard['SavedCard']['first_name'] : ''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cclname" class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-5">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <input type="Name on Card" class="form-control" id="cclname" name="cclname" placeholder="Last name" value="<?php echo!empty($savedCard['SavedCard']['first_name']) ? $savedCard['SavedCard']['last_name'] : ''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ccnum" class="col-sm-2 control-label">Card Number</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="ccnum" name="ccnum" maxlength="16" placeholder="4242424242424242" value="<?php echo!empty($savedCard['SavedCard']['cc_number']) ? $savedCard['SavedCard']['cc_number'] : ''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ccexp" class="col-sm-2 control-label">Expires on</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="ccexp" name="ccexp" placeholder="MM/YY" value="<?php echo!empty($savedCard['SavedCard']['expire']) ? $savedCard['SavedCard']['expire'] : ''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cccvv" class="col-sm-2 control-label">Security Code</label>
                                    <div class="col-sm-5">
                                        <input type="password" class="form-control" id="cccvv" name="cccvv" placeholder="456" maxlength="3" />
                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cccountry" class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="cccountry" name="cccountry" placeholder="India" value="<?php echo!empty($savedCard['SavedCard']['country']) ? $savedCard['SavedCard']['country'] : ''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ccpcode" class="col-sm-2 control-label">Postal Code</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="ccpcode" name="ccpcode" placeholder="Postal Code" value="<?php echo!empty($savedCard['SavedCard']['zip']) ? $savedCard['SavedCard']['zip'] : ''; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="remberThisCard" value="1" /> Remember this card
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default">Use This Card</button>
                                    </div>
                                </div>
                            </form>
                            <p>or</p>
                            <form class="form-inline">
                                <div class="form-group pull-left">
                                    <label class="pull-left">
                                        <input type="radio" name="payPalRadio" id="payPalRadio" value="1" />
                                        Pay with
                                    </label>
                                    <img class="img-responsive pull-left" src="/team4/learnfly/images/paypall.png">
                                </div>
                                <div class="lock-area pull-right"><p><i class="fa fa-lock" aria-hidden="true"></i>
                                        Secure Connection</p></div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                        <div class="confirm-pay">
                            <h3 class="pull-left">2. Confirm and Pay</h3>
                            <p class="pull-right"><button type="submit" class="btn btn-default">Edit Cart</button></p>
                            <div class="clearfix"></div>
                        </div>

                        <div class="shopping-cart-area">
                            <ul class="saveLaterParent">
                                <?php
                                $image = end($cart['PostImage']);
                                ?>
                                <li class="saveLaterListing">
                                    <div class="shopping">

                                        <div class="media">
                                            <div class="media-left">
                                                <img class="img-responsive" src="<?php echo $this->webroot . 'img/post_img/' . $image['originalpath']; ?>" />
                                            </div>
                                            <div class="media-body"> 
                                                <h4 class="media-heading"><?php echo $cart['Post']['post_title']; ?></h4>
                                                <p>Lorem Ipsum</p>
                                                <!--<h3>Advance CakePHP Course</h3>
                                                <h5>By NITS Ashis</h5>-->
                                            </div>
                                        </div>

                                        <div class="shopping-cart-price-area">
                                            <ul>

                                                <li><strong>$<?php echo $cart['Post']['price']; ?></strong></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                </li>
                            </ul>

                        </div>



                        <div class="complete-payment-area">
                            <div class="media">
                                <div class="media-left">
                                    <button type="submit" class="btn btn-default cpltPmnt" disabled>Complete Payment</button>
                                    <button class="btn btn-default hidden checkoutPaypal">Checkout with PayPal</button>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        Total:
                                        <span class="blue-text">$<?php echo $price; ?></span>
                                    </h4>
                                    By completing your purchase, you agree to these Terms of Service .
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="shopping-cart-right">
                        <h3>Total:</h3>
                        <p>$<span id="tot_price"><?php echo $price; ?></span></p>
                        <div class="off-text">
                            <p> 
                                <span style="text-decoration: line-through; font-size:18px; font-weight:400; color:#333;">
                                    $<span id="tot_old_price"><?php echo $old_price; ?></span>
                                </span> 
                                <span id="tot_percentage" style="font-size:18px; font-weight:400; color:#333;"><?php echo 100 - ceil(($price * 100) / $old_price) ?></span>
                                <span style="font-size:18px; font-weight:400; color:#333;">% off</span>
                            </p>
                        </div>

                        <p>
                            <button class="btn btn-default btn-lg cpltPmnt" disabled>Complete Payment</button>
                            <button class="btn btn-default btn-lg hidden checkoutPaypal">Checkout with PayPal</button>
                        </p>

                    </div>
                    <!--<div class="click-area">
                        <p>Click on a coupon code to filter</p>
                        <h4>HOLIDAY15D <span>is applied</span></h4>
                    </div>-->
                </div>
            </div>
        </div>

    </div>
</section>

<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paymentform" name="paymentform">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="nits.arpita@gmail.com" />
    <input type="hidden" name="currency_code" value="USD" />
    <input type="hidden" name="notify_url" value="<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'takethis_course_notify_url'), TRUE); ?>">
    <input type="hidden" name="return" value="<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'thankyou'), TRUE); ?>" />
    <input type="hidden" name="cancel_return" value="<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'index', 'payment_cancelled'), TRUE); ?>" />
    
    <input type="hidden" name="item_name" value="<?php echo $cart['Post']['post_title']; ?>" />
    <input type="hidden" name="amount" value="<?php echo $cart['Post']['price']; ?>" />
    <input type="hidden" name="custom" value="<?php echo $userid . '&&' . $cart['Post']['id']; ?>" />
    <input type="hidden" name="rm" value="2">
</form>

<script>
    $(function () {
        $('#newCardRadio, #payPalRadio').prop('checked', false);
        
        $('#newCardRadio').click(function(){
            $('#payPalRadio').prop('checked', false);
            $('#ccform').removeClass('hidden');
            $('.cpltPmnt').removeClass('hidden');
            $('.checkoutPaypal').addClass('hidden');
        });
        
        $('#payPalRadio').click(function(){
            $('#newCardRadio').prop('checked', false);
            $('#ccform').addClass('hidden');
            $('.cpltPmnt').addClass('hidden');
            $('.checkoutPaypal').removeClass('hidden');
        });
        
        $('.checkoutPaypal').click(function(){
            $('#paymentform').submit();
        });
        
        
        $('#ccform').submit(function () {
            var ccfname = $('#ccfname').val();
            var cclname = $('#cclname').val();
            var ccnum = $('#ccnum').val();
            var ccexp = $('#ccexp').val();
            var cccvv = $('#cccvv').val();
            var cccountry = $('#cccountry').val();
            var ccpcode = $('#ccpcode').val();
            if (ccfname != '' && cclname != '' && ccnum != '' && ccexp != '' && cccvv != '' && cccountry != '' && ccpcode != '') {
                $('.cpltPmnt').prop('disabled', false);
            } else {
                $('.cpltPmnt').prop('disabled', true);
            }
            return false;
        });

        $('.cpltPmnt').click(function () {
            var ccfname = $('#ccfname').val();
            var cclname = $('#cclname').val();
            var ccnum = $('#ccnum').val();
            var ccexp = $('#ccexp').val();
            var cccvv = $('#cccvv').val();
            var cccountry = $('#cccountry').val();
            var ccpcode = $('#ccpcode').val();
            var remberThisCard = 0;
            if ($('#remberThisCard').is(':checked')) {
                remberThisCard = 1;
            }
            if (ccfname != '' && cclname != '' && ccnum != '' && ccexp != '' && cccvv != '' && cccountry != '' && ccpcode != '') {
                $.ajax({
                    url: '<?php echo $this->webroot; ?>checkouts/takethis_course_ccpayment',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        ccfname: ccfname,
                        cclname: cclname,
                        ccnum: ccnum,
                        ccexp: ccexp,
                        cccvv: cccvv,
                        cccountry: cccountry,
                        ccpcode: ccpcode,
                        amt: '<?php echo $price; ?>',
                        postid: '<?php echo $cart['Post']['id']; ?>',
                        remberThisCard: remberThisCard
                    },
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    success: function (data) {
                        $('#loading').hide();
                        if (data.ack == '1') {
                            window.location.href = '<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'thankyou'), true); ?>';
                        } else {
                            alert('Payment failed');
                        }
                    }
                });
            } else {
                $('.cpltPmnt').prop('disabled', true);
            }
        });
    })
</script>