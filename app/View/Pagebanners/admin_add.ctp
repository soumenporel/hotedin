<?php ?>
<script>
    // $(document).ready(function () {
    //     $("#BlogAdminAddForm").validationEngine();
    // });
</script>
<style>
.error-modal {
    background: red;
 }
</style>
<div class="blogs form">
<?php echo $this->Form->create('Pagebanner', array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Banner'); ?></legend>
        
        <?php
        echo $this->Form->input('page_id',array('empty' => '(choose page)', 'label' => 'Page','required'=>'required' ,'class'=>'selectbox2'));
        echo $this->Form->input('image', array('type' => 'file', 'id'=>'BannerImageNew',array('required'=>'required')));?>
        <div class="input file" >
            <span style="color:red;font-size:12px;">* Image Type Should be .JPG,.JPEG,.PNG,.GIF.</span><br>
            <span style="color:red;font-size:12px;">**The Image Resolution Should be 1920x750.</span>
        </div>
        <?php
        echo $this->Form->input('title',array('required'=>'required'));
        echo $this->Form->input('desc',array('label' => 'Description', 'id' => 'banner_desc'));
        echo $this->Form->input('status',array('label' => 'Is Active'));
        echo $this->Form->input('order', array('style' => 'width:50%','default' => 0));
	?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="modal fade" id="alertModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header error-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Image Verification Error</h4>
        </div>
        <div class="modal-body">
          <p id="alertMessage"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('banner_desc',
            {
                width: "95%"
            });

var _URL = window.URL || window.webkitURL;    

function isSupportedBrowser() {
    return window.File && window.FileReader && window.FileList && window.Image;
}

function getSelectedFile() {
    var fileInput = document.getElementById("BannerImageNew");
    var fileIsSelected = fileInput && fileInput.files && fileInput.files[0];
    if (fileIsSelected)
        return fileInput.files[0];
    else
        return false;
}




$("#BannerImageNew").change(function(event) {
    var form = this;
    
    if (isSupportedBrowser()) {
        event.preventDefault(); //Stop the submit for now

        var file = getSelectedFile();
        if (!file) {
            alert("Please select an image file.");
            return;
        }
        
     
    }
});

function validateImageBeforeUpload(el) {
    var types = ['image/jpeg', 'image/gif', 'image/png']
    var imageFile = el;
    
    if(types.indexOf(imageFile.files[0].type) != -1) {
        var fileSize = imageFile.files[0].size;
        fileSize = fileSize / (1024*1024);
        if (fileSize > 2) {
            imageFile.value = '';
            alert('Maximum filesize is 2MB');
        }
    } else {
        imageFile.value = '';
        alert('Invalid image type');
    }
}
    
</script>

