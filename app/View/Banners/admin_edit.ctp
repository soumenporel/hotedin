<?php ?>
<script>
    // $(document).ready(function () {
    //     $("#BlogAdminEditForm").validationEngine();
    // });
</script>
<style>
.error-modal {
    background: red;
 }
</style>
<div class="blogs form">
<?php echo $this->Form->create('Banner',array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Banner'); ?></legend>
        <?php
        if($this->request->data['Banner']['image'] != '') {
        ?>
        <div style="width: 100%;">
            <img src="<?php echo $this->webroot.'banner/'.$this->request->data['Banner']['image'] ?>" style="width: 100%;" />
        </div>
        <?php
        }
        ?>
	<?php
        echo $this->Form->input('id');
        echo $this->Form->input('image', array('type' => 'hidden', 'name' => 'saved_image'));
        echo $this->Form->input('image', array('type' => 'file','id'=>'BannerImageNew'));?>
        <div class="input file" >
            <span style="color:red;font-size:12px;">* Image Type Should be .JPG,.JPEG,.PNG,.GIF.</span><br>
            <span style="color:red;font-size:12px;">**The Image Resolution Should be 1920x750.</span>
        </div>
        <?php
        echo $this->Form->input('title',array('required'=>'required'));
        echo $this->Form->input('desc',array( 'label' => 'Description', 'id' => 'banner_desc'));
        echo $this->Form->input('status');
        echo $this->Form->input('order', array('default' => 0));
	?>

    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<div class="modal fade" id="alertModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header error-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Image Verification Error</h4>
        </div>
        <div class="modal-body">
          <p id="alertMessage"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('banner_desc',
            {
                width: "95%"
            });

var _URL = window.URL || window.webkitURL;    

function isSupportedBrowser() {
    return window.File && window.FileReader && window.FileList && window.Image;
}

function getSelectedFile() {
    var fileInput = document.getElementById("BannerImageNew");
    var fileIsSelected = fileInput && fileInput.files && fileInput.files[0];
    if (fileIsSelected)
        return fileInput.files[0];
    else
        return false;
}

function isGoodImage(file) {
    var deferred = jQuery.Deferred();
    var image = new Image();
    
    image.onload = function() {
        // Check if image is bad/invalid
        if (this.width + this.height === 0) {
            this.onerror();
            return;
        }
        
        // Check the image resolution
        if (this.width == 1366 && this.height == 554) {
            deferred.resolve(true);
        } else {
            //alert("The Image Resolution Should be 1920x750.");
            $('#alertMessage').text("The Image Resolution Should be 1920x750.");
            deferred.resolve(false);
        }
    };
    
    image.onerror = function() {
        //alert("Invalid image. Please select an image file.");
        $('#alertMessage').text("Invalid image. Please select an image file.");
        deferred.resolve(false);
    }
    
    image.src = _URL.createObjectURL(file);
    
    return deferred.promise();
}


$("#BannerImageNew").change(function(event) {
    var form = this;
    
    if (isSupportedBrowser()) {
        event.preventDefault(); //Stop the submit for now

        var file = getSelectedFile();
        if (!file) {
            alert("Please select an image file.");
            return;
        }
        
        isGoodImage(file).then(function(isGood) {
            if (!isGood)
                //form.submit();
            $("#BannerImageNew").val('');
            $('#alertModal').modal('show');
            return;
        });
    }
});


    
</script>

