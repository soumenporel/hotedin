<style>
.success-modal {
    background: green;
 }
</style>
<div class="faqs index wp_pages_index">
    <h2><?php echo __('Banners'); ?></h2>
    <table style="width:100%;border:0px solid red;">
        <tr>
            <td style="width:70%;border:0px solid red;">&nbsp;</td>
            <td style="border:0px;">
                <a href="<?php echo($this->webroot);?>admin/banners/add" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New Banner</a>
            </td>      
        </tr>
        </tr>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th>Set Banners</th>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('image'); ?></th>
                <th><?php echo $this->Paginator->sort('title'); ?></th>
                <th><?php echo $this->Paginator->sort('description'); ?></th>
                <th><?php echo $this->Paginator->sort('status'); ?></th>
                <th><?php echo $this->Paginator->sort('order'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
	<?php
        if(!empty($banners)) :
            foreach ($banners as $key => $banner) :
        ?>
            <tr>
                <td><input type="checkbox" class="banner-class" name="select_banner" value="<?php echo $banner['Banner']['id']; ?>" <?php if($banner['Banner']['status']==1){ echo '';} ?>  ></td>
                <td width="4%">
                    <?php
                    echo ++$key;
                    ?>
                </td>
                <td width="11%">
                    <?php
                    $uploadFolder = "banner/";
                    $uploadPath = $this->webroot . $uploadFolder;
                    ?>
                    <img src="<?php echo ($banner['Banner']['image'] != '') ? $uploadPath . $banner['Banner']['image'] : ''; ?>" width="100" />
                </td>
                <td width="20%">
                    <?php
                    echo h($banner['Banner']['title']);
                    ?>
                </td>
                <td width="30%">
                    <?php
                    echo h($banner['Banner']['desc']);
                    ?>
                </td>
               <td><?php if($banner['Banner']['status']==1){ ?> <img src="<?php echo $this->webroot; ?>/img/success-01-128.png" style="height:30px;" /><?php }else{ ?><img src="<?php echo $this->webroot; ?>/img/cross-512.png" style="height:30px;" /><?php } ?>&nbsp;</td>
                <td>
                    <?php
                    echo h($banner['Banner']['order']);
                    ?>
                </td>
                <td >
                            <?php //echo $this->Html->link(__('View'), array('action' => 'view', $banner['Banner']['id'])); ?>
                            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                        array('action' => 'edit', $banner['Banner']['id']),
                        array('class' => 'btn btn_circle btn-edit', 'escape'=>false)); ?>
                            
                            <?php echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                        array('action' => 'delete', $banner['Banner']['id']),
                        array('class' => 'btn btn_circle btn-del', 'escape'=>false),
                        __('Are you sure you want to delete # %s?', $banner['Banner']['id'])); ?>
                </td>
            </tr>
        <?php
            endforeach;
        else :
        ?>
            <tr>
                <td colspan="7"><?php echo __('No Banner found'); ?></td>
            </tr>
        <?php
        endif;
        ?>
        </table>
        <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
        <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
        </div>
</div>

<div class="modal fade" id="alertModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header success-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Set Banner Image</h4>
        </div>
        <div class="modal-body">
          <p id="ajaxResponse"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<?php //echo($this->element('admin_sidebar'));?>
<script>
$(document).ready(function() {
    $(".banner-class1").click(function(){
        b_id = $(this).val();

        if ($(this).prop('checked')==true){ 
           var stat = 1;
        }else {
           var stat = 0;            
        }
        
        console.log(b_id);
        $.ajax({
                url: "<?php echo $this->webroot;?>banners/ajaxBannerSelect",
                data:{
                    banner_id:b_id,
                    status:stat
                },
                dataType:'json',
                type: 'POST',     
                success: function(result){
                    //console.log(result);
                    if(result.Ack==1){
                        $('#ajaxResponse').text(result.res);
                        $('#alertModal').modal('show');
                    }
                    else{
                        $('#ajaxResponse').text(result.res);
                        $('#alertModal').modal('show');
                    }
                }
        });
    });
});


</script>