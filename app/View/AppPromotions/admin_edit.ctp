<div class="categories form">
<?php
echo $this->Form->create('AppPromotion',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('App Promotion'); ?></legend>

	    <?php
    		echo $this->Form->input('id');
            echo $this->Form->input('points',array('type' => 'hidden'));
            echo $this->Form->input('hide_image',array('type' => 'hidden','value'=>isset($this->request->data['AppPromotion']['image'])?$this->request->data['AppPromotion']['image']:''));
            echo $this->Form->input('hide_android',array('type' => 'hidden','value'=>isset($this->request->data['AppPromotion']['android'])?$this->request->data['AppPromotion']['android']:''));
            echo $this->Form->input('hide_ios',array('type' => 'hidden','value'=>isset($this->request->data['AppPromotion']['ios'])?$this->request->data['AppPromotion']['ios']:''));
    		echo $this->Form->input('title',array('required'=>'required', 'label'=>' Title'));
            echo $this->Form->input('description',array('label'=>' Description'));
            ?>
            <span>Points :</span><br>No. 1<input num="1" id="addpoints" type="text" name="dat[1]" value="">
              <button type="button" id="points" name="button">Add More</button>
            <?php
    		echo $this->Form->input('image',array('type'=>'file'));
            echo $this->Form->input('android',array('type'=>'file', 'label'=>'Android App Store' ));
            echo $this->Form->input('ios',array('type'=>'file', 'label'=>'IOS App Store'));
            echo $this->Form->input('active',array('type'=>'checkbox'));
            ?>
            <div>
                <?php
                $uploadImgPath = WWW_ROOT.'appPromotions_image';
                if( $this->request->data['AppPromotion']['image']!='' && file_exists($uploadImgPath . '/' .  $this->request->data['AppPromotion']['image'])){
                ?>
                <p>Image :</p><img alt="" src="<?php echo $this->webroot;?>appPromotions_image/<?php echo $this->request->data['AppPromotion']['image'];?>" style=" height:80px; width:80px;">
                <?php
                }
                else{
                ?>
                    <img alt="noimage" src="<?php echo $this->webroot;?>img/noimage.png" style=" height:80px; width:80px;">

                <?php } ?>
            </div>
	</fieldset>

<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
      var count = 1;
      var points = $('#AppPromotionPoints').val();
      var splitedPoints = points.split(',');
      var flag = 0
      for(var i in splitedPoints) {
          flag++;count++;
          if(flag < 2){
              $('#addpoints').val(splitedPoints[i]);
          } else {
              var j = parseInt(i)+1;
              var htm = '<br>No. '+j+'<input id="addpoints" type="text" name="dat['+j+']" value="'+splitedPoints[i]+'">';
                    $('#addpoints').after(htm);
                    $('#addpoints').attr('id','');
          }

      }


        $('#points').click(function(event) {
            count++;
            var htm = '<br>No. '+count+'<input id="addpoints" type="text" name="dat['+count+']" value="">';
            $('#addpoints').after(htm);
            $('#addpoints').attr('id','');
        });

    });

</script>
