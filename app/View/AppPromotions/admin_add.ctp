<div class="categories form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('AppPromotion',array('enctype'=>'multipart/form-data'));
?>
	<fieldset>
		<legend><?php echo __('App Promotion'); ?></legend>

            <?php
              echo $this->Form->input('title',array('required'=>'required', 'label'=>'Title'));
              echo $this->Form->input('description',array('label'=>'Description')); ?>
              <span>Points :</span><br>No. 1<input num="1" id="addpoints" type="text" name="dat[1]" value="">
              <button type="button" id="points" name="button">Add More</button>
              <?php
		      echo $this->Form->input('image',array('type'=>'file'));
              echo $this->Form->input('android',array('type'=>'file', 'label'=>'Android App Store' ));
              echo $this->Form->input('ios',array('type'=>'file', 'label'=>'IOS App Store'));
              echo $this->Form->input('active',array('type'=>'checkbox'));
                ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

<script type="text/javascript">
    jQuery(document).ready(function($) {
      var count = 1;
        $('#points').click(function(event) {
            count++;
            var htm = '<br>No. '+count+'<input id="addpoints" type="text" name="dat['+count+']" value="">';
            $('#addpoints').after(htm);
            $('#addpoints').attr('id','');
        });
    });
</script>
