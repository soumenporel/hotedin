<div class="roles view">
    <h2><?php echo __('Membership Item'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($plan_items['MembershipItem']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($plan_items['MembershipItem']['name']); ?>
            &nbsp;
        </dd>
    </dl>
</div>