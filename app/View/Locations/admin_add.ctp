<div class="categories form">
<form name="user_location" method="post" action="" id="user_location">   
    <div class="input select">
    	<label for="user_id">Users:</label>
             <select name="data[User][id]" class="selectbox2" id="user_id" required>
                    <?php foreach ($all_users as $key => $value) { ?>
                    <option value="<?php echo $value['User']['id']; ?>" ><?php echo $value['User']['first_name'].' '.$value['User']['last_name'] ?></option>
                    <?php } ?>
             </select>
        </label>
    </div>
    <div class="input select">
    	<label for="country_id">Country:</label>
             <select name="data[User][country]" class="selectbox2" id="country_id" required>
             		<?php foreach ($countries as $key => $value) { ?>
                    <option value="<?php echo $value['Country']['id']; ?>" ><?php echo $value['Country']['name']; ?></option>
                    <?php } ?>
             </select>
        </label>
    </div>
    <div class="input select">
    	<label for="state_id">State:</label>
             <select name="data[User][state]" id="state_id" class="selectbox2">
                    <option value="" >Select State</option>
             </select>
        </label>
    </div>
    <div class="input select">
    	<label for="city_id">City:</label>
             <select name="data[User][city]" id="city_id" class="selectbox2">
                    <option value="" >select City</option>
            </select>
        </label>
    </div>
    <div class="input text">
    	<label for="zip_code">Zipcode</label>
    	<input name="data[User][zip]" maxlength="255" class="zip_code" type="text"  id="zip_code" required>
    </div>

         
            	<input type="submit" value="Submit">
            
</form> 
</div>

<script>
$(document).ready(function(){
    $("#country_id").change(function(){
        var country_id = $(this).val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>states/ajaxStates",
            type: 'post',
            dataType: 'json',
            data: {
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#state_id').html(result.html);
                } else {
                    $('#state_id').html(result.html);
                }
            }
        });
    });

    $("#state_id").change(function(){
        var state_id = $(this).val();
        var country_id = $("#country_id").val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>cities/ajaxCities",
            type: 'post',
            dataType: 'json',
            data: {
                s_id:state_id,
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#city_id').html(result.html);
                } else {
                    $('#city_id').html(result.html);
                }
            }
        });
    });
});
</script>    