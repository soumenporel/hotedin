<div class="contents form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('EmployerMembershipPlan'); ?>
	<fieldset>
		<legend><?php echo __('Edit Plans'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
	?>

		<div class="input number">
			<label for="MembershipPlanDuration">Duration</label>
			<input name="data[EmployerMembershipPlan][duration]" type="number" value="<?php echo $this->request->data['EmployerMembershipPlan']['duration']; ?>" id="MembershipPlanDuration">
			<select name="data[EmployerMembershipPlan][duration_unit]" required="required" class="selectbox2" id="MembershipPlanDurationIn">
				<option <?php  if($this->request->data['EmployerMembershipPlan']['duration_unit']=='Month'){ echo 'selected'; } ?> value="Month">Month</option>
				<option <?php  if($this->request->data['EmployerMembershipPlan']['duration_unit']=='Months'){ echo 'selected'; } ?> value="Months">Months</option>
				<option <?php  if($this->request->data['EmployerMembershipPlan']['duration_unit']=='Days'){ echo 'selected'; } ?> value="Days">Days</option>
			</select>
		</div>

	<?php

		echo $this->Form->input('jobs');
		echo $this->Form->input('max_user');
		echo $this->Form->input('price','Price');

?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
