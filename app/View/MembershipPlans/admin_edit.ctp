<div class="contents form">
<?php 
//echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('MembershipPlan'); ?>
	<fieldset>
		<legend><?php echo __('Edit Plans'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
	?>

		<div class="input number">
			<label for="MembershipPlanDuration">Duration</label>
			<input name="data[MembershipPlan][duration]" type="number" value="<?php echo $this->request->data['MembershipPlan']['duration']; ?>" id="MembershipPlanDuration">
			<select name="data[MembershipPlan][duration_in]" required="required" class="selectbox2" id="MembershipPlanDurationIn">
				<option <?php  if($this->request->data['MembershipPlan']['duration_in']=='Month'){ echo 'selected'; } ?> value="Month">Month</option>
				<option <?php  if($this->request->data['MembershipPlan']['duration_in']=='Months'){ echo 'selected'; } ?> value="Months">Months</option>
				<option <?php  if($this->request->data['MembershipPlan']['duration_in']=='Days'){ echo 'selected'; } ?> value="Days">Days</option>
			</select>
		</div>

	<?php

		echo $this->Form->input('MembershipItem',array(
            'label' => __('Membership Items',true),
            'type' => 'select',
            'multiple' => 'checkbox',
            'options' => $membership_items
        ));

		//echo $this->Form->input('duration');
		/*echo $this->Form->input('free_video',array('type'=>'checkbox'));
		echo $this->Form->input('unlimited_learning',array('type'=>'checkbox'));
		echo $this->Form->input('discussion',array('type'=>'checkbox'));
		echo $this->Form->input('assignment',array('type'=>'checkbox'));
		echo $this->Form->input('test',array('type'=>'checkbox'));
		echo $this->Form->input('certificate',array('type'=>'checkbox'));*/

		//echo $this->Form->input('no_of_post');
		//echo $this->Form->input('no_of_marketplace');
		echo $this->Form->input('price');
		// echo $this->Form->input('plan_code');
		
		
?>
<textarea name="data[MembershipPlan][content]" id="Contentcontent" style="width:900px; height:600px;" class="validate[required]" ><?php echo $this->request->data['MembershipPlan']['content']; ?></textarea>
    

	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('Contentcontent',
            {
                width: "95%"
            });
</script>
