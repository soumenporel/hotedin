<div class="contents form">
<?php
echo $this->Form->create('EmployerMembershipPlan'); ?>
	<fieldset>
		<legend><?php echo __('Add Employer Membership Plan'); ?></legend>
	<?php
	    echo $this->Form->input('title');
	?>
		<div class="input number">
			<label for="MembershipPlanDuration">Duration</label>
			<input name="data[EmployerMembershipPlan][duration]" type="number" id="MembershipPlanDuration">
			<select name="data[EmployerMembershipPlan][duration_unit]" required="required" class="selectbox2" id="MembershipPlanDurationIn">
				<option value="Month">Month</option>
				<option value="Months">Months</option>
				<option value="Days">Days</option>
			</select>
		</div>
	<?php
    echo $this->Form->input('jobs');
    echo $this->Form->input('max_user');
    echo $this->Form->input('price','Price');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
