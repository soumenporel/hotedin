<div class="contents index">
	<h2 style="width:100%;float:left;"><?php echo __('Employer Membership Plan'); ?></h2>

    <table cellpadding="0" cellspacing="0">
        <tr><a href="<?php echo($this->webroot);?>admin/MembershipPlans/employeradd" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New Plan</a>
            </tr>
        <tr>
    			<th><?php echo $this->Paginator->sort('id'); ?></th>
    			<th><?php echo $this->Paginator->sort('title'); ?></th>
    			<th><?php echo $this->Paginator->sort('jobs','No of Jobs'); ?></th>
    			<th><?php echo $this->Paginator->sort('max_user','Maximum User'); ?></th>
    			<th><?php echo $this->Paginator->sort('price'); ?></th>
    			<th><?php echo $this->Paginator->sort('duration'); ?></th>
                <th><?php echo $this->Paginator->sort('duration_unit', 'Unit'); ?></th>
    			<th class="actions"><?php echo __('Actions'); ?></th>
    	</tr>
        <?php $sl = 1; foreach ($plans as $plan) : ?>
            <tr>
                <td><?php echo $sl; ?>&nbsp;</td>
                <td><?php echo h($plan['EmployerMembershipPlan']['title']); ?>&nbsp;</td>
                <td><?php echo h($plan['EmployerMembershipPlan']['jobs']);?></td>
                <td><?php echo ($plan['EmployerMembershipPlan']['max_user']);?></td>
                <td><?php echo ($plan['EmployerMembershipPlan']['price']);?></td>
                <td><?php echo ($plan['EmployerMembershipPlan']['duration']);?></td>
                <td><?php echo ($plan['EmployerMembershipPlan']['duration_unit']);?></td>
                <td>
                    <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                                array('action' => 'employeredit', $plan['EmployerMembershipPlan']['id']),
                                array('class' => 'btn btn-info btn-xs', 'escape'=>false)); ?>
                    <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                                array('action' => 'employerdelete', $plan['EmployerMembershipPlan']['id']),
                                array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
                                __('Are you sure you want to delete # %s?', $plan['EmployerMembershipPlan']['id'])); ?>
                </td>
            </tr>
        <?php $sl++; endforeach; ?>
    </table>
    <p>
    <?php
    echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
    ));
    ?>	</p>
    <div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
    </div>
</div>
