<div class="contents form">
<?php 
echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('MembershipPlan'); ?>
	<fieldset>
		<legend><?php echo __('Add Membership Plan'); ?></legend>
	<?php
	    echo $this->Form->input('title');
	?>
		<div class="input number">
			<label for="MembershipPlanDuration">Duration</label>
			<input name="data[MembershipPlan][duration]" type="number" id="MembershipPlanDuration">
			<select name="data[MembershipPlan][duration_in]" required="required" class="selectbox2" id="MembershipPlanDurationIn">
				<option value="Month">Month</option>
				<option value="Months">Months</option>
				<option value="Days">Days</option>
			</select>
		</div>
	<?php

		echo $this->Form->input('MembershipItem',array(
            'label' => __('Membership Items',true),
            'type' => 'select',
            'multiple' => 'checkbox',
            'options' => $membership_items
        ));

	    // echo $this->Form->input('duration');

	    // echo $this->Form->input('free_video',array('type'=>'checkbox'));
		// echo $this->Form->input('unlimited_learning',array('type'=>'checkbox'));
		// echo $this->Form->input('discussion',array('type'=>'checkbox'));
		// echo $this->Form->input('assignment',array('type'=>'checkbox'));
		// echo $this->Form->input('test',array('type'=>'checkbox'));
		// echo $this->Form->input('certificate',array('type'=>'checkbox'));

	    echo $this->Form->input('price');

		// echo $this->Form->input('no_of_post');
		// echo $this->Form->input('no_of_marketplace');
		
		
		//echo $this->Form->input('content');
		echo $this->Form->input('content', array('type' => 'textarea'));

		

	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>