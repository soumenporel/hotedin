<!--<section class="populer_service">
        <div class="container">
		<div class="row">
		        <div class="col-md-12" >
		    			<h2><u>How it works</u></h2>
			</div>
		</div>
		<div class="row" style="width: 75%;margin: 0 auto;">
		        <div class="col-md-12" >
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer.</p>
			</div>
		</div>
            
            
	</div>
</section>-->


<section class="how-baner-section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-7 animated bounceInLeft">
                <div class="intro "><div class="heading"><h1 style=" font-weight: bold;text-shadow: 1px 2px 1px #000;font-size: 48px;">How it Works </h1></div><!-- <div class="sub-heading"><h5 style="font-size: 18px;color: #fff; padding:10px 0 0 0;">To assign an Errand Worker to your errand, simply log in to your Errand Champion account and go to your errand (you can find it in the <a href="<?php echo $this->webroot?>users/dashboard"> "My Account"</a> section.</h5></div> -->
                <!-- <h5 style="font-size: 19px; display:none;
color: #fff;">Go to the 'Offers' section of the page where you will see a summary of offers and the offered prices. After selecting an offer you are happy with, hit the 'view offer' button of that Errand Worker and if you wish to assign the errand click the "Assign Errand" button at the bottom of the dialogue box.<br><br>
The errand is now assigned and you can communicate the Worker using the Errand Champion emails.</h5> -->
                <div class="buttons-heading">
               <!--  <h5>Find out more about:</h5> -->
            </div></div>
                </div>
                <div class="col-md-5">
                <div class="iphone-img animated bounceInRight"></div>
                </div>
            </div>
        </div>
    </section>

     


    <div class="clearfix"></div>
    <section class="body-content">
    <div class="row">
    	<div class="container" style="margin-top:20px; display:none">
            
        	
           <!--  <h1>For Job Posters</h1> -->
            	<div class="col-md-6 animated bounceInLeft" id="test">
                    <div class="post-1-img"><img  style="margin:0 auto;" src="<?php echo $this->webroot?>images/poster.png"></div>
                	<div class="content" style="margin-bottom:25px;">

                        <h3>How to Use Us – Errand Poster </h3>
                        <div class="tagline">
                        <p><i class="fa  icon-c fa-hand-o-right"></i> Create a <strong>free</strong> account using Errand Champion online or on your mobile device.</p>
                        <span id="post_add" style="display: none;">
                        <p><i class="fa icon-c fa-hand-o-right"></i> Post your <strong>free</strong> Ad and tell us about the errands you need completing.</p>
                        <p><i class="fa icon-c fa-hand-o-right"></i> Review multiple applications from our Errand workers. Receive applications and use our community review system to select someone you like.</p>
                        <p><i class="fa icon-c fa-hand-o-right"></i> Agree on a suitable price and deposit the Errand Funds in the Errand Champion holding account which uses PayPal.</p>
                        <p><i class="fa fa-hand-o-right"></i> You can send messages to communicate about the errand details.</p>
                        <p><i class="fa icon-c fa-hand-o-right"></i> Once you are happy the errand has been completed successful our Workers will request Payment and Errand Champion will release the money.</p>
                    </span>
                        <p><button class="btn view_more one" itemid="post_add">View more</button></p>

                        <!-- <a href=""><p>See Task Suggestions</p></a> --></div></div></div>

                        <div class="col-md-2">
                    <div class="post-1-img" style="margin-top: 200px;"><img class="img-responsive" src="<?php echo $this->webroot?>images/hiw-post-1.jpg"></div>
                </div>

                        <div class="col-md-6 animated bounceInRight" id="test-one">
                            <div class="post-1-img"><img style="margin:0 auto;" src="<?php echo $this->webroot?>images/bidder.png"></div>
                    <div class="content" style="margin-bottom:25px;">
                        <h3>How to Use Us – Workers </h3>
                        <div class="tagline">
                        <p><i class="fa fa-hand-o-right"></i> Look through our database of errands and find an errand that’s right for you.  Refine your search based on your location and your skillset.</p>
                        <span id="find_jobs" style="display: none;">
                        <p><i class="fa fa-hand-o-right"></i> Once you have found an errand you want to apply make an offer.</p>
                        <p><i class="fa fa-hand-o-right"></i> If your offer is accepted you will be notified via your Errand Champion notification and be assigned to the errand.</p>
                        <p><i class="fa fa-hand-o-right"></i> Talk to the owner via the message system to arrange suitable completion.  In the meantime we will collect your funds from the Job Poster which will be released once the job has been completed.</p>
                        
                        <p><i class="fa fa-hand-o-right"></i> Once you have completed the errand request payment and Errand Champion admin will release money into your Paypal account.</p>
                    </span>
                        <p><button class="btn view_more" itemid="find_jobs">View more</button></p>
                        <!-- <a href=""><p>See Task Suggestions</p></a> --></div>

                    </div>
                </div>
                
            </div>
        </div>
        <div class="clearfix"></div>



        <!-- <section class="body-content" style="padding:50px 0;">
         <div class="row">
            <div class="container text-center">
                <div class="col-md-12" style="margin:0 auto;">
                <img src="<?php echo $this->webroot;?>images/how.jpg" style="margin:0 auto; width:500px;
                " >
                </div>
            </div>
         </div>
        </section> -->

<section class="back-section">
            <div class="row" style="width:80%; margin:0 auto;">
                <div class="col-md-12 text-center" style="padding:20px 0;">
                    <h1 style="color:#fff; font-size:40px;">Getting started as an Errand Owner</h1>
                    <p style="color:#fff;">The process of getting started on this site is easy and fast.</p>
                </div>
                <div class="row" style="padding:0 0 30px 0;">
                 <div class="col-md-6 text-center">
                     <img class="img-responsive" src="<?php echo $this->webroot;?>images/iPhone_5S.png" style="margin:0 auto;" >
              </div>

              <div class="col-md-6" style="color:#fff;">
                     
                                          
                                            
                         <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-mobile"></i>
                    <div class="float-text">
                       <h2>Post Your Errand</h2>
                       <h4>Breeze through your to-do list and tell us about what you need to get done.  Post as many errands as you want.</h4>
                    </div>
                </div>

                         <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-clipboard"></i>
                    <div class="float-text">
                        <h2>Name your Price or Review Bidding</h2>
                       <h4>Watch as the offers roll in and agree on a suitable price. See our pricing section below for some tips.</h4>
                    </div>
                </div>

                         <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-check"></i>
                    <div class="float-text">
                         <h2>Friendly Help is on it’s Way!</h2>
                       <h4>Zero in on our shortlist and use our review system and direct messaging platform to select the right person for you.</h4>
</div> </div>


                        <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-shield"></i>
                    <div class="float-text">
                         <h2>Make Secure Payment</h2>
                       <h4>Use our site to make a secure payment via PayPal.  Once you are happy the Errand is complete we will release the funds.  It’s as simple as that.</h4>
                    </div>
                </div>

                       <!--  <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-bookmark"></i>
                    <div class="float-text">
                        <h4>You can send messages to communicate about the errand details.</h4>
                    </div>
                </div> -->

                       

              </div>
          </div>
      </div>
             
        </section>




        

       <!--  <div class="grey-bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-4">
                    	<div class="post-img"><img src="<?php echo $this->webroot?>images/mail.png" alt=""/></div>
                    </div>
                    <div class="col-md-8">
                    <div class="txt"><h3>How to send private message</h3><div class="tagline"><p>Private messaging is only possible between the Job Poster and the Worker once the errand is

assigned.<br>
Once an errand is assigned the two Errand Champion Members can communicate about the errand e.g. exchanging personal details as phone numbers, addresses etc.
<br><strong>Privacy protection is very important to us. Our software ensures that your information is safe and no 
spam can reach your account.</strong>

</p></div></div>
                    </div>
                </div>
            </div>
        </div> -->
        
        <!-- <div class="container">
        	<div class="row">
            	<div class="col-md-8">
                	<div class="content" style="margin-bottom:50px;">
                        <h3>How does an errand become overdue</h3>
                        <div class="tagline">
                        <p>Once an errand has passed the deadline date given by the Errand Poster and the Errand Worker has not yet completed the errand overdue.<br>
As an Errand Poster ensure that your assigned Errand Worker completes the errand on time. Use private messaging to follow up with the assigned Errand Worker if the errand remains overdue.</p>

                        </div>

                      <h3>Best way to ensure your errand gets done</h3>
                        <div class="tagline">
                        <p>Provide as much information as possible about the errand and offer a price that is fair for the job.<br>
To get the best price and the best Errand Worker for your job, we suggest you to spread word about your errand on social networks. It will ensure more people as possible seeing your errand. It’s really easy just use the share button.</p>

                         <a href=""><p>See Task Suggestions</p>
                    </a> --></div>



                </div>
                </div>
                <!-- <div class="col-md-4">
                	<div class="post-1-img" style="margin-top:100px;"><img src="<?php echo $this->webroot?>images/post-img-3.png" alt=""/></div>
                </div> 
            </div>
        </div> -->
        <div class="clearfix"></div>



          <div class="grey-bg" style="background: #f36800;">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-6">
                    <div class="txt"><h3>How much it cost to use ErrandChampion?</h3><div class="tagline"><p>You decide. You name your price and all transactions are done easily and securely through your ErrandChampion account.

</p></div></div>
                    </div> -->
                    <div class="col-md-12 text-center">
                        <i class="fa icon-ca fa-usd"></i>
                    <div class="txt" style="margin: 25px auto 0; color: #fff;"><h3>Payments to Errand Champion
</h3><div class="tagline"><p>It’s free to join and to post as many errands as you like.

</p>
<p>It’s up to you how much you pay or get paid – agree on a suitable price for Errand Completion. A flat 10% of each Errand Price is reserved for our Service Fee.</p>
</div></div>
                    </div>
                </div>
            </div>
        </div>



        <div class="clearfix"></div>
        <section class="back-sectiontwo">
            <div class="row" style="width:80%; margin:0 auto;">
                <div class="col-md-12 text-center" style="padding:20px 0;">
                    <h1 style="color:#fff; font-size:40px;">How do you get started as an Errand Champion?</h1>
                </div>
                <div class="row" style="padding:0 0 30px 0;">
               

              <div class="col-md-6" style="color:#fff;">
                     
                                          
                                            
                         <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-thumbs-up"></i>
                    <div class="float-text">
                        <h2>Select the Errand Right for You</h2>
                       <h4>Browse through the wide range of errands and zero in on the ones that match your skills and experience.</h4>
                    </div>
                </div>

                         <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-shield"></i>
                    <div class="float-text">
                        <h2>Begin Bidding and Agree on Price</h2>
                       <h4>Talk directly to Errand Owners to thrash out any details. Errand Champion will notify you on selection and assign you to the errand.</h4>
                    </div>
                </div>

                         <!-- <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-bell"></i>
                    <div class="float-text">
                         <h4>If your offer is accepted you will be notified via your Errand Champion notification and be assigned to the errand.</h4>
</div> </div> -->


                        <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-envelope"></i>
                    <div class="float-text">
                        <h2>Complete the Errand and Let us Know</h2>
                         <h4>Don’t worry, Errand Champion will hold the agreed payment and release the payment once the errand has been completed. </h4>
                    </div>
                </div>

                        <div class="float-box" style="padding:10px 0;">
                    <i style="background: rgba(255, 255, 255, 0);" class="fa icon-c fa-money"></i>
                    <div class="float-text">
                        <h2>Get Paid</h2>
                        <h4>Watch the Cash Roll in.</h4>
                    </div>
                </div>

                       

              </div>

                <div class="col-md-6 text-center">
                     <img class="img-responsive" src="<?php echo $this->webroot;?>images/iPhone_5S.png" style="margin:0 auto;" >
              </div>
          </div>
      </div>
             
        </section>
<div class="clearfix"></div>

         <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="content" style="margin-bottom:50px; width:80%;">
                        <h3 class="text-center" style="margin-bottom:10px; font-size:28px;">How should I decide my Errand Price?</h3>
                         <p style="text-align:center;">A good payment should benefit both the owner and the Errand Champion.  Consider the following:</p>
                           <div class="tagline" style="margin-top: 25px;">
                        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-shield"></i>
                    <div class="float-text">
                        <h3>Effort</h3>
                        <p>Scope and effort needed to bring this errand to completion.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-arrows-h"></i>
                    <div class="float-text">
                        <h3>Travel</h3>
                        <p>The distance between the errand location and the Errand Champion will also influence the
payment. Travel costs should be reflected in the price.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-clock-o"></i>
                    <div class="float-text">
                        <h3>Time</h3>
                        <p>The time taken to complete the errand is also an important factor to consider when setting the price.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-lightbulb-o"></i>
                    <div class="float-text">
                        <h3>Skills</h3>
                        <p>What skillsets are needed for the job? The more specialized the skills, the higher the
price. Use of equipment like heavy industrial machinery and power tools will also raise the price.</p>
                    </div>
                </div>
            </div>

          
        </div>
                            </div>



                </div>
                </div>
                
            </div>
        </div>
         


        <div class="clearfix"></div>
          <!-- <div class="grey-bg" >
        	<div class="container">
            	<div class="row">
                	<div class="col-md-4">
                    	<div class="post-img" style="margin-top:150px;"><img src="<?php echo $this->webroot?>images/tick.png" alt=""/></div>
                    </div>
                    <div class="col-md-8">
                    <div class="txt">
                        <h3>What will happens if no one offers to help me with the errand</h3>
                        <div class="tagline"><p>The more people who see your errand, the more offers you’re likely to receive.<br>
Increase the number of people who see your errand by sharing it on social networks, just click on the share button.<br>
Provide as much info as possible about the errand you need done and make sure that you’ve offered a price that is fair for the job.</p>
                        </div>

                        <p><strong>If multiple Errand Workers offer to complete my errand, then what do I do?</strong>   </p>
                        <div class="tagline"><p>Your posted errand is gaining popularity among the Errand Worker.<br>
Assess each Errand Worker profile and assign the most suitable person to your errand. To know more about each person, ask them in the comment section, just be mindful not to ask for any private details.</p>
                        </div>

                        <p><strong>What to do if my errand gets expired? </strong>   </p>
                        <div class="tagline"><p>If the deadline date is passed and the errand has not been assigned to an Errand Worker, the errand will expire and will be removed from the errands feed. You can re-post your errand to find another Errand Worker in the meantime. All you have to do select the “post a similar task” and edit the due date.</p>
                        </div>


                    </div>
                    </div>
                </div>
            </div>
        </div> -->
           
        


        <div class="clearfix"></div>
       
        <!-- <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="content" style="margin-bottom: 60px;">
                                <h3>What happens if the Errand Champion worker doesn’t complete the errand?</h3>
                                <div class="tagline">
                                <p>We advise you find an alternative solution with the Errand Champion worker. The date of completion can be changed.<br>
Follow the steps:
<ul>
<li>Inform the Errand Champion worker via Errand Champion Private Messages that you are going to cancel the errand</li>
<li>Please contact Errand Champion admin to release the funds</li>
<li>Cancel the errand selecting the “cancel” button</li>
<li>Re-post your errand.</li>

</ul>
Please contact our support team for any other questions.

</p>

                                </div>

                              <h3>How to edit or cancel/delete my Errand?</h3>
                                <div class="tagline">
                                <p>If you want to cancel an errand that has been assigned, we advise you to follow the steps below:<br>
 Inform the Errand Champion Worker via Errand Champion Private messaging that you will have to cancel the errand. If the errand has not been assigned, you can update or delete your errand by following the below Quick Steps:
<ol>
    <li>Log in to Errand Champion from a web browser and click on "My Errands” tab</li>
     <li>Select the respective errand and click on the 'edit' icon and cancel the errand.</li>
</ol>

</p>

                               </div>



                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="post-1-img" style="margin-top: 175px;"><img src="<?php echo $this->webroot?>images/tick.png" alt=""/></div>
                        </div>
                    </div>
                </div> -->
            

        
    </section>
    <div class="clearfix"></div>
    <section class="task-section">
    <div style="margin-top:120px; margin-bottom: 120px" class="container"><h3 style="margin-top: 0; margin-bottom:20px; width:auto; display: block;"><!-- Lets get started --></h3><div class="txt"><!-- <a class="task-btn" href="<?php echo $this->webroot?>users/my_task">Post A task</a> -->
<a href="Javascript: void(0);" style="text-decoration: none;"><button style="border: 0;
border-radius: 0;
background: #F36800;
color: #fff;
text-transform: uppercase;
font-weight: 300;
text-shadow: none;
padding: 15px 25px;
font-size: 18px;

margin: 0 auto;
margin-top: 64px;
" class="btn btn-default post_an_errand">Post an Errand<i style="margin-left: 10px;
font-size: 16px;
display: inline-block;
border-radius: 100%;
border: 1px solid #E0E0E0;
width: 25px;
height: 25px;
line-height: 20px;display: inline-block;

text-align: center;" class="fa fa-arrow-right"></i></button></a>

<a href="<?php echo $this->webroot;?>errands/" style="text-decoration: none;"><button style="border: 0;
border-radius: 0;
background: #F36800;
color: #fff;
text-transform: uppercase;
font-weight: 300;
text-shadow: none;
padding: 15px 25px;
font-size: 18px;

margin: 0 auto;
margin-top: 64px;
margin-left: 15px;
display: inline-block;
" class="btn btn-default">Complete an Errand<i style="margin-left: 10px;
font-size: 16px;
display: inline-block;
border-radius: 100%;
border: 1px solid #E0E0E0;
width: 25px;
height: 25px;
line-height: 20px;
text-align: center; display: inline-block;
margin-left: 15px;" class="fa fa-arrow-right"></i></button></a>

    </div>





    </section>


    <style type="text/css">
    .icon-c {
        font-size: 40px !important;
/* color: #3c948b; */
background: #ffffff;
border-radius: 50%;
height: 80px;
width: 80px;
text-align: center;
line-height: 78px !important;
border: 1px solid #eeeeee;
transition: all 0.35s ease-out;
    }
    .float-text {
        display: inline-block;
width: 75%;
vertical-align: middle;
    }
  .float-text > h3{
    font-size: 23px!important;
  }

  .float-text {
overflow: hidden;
padding: 0 15px;
}

  .float-box:hover .icon-c, .icon-c:hover {
border-color: #000;
background: #000;
color: #ffffff;
}

.icon-ca {
    font-size: 40px !important;
/* color: #3c948b; */

border-radius: 50%;
height: 80px;
width: 80px;
text-align: center;
line-height: 78px !important;
border: 1px solid #eeeeee;
transition: all 0.35s ease-out;


border-color: #000;
background: #000;
color: #f7f7f7;
}

.icon-ca:hover {
    border-color: #000;
background: #f7f7f7;
color: #000;
}



.body-content h1 {font-size:40px!important;}
h2, .h2 {
  font-size: 28px;}
h4, .h4 {
  font-size: 16px;
}

.body-content .grey-bg .txt h3 {font-size:28px;}
.body-content .content h3 {font-size:27px}


    </style>


<script type="text/javascript">
$(function() {

    $('#test').animate({left:'18'}, 1200);
    // $('#test').delay(1000).animate({top:'10'}, 1200);

});
</script>

<script type="text/javascript">
// $(function() {

//     $('#test-one').animate({right:'18'}, 1200);
//     // $('#test-one').delay(1000).animate({top:'10'}, 1200);

// });
</script>

<script>
$(document).ready(function(){    
    $(".view_more").click(function () {
        var itemid =$(this).attr('itemid');
        if(itemid!=''){
            $('#'+itemid).show();
            $(this).hide();
        }
    }); 
}); 
</script>

<style type="text/css">




.how-work .hovicon {
width: 50px!important;
height: 50px!important;
}
.hovicon.effect-1 {
background: #f7f7f7;
/*-webkit-transition: background 0.2s, background 0.2s;*/
transition: background 0.2s, background 0.2s;
}

.float-text-v1 {
overflow: hidden;
padding: 15px 15px 15px 10px;
}

.together {
    width: 89%;
padding: 0;
margin: 0;
display: inline-block;
vertical-align: -webkit-baseline-middle;
}
</style>
