<div class="contents form">
<?php 
//echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('ManageHomepage'); ?>
	<fieldset>
		<legend><?php echo __('Add New Page'); ?></legend>
	<?php
		echo $this->Form->input('title',array('required'=>'required'));
		echo $this->Form->input('subtitle');
		echo $this->Form->input('youtube_link');
        echo $this->Form->input('status');
	?>
    <textarea name="data[ManageHomepage][description]" id="Contentcontent" style="width:600px; height:450px;"></textarea>
   </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>