<div class="contents form">
<?php 
//echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('ManageHomepage',array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Content'); ?></legend>
	<?php
        echo $this->Form->input('title');
		echo $this->Form->input('subtitle');
        echo $this->Form->input('youtube_link');

	?>
    <textarea name="data[ManageHomepage][description]" id="description" style="width:600px; height:450px;"><?php echo $this->request->data['ManageHomepage']['description']; ?></textarea>
        <?php echo $this->Form->end(__('Submit')); ?>
</div>