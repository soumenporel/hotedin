<table cellpadding="0" cellspacing="0"  id="sortable">
<h2 style="width:400px;float:left;"><?php echo __('Manage Homepage'); ?></h2>
<div class="contents index wp_pages_index">
        <thead>
            <tr>
                <th>
                    <?php echo __('Title'); ?>
                </th>
                <th>
                    <?php echo __('Subtitle'); ?>
                </th>
                <th>
                    <?php echo __('Description'); ?>
                </th>
                <th>
                    <?php echo __('Youtube Link'); ?>
                </th>
                <th>
                    <?php echo __('Actions'); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($managehomes as $managehome) { ?>
               <tr>
                   <td><?php echo $managehome['ManageHomepage']['title'] ?></td>
                   <td><?php echo $managehome['ManageHomepage']['subtitle'] ?></td>
                   <td><?php echo $managehome['ManageHomepage']['youtube_link'] ?></td>
                   <td><?php echo $managehome['ManageHomepage']['description'] ?></td>
                   <td><?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                    array('action' => 'edit', $managehome['ManageHomepage']['id']),
                    array('class' => 'btn btn_circle btn-edit', 'escape'=>false));
                    ?></td>
               </tr>   
            <?php } ?>
        </tbody>
    </table>
</div>