<!--login bg-->
<section class="post-jpb-section py-5 mb-5" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
  <div class="container">
      <div class="col-sm-12 col-md-5 mx-auto">
          <h1 class="post-jpb-section-titel text-center py-3">Post Job</h1>
      </div>
  </div>
</section>
<section class="salary-page-area">
          <div class="container">
      <div class="row">
          <h1  class="dashboard-heading candidate-heading-color text-center w-100 font-weight-bold">POST A JOB</h1>
          <div class="post-jpb-breadcum w-100 py-3">
              <ul>
                  <li><a class="active" href="#">Create Ad </a><i class="icon ion-ios-arrow-right"></i></li>
                  <li><a href="#">Preview </a><i class="icon ion-ios-arrow-right"></i></li>
                  <li><a href="#">Publish </a></li>
              </ul>
          </div>
          <h1 class="w-100 job-jnformation-text text-center py-3">Job Information</h1>

          </div>

          <form class="d-block w-100 post-jpb-area" action="<?php echo $this->webroot ?>postjobs/preview"  method="post">
          <div class="row">
              <div class="clearfix"></div>
                <div class="col-sm-6">
                  <label class="d-block">Title :</label>
                  <input type="text" class="form-control mb-4" id="formGroupExampleInput" name="data[Job][title]" placeholder="Job Title">
                </div>
               <div class="col-sm-6">
                  <label class="d-block">Job Type :</label>
                  <select class="form-control payment-select mb-4" name="data[Job][job_type]" >
                      <?php foreach ($jobtypes as $jobtype) : ?>
                                      <option value="<?php echo $jobtype['Jobtype']['id'] ?>"><?php echo $jobtype['Jobtype']['name'] ?></option>
                        <?php endforeach;  ?>
                                  </select>
               </div>
               <div class = "col-sm-6">
                  <label class = "d-block" >Description :</label>
                  <textarea class="form-control mb-4 post-jpb-text-area" name="data[Job][description]"></textarea>
              </div>
              <div class="col-sm-6">
                  <label class="d-block">Employer Questions  :</label>
                  <textarea class="form-control mb-4 post-jpb-text-area"  name="data[Job][employer_ques]"></textarea>
              </div>
              <div class="col-sm-6">
                  <label class="d-block">Category :</label>
                  <select class="form-control payment-select mb-4" name="data[Job][category]" >
                      <?php foreach ($jobCategories as $jobCategory) { ?>
                          <?php print_r($jobCategory); ?>
                         <option value="<?php echo $jobCategory['JobCategory']['id'] ?>"><?php echo $jobCategory['JobCategory']['title'] ?></option>
                      <?php } ?>
                  </select>
               </div>
               <div class="col-sm-6">
                  <label class="d-block">Salary per  :</label>
                  <select class="form-control payment-select mb-4" name="data[Job][salary_per]" >
                                      <option value="hourly">Hourly</option>
                                      <option value="daily">Daily</option>
                                      <option value="weekly">Weekly</option>
                                      <option value="monthly">Monthly</option>
                                      <option value="yearly">Yearly</option>
                                  </select>
               </div>

               <div class="col-sm-12">
                  <label class="d-block">Salary Rate :</label>
                   <input type="text" class="form-control mb-4" id="formGroupExampleInput" name="data[Job][salary_rate]" placeholder="">
              </div>

               <h1 class="w-100 job-jnformation-text text-center py-5">Company Information</h1>
               <div class="col-sm-4">
                  <label class="d-block">Company Name :</label>
                  <input type="text" class="form-control mb-4" name="data[Job][company_name]" id="formGroupExampleInput" value="<?php echo $companydetails['CompanyDetail']['company_name'] ?>">
                </div>

                <div class="col-sm-4">
                  <label class="d-block">Contact Email :</label>
                  <input type="text" class="form-control mb-4" id="formGroupExampleInput" name="data[Job][email_address]" value="<?php echo $companydetails['CompanyDetail']['email_address'] ?>">
               </div>

               <div class="col-sm-4">
                  <label class="d-block">Website :</label>
                  <input type="text" class="form-control mb-4" id="formGroupExampleInput" name="data[Job][website]" value="<?php echo $companydetails['CompanyDetail']['website_address'] ?>">
               </div>
               <?php if($companydetails['CompanyDetail']['logo'] != '') { ?>
                   <img src="<?php echo $this->webroot ; ?>company_logo/<?php echo $companydetails['CompanyDetail']['logo'] ?>" height="200px" width="200px" alt="">
                   <input type="hidden" name="data[Job][logo]" value="<?php echo $companydetails['CompanyDetail']['logo'] ?>" />
               <?php }else { ?>
               <div class="col-sm-12">
                  <label class="d-block">Logo :</label>
                  <div class="post-jpb-logo">
                       <div class="w-100 text-center pt-4">Drop files here</div>
                           <div class="upload_file_container">
                              <label class="blue-btn">Select file!</label>
                              <input type="file" name="data[Job][logo]" />
                           </div>
                  </div>
               </div>
           <?php } ?>


                <h1 class="w-100 job-jnformation-text text-center py-5">Location</h1>

                <div class="col-sm-6">
                  <label class="d-block">Country :</label>
                  <select id="country"  class="form-control payment-select mb-4" name="data[Job][country]" >
                                    <?php foreach ($countries as $country) { ?>
                                         <?php if($companydetails['Country']['id'] == $country['Country']['id']) { ?>
                                             <?php $countryCode = $companydetails['Country']['id'];  ?>
                                        <option value="<?php echo $country['Country']['id'] ?>" selected><?php echo $country['Country']['name'] ?></option>
                                    <?php }else { ?>
                                        <option value="<?php echo $country['Country']['id'] ?>"><?php echo $country['Country']['name'] ?></option>
                                    <?php } ?>
                                    <?php } ?>


                                  </select>

               </div>
                <div class="col-sm-6">
                  <label class="d-block">State : </label>
                  <select id="state" class="form-control payment-select mb-4" name="data[Job][state]">

                  </select>
               </div>
                 <div class="col-sm-6">
                  <label class="d-block">Zip-Code :</label>
                  <input type="text" class="form-control payment-select mb-4" name="data[Job][post_code]"  value="<?php echo $companydetails['CompanyDetail']['post_code'] ?>">

               </div>
                 <div class="col-sm-6">
                  <label class="d-block">City</label>
                  <input type="text" class="form-control" id="formGroupExampleInput" name="data[Job][city]" value="<?php echo $companydetails['City']['name'] ?>">
                  <div class="w-100 mb-4">For example: "Chicago", "London", "Anywhere" or "Telecommute".</div>
               </div>

                <h1 class="w-100 job-jnformation-text text-center py-0">Membership plan</h1>
                <div class="w-100 text-center pb-5">Listing Type  :</div>

                <?php if($user['EmployerMembershipPlan']['id'] != "") {?>
                 <div class="col-sm-12 col-md-6 mb-5">
                  <label class="d-block Purchase-text"><?php echo $user['EmployerMembershipPlan']['title'] ?></label>
                  <div class="mounth-area">
                      <label class="post-job-input"><?php echo $user['EmployerMembershipPlan']['jobs'] ?> job<br>
                          <?php echo $user['EmployerMembershipPlan']['duration'] ?> <?php echo $user['EmployerMembershipPlan']['duration_unit'] ?>
                        <input type="checkbox">
                        <span class="pose-c"></span>
                      </label>
                      <input type="submit" value="Purchased" class="post-job-add" name="" disabled>
                  </div>
                 </div>
             <?php }else { ?>
                  <div class="col-sm-12 col-md-6 mb-5">
                 <?php foreach ($plans as $plan) { ?>

                     <label class="d-block Purchase-text"><?php echo $plan['EmployerMembershipPlan']['title'] ?></label>
                     <div class="mounth-area">
                         <label class="post-job-input"><?php echo $plan['EmployerMembershipPlan']['jobs'] ?> job<br>
                             <?php echo $plan['EmployerMembershipPlan']['duration'] ?> <?php echo $plan['EmployerMembershipPlan']['duration_unit'] ?>
                           <input type="radio" name="membership" >
                           <span class="pose-c"></span>
                         </label>
                         <?php if($plan['EmployerMembershipPlan']['price'] == 0) {?>
                             <input type="submit" value="Free" class="post-job-add" name="">
                     <?php }else { ?>
                         <input type="submit" value="<?php echo $plan['EmployerMembershipPlan']['price']  ?>" class="post-job-add" name="">
                     <?php } ?>
                     </div>

                 <?php } ?>
                 </div>
              <?php } ?>

            <div class="w-100 text-center post-job-or mb-2">
                <input type="submit" class="candidate-register mb-5" value="Preview" placeholder="Preview" >
            </div>
                  <div class="w-100 text-center post-job-or mb-5">Or<br>
                      <input type="reset" name="reset" value="Reset">
                  </div>
          </div>
      </form>

  </div>
</section>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#country').on('change', function() {
            var value = $(this).val();
            $.ajax({
                url: '<?php echo $this->webroot; ?>postjobs/getStates',
                type: 'POST',
                data: {val: value},
                success: function(response){
                    var res = JSON.parse(response);
                    var i,htm="";
                    for(i in res){
                        htm +='<option class="remove" value="'+res[i]['State']['id']+'">'+res[i]['State']['name']+'</option>'
                    }
                    $('.remove').remove();
                    $('#state').append(htm);
                }
            });

        })

        function callAjax(value) {
            $.ajax({
                url: '<?php echo $this->webroot; ?>postjobs/getStates',
                type: 'POST',
                data: {val: value},
                success: function(response){
                    var res = JSON.parse(response);
                    var i,htm="";
                    for(i in res){
                        htm +='<option class="remove" value="'+res[i]['State']['id']+'">'+res[i]['State']['name']+'</option>'
                    }
                    $('.remove').remove();
                    $('#state').append(htm);
                }
            });
        }
        callAjax(<?php echo $countryCode; ?>);
    });
</script>
