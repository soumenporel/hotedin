<!--login bg-->
<section class="apply-job-section py-5 mb-5">
  <div class="container">
      <div class="col-sm-12 col-md-5 mx-auto">
          <h1 class="post-jpb-section-titel text-center py-3">Job Preview</h1>
      </div>
  </div>
</section>
<section class="salary-page-area">
          <div class="container">
              <form  action="<?php echo $this->webroot ?>postjobs/submit" method="post">
                  <input type="hidden" name="data[Job][title]" value="<?php echo $arr['Job']['title'] ?>">
                  <input type="hidden" name="data[Job][salary_rate]" value="<?php echo $arr['Job']['salary_rate'] ?>">
                  <input type="hidden" name="data[Job][job_type]" value="<?php echo $arr['Job']['job_type']; ?>">
                  <input type="hidden" name="data[Job][category]" value="<?php echo $arr['Job']['category'] ; ?>">
                  <input type="hidden" name="data[Job][salary_per]" value="<?php echo $arr['Job']['salary_per'] ?>">
                  <input type="hidden" name="data[Job][description]" value="<?php echo $arr['Job']['description'] ?>">
                  <input type="hidden" name="data[Job][employer_ques]" value="<?php echo $arr['Job']['employer_ques'] ?>">

                  <input type="hidden" name="data[Job][company_name]" value="<?php echo $arr['Job']['company_name'] ?>">
                  <input type="hidden" name="data[Job][email_address]" value="<?php echo $arr['Job']['email_address'] ?>">
                  <input type="hidden" name="data[Job][website]" value="<?php echo $arr['Job']['website'] ?>">
                  <input type="hidden" name="data[Job][logo]" value="<?php echo $arr['Job']['logo'] ?>">
                  <input type="hidden" name="data[Job][country]" value="<?php echo $arr['Job']['country'] ?>">
                  <input type="hidden" name="data[Job][state]" value="<?php echo $arr['Job']['state'] ?>">
                  <input type="hidden" name="data[Job][post_code]" value="<?php echo $arr['Job']['post_code'] ?>">
                  <input type="hidden" name="data[Job][city]" value="<?php echo $arr['Job']['city'] ?>">



      <div class="row">
          <h1  class="dashboard-heading candidate-heading-color text-center w-100 font-weight-bold mb-4">
          <?php echo $arr['Job']['company_name'] ?> Hiring 10 <?php echo $arr['Job']['title'] ?>, <?php echo $arr['Job']['country'] ?> Shift</h1>
          </div>

          <h2 class="mb-3 apply-job-titel">Job Description</h2>
          <p><?php echo $arr['Job']['description'] ?></p>

          <h2 class="mb-3 apply-job-titel py-2">Employer Questions :</h2><p><?php echo $arr['Job']['employer_ques'] ?></p>

          <h2 class="mb-3 apply-job-titel py-2">Job Category: <?php echo $categorys ?></h2>
          <p class="apply-job-para mb-5"><?php echo $jobtypes ?></p>
          <p class="apply-job-para mb-5">Salery :<?php echo $arr['Job']['salary_rate'] ?> <?php echo $arr['Job']['salary_per'] ?></p>

          <h2 class="mb-3 apply-job-titel py-2">keySkills</h2>
            <div class="mb-4">
              <button type="button" class="btn btn-primary apply-job-btn">Technical Support</button>
              <button type="button" class="btn btn-primary apply-job-btn">Networking</button>
              <button type="button" class="btn btn-primary apply-job-btn">Voice Proccess</button>
              <button type="button" class="btn btn-primary apply-job-btn">BPO</button>
              <button type="button" class="btn btn-primary apply-job-btn">International BPO</button>
              <button type="button" class="btn btn-primary apply-job-btn">Google</button>
               <button type="button" class="btn btn-primary apply-job-btn">Microsoft</button>
                 <button type="button" class="btn btn-primary apply-job-btn">MS</button>
               <button type="button" class="btn btn-primary apply-job-btn">Fresher</button>
            </div>
            <input type="submit" name="" value="Post">
</form>




  </div>
</section>
