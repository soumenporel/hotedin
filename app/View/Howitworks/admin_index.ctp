<div class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <!--statistics start-->
            <div class="row state-overview">
                <div class="col-md-1 col-xs-6 col-sm-1">&nbsp;</div>


            </div>
            <!--statistics end-->
        </div>
    </div>
</div>
<div class="users index">
	<h2 style="width:400px;float:left;"><?php echo __('Clients'); ?></h2>

    <table cellpadding="0" cellspacing="0">
	<tr>
		<th>Title</th>
		<th>Description</th>
		<th>Image</th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
    <?php foreach ($howitworks as $howitwork): ?>
        <tr>
    		<td><?php echo $howitwork['Howitwork']['title'] ?></td>
    		<td><?php echo $howitwork['Howitwork']['description'] ?></td>
                     <td width="11%">
                    <?php
                        $uploadFolder = "howitworks_image/";
                        $uploadPath = $this->webroot . $uploadFolder;
                        ?>
                        <img src="<?php echo ($howitwork['Howitwork']['image'] != '') ? $uploadPath . $howitwork['Howitwork']['image'] : ''; ?>" width="100" />
                     </td>
    		<td class="actions">
    			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $client['Client']['id'])); ?>
    			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $howitwork['Howitwork']['id'])); ?>

    			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $howitwork['Howitwork']['id']), null, __('Are you sure you want to delete %s?', $howitwork['Howitwork']['title'])); ?>
    		</td>
    	</tr>
    <?php endforeach ?>
