<div class="categories form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('Howitwork',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('How It Works'); ?></legend>

	    <?php
    		echo $this->Form->input('id');
            echo $this->Form->input('hide_image',array('type' => 'hidden','value'=>isset($this->request->data['Howitwork']['image'])?$this->request->data['Howitwork']['image']:''));
    		echo $this->Form->input('title',array('required'=>'required', 'label'=>' Title'));
            echo $this->Form->input('description',array('label'=>' Description'));
    		echo $this->Form->input('image',array('type'=>'file'));
            ?>
            <div>
                <?php
                $uploadImgPath = WWW_ROOT.'howitworks_image';
                if( $this->request->data['Howitwork']['image']!='' && file_exists($uploadImgPath . '/' .  $this->request->data['Howitwork']['image'])){
                ?>
                <p>Image :</p><img alt="" src="<?php echo $this->webroot;?>howitworks_image/<?php echo $this->request->data['Howitwork']['image'];?>" style=" height:80px; width:80px;">
                <?php
                }
                else{
                ?>
                    <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                <?php } ?>
            </div>
	</fieldset>

<?php echo $this->Form->end(__('Submit')); ?>
</div>
