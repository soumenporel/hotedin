<div class="categories form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('Howitwork',array('enctype'=>'multipart/form-data'));
?>
	<fieldset>
		<legend><?php echo __('How it work'); ?></legend>

            <?php
              echo $this->Form->input('title',array('required'=>'required', 'label'=>'Title'));
              echo $this->Form->input('description',array('label'=>'Description'));
		      echo $this->Form->input('image',array('type'=>'file'));
                ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
