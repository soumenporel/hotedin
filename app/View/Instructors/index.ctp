<section class="listing_result">
    <div class="container">
        <div class="row" style="background: #f6f6f6; border-left: 1px solid #dedede; border-right: 1px solid #dedede;">
            <div class="col-md-3 col-sm-3" style="padding:0">
                <div class="publicprofile filter">
                    <h3><i class="fa fa-filter"></i> Filter
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Clear all
                            </label>
                        </div>
                    </h3>
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Keyword</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search by Keyword">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Course Name</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Search by Course Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Degree</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="By Degree">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Specialisation/Subject</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Specialisation/Subject">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Location</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Location">
                        </div>
                        <button type="submit" class="btn btn-danger">Done</button>
                    </form>
                </div>
            </div>
            <div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
                <div class="profile_second_part">
                    <div class="search-result-head">
                        <h2>Total 300 result found </h2>
                        <form class="form-inline pull-right">
                            <div class="form-group">
                                <label for="exampleInputName2">Short By</label>
                                <select name="" class="form-control">
                                    <option>Popularity</option>
                                    <option>Language</option>
                                    <option>Reviews</option>
                                    <option>Newest</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="search-result-body">
                        <div class="row">
                            <?php
                            foreach ($results as $result) {
                                //print_r($result);die;
                                ?>  
                                <div class="col-md-6">
                                    <div class="instructors">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img src="<?php echo $this->webroot; ?>user_images/<?php echo $result['User']['user_image']; ?>" alt="" width="190" height="190">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><?php echo $result['User']['first_name'] . " " . $result['User']['last_name']; ?></h4>
                                                <p><?php echo $result['User']['first_name'] . " " . $result['User']['last_name']; ?></p>
                                                <p><?php echo $result['User']['email_address']; ?></p>
                                                <p><?php echo $result['User']['address']; ?></p>
                                            </div>
                                        </div>
                                        <span id="skillContainer_<?php echo $result['User']['id']; ?>"><button type="button" class="btn btn-primary" onclick="follwIns('<?php echo $result['User']['id']; ?>')">Follow</button></span>


                                        <span id="favContainer_<?php echo $result['User']['id']; ?>"><button type="button" class="btn btn-success" onclick="favIns('<?php echo $result['User']['id']; ?>')">Favourite</button></span>

                                    </div>
                                </div>
                                <?php
                            }
                            ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function follwIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>instructors/followinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#skillContainer_' + id).html('<button type="button" class="btn btn-danger" onclick="unfollwIns(' + data + ')">Unfollow</button>');
                }

            }
        });
    }


    function unfollwIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>instructors/unfollowinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#skillContainer_' + id).html('<button type="button" class="btn btn-primary" onclick="follwIns(' + data + ')">Follow</button>');
                }

            }
        });
    }



    function favIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>instructors/favinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#favContainer_' + id).html('<button type="button" class="btn btn-danger" onclick="unfavwIns(' + data + ')">Unfavourite</button>');
                }

            }
        });
    }


    function unfavwIns(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            url: "<?php echo $this->webroot; ?>instructors/unfavinstrutor",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data != 0)
                {
                    $('#favContainer_' + id).html('<button type="button" class="btn btn-success" onclick="favIns(' + data + ')">Favourite</button>');
                }

            }
        });
    }


</script>