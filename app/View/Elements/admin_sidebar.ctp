<?php

$uploadLogoFolder = "site_logo";
$uploadLogoPath = WWW_ROOT . $uploadLogoFolder;
$LogoName = $sitesetting['Setting']['logo'];
if(file_exists($uploadLogoPath . '/' . $LogoName) && $LogoName!=''){
    $LogoLink=$this->webroot.'site_logo/'.$LogoName;
}else{
    $LogoLink=$this->webroot.'adminFiles/images/logo.png';
}

if(!empty($role_restrictions)) {
    foreach ($role_restrictions as $key => $value) {
        $saved_accesibility[] = $value['Adminrolemeta']['meta_key'];
    }
} else {
    $saved_accesibility = array();
}

//pr($userdetails);

?>

<!-- left side start-->
<div class="left-side sticky-left-side">

    <!--logo and iconic logo start-->
    <div class="logo" style="background: white;" >
        <a href="javascript:void(0);"><img src="<?php echo $LogoLink;?>" alt="" style="height: 41px;"></a>
    </div>

    <div class="logo-icon text-center">
        <a href="javascript:void(0);"><img src="<?php echo $LogoLink;?>" style="height: 60px;" alt=""></a>
    </div>
    <!--logo and iconic logo end-->

    <div class="left-side-inner">

        <!-- visible to small devices only -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media logged-user">
                <div class="media-body">
                    <h4><a href="<?php echo $this->webroot?>admin/users/edit/<?php echo $userdetails['User']['id']; ?>"><?php echo($userdetails['User']['first_name'].' '.$userdetails['User']['last_name'])?></a></h4>
                </div>
            </div>

            <h5 class="left-nav-title">Account Information</h5>
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?php echo $this->webroot?>admin/users/edit/<?php echo $userdetails['User']['id']; ?>"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                <li><a href="<?php echo $this->webroot?>admin/users/changepwd"><i class="fa fa-user"></i> <span>Manage Password</span></a></li>
                <li><a href="<?php echo $this->webroot?>admin/users/logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>
<?php //print_r($roleAccess); ?>
        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
<!--		        <li class="active"><a href="<?php echo $this->webroot?>admin/users/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>-->

            <li class="<?php echo ($this->params['controller'] == 'settings' && $this->params['action'] == 'admin_sitelogo') ? 'active': ''; ?>">
                <a href="<?php echo $this->webroot?>admin/settings/sitelogo/1"><i class="fa fa-upload"></i> <span>Manage Logo</span></a>
            </li>


            <li>
                <a href="<?php echo $this->webroot?>admin/users/logout"><i class="fa fa-sign-out"></i> <span>Log Out</span></a>
            </li>

            <li>
                <a href="<?php echo $this->webroot?>admin/dashboards/index"><i class="fa fa-home"></i> <span>Dashboard</span></a>
            </li>



            <li>
                <a href="<?php echo $this->webroot?>admin/settings/edit/1"><i class="fa fa-cog"></i> <span>Manage Settings</span></a>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'roles') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-cog"></i> <span>Member Access</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'roles' && $this->params['action'] == 'admin_group_members') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot?>admin/roles/group_members">Group Members</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'roles' && $this->params['action'] == 'admin_group_n_permission') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot?>admin/roles/group_n_permission">Groups &amp; Permissions</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'roles' && $this->params['action'] == 'admin_manage_admins') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot?>admin/roles/manage_admins">Manage Admins</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'manage_homepages') ? 'nav-active': ''; ?>"><a href=""><i class="fa fa-file-text"></i> <span>Manage Homepage</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'manage_homepages' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                            <a href="<?php echo $this->webroot; ?>admin/manage_homepages/index"> List Sections</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'manage_homepages' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                            <a href="<?php echo $this->webroot; ?>admin/manage_homepages/add"> Add Sections</a>
                    </li>
                </ul>

            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'wp_pages') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>CMS Pages</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'wp_pages' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/wp_pages/index"> List Pages</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'wp_pages' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/wp_pages/add"> Add Pages</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'cms_page') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Contents</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'cms_page' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/cms_page/index"> List Contents</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'cms_page' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/cms_page/add"> Add Contents</a>
                    </li>
                </ul>
            </li>
             <li class="menu-list <?php echo ($this->params['controller'] == 'banners') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Home Banner</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'banners' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/banners/index"> List Home Banner</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'banners' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/banners/add"> Add Banner</a>
                    </li>
                </ul>
            </li>
             <li class="menu-list <?php echo ($this->params['controller'] == 'pagebanners') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Page Banner</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'pagebanners' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/pagebanners/index"> List Page Banner</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'pagebanners' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/pagebanners/add"> Add Banner</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Jobs') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Job</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Jobs' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Jobs/index"> List Job</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Jobs' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Jobs/add"> Add Job</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Jobtypes') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Job Type</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Jobtypes' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Jobtypes/index"> List Jobtypes</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Jobtypes' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Jobtypes/add"> Add Jobtypes</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Jobtypes' && $this->params['action'] == 'admin_category') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Jobtypes/category">Job Categories</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Countries') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Country</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Countries' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Countries/index"> List Countries</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Countries' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Countries/add"> Add Countries</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Salarypers') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Salarypers</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Salarypers' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Salarypers/index"> List Salarypers</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Salarypers' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Salarypers/add"> Add Salarypers</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Notifications') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Notifications</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Notifications' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Notifications/index"> List Notifications</a>
                    </li>
<!--                    <li class="<?php echo ($this->params['controller'] == 'Seourls' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seourls/add"> Add Seo URL</a>
                    </li>-->
                </ul>
            </li>
<!--            <li class="menu-list <?php echo ($this->params['controller'] == 'NotificationsController') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>FAQ</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'faqs' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/index"> List FAQ</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'faqs' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/add"> Add FAQ</a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_categories') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/categories"> Categories</a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_addcategory') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/addcategory"> Add Category</a>
                    </li>
                </ul>
            </li>
            -->


            <li class="menu-list <?php echo ($this->params['controller'] == 'blogs') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Blog</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'blogs' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/blogs/index"> List Blog</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'blogs' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/blogs/add"> Add Blog</a>
                    </li>
                </ul>
            </li>

<!--            <li class="menu-list <?php echo ($this->params['controller'] == 'PressReleases') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>News</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'PressReleases' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/PressReleases/index"> List News</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'PressReleases' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/PressReleases/add"> Add News</a>
                    </li>
                </ul>
            </li>-->

            <li>
                <a href="<?php echo $this->webroot?>admin/email_templates/index"><i class="fa fa-envelope"></i> <span>Email Templates</span>
                </a>
            </li>

            <li class="menu-list  <?php echo ($this->params['controller'] == 'normal_users') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span> Employer Management</span></a>
                <ul class="sub-menu-list">

                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/normal_users/index/1"> Employer Listing </a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/normal_users/add/1"> Add </a>
                    </li>
                </ul>
            </li>

            <li class="menu-list  <?php echo ($this->params['controller'] == 'normal_users') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span> Jobseeker Management</span></a>
                <ul class="sub-menu-list">

                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/normal_users/index/2"> Jobseeker Listing </a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/normal_users/add/2"> Add </a>
                    </li>
                </ul>
            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'membership_plans') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-pencil"></i> <span>Feature Membership Plans</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'membership_plans' && $this->params['action'] == 'admin_employer') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/membership_plans/employer"> Employer Membership</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'membership_items') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-pencil"></i> <span> Membership Items</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'membership_items' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/membership_items/index"> List Membership Items</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'membership_items' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/membership_items/add"> Add Membership Item</a>
                    </li>
                </ul>
            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'users' && ($this->params['action'] == 'admin_contact_us' || $this->params['action'] == 'admin_contact_us_add')) ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Contact Us</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_contact_us') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/users/contact_us"> List Contact</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_contact_us_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/users/contact_us_add"> Add Contact</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Seos') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-camera"></i> <span>SEO Keywords</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Seos' && $this->params['action'] == 'admin_listing') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seos/listing"> List SEO Keywords</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Languages' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seos/add"> Add SEO Keywords</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Sitemaps') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-ticket"></i> <span>Sitemaps</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Sitemaps' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Sitemaps/listing"> List Sitemap</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Sitemaps' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Sitemaps/add"> Add Sitemap</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Analytics') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Analytics</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Analytics' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Analytics/listing"> List Analytics</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Analytics' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Analytics/add"> Add Analytics</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Seourls') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Seo URL</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Seourls' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seourls/index"> List Seo URL</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Seourls' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seourls/add"> Add Seo URL</a>
                    </li>
                </ul>
            </li>

<!--            <li class="menu-list <?php echo ($this->params['controller'] == 'newsletters') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Newsletter</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/newsletters/index"> List Newsletter</a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/newsletters/add"> Add Newsletter</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'SocialMedias') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Social Medias</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'SocialMedias' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/SocialMedias/listing"> List Social Media</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'SocialMedias' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/SocialMedias/add"> Add Social Media</a>
                    </li>
                </ul>
            </li>

             <li class="menu-list <?php echo ($this->params['controller'] == 'Clients') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Clients</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Clients' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Clients/index"> List Client</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Clients' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Clients/add"> Add Client</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'blog_categories') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Blog category</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'blog_categories' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/blog_categories/index"> Blog category</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'blog_categories' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/blog_categories/add"> Add Blog Category</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'press_categories') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>News category</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'press_categories' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/press_categories/index"> List News category</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'press_categories' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/press_categories/add"> Add News Category</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'industries') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Industry</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'industries' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/industries/index"> List Industry</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'industries' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/industries/add"> Add Industry</a>
                    </li>
                </ul>
            </li>-->

            <li class="menu-list <?php echo ($this->params['controller'] == 'testimonials') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Testimonials</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'testimonials' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/testimonials/index"> List Testimonials</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'testimonials' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/testimonials/add"> Add Testimonials</a>
                    </li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'Clients') ? 'nav-active': ''; ?>">
               <a href=""><i class="fa fa-mobile"></i> <span>Clients</span></a>
               <ul class="sub-menu-list">
                   <li class="<?php echo ($this->params['controller'] == 'Clients' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                       <a href="<?php echo $this->webroot; ?>admin/Clients/index"> List Client</a>
                   </li>
                   <li class="<?php echo ($this->params['controller'] == 'Clients' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                       <a href="<?php echo $this->webroot; ?>admin/Clients/add"> Add Client</a>
                   </li>
               </ul>
           </li>

           <li class="menu-list <?php echo ($this->params['controller'] == 'Howitworks') ? 'nav-active': ''; ?>">
              <a href=""><i class="fa fa-mobile"></i> <span>How it Works</span></a>
              <ul class="sub-menu-list">
                  <li class="<?php echo ($this->params['controller'] == 'Howitworks' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                      <a href="<?php echo $this->webroot; ?>admin/Howitworks/index"> List Client</a>
                  </li>
                  <li class="<?php echo ($this->params['controller'] == 'Howitworks' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                      <a href="<?php echo $this->webroot; ?>admin/Howitworks/add"> Add Client</a>
                  </li>
              </ul>
          </li>

          <li class="menu-list <?php echo ($this->params['controller'] == 'app_promotions') ? 'nav-active': ''; ?>">
             <a href=""><i class="fa fa-mobile"></i> <span>App Promotion</span></a>
             <ul class="sub-menu-list">
                 <li class="<?php echo ($this->params['controller'] == 'app_promotions' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                     <a href="<?php echo $this->webroot; ?>admin/app_promotions/index"> Index</a>
                 </li>
                 <li class="<?php echo ($this->params['controller'] == 'app_promotions' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                     <a href="<?php echo $this->webroot; ?>admin/app_promotions/add"> Add</a>
                 </li>
             </ul>
         </li>

        </ul>
        <!--sidebar nav end-->

    </div>
</div>
<!-- left side end-->
