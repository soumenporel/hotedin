<link rel="stylesheet" href="<?php echo $this->webroot;?>star_rating/css/star-rating.css" media="all" type="text/css"/>

<style>
.rating-container .clear-rating {
    display: none;
}
.rating-container .caption {
    display: none;
}
</style>
<?php //pr($postDetails); exit;?>
<div class="course-dashboard__top">
               <div class="top--container container">
                  <a class="top__image hidden-xs" ng-click="gotoNextCurriculumObject()">
                    <?php if (empty($postDetails['PostVideo'])) {

                                if(isset($postDetails['PostImage'][0]['originalpath']) && !empty($postDetails['PostImage'][0]['originalpath'])){

                                    ?>
                                <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $postDetails['PostImage'][0]['originalpath']; ?>" style="width:425px;" >
                            <?php  }
                            else{?>
                                <img src="<?php echo $this->webroot; ?>img/placeholder.png" style="width:425px;" >
                           <?php }
                        } else {

                           $file = $postDetails['PostVideo'][0]['originalpath'];
                            $ext = pathinfo($file, PATHINFO_EXTENSION);
                            $videoExt = array('mp4', 'flv', 'mkv','mov','avi','wmv');
                            if (in_array(strtolower($ext), $videoExt)) {
                              $file_name = trim($postDetails['PostVideo'][0]['originalpath'], $ext);
                                ?>

                                <video width="100%" height="100%" id="video1" onClick="playPause();" >
                                    <source src="<?php echo $this->webroot; ?>img/post_video/<?php echo $postDetails['PostVideo'][0]['originalpath']; ?>" type="video/<?php echo $ext; ?>">
                                    <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>

                                <?php
                            }
                        }
                        ?>
                     <!-- <div class="image__overlay play-icon" ng-class="ctaOverlayClass" style=""></div> -->
                  </a>

                  <div class="top__detail pos-r">
                     <div class="fx-lc">
                        <div class="fx detail__course-title" ng-bind="course.title || '&nbsp;'">
                         <?php echo $postDetails['Post']['post_title']; ?></div>
                     </div>
                     <?php
                      $url = '';
                      //print_r($lastLectureDetails);die;
                        if(!empty($lastLectureDetails)){



                          if($lastLectureDetails['Lecture']['type']==0){
                            if($lastLectureDetails['Lecture']['media']!=''){

                                $ext = pathinfo($lastLectureDetails['Lecture']['media'], PATHINFO_EXTENSION);
                                $imageExt = array('jpeg' , 'jpg');
                                $videoExt = array('mp4','flv','mkv');
                                $pdfExt = array('pdf');
                                $docExt = array('doc','docx','ppt');

                                if(in_array(strtolower($ext), $imageExt)){
                                  $url = $this->webroot."learns/view_image/".$postDetails['Post']['slug']."/".$lastLectureDetails['Lecture']['id'];
                                }
                                if(in_array(strtolower($ext), $videoExt)){
                                  $url = $this->webroot.'learns/video_html/'.$postDetails['Post']['slug'].'/'.$lastLectureDetails['Lecture']['id'];
                                }
                                if(in_array(strtolower($ext), $pdfExt)){
                                  $url = $this->webroot."learns/read_pdf/".$postDetails['Post']['slug']."/".$lastLectureDetails['Lecture']['id'];
                                }
                                if(in_array(strtolower($ext), $docExt)){
                                  $url = $this->webroot."learns/read_doc/".$postDetails['Post']['slug']."/".$lastLectureDetails['Lecture']['id'];
                                }

                            }else{
                              $url = $this->webroot."learns/description/".$postDetails['Post']['slug']."/".$lastLectureDetails['Lecture']['id'];
                            }
                          }else{
                            $url = $this->webroot."learns/previewquiz/".$postDetails['Post']['slug']."/".$lastLectureDetails['Lecture']['id'];
                          }
                        }
                     ?>

                     <?php if($url!=''){ ?>
                     <!-- ngIf: nextCurriculumObject -->
                     <a href="<?php echo $url; ?>" class="btn btn-primary btn-lg detail__continue-button" style=""><span>Continue to Lecture </span></a>
                     <?php } ?>
                     <!-- end ngIf: nextCurriculumObject --> <!-- ngIf: reviewEnabled() -->
                     <?php if($postDetails['Post']['user_id']!=$userid){ //print_r($RatingExit); die;?>
                      <span class="ratethiscourse">Rate this course</span>
                      <!-- <div class="ratingPart">
                        <input type="text" class="rating rating-loading" value="<?php if(!empty($RatingExit) && $RatingExit['Rating']['ratting']!='' ){ echo $RatingExit['Rating']['ratting'];}else{ echo '0'; }?>" data-size="xs" title="">
                      </div> -->
                      <div id="rateYo" class="rating" ></div>
                      <div class="detail__progress">
                        <div class="fx-lb">
                           <div class="fx">
                              <!-- ngIf: progressInfo.completionRatio > 0 -->
                              <!-- ngIf: progressInfo.completionRatio > 0 && !hasOrganization -->
                              <!-- ngIf: progressInfo.completionRatio === 0 -->
                              <div ng-if="progressInfo.completionRatio === 0" class="" style="">
                                <span translate="">
                                  <!-- <span>Get started</span> -->
                                </span>
                                <span ng-bind-html="progressInfo.progressString | translate"><b><?php echo $readListCount; ?></b> of <b><?php echo $lectureCount; ?></b> items complete</span>
                              </div>
                              <!-- end ngIf: progressInfo.completionRatio === 0 -->
                              <div class="progress">
                                <?php
                                  $complitionPercentage = round(($readListCount/$lectureCount)*100);
                                ?>
                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $complitionPercentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $complitionPercentage; ?>%">
                                  <span class="sr-only"><?php echo $complitionPercentage; ?>% Complete</span>
                                </div>
                              </div>
                           </div>
                           <a ng-href="" target="_blank"> </a>
                           <!-- ngIf: course.has_certificate -->
                           <div ng-if="course.has_certificate" class="tooltip-container ucp-certificate certificate-not-ready" ng-class="getCertificateStatusClass()">
                              <a ng-href="" target="_blank">
                                 <i class="udi udi-trophy ucp-certificate-icon"></i>
                                 <div class="progress">
                                    <div class="bar bar-success"></div>
                                 </div>
                                 <!-- ngIf: getCertificateStatusClass() == 'certificate-not-ready' -->
                                 <div class="tooltip tooltip-neutral left" ng-if="getCertificateStatusClass() == 'certificate-not-ready'">
                                  <span class="tooltip-inner certificate-not-ready-text" translate="">
                                    <span>You need to finish this course to get your certificate of completion!</span>
                                  </span>
                                </div>
                                 <!-- end ngIf: getCertificateStatusClass() == 'certificate-not-ready' --> <!-- ngIf: getCertificateStatusClass() == 'certificate-in-progress' -->
                              </a>
                              <!-- ngIf: getCertificateStatusClass() == 'certificate-ready' -->
                           </div>
                           <!-- end ngIf: course.has_certificate -->
                        </div>
                      </div>
                      <?php } ?>
                  </div>

              </div>
            </div>


<!-- Modal -->
<div class="modal fade" id="ratingModal" role="dialog">
     <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Your Rating</h4><button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

          <form class="form-horizontal" name="Rating[]" id="rating_form">
            <!-- <input type="text" id="modal_star_reating" name="data[Rating][ratting]" class="rating rating-loading" value="2" data-size="xs" title=""></input> -->
            <div id="modal_star_reating" class="rating" ></div>
            <input type="hidden"  name="data[Rating][post_id]" value="<?php echo $postDetails['Post']['id']; ?>"></input>
            <input type="hidden"  name="data[Rating][user_id]" value="<?php echo $userid; ?>"></input>
            <input type="hidden"  name="data[Rating][id]" value="<?php if(isset($rating) && $rating!=''){ echo $rating; } ?>"></input>
              <div class="form-group row mt-3">
                <label class="control-label col-sm-2" for="email">Review:</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="email" name="data[Rating][comment]" placeholder="Enter Your Review"><?php if(!empty($RatingExit) && $RatingExit['Rating']['comment']!='' ){ echo $RatingExit['Rating']['comment'];}?></textarea>
                </div>
              </div>
              <?php //echo '<pre>'; print_r($RatingExit['Rating']); echo '</pre>';?>
              <div class="form-group row">
                <label class="control-label col-sm-5" for="pwd">Are you learning valuable information?:</label>
                <div class="col-sm-7">
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="1" <?php if(!empty($RatingExit) && $RatingExit['Rating']['valueable_information']== 1 ){ echo 'checked';}?> name="data[Rating][valueable_information]"><span class="radio-label"><span class="answer-label">Yes</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="0" <?php if(!empty($RatingExit) && $RatingExit['Rating']['valueable_information']== 0 ){ echo 'checked';}?> name="data[Rating][valueable_information]"><span class="radio-label"><span class="answer-label">No</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="2" <?php if(!empty($RatingExit) && $RatingExit['Rating']['valueable_information']== 2 ){ echo 'checked';}?> name="data[Rating][valueable_information]"><span class="radio-label"><span class="answer-label">Not Sure</span></span></label></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-5" for="pwd">Are the explanations of concepts clear?
                  :</label>
                <div class="col-sm-7">
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="1" <?php if(!empty($RatingExit) && $RatingExit['Rating']['concept_explanation']== 1 ){ echo 'checked';}?> name="data[Rating][concept_explanation]"><span class="radio-label"><span class="answer-label">Yes</span></span></label></div>
                   <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="0" <?php if(!empty($RatingExit) && $RatingExit['Rating']['concept_explanation']== 0 ){ echo 'checked';}?> name="data[Rating][concept_explanation]"><span class="radio-label"><span class="answer-label">No</span></span></label></div>
                   <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="2" <?php if(!empty($RatingExit) && $RatingExit['Rating']['concept_explanation']== 2 ){ echo 'checked';}?> name="data[Rating][concept_explanation]"><span class="radio-label"><span class="answer-label">Not Sure</span></span></label></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-5" for="pwd">Is the instructor's delivery engaging?:</label>
                <div class="col-sm-7">
                  <div class="answer-choice radio-button radio"><label><input name="data[Rating][delivery_engaging]" class="user-success" type="radio" value="1" <?php if(!empty($RatingExit) && $RatingExit['Rating']['delivery_engaging']== 1 ){ echo 'checked';}?> name="data[Rating][delivery_engaging]"><span class="radio-label"><span class="answer-label">Yes</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input name="data[Rating][delivery_engaging]" class="user-success" type="radio" value="0" <?php if(!empty($RatingExit) && $RatingExit['Rating']['delivery_engaging']== 0 ){ echo 'checked';}?> name="data[Rating][delivery_engaging]"><span class="radio-label"><span class="answer-label">No</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input name="data[Rating][delivery_engaging]" class="user-success" type="radio" value="2" <?php if(!empty($RatingExit) && $RatingExit['Rating']['delivery_engaging']== 2 ){ echo 'checked';}?> name="data[Rating][delivery_engaging]"><span class="radio-label"><span class="answer-label">Not Sure</span></span></label></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-5" for="pwd">Are there enough opportunities to apply what you are learning?:</label>
                <div class="col-sm-7">
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="1" <?php if(!empty($RatingExit) && $RatingExit['Rating']['opportunities']== 1 ){ echo 'checked';}?> name="data[Rating][opportunities]"><span class="radio-label"><span class="answer-label">Yes</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="0" <?php if(!empty($RatingExit) && $RatingExit['Rating']['opportunities']== 0 ){ echo 'checked';}?> name="data[Rating][opportunities]"><span class="radio-label"><span class="answer-label">No</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="2" <?php if(!empty($RatingExit) && $RatingExit['Rating']['opportunities']== 2 ){ echo 'checked';}?> name="data[Rating][opportunities]"><span class="radio-label"><span class="answer-label">Not Sure</span></span></label></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-5" for="pwd">Is the course delivering on your expectations?:</label>
                <div class="col-sm-7">
                	<div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="1" <?php if(!empty($RatingExit) && $RatingExit['Rating']['expectations']== 1 ){ echo 'checked';}?> name="data[Rating][expectations]"><span class="radio-label"><span class="answer-label">Yes</span></span></label></div>
                	<div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="0" <?php if(!empty($RatingExit) && $RatingExit['Rating']['expectations']== 0 ){ echo 'checked';}?> name="data[Rating][expectations]"><span class="radio-label"><span class="answer-label">No</span></span></label></div>
                    <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="2" <?php if(!empty($RatingExit) && $RatingExit['Rating']['expectations']== 2 ){ echo 'checked';}?> name="data[Rating][expectations]"><span class="radio-label"><span class="answer-label">Not Sure</span></span></label></div>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-5" for="pwd">Is the instructor knowledgeable about the topic?:</label>
                <div class="col-sm-7">
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="1" <?php if(!empty($RatingExit) && $RatingExit['Rating']['knowledgeable']== 1 ){ echo 'checked';}?> name="data[Rating][knowledgeable]"><span class="radio-label"><span class="answer-label">Yes</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="0" <?php if(!empty($RatingExit) && $RatingExit['Rating']['knowledgeable']== 0 ){ echo 'checked';}?> name="data[Rating][knowledgeable]"><span class="radio-label"><span class="answer-label">No</span></span></label></div>
                  <div class="answer-choice radio-button radio"><label><input  class="user-success" type="radio" value="2" <?php if(!empty($RatingExit) && $RatingExit['Rating']['knowledgeable']== 2 ){ echo 'checked';}?> name="data[Rating][knowledgeable]"><span class="radio-label"><span class="answer-label">Not Sure</span></span></label></div>
                </div>
              </div>


          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> <button type="button" id="form_submit" class="btn btn-primary">Submit Your Review</button>
        </div>
      </div>
      </form>

     </div>
</div>

<script type="text/javascript">
$(function () {
  $("#rateYo ").rateYo({
    rating: 1.5,
    halfStar: true
  });
});

$(function () {
  $("#modal_star_reating").rateYo({
    rating: 2.5,
    halfStar: true
  });
});
</script>

<script src="<?php echo $this->webroot;?>star_rating/js/star-rating.js" type="text/javascript"></script>
<script>


$(document).ready(function(){
     var rating;
     $('.rating,.kv-gly-star,.kv-gly-heart,.kv-uni-star,.kv-uni-rook,.kv-fa,.kv-fa-heart,.kv-svg,.kv-svg-heart').on(
      'change', function () {
          //console.log('Rating selected: ' + $(this).val());
          rating = $(this).val();
      });
     $(".rating").click(function(){
          var rating = $(this).rateYo("option", "rating");
          console.log('Rating selected: ' + rating);
          $('#modal_star_reating').rateYo('option','rating',rating);
          $("#ratingModal").modal('show');
     });

  $("#form_submit").click(function(){
      $.ajax({
          url: '<?php echo $this->webroot; ?>ratings/ajaxAddRating',
          type: 'POST',
          dataType: 'json',
          data: $("#rating_form").serialize(),
          success: function (data) {
              //console.log(data);
              if(data.Ack == 1){
                $("#ratingModal").modal('hide');
                var url = window.location.href;
                window.location.href = url;
              }
          }
    });
  });



});
</script>

<script>
var myVideo=document.getElementById("video1");

function playPause()
{
if (myVideo.paused)
  myVideo.play();
else
  myVideo.pause();
}

function makeBig()
{
myVideo.width=560;
}

function makeSmall()
{
myVideo.width=320;
}

function makeNormal()
{
myVideo.width=420;
}
</script>

<script>



</script>
