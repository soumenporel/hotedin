<?php //pr($course_status);?>
<style>
.publicprofile li a .fa-chevron-right{ float:right 	!important }
.publicprofile li a i{ float:left 	!important; padding-top: 3px; padding-right: 5px; }
.publicprofile li a .fa-check{ color: green; }
#approve_status {     
	color: #08d6e8;
    text-align: left;
    display: block;
    margin: 25px 0 16px 22px; 
}
.fa-circle-o::before {
    font-size: 20px;
}
.fa-stack-1x {
    bottom: 1px;
    left: -1px;
}
.fa-stack {
    margin-left: -4px;
    width: 1.4em;
}
.publicprofile li a .fa-chevron-right {
    margin-top: 4px;
}
a:hover, a:focus {
    text-decoration: none;
}
.submit_but{
	text-align: center;
}
</style>
<?php
if(isset($post_dtls['PostImage']) && !empty($post_dtls['PostImage'])){
$img_array = end($post_dtls['PostImage']);
$img = $this->webroot.'img/post_img/'.$img_array['originalpath'];
}
else{
$img = $this->webroot.'img/placeholder.png';
} 
?>
<div class="publicprofile">
	<div class="profileimg">
		<img src="<?php echo $img; ?>">
		<!-- <img src="https://s-media-cache-ak0.pinimg.com/originals/f6/ac/08/f6ac0865db98ead4e3625b95822dc0d5.jpg"> -->
		<h2><?php echo $post_dtls['Post']['post_title']; ?></h2>
		<p><?php echo $post_dtls['Post']['post_subtitle']; ?></p>
	</div>
	<ul>
		<li><a href="<?php echo $this->webroot; ?>posts/course_goals/<?php echo $post_dtls['Post']['slug']; ?>">
			<?php if(!array_key_exists("course_goals",$course_status)){ ?>
			<span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">1</span>
			</span>
			<?php } 
			else{ ?>
			<i class="fa fa-check" aria-hidden="true"></i>
			<?php } ?> 

			Course Goals <i class="fa fa-chevron-right"></i>
		</a></li>
		<li><a href="<?php echo $this->webroot; ?>posts/test_video/<?php echo $post_dtls['Post']['slug']; ?>">
			
			<?php if(!array_key_exists("test_video",$course_status)){ ?>
			<span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">2</span>
			</span>
			<?php } 
			else{ ?>
			<i class="fa fa-check" aria-hidden="true"></i>
			<?php } ?>  

			Test Video <i class="fa fa-chevron-right"></i>
		</a></li>
		<li><a href="<?php echo $this->webroot; ?>posts/curriculum_test/<?php echo $post_dtls['Post']['slug']; ?>">

			<?php if(!array_key_exists("curriculum",$course_status)){ ?>
			<span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">3</span>
			</span>
			<?php } 
			else{ ?>
			<i class="fa fa-check" aria-hidden="true"></i>
			<?php } ?>  

			Curriculum <i class="fa fa-chevron-right"></i></a></li>
		<li><a href="<?php echo $this->webroot; ?>posts/course_landing_page/<?php echo $post_dtls['Post']['slug']; ?>">

			<?php if(!array_key_exists("landing_page_status",$course_status)){ ?>
			<span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">4</span>
			</span>
			<?php } 
			else{ ?>
			<i class="fa fa-check" aria-hidden="true"></i>
			<?php } ?>  

			Course Landing Page <i class="fa fa-chevron-right"></i></a></li>
			
			<!-- <li><a href="<?php echo $this->webroot; ?>posts/price_coupons/<?php echo $post_dtls['Post']['slug']; ?>"> -->

			<?php if(!array_key_exists("price_coupons",$course_status)){ ?>
			<!-- <span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">5</span>
			</span> -->
			<?php } 
			else{ ?>
			<!-- <i class="fa fa-check" aria-hidden="true"></i> -->
			<?php } ?>  

			<!-- Price & Coupons <i class="fa fa-chevron-right"></i></a></li> -->
			
			<li><a href="<?php echo $this->webroot; ?>posts/automatic_message/<?php echo $post_dtls['Post']['slug']; ?>">

			<?php if(!array_key_exists("automatic_message",$course_status)){ ?>
			<span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">5</span>
			</span>
			<?php } 
			else{ ?>
			<i class="fa fa-check" aria-hidden="true"></i>
			<?php } ?>  

			Automatic Messages <i class="fa fa-chevron-right"></i></a></li>
                        
                        <li><a href="<?php echo $this->webroot; ?>posts/addassignment/<?php echo $post_dtls['Post']['slug']; ?>">

			<span class="fa-stack">
			  <i class="fa fa-circle-o fa-stack-2x fa-circle-thin"></i>
			  <span class="fa-stack-1x">6</span>
			</span>

			Add assignment <i class="fa fa-chevron-right"></i></a></li>
	</ul>
	<?php
	$count = count($course_status); 
	if($count==6){
		if($post_dtls['Post']['is_approve']==2){ ?>
			<div class="submit_but"  ><a href="<?php echo $this->webroot;?>posts/approve_status/<?php echo $post_dtls['Post']['slug'];?>"><button type="button" class="btn btn-default">Submit for review</button></a></div>
	<?php
		}
		 if($post_dtls['Post']['is_approve']==0){ ?>
			<p class="submit" id="approve_status">The Course Is In Review</p>
   <?php } 
	}
	else{ ?> 
	<div class="submit_but"><button type="button" class="btn btn-default" style="background: #E5E1E1;" disabled >Submit for review</button></div>
	<?php } ?>
	<p class="submit"><a href="#">Why can't I submit?</a></p>
</div>