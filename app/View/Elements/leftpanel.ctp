<style>
    .file-upload-button{
        display:none;
    }
    .file-upload-input{
        display:none;
    }
</style>
<div class="publicprofile">
    <div class="profileimg">
        <span id="replaceimage">
            <img src="<?php if (isset($userdetails['User']['user_image']) && $userdetails['User']['user_image'] != '') { ?>
                     <?php echo $this->webroot; ?>user_images/<?php
                     echo $userdetails['User']['user_image'];
                 } else {
                     echo $this->webroot;
                     ?>img/profile_img.jpg<?php } ?>"
                 width="190" height="190" alt=""/></span>
        <div class="update-pic-icon" >
            <div class="update-pic-img" ><i style="cursor:pointer;"  class="fa fa-pencil-square-o" aria-hidden="true" id="uploadbox">
                </i><input type="file" name="uploadbox" class="uploadbox" id="form" style="display:none">
            </div>
        </div>
        <h2><?php echo $userdetails['User']['first_name']; ?></h2>
        <p><?php
            if ($userdetails['User']['admin_type'] == 1) {
                echo 'Instructor';
            } else {
                echo 'Student';
            }
            ?></p>
    </div>
    <ul>
        <?php 
        if ($userdetails['User']['admin_type'] == 1) { 
            $dashboardpage="instructordashboard";
        }
        else
        {
            $dashboardpage="studentdashboard";
        }
            ?>
            <li>
                <a href="<?php echo $this->webroot; ?>users/<?php echo $dashboardpage; ?>"> Dashboard <i class="fa fa-chevron-right"></i>
                </a>
            </li>

        <li>
            <a href="<?php echo $this->webroot . 'users/editprofile'; ?>">Edit Profile <i class="fa fa-chevron-right"></i></a>
        </li>
        <li><a href="<?php echo $this->webroot . 'users/user_image'; ?>">Photo <i class="fa fa-chevron-right"></i></a></li>
<?php if ($userdetails['User']['admin_type']!= 1) { ?>
        <li><a href="<?php echo $this->webroot . 'user_courses'; ?>">My Courses <i class="fa fa-chevron-right"></i></a></li>
<?php } ?>
<?php if ($userdetails['User']['admin_type'] == 1) { ?>
        
         <li><a href="<?php echo $this->webroot . 'users/myaddedcourse'; ?>">My Uploaded Courses <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'posts/add_course'; ?>">Add Course <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'users/skills'; ?>">Skills <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'users/langueages'; ?>">Languages <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'users/addportfolio'; ?>">Add Portfolio <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'users/myportfolio'; ?>">My Portfolio <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'users/addaward'; ?>">Add Award <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="<?php echo $this->webroot . 'users/myaward'; ?>">My Award <i class="fa fa-chevron-right"></i></a></li>

            <li><a href="<?php echo $this->webroot . 'users/myreview'; ?>">Review Ratings <i class="fa fa-chevron-right"></i></a></li>
<?php } ?>    
        <!-- 
            <li><a href="#">Credit Cards <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="#">Privacy <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="#">Notifications <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="#">Preferences <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="#">API Clients <i class="fa fa-chevron-right"></i></a></li>
            <li><a href="#">Danger Zone <i class="fa fa-chevron-right"></i></a></li> 
        -->
<?php
//if($userdetails['User']['admin_type']==1){echo 
//'<li><a href="'.$this->webroot.'posts/user_courses">Your Courses <i class="fa fa-chevron-right"></i></a></li>';} 
?>
    </ul>
</div>
<script>


    $(document).on("change", "#form", function () {

        var file_data = $("#form").prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
        //form_data.append("user_id", 123)                 // Adding extra parameters to form_data
        $.ajax({
            url: "<?php echo $this->webroot . 'users/upload_image'; ?>",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data, // Setting the data attribute of ajax with file_data
            type: 'post', success: function (result) {
                $("#replaceimage").html('');
                $("#replaceimage").html('<img src="<?php echo $this->webroot; ?>user_images/' + result + '" />');
                $("#replaceimageTop").html('');
                $("#replaceimageTop").html('<img src="<?php echo $this->webroot; ?>user_images/' + result + '" />');
            }
        });
    })
    $(document).on("click", "#uploadbox", function () {
        $("#form").trigger("click");
    });


</script>