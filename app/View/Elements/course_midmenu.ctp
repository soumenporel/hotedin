<nav class="navbar navbar-toggleable-md navbar-light bg-faded my-navBar" style=" min-height: 60px; ">
                  
                  <div class="container">
                      <?php 
                      $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                        $uri_segments = explode('/', $uri_path);

                       
                       
                      ?>
                   <!-- <button class="navbar-toggler navbar-toggler-right" data-target="#course-taking-bottom-navbar" style="z-index: 9999; margin-top: 15px; "  type="button" data-toggle="collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" >  
                      <span class="navbar-toggler-icon"></span>                     
                    </button>-->
                     <div class="" id="course-taking-bottom-navbar" >
                        <ul class="navbar-nav mr-auto">
                              <li ui-sref-active="active" class="menues nav-item" > 
                                   <a ui-sref="base.dashboard.overview" ng-click="navbarCollapsed = true" translate="" href="<?php echo $this->webroot.'learns/overview/'.$postDetails['Post']['slug']; ?>" class="nav-link <?php if($uri_segments[4]=='overview'){ echo 'active'; } ?>">
                                        <span>Overview</span>
                                   </a> 
                              </li> 
                              <li ui-sref-active="active" class="menues nav-item" > 
                                   <a ui-sref="base.dashboard.content" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-content-tab" href="<?php echo $this->webroot;?>learns/course_content/<?php echo $postDetails['Post']['slug'];?>"  class="nav-link <?php if($uri_segments[4]=='course_content'){ echo 'active'; } ?>">
                                        <span>Course Content</span>
                                   </a> 
                              </li> 
                              <li ng-class="{active: ('base.dashboard.questions' | includedByState) || ('base.dashboard.question-details' | includedByState)}" class="menues nav-item" > 
                                   <a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/questions/'.$postDetails['Post']['slug']; ?>"  class="nav-link <?php if($uri_segments[4]=='questions'||$uri_segments[4]=='answers'){ echo 'active'; } ?>">
                                        <span>Q&amp;A</span>
                                   </a> 
                              </li>
                              <?php 
                              if($_SESSION['usertype']==2)
                                  { 
                                  if($_SESSION['membership_plan_id']==6||$_SESSION['membership_plan_id']==5||$_SESSION['membership_plan_id']==4)
                                  {
                                     ?>
                              <li class="menues nav-item" > 
                                   <a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/discussion/'.$postDetails['Post']['slug']; ?>"  class="nav-link <?php if($uri_segments[4]=='discussion'){ echo 'active'; } ?>">
                                        <span>Discussion forum</span>
                                   </a> 
                              </li>
                              <?php
                                  }
                              }
                              ?>
                              
                              
                              <?php 
                              if($_SESSION['usertype']==2)
                                  { 
                                  if($_SESSION['membership_plan_id']==6||$_SESSION['membership_plan_id']==5)
                                  {
                                     ?>
                              <li class="menues nav-item" > 
                                   <a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/assignment/'.$postDetails['Post']['slug']; ?>"  class="nav-link <?php if($uri_segments[4]=='assignment'){ echo 'active'; } ?>">
                                        <span>Assignment</span>
                                   </a> 
                              </li>
                              <?php
                                  }
                              }
                              else
                                  
                              {
                                ?>
                               <li class="menues nav-item" > 
                                   
                                   <a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/assignment_submitted/'.$postDetails['Post']['slug']; ?>"  class="nav-link <?php if($uri_segments[4]=='assignment_submitted'){ echo 'active'; } ?>">
                                        <span>Assignment</span>
                                   </a> 
                              </li>
                              <?php
                              }
                              ?>
                              
                              
                               
                           <li ui-sref-active="active" class="nav-item"> 
                              <a ui-sref="base.dashboard.bookmarks" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-bookmark-tab" href="<?php echo $this->webroot;?>learns/bookmarks/<?php echo $postDetails['Post']['slug'];?>"  class="nav-link <?php if($uri_segments[4]=='bookmarks'){ echo 'active'; } ?>">
                                   <span>Bookmarks</span>
                              </a> 
                           </li>
                          
                            <li class="menues nav-item" > 
                                   <?php if($_SESSION['usertype']==1){ $announcementLink1='instructor_announcement'; } else { $announcementLink1='announcement'; }?>
                                   <a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/'.$announcementLink1.'/'.$postDetails['Post']['slug']; ?>"  class="nav-link <?php if($uri_segments[4]==$announcementLink1||$uri_segments[4]=='assignment_evalute'){ echo 'active'; } ?>">
                                        <span>Announcements</span>
                                   </a> 
                              </li>
<!--                           <li ui-sref-active="active" class="dropdown nav-item">
                              <a href="javascript:void(0)" id="dLabel" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span>Options</span> 
                              </a> 
                               ngInclude: optionsDropdownTemplateUrl 
                              <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" ng-click="$event.stopPropagation();" ng-include="optionsDropdownTemplateUrl" style="width: 250px">
                                <li role="presentation" class="dropdown-item">
                                        ngIf: !course.favorite_time 
                                       <input type="hidden" id="hidden_user_id" value='<?php echo $userid?>'></input>
                                       <a role="menuitem" tabindex="-1" ng-click="course.favoriteCourse()" ng-if="!course.favorite_time" translate="">
                                           <span id="<?php echo $favoritexist ? 'unfavorite_this_course' : 'favorite_this_course'; ?>" data-id="<?php echo $postDetails['Post']['id']; ?>" ><?php echo $favoritexist ? 'Unfavorite this course' : 'Favorite this course'; ?></span>
                                       </a>
                                        end ngIf: !course.favorite_time  
                                        ngIf: course.favorite_time  
                                    </li>
                                    <li role="presentation" class="dropdown-item">
                                        ngIf: !course.archive_time 
                                       <a role="menuitem" tabindex="-1" ng-click="course.archiveCourse()" ng-if="!course.archive_time" translate="">
                                           <span id="<?php echo $archivexist ? 'unarchive_this_course' : 'archive_this_course'; ?>" data-id="<?php echo $postDetails['Post']['id']; ?>" ><?php echo $archivexist ? 'Unarchive this course' : 'Archive this course'; ?></span>
                                      </a>
                                       end ngIf: !course.archive_time  
                                       ngIf: course.archive_time  
                                </li> 
                                 <li role="presentation" class="dropdown-divider"></li>
                                 <li role="presentation"  class="dropdown-item">
                                   <a role="menuitem" tabindex="-1" ng-click="scrollToInstructor()" translate="">
                                        <span>Instructor profile</span>
                                   </a>
                                 </li>
                                 <li role="presentation"  class="dropdown-item">
                                   <a role="menuitem" tabindex="-1" ng-show="isGiftCourseEnabled &amp;&amp; course.is_paid &amp;&amp; !course.is_private" ng-href="https://www.udemy.com/gift/learn-wordpress-in-57-minutes/" target="_blank" translate="" href="https://www.udemy.com/gift/learn-wordpress-in-57-minutes/" class="ng-hide">
                                        <span>Gift this course</span>
                                   </a>
                                 </li>
                                 <li role="presentation" class="dropdown-item" >
                                   <a role="menuitem" tabindex="-1" ng-href="https://www.udemy.com/support/" target="_blank" translate="" href="https://www.udemy.com/support/">
                                        <span>Support</span>
                                   </a>
                                 </li>
                                  ngIf: reportAbusePopup.enabled 
                                 <li role="presentation" ng-if="reportAbusePopup.enabled"  class="dropdown-item">
                                    <a role="menuitem" tabindex="-1" ng-click="reportAbusePopup.open=true">
                                       <i class="udi udi-flag mr5"></i> 
                                       <span translate="">
                                        <span>Report abuse</span>
                                       </span> 
                                       <popup open="reportAbusePopup.open" ng-href="/feedback/report?related_object_type=course&amp;related_object_id=552598" style="display: block;" href="/feedback/report?related_object_type=course&amp;related_object_id=552598"> </popup>
                                    </a>
                                 </li>
                                  end ngIf: reportAbusePopup.enabled  
                                 <li role="presentation" class="dropdown-divider"></li>
                                  ngRepeat: (key, value) in course.notification_settings track by $id(key) 
                                 <li role="presentation" ng-repeat="(key, value) in course.notification_settings track by $id(key)"  class="dropdown-item"> 
                                   <a role="menuitem" tabindex="-1" class="email-settings ng-hide" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> <span class="checkbox1"> <label> <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" checked="checked" type="checkbox"> <span class="checkbox-label email-settings__title">  </span> </label> </span> </a> </li>
                                  end ngRepeat: (key, value) in course.notification_settings track by $id(key) 
                                 <li role="presentation" ng-repeat="(key, value) in course.notification_settings track by $id(key)"  class="dropdown-item"> <a role="menuitem" tabindex="-1" class="email-settings" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> <span class="checkbox1"> <label> <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" type="checkbox"> <span class="checkbox-label email-settings__title"> New announcement emails </span> </label> </span> </a> </li>
                                  end ngRepeat: (key, value) in course.notification_settings track by $id(key) 
                                 <li role="presentation" ng-repeat="(key, value) in course.notification_settings track by $id(key)"  class="dropdown-item"> <a role="menuitem" tabindex="-1" class="email-settings" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> <span class="checkbox1"> <label> <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" type="checkbox"> <span class="checkbox-label email-settings__title"> Promotional emails </span> </label> </span> </a> </li>
                                  end ngRepeat: (key, value) in course.notification_settings track by $id(key)  
                                 <li role="presentation" class="dropdown-divider"></li>
                                 <li role="presentation"  class="dropdown-item">
                                     ngIf: course.canBeUnenrolled   ngIf: course.refundFeatureEnabled 
                                    <div ng-if="course.refundFeatureEnabled">
                                        <a target="_blank" ng-href="" ng-show="course.was_purchased_by_student &amp;&amp; !course.canBeUnenrolled" ng-class="{'unsubscribe': course.is_refundable, 'cannot-refund': !course.is_refundable}" class="pl10 pb10 ng-hide cannot-refund" translate=""><span>Request a refund</span></a>  
                                       <div ng-hide="course.is_refundable" class="cannot-refund-info">
                                           <div ng-show="course.was_paid_by_student" translate="" class="ng-hide"><span>This course was purchased outside the 30-day refund policy and cannot be refunded.</span></div> 
                                          <div ng-show="!course.was_paid_by_student" translate="" class=""><span>This course was a free enrollment and cannot be refunded.</span></div>
                                       </div>
                                    </div>
                                     end ngIf: course.refundFeatureEnabled  
                                 </li>
                              </ul>
                           </li>-->
                        </ul>
                     </div>
                  </div>
               </nav>