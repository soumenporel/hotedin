<?php
//pr($orders);
//pr($settings);
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?php echo $this->webroot; ?>adminFiles/css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>--> 

<div class="contents index">
    <h2 style="width:400px;float:left;"><?php echo __('Sales Report'); ?></h2>


    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Search
                </header>
                <div class="panel-body">
<!--                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <label class="sr-only" for="start_date">Email address</label>
                            <input class="form-control dateTimePicker" id="start_date" name="start_date" placeholder="Start Date" type="text" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="end_date">Password</label>
                            <input class="form-control dateTimePicker" id="end_date" name="end_date" placeholder="End Date" type="text" />
                        </div>

                        <button type="submit" class="btn btn-primary">Search</button>
                    </form>-->

                    <?php
                    $base_url = array('controller' => 'sales_reports', 'action' => 'admin_index');
                    echo $this->Form->create("Filter",array('url' => $base_url, 'class' => 'form-inline'));
                    
                    echo $this->Form->input("start_date", array('label' => false, 'class' => 'form-control dateTimePicker', 'placeholder' => 'Start Date', 'div' => array('class' => 'form-group')));
                   
                    echo $this->Form->input("end_date", array('label' => false, 'class' => 'form-control dateTimePicker', 'placeholder' => 'End Date', 'div' => array('class' => 'form-group')));

                    echo $this->Form->submit("Search", array('div' => false, 'class' => 'btn btn-primary'));

                    // To reset all the filters we only need to redirect to the base_url
                    echo "<div class='submit actions'>";
                    echo $this->Html->link("Reset",$base_url);
                    echo "</div>";
                    echo $this->Form->end();
                    ?>

                </div>
            </section>

        </div>
    </div>


    <table cellpadding="0" cellspacing="0"  id="sortable">
        <thead>
            <tr>
                <th>
                    <?php echo __('id'); ?>
                </th>
                <th>
                    <?php echo __('Name'); ?>
                </th>
                <th>
                    <?php echo __('Transaction Id'); ?>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('quantity'); ?><?php //echo __('quantity'); ?>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('amount'); ?><?php //echo __('amount'); ?>
                </th>

                <th>
                    <?php echo $this->Paginator->sort('payment_date'); ?><?php //echo __('payment_date'); ?>
                </th>
                <th>
                    <?php echo __('status'); ?>
                </th>
                <th>
                    <?php echo __('Actions'); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($orders as $key => $order) :
                ?>
                <tr data-id="<?php echo $order['Order']['id']; ?>">
                    <td>
                        <?php echo ++$key; ?>&nbsp;
                    </td>
                    <td>
                        <?php echo h($order['User']['first_name'] . ' ' . $order['User']['last_name']); ?>
                    </td>
                    <td>
                        <?php echo h($order['Order']['transaction_id']); ?>
                    </td>
                    <td>
                        <?php echo h($order['Order']['quantity']); ?>
                    </td>
                    <td>
                        <?php echo '$' . h($order['Order']['amount']); ?>
                    </td>
                    <td>
                        <?php echo h($order['Order']['payment_date']); ?>
                    </td>
                    <td>
                        <?php
                        if (h($order['Order']['status']) == 'Success') {
                            echo 'Transaction successfull';
                        } elseif (h($order['Order']['status']) == 'Failure') {
                            echo 'Transaction failure';
                        } elseif (h($order['Order']['status']) == 'Canceled') {
                            echo 'Transaction canceled';
                        }
                        ?>
                    </td>

                    <td>
                        <?php
                        echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('action' => 'view', $order['Order']['id']), array('class' => 'btn btn-success btn-xs', 'escape' => false));
                        ?>
                        <?php if($order['Order']['status']!= 'Canceled'){ ?>
                            <?php
                            echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-ban')), array('action' => 'cancel', $order['Order']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false));
                            ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            endforeach;
            ?>
        </tbody>
    </table>
    
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
    
</div>

<script>
    $(function(){
        $('.dateTimePicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
</script>