<?php //pr($orderitem);
//pr($settingvalue); ?>
<div class="blogs view">
    <h2><?php echo __('Order View'); ?></h2>
    <dl>
		<dt><?php echo __('Post Id'); ?></dt>
        <dd>
			<?php echo h($orderitemvalue[0]['Post']['id']); ?>
        </dd>
        <dt><?php echo __('Post Title'); ?></dt>
        <dd>
			<?php echo h($orderitemvalue[0]['Post']['post_title']); ?>
        </dd>
        <dt><?php echo __('Instructor Name'); ?></dt>
        <dd>
			<?php echo h($orderitemvalue[0]['Post']['User']['first_name'].' '.$orderitemvalue[0]['Post']['User']['last_name']); ?>
        </dd>
		<dt><?php echo __('Price'); ?></dt>
        <dd>
			<?php echo '$'.h($orderitemvalue[0]['Post']['price']); ?>
        </dd>
		<dt><?php echo __('Admin Price'); ?></dt>
        <dd>
			<?php echo '$'.h(($settingvalue[0]['Setting']['set_commission']/100)*($orderitemvalue[0]['Post']['price'])); ?>
        </dd>
		<dt><?php echo __('Instructor Price'); ?></dt>
        <dd>
			<?php echo '$'.h($orderitemvalue[0]['Post']['price']-(($settingvalue[0]['Setting']['set_commission']/100)*($orderitemvalue[0]['Post']['price']))); ?>
        </dd>

        
    </dl>
</div>
<?php //echo($this->element('admin_sidebar'));?>
