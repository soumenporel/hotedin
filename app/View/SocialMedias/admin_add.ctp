<div class="categories form">
<?php echo $this->Form->create('SocialMedia',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Social Medias'); ?></legend>
	<?php
		
		echo $this->Form->input('title',array('required'=>'required'));
		echo $this->Form->input('url',array('required'=>'required'));
		echo $this->Form->input('icon',array('type'=>'file',array('required'=>'required')));
		echo $this->Form->input('status',['type' => 'checkbox']);
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>

