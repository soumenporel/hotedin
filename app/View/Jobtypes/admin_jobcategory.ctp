
<div class="row-fluid">
    <div class="span12">
        <div class="categories index">
            <h2><?php echo __('Job Categories'); ?></h2>
            <div>
                <table style="width: 100%;">
                    <tr>

                        <td>
                            <form name="Searchuserfrm" class="form-inline" method="post" action="" id="Searchuserfrm">
                                <input type="text" name="keyword" value="<?php echo isset($keywords) ? stripcslashes($keywords) : ''; ?>" placeholder="Search by Keyword.">
                                <select name="search_is_active" id="search_is_active">
                                    <option value="" >Select Option</option>
                                    <option value="1" <?php echo (isset($Newsearch_is_active) && $Newsearch_is_active == '1') ? 'selected' : ''; ?>>Active</option>
                                    <option value="0" <?php echo (isset($Newsearch_is_active) && $Newsearch_is_active == '0') ? 'selected' : ''; ?>>Inactive</option>
                                </select>
                                <button class="btn btn-primary" type="submit" name="search">Search</button>
                            </form>
                        </td>
                        <td>
                            <a href="<?php echo($this->webroot); ?>admin/categories/addjob" class="btn btn-info pull-right" style="margin-bottom: 20px;"><i class="fa fa-plus"></i> Add Parent Category</a>
                        </td>
                    </tr>      
                </table>
            </div>

            <div class="widget blue">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i>Category Table</h4>
                    <span class="tools">
                        <a href="javascript:;" class="icon-chevron-down"></a>
                        <a href="javascript:;" class="icon-remove"></a>
                    </span>
                </div>
                <div class="widget-body">
                    <table class="table table-striped table-bordered" id="sample_1">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('image'); ?></th>
                                <th><?php echo $this->Paginator->sort('category_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('category_description'); ?></th>
                                <th><?php echo $this->Paginator->sort('sortby'); ?></th>
                                <th><?php echo 'Subcategories'; ?></th>
                                <th><?php echo $this->Paginator->sort('active'); ?></th>
                                <th><?php echo _('Change Status'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $CatCnt = 0;
                            foreach ($categories as $category):
                                $CatCnt++;
                                //pr($category);
                                ?>
                            <tr class="odd gradeX">
                                <td><?php echo $CatCnt; //echo h($category['Category']['id']);     ?>&nbsp;</td>
                                <td><img src="<?php if(isset($category['CategoryImage']['0']['originalpath']) && $category['CategoryImage']['0']['originalpath']!='') { echo $this->webroot; ?>/img/cat_img/<?php echo $category['CategoryImage']['0']['originalpath']; } ?>" style="height:30px;" /></td>
                                <td><?php echo $category['Category']['category_name']; ?></td>
                                <td><?php echo substr($category['Category']['category_description'],0,50); ?></td>
                                <td><?php echo $category['Category']['sortby']; ?>&nbsp;</td>
                                <td><?php echo count($category['Children']); ?>&nbsp;</td>
                                <td><?php if ($category['Category']['status'] == 1) { ?> <img id="status<?php echo $category['Category']['id'] ?>" src="<?php echo $this->webroot; ?>/img/success-01-128.png" style="height:30px;" /><?php } else { ?><img id="status<?php echo $category['Category']['id'] ?>" src="<?php echo $this->webroot; ?>/img/cross-512.png" style="height:30px;" /><?php } ?>&nbsp;</td>
                                <td>
                                    <input <?php if($category['Category']['status']!='' && $category['Category']['status']==1){ echo 'checked'; } ?> data-toggle="toggle" class='slider' id='slider' data-id='<?php echo $category['Category']['id']; ?>' value='<?php echo $category['Category']['id']; ?>' data-on="Activated" data-off="Disabled" data-onstyle="success" data-offstyle="danger" type="checkbox">
                                </td>
                                <td>
                                        <?php
                                        echo $this->Html->link($this->Html->tag('i', 'View', array('class' => 'fa fa-plus')), array('action' => 'view', $category['Category']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                                        ?>

                                        <?php
                                        echo $this->Html->link($this->Html->tag('i', 'Edit', array('class' => 'fa fa-edit')), array('action' => 'editjob', $category['Category']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                                        ?>

                                        <?php
                                        echo $this->Form->postLink($this->Html->tag('i', 'Delete', array('class' => 'fa fa-times')), array('action' => 'deletejobcat', $category['Category']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $category['Category']['id']));
                                        ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>
<?php //echo $this->element('admin_sidebar');  ?>
<script src="<?php echo $this->webroot; ?>adminFiles/js/bootstrap-toggle.js"></script>
<script>
    $(document).ready(function () {
        $(function () {
            $('#toggle-two').bootstrapToggle({
                on: 'Activated',
                off: 'Disabled'
            });
        });
        $(".slider").change(function () {
            var self = $(this);
            var bid = $(this).val();

            if ($(this).is(':checked'))
            {
                var bid_type = 1;
            } // checked
            else
            {
                var bid_type = 0;
            }
            var model = 'Category';
            $.ajax({
                url: "<?php echo $this->webroot ?>Blogs/ajaxBlogstatus",
                type: "POST",
                dataType: 'json',
                data: {blogid: bid, blogstatustype: bid_type, model: model},
                success: function (result) {
                    if (result.ack == 1) {
                        $('#status' + bid).attr('src', '<?php echo $this->webroot; ?>img/success-01-128.png');
                    }
                    if (result.ack == 2) {
                        $('#status' + bid).attr('src', '<?php echo $this->webroot; ?>img/cross-512.png');
                    }
                },
            });

        });

    })
</script>
