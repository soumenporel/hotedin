<div class="categories index">
    <h2><?php echo __('JobCategory'); ?></h2>
    <div>
<table style=" border:none;">

                 <tr><a href="<?php echo($this->webroot);?>admin/Jobtypes/addcategory" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add </a>
            </tr>
            </table>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>


            <th><?php echo $this->Paginator->sort('name'); ?></th>


            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php
        $CatCnt=0;
        foreach ($categories as $category):
            $CatCnt++;
	?>
        <tr>
            <td><?php echo $CatCnt;?>&nbsp;</td>
            <td><?php echo $category['JobCategory']['title'];?></td>

            <td>

            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                        array('action' => 'editcategory', $category['JobCategory']['id']),
                        array('class' => 'btn btn-info btn-xs', 'escape'=>false)); ?>
            <?php echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                        array('action' => 'deletecategory', $category['JobCategory']['id']),
                        array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
                        __('Are you sure you want to delete # %s?', $category['JobCategory']['id'])); ?>
            </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
