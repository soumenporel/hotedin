<?php

if(!empty($cat_details['Product']))
{
?>
<section class="sub-menu" id="subb_part">
    <div class="container">
        <div class="row">
            <div class="col-md-6 middle-div">

                <?php
                foreach($cat_details['Children'] as $child)
                {
                ?>
                <div class="one_sub text-center" style="cursor:pointer;">
                    <img src="<?php echo $this->webroot ?>img/cat_img/<?php echo $child['CategoryImage'][0]['originalpath']; ?>" style="margin: 5px auto;" class="img-responsive"
                         onclick="location.href = '<?php echo $this->webroot . 'categories/' . $child['slug']; ?>'"/>
                    <p class="sub_para" onclick="location.href = '<?php echo $this->webroot . 'categories/' . $child['slug']; ?>'"><?php echo $child['category_name']; ?></p>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
</section>
<?php
}
else
{
	echo "<section class='tvshow text-center' style='min-height:500px;margin-top:20px;'><div class='watchseries2 text-center'><span class='alert alert-danger'>Sorry No Product Found.</span></div></section>";
}
?>

<?php
if(!empty($cat_details))
{
$i=0;	
foreach($cat_details['Product'] as $cat_detail)
{	

if($i==4)
{
$i=0;
}

if($i==0)
{
?>
<section class="tvshow text-center">
    <a href="#">
        <figure class="hero-image" style=" background-image: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>');background-repeat: no-repeat;background-size: 588px 401px;height: 401px;background-position:center top;margin-top: 40px;"></figure>
    </a>
    <div class="watchseries2 text-center">
        <h2><?php echo $cat_detail['product_name']; ?></h2>
        <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
        <span class="learn_more">
            <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
            <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
        </span>
    </div>
</section>
<?php } ?>

<?php
if($i==1)
{
?>


<section class="section section-features text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <a href="#"><figure class="tvprod" style="margin-top:50px; background-image: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>'); background-position:center top; background-repeat: no-repeat; height: 270px; width: 100%;"></figure></a>
                <div class="watchseries2 text-center"><h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                </div>			

                <span class="learn_more">
                    <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                    <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                </span>
            </div>
        </div>
    </div>
</section>

<?php
}
?>

<?php
if($i==2)
{
?>
<section class="section sections-third text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <figure class="tvprod3" style="background-image: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>'); background-repeat: no-repeat; background-position:center top; height: 377px;"></figure>
                <div class="watchseries2 text-center"><h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                </div>			
                <span class="learn_more">
                    <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                    <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                </span>


            </div>
        </div>
    </div>
</section>

<?php
}
?>

<?php
if($i==3)
{
?>

<section class="section section-features text-center margin-bottom30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="#"><figure class="tvprod4" style="background-image: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>'); background-repeat: no-repeat; background-position:center top; height: 377px;"></figure></a>
                <div class="watchseries2 text-center"><h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                </div>			

                <span class="learn_more">
                    <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                    <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                </span>
            </div>
        </div>
    </div>
</section>

<?php 
} 
$i++;
  }
}
?>