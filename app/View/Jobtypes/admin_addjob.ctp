<style>
    #CategoryAdminAddForm label.error1 {
        color: #b94a48;
        display: inline;
        margin-left: 10px;
        width: auto;
    }
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="widget blue">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i>Add Job Category</h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <div class="categories form">
                    <?php
                    echo $this->Form->create('Category', array(
                        'class' => 'form-horizontal',
                        'type' => 'file',
                        'inputDefaults' => array(
                            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                            'div' => array('class' => 'control-group'),
                            'label' => array('class' => 'control-label'),
                            'between' => '<div class="controls">',
                            'after' => '</div>',
                            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                        )
                    ));
                    ?>
                    <input type="hidden" name="data[Category][category_type]" value="2">
                    <fieldset>
                        <legend><?php echo __('Add Category'); ?></legend>
                        <?php
                        echo $this->Form->input('Category.category_name', array('required' => 'required', 'class' => 'span6'));
                        echo $this->Form->input('Category.category_description', array('id' => 'cat_desc', 'class' => 'ckeditor'));
                        //echo $this->Form->input('Category.category_type', array('class' => 'span6', 'options' => array('1' => 'Product', '2' => 'Job')));
			//echo $this->Form->input('Category.sortby', array('required' => 'required', 'class' => 'span6'));
                        //echo $this->Form->input('country_id');
                        //echo $this->Form->input('Category.is_principal');
                        echo $this->Form->input('Category.status');
                        //echo $this->Form->input('Category.show_in_homepage');
                        ?>
                        <!--<label >(Check This to Show The Category in The Home Page)</label>-->
                        <?php
                        //echo $this->Form->input('Category.category_of_the_day');
                        echo $this->Form->input('CategoryImage.originalpath', array('type' => 'file', 'class' => '', 'onchange' => 'validateImageBeforeUpload(this)', 'after' => '<span class="help-block">Supported file JPG, PNG, JPEG, GIF</span></div>'));
                        ?>
                    </fieldset>

                    <?php //echo $this->Form->input('Category.template', array('type' => 'hidden', 'class' => 'span6', 'id' => 'temp_id')); ?>

                     <!--<fieldset>
                        <legend><?php echo __('Select Template'); ?></legend>
                        <img class="imgcls1" src="<?php echo $this->webroot ?>template_image/Template1.png" id="1" style="width:100px;cursor:pointer;" onclick="csTemplate1()"><img src="<?php echo $this->webroot ?>template_image/green.png" style="width:30px;margin-top: -35px;display:none;" id="greenimg1">&nbsp;&nbsp;&nbsp;
                        <img class="imgcls2" src="<?php echo $this->webroot ?>template_image/Tempalte2.png" id="2" style="width:100px;cursor:pointer;" onclick="csTemplate2()"><img src="<?php echo $this->webroot ?>template_image/green.png" style="width:30px;margin-top: -35px;display:none;" id="greenimg2">&nbsp;&nbsp;&nbsp;
                        <img class="imgcls3" src="<?php echo $this->webroot ?>template_image/Template3.png" id="3" style="width:100px;cursor:pointer;" onclick="csTemplate3()"><img src="<?php echo $this->webroot ?>template_image/green.png" style="width:30px;margin-top: -35px;display:none;" id="greenimg3">&nbsp;&nbsp;&nbsp;
                        <img class="imgcls4" src="<?php echo $this->webroot ?>template_image/Template4.png" id="4" style="width:100px;cursor:pointer;" onclick="csTemplate4()"><img src="<?php echo $this->webroot ?>template_image/green.png" style="width:30px;margin-top: -35px;display:none;" id="greenimg4">
                     </fieldset>-->


                    <?php echo $this->Form->end(__('Submit')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //echo $this->element('admin_sidebar');   ?>
<script type="text/javascript" src="<?php echo $this->webroot; ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">

	CKEDITOR.replace( 'cat_desc',
	{

		filebrowserBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

		filebrowserImageBrowseUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

		filebrowserFlashBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

		filebrowserUploadUrl  :'<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',

		filebrowserImageUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Image',

		filebrowserFlashUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'

	});

</script>
<script type="text/javascript">
    $(function () {
        $("#CategoryAdminAddForm").validate({
            rules: {
                'data[Category][category_name]': {
                    required: true,
                    minlength: 2
                },
                'data[Category][category_description]': {
                    required: true
                },
                'data[Category][image]': {
                    required: true
                }
            }
        });
    });
</script>

<script>
function csTemplate1()
{

    var id = $(".imgcls1").attr("id");
    $("#temp_id").attr('value',id);
    $("#greenimg1").show();
    $("#greenimg2").hide();
    $("#greenimg3").hide();
    $("#greenimg4").hide();
}
function csTemplate2()
{

    var id = $(".imgcls2").attr("id");
    $("#temp_id").attr('value',id);
    $("#greenimg1").hide();
    $("#greenimg2").show();
    $("#greenimg3").hide();
    $("#greenimg4").hide();
}
function csTemplate3()
{

    var id = $(".imgcls3").attr("id");
    $("#temp_id").attr('value',id);
    $("#greenimg1").hide();
    $("#greenimg2").hide();
    $("#greenimg3").show();
    $("#greenimg4").hide();
}
function csTemplate4()
{

    var id = $(".imgcls4").attr("id");
    $("#temp_id").attr('value',id);
    $("#greenimg1").hide();
    $("#greenimg2").hide();
    $("#greenimg3").hide();
    $("#greenimg4").show();
}
</script>