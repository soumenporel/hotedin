<!DOCTYPE html>
<html>
    <head>
        <title><?php echo WELCOME; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1" property="viewport"></meta>
        <link type="text/css" rel="stylesheet" href="<?php echo $this->webroot; ?>css/vendor-styles.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->webroot; ?>css/styles.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $this->webroot; ?>css/site.css" />

        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js?cache=2016-06-20-09"></script>
    </head>

    <body class="lr-site">

        <div ng-app="lrSiteApp" id="lr-site-app">
            <div class="lr-site__bg-image"></div>
            <div lr-site>
                <div id="site-blocks" class="lr-site__blocks" style="display:block">
                    <section id="site-block--a1o8p1s6prnw8dch87f9maic4" class="site-block block--html site-block--a1o8p1s6prnw8dch87f9maic4">
                        <div class="site-block__bg-image site-block__bg-image--a1o8p1s6prnw8dch87f9maic4"></div>
                        <div class="container">
                            <div class="row">
                                <div class="site-block__col-wrap col-xs-12">
                                    <div class="site-block__col site-block__col--0 rel">
                                        <div class="site-block__col-bg-image site-block__col-bg-image--0"></div>
                                        <div class="site-block__content">
                                            <p><img src="<?php echo $this->webroot; ?>images/logo.png" alt="" draggable="true" class="insert--image wrap-on-align-left" data-flexing="constrain" data-zoom="1000" data-margin-top="5" style="z-index: 0; width: 100%; max-width: 212px; margin-top: 5px;">
                                            </p>
                                            <p><br></p>
                                            <p><a href="#" class="insert--anchor" target="_new"><img src="<?php echo $this->webroot; ?>images/jw6ssf-facebookicon.png" alt="" draggable="true" class="insert--image wrap-on-align-right" data-margin-top="25" data-flexing="constrain" data-zoom="1000" data-margin-left="0" data-margin-right="45" style="z-index: 0; width: 100%; max-width: 35px; margin-right:0px; margin-top: 25px;"></a>
                                            </p>
                                            <p><a href="#"><img src="<?php echo $this->webroot; ?>images/bavqfm-tiwttericon.png" alt="" draggable="true" class="insert--image wrap-on-align-right" data-margin-top="25" data-flexing="constrain" data-zoom="1000" data-margin-right="-78" style="z-index: 0; margin-top: 25px; width: 100%; max-width: 36px; margin-right: 45px;"></a>
                                            </p>
                                            <p><a href="#"><img src="<?php echo $this->webroot; ?>images/linkedin.png" alt="" draggable="true" class="insert--image wrap-on-align-right" data-margin-top="25" data-flexing="constrain" data-zoom="1000" data-margin-right="-78" style="z-index: 0; margin-top: 25px; width: 100%; max-width: 36px; margin-right: -78px;"></a>
                                            </p>
                                            <p><br>
                                            </p>
                                            <p><br>
                                            </p>
                                            <p><br>
                                            </p>
                                            <h1 style="text-align: center;"><br></h1>
                                            <h1 style="text-align: center;"><span class="color--5"><strong><?php echo JOIN_THE_REVOLUTION; ?></strong></span></h1>
                                            <h2 style="text-align: center;"></h2>
                                            <h2 style="text-align: center;"><span class="color--5"><?php echo WE_ARE_REVOLUTION; ?> </span></h2>
                                            <h1 style="text-align: center;"><a href="#site-block--2pf5fyxa22zp1y1i2mn42jgna" class="insert--anchor" rel="font-size: 64px;"><img src="<?php echo $this->webroot; ?>images/i4qep2-joinusbutton-new.png" alt="" draggable="true" class="insert--image wrap-off-align-center" data-flexing="constrain" data-zoom="695" data-z-index="0" data-margin-top="30" style="margin-top: 30px; z-index: 0; width: 100%; max-width: 186px;"></a>
                                            </h1>
                                            <p style="text-align: center;"><br>
                                            </p>
                                            <p style="text-align: center;"><br>
                                            </p>
                                            <p><br>
                                            </p>
                                            <p><br>
                                            </p>
                                            <p><br>
                                            </p>						</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section lr-multi-column-block id="site-block--40adp7k3uc5km758uatxptqn" class="site-block block--html site-block--40adp7k3uc5km758uatxptqn multi-column-html--htmlonebytwo">
                        <div class="site-block__bg-image site-block__bg-image--40adp7k3uc5km758uatxptqn"></div>
                        <div class="container">
                            <div class="row">
                                <?php
                                echo $landing_aboutus['CmsPage']['page_description'];
                                ?>
                                <!--                                <div class="site-block__col-wrap col-md-6">
                                                                    <div lr-column class="site-block__col site-block__col--0 rel">
                                                                        <div class="site-block__col-bg-image site-block__col-bg-image--0"></div>
                                                                        <div class="site-block__content">
                                                                            <p><img src="<?php echo $this->webroot; ?>images/computer.png" alt="" draggable="true" class="insert--image wrap-on-align-left" data-flexing="constrain" style="z-index: 0; width: 100%; margin-top: 35px;" data-zoom="1000" data-margin-top="35">
                                                                            </p>							</div>
                                                                    </div>
                                                                </div>
                                                                <div class="site-block__col-wrap col-md-6">
                                                                    <div lr-column class="site-block__col site-block__col--1 rel">
                                                                        <div class="site-block__col-bg-image site-block__col-bg-image--1"></div>
                                                                        <div class="site-block__content">
                                                                            <p style="text-align: left;"><span style="font-size: 36px;"><strong><span class="color--1"><br></span></strong></span></p>
                                
                                                                            <p style="text-align: left;"><span style="font-size: 36px;"><strong><span class="color--1"><br>About Us</span></strong></span></p>
                                                                            <p style="text-align: left; margin-top:0;"><span style="font-size: 16px;"><span class="color--5">Ladder.ng is the first online marketplace for training in Nigeria. Our goal is to make it easier for people looking for training courses (classroom & online) to book these courses by connecting them with reputable training providers.</span></span>
                                                                            </p>							</div>
                                                                    </div>
                                                                </div>-->
                            </div>
                        </div>
                    </section>






                    <section lr-multi-column-block id="site-block--cjz665qh8y8mhqsgjo7um6qwv" class="site-block block--html site-block--cjz665qh8y8mhqsgjo7um6qwv multi-column-html--htmlonebyfour">
                        <div class="site-block__bg-image site-block__bg-image--cjz665qh8y8mhqsgjo7um6qwv"></div>
                        <div class="container">
                            <div class="row">
                                <?php
                                echo $landing_hiw['CmsPage']['page_description'];
                                ?>
                                <!--                                <div class="col-sm-12">
                                                                    <h1 style="text-align: center; font-size:36px; margin-bottom: 40px">HOW IT WORKS</h1>
                                                                </div>
                                                                <div class="site-block__col-wrap col-sm-6 col-md-3">
                                                                    <div lr-column class="site-block__col site-block__col--0 rel">
                                                                        <div class="site-block__col-bg-image site-block__col-bg-image--0"></div>
                                                                        <div class="site-block__content">
                                                                            <p><img src="<?php echo $this->webroot; ?>images/h-icon1.png" alt="" draggable="true" class="insert--image wrap-off-align-center" data-flexing="constrain" data-zoom="1000" style="z-index: 0; width: 100%; max-width: 148px;">
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><strong><span style="font-size: 24px;">Search</span></strong></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h">Got the urge to learn something new? Use our search bar or browse the categories to see the range of courses available.</span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><br>
                                                                            </p>							</div>
                                                                    </div>
                                                                </div>
                                                                <div class="site-block__col-wrap col-sm-6 col-md-3">
                                                                    <div lr-column class="site-block__col site-block__col--1 rel">
                                                                        <div class="site-block__col-bg-image site-block__col-bg-image--1"></div>
                                                                        <div class="site-block__content">
                                                                            <p><img src="<?php echo $this->webroot; ?>images/h-icon2.png" alt="" draggable="true" class="insert--image wrap-off-align-center" data-flexing="constrain" data-zoom="1000" style="z-index: 0; width: 100%; max-width: 148px; margin-bottom: -19px;" data-margin-top="0" data-margin-bottom="-19"><br>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><strong><span style="font-size: 24px;">Compare</span></strong></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h">Compare and contract similar courses from various providers to determine which is best for you.</span><span class="color--5"></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>						</div>
                                                                    </div>
                                                                </div>
                                                                <div class="site-block__col-wrap col-sm-6 col-md-3">
                                                                    <div lr-column class="site-block__col site-block__col--2 rel">
                                                                        <div class="site-block__col-bg-image site-block__col-bg-image--2"></div>
                                                                        <div class="site-block__content">
                                                                            <p style="text-align: center;"><img src="<?php echo $this->webroot; ?>images/h-icon3.png" alt="" draggable="true" class="insert--image wrap-off-align-center" data-flexing="constrain" data-zoom="1000" style="z-index: 0; width: 100%; max-width: 148px; margin-bottom: -10px;" data-margin-top="0" data-margin-bottom="-10"><br><span class="color--h"><strong><span style="font-size: 24px;">Select</span></strong></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h">Further refine your search by price range, start date and location, then select the course you are interested in attending.</span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>							</div>
                                                                    </div>
                                                                </div>
                                                                <div class="site-block__col-wrap col-sm-6 col-md-3">
                                                                    <div lr-column class="site-block__col site-block__col--3 rel">
                                                                        <div class="site-block__col-bg-image site-block__col-bg-image--3"></div>
                                                                        <div class="site-block__content">
                                                                            <p style="text-align: center;"><img src="<?php echo $this->webroot; ?>images/h-icon4.png" alt="" draggable="true" class="insert--image wrap-off-align-center" data-flexing="constrain" data-zoom="1000" style="z-index: 0; width: 100%; max-width: 148px; margin-bottom: -10px;" data-margin-bottom="-10"><br><strong><span style="font-size: 24px;"></span></strong><span class="color--h"><strong><span style="font-size: 24px;">Book</span></strong></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h">Choose a date you'd like to attend and confirm your details. Your payment information will be securely processed by our payment partner.</span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>
                                                                            <p style="text-align: center;"><span class="color--h"><br></span>
                                                                            </p>							</div>
                                                                    </div>
                                                                </div>-->
                            </div>
                        </div>
                    </section>





                    <section site-user-signup-block lr-multi-column-block id="site-block--2pf5fyxa22zp1y1i2mn42jgna" class="site-block site-block--site-user-signup  site-block--2pf5fyxa22zp1y1i2mn42jgna uniform-column-height">
                        <div class="site-block__bg-image site-block__bg-image--2pf5fyxa22zp1y1i2mn42jgna"></div>


                        <!--
                          *
                          * SIGNUP FORM ON THE LEFT
                          *
                        -->
                        <div class="container">
                            <div class="row">
                                <div style="text-align:center; float: none; margin: 0 auto;" lr-builder-render-signup-form-column-classes class="site-block__col-wrap col-sm-8">
                                    <div lr-column class="site-block__col site-block__col--0">
                                        <div class="site-block__col-bg-image site-block__col-bg-image--0"></div>
                                        <div class="row">
                                            <div class="col-xs-12 site-user-signup__intro" >
                                                <p style="text-align: center;"><br>
                                                </p>
                                                <p style="text-align: center;"><br>
                                                </p>
                                                <p style="text-align: center;"><span class="color--5"><strong><span style="font-size: 42px;"><span class="color--3"><?php echo JOIN_THE_REVOLUTION; ?></span></span></strong></span>
                                                </p>
                                                <p style="text-align: center;"><span style="font-size: 18px;"><span class="color--5"><?php echo SIGN_UP_BELOW; ?></span></span>
                                                </p>		</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <p id="snup_msg" style="color:#fff;display:none;border:1px dashed;border-color:#fff;">asdsdad</p>
                                            </div>
                                            <div class="col-xs-12">
                                                <form class="form site-user-signup__form form-horizontal" id="signup_form">
                                                    <div class="mt-table mt-table--width-100 mt-table--align-center">
                                                        <div class="mt-td--centered-vertical site-user-signup__col-1">

                                                            <div class="site-user-signup__form-group m-lg-v form-group col-sm-3">
                                                                <input
                                                                    class="form-control form-control__input"
                                                                    name="fullName"
                                                                    id="fullName"
                                                                    type="text"
                                                                    placeholder="Full Name"
                                                                    required=""
                                                                    />
                                                            </div>

                                                            <div class="site-user-signup__form-group m-lg-v form-group col-sm-3">
                                                                <input
                                                                    class="form-control form-control__input"
                                                                    name="password"
                                                                    id="password"
                                                                    type="password"
                                                                    placeholder="Password"
                                                                    required=""
                                                                    />
                                                            </div>

                                                            <div class="site-user-signup__form-group m-lg-v form-group col-sm-3">
                                                                <input
                                                                    class="form-control form-control__input"
                                                                    name="email"
                                                                    id="email"
                                                                    type="email"
                                                                    placeholder="Email"
                                                                    required=""
                                                                    />
                                                            </div>
                                                            <div class="site-user-signup__form-group m-lg-v form-group col-sm-3">
                                                                <select class="form-control" name="admin_type" id="admin_type" required="">
                                                                    <option value=""><?php echo  SELECT_USER_TYPE; ?></option>
                                                                    <option value="1"><?php echo SERVICE_PROVIDER; ?></option>
                                                                    <option value="2"><?php echo EVENT_PROVIDER; ?></option>
                                                                    <option value="3"><?php echo CORPORATE_USER; ?></option>
                                                                </select>
                                                            </div>

                                                        </div>

                                                        <div class="mt-td--centered-vertical site-user-signup__col-2">

                                                            <div class="site-user-signup__form-group m-lg-v text-center">
                                                                <button
                                                                    name="signup_submit"
                                                                    id="signup_submit"
                                                                    type="submit"
                                                                    class="btn btn-default submit site-user-signup__submit"
                                                                    ><span><?php echo SIGN_UP; ?></span></button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>



                        <!--
                          *
                          * SIGNUP FORM ON THE RIGHT
                          *
                        -->


                    </section>
                </div>
            </div>
        </div>

        <script>
            (function ($) {
                $.ajaxSetup({
                    headers: { 'X-Requested-With': 'XMLHttpRequest' }
                });
                $('#signup_form').submit(function(){
                    $.ajax({
                        url: '<?php echo $this->webroot . 'partners/ajax_signup'; ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            fullName: $('#fullName').val(),
                            password: $('#password').val(),
                            email: $('#email').val(),
                            admin_type: $('#admin_type').val()
                        },
                        beforeSend: function() {
                            $('#snup_msg').css({
                                'border-color': '#fff',
                            }).text('Sending please wait..').show();
                        },
                        success: function(data) {
                            if(data.ack == '1') {
                                $('#snup_msg').css({
                                    'border-color': '#0f0',
                                }).text(data.msg).show();
                                
                                setTimeout(function(){
                                    $('#snup_msg').hide();
                                }, 3000);
                                
                            } else {
                                $('#snup_msg').css({
                                    'border-color': '#f00',
                                }).text(data.msg).show();
                                
                                setTimeout(function(){
                                    $('#snup_msg').hide();
                                }, 3000);
                            }
                        }
                    });
                    return false;
                });
            })(jQuery);
        </script>

    </body>
</html>
