<style>
.inner_header {
    background: #0c2440;
}
.header {
    position: relative;
}
.login_body{
    margin-bottom: 100px;
    margin-top: 80px;
}
</style>

<section class="login_body">
    <div>
      <div class="row">
        <div class="col-md-4" ></div>
        <div class="col-md-4" >  
          <h1><?php echo "Activation Link"; ?></h1>
          <form action="<?php echo $this->webroot.'users/activationlink';?>" method="post" id="forgotpassword">
            <div class="form-group">
              <input type="email" class="form-control " id="" name="data[User][email]"  placeholder="<?php echo "Enter Email";?>">

            </div>
            <div class="form-group">
             <button type="submit" class="btn btnPrimarys"><?php echo 'SUBMIT'; ?></button>
            </div>
          </form>
        </div>
        <div class="col-md-4" ></div>  
      </div>
    </div>
</section>
  <script>

$(document).ready(function() {
    $('#forgotpassword').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
          fields: {
              'data[User][email]': {
                  /* Initially, the validators of this field are disabled */

                  validators: {
                      notEmpty: {
                          message: 'The email address is required and cannot be empty'
                      },
                      emailAddress: {
                          message: 'The email address is not valid'
                      }
                  }

              }
          }
    });
});
</script>