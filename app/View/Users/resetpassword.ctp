<style>
.inner_header {
    background: #0c2440;
}
.header {
    position: relative;
}
.login_body{
    margin-bottom: 100px;
    margin-top: 80px;
}
</style>

<section class="login_body">
    <div>
      <div class="row">
        <div class="col-md-4" ></div>
        <div class="col-md-4" >  
          <h1><?php echo "Reset Password"; ?></h1>
          <form action="<?php echo $this->webroot.'users/resetpassword/'.$id;?>" method="post" id="resetpassword">
            <div class="form-group">
              <input type="password" class="form-control " id="" name="data[User][password]"  placeholder="<?php echo "Enter New Password";?>">
              <input type="hidden" class="form-control" id="userid" name="data[User][id]" value="<?php echo base64_decode($id); ?>"/>

            </div>
            <div class="form-group">
              <input type="password" class="form-control " id="confirmpassword" name="data[User][confpassword]"  placeholder="<?php echo 'Confirm Password';?>"/>
            </div>
            <div class="form-group">
             <button type="submit" class="btn btnPrimarys"><?php echo 'SUBMIT'; ?></button>
            </div>
          </form>
        </div>
        <div class="col-md-4" ></div>  
      </div>
    </div>
</section>

<script>
$(document).ready(function() {
    $('#resetpassword').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
                   
            'data[User][password]': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    }
                }
            },
            'data[User][confpassword]': {
            validators: {
              notEmpty: {
                        message: 'The Confirm Password is required and cannot be empty'
                    },
                identical: {
                    field: 'data[User][password]',
                    message: 'The password and its confirm are not the same'
                }
            }
        }
        
     
        }
    });
});
</script>