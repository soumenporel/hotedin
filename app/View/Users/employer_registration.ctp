<section class="employer-registration py-5" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
  <div class="container">
      <h1 class="registration-heading">Employer Registration</h1>
      <div class="col-sm-12 col-md-12">
          <p class="registration-para pb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer</p>
      </div>
  </div>
</section>

<section class="employer-form-section py-5">
  <div class="container">
          <form id="signupForm" method="post" action="" enctype="multipart/form-data">

              <div class="employer-box">
                  <h3>User Account</h3>

                      <div class="row">
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Username<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text" class="form-control" name="data[User][username]" required>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">E-mail<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="email" class="form-control" name="data[User][email_address]" required>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Password<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input id="pass" type="password" class="form-control" name="data[User][user_pass]" required>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Confirm Password<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input id="confpass" type="password" class="form-control" name="conf_pass" required>
                              </div>
                      </div>

              </div>
              <div class="employer-box">
                  <h3>Company</h3>

                      <div class="row">
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Email<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="email" class="form-control"  name="data[CompanyDetail][email_address]" required>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Name<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text" class="form-control" name="data[CompanyDetail][company_name]" required>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Slogan</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text" class="form-control" name="data[CompanyDetail][company_slogan]">
                              </div>

                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Logo</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <div class="employer-file-panel">
                                      <span>Drop files here</span>
                                      <div class="upload_file_container">
                                          <label class="blue-btn">Select file!</label>
                                      <input type="file" name="data[CompanyDetail][logo]" />
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Cover Image</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <div class="employer-file-panel">
                                      <span>Drop files here</span>
                                      <div class="upload_file_container">
                                          <label class="blue-btn">Select file!</label>
                                      <input type="file" name="data[CompanyDetail][cover_image]" />
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Website</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text" name="data[CompanyDetail][website_address]">
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Info</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <textarea class="employer-textarea" name="data[CompanyDetail][description]">

                                  </textarea>
                              </div>
                      </div>

              </div>

              <div class="employer-box">
                  <h3>Location</h3>

                      <div class="row">
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Address</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text"  name="data[CompanyDetail][address]">
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">City<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text"  name="data[CompanyDetail][city]" required>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">State</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <select class="" id="state" name="data[CompanyDetail][state]">

                                  </select>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Country<span>*</span></label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <select id="country" class="" name="data[CompanyDetail][country]">
                                      <?php foreach ($countries as $country) { ?>
                                          <option value="<?php echo $country['Country']['id'] ?>"><?php echo $country['Country']['name'] ?></option>
                                      <?php } ?>
                                  </select>
                              </div>
                              <div class="col-sm-12 col-md-4">
                                  <label for="email">Zip-code/Post code</label>
                              </div>
                              <div class="col-sm-12 col-md-8">
                                  <input type="text"  name="data[CompanyDetail][post_code]">
                              </div>
                      </div>
                      </div>
                      <input type="submit" class="candidate-register" placeholder="Register" name="">


</form>

  </div>
</section>
<script>
    $(document).ready(function () {
        $('#country').on('change', function() {
            var value = $(this).val();
            $.ajax({
                url: '<?php echo $this->webroot; ?>postjobs/getStates',
                type: 'POST',
                data: {val: value},
                success: function(response){
                    var res = JSON.parse(response);
                    var i,htm="";
                    for(i in res){
                        htm +='<option class="remove" value="'+res[i]['State']['id']+'">'+res[i]['State']['name']+'</option>'
                    }
                    $('.remove').remove();
                    $('#state').append(htm);
                }
            });

        })
      $("#signupForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[User][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty.'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address. '
                    }
                }

            },
          'data[User][user_pass]': {
            validators: {
              notEmpty: {
                message: 'The password is required and cannot be empty.'
                },
                identical: {
                    field: 'conf_pass',
                    message: 'The password and its confirm password are not the same'
                }
            }
        },
          'conf_pass': {
            validators: {
              notEmpty: {
                message: 'The password is required and cannot be empty.'
                },
                identical: {
                    field: 'data[User][user_pass]',
                    message: 'The password and its confirm password are not the same'
                }
            }
          }
        }
      });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#country').on('change', function() {
            var value = $(this).val();
            $.ajax({
                url: '<?php echo $this->webroot; ?>postjobs/getStates',
                type: 'POST',
                data: {val: value},
                success: function(response){
                    var res = JSON.parse(response);
                    var i,htm="";
                    for(i in res){
                        htm +='<option class="remove" value="'+res[i]['State']['id']+'">'+res[i]['State']['name']+'</option>'
                    }
                    $('.remove').remove();
                    $('#state').append(htm);
                }
            });

        })

        function callAjax(value) {
            $.ajax({
                url: '<?php echo $this->webroot; ?>postjobs/getStates',
                type: 'POST',
                data: {val: value},
                success: function(response){
                    var res = JSON.parse(response);
                    var i,htm="";
                    for(i in res){
                        htm +='<option class="remove" value="'+res[i]['State']['id']+'">'+res[i]['State']['name']+'</option>'
                    }
                    $('.remove').remove();
                    $('#state').append(htm);
                }
            });
        }
        callAjax(<?php echo $countryCode; ?>);
    });
</script>
