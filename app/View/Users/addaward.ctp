

<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}
</style>
<section class="profileedit">
    <div class="container">
        <div class="row pt-4">
            <div class="col-md-3 col-sm-3">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel');?>
            </div>
            <div class="col-md-9 col-sm-9">
                <div class="profile_second_part profile-from">
                    <h2>Award</h2>
                    <p>Add information about yourself to share on your profile.</p>
							<form action="" enctype="multipart/form-data" id="UserAddportfolioForm" method="post" accept-charset="utf-8" class="form-area">

							<div class="form-group">
							<label>Award Name:</label>
							<input type="text" class="form-control"  id="userFname" name="data[Award][name]" placeholder="Enter album name"/>
							</div>
							<div class="form-group">
							<label>Award Description:</label>
							<textarea name="data[Award][description]" class="form-control" placeholder="Enter album description" ></textarea>
							</div>


							<div class="form-group image_part">
							<label>Add Images:</label>
							<div class="photo-upload">
							<!--                                          <div class="upload-pic">
							<img src="<?php echo $this->webroot;?>img/video.jpg" alt="">
							</div>-->
							<div class="fileUpload">
								<b class="file-input">upload Images <i class="fa fa-upload"></i> <input type="file" class="upload" name="data[img][]" multiple></b>
							</div>
							</div>
<!--							<ul class="upload-image-box">
								<li>
									<img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
								</li>
								<li>
									<img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
								</li>
								<li>
									<img src="   /team4/studilmu/user_images/59a8f3bc09936.jpg" alt="" width="190" height="190">
								</li>
							</ul>-->
							</div>


							<div class="form-group">
							<input type="submit" class="btn btn-default" value="Save Change">
							</div>
							</form>
                </div>
            </div>
        </div>
    </div>
</section>
