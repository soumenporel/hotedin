<?php 
// pr($userDetails); exit;
$userType = ''; 
if($userDetails['User']['admin_type']==2){ $userType = 'Student'; }
if($userDetails['User']['admin_type']==1){ $userType = 'Tutor'; }
?>
<style>
.form-horizontal .form_part1 {
    width: 15px;
}
.form-horizontal .formdiv {
    border: 0px; 
    width: 94%;
    padding: 14px 0 20px 20px;
}
.header{
  position:relative;
}

.inner_header{
  background:#0c2440;
}
</style>
<section class="profileedit">
  <div class="container">
    <div class="row" style="background: #f6f6f6; border-right: #ddd solid 1px;">
      <div class="col-md-3 col-sm-3" style="padding: 0; border-left: #ddd solid 1px; border-right: #ddd solid 1px;">
          <!-- left panel !-->
          <?php echo $this->element('leftpanel');?>
      </div>
      <div class="col-md-9 col-sm-9" style="padding: 0">
        <div class="profile_second_part">
            <h2> Delete Your account </h2>
            <!-- <p>Add information about yourself to share on your profile.</p> -->
            <?php if($userPost>0){ ?>
              <div id="remove-account">
                  
                      <p>
                          
                              We're sorry to see you go. If you are currently the owner or an active instructor of a course,
                              please delete your course first before deleting your account.Please go to course add section and remove the course..
                              
                              
                          
                      </p>
                  
              </div>
            <?php } else 
            { ?>  

            <div id="remove-account">
                
                    <p>
                        <b class="text-danger">Warning:</b>
                        
                            If you delete your account, you will be unsubscribed from
                            all your 19 courses, and will lose access
                            forever.
                        
                    </p>
                    <div class="mt15">
                        <a class="btn btn-danger" href="javascript:void(0)" id="delete_user" data-id="<?php echo $userDetails['User']['id'] ?>">
                           Delete
                        </a>
                    </div>
                
            </div>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$(document).ready(function(){

    $("#delete_user").click(function(){
    var id = $(this).data('id');
    // console.log(id);          
        $.ajax({
            url: "<?php echo $this->webroot;?>users/ajaxDeleteUser",
            data:{user_id:id},
            dataType:'json',
            type: 'POST',     
            success: function(result){
              //console.log(result);
              if(result.Ack==1){
                  var url = <?php echo $this->webroot; ?>;
                  window.location.href = url;
              }
            }
        });
   
    });

});
</script>




