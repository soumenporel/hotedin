
<style>
    .header{
        position: relative !important;
    }

    .inner_header {
        background: #0c2440 none repeat scroll 0 0;
    }
</style>
<section class="profileedit">
    <div class="container">
        <div class="row pt-4">
            <div class="col-md-3 col-sm-3">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel'); ?>
            </div>
            <div class="col-md-9 col-sm-9">
                <div class="profile_second_part profile-from">
                    <h2>Language</h2>
                    <div id="msg"></div>

                    <form class="form-area addLanguage">
                        <div class="form-group">
                            <label class="control-label" for="text">Add Language:</label>
                            <input type="hidden" id="is_exist" value="0">
                            <input type="hidden" placeholder="id" class="skill_cat_id" id="skill_cat_id"/>

                            <input type="text" class="form-control form_part skill_input" id="skill_input1" placeholder="Type Language">
                            <a class="save_skill_btn" id="save_skill_btn1" style="display: none;"><button class="save-btn-skill">Save</button></a>


                        </div>

                    </form>
                    <div class="my-added-skill" id="skillContainer">
                        <?php
                        if (!empty($language_lists) && count($language_lists) > 0) {
                            foreach ($language_lists as $language_list) {
                                ?>
                                <span id="<?php echo $language_list['LanguagePreference']['id']; ?>">
                                    <?php echo $language_list['LanguagePreference']['language_name']; ?>
                                    <a href="javascript:void(0);" class="fa fa-close" onclick="remove_skill(<?php echo $language_list['LanguagePreference']['id']; ?>);"></a>
                                </span>
                                <?php
                            }
                        }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function () {
        $('#skill_input1').autocomplete({
            minLength: 1,
            source: function (request, response) {
                var keyword = $('#skill_input1').val();
                var cat_id = 1;

                var url = '<?php echo $this->webroot . 'users/languagesuggest/' ?>' + keyword;
                $.getJSON(url, response);
            }


        });

        $('#skill_input1').data("ui-autocomplete")._renderItem = function (ul, item) {

            var $li = $('<li>');
            var label = item.label;
            $li.addClass('suggestrow skillsuggestrow');
            $li.attr('data-value', item.link);

            $li.attr('data-label', item.label);
            $li.append('<a href="javascript:void(0);" onclick="skillFunction1(\'' + item.label + '\',\'' + item.link + '\');">');
            $li.find('a').append('<div class="suggestDiv"><span class="suggestName">' + item.label + '</span></div>');

            return $li.appendTo(ul);
        };

    });

    function skillFunction1(label, link)
    {
        //alert(label);
        //alert(slug);
        //alert(link);
        $('#skill_input1').val(label);
        $('#is_exist').val('1');
        $('#skill_cat_id').val(link);
        $('#save_skill_btn1').click();
    }

    $(".save_skill_btn").click(function (event) {
        $('#msg').removeClass('alert alert-danger');
        $('#msg').html('');
        //alert(1);
        event.preventDefault();
        var skill = $(this).parent().find('input').val();
        //alert(skill);
        var is_exist = $('#is_exist').val();
        var skill_cat_id = $('#skill_cat_id').val();
        $('.skill_input').val('');
        $('#is_exist').val('0');


        //alert(cat_id);
        //alert(skill);
        //alert(is_exist);
        //alert(slug);
        //alert(skill_cat_id);
        //alert(freelancer_id);//return false;



        //alert(1);
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            dataType: 'json',
            url: "<?php echo $this->webroot; ?>users/addLanguage",
            data: {
                skill: skill,
                is_exist: is_exist,
                language_id: skill_cat_id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data.ACK == 1)
                {
                    $('#skillContainer').append('<span id="' + data.skill_id + '">' + data.skill_name + '<a href="#" class="fa fa-close" onclick="remove_skill(' + data.skill_id + ');"></a></span>');
                    $('.skill_input').val('');
                }
                else
                {
                    $('#msg').html(data.html);
                    $('#msg').addClass('alert alert-danger');
                    setTimeout(function () {
                        $('#msg').hide();
                    }, 4000);
                }

            }
        });
    });

    function remove_skill(id)
    {
        $.ajax({
            type: "POST", // Type of request to be send, called as method
            dataType: 'json',
            url: "<?php echo $this->webroot; ?>users/removeLanguage",
            data: {
                id: id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
                if (data.ACK == 1)
                {
                    $('#' + id).hide();
                    $('#msg').html(data.html);
                    $('#msg').addClass('alert alert-success');
                    setTimeout(function () {
                        $('#msg').hide();
                    }, 4000);
                }

            }
        });
    }
</script>
