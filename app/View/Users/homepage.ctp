<style>
.navBg {
    background: #fff;
    border-bottom: 1px solid #cbcbcb;
    border-top: 1px solid #000;
}
.navBg a {
    display: inline-block;
    color: rgba(0,0,0,.55);
    font-size: 13px;
    font-weight: 400;
    padding: 12px 14px;
	text-align:center;
	border-right:solid 1px #eee;
	transition:ease 0.5s
}
.navBg a:hover{background:#f4f4f4; color:#000;}
.navBg a:last-child {
    border-right:0;
}
@media (max-width:890px){
.navBg a {
    font-size: 12px;
	font-weight:600;
    padding: 8px 8px;
}
}
@media (max-width:767px){
.navBg a {
    font-size: 10px;
	border-right:0;
    padding: 4px 3px;
}
}
.carousel-control {
	width:40px;
	right:-45px !important;
}
.carousel-control.left {
	background:none !important;
	height:35px;
	right:20px !important;
	top:-65px;
}
.carousel-control.right {
	background:none !important;
	height:35px;
	top:-65px;
	right:-10px !important;
}
.course_bottom_part {
    padding: 0px 10px 0px 10px !important;
    background: #fff !important;
}
.carousel-inner .col-lg-3, .carousel-inner .col-md-3, .carousel-inner .col-sm-6{padding-left:7px; padding-right:7px;}
.carousel-control {
    top: -52px !important;
    color: #000 !important;
    text-shadow: none;
    opacity: 1;
}
.home-cate {
    width: 19%;
}
.text{
    min-height: 146px;
}
@media (max-width:991px){
.home-cate {
    width: 18%;
}
.popular-courses .text h4 a {
    font-size: 12px;
	font-weight:600;
	line-height:14px !important;
}
}
@media (max-width:767px){
.home-cate {
    width: 30%;
}
.popular-courses .text h4 a {
    font-size: 13px;
}
}
@media (max-width:400px){
.home-cate {
    width: 44%;
}
}
</style>
<nav class="navBg">
   <div class="container">
	  <?php foreach ($homepageCategory as $key => $val) { ?>
    <a href="<?php echo $this->webroot.'courses/'.$val['Category']['slug']; ?>">
      <span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="<?php echo $this->webroot . 'img/cat_logo_img/'.$val['Category']['imagelogo']; ?>">&nbsp; </span><?php echo ' '.$val['Category']['category_name']; ?>
    </a>
	  <?php } 

    // pr($homepageCategory['0']);

    ?>
    <!-- <a href="#">
      <span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="/img/cat_logo_img/590706dd69d68.png">&nbsp; </span>Business
    </a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="/img/cat_logo_img/59070a2324117.jpg">&nbsp; </span>IT & Software</a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 18px;" src="/img/cat_logo_img/5907221505dec.jpg">&nbsp; </span>Personal Development</a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="/img/cat_logo_img/59072e69d42d6.png">&nbsp; </span>Design</a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 18px;" src="/img/cat_logo_img/590728df6d14a.jpg">&nbsp; </span>Marketing</a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="/img/cat_logo_img/59085af35134e.png">&nbsp; </span>Office Productivity</a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="/img/cat_logo_img/5908148b57e27.png">&nbsp; </span>Music</a>
	  <a href="#"><span class="hidden-xs hidden-sm hidden-md"><img style="width: 15px;" src="/img/cat_logo_img/59072d2bc2cce.png">&nbsp; </span>Health & Fitness</a> -->
   </div>	  
</nav>
<section class="theme-bg-color padding-top-30 padding-btm-40">
 <div class="container">
<h4 class="text-uppercase">Best Sellers in "<?php echo $homepageCategory['0']['Category']['category_name']; ?>"</h4>
<hr style="margin-top:0;">
  <section class="inner_content theme-bg-color popular-courses">
 
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <?php 
            $blogs = $this->requestAction('/homepages/postdata/' . $homepageCategory['0']['Category']['id']);
            foreach ($blogs as $val) {
          ?>
            <div class="home-cate">
                <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $val['Post']['slug']; ?>'">
                      <div class="img-holder"><img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $val['PostImage']['0']['originalpath']; ?>" style="height: 140px;" class="img-responsive" alt="" /></div>
                      <div class="text">
                          <h4><a href="javascript:void(0);"><?php 
                                if(strlen($val['Post']['post_title'])<=30)
                                {
                                  echo $val['Post']['post_title'];
                                }
                                else
                                {
                                  $y=substr($val['Post']['post_title'],0,30) . '...';
                                  echo $y;
                                }
                          // echo $val['Post']['post_title']; 
                          ?></a></h4>
                          <!--<div class="img img-circle">
                              <img src="<?php if (isset($val['User']['user_image']) && $val['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                                  echo $val['User']['user_image'];
                              } else {
                                  echo $this->webroot;
                                  ?>img/profile_img.jpg<?php } ?>" alt="Image"></div>-->
                          <?php
                          if (!empty($val['User'])) {
                          ?>
                          <h6><?php echo $val['User']['first_name'] . ' ' . $val['User']['last_name']; ?></h6>
                          <?php
                          }
                          ?>
                          <ul>
                              <?php 
                                      if(isset($val['Rating']) && $val['Rating']!=''){
                                          $b = count($val['Rating']);
                                      
                                          $a=0;
                                          foreach ($val['Rating'] as $value) {
                                              $a=$a + $value['ratting'];
                                          }
                                      }   
                                      $finalrating='';
                                      if($b!=0){
                                          $finalrating = ($a / $b);
                                      }

                                       if(isset($finalrating) && $finalrating!='') {  
                                          for($x=1;$x<=$finalrating;$x++) { ?>
                                              <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                          <?php }
                                          if (strpos($finalrating,'.')) {  ?>
                                              <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                          <?php  $x++;
                                          }
                                          while ($x<=5) { ?>
                                              <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                          <?php $x++;
                                          } 
                                        }else { ?>
                                              <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                        <?php } ?>
                             <!--  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                              <li><i class="fa fa-star" aria-hidden="true"></i></li>
                              <li><i class="fa fa-star" aria-hidden="true"></i></li>
                              <li><i class="fa fa-star" aria-hidden="true"></i></li>
                              <li><i class="fa fa-star-o" aria-hidden="true"></i></li> -->                        
                          </ul>
                          <!--<p><?php echo $val['User']['email_address']; ?> </p>-->
                          <div class="clear-fix">
                              <ul class="float-left">
                                 <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>-->
                                  <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $val['User']['address']; ?></li>
                                  <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                    <?php 
                                    if($val['Post']['price']==0){
                                      echo 'Free';  
                                    }else{
                                      $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($val['Post']['currency_type'],$val['Post']['price'])));
                                      echo $price['symbol'].round($price['price']);
                                      //echo '$'.round($val['Post']['price']);  
                                    }  
                                     ?>
                                  </a></li>
                              </ul>

                              
                          </div>
                      </div> 
                  </div>
            </div>
          <?php } ?>
          <!-- <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->
        </div>
        
        
      </div>
      
      <!-- Left and right controls --> 
      <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> --> </div>
</section>
<h4 class="text-uppercase">Best Sellers in "<?php echo $homepageCategory['1']['Category']['category_name']; ?>"</h4>
<hr style="margin-top:0;">
<section class="inner_content theme-bg-color popular-courses">
 
    <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <?php 
            $blogs1 = $this->requestAction('/homepages/postdata/' . $homepageCategory['1']['Category']['id']);
            foreach ($blogs1 as $val1) {
          ?>
            <div class="home-cate">
              <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $val1['Post']['slug']; ?>'">
                    <div class="img-holder"><img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $val1['PostImage']['0']['originalpath']; ?>" style="height: 140px;" class="img-responsive" alt="" /></div>
                    <div class="text">
                        <h4><a href="javascript:void(0);"><?php 
                                if(strlen($val1['Post']['post_title'])<=30)
                                {
                                  echo $val1['Post']['post_title'];
                                }
                                else
                                {
                                  $y=substr($val1['Post']['post_title'],0,30) . '...';
                                  echo $y;
                                }
                        // echo $val1['Post']['post_title']; 
                        ?></a></h4>
                        <!--<div class="img img-circle">
                            <img src="<?php if (isset($val['User']['user_image']) && $val['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                                echo $val['User']['user_image'];
                            } else {
                                echo $this->webroot;
                                ?>img/profile_img.jpg<?php } ?>" alt="Image"></div>-->
                        <?php
                        if (!empty($val1['User'])) {
                        ?>
                        <h6><?php echo $val1['User']['first_name'] . ' ' . $val1['User']['last_name']; ?></h6>
                        <?php
                        }
                        ?>
                        <ul>
                            <?php 
                                    if(isset($val1['Rating']) && $val1['Rating']!=''){
                                        $b = count($val1['Rating']);
                                    
                                        $a=0;
                                        foreach ($val1['Rating'] as $value) {
                                            $a=$a + $value['ratting'];
                                        }
                                    }   
                                    $finalrating='';
                                    if($b!=0){
                                        $finalrating = ($a / $b);
                                    }

                                     if(isset($finalrating) && $finalrating!='') {  
                                        for($x=1;$x<=$finalrating;$x++) { ?>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <?php }
                                        if (strpos($finalrating,'.')) {  ?>
                                            <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                        <?php  $x++;
                                        }
                                        while ($x<=5) { ?>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                        <?php $x++;
                                        } 
                                      }else { ?>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                      <?php } ?>
                           <!--  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star-o" aria-hidden="true"></i></li> -->                        
                        </ul>
                        <!--<p><?php echo $val['User']['email_address']; ?> </p>-->
                        <div class="clear-fix">
                            <ul class="float-left">
                               <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>-->
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $val1['User']['address']; ?></li>
                                <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                  <?php 
                                  if($val1['Post']['price']==0){
                                    echo 'Free';  
                                  }else{
                                    $price1 = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($val1['Post']['currency_type'],$val1['Post']['price'])));
                                    echo $price1['symbol'].round($price1['price']);
                                    //echo '$'.round($val1['Post']['price']);  
                                  }  
                                   ?>
                                </a></li>
                            </ul>

                            
                        </div>
                    </div> 
                </div>
            </div>
          <?php } ?>
          <!-- <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="home-cate">
            <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '/an-overview-of-the-graphic-design'">
              <div class="img-holder"><img src="/img/post_img/2086901463_graphic-design.jpg" style="height: 140px;" class="img-responsive" alt=""></div>
              <div class="text">
                <h4><a href="javascript:void(0);">An Overview of the Graphic Design</a></h4>
                
                <h6>aaron smith</h6>
                <ul>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i></li>
                </ul>
                <div class="clear-fix">
                  <ul class="float-left">
                    
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> United States</li>
                    <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover"> Free </a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->
        </div>

      </div>
      
      <!-- Left and right controls --> 
      <!-- <a class="left carousel-control" href="#myCarousel1" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel1" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> --> </div>
</section>
<?php if($searchText!=''){ ?>
<h4 class="text-uppercase">Because you searched for "<?php echo $searchText; ?>"</h4>
<hr style="margin-top:0;">
<section class="inner_content theme-bg-color popular-courses">
 
    <div id="myCarousel2" class="carousel slide" data-ride="carousel"> 
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <?php foreach ($searchCourses as $course) { ?>
          <div class="home-cate">
              <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $course['Post']['slug']; ?>'">
                    <div class="img-holder"><img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $course['PostImage']['0']['originalpath']; ?>" style="height: 140px;" class="img-responsive" alt="" /></div>
                    <div class="text">
                        <h4><a href="javascript:void(0);"><?php 
                                if(strlen($course['Post']['post_title'])<=30)
                                {
                                  echo $course['Post']['post_title'];
                                }
                                else
                                {
                                  $y=substr($course['Post']['post_title'],0,30) . '...';
                                  echo $y;
                                }
                        // echo $course['Post']['post_title']; 
                        ?></a></h4>
                        <!--<div class="img img-circle">
                            <img src="<?php if (isset($val['User']['user_image']) && $val['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                                echo $val['User']['user_image'];
                            } else {
                                echo $this->webroot;
                                ?>img/profile_img.jpg<?php } ?>" alt="Image"></div>-->
                        <?php
                        if (!empty($course['User'])) {
                        ?>
                        <h6><?php echo $course['User']['first_name'] . ' ' . $course['User']['last_name']; ?></h6>
                        <?php
                        }
                        ?>
                        <ul>
                            <?php 
                                    if(isset($course['Rating']) && $course['Rating']!=''){
                                        $b = count($course['Rating']);
                                    
                                        $a=0;
                                        foreach ($course['Rating'] as $value) {
                                            $a=$a + $value['ratting'];
                                        }
                                    }   
                                    $finalrating='';
                                    if($b!=0){
                                        $finalrating = ($a / $b);
                                    }

                                     if(isset($finalrating) && $finalrating!='') {  
                                        for($x=1;$x<=$finalrating;$x++) { ?>
                                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <?php }
                                        if (strpos($finalrating,'.')) {  ?>
                                            <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                        <?php  $x++;
                                        }
                                        while ($x<=5) { ?>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                        <?php $x++;
                                        } 
                                      }else { ?>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                      <?php } ?>
                           <!--  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star-o" aria-hidden="true"></i></li> -->                        
                        </ul>
                        <!--<p><?php echo $val['User']['email_address']; ?> </p>-->
                        <div class="clear-fix">
                            <ul class="float-left">
                               <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>-->
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $course['User']['address']; ?></li>
                                <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                  <?php 
                                  if($course['Post']['price']==0){
                                    echo 'Free';  
                                  }else{
                                    $price2 = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($course['Post']['currency_type'],$course['Post']['price'])));
                                    echo $price2['symbol'].round($price2['price']);
                                    //echo '$'.round($course['Post']['price']);  
                                  }  
                                   ?>
                                </a></li>
                            </ul>

                            
                        </div>
                    </div> 
                </div>
            </div>
          <?php } ?>
        </div>
      </div>
      
      <!-- Left and right controls --> 
      <!-- <a class="left carousel-control" href="#myCarousel2" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel2" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> --> </div>
</section>
<?php } ?>
<section class="categorySection">
    <div class="row text-center">
       <div class="col-sm-5">
        <article class="category lg">
            <a class="category-text" href="<?php echo $this->webroot.'courses/'.$homepageCategory[0]['Category']['slug'];?>" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(&quot;<?php echo $this->webroot; ?>img/cat_img/<?php echo $homepageCategory[0]['CategoryImage']['0']['originalpath']; ?>&quot;);"> 
                <div class="category-info">
                    <h3 class="category-name"> <?php echo $homepageCategory[0]['Category']['category_name']; ?> <br /><small>Over <?php 
                      if($homepageCategory[0]['Category']['post_count']!=''){
                        echo $homepageCategory[0]['Category']['post_count'];
                      }else {
                        echo '0';
                      }
                       ?> courses</small></h3>
                   
                    <p><span class="category-heading"><i class="fa fa-book play-circle"></i><strong>Preview Subject</strong></span></p>
                </div>
            </a>
          </article>
       </div>  
       <div class="col-sm-7">
         <div class="row">
           <div class="col-xs-6">
             <article class="category sm">
            <a class="category-text" href="<?php echo $this->webroot.'courses/'.$homepageCategory[1]['Category']['slug'];?>" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(&quot;<?php echo $this->webroot; ?>img/cat_img/<?php echo $homepageCategory[1]['CategoryImage']['0']['originalpath']; ?>&quot;);"> 
                <div class="category-info">
                    <h3 class="category-name"> <?php echo $homepageCategory[1]['Category']['category_name']; ?> <br /><small>Over <?php 
                      if($homepageCategory[1]['Category']['post_count']!=''){
                        echo $homepageCategory[1]['Category']['post_count'];
                      }else {
                        echo '0';
                      }
                       ?> courses</small></h3>
                   
                    <p><span class="category-heading"><i class="fa fa-book play-circle"></i><strong>Preview Subject</strong></span></p>
                </div>
            </a>
          </article>
           </div>
           <div class="col-xs-6">
             <article class="category sm">
            <a class="category-text" href="<?php echo $this->webroot.'courses/'.$homepageCategory[2]['Category']['slug'];?>" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(&quot;<?php echo $this->webroot; ?>img/cat_img/<?php echo $homepageCategory[2]['CategoryImage']['0']['originalpath']; ?>&quot;);"> 
                <div class="category-info">
                    <h3 class="category-name"> <?php echo $homepageCategory[2]['Category']['category_name']; ?> <br /><small>Over <?php 
                      if($homepageCategory[2]['Category']['post_count']!=''){
                        echo $homepageCategory[2]['Category']['post_count'];
                      }else {
                        echo '0';
                      }
                       ?> courses</small></h3>
                   
                    <p><span class="category-heading"><i class="fa fa-book play-circle"></i><strong>Preview Subject</strong></span></p>
                </div>
            </a>
          </article>
           </div>
         </div>
         <div class="row">
           <div class="col-xs-6">
             <article class="category sm">
            <a class="category-text" href="<?php echo $this->webroot.'courses/'.$homepageCategory[3]['Category']['slug'];?>" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(&quot;<?php echo $this->webroot; ?>img/cat_img/<?php echo $homepageCategory[3]['CategoryImage']['0']['originalpath']; ?>&quot;);"> 
                <div class="category-info">
                    <h3 class="category-name"> <?php echo $homepageCategory[3]['Category']['category_name']; ?> <br /><small>Over <?php 
                      if($homepageCategory[3]['Category']['post_count']!=''){
                        echo $homepageCategory[3]['Category']['post_count'];
                      }else {
                        echo '0';
                      }
                       ?> courses</small></h3>
                   
                    <p><span class="category-heading"><i class="fa fa-book play-circle"></i><strong>Preview Subject</strong></span></p>
                </div>
            </a>
          </article>
           </div>
           <div class="col-xs-6">
             <article class="category sm">
            <a class="category-text" href="<?php echo $this->webroot.'courses/'.$homepageCategory[4]['Category']['slug'];?>" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(&quot;<?php echo $this->webroot; ?>img/cat_img/<?php echo $homepageCategory[4]['CategoryImage']['0']['originalpath']; ?>&quot;);"> 
                <div class="category-info">
                    <h3 class="category-name"> <?php echo $homepageCategory[4]['Category']['category_name']; ?> <br /><small>Over <?php 
                      if($homepageCategory[4]['Category']['post_count']!=''){
                        echo $homepageCategory[4]['Category']['post_count'];
                      }else {
                        echo '0';
                      }
                       ?> courses</small></h3>
                   
                    <p><span class="category-heading"><i class="fa fa-book play-circle"></i><strong>Preview Subject</strong></span></p>
                </div>
            </a>
          </article>
           </div>
         </div>
       </div>
    </div>
</section>
</div>
</section>