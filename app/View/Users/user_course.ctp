
<style>
.fa-fw{
    color: #ff7902;
}
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

.carousel-control{width:40px; right:-45px !important;}
.carousel-control.left{background:none !important; height:35px; right:20px !important; top:-65px;}
.carousel-control.right{background:none !important; height:35px; top:-65px; right:-10px !important;}

.carousel-inner p {
    font-size: 17px;
}

</style>

<section class="" style="padding-top:50px; padding-bottom:50px;">

    <div class="container">

        <div class="row">

            <div class="col-sm-3">

            <img src="http://35.154.208.200/img/social-media.jpeg" class="img-responsive">

            </div>

            <div class="col-sm-7">

                <h4 class="he" style="padding-top:17px;">Congratulations! <span style="font-weight:300;">You've successfully enrolled in</span></h4>
                <h2 style="margin: 0; text-transform: uppercase; font-weight: 300; font-size:25px;">Understanding Hashtags in Social Media</h2>
                <div class="access_icon"><p style="text-align:left;">By Jerad Hii, Photography, Web Designer, Online Marketer, Husband, Father</p>
            </div>
            </div>

            <div class="col-sm-2" style="margin-top: 58px">

            <a href="javascript:void(0)" class="btn-course">Go to Course</a>

            </div>

        </div>

    </div>

</section>
<?php // echo'<pre>'; print_r($user_courses); echo'</pre>'; exit;?>
<section class="inner_content theme-bg-color" style="padding-top:50px; padding-bottom:50px;" >
    <div class="container">

        <div class="container">
              <h4 class="he" style="padding:8px; position:relative; top:-20px; background:#25c6d4; color:#fff; font-weight:700">Because you enrolled in Understading Hashtags in Social Media</h4>  
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">

                    <?php foreach ($user_courses as $key => $val) { //echo '<pre>'; print_r($val); echo '</pre>'; ?>
                        
                        <div class="col-lg-3 col-md-3 col-sm-6 item" >
                            <div class="coursename thumbnail" style="cursor:pointer;">
                                <div class="courseimage" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $val['Post']['slug']; ?>'">
                                    <img class="img-responsive" style="height:162px; width:162px;" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $val['Post']['PostImage'][0]['originalpath']; ?>">
                                </div>
                    
                                <div class="course_bottom_part">
                                    <div class="name-p" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $val['Post']['slug']; ?>'"> <?php echo $val['Post']['post_title']; ?></div>

                                    <div class="img img-circle">
                                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                                    </div>

                                    <div class="writer_name">By  <?php
                                if (!empty($val['User'])) {
                                ?>
                                <?php echo $val['User']['first_name'] . ' ' . $val['User']['last_name']; ?>
                                <?php
                                }
                                ?></div>
                                    <div class="rating">
                                        <span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                                        <span class="starimg">
                                            <?php 
                                        if(isset($val['Rating']) && $val['Rating']!=''){
                                            $b = count($val['Rating']);
                                        
                                            $a=0;
                                            foreach ($val['Rating'] as $value) {
                                                $a=$a + $value['ratting'];
                                            }
                                        }   
                                        $finalrating='';
                                        if($b!=0){
                                            $finalrating = ($a / $b);
                                        }

                                         if(isset($finalrating) && $finalrating!='') {  
                                            for($x=1;$x<=$finalrating;$x++) { ?>
                                                <i class="fa fa-star" aria-hidden="true" style="color: gold; padding-right:1px !important;"></i>
                                            <?php }
                                            if (strpos($finalrating,'.')) {  ?>
                                                <i class="fa fa-star-half-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                            <?php  $x++;
                                            }
                                            while ($x<=5) { ?>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                            <?php $x++;
                                            } ?>
                                            &nbsp;&nbsp;<?php echo $finalrating; ?>
                                        <?php     
                                          }else { ?>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: gold;padding-right:1px !important;"></i>
                                    <?php } ?> 
                                        </span>
                            
                                    </div>

                                    <p><?php echo $val['User']['email_address']; ?> </p>
                                                
                                    <div class="price">
                                        <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                        <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            
                                <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li> -->
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $val['User']['address']; ?></li>
                                        <li><a href="#" class="tran3s p-color-bg themehover"><?php 
                                          if($val['Post']['price']==0){
                                            echo 'Free';  
                                          }else{
                                            echo '$'.round($val['Post']['price']);  
                                          }  
                                           ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php } ?>    
            <!-- <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>
                  </div>

                  <div class="item">
                        <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>
                  </div>
                
                  <div class="item">
                        <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 item" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'">
                        <img class="img-responsive" style="height:162px; width:162px;" src="/img/post_img/214407216_computer.png">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '/posts/course_details/beginner-photoshop-to-html5-and-css3'"> Beginner Photoshop to HTML5 and CSS3</div>

                        <div class="img img-circle">
                        <img src="/img/profile_img.jpg" alt="Image" class="img-responsive">
                        </div>

                        <div class="writer_name">By aaron smith</div>
                        <div class="rating"><span class="rateStarFirst starimg" data-score="3.5" readonly="true"></span>
                            <span class="starimg">
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                                <i class="fa fa-star-o"  style="color: gold;"></i>
                            </span>
                            
                        </div>

                        <p>aaron.smith0158@gmail.com </p>
                                                
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clear-fix course-bottom-list">
                                    <ul class="float-left">
                                        <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="#" class="tran3s p-color-bg themehover">$0</a></li>
                                    </ul>
                        </div>
                </div>
            </div> -->
                  </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>

    </div>
</section>