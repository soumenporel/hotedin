<section class="candidate-section py-5">
</section>

<div class="container">
      <div class="row d-flex justify-content-center">
          <div class="candidate-dashboard-panel">
              <h1  class="dashboard-heading candidate-heading-color text-center">Candidate Dashboard</h1>

              <p class="candidate-paragraph text-center py-3 px-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500
              </p>
              <h3 class="profile-completion text-center pb-4">Profile Completion (62%)</h3>

              <div class="col-md-8 mx-auto mb-3">
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                    aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    </div>
                  </div>

              </div>
          </div>
      </div>
  </div>

<section class="dashboard-section py-4">
  <div class="container">
      <div class="row">
          <h3 class="manage text-center mb-5">Manage</h3>
          <div class="dashboard-list text-center">
              <ul>
                  <li>
                      <a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic1.png"></a>
                      <h3>Post A Job</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic2.png"></a>
                      <h3>Listing</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic3.png"></a>
                      <h3>Application</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic4.png"></a>
                      <h3>Edit Profile</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/dashboars-pic5.png"></a>
                      <h3>Membership</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic5.png"></a>
                      <h3>Payment History</h3>
                  </li>
              </ul>
          </div>
          <h3 class="manage text-center pt-4 mb-4 mb-5">Account</h3>
          <div class="dashboard-list text-center">
              <ul>
                  <li>
                      <a href="<?php echo $this->webroot ?>users/logout"><img src="<?php echo $this->webroot ?>/images/candidate-pic6.png"></a>
                      <h3>Logout</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic7.png"></a>
                      <h3>Change Password</h3>
                  </li>
                  <li><a href="#"><img src="<?php echo $this->webroot ?>/images/candidate-pic8.png"></a>
                      <h3>Delete Account</h3>
                  </li>

              </ul>
          </div>
      </div>
  </div>
</section>

<section class="job-categories-panel py-4">
  <div class="container">
      <div class="row">
          <h3 class="pb-3">Candidate Job Listing</h3>
      <!-- 	<div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">Bar/pub management  (24)</a></li>
                  <li><a href="#">Bar/pub staff  (24)</a></li>
                  <li><a href="#">Catering management  (24)</a></li>
                  <li><a href="#">Catering staff  (1)</a></li>
                  <li><a href="#">Concierge   (4)</a></li>
              </ul>
          </div>
          <div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">Conference and banqueting (0)</a></li>
                  <li><a href="#">Default   (24)</a></li>
                  <li><a href="#">Event management  (24)</a></li>
                  <li><a href="#">Events  (8)</a></li>
                  <li><a href="#">Front of house   (24)</a></li>
              </ul>
          </div>
          <div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">Hotel management   (24)</a></li>
                  <li><a href="#">Hotel receptionist   (8)</a></li>
                  <li><a href="#">Housekeeper   (4)</a></li>
                  <li><a href="#">Housekeeping    (17)</a></li>
                  <li><a href="#">Kitchen assistant    (12)</a></li>
              </ul>
          </div>

          <div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">Kitchen manager    (5)</a></li>
                  <li><a href="#">Management    (2)</a></li>
                  <li><a href="#">Operations manager   (0)</a></li>
                  <li><a href="#">Porter   (3)</a></li>
                  <li><a href="#">Restaurant management   (1)</a></li>
              </ul>
          </div>

          <div class="clearfix py-4"></div>

          <div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">View Jobs</a></li>
                  <li><a href="#">My Applications</a></li>
                  <li><a href="#">Employers</a></li>
                  <li><a href="#">Advanced Search</a></li>

              </ul>
          </div>

          <div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">Post a Job</a></li>
                  <li><a href="#">Browse Resumes</a></li>
                  <li><a href="#">Search Resumes</a></li>
                  <li><a href="#">Company Jobs</a></li>
              </ul>
          </div>

          <div class="col-sm-12- col-md-6 col-lg-3">
              <ul class="dashboard-listing">
                  <li><a href="#">Job Applications</a></li>
              </ul>
          </div>
          <div class="clearfix py-4"></div> -->

          <div class="owl-carousel owl-theme candided">
                      <div class="item text-center">
                              <div class="candided-job-panel">
                                <div class="blog-panel-image h-100">
                                  <img src="<?php echo $this->webroot ?>/images/blog-image.jpg">
                                </div>
                              <h1>PHP Developer, API</h1>
                              <h2><i class="icon ion-ios-location"></i> London, United Kingdom</h2>

                              <div class="Leaders-btn"><a href="#">Apply</a></div>
                             </div>
                      </div>

                      <div class="item text-center">
                              <div class="candided-job-panel">
                                 <div class="blog-panel-image h-100">
                                  <img src="<?php echo $this->webroot ?>/images/blog-image2.jpg">
                                </div>
                              <h1>Web Designer</h1>
                              <h2><i class="icon ion-ios-location"></i> London, United Kingdom </h2>

                              <div class="Leaders-btn"><a href="#">Apply</a></div>
                             </div>
                      </div>
                      <div class="item text-center">
                              <div class="candided-job-panel">
                                <div class="blog-panel-image h-100">
                                  <img src="<?php echo $this->webroot ?>/images/blog-image3.jpg">
                                </div>
                               <h1>Digital Marketing</h1>
                              <h2><i class="icon ion-ios-location"></i> London, United Kingdom </h2>

                              <div class="Leaders-btn"><a href="#">Apply</a></div>
                             </div>
                      </div>
                      <div class="item text-center">
                              <div class="candided-job-panel">
                                <div class="blog-panel-image h-100">
                                  <img src="<?php echo $this->webroot ?>/images/blog-image2.jpg">
                                </div>
                              <h1>Digital Marketing</h1>
                              <h2><i class="icon ion-ios-location"></i> London, United Kingdom </h2>

                              <div class="Leaders-btn"><a href="#">Apply</a></div>
                             </div>
                      </div>
                      <div class="item text-center">
                              <div class="candided-job-panel">
                                 <div class="blog-panel-image h-100">
                                  <img src="<?php echo $this->webroot ?>/images/blog-image.jpg">
                                </div>
                              <h1>Front Office Manager</h1>
                              <h2><i class="icon ion-ios-location"></i> Port Aventura</h2>

                              <div class="Leaders-btn"><a href="#">Apply</a></div>
                             </div>
                      </div>

          </div>

      </div>
  </div>
</section>

<!-- <section class="dashboard-blog-section py-5">
  <div class="container">
      <div class="row">
          <h1 class="dashboard-blog-heading text-center">Blog</h1>
          <p class="text-center dashboard-blog-para pb-3">Access industry insights, career advice, and priceless tips from top players and hospitality experts.</p>

          <div class="dashboard-blog-grid">
              <div class="blog-grid-box">
                  <div class="blog-grid-box-image">
                      <img class="img-fluid" src="<?php echo $this->webroot ?>/images/blog-pic1.jpg">
                  </div>
                  <p class="blog-grid-box-title">
                      Raju Hirani : Executive vice
                      chanslor of Fobia Fox
                  </p>
                  <div class="leaders-btn"><a href="#">Leaders</a></div>
              </div>
          </div>
          <div class="dashboard-blog-grid">
              <div class="blog-grid-box">
                  <div class="blog-grid-box-image">
                      <img class="img-fluid" src="<?php echo $this->webroot ?>/images/blog-pic2.jpg">
                  </div>
                  <p class="blog-grid-box-title">
                      Raju Hirani : Executive vice
                      chanslor of Fobia Fox
                  </p>
                  <div class="leaders-btn"><a href="#">Professions</a></div>
              </div>
          </div>
          <div class="dashboard-blog-grid">
              <div class="blog-grid-box">
                  <div class="blog-grid-box-image">
                      <img class="img-fluid" src="<?php echo $this->webroot ?>/images/blog-pic3.jpg">
                  </div>
                  <p class="blog-grid-box-title">
                      Raju Hirani : Executive vice
                      chanslor of Fobia Fox
                  </p>
                  <div class="leaders-btn"><a href="#">Leaders</a></div>
              </div>
          </div>
          <div class="dashboard-blog-grid">
              <div class="blog-grid-box">
                  <div class="blog-grid-box-image">
                      <img class="img-fluid" src="<?php echo $this->webroot ?>/images/blog-pic4.jpg">
                  </div>
                  <p class="blog-grid-box-title">
                      Raju Hirani : Executive vice
                      chanslor of Fobia Fox
                  </p>
                  <div class="leaders-btn"><a href="#">Professions</a></div>
              </div>
          </div>
          <div class="dashboard-blog-grid">
              <div class="blog-grid-box">
                  <div class="blog-grid-box-image">
                      <img class="img-fluid" src="<?php echo $this->webroot ?>/images/blog-pic5.jpg">
                  </div>
                  <p class="blog-grid-box-title">
                      Raju Hirani : Executive vice
                      chanslor of Fobia Fox
                  </p>
                  <div class="leaders-btn text-center"><a href="#">Leaders</a></div>
              </div>
          </div>

      </div>
  </div>
</section> -->

<section class="blog-section">
<div class="container">
      <div class="row">
          <h1>Blog</h1>
          <p>ccess industry insights, career advice, and priceless tips from top players and hospitality experts.</p>

          <div class="col-xs-12 col-sm-12 col-md-4 text-center">
              <div class="blog-panel">
                  <div class="blog-panel-image">
                      <img src="<?php echo $this->webroot ?>/images/blog-image.jpg">
                  </div>
                  <div class="middle-image-box">
                      <img src="<?php echo $this->webroot ?>/images/small-image1.jpg">
                  </div>
                  <h1>Front Office Manager</h1>
                  <h2>Port Aventura</h2>
                  <div class="Leaders-btn"><a href="#">Leaders</a></div>
                  <p>Lorem Ipsum </p>
              </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-4 text-center">
              <div class="blog-panel">
                  <div class="blog-panel-image">
                      <img src="<?php echo $this->webroot ?>/images/blog-image2.jpg">

                  </div>
                      <div class="middle-image-box">
                              <img src="<?php echo $this->webroot ?>/images/small-image2.jpg">
                          </div>
                  <h1>Doorman</h1>
                  <h2>Istanbul Sisli</h2>
                  <div class="Leaders-btn"><a href="#">Leaders</a></div>
                  <p>Lorem Ipsum </p>
              </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-4 text-center">
              <div class="blog-panel">
                  <div class="blog-panel-image">
                      <img src="<?php echo $this->webroot ?>/images/blog-image3.jpg">

                  </div>
                  <div class="middle-image-box">
                              <img src="<?php echo $this->webroot ?>/images/small-image3.jpg">
                          </div>
                  <h1>Front Office Manager</h1>
                  <h2>Port Aventura</h2>
                  <div class="Leaders-btn"><a href="#">Leaders</a></div>
                  <p>Lorem Ipsum </p>
              </div>
          </div>


      </div>
  </div>
</section>




<section class="hospitality">
   <div class="container-fluid">
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6">
              <img class="img-responsive" src="<?php echo $this->webroot ?>/images/hospitality-image.jpg">
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6">
              <div class="hospitality-right">
                  <h1>Apply For Hospitality Jobs</h1>
                  <div class="hospitality-list">
                      <ul>
                          <li><i class="icon ion-ios-checkmark-empty"></i> Get instant job notifications</li>
                          <li><i class="icon ion-ios-checkmark-empty"></i> Apply direct from the job app</li>
                          <li><i class="icon ion-ios-checkmark-empty"></i> Save jobs and searches</li>
                      </ul>
                  </div>
                  <h2>Great App For Us Busy Chefs.....</h2>
                  <ul class="star-listing">
                      <li><i class="icon ion-android-star"></i></li>
                      <li><i class="icon ion-android-star"></i></li>
                      <li><i class="icon ion-android-star"></i></li>
                      <li><i class="icon ion-android-star"></i></li>
                      <li><i class="icon ion-android-star"></i></li>
                  </ul>
                  <h3>Download the job app now</h3>
                  <ul class="star-listing btn-listing">
                      <li><a href="#"><img src="<?php echo $this->webroot ?>/images/app-store_btn.jpg"></a></li>
                      <li><a href="#"><img src="<?php echo $this->webroot ?>/images/googlt-btn.jpg"></a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>

</section>
<script>
    $(window).scroll(function(){
      var sticky = $('.sticky'),
          scroll = $(window).scrollTop();

      if (scroll >= 50) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
</script>
<script>
        $(document).ready(function() {
          var owl = $('.owl-carousel');
          owl.owlCarousel({
            margin: 10,
            dots:false,
            autoplay:true,
            autoplayTimeout:1000,
            nav: true,
            loop: true,
            responsive: {
              0: {
                items: 1
              },
              600: {
                items: 2
              },
              1000: {
                items: 4
              }
            }
          })
        })
      </script>

      <script type="text/javascript">
        $('.candided').owlCarousel({
                     loop:true,
                     margin:10,
                      autoplay:true,
                      autoplayTimeout:1000,
                     nav:true,

            responsive:{
                     0:{
                        items:1
                       },
                    600:{
                     items:3
                         },
                         1000:{
                         items:4
                         }
            }
            })
      </script>
