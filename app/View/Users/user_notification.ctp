<section class="notification-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2>Notifications</h2>
			</div>
			<div class="col-lg-12">
				<a href="" class="settings"><i class="fa fa-cog"></i></a>
				<ul class="nav nav-tabs navbar-right" role="tablist">
					<li role="presentation" class="active"><a href="#instrctr" aria-controls="instrctr" role="tab" data-toggle="tab">Instructor</a></li>
					<li role="presentation"><a href="#studnt" aria-controls="studnt" role="tab" data-toggle="tab">Student</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="noti-body">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 middle-div">
				<div class="tab-content">
					<?php //foreach ($instructorNotifications as $key => $instructorNotification) { ?>
                    <?php //echo '<pre>'; print_r($instructorNotifications); echo '</pre>'; ?>
                    <?php if(!empty($instructorNotifications)){ ?>
                    <div role="tabpanel" class="tab-pane fade in active" id="instrctr">
						<div class="notifictn_item">
							<a href="<?php echo $this->webroot.'users/dashboard/qna';?>">
								<div class="flx-view">
									<p class="notification__message"> New Questin is posted in your course </p>
								</div>
							</a>
						</div>
					</div>
                    <?php }
                    else{?>
                    <div role="tabpanel" class="tab-pane fade in active" id="instrctr">
                        <div class="notifictn_item">
                            <div class="flx-view">
                                <p class="notification__message"> No new Notification. </p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
					<?php //} ?>
                    <div role="tabpanel" class="tab-pane fade" id="studnt">
                        <?php if(!empty($studentNotifications)){ ?>
						<?php foreach ($studentNotifications as $key => $studentNotification) { ?>
                        <div class="notifictn_item">
							<a href="<?php echo $this->webroot.'learns/announcement/'.$studentNotification['Post']['slug'];?>">
								<div class="notification__img">
									<img class="" src="<?php if (isset($studentNotification['Post']['User']['user_image']) && $studentNotification['Post']['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                                            echo $studentNotification['Post']['User']['user_image'];
                                            } else {
                                            echo $this->webroot;
                                            ?>img/profile_img.jpg<?php } ?>"> 
								</div>
								<div class="flx-view">
									<p class="notification__message">
                                        <?php 
                                            echo $studentNotification['UserNotification']['comment'];
                                        ?>
                                    </p>
									<p class="notification__time">
                                        <?php  
                                            $start_date = new DateTime($studentNotification['UserNotification']['time']);
                                            $your_date = date('Y-m-d H:i:s');
                                            $since_start = $start_date->diff(new DateTime($your_date));
                                            //echo $since_start = floor(($your_date - $start_date) / (60 * 60 * 24));
                                            //echo $since_start->days.' days total'; 
                                            $y = $since_start->y . ' years ago';
                                            $m = $since_start->m . ' months ago';
                                            $d = $since_start->d . ' days ago';
                                            $h = $since_start->h . ' hours ago';
                                            $i = $since_start->i . ' minutes ago';
                                            $s = $since_start->s . ' seconds ago';
                                            if ($y != 0) {
                                                echo $y;
                                            } else {
                                                if ($m != 0) {
                                                    echo $m;
                                                } else {
                                                    if ($d != 0) {
                                                        echo $d;
                                                    } else {
                                                        if ($h != 0) {
                                                            echo $h;
                                                        } else {
                                                            if ($i != 0) {
                                                                echo $i;
                                                            } else {
                                                                if ($s != 0) {
                                                                    echo $s;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        ?>
                                    </p>
								</div>
							</a>
						</div>
                        <?php } ?>
                        <?php }
                        else{ ?>
                            <div role="tabpanel" class="tab-pane fade in active" id="instrctr">
                                <div class="notifictn_item">
                                        <div class="flx-view">
                                            <p class="notification__message"> No new Notification. </p>
                                        </div>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </div>
				
                </div>
			</div>
		</div>
	</div>
</div>


