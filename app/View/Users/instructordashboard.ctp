<section class="profileedit">
	<div class="container">
		<div class="row" style="background: #104A80;border-right: 1px solid #dedede;">
	      <div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
             <div class="profile_second_part student-dashboard-body">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
							  <div class="panel-heading">
								  <h3>Earnings</h3>
								  <span>This Month</span>
							  </div>
							  <div class="panel-body">
							  		<img class="img-fluid" src="http://www.actiprosoftware.com/content/images/products/controls-universal-charts/LineChartType.png"/>
							  </div>
							</div>
							<div class="panel panel-default">
							  <div class="panel-heading">
								  <h3>Transaction</h3>
								  <span>Latest from statement</span>
							  </div>
							  <div class="panel-body">
							  		<ul>
									  	<li>
									  		<div class="row">
												<div class="col-sm-3">
													<p>Basic of Html</p>
												</div>
												<div class="col-sm-3">
													<p>#26556545</p>
												</div>
												<div class="col-sm-2">
													<p>$20</p>
												</div>
												<div class="col-sm-4">
													<p>12 jan 2016</p>
												</div>
											</div>
									  	</li>
									  	<li>
									  		<div class="row">
												<div class="col-sm-3">
													<p>Basic of Html</p>
												</div>
												<div class="col-sm-3">
													<p>#26556545</p>
												</div>
												<div class="col-sm-2">
													<p>$20</p>
												</div>
												<div class="col-sm-4">
													<p>12 jan 2016</p>
												</div>
											</div>
									  	</li>
									  	<li>
									  		<div class="row">
												<div class="col-sm-3">
													<p>Basic of Html</p>
												</div>
												<div class="col-sm-3">
													<p>#26556545</p>
												</div>
												<div class="col-sm-2">
													<p>$20</p>
												</div>
												<div class="col-sm-4">
													<p>12 jan 2016</p>
												</div>
											</div>
									  	</li>
									  	<li>
									  		<div class="row">
												<div class="col-sm-3">
													<p>Basic of Html</p>
												</div>
												<div class="col-sm-3">
													<p>#26556545</p>
												</div>
												<div class="col-sm-2">
													<p>$20</p>
												</div>
												<div class="col-sm-4">
													<p>12 jan 2016</p>
												</div>
											</div>
									  	</li>
									</ul>
							  </div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-default">
							  <div class="panel-heading">
								  <h3>Courses</h3>
								  <span>Your recent courses</span>
							  </div>
							  <div class="panel-body">
                                                              <?php //print_r($userdata); ?>
							    <ul>
									
                                                                <?php foreach($userdata['Post'] as $post) { ?>
                                                                        <li>
										<div class="row">
											<div class="col-sm-6">
												<p>
                                                                                                    <a href="<?php echo $this->webroot; ?>posts/course_goals/<?php echo $post['slug'];?>" target="_blank"><?php echo $post['post_title']; ?></a></p>
											</div>
											<div class="col-sm-6">
												<div class="progress">
												  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
												    <span class="sr-only">40% Complete (success)</span>
												  </div>
												</div>
											</div>
										</div>
									</li>
                                                                <?php } ?>
									<li>
										<a href="<?php echo $this->webroot;?>posts/add_course" class="btn btn-primary">Create Course +</a>
									</li>
								</ul>
							  </div>
							</div>
							<div class="panel panel-default">
							  <div class="panel-heading">
							  		<h3>Comments</h3>
							  		<span>Latest Student Comments</span>
                                                                       
							  </div>
							  <div class="panel-body">
							  		<ul>
									 <?php foreach($ratings as $rating) { ?>  	
                                                                            <li>
                                                                            <div class="media"> 
                                                                                    <div class="media-left"> 
                                                                                            <a href="#"> 
                                                                                            <img src="<?php if (isset($rating['User']['user_image']) && $rating['User']['user_image'] != '') { ?> <?php echo $this->webroot; ?>user_images/<?php echo $rating['User']['user_image'];} else {echo $this->webroot;?>img/profile_img.jpg<?php } ?>" alt="">
                                                                                            </a> 
                                                                                    </div> 
                                                                                    <div class="media-body"> 
                                                                                            <h4 class="media-heading"><?php echo $rating['User']['first_name']." ".$rating['User']['last_name']; ?></h4>
                                                                                            <p><?php echo $rating['UserRating']['comment']; ?></p> 
                                                                                    </div> 
                                                                                    <div class="media-right"> 
                                                                                            <span><?php echo date('F j Y h:i:s',strtotime($rating['UserRating']['ratting_date'])); ?></span>
                                                                                    </div> 
                                                                            </div>
									  	</li>
                                                                         <?php } ?>
									</ul>							  		
							  </div>
							</div>
						</div>
					</div>
             </div>
         </div>
			<div class="col-md-3 col-sm-3" style="padding:0 ">
	         <div class="publicprofile student-dashboard-left">
                                    <div class="profileimg">
						<span id="replaceimage">
						   <img src="<?php if (isset($userdata['User']['user_image']) && $userdata['User']['user_image'] != '') { ?>
                     <?php echo $this->webroot; ?>user_images/<?php
                     echo $userdata['User']['user_image'];
                 } else {
                     echo $this->webroot;
                     ?>img/profile_img.jpg<?php } ?>"
                 width="190" height="190" alt=""/>
						</span>
						<div class="update-pic-icon">
						   <div class="update-pic-img"><i style="cursor:pointer;" class="fa fa-pencil-square-o" aria-hidden="true" id="uploadbox">
						       </i><input name="uploadbox" class="uploadbox" id="form" style="display:none" type="file">
						   </div>
						</div>
						<h2><?php echo $userdata['User']['first_name']." ".$userdata['User']['last_name']; ?></h2>
                                                <p><?php
                                                    if ($userdata['User']['admin_type'] == 1) {
                                                        echo 'Instructor';
                                                    } else {
                                                        echo 'Student';
                                                    }
                                                    ?></p>
				    </div>
				    <ul class="open-close-menu">
<!--						<li><a href="#" class="clicks"><span class="fa fa-dashboard"></span>Dashboard <i class="fa fa-plus"></i></a>
							<ul>
								<li><a href="">link 1</a></li>
								<li><a href="">link 2</a></li>
								<li><a href="">link 3</a></li>
							</ul>
						</li>-->
                                                <li><a href="<?php echo $this->webroot . 'users/dashboard'; ?>" class="clicks"><span class="fa fa-dashboard"></span>Dashboard <i class="fa fa-plus"></i></a>
						<li><a href="<?php echo $this->webroot . 'users/myaddedcourse'; ?>"><span class="fa fa-graduation-cap"></span>Courses <i class="fa fa-plus"></i></a></li>
<!--						<li><a href="#"><span class="fa fa-question-circle"></span>Forum <i class="fa fa-plus"></i></a></li>-->
						<li><a href="<?php echo $this->webroot . 'users/editprofile'; ?>"><span class="fa fa-user"></span>Account <i class="fa fa-plus"></i></a></li>
<!--						<li><a href="#"><span class="fa fa-commenting"></span>Massages <i class="fa fa-plus"></i></a></li>-->
						<li><a href="<?php echo $this->webroot . 'users/logout'; ?>"><span class="fa fa-power-off"></span>Logout <i class="fa fa-plus"></i></a></li> 
					</ul>
				</div>
	      </div>
      </div>
	</div>
</section>
<script>
	$(document).ready(function(){
		$('.open-close-menu > li > a.clicks').click(function(){
			$(this).find('.fa-plus').toggleClass('fa-minus');
			$(this).next('ul').slideToggle();
		});
	})
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>


    $(document).on("change", "#form", function () {

        var file_data = $("#form").prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
        //form_data.append("user_id", 123)                 // Adding extra parameters to form_data
        $.ajax({
            url: "<?php echo $this->webroot . 'users/upload_image'; ?>",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data, // Setting the data attribute of ajax with file_data
            type: 'post', success: function (result) {
                $("#replaceimage").html('');
                $("#replaceimage").html('<img src="<?php echo $this->webroot; ?>user_images/' + result + '" />');
                $("#replaceimageTop").html('');
                $("#replaceimageTop").html('<img src="<?php echo $this->webroot; ?>user_images/' + result + '" />');
            }
        });
    })
    $(document).on("click", "#uploadbox", function () {
        $("#form").trigger("click");
    });


</script>
