<style>
.IN-widget, .IN-widget span , span[id^='li_ui_li_gen_'], a[id^='li_ui_li_gen_']{
    width:100%;
}

.inner_header{
    background:#0c2440;
}
.btnYellow{ margin-bottom: 10px; }

</style>

<script type="text/javascript" src="http://platform.linkedin.com/in.js">
    api_key: 81t9rpn8scwyhb
    authorize: true
    onLoad: onLinkedInLoad
</script>
<section class="login-header-bg" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
  <div class="container">
      <div class="row justify-content-right d-flex">
          <div class="col-md-12">
          <div class="login-box-form">

              <h1>Login</h1>

              <form class="login-form" action="<?php echo $this->webroot?>users/login" method="post">

                <label for="email">Username or Email </label>
                <input type="text" placeholder="Enter Email" name="data[User][email_address]" required>

                <label for="psw">Password</label>
                <input type="password" placeholder="Enter Password" name="data[User][user_pass]" required>
                <div class="row">

                <div class="col-md-6">
                  <label class="check-div padding-l">Remember Me
                    <input type="checkbox" name="data[User][rembme]">
                    <span class="checkmark"></span>
                  </label>
                </div>

                <div class="col-sm-12 col-md-6 text-right">
                  <a style="text-decoration: none;" href="">Forgot Your Password?</a>
                </div>

                </div>
                <div class="clearfix text-center">
                  <input type="submit" class="login-form-submit-bnt" name="">
                </div>
          </form>

          <div class="row">
                    <div class="col-sm-12 col-md-6 text-center py-3">
                      <a style="color:#3d5b99;text-decoration: none;" href="<?php echo $this->webroot; ?>users/employerRegistration">Employer Registration</a>
                    </div>

                    <div class="col-sm-12 col-md-6 text-center py-3">
                      <a style="color:#3d5b99;text-decoration: none;" href="<?php echo $this->webroot; ?>normal_users/candidateRegistration">Candidate Registration</a>
                    </div>
                </div>
                  <div class="cartoon-box">
                      <img class="img-fluid" src="<?php echo $this->webroot ?>images/new/cartoon.png">
                  </div>
          </div>
      </div>

      </div>
  </div>
</section>

<script>

$(document).ready(function(){

    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '364420670573883',
            status: true,
            xfbml: true,
            version: 'v2.8'
        });
        $('#fbloginpage').click(function (e) {
            e.preventDefault();
            FB.login(function (response) {
                console.log(response);
                if (!response || response.status !== 'connected') {
                    alert('Failed');
                } else {

                    FB.api('/me', {fields: 'id,first_name,last_name,email'}, function (response) {
                        //console.log(response);
                        var fb_user_id = response.id;
                        var fb_first_name = response.first_name;
                        var fb_last_name = response.last_name;
                        var fb_email = response.email;
                        //console.log(JSON.stringify(response));

                        $.ajax({
                            url: '<?php echo $this->webroot . 'users/sociallogin'; ?>',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                fbId: fb_user_id,
                                email: fb_email,
                                fname: fb_first_name,
                                lname: fb_last_name,
                                signupby: 'facebook'
                            },
                            beforeSend: function () {
                                $('#social_login_msg').css({
                                    'border-color': '#0f0'
                                }).text('Sending please wait...').show();
                            },
                            success: function (data) {
                                //console.log(data);
                                if (data.ack == '1') {
                                    $('#social_login_msg').css({
                                        'border-color': '#0f0'
                                    }).text(data.msg).show();
                                    setTimeout(function () {
                                        window.location.href = '<?php echo $this->webroot; ?>users/homepage';
                                    }, 3000);
                                } else {
                                    $('#social_login_msg').css({
                                        'border-color': '#0f0'
                                    }).text(data.msg).show();
                                    setTimeout(function () {
                                        $('#social_login_msg').hide();
                                    }, 3000);
                                }
                            }
                        });
                    });
                }
            }, {scope: 'public_profile,email'});
        });

    });
})

$(document).ready(function () {
    $("#UserLoginForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[User][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address'
                    }

                }

            },
            'data[User][user_pass]': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    }
                }
            }
        }
    })

});


    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        $('a[id*=li_ui_li_gen_]').html('<li class="linkedini"><a href="javascript:void(0)" ><i class="fa fa-linkedin"></i></a></li>');
        IN.Event.on(IN, "auth", getProfileData);
    }
     // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
        var ln_id           = data.values[0].id;
        var ln_email        = data.values[0].emailAddress;
        var ln_firstName    = data.values[0].firstName
        var ln_lastName     = data.values[0].lastName;

        $.ajax({
            url: '<?php echo $this->webroot . 'users/linkedinLoginRegister' ?>',
            type: 'post',
            dataType: 'json',
            data: {
                ln_id: ln_id,
                ln_email: ln_email,
                ln_firstName: ln_firstName,
                ln_lastName: ln_lastName
            },
            success: function(data) {
                //console.log(data);
                if(data.ack == 1) {
                    if(data.url != '') {
                        window.location.href = data.url;
                    } else {
                         location.reload();
                    }
                }
            }
        });

    }

    // Handle an error response from the API call
    function onError(error) {
        //console.log(error);
        $('#errMsg').html('');
        $('#errMsg').html('<div class="col-md-12"><div class="alert alert-danger"><strong>Error!</strong>'+error+'</div></div>');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        //IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me").fields("id","first-name", "last-name", "email-address").result(onSuccess).error(onError);
    }


   (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/client:plusone.js?onload=googleonLoadCallback1';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();

    function googleonLoadCallback1()
    {
        gapi.client.setApiKey('AIzaSyC5RAcRoRxt3KoJolWkaza-SwSSH2xVo6U'); //set your API KEY
        gapi.client.load('plus', 'v1', function () {
        });//Load Google + API
    }

    function google_login() {
        var myParams = {
            'clientid': '950909291162-aafe95gcrv1h8u02kf6kl2vl77kijq6a.apps.googleusercontent.com', //You need to set client id
            'cookiepolicy': 'single_host_origin',
            'callback': 'googleloginCallback', //callback function
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
        };
        gapi.auth.signIn(myParams);
    }

    function googleloginCallback(result) {
        if (result['status']['signed_in'])
        {
            //console.log(result);
            //alert("Login Success");
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });

            request.execute(function (resp) {
                var email = resp.emails[0].value;
                var gpId = resp.id;
                var fname = resp.name.familyName;
                var lname = resp.name.givenName;

                $.ajax({
                    url: '<?php echo $this->webroot . 'users/sociallogingplus'; ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        gpId: gpId,
                        email: email,
                        fname: fname,
                        lname: lname,
		                signupby:'googleplus'
                    },
                    beforeSend: function () {
                        $('#social_login_msg').css({
                            'border-color': '#0f0'
                        }).text('Sending please wait...').show();
                    },
                    success: function (data) {
                        if (data.ack == '1') {
                            $('#social_login_msg').css({
                                'border-color': '#0f0'
                            }).text(data.msg).show();
                            setTimeout(function () {
                             window.location.href = '<?php echo $this->webroot . 'users/homepage'; ?>';
                            }, 3000);


                        } else {
                            $('#social_login_msg').css({
                                'border-color': '#0f0'
                            }).text(data.msg).show();
                            setTimeout(function () {
                                $('#social_login_msg').hide();
                            }, 3000);
                        }
                    }
                });

            });
        }

    }
</script>
    <!-- Bootstrap core JavaScript -->
    <script>
        $(window).scroll(function(){
          var sticky = $('.sticky'),
              scroll = $(window).scrollTop();

          if (scroll >= 50) sticky.addClass('fixed');
          else sticky.removeClass('fixed');
        });
    </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                dots:true,
                nav: false,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          </script>
