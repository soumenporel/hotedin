<?php
//pr($settings);
//echo $cookieHelper->read('landing');
//echo $cookieHelper->write('landing', '2', $encrypt = false, $expires = null);
//echo $cookieHelper->read('landing');
?>
<style>
.help-block{ color: red; }
</style>
<section class="inner_banners">
    <img src="<?php echo $this->webroot; ?>images/contact.jpg" alt="" style="width:100%;"/> 

</section>
<section class="contact_holder" style="margin-bottom:65px;">
    <div class="container" style="margin-top: 50px;">
        <div class="row">
            <div class="col-md-6 col-sm-6 contacts">
	    <h2 class="heading_text">Contact Us</h2>
                <i>Please fill the form & send it to us.</i>
                <?php
                if($this->Session->read('Contact.ack') == '1') {
                    echo '<p class="text-success">'.$this->Session->read('Contact.msg').'</p>';
                    if($this->Session->read('Contact.msg')) {
                        unset($_SESSION['Contact']);
                    }
                } else {
                    echo '<p class="text-danger">'.$this->Session->read('Contact.msg').'</p>';
                    if($this->Session->read('Contact.msg')) {
                        unset($_SESSION['Contact']);
                    }
                }
                ?>
                <form action="<?php echo $this->webroot . 'users/contact_us'; ?>" method="post" id="UserLoginForm">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Your Name</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="data[Contact][name]" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Your Email</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" name="data[Contact][email_address]" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Subject</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" name="data[Contact][subject]" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Your Message</label>
                        <textarea class="form-control" id="exampleInputPassword1" name="data[Contact][message]" placeholder=""></textarea>
                    </div>
                    <!-- <div class="g-recaptcha" data-sitekey="6LcekA0UAAAAAHvOP92mWJCAb0T00zXnipd9ovdA"></div> -->
                    <button type="submit" class="btn btn-default" style="background: #08d6e8 none repeat scroll 0 0;
border: 0 none;
color: #fff;
font-size: 14px;
padding: 11px 30px;
text-transform: uppercase;">SEND</button>
                </form>
            </div>
            <div class="col-md-6 contact_info">
              <h2 class="heading_text">Contact Information:</h3>
                <ul>
                    <li><i class="fa fa-map-marker"></i><span><?php echo ' '.$settings['Setting']['address']; ?></span></li>
                    <li><i class="fa fa-phone"></i><span><?php echo ' '.$settings['Setting']['phone']; ?></span></li>
                    <li><i class="fa fa-envelope"></i><span><?php echo ' '.$settings['Setting']['site_email']; ?></span></li>
                    <li><i class="fa fa-skype"></i><span><?php echo ' '.$settings['Setting']['skype']; ?></span></li>
                </ul>
                <h3>Map Location:</h3>
                    <!--<img src="<?php echo $this->webroot; ?>images/map_section.jpg" alt="" class="img-responsive"/>-->
                    <iframe width="100%" height="230" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?php echo $settings['Setting']['address']; ?>&output=embed"></iframe>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function () {
    $("#UserLoginForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
	  'data[Contact][name]': {
                validators: {
                    notEmpty: {
                        message: 'Name is required and cannot be empty'
                    }
                }
            },
            'data[Contact][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address'
                    }
                }

            },
	     'data[Contact][subject]': {
                validators: {
                    notEmpty: {
                        message: 'Subject is required and cannot be empty'
                    }
                }
            },
	       'data[Contact][message]': {
                validators: {
                    notEmpty: {
                        message: 'Message is required and cannot be empty'
                    }
                }
            }
        }
    })
});
$("#UserLoginForm").submit(function(){
    // var googleResponse = jQuery('#g-recaptcha-response').val();
    // if (!googleResponse) {
    //     $('<p style="color:red !important" class=error-captcha"><span class="glyphicon glyphicon-remove " ></span> Please fill up the captcha.</p>" ').insertAfter("#html_element");
    //     return false;
    // } else {

    //     return true;
    // }
});
</script>
<script src='https://www.google.com/recaptcha/api.js'></script> 