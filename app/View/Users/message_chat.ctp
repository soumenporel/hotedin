<section class="inner-wrapper" style="background:#EFEFF4;">
	<div class="container">
		<div class="row">
			<div class="col-md-11 middle-div">
				<div class="row">
					<div class="col-md-3 chat_left">
						<div class="chat_left_holder">
							<div class="chat_header_search">
								<form class="form-inline">
								  <div class="form-group">
								    <input type="text" class="form-control" id="exampleInputEmail2" placeholder="">
								  </div>
								  <button type="submit" class="btn btn-default">Go</button>
								</form>
							</div>
							<div class="chat_left_list">
								<div class="media activs">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								    <span class="pink">MarketPlace</span>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">We Love Shoes</h4>
								    <p>Hithere,Are you still looking for this...</p>
								  </div>
								  <div class="media-right">
								    <span>4:40 AM</span>
								    <b>3</b>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								    <span class="sky">MarketPlace</span>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">We Love Shoes</h4>
								    <p>Hithere,Are you still looking for this...</p>
								  </div>
								  <div class="media-right">
								    <span>4:40 AM</span>
								    <b>3</b>
								  </div>
								</div>
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								    <span class="yellow">MarketPlace</span>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">We Love Shoes</h4>
								    <p>Hithere,Are you still looking for this...</p>
								  </div>
								  <div class="media-right">
								    <span>4:40 AM</span>
								    <b>3</b>
								  </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 chat_middle">
						<div class="chat_middle_holder">
							<div class="chat_mid_header">
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">Macbook Pro 17"</h4>
								    <p><i class="head_locatin"></i>5 miles - Lawrenceville</p>
								    <p><i class="head_times"></i>5 min ago</p>
								  </div>
								  <div class="media-right">
								    <span>$2,350</span>
								    <p>Negotiable</p>
								  </div>
								</div>
							</div>
							<div class="chat_body">
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								  </div>
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
								<div class="media media_me">
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
								<div class="sp_date">
									<span>THU, SEP 18, 2:43 PM</span>
								</div>
								<div class="media">
								  <div class="media-left">
								    <a href="#">
								      <img class="media-object" src="<?php echo $this->webroot; ?>/images/user-image.jpg">
								    </a>
								  </div>
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
								<div class="media media_me">
								  <div class="media-body">
								    <p>Hithere,Are you still looking for this... <span>14:19 pm</span></p>
								   
								  </div>
								</div>
							</div>
							<div class="chat_footer">
								<form class="form-inline">
								  <div class="form-group">
								    <input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
								  </div>
								  <div class="form-group">
								    <button type="submit" class="btn btn-default"><img src="<?php echo $this->webroot; ?>/images/flight-white.svg" style="width:25px"></button>
								  </div>
								  <div class="form-group">
								    <button type="submit" class="btn btn-default acpt_ofr">Accept Offer</button>
								  </div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="spotlight">
							<div class="sponsord_add">
								<h3>Sponsored Ads</h3>
								<span>Based on your interests</span>
							</div>
							<div class="spotlight-holder">
								<div class="spotlight-holder-top">
									<span class="round"><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></span>
									<p>We Love Shoes <span class="small-desc">75 Products  |  19 followers</span></p>
								</div>
								<ul>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-1.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-2.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-3.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-4.png"></li>
								</ul>
								<div class="clearfix"></div>
								<p class="text-right follow-link"><a class="btn btn-default btn-sm" href="">Follow</a></p>
							</div>
							<div class="spotlight-holder">
								<div class="spotlight-holder-top">
									<span class="round"><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></span>
									<p>We Love Shoes <span class="small-desc">75 Products  |  19 followers</span></p>
								</div>
								<ul>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-1.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-2.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-3.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-4.png"></li>
								</ul>
								<div class="clearfix"></div>
								<p class="text-right follow-link"><a class="btn btn-default btn-sm" href="">Follow</a></p>
							</div>
							<div class="spotlight-holder">
								<div class="spotlight-holder-top">
									<span class="round"><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></span>
									<p>We Love Shoes <span class="small-desc">75 Products  |  19 followers</span></p>
								</div>
								<ul>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-1.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-2.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-3.png"></li>
									<li><img class="img-responsive" alt="" src="<?php echo $this->webroot; ?>/images/spot-4.png"></li>
								</ul>
								<div class="clearfix"></div>
								<p class="text-right follow-link"><a class="btn btn-default btn-sm" href="">Follow</a></p>
							</div>
							<p class="text-center">
								<a href="">See More</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="acpt_offer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Accept Offer</h4>
      </div>
      <form>
	      <div class="modal-body">
			  <div class="form-group">
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Meeting Tittle">
			  </div>
			  <div class="form-group">
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Location">
			  </div>
			  <div class="form-group">
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Price (If any)">
			  </div>
			  <div class="form-group">
			    <textarea class="form-control" id="exampleInputEmail1" placeholder="Meeting Details"></textarea>
			  </div>
			  <div class="form-group">
			  	<div class="row">
			  		<div class="col-md-6"><input type="date" class="form-control" id="exampleInputEmail1" placeholder="Date"></div>
			  		<div class="col-md-6"><input type="time" class="form-control" id="exampleInputEmail1" placeholder="Time"></div>
			  	</div>
			  </div>
			  <div class="form-group">
			   	<p>Who’s Going</p>
			   	<ul>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></li>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></li>
				</ul>
			  </div>
			  <div class="form-group">
			   	<p>Emergency Contact</p>
			   	<ul>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/user-image.jpg"></li>
				   	<li><img alt="" src="<?php echo $this->webroot; ?>/images/plus_circle.svg"></li>
				</ul>
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	        <button type="button" class="btn btn-primary">Close Deal</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script>
	$('#acpt_offer').modal('show');
</script>











