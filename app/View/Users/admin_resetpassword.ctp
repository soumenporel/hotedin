<div class="login-holder">
  <h1><?php echo 'Reset Password'; ?></h1>
  <form action="" method="post" id="resetpassword">
    
    
    <div class="input text">
        <label for="password">Password</label>
            <input type="password" class="form-control " id="password" name="data[User][password]"  placeholder="<?php echo 'Enter New Password'; ?>"/>
    </div>
    
    <div class="input text">
      <label for="confirmpassword">Confirm Password</label>
          <input type="password" class="form-control " id="confirmpassword" name="data[User][confpassword]"  placeholder="<?php echo 'Confirm Password';?>"/>
    </div>
            <input type="submit" value="Submit">
  </form>
</div>

<script>
    $("form").submit(function(){ 
    pass=$("#password").val();
    confirm_pass=$("#confirmpassword").val();
    if(pass!=confirm_pass){
        $('#confirmpassword').get(0).setCustomValidity("Confirm  password doe's not match.");        
        return false;
    }  
    else {  
        $('#confirmpassword').get(0).setCustomValidity("");        
        
        }                 
});  
 </script>