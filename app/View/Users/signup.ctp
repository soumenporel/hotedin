<style>
/*  #alertDayMessage2 small, #alertDayMessage1 small, #alertDayMessage small { color : #a94442;}
  .social_buttons button {
    border: 0 none;
    border-radius: 2px;
    color: #fff;
    font-size: 14px;
    height: 40px;
    letter-spacing: 1px;
    margin: 5px 0;
    text-transform: uppercase;
    width: 100%;
}
.social_buttons .ln_btn
{
    background: #0077B5;
}
.IN-widget, .IN-widget span , span[id^='li_ui_li_gen_'], a[id^='li_ui_li_gen_']{
    width:100%;
}*/
.li_ui_li_gen_1503638785983_0-title-text{
  display: none;
}
</style>

    <!--  inner  slider   -->

    <section class="home-slider inner-banner">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/1.jpg" alt="First slide">

            </div>
              <div class="carousel-item">
              <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/2.jpg" alt="First slide">
            </div>
              <div class="carousel-item">
              <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/3.jpg" alt="First slide">
                </div>
            </div>
            <?php echo $bannerContent['CmsPage']['page_description']; ?>
          </div>
        </div>
    </section>

    <section class="py-5">
      <div class="container">
          <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">Ready to Start Teaching? <br>Let's Start Now. It's <span class="font-weight-bold" style="color:#f00; "> Free</span>
            <em class="dvdr">
              <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
            </em>
          </h1>
          <!-- <h1 class="zilla text-blue font-weight-light text-center">Become A Student and Start Learning. It's <span class="text-danger">Free</span></h1> -->
          <div class="row mt-5">
              <div class="col-lg-5">
                  <img src="<?php echo $this->webroot; ?>img/blue-icon.png" alt="" class="img-fluid">
                  <!-- <h1 class="zilla text-danger font-weight-light my-3">Learn from Studilmu and Enhance Your Skills.</h1> -->
              </div>
              <div class="col-lg-7">
                  <form class="row" action="" method="post" id="UserSignupForm" >
                      <div class="form-group col-md-6">
                          <label class="font-14">First Name</label>
                          <input type="text" id="firstname" name="data[User][first_name]" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                          <label class="font-14">Last Name</label>
                          <input type="text" id="lastname" name="data[User][last_name]" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                          <label class="font-14">Email</label>
                          <input type="email" id="email" name="data[User][email_address]" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                          <label class="font-14">Password</label>
                          <input type="password" name="data[User][user_pass]" class="form-control">
                          <input type="hidden" id="fbid" name="data[User][facebook_id]" class="form-control">
                          <input type="hidden" id="gpid" name="data[User][googleplus_id]" class="form-control">
                          <input type="hidden" id="lnid" name="data[User][linkedin_id]" class="form-control">
                      </div>
                      <div class="form-group col-md-3">
                        <a type="submit" id = "fbsignupTeacher" class="btn btn-gp btn-lg text-uppercase join-us-grp-btn">
                          <i class="fa fa-facebook"> </i>
                        </a>
                      </div>
                      <div class="form-group col-md-3">
                        <a type="submit" onclick="google_signup()" class="btn btn-fb btn-lg text-uppercase join-us-grp-btn">
                          <i class="fa fa-google-plus"></i>
                        </a>
                      </div>
                      <div class="form-group col-md-3" >
                        <!--<a  href="Javascript: void(0);" style="display: inline-block; width: 100%;"><script type="in/Login" ></script></a>-->
                        <a type="submit" onclick="" class="btn btn-lkd btn-lg text-uppercase join-us-grp-btn">
                          <i class="fa fa-linkedin"></i>
                        </a>
                      </div>
                      <div class="form-group col-md-3">
                          <button type="submit" class="btn btn-success btn-lg text-uppercase join-us-grp-btn">join Us</button>
                      </div>
                  </form>
              </div>
              <h1 class="zilla text-danger font-weight-light mt-5 col-lg-12 text-center"><sup>*</sup> Teach on Studilmu share your passion and inspire others.</h1>
          </div>
      </div>
    </section>


    <!--    banner bottom area  -->
    <?php echo $bannerMenu['CmsPage']['page_description']; ?>
    <!--   start teaching   -->


    <!--  testimonials  -->

    <section class="testimonials py-5 bg-faded text-center">
      <div class="container">
          <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"><span class="font-weight-bold" style="color:#f00; "> Testimonials</span>
            <em class="dvdr">
              <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
            </em>
          </h1>
          <div id="carouseltestimonials" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php $aCount = 1; foreach ($testimonialdata as $key => $testimonial) { ?>
                  <div class="carousel-item <?php if($aCount==1){ echo 'active'; } ?>">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-test">
                            <h5 class="font-weight-light"><?php echo $testimonial['Testimonial']['description']; ?></h5>
                            <!-- <div class="test-pic my-3 mx-auto">
                                <img src="<?php echo $this->webroot;?>user_images/<?php echo $testimonial['User']['user_image']?>" alt="">
                            </div> -->
                            <h5 class="font-weight-bold"><?php echo $testimonial['User']['first_name'].' '.$testimonial['User']['last_name']; ?></h5>
                        </div>
                    </div>
                  </div>
                <?php $aCount++; } ?>
                <!-- <div class="carousel-item">
                  <div class="row justify-content-center">
                      <div class="col-lg-8 col-test">
                          <h5 class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h5>
                          <div class="test-pic my-3 mx-auto">
                              <img src="img/test-1.png" alt="">
                          </div>
                          <h5 class="font-weight-bold">James Ranson</h5>
                      </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="row justify-content-center">
                      <div class="col-lg-8 col-test">
                          <h5 class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h5>
                          <div class="test-pic my-3 mx-auto">
                              <img src="img/test-1.png" alt="">
                          </div>
                          <h5 class="font-weight-bold">James Ranson</h5>
                      </div>
                  </div>
                </div> -->
              </div>
              <ol class="carousel-indicators mt-3">
                <?php $ti = 0; foreach ($testimonialdata as $key => $testimonial) { ?>
                  <li data-target="#carouseltestimonials" data-slide-to="<?php echo $ti; ?>" class="<?= ($ti==0) ? 'active': ''; ?>"></li>
                <?php $ti++; } ?>
                <!-- <li data-target="#carouseltestimonials" data-slide-to="0" class="active"></li>
                <li data-target="#carouseltestimonials" data-slide-to="1"></li>
                <li data-target="#carouseltestimonials" data-slide-to="2"></li> -->
              </ol>
            </div>
      </div>
    </section>



    <!--   faq   -->


    <section class="py-5 faq">
        <div class="container">
            <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">Frequently Asked Question <span class="font-weight-bold" style="color:#f00; "> (FAQ)</span>
              <em class="dvdr">
                <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
              </em>
            </h1>
            <div id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($faqs as $key => $faq) { ?>
                <div class="card mb-3">
                    <div class="card-header border-bottom-0" role="tab" id="headingOne">
                        <h4 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['Faq']['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['Faq']['id']; ?>" class="d-block text-blue">
                              <span class="font-14"><?php echo $faq['Faq']['title']; ?></span> <i class="ion-ios-plus-outline float-right"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $faq['Faq']['id']; ?>" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
		                  <div class="card-block pt-0">
		                    <p class=" font-14 text-gray"><?php echo $faq['Faq']['description']; ?></p>
		                  </div>
		                </div>
                </div>

                <?php } ?>
            </div>
        </div>
    </section>
<script>
$(document).ready(function () {
    $("#UserSignupForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[User][first_name]': {
                validators: {
                    notEmpty: {
                        message: 'The First Name is required and cannot be empty.'
                    }
                }
            },
            'data[User][last_name]': {
                validators: {
                    notEmpty: {
                        message: 'The Last Name is required and cannot be empty.'
                    }
                }
            },
            'data[User][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty.'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address. '
                    }
                }

            },
            'data[User][user_pass]': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty.'
                    }
                }
            }
        }
    });


    //Facebook login
    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '364420670573883',
            status: true,
            xfbml: true,
            version: 'v2.8'
        });

        $('#fbsignupTeacher').click(function (e) {
            e.preventDefault();
            FB.login(function (response) {
                console.log(response);
                if (!response || response.status !== 'connected') {
                    alert('Failed');
                } else {

                    FB.api('/me', {fields: 'id,first_name,last_name,email'}, function (response) {
                        console.log(response);
                        var fb_user_id = response.id;
                        var fb_first_name = response.first_name;
                        var fb_last_name = response.last_name;
                        var fb_email = response.email;
                        console.log(JSON.stringify(response));
                        //alert(fb_first_name);
                        $('#firstname').val(fb_first_name);
                        $('#lastname').val(fb_last_name);
                        $('#email').val(fb_email);
                        $('#fbid').val(fb_user_id);

                    });
                }
            }, {scope: 'public_profile,email'});
        });

    });
});

    function google_signup() {
        // console.log('hii');
        var myParams = {
            'clientid': '781797887038-sk9f5h5rujn8r0on8v1den33ifkanc9n.apps.googleusercontent.com',
            //You need to set client id
            'cookiepolicy': 'single_host_origin',
            'callback': 'googleSignupCallback', //callback function
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
        };
        gapi.auth.signIn(myParams);
    }

    function googleSignupCallback(result) {
        if (result['status']['signed_in'])
        {

            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });

            request.execute(function (resp) {
                console.log(resp);
                var email = resp.emails[0].value;
                var gpId = resp.id;
                var fname = resp.name.givenName;
                var lname = resp.name.familyName;

                $('#firstname').val(fname);
                $('#lastname').val(lname);
                $('#email').val(email);
                $('#gpid').val(gpId);

            });
        }
    }

    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        $('a[id*=li_ui_li_gen_]').html('<button class="ln_btn btn btn-info"><i class="fa fa-linkedin"></i> <?php echo 'Linkedin'; ?></button>');
        IN.Event.on(IN, "auth", getProfileData);
    }
     // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
        var ln_id           = data.values[0].id;
        var ln_email        = data.values[0].emailAddress;
        var ln_firstName    = data.values[0].firstName
        var ln_lastName     = data.values[0].lastName;

        $('#firstname').val(ln_firstName);
        $('#lastname').val(ln_lastName);
        $('#email').val(ln_email);
        $('#lnid').val(ln_id);

        // $.ajax({
        //     url: '<?php echo $this->webroot . 'users/linkedinLoginRegister' ?>',
        //     type: 'post',
        //     dataType: 'json',
        //     data: {
        //         ln_id: ln_id,
        //         ln_email: ln_email,
        //         ln_firstName: ln_firstName,
        //         ln_lastName: ln_lastName
        //     },
        //     success: function(data) {
        //         if(data.ack == 1) {
        //             if(data.url != '') {
        //                 window.location.href = data.url;
        //             } else {
        //                  onLinkedInLoad();
        //             }
        //         } else {

        //             onLinkedInLoad();
        //         }
        //     }
        // });

    }

    // Handle an error response from the API call
    function onError(error) {
        //console.log(error);
        $('#errMsg').html('');
        $('#errMsg').html('<div class="col-md-12"><div class="alert alert-danger"><strong>Error!</strong>'+error+'</div></div>');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        //IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me").fields("id","first-name", "last-name", "email-address").result(onSuccess).error(onError);

    }

</script>
