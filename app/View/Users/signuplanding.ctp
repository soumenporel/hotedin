      <style>
      .bg-light-blue {
          background: #2F75B5 !important;
      }
      </style>
      <!--  inner  slider   -->
      
      <section class="home-slider inner-banner">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                  <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/3.jpg" alt="First slide">
                    
                </div>
                  <div class="carousel-item">
                  <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/3.jpg" alt="First slide">
                    
                </div>
                  <div class="carousel-item">
                  <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/3.jpg" alt="First slide">
                    
              </div>

                    <div class="carousel-caption">
                        <div class="row text-left align-items-center">
                          <div class="col-xl-5 col-lg-7 text-white">
                              <h2 class="zilla">Keep Learning, Keep Growing</h2>
                              <h4 class="font-weight-light mb-4">When you stop learning, you stop growing.
                                  Never stop learning, because life never stop teaching</h4>
                          </div>
                      </div>
                </div>
            </div>
      </section>
      
      <!--    banner bottom area  -->
      
      <section class="banner-bottom-area bg-blue py-5">
        <?php echo $bannerContent['CmsPage']['page_description']; ?>
      </section>
      
      
      <!--  our  pricing  -->
      
      <section class="py-5 pricing">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-lg-7 text-center mb-4">
                      <h1 class="zilla font-weight-light text-blue">Our Pricing</h1>
                      <p class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                  </div>
              </div>
          <table class="table table-bordered table-responsive d-block d-lg-table">
              <thead>
                  <th><h1 class="zilla font-weight-light">Pricing</h1></th>
                  <th class="bg-danger text-white text-center"><h1 class="zilla font-weight-light text-uppercase">Free</h1></th>
                  <th class="text-center p-0 price-th">
                      <h4 class="zilla font-weight-light m-0 p-3 bg-blue bg-light-sky text-white">Basic Membership</h4>
                      <table  class="w-100"> 
                          <tr>
                              <td class="border-top-0 border-bottom-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['0']['MembershipPlan']['duration'].' '.$mPlans['0']['MembershipPlan']['duration_in']; ?></p></td>
                              <td class="border-top-0 border-bottom-0 border-right-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['1']['MembershipPlan']['duration'].' '.$mPlans['1']['MembershipPlan']['duration_in']; ?></p></td>
                          </tr>
                      </table>
                  </th>
                  <th class="text-center p-0 price-th">
                      <h4 class="zilla font-weight-light m-0 p-3 bg-success bg-light-blue text-white">Premium Membership</h4>
                      <table  class="w-100">
                          <tr>
                              <td class="border-top-0 border-bottom-0 border-left-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['2']['MembershipPlan']['duration'].' '.$mPlans['2']['MembershipPlan']['duration_in']; ?></p></td>
                              <td class="border-top-0 border-bottom-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['3']['MembershipPlan']['duration'].' '.$mPlans['3']['MembershipPlan']['duration_in']; ?></p></td>
                          </tr>
                      </table>
                  </th>
              </thead>
              <tbody>
                  <?php foreach ($planItems as $key => $item) { ?>
                  <tr>
                      <td><h5><?php echo $item['MembershipItem']['name']; ?></h5></td>
                      <?php  
                        $planList = $this->requestAction('/homepages/itemList/' . $item['MembershipItem']['id']);
                      ?>
                      <td class="text-center"><h4><i class="text-danger <?php echo (in_array($mPlans['4']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['0']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['1']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['2']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?> "></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['3']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <?php } ?>
                  <!-- <tr>
                      <td><h5>Unlimited Learning from all videos</h5></td>
                      <td class="text-center"><h4><i class="text-danger <?php echo ($mPlans['4']['MembershipPlan']['unlimited_learning']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['0']['MembershipPlan']['unlimited_learning']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['1']['MembershipPlan']['unlimited_learning']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                                    <h4><i class="text-danger <?php echo ($mPlans['2']['MembershipPlan']['unlimited_learning']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                                  <td class="text-center border-0 w-50">
                                    <h4><i class="<?php echo ($mPlans['3']['MembershipPlan']['unlimited_learning']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td><h5>Discussion</h5></td>
                      <td class="text-center"><h4><i class="text-danger <?php echo ($mPlans['4']['MembershipPlan']['discussion']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['0']['MembershipPlan']['discussion']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['1']['MembershipPlan']['discussion']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                                    <h4><i class="text-danger <?php echo ($mPlans['2']['MembershipPlan']['discussion']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                                  <td class="text-center border-0 w-50">
                                    <h4><i class="<?php echo ($mPlans['3']['MembershipPlan']['discussion']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td><h5>Assignment</h5></td>
                      <td class="text-center"><h4><i class="text-danger <?php echo ($mPlans['4']['MembershipPlan']['assignment']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['0']['MembershipPlan']['assignment']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['1']['MembershipPlan']['assignment']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                                    <h4><i class="text-danger <?php echo ($mPlans['2']['MembershipPlan']['assignment']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                                  <td class="text-center border-0 w-50">
                                    <h4><i class="<?php echo ($mPlans['3']['MembershipPlan']['assignment']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td><h5>Test</h5></td>
                      <td class="text-center"><h4><i class="text-danger <?php echo ($mPlans['4']['MembershipPlan']['test']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['0']['MembershipPlan']['test']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['1']['MembershipPlan']['test']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                                    <h4><i class="text-danger <?php echo ($mPlans['2']['MembershipPlan']['test']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                                  <td class="text-center border-0 w-50">
                                    <h4><i class="<?php echo ($mPlans['3']['MembershipPlan']['test']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <tr>
                      <td><h5>Certification</h5></td>
                      <td class="text-center"><h4><i class="text-danger <?php echo ($mPlans['4']['MembershipPlan']['certificate']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['0']['MembershipPlan']['certificate']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo ($mPlans['1']['MembershipPlan']['certificate']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                                    <h4><i class="text-danger <?php echo ($mPlans['2']['MembershipPlan']['certificate']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                                  <td class="text-center border-0 w-50">
                                    <h4><i class="<?php echo ($mPlans['3']['MembershipPlan']['certificate']==1)? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>-->
                  <tr>
                      <td><h4>Membership Fees</h4></td>
                      <td class="text-center"><h4 class="text-success"> Free </h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4 class="text-success">Rp</br> <?php echo number_format($mPlans['0']['MembershipPlan']['price'],0,"",".");?></h4></td>
                                  <td class="text-center border-0 w-50"><h4 class="text-success">Rp</br> <?php echo number_format($mPlans['1']['MembershipPlan']['price'],0,"",".");?></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4 class="text-success">Rp</br> <?php echo number_format($mPlans['2']['MembershipPlan']['price'],0,"",".");?></h4></td>
                                  <td class="text-center border-0 w-50"><h4 class="text-success">Rp</br> <?php echo number_format($mPlans['3']['MembershipPlan']['price'],0,"",".");?></h4></td>
                              </tr>
                          </table>
                      </td>
                  </tr> 
              </tbody>
          </table>
              <div class="text-right">
                  <!-- <a href="#" class="btn btn-danger btn-lg text-uppercase">Free</a> -->
                  <!-- <a href="#" class="btn btn-success btn-lg text-uppercase">Continue</a> -->
              </div>
          </div>
      </section>
      
      
      <!--  testimonials  -->
      
      <section class="testimonials py-5 bg-faded text-center">
      <div class="container">
          <h1 class="zilla text-blue font-weight-light mb-4">Testimonials</h1>
          <div id="carouseltestimonials" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php $slCount = 1; foreach ($testimonialdata as $key => $testimonial) { ?>
                  <div class="carousel-item <?php if($slCount==1){ echo 'active'; } ?>">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-test">
                            <h5 class="font-weight-light"><?php echo $testimonial['Testimonial']['description']; ?></h5>
                            <div class="test-pic my-3 mx-auto">
                                <img src="<?php echo $this->webroot;?>user_images/<?php echo $testimonial['User']['user_image']?>" alt="">
                            </div>
                            <h5 class="font-weight-bold"><?php echo $testimonial['User']['first_name'].' '.$testimonial['User']['last_name']; ?></h5>
                        </div>
                    </div>
                  </div>
                <?php $slCount++; } ?>
                <!-- <div class="carousel-item">
                  <div class="row justify-content-center">
                      <div class="col-lg-8 col-test">
                          <h5 class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h5>
                          <div class="test-pic my-3 mx-auto">
                              <img src="img/test-1.png" alt="">
                          </div>
                          <h5 class="font-weight-bold">James Ranson</h5>
                      </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="row justify-content-center">
                      <div class="col-lg-8 col-test">
                          <h5 class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h5>
                          <div class="test-pic my-3 mx-auto">
                              <img src="img/test-1.png" alt="">
                          </div>
                          <h5 class="font-weight-bold">James Ranson</h5>
                      </div>
                  </div>
                </div> -->
              </div>
              <ol class="carousel-indicators mt-3">
                <?php $ti = 0; foreach ($testimonialdata as $key => $testimonial) { ?>
                  <li data-target="#carouseltestimonials" data-slide-to="<?php echo $ti; ?>" class="<?= ($ti==0) ? 'active': ''; ?>"></li>
                <?php $ti++; } ?>
                <!-- <li data-target="#carouseltestimonials" data-slide-to="0" class="active"></li>
                <li data-target="#carouseltestimonials" data-slide-to="1"></li>
                <li data-target="#carouseltestimonials" data-slide-to="2"></li> -->
              </ol>
            </div>
      </div>
    </section>
      
      
      <!--   faq   -->
      
      
      <section class="py-5 faq">
        <div class="container">
            <h1 class="zilla text-blue font-weight-light mb-5 text-center">Frequently Asked Question (FAQ)</h1>
            <div id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($faqs as $key => $faq) { ?>
                <div class="card mb-3">
                    <div class="card-header border-bottom-0" role="tab" id="headingOne">
                        <h4 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['Faq']['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['Faq']['id']; ?>" class="d-block text-blue">
                              <span class="font-14"><?php echo $faq['Faq']['title']; ?></span> <i class="ion-ios-plus-outline float-right"></i>
                            </a>
                        </h4>
                    </div>
                </div>
                <div id="collapse<?php echo $faq['Faq']['id']; ?>" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                  <div class="card-block pt-0">
                    <p class=" font-14 text-gray"><?php echo $faq['Faq']['description']; ?></p>
                  </div>
                </div>    
                <?php } ?>
            </div>    
        </div>
      </section>