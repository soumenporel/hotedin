

<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}
</style>
<section class="profileedit">
    <div class="container">
        <div class="row" style="background: #f6f6f6; border-left: 1px solid #dedede; border-right: 1px solid #dedede;">
            <div class="col-md-3 col-sm-3" style="padding:0 ">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel');?>
            </div>
            <div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
                <div class="profile_second_part">
                    <h2>Portfolio</h2>
                    <p>Add information about yourself to share on your profile.</p>
                    <div class="row">
                    		<!--<div class="col-md-6 col-sm-6">
									<figure class="course-box bg-white profile-box">
										<a href="" class="delete"><i class="fa fa-trash-o"></i></a>
									   <div class="profile-image">
									       <img src="/team4/studilmu/img/storytelling201.jpg">
									   </div>									   
									   <ul>
									   	<li class="pull-left"><a href="#" data-toggle="modal" data-target="#myModal" class="view">View Profile</a></li>
									   	<li class="pull-right"><a href="" class="edit">Edit Profile</a></li>
									   </ul>
									   <figcaption class="p-3 pt-5">
									       <h5>Rahul Roy</h5>
									       <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the </p>
									   </figcaption>
									</figure>
                      	</div>-->
                      	<div class="col-md-6 col-sm-6">
                      		<div class="portfolio" style="background: url('/team4/studilmu/img/storytelling201.jpg')">
                      			<div class="inner-box"><a href="" class="delete"><i class="fa fa-trash-o"></i></a>
                      				<div class="center-text">
	                      				<a href="#" onclick="openModal();" class="view"><h2>Title lorem ipsum</h2></a>
                      				</div>
                      				<a href="" class="edit"><i class="fa fa-edit"></i></a>
                      			</div>
                      		</div>
                      	</div>
                      	<div class="col-md-6 col-sm-6">
                      		<div class="portfolio" style="background: url('/team4/studilmu/img/storytelling201.jpg')">
                      			<div class="inner-box"><a href="" class="delete"><i class="fa fa-trash-o"></i></a>
                      				<div class="center-text">
	                      				<a href="#" class="view"><h2>Title lorem ipsum</h2></a>
                      				</div>
                      				<a href="" class="edit"><i class="fa fa-edit"></i></a>
                      			</div>
                      		</div>
                      	</div>
                      	<div class="col-md-6 col-sm-6">
                      		<div class="portfolio" style="background: url('/team4/studilmu/img/storytelling201.jpg')">
                      			<div class="inner-box"><a href="" class="delete"><i class="fa fa-trash-o"></i></a>
                      				<div class="center-text">
	                      				<a href="#" class="view"><h2>Title lorem ipsum</h2></a>
                      				</div>
                      				<a href="" class="edit"><i class="fa fa-edit"></i></a>
                      			</div>
                      		</div>
                      	</div>
                      	<div class="col-md-6 col-sm-6">
                      		<div class="portfolio" style="background: url('/team4/studilmu/img/storytelling201.jpg')">
                      			<div class="inner-box"><a href="" class="delete"><i class="fa fa-trash-o"></i></a>
                      				<div class="center-text">
	                      				<a href="#"class="view"><h2>Title lorem ipsum</h2></a>
                      				</div>
                      				<a href="" class="edit"><i class="fa fa-edit"></i></a>
                      			</div>
                      		</div>
                      	</div>
							<!--<div class="col-sm-4 col-md-4 col-12">
                    		<div class="formsPart">
                     
				    <div class="col-sm-12 col-12">
				    	<div class="row">
				    		<div class="col-sm-12 col-md-12 col-12">
				    			<div class="imagesPart">
				    				<img src="<?php echo $this->webroot;?>images/img_lights.jpg" class="img-fluid"></a>
				    				
				    			</div>
				    		</div>
				    	</div>
				    </div>
				    <div class="textPart">
                     	<h3>Lorem Ipsum is simply</h3>
                     	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                     </div>
				    <span class="closeParts"><i class="fa fa-times" aria-hidden="true"></i></span>
				    <div class="text-center"><button type="button" class="btn btn-success btn-sm">View</button></div>
				    </div>
                    	</div>
                    	<div class="col-sm-4 col-md-4 col-12">
                    		<div class="formsPart">
                     
				    <div class="col-sm-12 col-12">
				    	<div class="row">
				    		<div class="col-sm-12 col-md-12 col-12">
				    			<div class="imagesPart">
				    				<img src="<?php echo $this->webroot;?>images/img_lights.jpg" class="img-fluid"></a>
				    				
				    			</div>
				    		</div>
				    	</div>
				    </div>
				    <div class="textPart">
                     	<h3>Lorem Ipsum is simply</h3>
                     	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                     </div>
				    <span class="closeParts"><i class="fa fa-times" aria-hidden="true"></i></span>
				    <div class="text-center"><button type="button" class="btn btn-success btn-sm">View</button></div>
				    </div>
                    	</div>
                    	<div class="col-sm-4 col-md-4 col-12">
                    		<div class="formsPart">
                     
				    <div class="col-sm-12 col-12">
				    	<div class="row">
				    		<div class="col-sm-12 col-md-12 col-12">
				    			<div class="imagesPart">
				    				<img src="<?php echo $this->webroot;?>images/img_lights.jpg" class="img-fluid"></a>
				    				
				    			</div>
				    		</div>
				    	</div>
				    </div>
				    <div class="textPart">
                     	<h3>Lorem Ipsum is simply</h3>
                     	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                     </div>
				    <span class="closeParts"><i class="fa fa-times" aria-hidden="true"></i></span>
				    <div class="text-center"><button type="button" class="btn btn-success btn-sm">View</button></div>
				    </div>
				    	
                    	</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="myModal" class="modal modal-light-box">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext">1 / 2</div>
      <img src="/team4/studilmu/img/cat_img/59917ebfafb53.png" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext">2 / 2</div>
      <img src="/team4/studilmu/img/cat_img/59917e590c53f.png" style="width:100%">
    </div>

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <div class="align-small">
	    <div class="column">
	      <img class="demo cursor" src="/team4/studilmu/img/cat_img/59917ebfafb53.png" style="width:100%" onclick="currentSlide(1)" alt="Nature and sunrise">
	    </div>
	    <div class="column">
	      <img class="demo cursor" src="/team4/studilmu/img/cat_img/59917e590c53f.png" style="width:100%" onclick="currentSlide(2)" alt="Trolltunga, Norway">
	    </div>
    </div>
  </div>
</div>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAr9mFfvbx8XxKEJPE3By2AghiC8c2e-5s&libraries=places&callback=initMap"
        async defer></script>

<script>
      function initMap() {
          var autocomplete = new google.maps.places.Autocomplete($("#UserAddress")[0], {});
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
              var place = autocomplete.getPlace();
              console.log(place.address_components);
          });
      }

      // function loadScript() {
      //     var script = document.createElement("script");
      //     script.type = "text/javascript";
      //     script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAr9mFfvbx8XxKEJPE3By2AghiC8c2e-5s&libraries=places";
      //     document.body.appendChild(script);
      // }

      //window.onload = loadScript;
</script>
