<?php 
// pr($userDetails); exit;
$userType = ''; 
if($userDetails['User']['admin_type']==2){ $userType = 'Student'; }
if($userDetails['User']['admin_type']==1){ $userType = 'Tutor'; }
?>
<style>
.form-horizontal .form_part1 {
    width: 15px;
}
.form-horizontal .formdiv {
    border: 0px; 
    width: 94%;
    padding: 14px 0 20px 20px;
}
.header{
  position:relative;
}

.inner_header{
  background:#0c2440;
}
</style>
<section class="profileedit">
  <div class="container">
    <div class="row" style="background: #f6f6f6; border-right: 1px solid #ddd">
      <div class="col-md-3 col-sm-3" style="border-left: #ddd solid 1px; border-right: #ddd solid 1px; padding: 0">
          <!-- left panel !-->
          <?php echo $this->element('leftpanel');?>
      </div>
      <div class="col-md-9 col-sm-9" style="padding: 0">
        <div class="profile_second_part">
            <h2> Preferences </h2>
            <!-- <p>Add information about yourself to share on your profile.</p> -->
           
              <div id="remove-account">
                  
                      <p>
                          
                             We currently do not have any features for which you can edit preferences.
                              
                          
                      </p>
                  
              </div>
            
        </div>
      </div>
    </div>
  </div>
</section>