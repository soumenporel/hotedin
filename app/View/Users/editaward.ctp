

<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}
</style>
<section class="profileedit">
    <div class="container">
        <div class="row" style="background: #f6f6f6; border-left: 1px solid #dedede; border-right: 1px solid #dedede;">
            <div class="col-md-3 col-sm-3" style="padding:0 ">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel');?>
            </div>
            <div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
                <div class="profile_second_part profile-from">
                    <h2>Edit Award</h2>
                    <p>Add information about yourself to share on your profile.</p>
                     <form action="" enctype="multipart/form-data" id="UserAddawardForm" method="post" accept-charset="utf-8" class="form-area">
                 <div id="picture_msg1" style="display:none;" class="alert"></div>
                            <div class="form-group">
					<label>Award Name:</label>
                                        <input type="text" class="form-control" id="userFname" name="data[Award][name]" value="<?php echo $portfolioDetails['Award']['name'];?>"/>
				</div>
				<div class="form-group">
					<label>Award Description:</label>
                                        <textarea name="data[Award][description]" class="form-control" placeholder="Enter award description" ><?php echo $portfolioDetails['Award']['description'];?></textarea>
				</div>
                                
                              
                         <div class="image_part">
                                    <div class="form-group">
                                      <label>Add Images:</label>
                                       <div class="photo-upload">
<!--                                          <div class="upload-pic">
                                              <img src="<?php echo $this->webroot;?>img/video.jpg" alt="">
                                          </div>-->
                                          <div class="fileUpload">
                                                
                                                <b class="file-input">upload Images <i class="fa fa-upload"></i><input type="file" class="upload" name="data[img][]" multiple></b>
                                            </div>
                                      </div>
                                  </div>
                 
											<ul class="upload-image-box">
												<?php
												//print_r($portfolioDetails);
												//for images................................
													if(!empty($portfolioDetails['AwardImg'])&&count($portfolioDetails['AwardImg'])>0)
													{
													foreach($portfolioDetails['AwardImg'] as $awardimage)
													{
													//print_r($awardimage);
													?>
												<li id="<?php echo $awardimage['id'];?>">
													<a href="<?php echo $this->webroot;?>award/<?php echo $awardimage['name'];?>" class="flipLightBox"> 
													<img src="<?php echo $this->webroot;?>award/<?php echo $awardimage['name'];?>" />
													<span></span> </a>
													<a class="my-gallery-close" onclick="removeFile(<?php echo $awardimage['id'];?>);"><i class="fa fa-close"></i></a>
												</li>
												<?php
												}
												}

												?>
											</ul>    
                                    
                     
                        </div>
                                   
         

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-default" value="Save Change">
                                    </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
       function removeFile(id)
    {
        //alert(1);
        $.ajax({
            url: "<?php echo $this->webroot;?>users/delawardfile", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            dataType: 'json',
            data: {
                id:id
            },   // To send DOMDocument or non processed data file it is set to false
            success: function(result)   // A function to be called if request succeeds
            {
                 
                //console.log(result);
                //alert(result.ACK);
               
                if(result.ACK==1)
                {
                    $('#picture_msg1').addClass('alert-success');        
                    $('#picture_msg1').removeClass('alert-danger');
                    $('#'+id).hide();
                }
                else
                {
                    $('#picture_msg1').addClass('alert-danger');        
                    $('#picture_msg1').removeClass('alert-success');
                }
                $("#picture_msg1").html(result.html);
                $('#picture_msg1').show();
                setTimeout(function(){ $('#picture_msg1').hide(); }, 3000);
            }
            });
    }
</script>
