<section class="profileedit">
	<div class="container">
		<div class="row" style="background: #104A80; border-left: 1px solid #dedede; border-right: 1px solid #dedede;">
	      <div class="col-md-3 col-sm-3" style="padding:0 ">
	         <div class="publicprofile student-dashboard-left">
				    <div class="profileimg">
						<span id="replaceimage">
						  <img src="<?php if (isset($userdata['User']['user_image']) && $userdata['User']['user_image'] != '') { ?>
                     <?php echo $this->webroot; ?>user_images/<?php
                     echo $userdata['User']['user_image'];
                 } else {
                     echo $this->webroot;
                     ?>img/profile_img.jpg<?php } ?>"
                 width="190" height="190" alt=""/>
						</span>
						<div class="update-pic-icon">
						   <div class="update-pic-img"><i style="cursor:pointer;" class="fa fa-pencil-square-o" aria-hidden="true" id="uploadbox">
						       </i><input name="uploadbox" class="uploadbox" id="form" style="display:none" type="file">
						   </div>
						</div>
						<h2><?php echo $userdata['User']['first_name']." ".$userdata['User']['last_name']; ?></h2>
                                                <p><?php
                                                    if ($userdata['User']['admin_type'] == 1) {
                                                        echo 'Instructor';
                                                    } else {
                                                        echo 'Student';
                                                    }
                                                    ?></p>
				    </div>
				    <ul class="open-close-menu">
						<li><a href="<?php echo $this->webroot . 'users/dashboard'; ?>" class="clicks"><span class="fa fa-dashboard"></span>Dashboard <i class="fa fa-plus"></i></a>
						<li><a href="<?php echo $this->webroot . 'user_courses'; ?>"><span class="fa fa-graduation-cap"></span>Courses <i class="fa fa-plus"></i></a></li>
<!--						<li><a href="#"><span class="fa fa-question-circle"></span>Forum <i class="fa fa-plus"></i></a></li>-->
						<li><a href="<?php echo $this->webroot . 'users/editprofile'; ?>"><span class="fa fa-user"></span>Account <i class="fa fa-plus"></i></a></li>
<!--						<li><a href="#"><span class="fa fa-commenting"></span>Massages <i class="fa fa-plus"></i></a></li>-->
						<li><a href="<?php echo $this->webroot . 'users/logout'; ?>"><span class="fa fa-power-off"></span>Logout <i class="fa fa-plus"></i></a></li> 
					</ul>
				</div>
	      </div>
	      <div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
             <div class="profile_second_part student-dashboard-body">
					<h2>Overview</h2>
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
							  <div class="panel-heading">
								  <h3>Courses</h3>
								  <span>Your recent courses</span>
							  </div>
							  <div class="panel-body">
							    <ul>
                                                                <?php
                                                            foreach ($userPosts as $course) {
                                                                //pr( $course['PostImage'] ); exit;
                                                                ?>
									<li>
										<div class="row">
											<div class="col-sm-6">
                                                                                            <p><a href="<?php echo $this->webroot;?>learns/course_content/<?php echo $course['Post']['slug']; ?>">
                                                                                                <?php
                                                                                                    if(strlen($course['Post']['post_title'])<=30)
                                                                                                    {
                                                                                                      echo $course['Post']['post_title'];
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                      $y=substr($course['Post']['post_title'],0,30) . '...';
                                                                                                      echo $y;
                                                                                                    }
                                                                                            // echo $course['Post']['post_title'];
                                                                                            ?>
                                                                                                </a>
                                                                                            </p>
											</div>
											<div class="col-sm-6">
												<div class="progress">
<!--												  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
												    <span class="sr-only">40% Complete (success)</span>
												  </div>-->
												</div>
											</div>
										</div>
									</li>
                                                            <?php } ?>
									<li>
                                                                            <a class="btn btn-primary" href="<?php echo $this->webroot . 'user_courses'; ?>">View more</a>
									</li>
								</ul>
							  </div>
							</div>
							<div class="panel panel-default">
							  <div class="panel-heading">
							  		<h3>Quizzes</h3>
							  		<span>Your recent performence</span>
                                                                        <?php //print_r($quizes); ?>
							  </div>
							  <div class="panel-body">
							  	 	<ul>
                                                                            <?php
                                                            foreach ($quizes as $quize) {
                                                                $quizDetails = $this->requestAction('/users/quizDetails/' . $quize['QuizScore']['quiz_id']);
                                                               // print_r($quizDetails);die;
                                                                ?>
										<li>
											<div class="row">
												<div class="col-sm-8">
													<strong><?php echo $quizDetails['Lecture']['title']; ?></strong>
                                                                                                        <p>Courses <a href="<?php echo $this->webroot . 'learns/course_content/'.$quizDetails['Post']['slug']; ?>" target="_blank"><?php echo $quizDetails['Post']['post_title']; ?></a></p>
												</div>
												<div class="col-sm-4">
													<b><?php echo round($quize['QuizScore']['percentage']); ?>%</b>
												</div>
											</div>
										</li>
										<?php } ?>
									</ul>
							  </div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-default">
							  <div class="panel-heading">
							  		<h3>Rewards</h3>
							  		<span>Your Latest Achivements</span>
							  </div>
							  <div class="panel-body">
							  	 <ul>
									<li>
									    <a href="" class="rewards-circle"><i class="fa fa-star"></i></a>
									    <a href="" class="rewards-circle"><i class="fa fa-mortar-board"></i></a>
									    <a href="" class="rewards-circle"><i class="fa fa-diamond"></i></a>
									    <a href="" class="rewards-circle"><i class="fa fa-share-alt"></i></a>
									    <a href="" class="rewards-circle"><i class="fa fa-smile-o"></i></a>
							    	</li>
							    </ul>
							  </div>
							</div>
							<div class="panel panel-default">
							  <div class="panel-heading">
							  		<h3>Certificates <?php echo count($certificates); ?></h3>
							  		<span>Your Latest Achivements</span>
							  </div>
							  <div class="panel-body">
							  	 <ul>
									<li>
                                                                            <?php foreach($certificates as $certificate){ ?>
                                                                            <a href="<?php echo $this->webroot; ?>certificate/<?php echo $certificate['QuizScore']['file_name']; ?>" target="_blank" class="certificate-circle" alt="<?php echo $certificate['QuizScore']['file_name']; ?>"><i class="fa fa-file-text"></i></a>
                                                                            <?php } ?>
							    	</li>
							    </ul>
							  </div>
							</div>
<!--							<div class="panel panel-default">
							  <div class="panel-heading">
							  		<h3>Forum Activity</h3>
							  		<span>Latest Forum</span>
							  </div>
							  <div class="panel-body">
							  		<ul>
									  	<li>
									  		<div class="media"> 
										  		<div class="media-left"> 
											  		<a href="#"> 
											  			<img src="/team4/studilmu/user_images/59a8f3bc09936.jpg" alt="">
											  		</a> 
										  		</div> 
										  		<div class="media-body"> 
										  			<h4 class="media-heading">Media heading</h4>
										  			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p> 
										  		</div> 
										  		<div class="media-right"> 
										  			<span>1 hr ago</span>
										  		</div> 
									  		</div>
									  	</li>
									</ul>							  		
							  </div>
							</div>-->
						</div>
					</div>
             </div>
         </div>
      </div>
	</div>
</section>
<script>
	$(document).ready(function(){
		$('.open-close-menu > li > a.clicks').click(function(){
			$(this).find('.fa-plus').toggleClass('fa-minus');
			$(this).next('ul').slideToggle();
		});
	})
</script>

<script>


    $(document).on("change", "#form", function () {

        var file_data = $("#form").prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
        //form_data.append("user_id", 123)                 // Adding extra parameters to form_data
        $.ajax({
            url: "<?php echo $this->webroot . 'users/upload_image'; ?>",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data, // Setting the data attribute of ajax with file_data
            type: 'post', success: function (result) {
                $("#replaceimage").html('');
                $("#replaceimage").html('<img src="<?php echo $this->webroot; ?>user_images/' + result + '" />');
                $("#replaceimageTop").html('');
                $("#replaceimageTop").html('<img src="<?php echo $this->webroot; ?>user_images/' + result + '" />');
            }
        });
    })
    $(document).on("click", "#uploadbox", function () {
        $("#form").trigger("click");
    });


</script>
