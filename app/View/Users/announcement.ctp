<body id="udemy" class="announcement  udemy  pageloaded" cz-shortcut-listen="true" style="">
<nav class="announcement__header">
        <div class="container">

            <a class="btn btn-default" href="<?php echo $this->webroot;?>users/dashboard" style="margin-top: 18px;" >
                Back to Instructor Dashboard
            </a>
            
            <h1 class="header__title">Create Announcement</h1>
        </div>
    </nav>

    <div class="container ud-angular-loaded" data-module-id="course-manage-announcements" data-module-name="ng/apps/course-manage-announcements/app" data-ng-init="promotionalCourses=[];
    anyPromotionalCourseExcluded=false;
    educationalCourses=[];
    publishedCourses=[];
    anyEducationalCourseExcluded=false;
    isOrganization=false;
    ">
        <!-- ngView: undefined --><ng-view>
        <announcement-form promotional-courses="promotionalCourses" educational-courses="educationalCourses" published-courses="publishedCourses" source-course-id="courseId" announcement="announcement">
            <form name="announcementForm" data-ng-hide="announcement.showSuccessMessage" novalidate="" class="ng-pristine ng-valid-date ng-invalid ng-invalid-required ng-valid-maxlength"> 
                <fieldset ng-disabled="isSubmitting"> 
                    <div class="announcement__type-selector"> 
                            <span>Type</span> 
            				<select class="selectpicker selectPart">
							  <option>Educational</option>
							  <option>Promotional</option>
							</select>
 
                    </div> 
            </form>    
                
                        <!-- <form method="post" action="#" enctype="multipart/form-data" >

                            <input type="file" name="announcement_media" id="media"></input>
                            <textarea name="announcement_post" id="post" ></textarea>
                            <input type="submit" value="Submit" ></input>
                        </form>  -->

                    <form method="post" action=""  id="new_announcement" enctype="multipart/form-data" class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Select Your Course:</label>
                        <div class="col-sm-10">
                          <select name="data[Announcement][post_id]" id="post_name" required class="selectField selectfield1">
                            <?php  
                            foreach ($posts as $key => $post) { ?>
                                <option value="<?php echo $key; ?>" ><?php echo $post; ?></option>
                            <?php 
                            } ?>            
                          </select>
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Announcement Course:</label>
                        <div class="col-sm-10">
                          		<div class="input-group browseButton">
					                <label class="input-group-btn">
					                    <span class="btn btn-primary">
					                        Browse&hellip; <input type="file" style="display: none;" multiple>
					                    </span>
					                </label>
					                <input type="text" class="form-control" readonly>
					            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Announcement Post:</label>
                        <div class="col-sm-10"> 
                          <textarea class="form-control" id="post" name="data[Announcement][announcement]" placeholder="Enter Your Announcement Content"></textarea>
                          <input type="hidden" name="data[Announcement][user_id] " value="<?php echo $userid;?>" ></input>
                        </div>
                      </div>
                      <div class="form-group bulk_uploader"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" id="submit_button" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>          

                         

                     </div>
<!-- Response Modal -->
  <div class="modal fade" id="responseModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #08d6e8;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="headerResponse" ></h4>
        </div>
        <div class="modal-body">
          <p id="ajaxResponse" ></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<script>
$(document).ready(function (e) {
    $('#new_announcement').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type:'POST',
            url: '<?php echo $this->webroot;?>announcements/ajaxSaveAnnouncement',
            data:formData,
            dataType:'json',
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                if(data.Ack == 1){
                    $("#ajaxResponse").html('Your Announcement is saved successfully');
                    $("#headerResponse").html('Success..');
                    $('#responseModal').modal('show');
                    //window.location.href='<?php echo $this->webroot;?>users/dashboard';
                }
            }
        });
    }));
    $("#responseModal").on("hidden.bs.modal", function () {
      window.location.href='<?php echo $this->webroot;?>users/dashboard';
    });
});
</script>
<script>
	$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});
</script>












                     <script type="text/javascript" src="/staticx/udemy/js/webpack/ud-entry.993b3c397298da0df524.js" async=""></script>



                     <div style="display: none; visibility: hidden;">
                        <script>(function(){var a=window._fbq||(window._fbq=[]);if(!a.loaded){var b=document.createElement("script");b.async=!0;b.src="//connect.facebook.net/en_US/fbds.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c);a.loaded=!0}a.push(["addPixelId","1457291081167286"]);a.push(["addPixelId","1053613454760306"])})();window._fbq=window._fbq||[];window._fbq.push(["track","PixelInitialized",{}]);
                        </script>
                     <noscript></noscript></div><div style="display: none; visibility: hidden;"><script>if(0<jQuery('div[data-purpose\x3d"introduction-video"]').length){var Category=jQuery("a.cd-ca").text().trim(),SubCategory=jQuery("a.cd-ca").next().text().trim();ga("set","dimension9",Category);ga("set","dimension10",SubCategory);window._fbq=window._fbq||[];_fbq.push(["track",Category]);_fbq.push(["track",SubCategory])};</script></div><script type="text/javascript" id="" src="https://www.dwin1.com/6554.js"></script><div style="display: none; visibility: hidden;"><script>if(0<jQuery('div[data-purpose\x3d"introduction-video"]').length){var id=jQuery(".one-col-landing").attr("data-course-id"),price=jQuery(".current-price").text().replace("$","").trim();window._fbq=window._fbq||[];_fbq.push(["track","ViewContent",{content_ids:id,content_type:"product"}])};</script></div><div style="display: none; visibility: hidden;"><script>UD.GoogleAnalytics.setValue("dimension7","true");UD.GoogleAnalytics.trackEvent("ismember","ismember","ismember",void 0,void 0,{nonInteraction:1});window._fbq=window._fbq||[];_fbq.push(["track","isMember"]);</script></div>

                     <script type="text/javascript" id="">var _kiq=_kiq||[];(function(){setTimeout(function(){var a=document,b=a.getElementsByTagName("script")[0],a=a.createElement("script");a.type="text/javascript";a.async=!0;a.src="//s3.amazonaws.com/ki.js/34436/6GL.js";b.parentNode.insertBefore(a,b)},1)})();(function(){_kiq.push(["identify",google_tag_manager["GTM-7BF3X"].macro('gtm11')||""]);_kiq.push(["set",{user_id:google_tag_manager["GTM-7BF3X"].macro('gtm12')||"",visitor_id:google_tag_manager["GTM-7BF3X"].macro('gtm13')||""}])})();</script><iframe id="AWIN_CDT" src="about:blank" style="height: 0px !important; width: 0px !important; visibility: hidden !important; display: inherit !important; margin: 0px !important; border: 0px !important; padding: 0px !important;"></iframe>
                 </body>

                 