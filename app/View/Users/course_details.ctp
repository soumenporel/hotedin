<?php //pr($coursedetail); ?>
<link href="<?php echo $this->webroot.'video/';?>css/video-js.css" rel="stylesheet" type="text/css">
<!-- <link href="<?php echo $this->webroot.'video/';?>css/bootstrap.css" rel="stylesheet"> -->


<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
<script>videojs.options.flash.swf = "video-js.swf";</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="<?php echo $this->webroot.'video/';?>videoStyle.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
.inner_header{
    background:#0c2440;
}
.inner-scroll-area {
    margin-top: -30px;
}
.big-title, .big-titles {
    display: block;
    position: relative;
    margin-bottom: 10px;
    margin-top:25px;
}
.big-titles {
    margin-bottom: 0px;
    margin-top:35px;
}
.related-title, .related-titles {
    background:url(http://35.154.208.200/learnfly/img/dots.png) 0 6px repeat-x;
    font-size: 17px;
    padding: 0;
    text-transform: uppercase;
    margin: 0;
    line-height: 1;
    font-weight: bold;
}
.related-titles {
    font-size: 28px;
    line-height: 30px;
    background:url(http://35.154.208.200/learnfly/img/dots.gif) 0 6px repeat-x;
}
.related-title span {
    padding-right: 10px;
    background: #fff;
}
.big-titles span {
    padding-right: 10px;
    background: #fff;
}
#instructor .media-body{padding-left:15px;}

.course-header{padding:40px 0; }
.course-header .vjs-default-skin .vjs-big-play-button{border-radius:6px !important; height: 55px !important; width: 55px !important; }
.course-header .vjs-default-skin .vjs-big-play-button::before{line-height: 1.5em;}
</style>
<section>

    <div class="inner_content" style="padding-bottom:0px; padding-top:80px;">

        <div class="container">

            <div class="row course-header">

                <div class="col-sm-3">

                    <div>
                        <?php if (empty($coursedetail['PostVideo'])) { 
                                if(isset($coursedetail['PostImage'][0]['originalpath']) && !empty($coursedetail['PostImage'][0]['originalpath'])){
                                    $srcIMG = $this->webroot.'img/post_img/'.$coursedetail['PostImage'][0]['originalpath'];
                            ?>
                                <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $coursedetail['PostImage'][0]['originalpath']; ?>">
                            <?php  }
                            else{ 
                                    $srcIMG = $this->webroot.'img/placeholder.png';
                                ?>
                                <img src="<?php echo $this->webroot; ?>img/placeholder.png">
                           <?php }
                        } else {
                            $file = $coursedetail['PostVideo'][0]['originalpath'];
                            $ext = pathinfo($file, PATHINFO_EXTENSION);
                            $videoExt = array('mp4', 'flv', 'mkv');
                            if (in_array(strtolower($ext), $videoExt)) {
                                $file_name = trim($coursedetail['PostVideo'][0]['originalpath'], $ext);
                                ?>

                                
                                <div id="instructions">
                                    <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-button" controls preload="auto" width="100%"
                                          poster="<?php echo $srcIMG; ?>"
                                          data-setup='{ "playbackRates": [0.5, 1, 1.5, 2] }' >
                                        <source  src="<?php echo $this->webroot; ?>img/post_video/<?php echo $coursedetail['PostVideo'][0]['originalpath']; ?>" type="video/<?php echo $ext; ?>">
                                        <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>webm" type='video/webm'>
                                        <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>ogv" type='video/ogg' />
                                        <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track>
                                        <!-- Tracks need an ending tag thanks to IE9 -->
                                        <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track>
                                        <!-- Tracks need an ending tag thanks to IE9 -->
                                        
                                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                                          
                                    </video>
                                </div> 


                            <?php
                            }
                        }
                        ?>

                    </div>

                </div>

                <div class="col-sm-7">

                        <?php //pr($coursedetail); ?>
                            <h2 class="text-uppercase"><?php echo $coursedetail['Post']['post_title']; ?></h2>
                            <hr style="margin-top: 0px;margin-bottom: 10px;">
                            <p><?php echo $coursedetail['Post']['short_summary']; ?> </p>
                            <p>
                                <!--<img src="<?php echo $this->webroot; ?>img/star_image.jpg">-->
                                <!-- <span>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </span> -->
                                <?php 
                                    if(isset($coursedetail['Rating']) && $coursedetail['Rating']!=''){
                                        $b = count($coursedetail['Rating']);
                                    
                                        $a=0;
                                        foreach ($coursedetail['Rating'] as $value) {
                                            $a=$a + $value['ratting'];
                                        }
                                    }
                                    $finalrating='';
                                    if($b!=0){
                                        $finalrating = ($a / $b);
                                    }

                                     if(isset($finalrating) && $finalrating!='') {  
                                        for($x=1;$x<=$finalrating;$x++) { ?>
                                            <i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>
                                        <?php }
                                        if (strpos($finalrating,'.')) {  ?>
                                            <i class="fa fa-star-half-o" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>
                                        <?php  $x++;
                                        }
                                        while ($x<=5) { ?>
                                            <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>
                                        <?php $x++;
                                        } } ?><?php echo $finalrating; ?> (<?php echo $b; ?> ratings) <span class="students"><i class="fa fa-circle"></i>
                                    <?php echo $totalEnrolled;?> students enrolled</span> 
                                <?php if ($coursedetail['Post']['is_bestselling'] == '1') { ?>
                                    <span class="bestseller-badge__type"> Bestselling </span>
                                <?php } ?>
                            </p>
                            

                </div>

                <div class="col-sm-2">

                    <p><strong style="font-size:18px; display: block">Instructed by</strong> <br> <a href="#"style="color: #027b86;font-weight: 400;"><?php echo $coursedetail['User']['first_name'].' '.$coursedetail['User']['last_name']; ?></a>/ <!-- <a href="#">Affiliate Marketing</a> --> </p>

                </div>

            </div>

        </div>

    </div>


</section>