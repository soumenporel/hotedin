<style>
/*  #alertDayMessage2 small, #alertDayMessage1 small, #alertDayMessage small { color : #a94442;}
  .social_buttons button {
    border: 0 none;
    border-radius: 2px;
    color: #fff;
    font-size: 14px;
    height: 40px;
    letter-spacing: 1px;
    margin: 5px 0;
    text-transform: uppercase;
    width: 100%;
}
.social_buttons .ln_btn
{
    background: #0077B5;
}
.IN-widget, .IN-widget span , span[id^='li_ui_li_gen_'], a[id^='li_ui_li_gen_']{
    width:100%;
}*/
.li_ui_li_gen_1503638785983_0-title-text{
  display: none;
}
</style>

    <!--  inner  slider   -->


    <section class="home-slider inner-banner">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
                    <?php $bi = 1; foreach ($banners as $key => $banner) { ?>
          <div class="carousel-item <?php echo ($bi==1) ? 'active' : '';?>" >        
        
                   <img class="" src="<?php echo $this->webroot; ?>banner/<?php echo $banner['Pagebanner']['image']; ?>" alt="#">
                  </div>
                <?php $bi++; } ?>
            </div>
            <?php echo $bannerContent['CmsPage']['page_description']; ?>
          </div>
        </div>
    </section>

 <section class="py-5 pricing" id="pricing_part">
      <div class="container">
        <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">Membership<span class="font-weight-bold" style="color:#f00; "> Package</span>
          <em class="dvdr">
            <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
          </em>
        </h1>
          <!--<div class="row">
              <div class="col-lg-4">
                  <h1 class="zilla">Pricing</h1>
              </div>
          </div>-->
          <?php //echo '<pre>'; print_r($userdetails['User']); echo '</pre>'; ?>
          <table class="table table-bordered table-responsive d-block d-lg-table">
              <thead>
                  <th><h1 class="zilla font-weight-light">Membership Package</h1></th>
                  <th class="bg-danger text-white text-center"><h1 class="zilla font-weight-light text-uppercase">Free</h1></th>
                  <th class="text-center p-0 price-th">
                      <h4 class="zilla font-weight-light m-0 p-3 bg-light-sky text-white">Basic Membership</h4>
                      <table  class="w-100">
                          <tr>
                              <td class="border-top-0 border-bottom-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['0']['MembershipPlan']['duration'].' '.$mPlans['0']['MembershipPlan']['duration_in']; ?></p></td>
                              <td class="border-top-0 border-bottom-0 border-right-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['1']['MembershipPlan']['duration'].' '.$mPlans['1']['MembershipPlan']['duration_in']; ?></p></td>
                          </tr>
                      </table>
                  </th>
                  <th class="text-center p-0 price-th">
                      <h4 class="zilla font-weight-light m-0 p-3 bg-light-blue text-white">Premium Membership</h4>
                      <table  class="w-100">
                          <tr>
                              <td class="border-top-0 border-bottom-0 border-left-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['2']['MembershipPlan']['duration'].' '.$mPlans['2']['MembershipPlan']['duration_in']; ?></p></td>
                              <td class="border-top-0 border-bottom-0 w-50"><p class="font-weight-normal"><?php echo $mPlans['3']['MembershipPlan']['duration'].' '.$mPlans['3']['MembershipPlan']['duration_in']; ?></p></td>
                          </tr>
                      </table>
                  </th>
              </thead>
              <tbody>
                  <?php foreach ($planItems as $key => $item) { ?>
                  <tr>
                      <td><h5><?php echo $item['MembershipItem']['name']; ?></h5></td>
                      <?php
                        $planList = $this->requestAction('/homepages/itemList/' . $item['MembershipItem']['id']);
                      ?>
                      <td class="text-center"><h4><i class="text-danger <?php echo (in_array($mPlans['4']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['0']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['1']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['2']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?> "></i></h4></td>
                                  <td class="text-center border-0 w-50"><h4><i class="text-danger <?php echo (in_array($mPlans['3']['MembershipPlan']['id'], $planList))? 'ion-android-checkmark-circle text-success':'ion-ios-close-outline'; ?>"></i></h4></td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  <?php } ?>

                  <?php // echo '<pre>'; print_r($userDetails['MembershipPlan']['price']); echo '</pre>';?>
                  <tr class="gray-bg">
                      <td><h4>Membership Fee</h4></td>
                      <td class="text-center"><h4 class="text-success"> Free </h4></td>
                      <td class="p-0">
                          <table class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4 class="text-success">Rp <?php echo number_format($mPlans['0']['MembershipPlan']['price'],0,"",".");?></h4></td>
                                  <td class="text-center border-0 w-50"><h4 class="text-success">Rp <?php echo number_format($mPlans['1']['MembershipPlan']['price'],0,"",".");?></h4></td>
                              </tr>
                          </table>
                      </td>
                      <td class="p-0">
                          <table  class="w-100">
                              <tr>
                                  <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50"><h4 class="text-success">Rp <?php echo number_format($mPlans['2']['MembershipPlan']['price'],0,"",".");?></h4></td>
                                  <td class="text-center border-0 w-50"><h4 class="text-success">Rp <?php  $p3 = number_format($mPlans['3']['MembershipPlan']['price'],0,"","."); echo $p3; ?></h4></td>
                              </tr>
                          </table>
                      </td>
                  </tr>

                  <?php //pr($userDetails); ?>
                  <tr class="<?php if($userid && $userdetails['User']['admin_type']==2){ echo ''; }else{ echo 'showRow'; } ?>" >
                    <td></td>
                    <td class="text-center"><h4 class="text-success">  <a href="Javascript: void(0);" onclick="addplan('7');"class="btn btn-danger">Select</a> </h4></td>
                    <td class="p-0">
                      <table class="w-100">
                        <tr>
                          <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                            <h4 class="text-success">

                                <a href="Javascript: void(0);" onclick="addplan(<?php echo $mPlans['0']['MembershipPlan']['id'];?>);" class="btn bg-light-sky text-white">Select</a>

                            </h4>
                          </td>
                          <td class="text-center border-0 w-50">
                            <h4 class="text-success">

                                <a href="Javascript: void(0);" onclick="addplan(<?php echo $mPlans['1']['MembershipPlan']['id'];?>);" class="btn bg-light-sky text-white">Select</a>

                            </h4>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td class="p-0">
                      <table  class="w-100">
                        <tr>
                          <td class="text-center border-top-0 border-bottom-0 border-left-0 w-50">
                            <h4 class="text-success">

                                <a href="Javascript: void(0);" onclick="addplan(<?php echo $mPlans['2']['MembershipPlan']['id'];?>);" class="btn btn-primary">Select</a>

                            </h4>
                          </td>
                          <td class="text-center border-0 w-50">
                            <h4 class="text-success">

                                <a href="Javascript: void(0);" onclick="addplan(<?php echo $mPlans['3']['MembershipPlan']['id'];?>);" class="btn btn-primary">Select</a>

                            </h4>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>

              </tbody>
          </table>
      </div>
    </section>


    <!--    banner bottom area  -->
    <?php echo $bannerMenu['CmsPage']['page_description']; ?>
    <!--   start teaching   -->



    <section class="py-5" style="display:none;" id="signup_part">
      <div class="container">
          <!-- <h1 class="zilla text-blue font-weight-light text-center">Ready to Start Teaching? Let's Start Now. It's <span class="text-danger">Free</span></h1> -->
          <h1 class="zilla text-blue font-weight-light text-center">Become A Student and Start Learning. 
              <span id='free_txt' style="display:none;">It's <span class="text-danger">Free</span></span></h1>
          <p id="ajaxFlashMessage1" style="color:red;"></p>
          <div class="row mt-5">
              <div class="col-lg-7">
                  <img src="<?php echo $this->webroot; ?>img/blue-icon-for-student.png" alt="" class="img-fluid">
                 <!--  <h1 class="zilla text-danger font-weight-light my-3">Teach on Studilmu share your passion and inspire others.</h1> -->
                  <h1 class="zilla text-danger font-weight-light my-3">Learn from Studilmu and Enhance Your Skills.</h1>
                  
                  <h2>For bank payment please pay to this account</h2>
                  <p>Bank :<?php echo $settingbank['Setting']['bank_name']; ?></p>
                  <p>Branch :<?php echo $settingbank['Setting']['branch']; ?></p>
                  <p>Account No :<?php echo $settingbank['Setting']['acc_no']; ?></p>
                  <p>Account holders name :<?php echo $settingbank['Setting']['holders_name']; ?></p>
                  
              </div>

              <div class="col-lg-5">
                  <form class="row" action="" method="post" id="UserSignupForm" >
                      <input type="hidden" id="membership_plan_id" name="data[User][membership_plan_id]" class="form-control" value="">
                      <div class="form-group col-md-6">
                          <label class="font-14">First Name</label>
                          <input type="text" id="firstname" name="data[User][first_name]" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                          <label class="font-14">Last Name</label>
                          <input type="text" id="lastname" name="data[User][last_name]" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                          <label class="font-14">Email</label>
                          <input type="email" id="email" name="data[User][email_address]" class="form-control">
                      </div>
                      <div class="form-group col-md-6">
                          <label class="font-14">Password</label>
                          <input type="password" id="password" name="data[User][user_pass]" class="form-control">
                          <input type="hidden" id="fbid" name="data[User][facebook_id]" class="form-control">
                          <input type="hidden" id="gpid" name="data[User][googleplus_id]" class="form-control">
                          <input type="hidden" id="lnid" name="data[User][linkedin_id]" class="form-control">
                      </div>
                      <input type="hidden" id="payment_type" value="" name="data[User][payment_type]"/>
                      <div class="col-md-12">
                      	<div class="row" id="paymentOptions">
                            <button type="button" class="btn btn-success btn-sm mr-3" id="signupSubmit" style="display: none;">Sign up</button>
                        <button type="button" class="btn btn-success btn-sm mr-3" id="cardSubmit">Credit Card Payment</button>
                        <button class="btn btn-primary btn-sm" type="button" id="bankSubmit">Bank Account</button>
                      	</div>
                      </div>
                      <div class="form-group col-md-12">
                          <!-- <button type="submit" class="btn btn-danger btn-block mt-3">join Us</button> -->
                      </div>
                      <div class="become_a_student">
	                        <a type="submit" id = "fbsignupTeacher" class="btn btn-gp btn-lg text-uppercase join-us-grp-btn">
	                          <i class="fa fa-facebook"> </i>
	                        </a>
	                      	<script type="in/Login" ></script>
	                        <a type="submit" onclick="google_signup()" class="btn btn-fb btn-lg text-uppercase join-us-grp-btn">
	                          <i class="fa fa-google-plus"></i>
	                        </a>
                      </div>

                  </form>
              </div>
          </div>
      </div>
    </section>

     <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"> Bank Payment Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form action="" method="post">

          <div class="form-group">
            <label for="exampleSelect1">Select Bank</label>
            <select class="form-control" name="data[Order][bank_id]">
              <?php foreach($bank_ids as $bank) { ?>
                <option value="<?php echo $bank['Bank']['id']; ?>"><?php echo $bank['Bank']['bank_name']; ?></option>
              <?php } ?>
            </select>
          </div>

           <div class="form-group">
            <label>Upload Payment Receipt</label>
            <input type="file" class="form-control-file form-control" name="data[Order][document]" required="required">
          </div>

          <div class="text-center">
              <button class="btn btn-primary" type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>
    <!--  testimonials  -->

    <section class="testimonials py-5 bg-faded text-center">
      <div class="container">
          <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative"><span class="font-weight-bold" style="color:#f00; "> Testimonials</span>
            <em class="dvdr">
              <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
            </em>
          </h1>
          <div id="carouseltestimonials" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
                <?php $aCount = 1; foreach ($testimonialdata as $key => $testimonial) { ?>
                  <div class="carousel-item <?php if($aCount==1){ echo 'active'; } ?>">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-test">
                            <h5 class="font-weight-light"><?php echo $testimonial['Testimonial']['description']; ?></h5>
                            <!-- <div class="test-pic my-3 mx-auto">
                                <img src="<?php echo $this->webroot;?>user_images/<?php echo $testimonial['User']['user_image']?>" alt="">
                            </div> -->
                            <h5 class="font-weight-bold"><?php echo $testimonial['User']['first_name'].' '.$testimonial['User']['last_name']; ?></h5>
                        </div>
                    </div>
                  </div>
                <?php $aCount++; } ?>
                <!-- <div class="carousel-item">
                  <div class="row justify-content-center">
                      <div class="col-lg-8 col-test">
                          <h5 class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h5>
                          <div class="test-pic my-3 mx-auto">
                              <img src="img/test-1.png" alt="">
                          </div>
                          <h5 class="font-weight-bold">James Ranson</h5>
                      </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="row justify-content-center">
                      <div class="col-lg-8 col-test">
                          <h5 class="font-weight-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h5>
                          <div class="test-pic my-3 mx-auto">
                              <img src="img/test-1.png" alt="">
                          </div>
                          <h5 class="font-weight-bold">James Ranson</h5>
                      </div>
                  </div>
                </div> -->
              </div>
              <ol class="carousel-indicators mt-3">
                <?php $ti = 0; foreach ($testimonialdata as $key => $testimonial) { ?>
                  <li data-target="#carouseltestimonials" data-slide-to="<?php echo $ti; ?>" class="<?= ($ti==0) ? 'active': ''; ?>"></li>
                <?php $ti++; } ?>
                <!-- <li data-target="#carouseltestimonials" data-slide-to="0" class="active"></li>
                <li data-target="#carouseltestimonials" data-slide-to="1"></li>
                <li data-target="#carouseltestimonials" data-slide-to="2"></li> -->
              </ol>
            </div>
      </div>
    </section>


    <!--   faq   -->


    <section class="py-5 faq">
        <div class="container">
            <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">
              Frequently Asked Question<span class="font-weight-bold" style="color:#f00; "> (FAQ) </span>
              <em class="dvdr">
                <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
              </em>
            </h1>
            <div id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($faqs as $key => $faq) { ?>
                <div class="card mb-3">
                    <div class="card-header border-bottom-0" role="tab" id="headingOne">
                        <h4 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq['Faq']['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $faq['Faq']['id']; ?>" class="d-block text-blue">
                              <span class="font-14"><?php echo $faq['Faq']['title']; ?></span> <i class="ion-ios-plus-outline float-right"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $faq['Faq']['id']; ?>" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
		                  <div class="card-block pt-0">
		                    <p class=" font-14 text-gray"><?php echo $faq['Faq']['description']; ?></p>
		                  </div>
		                </div>
                </div>

                <?php } ?>
            </div>
        </div>
    </section>


<!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paymentformPaypal" name="paymentform">
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <input type="hidden" name="rm" value="2">
    <input type="hidden" name="quantity" value="1">
    <input type="hidden" name="business" value="nits.arpita@gmail.com">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="notify_url" value="<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'notify_url'), TRUE); ?>">
    <input type="hidden" name="return" value="<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'thankyou_paypal'), TRUE); ?>" />
    <input type="hidden" name="cancel_return" value="<?php echo Router::url(array('controller' => 'checkouts', 'action' => 'index', 'payment_cancelled'), TRUE); ?>" />
    <input type="hidden" name="item_name" value="">
    <input type="hidden" name="amount" id = "paypalAmount" value="">
    <input type="text" name="custom" id="paypalCustomField" value="">
    <input type="hidden" name="a3" id = "paypalAmount3" value="">
    <input type="hidden" name="p3" value="1">
    <input type="hidden" name="t3" value="M">
    <input type="hidden" name="src" value="1">
    <input type="hidden" name="sra" value="1">
</form> -->

   <form action="<?php echo $this->webroot; ?>checkouts/paymentTestNew" method="POST" style="display: none;" id="paymentformCard">
    <input type="hidden" name="amount"  id = "cardAmount" value=""/>
    <input type="submit" value="Card Payment" class="btn btn-success">
  </form>

<script>
$(document).ready(function () {
    $("#UserSignupForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[User][first_name]': {
                validators: {
                    notEmpty: {
                        message: 'The First Name is required and cannot be empty.'
                    }
                }
            },
            'data[User][last_name]': {
                validators: {
                    notEmpty: {
                        message: 'The Last Name is required and cannot be empty.'
                    }
                }
            },
            'data[User][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty.'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address. '
                    }
                }

            },
            'data[User][user_pass]': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty.'
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        var $form = $(e.target),
        fv = $form.data('formValidation');
        var field = $('#UserSignupForm').serialize();
        var payment_type=$('#payment_type').val();
        //alert(payment_type);

        $.ajax({
            url: '<?php echo $this->webroot . 'users/ajaxSignupStudent' ?>',
            type: 'post',
            dataType: 'json',
            data: field,
            success: function(data) {
                //alert(data.Ack);
                if(data.Ack==1){
                    if(payment_type=='Paypal')
                    {
                    var user_id=0;
                  customValue = user_id + '|' + data.planid + '|' + data.amount + '|'+ data.firstname + '|' + data.lastname + '|' + data.email + '|' + data.password;
                //alert(customValue);  return false;
                $("#paypalCustomField").val(customValue);
                  $("#paymentformPaypal").submit();
                    }
                    else if(payment_type=='Card')
                    {
                //alert(4);            
                $("#paymentformCard").submit();


                    }
                    else if(payment_type=='Bank')
                    {
                //alert(3);       
                $('#myModal').modal('show');
                    }
                    else
                    {
                $("#ajaxFlashMessage1").removeClass('error');
                $("#ajaxFlashMessage1").addClass('success');
                $("#ajaxFlashMessage1").html(data.res);
                  $("#ajaxFlashMessage1").css('display','block'); 
                    setTimeout(function(){  
                      $("#ajaxFlashMessage1").hide();
                      window.location.href = '<?php echo $this->webroot ; ?>'; 
                  }, 3000);
                    }
                }else{
                  $("#ajaxFlashMessage1").html(data.msg);
                  $("#ajaxFlashMessage1").css('display','block');
                 
                }
            }
        });

        //$("#paymentformPaypal").submit();

    });

    //Facebook login
    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '364420670573883',
            status: true,
            xfbml: true,
            version: 'v2.8'
        });

        $('#fbsignupTeacher').click(function (e) {
            e.preventDefault();
            FB.login(function (response) {
                console.log(response);
                if (!response || response.status !== 'connected') {
                    alert('Failed');
                } else {

                    FB.api('/me', {fields: 'id,first_name,last_name,email'}, function (response) {
                        console.log(response);
                        var fb_user_id = response.id;
                        var fb_first_name = response.first_name;
                        var fb_last_name = response.last_name;
                        var fb_email = response.email;
                        console.log(JSON.stringify(response));
                        //alert(fb_first_name);
                        $('#firstname').val(fb_first_name);
                        $('#lastname').val(fb_last_name);
                        $('#email').val(fb_email);
                        $('#fbid').val(fb_user_id);

                    });
                }
            }, {scope: 'public_profile,email'});
        });

    });
});

    function google_signup() {
        // console.log('hii');
        var myParams = {
            'clientid': '781797887038-sk9f5h5rujn8r0on8v1den33ifkanc9n.apps.googleusercontent.com',
            //You need to set client id
            'cookiepolicy': 'single_host_origin',
            'callback': 'googleSignupCallback', //callback function
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
        };
        gapi.auth.signIn(myParams);
    }

    function googleSignupCallback(result) {
        if (result['status']['signed_in'])
        {

            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });

            request.execute(function (resp) {
                console.log(resp);
                var email = resp.emails[0].value;
                var gpId = resp.id;
                var fname = resp.name.givenName;
                var lname = resp.name.familyName;

                $('#firstname').val(fname);
                $('#lastname').val(lname);
                $('#email').val(email);
                $('#gpid').val(gpId);

            });
        }
    }

    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        $('a[id*=li_ui_li_gen_]').html('<button class="ln_btn btn btn-info"><i class="fa fa-linkedin"></i> <?php echo 'Linkedin'; ?></button>');
        IN.Event.on(IN, "auth", getProfileData);
    }
     // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
        var ln_id           = data.values[0].id;
        var ln_email        = data.values[0].emailAddress;
        var ln_firstName    = data.values[0].firstName
        var ln_lastName     = data.values[0].lastName;

        $('#firstname').val(ln_firstName);
        $('#lastname').val(ln_lastName);
        $('#email').val(ln_email);
        $('#lnid').val(ln_id);

        // $.ajax({
        //     url: '<?php echo $this->webroot . 'users/linkedinLoginRegister' ?>',
        //     type: 'post',
        //     dataType: 'json',
        //     data: {
        //         ln_id: ln_id,
        //         ln_email: ln_email,
        //         ln_firstName: ln_firstName,
        //         ln_lastName: ln_lastName
        //     },
        //     success: function(data) {
        //         if(data.ack == 1) {
        //             if(data.url != '') {
        //                 window.location.href = data.url;
        //             } else {
        //                  onLinkedInLoad();
        //             }
        //         } else {

        //             onLinkedInLoad();
        //         }
        //     }
        // });

    }

    // Handle an error response from the API call
    function onError(error) {
        //console.log(error);
        $('#errMsg').html('');
        $('#errMsg').html('<div class="col-md-12"><div class="alert alert-danger"><strong>Error!</strong>'+error+'</div></div>');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        //IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me").fields("id","first-name", "last-name", "email-address").result(onSuccess).error(onError);

    }

function addplan(plan_id){

    $.ajax({
        url: '<?php echo $this->webroot . 'membership_plans/ajaxMembershipPlanDetails' ?>',
        type: 'post',
        dataType: 'json',
        data: {
            plan_id: plan_id
        },
        success: function(data) {
          if(data.Ack == 1){
            console.log(data.detail.id);
            var customField = data.detail.id ;
            $("#paypalCustomField").val(customField);
            $("#paypalAmount").val(data.detail.price);
            $("#paypalAmount3").val(data.detail.price);
            $('#cardAmount').val(data.detail.price);

            $('#membership_plan_id').val(plan_id);
            $('#pricing_part').hide();
            $('#signup_part').show();
            if(plan_id==7)
            {
             $('#signupSubmit').show();
             $('#cardSubmit').hide();
             $('#bankSubmit').hide();
             //$('#paymentOptions').html('<div class="col-md-4 col-sm-4 col-12"><div class="radio"><label><button type="button" class="btn btn-success btn-sm" id="signupSubmit">Sign up</button><label></div></div>');
             $('#free_txt').show();
            }

          }
        }
    });

    // $('#membership_plan_id').val(plan_id);
    // $('#pricing_part').hide();
    // $('#signup_part').show();
}

$("#paypalSubmit").click(function(){
  $('#payment_type').val('Paypal');
  //var customValue = $("#paypalCustomField").val();
  //customValue = customValue + '|' + firstname + '|' + lastname + '|' + email + '|' + password;
  //$("#paypalCustomField").val(customValue);
  $("#UserSignupForm").submit();

});

//for midtrans payment...................................................
$("#cardSubmit").click(function(){
  
  $('#payment_type').val('Card');

  $("#UserSignupForm").submit();

});

//for bank account payment........................................................

$("#bankSubmit").click(function(){
  $('#payment_type').val('Bank');

  $("#UserSignupForm").submit();

});

$("#signupSubmit").click(function(){
    //alert('1');
  $('#payment_type').val('Free');
  $("#UserSignupForm").submit();

});


history.pushState(null, null, '<?php echo $_SERVER["REQUEST_URI"]; ?>');
window.addEventListener('popstate', function(event) {
    window.location.assign("<?php echo $this->webroot;?>users/signupstudent");
});
</script>
