<style>
.header{
	position:relative;
}

.inner_header{
	background:#0c2440;
}


</style>
<section class="profileedit">
	<div class="container">
		<div class="row pt-4">
			<div class="col-md-3 col-sm-3">
				<?php echo $this->element('leftpanel'); ?>
			</div>
			<div class="col-md-9 col-sm-9" style="padding:0">
				<div class="profile_second_part photo pt-0">
					<p class="pull-right course_setting"><span><i class="fa fa-cog" aria-hidden="true"></i>
 <a href="#">Course Settings</a></span>
 						<span><i class="fa fa-question-circle" aria-hidden="true"></i>
<a href="#">help</a></span></p>
					<h2>Photo</h2>

					<p>Add a nice photo of yourself for your profile.</p>
					<form action="<?php echo $this->webroot;?>users/user_image" class="form-horizontal form_parttestvideo" id="PostAddCourseForm" enctype="multipart/form-data" method="post" accept-charset="utf-8">

						  <div class="form-group">
						    <label class="control-label col-sm-3" for="text">Image Preview:</label>
						    <div class="col-sm-9">
						      	<div class="previmg" id="image_preview">
						      	<?php if(isset($userdetails['User']['user_image']) && !empty($userdetails['User']['user_image'])){?>
						      		<img src="<?php echo $this->webroot;?>user_images/<?php echo $userdetails['User']['user_image'];?>" style="" alt="" class="img-responsive">
						      	<?php }else{ ?>
						      		<img src="<?php echo $this->webroot;?>img/imgman.jpg" alt="" class="img-responsive">
						        <?php  } ?>
						      		<p style="padding-bottom: 20px; overflow: hidden;">Your beautiful, clean, non-pixelated image should be at minimum 200x200 pixels.</p>
						      	</div>
						    </div>
						  </div>
					 	<div class="form-group">&nbsp;</div>
					 	<div class="form-group">
						    <label class="control-label col-sm-3" for="text">Add / Change Image:</label>
						    <div class="col-sm-9">
						      <div class="custom-file-upload">
									    <!--<label for="file">File: </label>-->

									    <input type="hidden" id="image_name" name="data[User][image_name]" value="<?php echo $userdetails['User']['user_image'];?>" />
									    <input type="file" id="image_file" name="data[User][image]" />
									</div>
						    </div>
						  </div>
					 	<div class="bulk_uploader">
					 		<div class="col-sm-9 col-sm-offset-3">
					 			<button type="submit" class="btn btn-default">Save</button>
					 		</div>
					 	</div>
					 </form>
				</div>



				</div>
		</div>
	</div>
</section>
<script>
$(function () {

    $("#image_file").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#image_preview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        //img.attr("style", "height:325px;width:622px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});
</script>
