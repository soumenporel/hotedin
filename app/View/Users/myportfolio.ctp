
<?php //print_r($user_portfolio); ?>
<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}
</style>
<section class="profileedit">
    <div class="container">
        <div class="row pt-4">
            <div class="col-md-3 col-sm-3">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel');?>
            </div>
            <div class="col-md-9 col-sm-9">
                <div id="picture_msg1" style="display:none;" class="alert"></div>
                <div class="profile_second_part">
                    <h2>Portfolio</h2>
                    <p>Add information about yourself to share on your profile.</p>
                    <div class="row">
                    	<?php
                        foreach ($user_portfolio as $portfolio)
                        {
                            //print_r($portfolio);die;
                            if(count($portfolio['PortfolioImg'])>0)
                            {
                                $bg_img=$this->webroot."portfolio/".$portfolio['PortfolioImg'][0]['name'];
                            }
                            else {
                                $bg_img=$this->webroot."img/storytelling201.jpg";
                            }
                        ?>


                      	<div class="col-md-6 col-sm-6" id='<?php echo $portfolio['Portfolio']['id'];?>'>
                      		<div class="portfolio" style="background: url('<?php echo $bg_img; ?>')">
                      			<div class="inner-box"><a href="javascript:void(0);" onclick="removeFile(<?php echo $portfolio['Portfolio']['id'];?>);" class="delete"><i class="fa fa-trash-o"></i></a>
                      				<div class="center-text">
	                      				<a href="#" onclick="openModal(<?php echo $portfolio['Portfolio']['id']; ?>);" class="view"><h2><?php echo $portfolio['Portfolio']['name']; ?></h2></a>
                      				</div>
                      				<a href="<?php echo $this->webroot.'users/editportfolio/'.$portfolio['Portfolio']['id'];?>" class="edit"><i class="fa fa-edit"></i></a>
                      			</div>
                      		</div>
                      	</div>
                      <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="myModal" class="modal modal-light-box">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    test
  </div>
</div>

<script>
function openModal(id) {
    //alert(id);
    //$('#caption').html(id);
     $.ajax({
            type: "POST",             // Type of request to be send, called as method
            dataType: 'json',
            url: "<?php echo $this->webroot;?>users/portfolioPopup",
            data: {
                id:id
            },
            beforeSend: function () {
            },
            success: function (data) {
                //alert(data.ACK);
               $('.modal-content').html(data.html);
               currentSlide(1);

            }
        });
   document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  //var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  //captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<script>
       function removeFile(id)
    {
        //alert(1);
        if (confirm("Are you sure to delete this portfolio?") == true) {
        $.ajax({
            url: "<?php echo $this->webroot;?>users/delportfolio", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            dataType: 'json',
            data: {
                id:id
            },   // To send DOMDocument or non processed data file it is set to false
            success: function(result)   // A function to be called if request succeeds
            {

                //console.log(result);
                //alert(result.ACK);

                if(result.ACK==1)
                {
                    $('#picture_msg1').addClass('alert-success');
                    $('#picture_msg1').removeClass('alert-danger');
                    $('#'+id).hide();
                }
                else
                {
                    $('#picture_msg1').addClass('alert-danger');
                    $('#picture_msg1').removeClass('alert-success');
                }
                $("#picture_msg1").html(result.html);
                $('#picture_msg1').show();
                setTimeout(function(){ $('#picture_msg1').hide(); }, 3000);
            }
            });
            }
    }
</script>
