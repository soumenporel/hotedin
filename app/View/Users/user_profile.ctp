<?php //echo '<pre>'; print_r($courseDetails[3]); echo '</pre>'; ?>
<?php //echo 1; print_r($userdata);die; ?>
<div class="user_profile">
	<div class="userprof_top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?php echo $userdata['User']['first_name'].' '.$userdata['User']['last_name'];?></h2>
					<p><?php echo $userdata['User']['email_address']; ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="userprof_bottom mb-2">
		<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="profile_image">
					<img src="<?php if (isset($userdata['User']['user_image']) && $userdata['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                echo $userdata['User']['user_image'];
            } else {
                echo $this->webroot;
                ?>img/profile_img.jpg<?php } ?>" class="img-responsive" style="height: 261px;">
				</div>
				<!-- <button type="button" class="btn btn-block btn-primary">Send Message</button> -->
                               <?php if($userid>0){ ?>
                                <div id="rateYo" style="margin: 0 auto;"></div>
                               <?php } else { ?>
        <div id="rateYo" style="margin: 0 auto;" data-rateyo-read-only="true"></div>
                               <?php } ?>
				<ul class="sociallist">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="text-content short-text">
					<!-- p> I am a social scientist (M.A.), leadership trainer, thinker, poet, motivational speaker, and professional certified coach (PCC), helping bright, enthusiastic people excel.</p>

<p>I have specialised in turning the insights of neuroscience into down-to-earth tools, and I help executives, managers and teams boost their change processes, relational power and results.</p>

<p>If you want to learn more about my work feel free to check out Play Your Brain or my webpage. My ambition is to reach young and old with powerful and fun brain-based tools and my next course on Udemy will be on "Brain-Smart Kids".</p>

<p>I believe in art and science united. I believe in making difficult things really easy. I believe in the power of the tiny tweaks: boosting strategies and results by changing approaches slightly and brightly. </p>
<p><strong>Testimonials</strong></p>
 <p>"As illuminating as her books." David Shutts, Director</p>

<p>"Anette Prehn is the Danish Stephen Covey." Michael Toft, Manager</p>

<p>"The most exemplary trainer I have worked with for 40 years." Susanne Bloch-Jespersen, Head of</p>
 "Anette Prehn is a powerful, challenging and world-class trainer and a sublime expert within the field of NeuroLeadership. She stretches my team of very experienced learning and development experts with ease, enthusiasm and elegance." Trine Brahm, Head of People Development

<p>“I wish I could buy a jar of Anette Prehn's amazing energy". Annie Lorentzen, Branch Manager</p>

<p>"In my life and leadership there is a "before" and an "after" Anette Prehn". Alan Larsen, Headmaster</p>

<p>"The most personally developing training I have ever done" Claus Hammer, CEO</p>

<p>"When is course no 2, 3 and 4 coming?" Rose Olsen, Principal </p> -->
			<?php echo $userdata['User']['biography'];?>
			</div>
<div class="show-more">
        <a href="#">Show more</a>
    </div>
    	<ul class="instructor_state">
    		<li>
    			<div class="small"> Students </div>
				<div class="a2"> 12<?php echo $totalStudent; ?> </div>
    		</li>
    		<li>
    			<div class="small"> Courses </div>
				<div class="a2"> 13<?php echo $totalCourses; ?> </div>
    		</li>
    		<li>
    			<div class="small"> Reviews </div>
				<div class="a2"> 06<?php echo $totalReview; ?> </div>
    		</li>
    	</ul>
		</div>
	</div>
	</div>
</div>
<!--<div class="course_taught">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h2 class="text-center heading">Courses taught by <?php echo $userdata['User']['first_name'].' '.$userdata['User']['last_name'];?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul>
				<?php foreach ($userdata['Post'] as $key => $courseDetail) {

						if(isset($courseDetail['PostImage']) && !empty($courseDetail['PostImage'])){
							$img_array = end($courseDetail['PostImage']);
							$img = $this->webroot.'img/post_img/'.$img_array['originalpath'];
						}else{
							$img = $this->webroot.'img/placeholder.png';
						}
				?>

					<li class="card" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?>posts/course_details/<?php echo $courseDetail['Post']['slug']; ?>'">
						<div class="card_image">
								<img class="img-responsive img" src="<?php echo $img;?>" style="height: 200px;">
							</div>
							<div class="card_details">
								<strong><?php echo $courseDetail['Post']['post_title']; ?>
            					</strong>
            					<div class="detailText">
                					<?php echo $courseDetail['Post']['post_description']; ?>
                				</div>
            					<div class="details_bottom">
            						<div class="details_rating">
            							<span>
            								 <i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<?php
                                            if(isset($courseDetail['Rating']) && $courseDetail['Rating']!=''){
                                                $b = count($courseDetail['Rating']);

                                                $a=0;
                                                foreach ($courseDetail['Rating'] as $value) {
                                                    $a=$a + $value['ratting'];
                                                }
                                            }
                                            $finalrating='';
                                            if($b!=0){
                                                $finalrating = ($a / $b);
                                            }

                                             if(isset($finalrating) && $finalrating!='') {
                                                for($x=1;$x<=$finalrating;$x++) { ?>
                                                    <i class="fa fa-star" aria-hidden="true" style="color: green; font-size:1.0em;" ></i>
                                                <?php }
                                                if (strpos($finalrating,'.')) {  ?>
                                                    <i class="fa fa-star-half-o" aria-hidden="true" style="color: green; font-size:1.0em;" ></i>
                                                <?php  $x++;
                                                }
                                                while ($x<=5) { ?>
                                                    <i class="fa fa-star-o" aria-hidden="true" style="color: green; font-size:1.0em;" ></i>
                                                <?php $x++;
                                                } } ?>
            							</span>
            							<span class="digits"> (<?php echo $b; ?>)</span>
                                                        </div>
            							<div class="details_price"><div>
            							<span class="card_price"><?php echo $courseDetail['Post']['currency_type']; echo $courseDetail['Post']['price']; ?></span>
            						</div>
            					</div>
            				</div>
            				</div>
            				<div class="wishlist">
            					 <i class="fa fa-heart"></i>
            			</div>
					</li>
				<?php } ?>




					 <li class="card">
						<a href="#">
							<div class="card_image">
								<img class="img-responsive img" src="<?php echo $this->webroot;?>images/brain_changes.jpg">
							</div>
							<div class="card_details">
								<strong>Master Your Mindset &amp; Brain: Framestorm Your Way to Success
            					</strong>
            					<div class="detailText">
                					Anette Prehn,
                					Author, Social scientist, Behavioural Expert

            					</div>
            					<div class="details_bottom">
            						<div class="details_rating">
            							<span>
            								<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
            							</span>
            							<span class="digits"> (18)</span></div>
            							<div class="details_price"><div>
            							<span class="card_price">$100</span>
            						</div>
            					</div>
            				</div>
            				</div>
            				<div class="wishlist"><i class="fa fa-heart"></i></div>
						</a>
					</li>
				</ul>
			</div>
		</div>-->
<!--    <div class="row">
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
   <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
      <figure class="course-box bg-white">
         <div class="course-video">
            <iframe width="100%" height="215" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/Hrd0NiWMIjk"></iframe>
         </div>
         <figcaption class="bg-white p-3">
            <h5>Course Name</h5>
            <p class="font-14 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="font-14 text-gray mb-2">David Gassner</p>
            <p class="font-12 mb-2">
               <span class="text-blue">
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               <i class="ion-ios-star"></i>
               </span>
               <span><span class="font-weight-bold">4.6</span> (2,102) </span>
            </p>
         </figcaption>
      </figure>
   </div>
</div>-->
	</div>
</div>

<div class="modal" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="rateyo_next"></div>
        <input type="hidden" id="ratingNo" />
        <textarea class="form-control mt-3" rows="3" id="comment"></textarea>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary" onclick="submitRating();">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$(function () {
  $("#rateYo").rateYo({
      rating: <?php echo $userdata['User']['avg_rating'];?>,
      halfStar: true
    });
});

$(function () {
  $("#rateYo").rateYo()
    .on("rateyo.set", function (e, data) {
      var newdata = data.rating;
      $('#ratingNo').val(newdata);
      
      $('#myModal').modal('show');
      setModalRate(newdata);
  });
  
  
});

function setModalRate(newdata){
  $("#rateyo_next").rateYo({
    rating: newdata,
    halfStar: true
  });
}

$(function () {
   $("#rateyo_next").rateYo()
    .on("rateyo.set", function (e, data) {
      var newdata = data.rating;
      
       $('#ratingNo').val(newdata);
      $('#myModal').modal('show');
  }); 
    
});

</script>
<script>
	$(".show-more a").each(function() {
    var $link = $(this);
    var $content = $link.parent().prev("div.text-content");

    console.log($link);

    var visibleHeight = $content[0].clientHeight;
    var actualHide = $content[0].scrollHeight - 1;

    console.log(actualHide);
    console.log(visibleHeight);

    if (actualHide > visibleHeight) {
        $link.show();
    } else {
        $link.hide();
    }
});

$(".show-more a").on("click", function() {
    var $link = $(this);
    var $content = $link.parent().prev("div.text-content");
    var linkText = $link.text();

    $content.toggleClass("short-text, full-text");

    $link.text(getShowLinkText(linkText));

    return false;
});

function getShowLinkText(currentText) {
    var newText = '';

    if (currentText.toUpperCase() === "SHOW MORE") {
        newText = "Show less";
    } else {
        newText = "Show more";
    }

    return newText;
}
function submitRating()
{
    var rating=$('#ratingNo').val();
    var comment=$('#comment').val();
     $.ajax({
            url: '<?php echo $this->webroot; ?>users/addUserReview',
            type: 'POST',
            dataType: 'json',
            data: {
                rating: rating,
                comment: comment,
                user_id:<?php echo $userdata['User']['id']; ?>,
                fromid:<?php echo $userid; ?>
            },
            success: function (data) {
               
                if (data == '1') {
                    $('.close').click();
                   $('#ajaxFlashMessage').show();
                                
                               $('#ajaxFlashMessage').html('review added successfully!');
                               $('#ajaxFlashMessage').removeClass('error');
                               $('#ajaxFlashMessage').addClass('success');
                                setTimeout(function () {
                                    $('#ajaxFlashMessage').hide();
                                    $('#ajaxFlashMessage').html('');
                                }, 3000);
                } else {
                                $('#ajaxFlashMessage').show();
                               
                                $('#ajaxFlashMessage').html('Please try again');
                                $('#ajaxFlashMessage').removeClass('success');
                                $('#ajaxFlashMessage').addClass('error');
                                setTimeout(function () {
                                    $('#ajaxFlashMessage').hide();
                                    $('#ajaxFlashMessage').html('');
                                }, 3000);
                }
            }
          }); 
}
</script>
