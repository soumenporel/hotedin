<?php 
// pr($userDetails); exit;
$userType = ''; 
if($userDetails['User']['admin_type']==2){ $userType = 'Student'; }
if($userDetails['User']['admin_type']==1){ $userType = 'Tutor'; }
?>
<style>
.form-horizontal .form_part1 {
    width: 15px;
}
.form-horizontal .formdiv {
    border: 0px; 
    width: 94%;
    padding: 14px 0 20px 20px;
}
.header{
  position:relative;
}

.inner_header{
  background:#0c2440;
}
</style>
<section class="profileedit">
  <div class="container">
    <div class="row" style="background: #f6f6f6; border-right: #ddd solid 1px;">
      <div class="col-md-3 col-sm-3" style="padding: 0; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
          <!-- left panel !-->
          <?php echo $this->element('leftpanel');?>
      </div>
      <div class="col-md-9 col-sm-9" style="padding: 0">
        <div class="profile_second_part">
            <h2> Notification Setting </h2>
            <!-- <p>Add information about yourself to share on your profile.</p> -->
            <form class="form-horizontal" method="post" action='' id='edit_profile'>
                <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3">
                    <input type="hidden" class="form-control form_part1" name="data[User][notification_status]" value="0">
                    <input type="checkbox" class="form-control form_part1" <?php if($userDetails['User']['notification_status']==1){ echo 'checked'; } ?> name="data[User][notification_status]" id="text"  value="1">
                    <div class="formdiv"> Don't send me any promotional emails </div>
                  </div>
                </div>
                
                <input type="hidden" name="data[User][id]" id="text"  value="<?php echo $userDetails['User']['id']; ?>" >
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default">Save</button>
                  </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</section>


