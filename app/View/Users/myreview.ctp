
<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}
</style>
<section class="profileedit pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel');?>
            </div>
            <div class="col-md-9 col-sm-9">
                <div class="profile_second_part profile-from">
                    <h2 class="mb-0">Review</h2>
                    <hr>
                    
                    <?php //print_r($ratings);?>
                    
                    <?php foreach($ratings as $rating) { ?>
                    <div class="media mb-4">
                       <div>
                          <img height="50" alt="" src="<?php if (isset($rating['User']['user_image']) && $rating['User']['user_image'] != '') { ?> <?php echo $this->webroot; ?>user_images/<?php echo $rating['User']['user_image'];} else {echo $this->webroot;?>img/profile_img.jpg<?php } ?>" class="d-flex mr-3 rounded-circle">
                       </div>
                       <div class="media-body">
                          <h5 class="mt-0 mb-0"><?php echo $rating['User']['first_name']." ".$rating['User']['last_name']; ?></h5>
                          <div class="rateyo" data-rateyo-read-only="true" data-rateyo-rating="<?php echo $rating['UserRating']['rating']; ?>"></div>
<!--                          <p class="font-12 mb-2">
                             <span class="text-blue">
                             <i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:1.0em;"></i>
                             <i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:1.0em;"></i>
                             <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;"></i>
                             <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;"></i>
                             <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;"></i>
                             </span>
                          </p>-->
                          <p class="mb-1"><?php echo $rating['UserRating']['comment']; ?></p>
                          <div class="date font-12 font-weight-bold"><?php echo date('F j Y h:i:s',strtotime($rating['UserRating']['ratting_date'])); ?></div>
                       </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
   $(function () {
  $(".rateyo").rateYo();
});
    </script>