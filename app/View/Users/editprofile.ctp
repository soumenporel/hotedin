<?php //pr($userDetails); exit;
$userType = '';
if($userDetails['User']['admin_type']==2){ $userType = 'Student'; }
if($userDetails['User']['admin_type']==1){ $userType = 'Tutor'; }
?>


<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}
</style>
<section class="profileedit pt-5">
    <div class="container">
        <div class="row mb-4">
            <div class="col-md-3 col-sm-3" style="padding:0 ">
                <!-- left panel !-->
                <?php echo $this->element('leftpanel');?>
            </div>
            <div class="col-md-9 col-sm-9" style="padding:0;">
                <div class="profile_second_part profile-from pt-0">
                    <h2>Profile</h2>
                    <p>Add information about yourself to share on your profile.</p>
                     <form class="form-horizontal form-area" method="post" action='' id='edit_profile'>
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="text">Basic:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control form_part" id="text" placeholder="Designation" value="<?php echo $userType; ?>" disabled>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part" name="data[User][first_name]" id="text" placeholder="First Name" value="<?php echo $userDetails['User']['first_name'];?>" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part" name="data[User][last_name]" id="text" placeholder="Last Name" value="<?php echo $userDetails['User']['last_name'];?>" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part" name="data[User][email_address]" id="text" placeholder="Email Address" value="<?php echo $userDetails['User']['email_address'];?>" required>
                            </div>
                          </div>
                          <!-- <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part form_text" id="text" placeholder="Headline">
                              <div class="number">75</div>
                            </div>
                          </div> -->
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="text">Biography:</label>
                            <div class="col-sm-9">
                              <textarea class="form-control textareabox" name="data[User][biography]" rows="5" id="comment"><?php echo $userDetails['User']['biography'];?>
                              </textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                              <select class="form-control select_box_part" name="data[User][language_preference]" >
                                <!-- <option value="">Select language</option> -->
                                <option value="Es" <?php if($userDetails['User']['language_preference']=='Es'){ echo 'selected'; }?>>Bahasa Indonesia</option>
                                <option value="En" <?php if($userDetails['User']['language_preference']=='En'){ echo 'selected'; }?> >English</option>
                                
                                <option value="Ot" <?php if($userDetails['User']['language_preference']=='Ot'){ echo 'selected'; }?>>Others</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="text">Location:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control form_part" name="data[User][address]" id="UserAddress" placeholder="Street Address" value="<?php echo $userDetails['User']['address'];?>">
                              <input type="hidden" name="data[User][user_lat]" value="" id="search_lat"/>
                            <input type="hidden" name="data[User][user_lon]" value="" id="search_lng"/>
                            </div>
                          </div>
                         <?php if($userDetails['User']['admin_type']==1){ ?>
                           <div class="form-group">
                            <label class="control-label col-sm-3" for="text">Degree:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control form_part" name="data[User][degree]"  placeholder="Degree" value="<?php echo $userDetails['User']['degree'];?>">
                            </div>
                          </div>

                    <div class="form-group">
                            <label class="control-label col-sm-3" for="text">Subject:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control form_part" name="data[User][subject]"  placeholder="Subject" value="<?php echo $userDetails['User']['subject'];?>">
                            </div>
                          </div>                         <?php } ?>
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="text">Links:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control form_part" name="data[User][website_link]" id="text" placeholder="Website (http(s)://..)" value="<?php echo $userDetails['User']['website_link'];?>">
                            </div>
                          </div>
                          <div class="form-group">

                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part1" name="data[User][google_link]" id="text" placeholder="https://plus.google.com/" value="<?php echo $userDetails['User']['google_link'];?>">
                              <div class="formdiv formLink">Google + Link</div>
                            </div>
                          </div>
                          <div class="form-group">

                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part1" name="data[User][twitter_link]" id="text" placeholder="http://twitter.com/" value="<?php echo $userDetails['User']['twitter_link'];?>">
                              <div class="formdiv formLink">Twitter Profile</div>
                            </div>
                          </div>
                          <div class="form-group">

                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part1" name="data[User][facebook_link]" id="text" placeholder="http://www.facebook.com/ " value="<?php echo $userDetails['User']['facebook_link'];?>">
                              <div class="formdiv formLink">Facebook  Profile</div>
                            </div>
                          </div>
                          <div class="form-group">

                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part1" name="data[User][linkedin_link]" id="text" placeholder="http://www.linkedin.com/   " value="<?php echo $userDetails['User']['linkedin_link'];?>">
                              <div class="formdiv formLink"> Linkedin  Profile</div>
                            </div>
                          </div>
                          <div class="form-group">

                            <div class="col-sm-9 col-sm-offset-3">
                              <input type="text" class="form-control form_part1" name="data[User][youtube_link]" id="text" placeholder="http://www.youtube.com/ " value="<?php echo $userDetails['User']['youtube_link'];?>">
                              <div class="formdiv formLink"> Youtube  Profile</div>
                            </div>
                          </div>
                          <input type="hidden" name="data[User][id]" id="text"  value="<?php echo $userDetails['User']['id']; ?>" >
                          <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                              <button type="submit" class="btn btn-default">Save</button>
                            </div>
                          </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function () {
    $("#edit_profile").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[User][first_name]': {
                validators: {
                    notEmpty: {
                        message: 'The First Name is required and cannot be empty'
                    }
                }
            },
            'data[User][last_name]': {
                validators: {
                    notEmpty: {
                        message: 'The Last Name is required and cannot be empty'
                    }
                }
            },
            'data[User][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address'
                    }
                }
            }
        }
    })
});
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAr9mFfvbx8XxKEJPE3By2AghiC8c2e-5s&libraries=places&callback=initMap"
        async defer></script>

<script>
      function initMap() {
          var autocomplete = new google.maps.places.Autocomplete($("#UserAddress")[0], {});
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
              var place = autocomplete.getPlace();
               $('#search_lat').val(place.geometry.location.lat());
              $('#search_lng').val(place.geometry.location.lng());
              console.log(place.address_components);
          });
      }

      // function loadScript() {
      //     var script = document.createElement("script");
      //     script.type = "text/javascript";
      //     script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAr9mFfvbx8XxKEJPE3By2AghiC8c2e-5s&libraries=places";
      //     document.body.appendChild(script);
      // }

      //window.onload = loadScript;
</script>
<style>
	.profile-from form.form-area{
	 border: 1px solid #dddddd;
    padding: 15px;
    background: #F6F6F6 none repeat scroll 0 0;
}
.profile-from form.form-area label {
    display: block;
    font-size: 13px;
    font-weight: 400;
    width: 100%;
}
.profile-from form.form-area .form-control {
    border-radius: 0;
    font-size: 13px;
    min-height: 40px;
}
</style>
