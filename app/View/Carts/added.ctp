<section class="inner_content">
    <div class="container">

        <div class="cart-area">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="cart-area-left">
                        <div class="media">
                            <div class="media-left">
                                <img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $post['PostImage'][0]['originalpath']; ?>">

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><b>Congratulations!</b> You've successfully added</h4>
                                <h3><?php echo $post['Post']['post_title']; ?></h3>
                                <h5>By <?php echo $post['User']['first_name'] . ' ' . $post['User']['last_name']; ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="cart-area-right">
                        <a class="btn btn-default btn-lg" href="<?php echo $this->webroot; ?>carts">Go to cart</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="affiliate-marketing">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h2>Because you added Affiliate Marketing - A Beginner's Guide to Earning Online</h2>
                </div>
                <div class="col-md-12">
                    <div class="jcarousel-wrapper">
                        <div class="jcarousel" data-jcarousel="true" data-jcarouselautoscroll="true">
                            <ul id="mycarousel" style="left: -371.859px; top: 0px;" data-jcarousel="true">






                                <li style="width: 293px;">
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/big-data-1'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/584168fc4065e.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Big Data</div>
                                            <div class="course_para">
                                                <p>
                                                    Big Data Course</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Big Data</span> <span class="doller25">$Big Data</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$20</span> <span class="doller25">$25</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px; left: 0px; top: 0px;">
                                    <div class="coursename" style="cursor: pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/meganto'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/815014252_wserwerwer.png"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Meganto</div>
                                            <div class="course_para">
                                                <p>
                                                    test</p>
                                            </div>
                                            <div class="writer_name">By Soumya Sankar Mondal</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Meganto</span> <span class="doller25">$Meganto</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$0</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px; left: 0px; top: 0px;">
                                    <div class="coursename" style="cursor: pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/advance-cakephp-course'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/584169d33689c.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Advance CakePHP Course</div>
                                            <div class="course_para">
                                                <p>
                                                    New Advance CakePHP Course</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Advance CakePHP Course</span> <span class="doller25">$Advance CakePHP Course</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px;">
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/java'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/5841699050681.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Complete Java SE 8</div>
                                            <div class="course_para">
                                                <p>Java is one of the most popular programming languages used in professional application....                                    </p></div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Complete Java SE 8</span> <span class="doller25">$Complete Java SE 8</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$20</span> <span class="doller25">$25</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px; left: 0px; top: 0px;">
                                    <div class="coursename" style="cursor: pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/new-mvc-course'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/5841696cd9d9b.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">New MVC Course</div>
                                            <div class="course_para">
                                                <p>
                                                    New Programming course</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$New MVC Course</span> <span class="doller25">$New MVC Course</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px;">
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/advance-javascript'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/5841692bbeb52.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Advance JavaScript </div>
                                            <div class="course_para">
                                                <p>
                                                    New JavaScript Course for IT Professionals</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Advance JavaScript </span> <span class="doller25">$Advance JavaScript </span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$20</span> <span class="doller25">$25</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li></ul>
                        </div>

                        <!--<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                        <a href="#" class="jcarousel-control-next">&rsaquo;</a>-->
                        <!--<p class="jcarousel-pagination" data-jcarouselpagination="true"><a href="#1" class="">1</a><a href="#2" class="active">2</a><a href="#3">3</a><a href="#4" class="">4</a><a href="#5" class="">5</a><a href="#6" class="">6</a></p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="affiliate-marketing">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h2>Because you added Affiliate Marketing - A Beginner's Guide to Earning Online</h2>
                </div>
                <div class="col-md-12">
                    <div class="jcarousel-wrapper">
                        <div class="jcarousel" data-jcarousel="true" data-jcarouselautoscroll="true">
                            <ul id="mycarousel" style="left: -371.859px; top: 0px;" data-jcarousel="true">






                                <li style="width: 293px;">
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/big-data-1'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/584168fc4065e.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Big Data</div>
                                            <div class="course_para">
                                                <p>
                                                    Big Data Course</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Big Data</span> <span class="doller25">$Big Data</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$20</span> <span class="doller25">$25</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px; left: 0px; top: 0px;">
                                    <div class="coursename" style="cursor: pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/meganto'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/815014252_wserwerwer.png"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Meganto</div>
                                            <div class="course_para">
                                                <p>
                                                    test</p>
                                            </div>
                                            <div class="writer_name">By Soumya Sankar Mondal</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Meganto</span> <span class="doller25">$Meganto</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$0</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px; left: 0px; top: 0px;">
                                    <div class="coursename" style="cursor: pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/advance-cakephp-course'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/584169d33689c.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Advance CakePHP Course</div>
                                            <div class="course_para">
                                                <p>
                                                    New Advance CakePHP Course</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Advance CakePHP Course</span> <span class="doller25">$Advance CakePHP Course</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px;">
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/java'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/5841699050681.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Complete Java SE 8</div>
                                            <div class="course_para">
                                                <p>Java is one of the most popular programming languages used in professional application....                                    </p></div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Complete Java SE 8</span> <span class="doller25">$Complete Java SE 8</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$20</span> <span class="doller25">$25</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px; left: 0px; top: 0px;">
                                    <div class="coursename" style="cursor: pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/new-mvc-course'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/5841696cd9d9b.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">New MVC Course</div>
                                            <div class="course_para">
                                                <p>
                                                    New Programming course</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$New MVC Course</span> <span class="doller25">$New MVC Course</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li><li style="width: 293px;">
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '/team4/learnfly/posts/course_details/advance-javascript'">
                                        <div class="courseimage"><img src="/team4/learnfly/img/post_img/5841692bbeb52.jpg"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p">Advance JavaScript </div>
                                            <div class="course_para">
                                                <p>
                                                    New JavaScript Course for IT Professionals</p>
                                            </div>
                                            <div class="writer_name">By NITS Ashis</div>
                                            <div class="rating">Rating: <img src="/team4/learnfly/img/star.png"></div>
                                            <!--<div class="row">
                                                    <div class="dollerprice">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"><span class="pricenin">$Advance JavaScript </span> <span class="doller25">$Advance JavaScript </span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"></div>
                                                    </div>
                                            </div>-->
                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$20</span> <span class="doller25">$25</span></div>
                                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li></ul>
                        </div>

                        <!--<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                        <a href="#" class="jcarousel-control-next">&rsaquo;</a>-->
                        <!--<p class="jcarousel-pagination" data-jcarouselpagination="true"><a href="#1" class="">1</a><a href="#2" class="active">2</a><a href="#3">3</a><a href="#4" class="">4</a><a href="#5" class="">5</a><a href="#6" class="">6</a></p>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="go-to-cart">
        	<div class="row">
            	<div class="col-md-3 col-md-offset-4">
                	<a class="btn btn-default btn-lg" href="<?php echo $this->webroot; ?>carts">Go to cart</a>
                </div>
            </div>
        </div>



    </div>
</section>
