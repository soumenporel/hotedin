<section class="inner_content">
    <div class="container">

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="heading_part">
                    <h2>Shopping Cart</h2>
                </div>
            </div>
        </div>
        <div class="shopping-cart">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="shopping-cart-left">
                        <h3><span id="cartCnt"><?php echo!empty($cartItems['CartItem']) ? count($cartItems['CartItem']) : '0'; ?></span> Course in Cart</h3>

                        <div class="shopping-cart-area">
                            <ul class="cartParent">
                                <?php
                                if (!empty($cartItems['CartItem'])) {
                                    $price = 0;
                                    $old_price = 0;
                                    foreach ($cartItems['CartItem'] as $cartItem) {
                                        if ($userid) {
                                            $image = end($cartItem['Post']['PostImage']);
                                        } else {
                                            $image = end($cartItem['PostImage']);
                                        }
                                        $price += $cartItem['Post']['price'];
                                        $old_price += $cartItem['Post']['old_price'];
                                        ?>
                                        <li class="cartListing">
                                            <div class="shopping">

                                                <div class="media">
                                                    <div class="media-left">
                                                        <img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $image['originalpath']; ?>">

                                                    </div>
                                                    <div class="media-body"> 
                                                        <h4 class="media-heading"><?php echo $cartItem['Post']['post_title']; ?></h4>
                                                        <p>By NITS Ashis</p>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-remove-area">
                                                    <ul>
                                                        <li>
                                                            <a href="javascript:void(0)" class="removeCart" 
                                                               data-cartid="<?php echo $cartItems['Cart']['id']; ?>" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Remove</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="cartToSaveLater" 
                                                               data-cartid="<?php echo $cartItems['Cart']['id']; ?>" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Save for later</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="cartToWishlist" 
                                                               data-cartid="<?php echo $cartItems['Cart']['id']; ?>" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Add to wishlists</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="shopping-cart-price-area">
                                                    <ul>
                                                        <li>
                                                            <div class="price-tag">$<?php echo $cartItem['Post']['price']; ?> <i class="fa fa-tag" aria-hidden="true"></i></div>
                                                        </li>
                                                        <li>$<?php echo $cartItem['Post']['old_price']; ?></li>
                                                    </ul>
                                                </div>
                                                <div class="clearfix"></div>

                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>


                        <h3>Save for later</h3>

                        <div class="shopping-cart-area">
                            <ul class="saveLaterParent">
                                <?php
                                if (!empty($saveLater)) {
                                    foreach ($saveLater as $cartItem) {
                                        if ($userid) {
                                            $image = end($cartItem['Post']['PostImage']);
                                        } else {
                                            $image = end($cartItem['PostImage']);
                                        }
                                        ?>
                                        <li class="saveLaterListing">
                                            <div class="shopping">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $image['originalpath']; ?>">

                                                    </div>
                                                    <div class="media-body"> 
                                                        <h4 class="media-heading"><?php echo $cartItem['Post']['post_title']; ?></h4>
                                                        <p>By NITS Ashis</p>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-remove-area">
                                                    <ul>
                                                        <li>
                                                            <a href="javascript:void(0)" class="removeSaveLater" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Remove</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="saveLaterToCart" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Move to Cart</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="shopping-cart-price-area">
                                                    <ul>
                                                        <li>
                                                            <div class="price-tag">$<?php echo $cartItem['Post']['price']; ?> <i class="fa fa-tag" aria-hidden="true"></i></div>
                                                        </li>
                                                        <li>$<?php echo $cartItem['Post']['old_price']; ?></li>
                                                    </ul></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <h3>Recently added wishlist</h3>

                        <div class="shopping-cart-area">
                            <ul class="wishlistParent">
                                <?php
                                if (!empty($wishlist)) {
                                    foreach ($wishlist as $cartItem) {
                                        if ($userid) {
                                            $image = end($cartItem['Post']['PostImage']);
                                        } else {
                                            $image = end($cartItem['PostImage']);
                                        }
                                        ?>
                                        <li class="wishlistListing">
                                            <div class="shopping">

                                                <div class="media">
                                                    <div class="media-left">
                                                        <img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $image['originalpath']; ?>">

                                                    </div>
                                                    <div class="media-body"> 
                                                        <h4 class="media-heading"><?php echo $cartItem['Post']['post_title']; ?></h4>
                                                        <p>By NITS Ashis</p>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-remove-area">
                                                    <ul>
                                                        <li>
                                                            <a href="javascript:void(0)" class="removeWishlist" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Remove</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0)" class="wishlistToCart" 
                                                               data-postid="<?php echo $cartItem['Post']['id']; ?>">Move to Cart</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="shopping-cart-price-area">
                                                    <ul>
                                                        <li><div class="price-tag">$<?php echo $cartItem['Post']['price']; ?> <i class="fa fa-tag" aria-hidden="true"></i></div>
                                                        </li>
                                                        <li>$<?php echo $cartItem['Post']['old_price']; ?></li>
                                                    </ul></div>
                                                <div class="clearfix"></div>

                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>


                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="shopping-cart-right">
                        <h3>Total:</h3>
                        <p>$<span id="tot_price"><?php echo!empty($price) ? $price : '0'; ?></span></p>
                        <div class="off-text">
                            <p>
                                <span style="text-decoration: line-through; font-size:18px; font-weight:400; color:#333;">
                                    $<span id="tot_old_price"><?php echo!empty($old_price) ? $old_price : '0'; ?></span>
                                </span> 
                                <span id="tot_percentage" style="font-size:18px; font-weight:400; color:#333;">
                                    <?php echo!empty($price) ? (100 - ceil(($price * 100) / $old_price)) : '0'; ?>
                                </span>
                                <span style="font-size:18px; font-weight:400; color:#333;">% off</span>
                            </p>
                        </div>

                        <p>
                            <a class="btn btn-default btn-lg" href="<?php echo $this->webroot; ?>checkouts">Checkout</a>
                        </p>
                        <!--<div class="shopping-cart-right-form">
                            <form class="form-inline">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
                                </div>
                                <button type="submit" class="btn btn-default">Sign in</button>
                            </form>
                        </div>-->
                    </div>
                    <!--<div class="click-area">
                        <p>Click on a coupon code to filter</p>
                        <h4>HOLIDAY15D <span>is applied</span></h4>
                    </div>-->
                </div>
            </div>
        </div>


    </div>
</section>

<script>
    $(function () {
<?php
if ($userid) {
    ?>
            $(document).on('click', '.removeCart', function (e) {
                if (confirm('Do You want remove this item')) {
                    e.preventDefault();
                    var self = $(this);
                    var cartid = self.data('cartid');
                    var postid = self.data('postid');

                    $.ajax({
                        url: '<?php echo $this->webroot; ?>carts/removefromcart',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            cartid: cartid,
                            postid: postid
                        },
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        success: function (data) {
                            $('#loading').hide();
                            if (data.ack == '1') {
                                generateCartPageHtml(data.html);
                            } else {
                                alert(data.msg);
                            }
                        }
                    });
                }    
            });
    <?php
} else {
    ?>
            $(document).on('click', '.removeCart', function (e) {
                if (confirm('Do You want remove this item')) {
                    e.preventDefault();
                    var self = $(this);
                    var cartid = self.data('cartid');
                    var postid = self.data('postid');

                    $.ajax({
                        url: '<?php echo $this->webroot; ?>carts/removefromcart_session',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            cartid: cartid,
                            postid: postid
                        },
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        success: function (data) {
                            $('#loading').hide();
                            if (data.ack == '1') {
                                $('#cartCnt').text(data.cnt);
                                self.closest('tr').remove();
                                $('#tot_price').text(data.price);
                                $('#tot_old_price').text(data.old_price);
                                $('#tot_percentage').text(data.percentage);
                            } else {
                                alert(data.msg);
                            }
                        }
                    });
                }
            });
    <?php
}
?>

        $(document).on('click', '.cartToSaveLater', function () {
            var self = $(this);
            var cartid = self.data('cartid');
            var postid = self.data('postid');

            $.ajax({
                url: '<?php echo $this->webroot; ?>save_laters/cart_to_savelater',
                type: 'POST',
                dataType: 'json',
                data: {
                    cartid: cartid,
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#loading').hide();
                    if (data.ack == '1') {
                        generateCartPageHtml(data.html);
                    } else {
                        if ($('#flashMessage').length > 0) {
                            $('#flashMessage').text(data.msg).show();
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        } else {
                            var msg = '<div id="flashMessage" class="message">' + data.msg + '</div>';
                            $(msg).insertAfter('.header.inner_header');
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        }
                    }
                }
            });
        });

        $(document).on('click', '.removeSaveLater', function () {
            if (confirm('Do you want to delete this item')) {    
                var self = $(this);
                var cartid = self.data('cartid');
                var postid = self.data('postid');

                $.ajax({
                    url: '<?php echo $this->webroot; ?>save_laters/remove_save_later',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        postid: postid,
                        userid: '<?php echo $userid; ?>'
                    },
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    success: function (data) {
                        $('#loading').hide();
                        if (data.ack == '1') {
                            generateCartPageHtml(data.html);
                        } else {

                        }
                    }
                });
            }    
        });

        $(document).on('click', '.cartToWishlist', function () {
            var self = $(this);
            var cartid = self.data('cartid');
            var postid = self.data('postid');

            $.ajax({
                url: '<?php echo $this->webroot; ?>wishlists/cart_to_wishlist',
                type: 'POST',
                dataType: 'json',
                data: {
                    cartid: cartid,
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#loading').hide();
                    if (data.ack == '1') {
                        generateCartPageHtml(data.html);
                    } else {
                        if ($('#flashMessage').length > 0) {
                            $('#flashMessage').text(data.msg).show();
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        } else {
                            var msg = '<div id="flashMessage" class="message">' + data.msg + '</div>';
                            $(msg).insertAfter('.header.inner_header');
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        }
                    }
                }
            });
        });

        $(document).on('click', '.removeWishlist', function () {
            var self = $(this);
            var cartid = self.data('cartid');
            var postid = self.data('postid');

            $.ajax({
                url: '<?php echo $this->webroot; ?>wishlists/remove_wishlist',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#loading').hide();
                    if (data.ack == '1') {
                        generateCartPageHtml(data.html);
                    } else {

                    }
                }
            });
        });

        $(document).on('click', '.saveLaterToCart', function () {
            var self = $(this);
            var postid = self.data('postid');

            $.ajax({
                url: '<?php echo $this->webroot; ?>carts/save_later_to_cart',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#loading').hide();
                    if (data.ack == '1') {
                        generateCartPageHtml(data.html);
                    } else {
                        if ($('#flashMessage').length > 0) {
                            $('#flashMessage').text(data.msg).show();
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        } else {
                            var msg = '<div id="flashMessage" class="message">' + data.msg + '</div>';
                            $(msg).insertAfter('.header.inner_header');
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        }
                    }
                }
            });
        });

        $(document).on('click', '.wishlistToCart', function () {
            var self = $(this);
            var postid = self.data('postid');

            $.ajax({
                url: '<?php echo $this->webroot; ?>carts/wishlist_to_cart',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#loading').hide();
                    if (data.ack == '1') {
                        generateCartPageHtml(data.html);
                    } else {
                        if ($('#flashMessage').length > 0) {
                            $('#flashMessage').text(data.msg).show();
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        } else {
                            var msg = '<div id="flashMessage" class="message">' + data.msg + '</div>';
                            $(msg).insertAfter('.header.inner_header');
                            setTimeout(function () {
                                $('#flashMessage').hide();
                            }, 2000);
                        }
                    }
                }
            });
        });

    });

    function generateCartPageHtml(data) {
        var cartHtml = '';
        var saveLaterHtml = '';
        var wishlistHtml = '';
        var headerCartHtml = '';

        var cartObj = data.CartItems;
        var saveLaterObj = data.SaveLater;
        var wishlistObj = data.Wishlist;

        $.each(cartObj, function (key, value) {
            cartHtml += '<li class="cartListing"><div class="shopping"><div class="media"><div class="media-left">' +
                    '<img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/' + value.Post.PostImage[0].originalpath + '">' +
                    '</div><div class="media-body">' +
                    '<h4 class="media-heading">' + value.Post.post_title + '</h4>' +
                    '<p>By NITS Ashis</p>' +
                    '</div></div><div class="shopping-cart-remove-area"><ul>' +
                    '<li><a href="javascript:void(0)" class="removeCart" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Remove</a>' +
                    '</li>' +
                    '<li>' +
                    '<a href="javascript:void(0)" class="cartToSaveLater" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Save for later</a>' +
                    '</li>' +
                    '<li>' +
                    '<a href="javascript:void(0)" class="cartToWishlist" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Add to wishlists</a>' +
                    '</li>' +
                    '</ul></div><div class="shopping-cart-price-area"><ul>' +
                    '<li>' +
                    '<div class="price-tag">$' + value.Post.price + ' <i class="fa fa-tag" aria-hidden="true"></i></div>' +
                    '</li>' +
                    '<li>$' + value.Post.old_price + '</li>' +
                    '</ul></div><div class="clearfix"></div></div></li>';
        });

        $.each(saveLaterObj, function (key, value) {
            saveLaterHtml += '<li class="saveLaterListing"><div class="shopping"><div class="media"><div class="media-left">' +
                    '<img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/' + value.Post.PostImage[0].originalpath + '">' +
                    '</div><div class="media-body">' +
                    '<h4 class="media-heading">' + value.Post.post_title + '</h4>' +
                    '<p>By NITS Ashis</p>' +
                    '</div></div><div class="shopping-cart-remove-area"><ul>' +
                    '<li><a href="javascript:void(0)" class="removeSaveLater" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Remove</a>' +
                    '</li>' +
                    '<li>' +
                    '<a href="javascript:void(0)" class="saveLaterToCart" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Move to cart</a>' +
                    '</li>' +
                    '</ul></div><div class="shopping-cart-price-area"><ul>' +
                    '<li>' +
                    '<div class="price-tag">$' + value.Post.price + ' <i class="fa fa-tag" aria-hidden="true"></i></div>' +
                    '</li>' +
                    '<li>$' + value.Post.old_price + '</li>' +
                    '</ul></div><div class="clearfix"></div></div></li>';
        });

        $.each(wishlistObj, function (key, value) {
            wishlistHtml += '<li class="saveLaterListing"><div class="shopping"><div class="media"><div class="media-left">' +
                    '<img class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/' + value.Post.PostImage[0].originalpath + '">' +
                    '</div><div class="media-body">' +
                    '<h4 class="media-heading">' + value.Post.post_title + '</h4>' +
                    '<p>By NITS Ashis</p>' +
                    '</div></div><div class="shopping-cart-remove-area"><ul>' +
                    '<li><a href="javascript:void(0)" class="removeWishlist" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Remove</a>' +
                    '</li>' +
                    '<li>' +
                    '<a href="javascript:void(0)" class="wishlistToCart" ' +
                    'data-cartid="<?php echo $cartItems['Cart']['id']; ?>" ' +
                    'data-postid="' + value.Post.id + '">Move to cart</a>' +
                    '</li>' +
                    '</ul></div><div class="shopping-cart-price-area"><ul>' +
                    '<li>' +
                    '<div class="price-tag">$' + value.Post.price + ' <i class="fa fa-tag" aria-hidden="true"></i></div>' +
                    '</li>' +
                    '<li>$' + value.Post.old_price + '</li>' +
                    '</ul></div><div class="clearfix"></div></div></li>';
        });

        $('.cartParent').html(cartHtml);
        $('.saveLaterParent').html(saveLaterHtml);
        $('.wishlistParent').html(wishlistHtml);
        $('#cartCnt').text(data.cnt);
        $('#cart-item-count').html(data.cnt);
        $('#tot_price').text(data.price);
        $('#tot_old_price').text(data.old_price);
        $('#tot_percentage').text(data.percentage);
        console.log(cartObj.length);
        if (cartObj.length > 0) {

            $.each(cartObj, function (key, value) {
                headerCartHtml += '<tr>';
                headerCartHtml += '<td class="table_part_img">' +
                        '<img src="<?php echo $this->webroot; ?>img/post_img/' + value.Post.PostImage[0].originalpath + '">' +
                        '</td><td class="table_text">' +
                        '<a href="#">' + value.Post.post_title + '</a>' +
                        '<span>' + value.Post.User.first_name + ' ' + value.Post.User.last_name + '</span>' +
                        '</td>';
                headerCartHtml += '<td class="table_price">$' + value.Post.price + ' <span>$' + value.Post.old_price + '</span></td>';
                headerCartHtml += '</tr>';
            });

            headerCartHtml += '<td class="total" colspan="3">Your total: <span class="total_price">$' + data.price + '</span><span class="cutoff"> $' + data.old_price + '</span></td>';
        } else {
            headerCartHtml += '<tr>';
            headerCartHtml += '<td class="table_text"><span>Not found</span></td>';
            headerCartHtml += '</tr>';
            headerCartHtml += '<td class="total" colspan="3">Your total: <span class="total_price">$' + data.price + '</span><span class="cutoff"> $' + data.old_price + '</span></td>';
        }

        $('#headerCart').html(headerCartHtml);
    }
</script>