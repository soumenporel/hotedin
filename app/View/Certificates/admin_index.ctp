<?php
//pr($posts);

?>
<div class="categories index">
    <h2><?php echo __('Certificate List'); ?></h2>
    <div>
      
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
           
           <th><?php echo $this->Paginator->sort('user_name'); ?></th>
            <th><?php echo $this->Paginator->sort('course_name', 'Course Title'); ?></th>
            <th><?php echo $this->Paginator->sort('quiz_name', 'Quiz'); ?></th>
            <th><?php echo $this->Paginator->sort('score'); ?></th>
            <th><?php echo $this->Paginator->sort('percentage'); ?></th>

            <th><?php echo $this->Paginator->sort('test_date', 'Date of test'); ?></th>
            <th>Certificate No</th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php
        $CatCnt=0;
        foreach ($posts as $post):
            $CatCnt++;
        
        $file_name=explode("_",$post['QuizScore']['file_name']);
        $certificateNo=str_replace('.pdf','',$file_name['1']);
	
	?>
        <tr>
            <td><?php echo $CatCnt; ?>&nbsp;</td>
            
            <td>
                <?php echo $post['user_name'];?>
            </td> 
            <td><?php echo $post['course_name'];?></td>
            <td><?php echo $post['quiz_name'];?></td>
            <td><?php echo $post['QuizScore']['score'];?></td>
            <td><?php echo $post['QuizScore']['percentage'];?></td>
            <td><?php echo date('F j Y',strtotime($post['QuizScore']['test_date']));?></td>
            <td><?php echo $certificateNo; ?></td>
            <td>
                <a href="<?php echo $this->webroot;?>certificate/<?php echo $post['QuizScore']['file_name']; ?>" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a>
               
            </td>


        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
