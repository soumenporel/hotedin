<div class="users index">
	<h2 style="width:400px;float:left;"><?php echo __('Discoussion'); ?></h2>
	<table style="width:100%;border:0px solid red;">
        <tr>
            <td style="width:70%;border:0px solid red;">&nbsp;</td>
            <td ><a href="<?php echo($this->webroot);?>admin/ratings/add" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New Discoussion</a>  
        </tr></td>
        </tr>
    </table>    
        
        
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th>SN</th>
		<th><?php echo $this->Paginator->sort('first_name'); ?></th>
		<th><?php echo $this->Paginator->sort('last_name'); ?></th>
		<th><?php echo $this->Paginator->sort('post_id','Course Name'); ?></th>
		<th><?php echo $this->Paginator->sort('ratting','Rating'); ?></th>
		<th><?php echo $this->Paginator->sort('description','Review'); ?></th>
		<th><?php echo $this->Paginator->sort('status'); ?></th>
		<th><?php echo $this->Paginator->sort('ratting_date','Date'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
        $Cnt=0;
        $uploadImgPath = WWW_ROOT.'user_images';
        foreach ($ratings as $rating): 
	    $Cnt++;?>
	<tr>
		<td><?php echo $Cnt;?>&nbsp;</td>
		<td><?php echo h($rating['User']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($rating['User']['last_name']); ?>&nbsp;</td>
		<td><?php echo h($rating['Post']['post_title']); ?>&nbsp;</td>
		<td>
			<?php for ($i=0; $i <$rating['Rating']['ratting'] ; $i++) { ?>
               <i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>

          <?php  }
            $cnt=5-$rating['Rating']['ratting'];
            for ($i=0; $i <$cnt  ; $i++) { ?>
               <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>

           <?php  } ?>
			&nbsp;</td>		
		<td><?php echo strip_tags($rating['Rating']['comment']); ?>&nbsp;</td>
		<td><?php if($rating['Rating']['status']==1){ ?> <img src="<?php echo $this->webroot; ?>/img/success-01-128.png" style="height:30px;" /><?php }else{ ?><img src="<?php echo $this->webroot; ?>/img/cross-512.png" style="height:30px;" /><?php } ?>&nbsp;</td>
		<td><?php echo h($rating['Rating']['ratting_date']); ?>&nbsp;</td> 
		<td >
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')),
                        array('action' => 'view', $rating['Rating']['id']),
                        array('class' => 'btn btn-success btn-xs', 'escape'=>false)); ?>
                            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                        array('action' => 'edit', $rating['Rating']['id']),
                        array('class' => 'btn btn-info btn-xs', 'escape'=>false)); ?>
                            <?php echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                        array('action' => 'delete', $rating['Rating']['id']),
                        array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
                        __('Are you sure you want to delete # %s?', $rating['Rating']['id'])); ?>
                    </td>


		 
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

<style>
.title a{
    color: #fff !important;
}
</style> 