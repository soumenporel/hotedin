<style>

.media {
  // Proper spacing between instances of .media
  margin-top: 15px;

  &:first-child {
    margin-top: 0;
  }
}

.media,
.media-body {
  zoom: 1;
  overflow: hidden;
}

.media-body {
  width: 10000px;
}

.media-object {
  display: block;

  // Fix collapse in webkit from max-width: 100% and display: table-cell.
  &.img-thumbnail {
    max-width: none;
  }
}

.media-right,
.media > .pull-right {
  padding-left: 10px;
}

.media-left,
.media > .pull-left {
  padding-right: 10px;
}

.media-left,
.media-right,
.media-body {
  display: table-cell;
  vertical-align: top;
}

.media-middle {
  vertical-align: middle;
}

.media-bottom {
  vertical-align: bottom;
}

// Reset margins on headings for tighter default spacing
.media-heading {
  margin-top: 0;
  margin-bottom: 5px;
}

// Media list variation
//
// Undo default ul/ol styles
.media-list {
  padding-left: 0;
  list-style: none;
}

</style>

<div class="testimonials view">
    <h2><?php echo __('Rating'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($rating['Rating']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('User Name'); ?></dt>
        <dd>
            <?php echo h($rating['User']['first_name'].' '.$rating['User']['last_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Course Name'); ?></dt>
        <dd>
            <?php echo h($rating['Post']['post_title']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Rating'); ?></dt>
        <dd>
            <?php for ($i=0; $i <$rating['Rating']['ratting'] ; $i++) { ?>
               <i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>

          <?php  }
            $cnt=5-$rating['Rating']['ratting'];
            for ($i=0; $i <$cnt  ; $i++) { ?>
               <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>

           <?php  } ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Rewiew'); ?></dt>
        <dd>
            <?php echo strip_tags($rating['Rating']['comment']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Date'); ?></dt>
        <dd>
            <?php echo h($rating['Rating']['ratting_date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
            <?php echo $rating['Rating']['status']=='1' ? 'Active' : 'Inactive'; ?>
            &nbsp;
        </dd>
    </dl>

    <?php 
        // echo '<pre>'; print_r($comment); echo '</pre>'; 

        if(isset($rating['User']['UserImage']['0']['originalpath']) && $rating['User']['UserImage']['0']['originalpath']!='' ){
            $imgUser = $this->webroot.'user_images/'.$rating['User']['UserImage']['0']['originalpath'];
        }else{
            $imgUser = $this->webroot.'user_images/default.png';
        }

    ?>

    <div>
        <h2> Comments </h2>
        <ul class="media-list"> 
            <li class="media"> 
                <div class="media-left"> 
                    <a href="#"> 
                        <img alt="64x64" class="media-object" src="<?php echo $imgUser; ?>" src="" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                    </a> 
                </div> 
                <div class="media-body"> 
                    <h4 class="media-heading"><?php echo $rating['User']['first_name'].' '.$rating['User']['last_name']; ?></h4> 
                    <p><?php echo strip_tags($rating['Rating']['comment']); ?></p>
                    <p>
                        <?php for ($i=0; $i <$rating['Rating']['ratting'] ; $i++) { ?>
                           <i class="fa fa-star" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>

                      <?php  }
                        $cnt=5-$rating['Rating']['ratting'];
                        for ($i=0; $i <$cnt  ; $i++) { ?>
                           <i class="fa fa-star-o" aria-hidden="true" style="color: gold; font-size:1.0em;" ></i>

                       <?php  } ?>
                    </p> 
                    <?php foreach ($comment as $key => $value) { 

                        if(isset($value['User']['UserImage']['0']['originalpath']) && $value['User']['UserImage']['0']['originalpath']!='' ){
                            $imgUserC = $this->webroot.'user_images/'.$value['User']['UserImage']['0']['originalpath'];
                        }else{
                            $imgUserC = $this->webroot.'user_images/default.png';
                        }

                    ?>
                        <div class="media"> 
                            <div class="media-left"> 
                                <a href="#"> 
                                    <img alt="64x64" class="media-object" src="<?php echo $imgUserC; ?>" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                                </a> 
                            </div> 

                            <div class="media-body"> 
                                <h4 class="media-heading">
                                    <?php if($value['Comment']['user_id']==152){ echo 'Admin';  }else{ echo $value['User']['first_name'].' '.$value['User']['last_name']; } ?>
                                </h4> <?php echo $value['Comment']['message']; ?> 

                            </div> 
                        </div> 
                    <?php } ?>                    
                    <div class="media"> 
                        <!-- <div class="media-left"> 
                            <textarea></textarea> 
                        </div> --> 
                        <div class="media-body">
                            <form action="" method="post" > 
                                <textarea name="message" ></textarea>
                                <input type="hidden" name="post_id" value="<?php echo $rating['Rating']['post_id']; ?>"></input>
                                <input type="hidden" name="rating_id" value="<?php echo $rating['Rating']['id']; ?>"></input>  
                                <button type="submit" class="btn btn-info" >Save</button>
                            </form>    
                        </div> 
                    </div>  
                </div> 
            </li> 
        </ul>
    </div>    
</div>