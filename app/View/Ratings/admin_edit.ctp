<div class="categories form">
<?php echo $this->Form->create('Rating',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Rating'); ?></legend>
	<?php
       	echo $this->Form->input('id');
        echo $this->Form->input('user_id', array('empty' => '(Choose any User)','required'=>'required'));
		echo $this->Form->input('post_id',array('empty' => '(Choose any Course)','label' =>'Courses','required'=>'required')); ?>
		<div class="input number">
			<label for="BannerOrder">Give Your Review</label>
			<div id="stars-default" data-rating = "<?php echo $this->request->data['Rating']['ratting'];?>"><input type = "hidden" name="rating"/></div>
		</div>
		<input type="hidden" name="data[Rating][ratting]" id="review_val" >
		<?php 
        echo $this->Form->input('comment', array('label' => 'Your Review', 'type' => 'textarea','id'=>'description'));
        echo $this->Form->input('status');
	?>
    </fieldset>


<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('description',
            {
                width: "95%"
            });
</script>  
<script>
(function ( $ ) {
  
    $.fn.rating = function( method, options ) {
    method = method || 'create';
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
      limit: 5,
      value: 0,
      glyph: "fa fa-star",
            coloroff: "gray",
      coloron: "gold",
      size: "2.0em",
      cursor: "default",
      onClick: function () {},
            endofarray: "idontmatter"
        }, options );
    var style = "";
    style = style + "font-size:" + settings.size + "; ";
    style = style + "color:" + settings.coloroff + "; ";
    style = style + "cursor:" + settings.cursor + "; ";
  

    
    if (method == 'create')
    {
      //this.html('');  //junk whatever was there
      
      //initialize the data-rating property
      this.each(function(){
        attr = $(this).attr('data-rating');
        if (attr === undefined || attr === false) { $(this).attr('data-rating',settings.value); }
      })
      
      //bolt in the glyphs
      for (var i = 0; i < settings.limit; i++)
      {
        this.append('<span data-value="' + (i+1) + '" class="ratingicon glyphicon ' + settings.glyph + '" style="' + style + '" aria-hidden="true"></span>');
      }
      
      //paint
      this.each(function() { paint($(this)); });

    }
    if (method == 'set')
    {
      this.attr('data-rating',options);
      this.each(function() { paint($(this)); });
    }
    if (method == 'get')
    {
      return this.attr('data-rating');
    }
    //register the click events
    this.find("span.ratingicon").click(function() {
      rating = $(this).attr('data-value')
      $(this).parent().attr('data-rating',rating);
      paint($(this).parent());
      settings.onClick.call( $(this).parent() );
    })
    function paint(div)
    {
      rating = parseInt(div.attr('data-rating'));
      div.find("input").val(rating);  //if there is an input in the div lets set it's value
      div.find("span.ratingicon").each(function(){  //now paint the stars
        
        var rating = parseInt($(this).parent().attr('data-rating'));
        $("#review_val").val(rating);
        var value = parseInt($(this).attr('data-value'));
        
        if (value > rating) { $(this).css('color',settings.coloroff); }
        else { $(this).css('color',settings.coloron); }
      })
    }

    };
 
}( jQuery ));

$(document).ready(function(){

  $("#stars-default").rating();
  $("#stars-green").rating('create',{coloron:'green',onClick:function(){ alert('rating is ' + this.attr('data-rating')); }});
  $("#stars-herats").rating('create',{coloron:'red',limit:10,glyph:'glyphicon-heart'}); 
});
$(document).ready(function(){
  $("#form_submit").click(function(){
    $.ajax({
      url: "<?php echo $this->webroot;?>reviews/ajaxReviewForm",
      type: "POST",
      data: $("#review_form").serialize(),
       success: function(result){
        if(result==1){
          $("#responce").show()
          setTimeout(function() { $("#responce").hide(); }, 5000);
        }
       }
    });
  });
});
</script>