<div class="users index">
    <h2 style="width:100%;float:left;"><?php if($type==1){ echo 'Employer';} else { echo 'Jobseeker';} ?></h2>
    <form name="Searchuserfrm" method="post"  id="Searchuserfrm">   
        <table style=" border:none;" class="data-table">
<!--            <tr>
                <td width="12%">Enter Email Address:</td> 
                <td width="20%"><input type="email" name="email_address" id="email_address" value="<?php echo $email;?>"/></td>

                <td><input type="submit" name="search" value="Search">
                </td>
            </tr> 
            -->
            <tr>
                <a href="<?php echo($this->webroot);?>admin/dashboards/index" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a> 
                <?php if($type==1){ ?>
                <a href="<?php echo($this->webroot);?>admin/normal_users/add/1" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add Employer</a>  
                <?php } else { ?>
                 <a href="<?php echo($this->webroot);?>admin/normal_users/add/2" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add Job seeker</a>  
                <?php } ?>
        </tr>
            
            </tr>
        </table>
    </form>
    <?php //if($type==2){ ?>
<!--     <form name="send_push_form" method="post" action="<?php echo($this->webroot);?>admin/normal_users/send_push_form">
         <table style=" border:none;" class="data-table">
    <tr>
            <a href="javascript:void(0);" class="btn btn-info pull-right" onclick="$('.push_textbox').show();">Send Push Notification</a> 
            </tr>
            <tr class="push_textbox" style="display: none;">
                <td width="12%">Enter Message here:</td> 
                <td width="20%"><input type="text" name="push_msg" id="push_msg"/></td>

                <td><input type="submit" name="push_submit" value="Submit">
                </td>
            </tr>  
         </table>
     </form>-->
    <?php //} ?>
<!--    <form name="student_details_export" method="post" action="<?php echo($this->webroot);?>admin/normal_users/exportcsv">
        <table style=" border:none;" class="data-table">
            <tr>
                <?php if( $type == '2' ){ ?>
                    <td width="15%">Membership Type :</td> 
                    <td width="20%">
                        <select name="plan_id" id="plan_id" class="selectbox2">
                            <option value="" >Select Option</option>
                            <?php foreach ($membership_plan_list as $key => $value) {?>
                            <option value="<?php echo $value['id'];?>"><?php echo $value['title']/*.' '.$value['duration_in']*/;?>
                            </option>
                            <?php }?>
                        </select>
                    </td>
                <?php }else{ ?>
                    <td width="15%">Export to CSV File:</td>    
                <?php } ?>
                <td>
                    <button id="export_csv" type="button" class="btn btn-primary">Export CSV</button>
                </td>
            </tr>
            <tr id="error_div"></tr>
            <tr id="csv_check" style="display:none">
                <td width="15%">Type Your Password:</td> 
                <td width="20%">
                    <input type="hidden" name="selected_plan_id" id="selected_plan_id" value="">
                    <input type="text" name="csv_password" id="csv_password">

                </td>
                <td>
                    <input type="submit" name="search" value="Submit">
                </td>
            </tr>
        </table>
    </form>-->

    <table class="table table-striped table-bordered bulk-action" id="sample_1">
        <tr>
            <td width="11%">Bulk Action :</td> 
            <td width="20%"><select name="action_type" id="action_type" class="selectbox2">
                <option value="">Select Option</option>
                <option value="1">Delete</option>
                <option value="2">Approve</option>
                <option value="3">Disapprove</option>
            </select>
        </td>
        <td>
            <button id="bulk_action" type="button" class="btn btn-primary">Action</button>
        </td>
    </tr> 

</table>

<table cellpadding="0" cellspacing="0">
    <tr>
        <th style="width:10%;" ><input style="margin-top: 4px;" type="checkbox" id="sel_all" value="1"/>Select All</th>
        <th>SN<?php //echo $this->Paginator->sort('id');       ?></th>
        <th><?php echo $this->Paginator->sort('first_name'); ?></th>
        <th style="width: 9%"><?php echo $this->Paginator->sort('last_name'); ?></th>
        <th>Profile</th>

        <th style="width: 9%"><?php echo $this->Paginator->sort('admin_type', 'User Type'); ?></th>
<!--        <th><?php echo $this->Paginator->sort('membership_plan_id', 'Membership type'); ?></th>
        <th>Member Since  - Until</th>-->
        <th style="width:9%;" ><?php echo $this->Paginator->sort('email_address', 'Email'); ?></th>
        <th style="width: 9%"><?php echo $this->Paginator->sort('activated', 'Activation Status'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    <?php
    $UserCnt = 0;
    $uploadImgPath = WWW_ROOT . 'user_images';
    foreach ($users as $user):
            //print_r($user);die;
        $UserCnt++;
    ?>
    <tr>
        <td>
            <input type="checkbox" class="check" name="user_id[]" value="<?php echo ($user['User']['id']); ?>">
        </td>
        <td><?php echo $UserCnt; //echo h($user['User']['id']);       ?>&nbsp;</td>
        <td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
        <td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>	
         <td><?php
            $per_profile_img = isset($user['User']['user_image']) ? $user['User']['user_image']: '';
            if ($per_profile_img != '' && file_exists($uploadImgPath . '/' . $per_profile_img)) {
                $ImgLink = $this->webroot . 'user_images/' . $per_profile_img;
            } else {
                $ImgLink = $this->webroot . 'user_images/default.png';
            }
            echo '<img src="' . $ImgLink . '" alt="" height="70px" width="70px"/>';
            ?></td>	

    <!--     <td><?php
            $per_profile_img = isset($user['UserImage']['0']['originalpath']) ? $user['UserImage']['0']['originalpath'] : '';
            if ($per_profile_img != '' && file_exists($uploadImgPath . '/' . $per_profile_img)) {
                $ImgLink = $this->webroot . 'user_images/' . $per_profile_img;
            } else {
                $ImgLink = $this->webroot . 'user_images/default.png';
            }
            echo '<img src="' . $ImgLink . '" alt="" height="70px" width="70px"/>';
            ?></td> -->

            <td><?php
                if ($user['User']['admin_type'] == 1) {
                    echo 'Employer';
                }
                if ($user['User']['admin_type'] == 2) {
                    echo 'Job seeker';
                }
                ?>&nbsp;</td>

<!--                <td><?php echo $user['MembershipPlan']['title']; ?>&nbsp;</td>
                <td>
                    <?php 
                    if(!empty($user['Subscription'])){
                        echo date('M d,Y',strtotime($user['Subscription'][0]['from_date'])).' - '.date('M d,Y',strtotime($user['Subscription'][0]['to_date'])); 
                    }
                    ?>&nbsp;</td>-->
                    <td><?php echo h($user['User']['email_address']); ?>&nbsp;</td>
                    <td><?php if ($user['User']['activated'] == 1) { ?> <img src="<?php echo $this->webroot; ?>img/success-01-128.png" style="height:30px;" /><?php } else { ?><img src="<?php echo $this->webroot; ?>img/cross-512.png" style="height:30px;" /><?php } ?>&nbsp;</td>

                    <td style="width: 9%">
                        <?php
                        echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-success btn-xs', 'escape' => false));
                        ?>
                        <?php
                        echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                        ?>
                        <?php
                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $user['User']['id']));
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <p>
            <?php
            echo $this->Paginator->counter(array(
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));
                ?>	</p>
                <div class="paging">
                    <?php
                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                    ?>
                </div>
            </div>

            <style>
                .title a{
                    color: #fff !important;
                }
            </style>

            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <script>
        (function ($) {
            var total_check = $("input[name='user_id[]']").length;

            $('#sel_all').click(function () {
                if ($(this).is(':checked')) {
                    $('input[name="user_id[]"]').prop('checked', true);
                } else {
                    $('input[name="user_id[]"]').prop('checked', false);
                }

            });

            $("input[name='user_id[]']").click(function () {
                var check_count = $("input[name='user_id[]']:checked").length;
                if (check_count == total_check) {
                    $('#sel_all').prop('checked', true);
                } else {
                    $('#sel_all').prop('checked', false);
                }
            });

            $('#bulk_action').click(function () {
                var action_type = $('#action_type').val();
                if (action_type != '') {

                    var user_ids = [];
                    $("input[name='user_id[]']:checked").each(function () {
                        user_ids.push($(this).val());
                    });
                    var size = user_ids.length;
                    if (size != 0) {

                        $.ajax({
                            url: "<?php echo $this->webroot; ?>users/bulkAction",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                action_type: action_type,
                                user_ids: user_ids
                            },
                            success: function (result) {
                                if (result.Ack == 1) {
                                //alert(result.res);
                                location.reload();
                            }
                        }
                    });
                    }
                    else {
                        alert('No User Is selected');
                    }
                }
                else {
                    alert('No Action selected');
                }
            });

$('#export_csv').click(function () {
    var plan_id = $('#plan_id').val();
    if(plan_id!='')
    {
        $("#error_div").html('');
        $("#csv_check").show();
        $("#selected_plan_id").val(plan_id);
    }
    else
    {
        $("#csv_check").hide();
        $("#error_div").html('<font color="red">Please Select Plan</font>');
        return false;
    }
});

})(jQuery);
$(document).ready(function () {
    $('input[name="user_id[]"]').prop('checked', false);
    $('#sel_all').prop('checked', false);
    /*$('#Searchuserfrm').submit(function (e) {
        alert('i am here');
        e.preventDefault();
        var emailaddress=$('#email_address').val();
        <?php
        if($user_type!='' && $email!='')
        {
        ?>
        var locationhref='https://www.studilmu.com/admin/normal_users/index/<?php echo $user_type; ?>/<?php echo $email; ?>';
        <?php
        }
        else if($user_type!='')
        {
         ?>
                        var locationhref='https://www.studilmu.com/admin/normal_users/index/<?php echo $user_type; ?>;
         <?php
            
        }
        ?>
                    window.location.href=locationhref;

            });*/
    $( "#Searchuserfrm" ).submit(function( event ) {
 var emailaddress=$('#email_address').val();
 if(emailaddress!='')
 {
        var locationhref='https://www.studilmu.com/admin/normal_users/index/<?php echo $user_type; ?>/'+emailaddress;
    }
    else
    {
         var locationhref='https://www.studilmu.com/admin/normal_users/index/<?php echo $user_type; ?>';
        }
                   window.location.href=locationhref;
  event.preventDefault();
});
});
</script> 
</div>
</div>
</div>