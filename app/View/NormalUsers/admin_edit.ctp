<div class="row-fluid">
	<div class="span12">
		<div class="widget green">
			<!-- <div class="widget-title">
				<h4><i class="icon-reorder"></i>Edit Application</h4>
				<span class="tools">
				<a href="javascript:;" class="icon-chevron-down"></a>
				<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div> -->
			<div class="widget-body">
<div class="users form">
    <?php echo $this->Form->create('User', array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit User'); ?></legend>
        <?php
        echo $this->Form->input('hidpw', array('type' => 'hidden', 'value' => $this->request->data['User']['user_pass']));
        echo $this->Form->input('id');
        echo $this->Form->input('first_name',array('required'=>'required'));
        echo $this->Form->input('last_name',array('required'=>'required'));
        echo $this->Form->input('email_address',array('required'=>'required'));
      ?>
         <div class="input select">
        <label for="state_id">User Type:</label>
        <select name="data[User][admin_type]" class="selectbox2" onchange="planShow(this.value);">
                   
                 <option value="2" <?php if($this->request->data['User']['admin_type']==2){ ?>selected="selected" <?php } ?>>Student</option>
                  <option value="1" <?php if($this->request->data['User']['admin_type']==1){ ?>selected="selected" <?php } ?>>Instructor</option>     
                   
             </select>
        </label>
        </div>
        <div class="input select membership_div" <?php if($this->request->data['User']['admin_type']==1){?> style="display:none;"<?php } ?>>
        <label for="state_id">Membership Plan:</label>
             <select name="data[User][membership_plan_id]" class="selectbox2" >
                    <option value="" >Select Plan</option>
                    <?php foreach($membership_plan_ids as $plan) {?>
                    <option value="<?php echo $plan['MembershipPlan']['id']; ?>" <?php if($plan['MembershipPlan']['id']==$this->request->data['User']['membership_plan_id']){ ?>selected="selected" <?php } ?>>
                        <?php echo $plan['MembershipPlan']['title']." - ".$plan['MembershipPlan']['duration']." ".$plan['MembershipPlan']['duration_in']; ?></option>
                    <?php } ?>
             </select>
        </label>
        </div>
        <?php
        echo $this->Form->input('address',array('label'=>'Street Address'));
        echo $this->Form->input('biography',array('id'=>'UserBiography'));
        // echo $this->Form->input('language_preference',array('options'=>$languages));
        //echo $this->Form->input('roles');
        echo $this->Form->input('site_notification');
        // echo $this->Form->input('is_baclisted');
        ?>
        <!-- <div class="input select">
            <label for="state_id">External Links:</label>
            <div class="clear-fix">
            Website Link: <input name="data[User][website_link]" maxlength="255" value=""  type="text"></div>
            <div class="clear-fix">
            Facebook Link: <input name="data[User][facebook_link]" maxlength="255" value=""  type="text"></div>
            <div class="clear-fix">
            Google+ Link: <input name="data[User][google_link]" maxlength="255" value=""  type="text"></div>
            <div class="clear-fix">
            Twitter Link: <input name="data[User][twitter_link]" maxlength="255" value=""  type="text"></div>
            <div class="clear-fix">
            Linkedin Link: <input name="data[User][linkedin_link]" maxlength="255" value=""  type="text"></div>
            <div class="clear-fix">
            Youtube Link: <input name="data[User][youtube_link]" maxlength="255" value=""  type="text"></div>
        </div> -->
        <?php 
        echo $this->Form->input('Phone_number',array('required'=>'required'));
        echo $this->Form->input('activated', array('label' => 'Activity Status'));
        if (!isset($this->request->data['UserImage']['0']['id'])) {
            echo $this->Form->input('userimage_id', array('type' => 'hidden', 'default' => ''));
        } else {
            echo $this->Form->input('userimage_id', array('type' => 'hidden', 'default' => $this->request->data['UserImage']['0']['id']));
        }
        
        // echo $this->Form->input('role', array('label' => 'Role', 'options' => $roles));
        
        echo $this->Form->input('image', array('type' => 'file'));
        ?>
        <div>
            <?php
            if (isset($this->request->data['User']['user_image']) and ! empty($this->request->data['User']['user_image'])) {
                ?>
                <img alt="" src="<?php echo $this->webroot; ?>user_images/<?php echo $this->request->data['User']['user_image']; ?>" style=" height:80px; width:80px;">
                <?php
            } else {
                ?>
                <img alt="" src="<?php echo $this->webroot; ?>user_images/default.png" style=" height:80px; width:80px;">

            <?php } ?>
        </div> 
      <!--   <div>
            <?php
            if (isset($this->request->data['UserImage']['0']['originalpath']) and ! empty($this->request->data['UserImage']['0']['originalpath'])) {
                ?>
                <img alt="" src="<?php echo $this->webroot; ?>user_images/<?php echo $this->request->data['UserImage']['0']['originalpath']; ?>" style=" height:80px; width:80px;">
                <?php
            } else {
                ?>
                <img alt="" src="<?php echo $this->webroot; ?>user_images/default.png" style=" height:80px; width:80px;">

            <?php } ?>
        </div> -->
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script> -->
<script>
    // var autocomplete = new google.maps.places.Autocomplete($("#UserAddress")[0], {});

    // google.maps.event.addListener(autocomplete, 'place_changed', function() {
    //     var place = autocomplete.getPlace();
    //     console.log(place.address_components);
    // });
</script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'UserBiography',
    {
    width: "95%"
    });

$(document).ready(function(){
    $("#country_id").change(function(){
        var country_id = $(this).val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>states/ajaxStates",
            type: 'post',
            dataType: 'json',
            data: {
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#state_id').html(result.html);
                } else {
                    $('#state_id').html(result.html);
                }
            }
        });
    });

    $("#state_id").change(function(){
        var state_id = $(this).val();
        var country_id = $("#country_id").val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>cities/ajaxCities",
            type: 'post',
            dataType: 'json',
            data: {
                s_id:state_id,
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#city_id').html(result.html);
                } else {
                    $('#city_id').html(result.html);
                }
            }
        });
    });
});

function planShow(userType)
{
  if(userType==1)
  {
   $('.membership_div').hide();   
  }
  else
  {
    $('.membership_div').show();    
  }
}
</script>
</div>
		</div>
	</div>
</div>
