<div class="users index">
    <h2 style="width:100%;float:left;"><?php echo __('User'); ?></h2>
    <form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">   
        <table style=" border:none;" class="data-table">
            <tr>
                <td width="12%">User Type :</td> 
                    <td width="20%"><select name="user_type" id="user_type" class="selectbox2">
                        <option value="" >Select Option</option>
                        <option value="1" <?php echo (isset($user_type) && $user_type == '1') ? 'selected' : ''; ?>>Instructor</option>
                        <option value="2" <?php echo (isset($user_type) && $user_type == '2') ? 'selected' : ''; ?>>Student</option>
                    </select></td>

                    <td><input type="submit" name="search" value="Search">
                </td>
            </tr> 
            
            <tr>
            <a href="<?php echo($this->webroot); ?>admin/dashboards/index" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a> 
            <a href="<?php echo($this->webroot); ?>admin/normal_users/add" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New User</a>  
            </tr>   
        </table>
    </form>    
    <table class="table table-striped table-bordered bulk-action" id="sample_1">
        <tr>
            <td width="11%">Bulk Action :</td> 
                <td width="20%"><select name="action_type" id="action_type" class="selectbox2">
                    <option value="" >Select Option</option>
                    <option value="1" >Delete</option>
                    <option value="2" >Approve</option>
                    <option value="3" >Disapprove</option>
                </select>
				</td>
				<td>
                <button id="bulk_action" type="button" class="btn btn-primary">Action</button>
            </td>
        </tr> 

    </table>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="width:10%;" ><input style="margin-top: 4px;" type="checkbox" id="sel_all" value="1"/>Select All</th>
            <th>SN<?php //echo $this->Paginator->sort('id');       ?></th>
            <th><?php echo $this->Paginator->sort('first_name'); ?></th>
            <th style="width: 9%"><?php echo $this->Paginator->sort('last_name'); ?></th>
            <th>Profile</th>
            
            <th style="width: 9%"><?php echo $this->Paginator->sort('admin_type', 'User Type'); ?></th>
           <th><?php echo $this->Paginator->sort('membership_plan_id', 'Membership type'); ?></th>
            <th>Member Since  - Until</th>
            <th style="width:9%;" ><?php echo $this->Paginator->sort('email_address', 'Email'); ?></th>
            <th style="width: 9%"><?php echo $this->Paginator->sort('activated', 'Activation Status'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php
        $UserCnt = 0;
        $uploadImgPath = WWW_ROOT . 'user_images';
        foreach ($users as $user):
            //print_r($user);die;
            $UserCnt++;
            ?>
            <tr>
                <td>
                    <input type="checkbox" class="check" name="user_id[]" value="<?php echo ($user['User']['id']); ?>">
                </td>
                <td><?php echo $UserCnt; //echo h($user['User']['id']);       ?>&nbsp;</td>
                <td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>		
                <td><?php
                    $per_profile_img = isset($user['UserImage']['0']['originalpath']) ? $user['UserImage']['0']['originalpath'] : '';
                    if ($per_profile_img != '' && file_exists($uploadImgPath . '/' . $per_profile_img)) {
                        $ImgLink = $this->webroot . 'user_images/' . $per_profile_img;
                    } else {
                        $ImgLink = $this->webroot . 'user_images/default.png';
                    }
                    echo '<img src="' . $ImgLink . '" alt="" height="70px" width="70px"/>';
                    ?></td>
              
                <td><?php
                    if ($user['User']['admin_type'] == 1) {
                        echo 'Instructor';
                    }
                    if ($user['User']['admin_type'] == 2) {
                        echo 'Student';
                    }
                    ?>&nbsp;</td>

                <td><?php echo $user['MembershipPlan']['title']; ?>&nbsp;</td>
                <td>
                    <?php 
                    if(!empty($user['Subscription'])){
                    echo date('M d,Y',strtotime($user['Subscription'][0]['from_date'])).' - '.date('M d,Y',strtotime($user['Subscription'][0]['to_date'])); 
                    }
                    ?>&nbsp;</td>
                <td><?php echo h($user['User']['email_address']); ?>&nbsp;</td>
                <td><?php if ($user['User']['activated'] == 1) { ?> <img src="<?php echo $this->webroot; ?>/img/success-01-128.png" style="height:30px;" /><?php } else { ?><img src="<?php echo $this->webroot; ?>/img/cross-512.png" style="height:30px;" /><?php } ?>&nbsp;</td>

                <td style="width: 9%">
                    <?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-success btn-xs', 'escape' => false));
                    ?>
                    <?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                    ?>
                    <?php
                    echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $user['User']['id']));
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>

<style>
    .title a{
        color: #fff !important;
    }
</style>

<script>
    (function ($) {
        var total_check = $("input[name='user_id[]']").length;

        $('#sel_all').click(function () {
            if ($(this).is(':checked')) {
                $('input[name="user_id[]"]').prop('checked', true);
            } else {
                $('input[name="user_id[]"]').prop('checked', false);
            }

        });

        $("input[name='user_id[]']").click(function () {
            var check_count = $("input[name='user_id[]']:checked").length;
            if (check_count == total_check) {
                $('#sel_all').prop('checked', true);
            } else {
                $('#sel_all').prop('checked', false);
            }
        });

        $('#bulk_action').click(function () {
            var action_type = $('#action_type').val();
            if (action_type != '') {

                var user_ids = [];
                $("input[name='user_id[]']:checked").each(function () {
                    user_ids.push($(this).val());
                });
                var size = user_ids.length;
                if (size != 0) {

                    $.ajax({
                        url: "<?php echo $this->webroot; ?>users/bulkAction",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            action_type: action_type,
                            user_ids: user_ids
                        },
                        success: function (result) {
                            if (result.Ack == 1) {
                                //alert(result.res);
                                location.reload();
                            }
                        }
                    });

                }
                else {
                    alert('No User Is selected');
                }
            }
            else {
                alert('No Action selected');
            }
        });
    })(jQuery);
    $(document).ready(function () {
        $('input[name="user_id[]"]').prop('checked', false);
        $('#sel_all').prop('checked', false);
    });
</script> 
</div>
</div>
</div>