<div class="row-fluid">
	<div class="span12">
		<div class="widget green">
			<!-- <div class="widget-title">
				<h4><i class="icon-reorder"></i>Add Application</h4>
				<span class="tools">
				<a href="javascript:;" class="icon-chevron-down"></a>
				<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div> -->
			<div class="widget-body">
<div class="users form">
<?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
	<?php
        
        echo $this->Form->input('first_name',array('required'=>'required'));
        echo $this->Form->input('last_name',array('required'=>'required'));
        echo $this->Form->input('email_address', array('type' => 'email','required'=>'required'));
        echo $this->Form->input('user_pass', array('type' => 'password','required'=>'required'));
        
       // echo $this->Form->input('video_limit');
        //echo $this->Form->input('video_size',array('type'=>'number','label'=>'Video Size[ in MBs ]'));
        //echo $this->Form->input('external_link',array('placeholder'=>'Like facebook or linkedin profile link'));
        // echo $this->Form->input('country',array('id'=>'country_id','required'=>'required'));
        ?>
        <div class="input select">
        <label for="state_id">User Type:</label>
        <input type="hidden" name="data[User][admin_type]" class="selectbox2" value="<?php echo $type; ?>" />
<!--        <select name="data[User][admin_type]" class="selectbox2" onchange="planShow(this.value);">
                   
                 <option value="2">Student</option>
                  <option value="1">Instructor</option>     
                   
             </select>-->
        </label>
        </div>
        
         <div class="input select membership_div">
        <label for="state_id">Membership Plan:</label>
             <select name="data[User][membership_plan_id]" class="selectbox2">
                    <option value="" >Select Plan</option>
                    <?php foreach($membership_plan_ids as $plan) {?>
                    <option value="<?php echo $plan['MembershipPlan']['id']; ?>" >
                        <?php echo $plan['MembershipPlan']['title']." - ".$plan['MembershipPlan']['duration']." ".$plan['MembershipPlan']['duration_in']; ?></option>
                    <?php } ?>
             </select>
        </label>
        </div>
       
        <?php 
        // echo $this->Form->input('state');
        // echo $this->Form->input('city');
        // echo $this->Form->input('zip',array('required'=>'required'));
        echo $this->Form->input('address',array('label'=>'Street Address'));
        echo $this->Form->input('Phone_number',array('required'=>'required'));
//        echo $this->Form->input('role',array(
//    'type' => 'select',
//    'empty' => 'Select Role'// <-- Shows as the first item and has no value
//));
        // echo $this->Form->input('language_preference',array('options'=>$languages));
        echo $this->Form->input('site_notification');
        // echo $this->Form->input('is_baclisted');
        ?>
        <!-- <div class="input select">
            <label for="state_id">External Links:</label>
            <ul class="social_links">
            	<li><label>Website Link:</label> <input name="data[User][website_link]" maxlength="255" value=""  type="text"></li>
            	<li><label>Facebook Link:</label> <input name="data[User][facebook_link]" maxlength="255" value=""  type="text"></li>
            <li><label>Google+ Link:</label> <input name="data[User][google_link]" maxlength="255" value=""  type="text">		</li>
            <li><label>Twitter Link:</label> <input name="data[User][twitter_link]" maxlength="255" value=""  type="text"></li>
            <li><label>Linkedin Link:</label> <input name="data[User][linkedin_link]" maxlength="255" value=""  type="text"></li>
            <li><label>Youtube Link:</label> <input name="data[User][youtube_link]" maxlength="255" value=""  type="text"> 		</li>
            </ul>
        </div> -->
        <?php 
        //echo $this->Form->input('has_marketplace');
        echo $this->Form->input('status');
        echo $this->Form->input('image',array('type'=>'file','required'=>'required'));
        echo $this->Form->input('biography',array('id'=>'UserBiography'));
        // echo $this->Form->input('role',array('options'=>$roles));
        echo $this->Form->input('admin_type', array('type' => 'hidden', 'value' => 2));
	?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script> -->
 <script>
//     var autocomplete = new google.maps.places.Autocomplete($("#UserAddress")[0], {});

//     google.maps.event.addListener(autocomplete, 'place_changed', function() {
//         var place = autocomplete.getPlace();
//         console.log(place.address_components);
//     });
</script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'UserBiography',
    {
    width: "95%"
    });

    $(document).ready(function(){
    $("#country_id").change(function(){
        var country_id = $(this).val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>states/ajaxStates",
            type: 'post',
            dataType: 'json',
            data: {
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#state_id').html(result.html);
                } else {
                    $('#state_id').html(result.html);
                }
            }
        });
    });

    $("#state_id").change(function(){
        var state_id = $(this).val();
        var country_id = $("#country_id").val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>cities/ajaxCities",
            type: 'post',
            dataType: 'json',
            data: {
                s_id:state_id,
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#city_id').html(result.html);
                } else {
                    $('#city_id').html(result.html);
                }
            }
        });
    });
});

function planShow(userType)
{
  if(userType==1)
  {
   $('.membership_div').hide();   
  }
  else
  {
    $('.membership_div').show();    
  }
}
</script>
</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
	CKEDITOR.replace( 'Contentcontent',
	{
	width: "95%"
	});
</script>
