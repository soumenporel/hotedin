<!--  inner  slider   -->
  <?php //echo '<pre>'; print_r($post_related); echo '</pre>'; 
  ?>

<link href="<?php echo $this->webroot.'video/';?>css/video-js.css" rel="stylesheet" type="text/css">
<!-- <link href="<?php echo $this->webroot.'video/';?>css/bootstrap.css" rel="stylesheet"> -->


<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
<script>videojs.options.flash.swf = "video-js.swf";</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="<?php echo $this->webroot.'video/';?>videoStyle.css" rel="stylesheet" type="text/css">
<style>
  ul.angle-double-right {
    list-style-type: none;
}
.big-title {
    padding-top: 16px;
    /* font-size: 19px; */
}
</style>

  <section class="min-bnr">
    <div class="container">
      <h1 class="zilla text-white font-weight-light pt-3 mb-1"><?php if(isset($post_dtls['Post']['post_title']) && $post_dtls['Post']['post_title'] !='') { echo $post_dtls['Post']['post_title']; } ?></h1>
      <ul class="text-white bnr-ul">
        <li><a href="<?php echo $this->webroot.'posts/course_filter/'.$post_dtls['Category']['slug']; ?>"><?php if(isset($post_dtls['Category']['category_name']) && $post_dtls['Category']['category_name'] !='') { echo $post_dtls['Category']['category_name']; } ?></a></li>
        <li><a href="javascript:void(0)"><?php if(isset($post_dtls['Post']['post_title']) && $post_dtls['Post']['post_title'] !='') { echo $post_dtls['Post']['post_title']; } ?></a></li>
      </ul>
    </div>
  </section>
<section class="mainWarp">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
      <div class="warp-details">
      <div class="pl-3 pt-3 pr-3 pb-1">
        <ul class="text-white bnr-ul">
          <li><a href="<?php echo $this->webroot.'posts/course_filter/'.$post_dtls['Category']['slug']; ?>"><span><?php if(isset($post_dtls['Category']['category_name']) && $post_dtls['Category']['category_name'] !='') { echo $post_dtls['Category']['category_name']; } ?></span></a></li>
          <li><a href="javascript:void(0)"><span><?php if(isset($post_dtls['Post']['post_title']) && $post_dtls['Post']['post_title'] !='') { echo $post_dtls['Post']['post_title']; } ?></span></a></li>
        </ul>
      </div>
      <div class="detailsHdr pt-0">
          <h2><?php if(isset($post_dtls['Post']['post_title']) && $post_dtls['Post']['post_title'] !='') { echo $post_dtls['Post']['post_title']; } ?></h2>
        <button class="backBtn"> <img src="<?php echo $this->webroot; ?>img/forward-arrow.png" alt=""></button>
      </div>

      <!--For Video start-->

      <?php 
        if(isset($post_dtls['PostVideo'][0]['originalpath']) && $post_dtls['PostVideo'][0]['originalpath'] !=''){

          $file = $post_dtls['PostVideo'][0]['originalpath'];
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          $videoExt = array('mp4', 'flv', 'mkv');
          if(in_array(strtolower($ext), $videoExt)) {
            $file_name = trim($post_dtls['PostVideo'][0]['originalpath'], $ext);
          ?>
          <div class="video-warp">
            <!-- <img src="<?php echo $this->webroot; ?>img/details.png" alt=""> -->
            <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-button" controls preload="auto" width="100%" height="488px">
              poster="<?php echo $srcIMG; ?>"
              data-setup='{ "playbackRates": [0.5, 1, 1.5, 2] }' >
              <source  src="<?php echo $this->webroot; ?>img/post_video/<?php echo $post_dtls['PostVideo'][0]['originalpath']; ?>" type="video/<?php echo $ext; ?>">
              <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>webm" type='video/webm'>
              <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>ogv" type='video/ogg' />
              <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track>
              <!-- Tracks need an ending tag thanks to IE9 -->
              <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track>
              <!-- Tracks need an ending tag thanks to IE9 -->
                      
              <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                          
            </video>
          </div>
      <?php } } else { ?>
          <div class="video-warp">
            <img src="<?php if(isset($post_dtls['PostImage'][0]['originalpath']) && $post_dtls['PostImage'][0]['originalpath'] !='') { echo $this->webroot.'img/post_img/'.$post_dtls['PostImage'][0]['originalpath']; } else{echo 'http://111.93.169.90/team4/studilmu/img/details.png'; } ?>" alt="">
          </div>
      <?php  } ?>

      <!--For Video End-->

        <div>
        <div class="top-courses top-courses-details">
          <ul class="nav nav-tabs justify-content-center border-0 d-sm-flex d-block" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" href="#tab1" role="tab" data-toggle="tab">Overview</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tab2" role="tab" data-toggle="tab"> Transcript</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tab3" role="tab" data-toggle="tab">View Offline</a>
              </li>
          </ul>
          <div class="tab-content tab-content-details">
            <div role="tabpanel" class="tab-pane fade in active show" id="tab1">
              <div class="media  details-author">
                <img class="d-flex align-self-center mr-3" src="<?php if(isset($post_dtls['User']['UserImage'][0]['originalpath']) && $post_dtls['User']['UserImage'][0]['originalpath'] !='') { echo $this->webroot.'user_images/'.$post_dtls['User']['UserImage'][0]['originalpath']; } else{echo $this->webroot.'user_images/default.png';} ?>" alt="Generic placeholder image" width="70">
                <div class="media-body">
                  <div class="name">
                    <span>Author:</span>
                    <?php if(isset($post_dtls['User']['first_name']) && $post_dtls['User']['first_name'] !='') { echo $post_dtls['User']['first_name'].' '.$post_dtls['User']['last_name']; } ?>
                  </div>
                  <ul class="dateLst">
                    <li>
                      <div  class="name">
                        <span>Updated:</span>
                         <?php if(isset($post_dtls['Post']['modified_date']) && $post_dtls['Post']['modified_date'] !='') { echo date('m-d-Y',strtotime($post_dtls['Post']['modified_date'])); } ?>
                      </div>
                    <li>
                      <div class="name">
                        <span>Released:</span>
                          <?php if(isset($post_dtls['Post']['post_date']) && $post_dtls['Post']['post_date'] !='') { echo date('m-d-Y',strtotime($post_dtls['Post']['post_date'])); } ?>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="details-author-msg">
                <p><?php if(isset($post_dtls['Post']['post_description']) && $post_dtls['Post']['post_description'] !='') { echo $post_dtls['Post']['post_description']; } ?></p>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab2">
            <div class="body-scrolling">
                  <!--<h2>About This Course </h2>-->
                  
                  <?php  $course_goals = json_decode($post_dtls['Post']['course_goals']); ?>
                  
                  <?php if($post_dtls['Post']['course_goals']!=''){ ?>
                  <div class="big-title">
                      <h4 class="related-title">
                          <span>What are the requirements?</span>
                      </h4>
                  </div>
                  <ul class="angle-double-right">
                      <?php
                      foreach ($course_goals->need_to_know as $key => $value1) { ?>
                      <li> <?php echo $value1;?></li>
                      <?php } ?>
                  </ul>
                  <div class="big-title">
                      <h4 class="related-title">
                          <span>What am I going to get from this course?</span>
                      </h4>
                  </div>
                  <ul class="angle-double-right">
                      <?php
                      foreach ($course_goals->will_be_able as $key => $value2) { ?>
                      <li><?php echo $value2;?></li>
                      <?php } ?>
                  </ul>
                   <div class="big-title">
                      <h4 class="related-title">
                          <span>Who is the target audience?</span>
                      </h4>
                  </div>
                  <ul class="angle-double-right">
                      <?php
                      foreach ($course_goals->target_student as $key => $value3) { ?>
                      <li> <?php echo $value3;?></li>
                      <?php } ?>
                  </ul>
                  <?php } ?>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade pt-3" id="tab3"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div>
          </div>
            <div>
                <?php if(isset($post_dtls['PostAudio'][0]['originalpath']) && $post_dtls['PostAudio'][0]['originalpath'] !='') {?>
                <?php //print_r($post_dtls['PostAudio'][0]['originalpath']); ?>
                <audio controls>
   
                <source src="<?php echo WWW_ROOT ?>img/post_audio/<?php echo $post_dtls['PostAudio'][0]['originalpath']; ?>" type="audio/mpeg">

                </audio>
                <?php } ?>
            </div>
        </div>
        </div>
      </div>
      <div class="curriculum">
      	<h5>Curriculum</h5>
      	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingOne">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          Collapsible Group Item #1
		        </a>
		      </h4>
		    </div>
		    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
		      <div class="panel-body">
		         <div class="row">
		         	<div class="col-md-4"><a class="fa fa-play"></a> Lecture 1 :</div>
		         	<div class="col-md-6">The Attention Rule Preview The Course</div>
		         	<div class="col-md-2">00:01:09</div>
		         </div>   
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingTwo">
		      <h4 class="panel-title">
		        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		          Collapsible Group Item #2
		        </a>
		      </h4>
		    </div>
		    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		      <div class="panel-body">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingThree">
		      <h4 class="panel-title">
		        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
		          Collapsible Group Item #3
		        </a>
		      </h4>
		    </div>
		    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
		      <div class="panel-body">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		</div>
      </div>
      </div>
      <div class="col-md-3">
      <div class="warp-details">
        <h3 class="RelatedCourses">Related Courses</h3>
        
        <?php if(!empty($post_related)){
          foreach ($post_related as $key => $val) { //pr($val); ?>
        <a href="<?php echo $this->webroot.'posts/details/'.$val['Post']['slug']; ?>" style="text-decoration: none;"> 
        <figure class="course-box course-box-details">
          <div class="course-video">
            <div class="dtls-sideImg">
                <?php if(!empty($val['PostImage']))
                {
                  
                ?>
              <img src="<?php echo $this->webroot.'img/post_img/'.$val['PostImage'][0]['originalpath']; ?>" alt="No Image">
                <?php } else { ?>
              No Image Availabale
                <?php } ?>
            </div>
          </div>
          <figcaption class="bg-white p-3">
              <h5><?php echo $val['Post']['post_title']; ?></h5>
              <p class="font-14 mb-2"><?php echo (substr($val['Post']['post_description'],0,80).'....'); ?></p>
              <p class="font-14 text-gray mb-2"><?php echo $val['User']['first_name'].' '.$val['User']['last_name']; ?></p>
              <p class="font-12 mb-2">
                  <span class="text-blue">
                      <i class="ion-ios-star"></i>
                      <i class="ion-ios-star"></i>
                      <i class="ion-ios-star"></i>
                      <i class="ion-ios-star"></i>
                      <i class="ion-ios-star"></i>
                  </span>
                  <span><span class="font-weight-bold">4.6</span> (2,102) </span>
              </p>
              <!-- <p class="c-price d-flex justify-content-between align-items-center"><span class="o-price">$155.00</span> <span class="d-price">$115.00</span></p> -->
          </figcaption>
        </figure>
        </a> 
        <?php } } ?>

      </div>
        <!-- Go to www.addthis.com/dashboard to customize your tools --> 
        <div class="addthis_inline_share_toolbox_by0y" data-description="<?php echo $post_dtls['Post']['post_description']; ?>" data-title="<?php echo $post_dtls['Post']['post_title']; ?>"  ></div>
        <?php //echo $this->webroot.'posts/details/'.$post_dtls['Post']['slug']; ?>
      </div>
    </div>
  </div>
</section>

<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-592e49ff7817a982"></script> 