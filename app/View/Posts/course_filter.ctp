  <!--  inner  slider   -->

  <section class="home-slider inner-banner">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <!-- <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol> -->
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <img class="" src="<?php echo $this->webroot; ?>img/inner-banner/1.jpg" alt="First slide">
                <div class="carousel-caption">
                    <div class="row text-left align-items-center">
                      <div class="col-xl-5 col-lg-7 text-white">
                          <h2 class="zilla"><?php echo $categoryDetails['Category']['category_name']; ?></h2>
                          <!-- <h6 class="font-14 font-weight-light">All library  |  Web development</h6> -->
                          <p class="font-14 font-weight-light"><?php echo $categoryDetails['Category']['category_description']; ?> </p>
                          <!-- <a href="#" class="btn btn-primary text-uppercase mt-lg-3">Enquire Now</a> -->
                      </div>
                  </div>
                </div>
            </div>

          </div>
        </div>
  </section>


  <!--   top courses -->

  <section class="py-5 top-courses">
      <div class="container">
          <ul class="nav nav-tabs justify-content-center border-0 d-sm-flex d-block" role="tablist">
              <?php // echo '<pre>'; print_r($topCategories); echo '</pre>'; ?>
              <?php $cati=1; foreach ($topCategories as $key => $Category) { ?>
              <li class="nav-item">
                <a class="nav-link <?= ($categoryDetails['Category']['id'] == $key) ? 'active' : ''; ?>" href="#tab<?php echo $cati; ?>" role="tab" data-toggle="tab"><?php echo $Category ?></a>
              </li>
              <?php $cati++; } ?>
              <!-- <li class="nav-item">
                <a class="nav-link" href="#tab2" role="tab" data-toggle="tab">Strategy</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tab3" role="tab" data-toggle="tab">Customer Service</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tab4" role="tab" data-toggle="tab">State</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tab5" role="tab" data-toggle="tab">Others</a>
              </li> -->
          </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <?php $catj = 1; foreach ($topCategories as $key => $cat) {?>
                  <div role="tabpanel" class="tab-pane fade in <?= ($categoryDetails['Category']['id'] == $key) ? 'active show' : ''; ?> " id="tab<?php echo $catj; ?>">
                      <div class="row mt-3">
                          <?php
                          $blogs = $this->requestAction(array('controller'=>'homepages','action'=>'postdata',$key));
                          if(!empty($blogs)){
                            foreach ($blogs as $key => $blog) {

                            ?>
                                  <?php //echo '<pre>'; print_r($blog); echo '</pre>'; ?>
                                  <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
                                   <a href="<?php echo $this->webroot.'posts/details/'.$blog['Post']['slug']; ?>" style="text-decoration: none;color: #2e2e2e;">
                                      <figure class="course-box bg-white newCourse-box">
                                          <div class="course-video p-relative">
                                              <img width="100%" height="215" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $blog['PostImage']['0']['originalpath']; ?>" ></img>
                                              <div class="previewCourse">
                                                  <div class="text-center">
                                                    <span class="font-30"><i class="fa fa-play" aria-hidden="true"></i></span>
                                                    <div class="font-weight-bold font-12">Preview this course</div>
                                                  </div>
                                              </div>
                                          </div>
                                          <figcaption class="bg-white p-3">
                                              <h5><?php echo $blog['Post']['post_title']; ?></h5>
                                              <!-- <p class="font-14 mb-2"><?php echo substr($blog['Post']['post_description'],0,150) ; ?></p> -->
                                              <h6 class="font-weight-light font-12"><?php echo $blog['User']['first_name'].' '.$blog['User']['last_name']; ?></h6>
                                              <p class="font-12 mb-2">
                                                  <span class="text-blue">
                                                      <i class="ion-ios-star"></i>
                                                      <i class="ion-ios-star"></i>
                                                      <i class="ion-ios-star"></i>
                                                      <i class="ion-ios-star"></i>
                                                      <i class="ion-ios-star"></i>
                                                  </span>
                                                  <span><span class="font-weight-bold mr-1">4.6</span> ( 2,102 views) </span>
                                              </p>
                                            <!--   <p class="c-price d-flex justify-content-between align-items-center">
                                                <span class="o-price">$155.00</span> <span class="d-price">$115.00</span>
                                              </p>-->
                                          </figcaption>
                                      </figure>
                                      </a>
                                  </div>
                            <?php
                            }
                          }else{ ?>
                              <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
                                No Course Under this Category
                              </div>
                    <?php } ?>

                      </div>
                      
                      <?php if(!empty($blogs)){ ?>
                <div class="row">
          	<div class="col-lg-12 text-center"><a class="btn btn-primary btnPart mt-4" href="<?php echo $this->webroot; ?>posts/course_filter/<?php echo $blogs[0]['Category']['slug']; ?>#catcourseListing">Browse All
                <i class="fa fa-caret-up"></i></a></div>
             </div> 
              <?php } ?>
                  </div>
              <?php $catj++; } ?>
      </div>
  </section>


  <!--   search  result   -->

  <section class="bg-faded py-5" id="catcourseListing">
      <div class="container">
          <div class="row">
              <div class="hidden-xs hidden-sm col-md-2">
                  <div class="mb-4">
                      <p class="font-weight-bold mb-1 font-14">Topics</p>
                      <ul class="font-14">
                          <?php foreach ($panelCategories as $keyPanel => $catPanel) {  ?>
                            <li class="list-unstyled d-flex justify-content-between font-12 mb-1">
                                <a href="<?php echo $this->webroot;?>posts/course_filter/<?php echo $catPanel['Category']['slug']; ?>"><span class="mr-2" style="color:#000;"><?php echo $catPanel['Category']['category_name']; ?> <span style="color:#9999A5">( <?php echo count($catPanel['Post']); ?> )</span></span></a></li>
                          <?php } ?>

                      </ul>
                  </div>
                  <!-- <div class="mb-4">
                      <p class="font-weight-bold">Topics</p>
                      <ul class="font-14">
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Leadership</span><span>(21547)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Strategy</span><span>(247)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Customer Service</span><span>(47)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>State </span><span>(21547)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Others  </span><span>(547)</span></li>
                      </ul>
                  </div>
                  <div class="mb-4">
                      <p class="font-weight-bold">Topics</p>
                      <ul class="font-14">
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Leadership</span><span>(21547)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Strategy</span><span>(247)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Customer Service</span><span>(47)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>State </span><span>(21547)</span></li>
                          <li class="list-unstyled d-flex justify-content-between mb-2"><span>Others  </span><span>(547)</span></li>
                      </ul>
                  </div> -->
              </div>
              <div class="col-xs-12 col-md-10 course-list-box">
                     <?php
                  if(!empty($allPosts)){
                      ?>
                <div class="search-result">
                  <div class="search_result_hdr">
                    <div class="row">
                      <div class="col-lg-6 d-flex align-items-center">
                        <span class="font-weight-bold font-14 mr-2"><?php echo 'showing '.count($allPosts).' of '.count($categoryDetails['Post']).' courses'; ?></span>
                        <span class="font-14 mr-3">  </span>
<!--                        <span class="font-weight-bold font-14 mr-">386</span>
                        <span class="font-14 mr-2"> Video Tutorials</span>-->
                      </div>
                      <div class="col-lg-6">
                        <div class="downCorseulsortBy d-flex align-items-center justify-content-end">
                          <div class="font-14">Sort By : </div>
                          <select class="form-control font-14" id='order_filter' onchange="searchAjax();">
                            <option value="newest">release date (newest first)</option>
                             <option value="oldest">release date (oldest first)</option>
                            <option value="titleAsc">course title (a-z)</option>
                            <option value="titleDesc">course title (z-a)</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="downCorseul filter_content">
                 <?php

                 foreach ($allPosts as $key => $blog) {
                    // print_r($blog);die;
                     if($blog['Post']['skill_level']==1)
                     {
                         $level='Beginer';
                     }
                     elseif($blog['Post']['skill_level']==2)
                     {
                         $level='Intermediate';
                     }
                     else
                     {
                        $level='Expert';

                     }
                 ?>
                 <li>
                    <a href="<?php echo $this->webroot.'posts/details/'.$blog['Post']['slug']; ?>" class="d-flex">
                      <div class="downCorseimg mr-3 p-relative">
                        <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $blog['PostImage']['0']['originalpath']; ?>" alt="">
                        <div class="preview">preview</div>
                      </div>
                      <div class="downCorseimgMenu">
                        <div>
                          <h3><?php echo $blog['Post']['post_title']; ?> </h3>
                          <div class="font-12"><?php echo substr($blog['Post']['post_description'],0,150) ; ?> </div>
                          <div class="mt-2">
<!--                            <span class="font-12 mr-2">1h 46m</span>-->
                              <span class="font-12 mr-2"><?php echo $level; ?></span>
                              <span class="font-12 mr-2"><span><?php echo date('M d,Y',strtotime($blog['Post']['post_date'])); ?></span></span>
                              <span class="font-12 mr-2">Views <span><?php echo $blog['Post']['click_count']; ?></span></span>
                          </div>
                        </div>
                      </div>
                    </a>
                  </li>
                <?php
                 }
                  ?>

                </ul>
                    
                    <p class="pagingPart">
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
                </div>
                  <?php  } ?>
              </div>
          </div>
              </div>
          </div>
      </div>
  </section>

  <script type="text/javascript">
        function searchAjax()
        {
          //alert(1);

          var order_filter1 = $("#order_filter").val();
          //alert(order_filter1);
          $.ajax({
            type: "POST",             // Type of request to be send, called as method
            dataType: 'json',
            url: "<?php echo $this->webroot;?>Posts/sortcoursefilter",
            data: {
                cat_id:'<?php echo $cat_id; ?>',
                order_filter:order_filter1
            },
            beforeSend: function () {
            },
            success: function (result) {
             //console.log(result);
                    //alert(result.ACK);

                    $('#searchCount').html(result.searchCount);
                    $('.filter_content').empty();
                    $('.filter_content').append(result.html);
            }
        });
        }

  </script>
