<style>
/*.list_number1{
	text-align: center;
}*/
.event-section .single-event {
    border-bottom: 1px solid #777;
}
.event-section .hvr-float-shadow {
    margin-bottom: 0px;
}
</style>
<section class="inner_banner1">
	<div class="inner_shadow">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row justify-content-center">
						
						<div class="col-lg-8 text-center py-lg-4">
							<div class="clearfix" id="sliderCTA">
								<?php echo $topContent['CmsPage']['page_description']; ?>
								
								<form action="/team4/studilmu/posts/add_course" name="add_course" method="post">

								  <div class="form-group c-search">
								    <input type="text" class="form-control" name="post_title" id="" style="padding:10px 15px;" required>
								  </div>
								    <button type="submit" class="btn btn-primary">Create Course</button>	
								    <div class="clearfix"></div>
								    <!--<p>Start by entering the title of a course:</p>		-->				
							   </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</section>

<?php echo $midContent['CmsPage']['page_description']; ?>

<section class="ready_to_create">
	<div class="container" style="padding-bottom:40px;">
		<div class="row justify-content-center">
			<div class="col-lg-4 text-center py-lg-4">
				<?php echo $bottomContent['CmsPage']['page_description']; ?>
				<div class="form-group">
				<form action="/team4/studilmu/posts/add_course" name="add_course" method="post">
				  <input type="text" class="form-control" name="post_title" id="usr" placeholder="e.g. learn photoshop cc from scrath" required>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Create Course</button>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
	(function() {

    var quotes = $(".quotes");
    var quoteIndex = -1;
    
    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(2000)
            .delay(2000)
            .fadeOut(2000, showNextQuote);
    }
    
    showNextQuote();
    
})();

$(document).ready(function () {
	$('.panel-group .panel-collapse.in').prev().addClass('active');
    $('.panel-group').on('show.bs.collapse', function(e) {
        $(e.target).prev('.panel-heading').addClass('active');
    }).on('hide.bs.collapse', function(e) {
        $(e.target).prev('.panel-heading').removeClass('active');
    });
});
</script>  