<script src="<?php echo $this->webroot . 'js/angular.min.js'; ?>"></script>
<style type="text/css">
	ul {
		list-style: outside none none;
		margin: 0;
		padding: 0;
	}
	.cfi-forms-list {
		margin-top: 15px;
		border: 0 none;
		padding: 0;
		position: relative;
		width: 100%;
	}
	.fx {
		-moz-box-flex: 1;
		flex: 1 1 0;
		min-width: 1px;
	}
	.df, .fx-flex {
		display: flex;
	}
	.cfi-sortables-list .add-forms-container .cfi-add, .cfi-forms-list .add-forms-container .cfi-add {
		flex: 1 0 33%;
		min-width: initial;
		padding: 5px;
	}
	
	.cfi-sortables-list .cfi-warning .content, .cfi-forms-list .cfi-warning .content, .cfi-sortables-list .cfi-add .content, .cfi-forms-list .cfi-add .content {
		cursor: pointer;
		display: block;
		font-size: 15px;
		font-weight: normal;
	}
	.cfi-sortables-list .cfi-warning > a, .cfi-forms-list .cfi-warning > a, .cfi-sortables-list .cfi-add > a, .cfi-forms-list .cfi-add > a {
		-moz-user-select: none;
		background-color: #ffffff;
		background-image: none;
		border: 1px solid rgba(0, 0, 0, 0.2);
		border-radius: 2px;
		color: #0e600e;
		cursor: pointer;
		display: inline-block;
		font-size: 15px; 
		font-weight: normal;
		line-height: 1.35135;
		margin-bottom: 0;
		padding: 11px 12px;
		text-align: center;
		vertical-align: middle;
		white-space: nowrap;
	}
	.cfi-sortables-list .add-forms-container .cfi-add, .cfi-forms-list .add-forms-container .cfi-add {
		flex: 1 0 33%;
		min-width: initial;
		padding: 5px;
	}
	.cfi-sortables-list .cfi-chapter.cfi-add, .cfi-forms-list .cfi-chapter.cfi-add, .cfi-sortables-list .cfi-chapter.cfi-add-form, .cfi-forms-list .cfi-chapter.cfi-add-form {
		margin: 0;
	}
	.cfi-sortables-list .add-forms-container, .cfi-forms-list .add-forms-container {
		margin: 0 -5px 20px;
	}
	.cfi-sortables-list .cfi-warning > a:hover, .cfi-forms-list .cfi-warning > a:hover, .cfi-sortables-list .cfi-add > a:hover, .cfi-forms-list .cfi-add > a:hover {
		background-color: #f7f7f7;
		border-color: rgba(0, 0, 0, 0.2);
	}
	.cfi-chapter-container {
		background-color: #f4f4f4;
		border: 1px solid rgba(0, 0, 0, 0.55);
		margin: 60px 0 20px;
	}
	.cfi-sortables-list .cfi-chapter.cfi-sortable, .cfi-forms-list .cfi-chapter.cfi-sortable {
		margin: 10px;
	}
	.cfi-sortables-list .cfi-sortable .cfi-content, .cfi-forms-list .cfi-sortable .cfi-content {
		/*cursor: move;*/
		height: 54px;
	}
	.cfi-sortables-list .editing, .cfi-forms-list .editing, .cfi-sortables-list .cfi-content, .cfi-forms-list .cfi-content {
		overflow: hidden;
		position: relative;
		z-index: 1;
	}
	.cfi-sortables-list .add-forms-container .cfi-add-form, .cfi-forms-list .add-forms-container .cfi-add-form {
		margin: 0 5px;
	}
	.cfi-sortables-list .cfi-sortable.cfi-lecture, .cfi-forms-list .cfi-sortable.cfi-lecture, .cfi-sortables-list .cfi-add-form.cfi-lecture, .cfi-forms-list .cfi-add-form.cfi-lecture, .cfi-sortables-list .cfi-sortable.cfi-quiz, .cfi-forms-list .cfi-sortable.cfi-quiz, .cfi-sortables-list .cfi-add-form.cfi-quiz, .cfi-forms-list .cfi-add-form.cfi-quiz {
		position: relative;
	}
	.cfi-sortables-list .closed.editing, .cfi-forms-list .closed.editing, .cfi-sortables-list .closed.cfi-content, .cfi-forms-list .closed.cfi-content {
		display: none;
	}
	.cfi-sortables-list .add-forms-container .cfi-add-form.opened, .cfi-forms-list .add-forms-container .cfi-add-form.opened {
		position: relative;
		width: 100%;
	}
	.cfi-sortables-list .add-forms-container .cfi-add-form, .cfi-forms-list .add-forms-container .cfi-add-form {
		margin: 0 5px;
	}
	.content.ui-state-default {
		display: block;
	}
	.action-buttons .fa.fa-pencil, .action-buttons .fa.fa-trash {
	    display: none;
	}
	.content.ui-state-default:hover .fa.fa-pencil, .content.ui-state-default .action-buttons:hover .fa.fa-pencil {
		display: inline-block;
		cursor: pointer;
	}
	.content.ui-state-default:hover .fa.fa-trash, .content.ui-state-default .action-buttons:hover .fa.fa-trash {
		display: inline-block;
		cursor: pointer;
	}
	.edit-handle.fa.fa-pencil, .delete-handle.fa.fa-trash {
	    display: none;
	}
	.content.ui-state-default:hover .edit-handle.fa.fa-pencil, .content.ui-state-default:hover .delete-handle.fa.fa-trash {
		display: inline-block;
		cursor: pointer;
	}
	.action-buttons .fa.fa-bars:hover {
		cursor: move;
	}
	.cfi-sortables-list .gray-container .header, .cfi-forms-list .gray-container .header {
	    -moz-border-bottom-colors: none;
	    -moz-border-left-colors: none;
	    -moz-border-right-colors: none;
	    -moz-border-top-colors: none;
	    background: #ffffff none repeat scroll 0 0;
	    border-color: rgba(0, 0, 0, 0.25) rgba(0, 0, 0, 0.25) -moz-use-text-color;
	    border-image: none;
	    border-style: solid solid none;
	    border-width: 1px 1px medium;
	    color: #353535;
	    float: right;
	    font-weight: bold;
	    margin: -34px 30px 0 0;
	    padding: 7px 35px 6px 10px;
	    position: relative;
	}
	.cfi-sortables-list .gray-container .main, .cfi-forms-list .gray-container .main {
		border-top: 1px solid rgba(0, 0, 0, 0.25);
		color: #555;
		font-size: 13px;
		margin: 0 10px 10px;
	}
	.cfi-sortables-list .question-list-wrapper .question-list, .cfi-forms-list .question-list-wrapper .question-list {
	    border-top: 1px dotted #ccc;
	    counter-reset: numList;
	    margin-top: 15px;
	    padding-top: 10px;
	}

	.cfi-sortables-list .question-list-wrapper .question-list li::before, .cfi-forms-list .question-list-wrapper .question-list li::before {
	    content: counter(numList, decimal) ".";
	    counter-increment: numList;
	    float: left;
	    font-weight: bold;
	    margin-right: 3px;
	}
	.cfi-sortables-list .question-list-wrapper .question-list li, .cfi-forms-list .question-list-wrapper .question-list li {
	    font-size: 13px;
	    overflow: hidden;
	    padding: 5px 0;
	    position: relative;
	    transition: background-color 1s ease 0s;
	}
	.cfi-sortables-list .question-list-wrapper .question-list .prompt, .cfi-forms-list .question-list-wrapper .question-list .prompt {
	    color: #555;
	    float: left;
	    font-size: 13px;
	    max-width: 440px;
	}
	.ellipsis {
	    display: block;
	    overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	}
	.cfi-sortables-list .question-list-wrapper .question-list span, .cfi-forms-list .question-list-wrapper .question-list span {
	    color: #999999;
	    float: left;
	    margin-left: 5px;
	    max-width: 110px;
	}
	.question-list li:hover .edit-handle, .question-list li:hover .delete-handle {
		display: inline-block;
		cursor: pointer;
	}
</style>
<section class="profileedit" ng-app="myApp" ng-controller="myCtrl">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<?php 
				/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="profile_second_part curriculum_part">
					<div class="clearfix">
						<div class="">
							<h2 class="" style="display: inline-block; margin: 7px; width: auto;">Curriculum</h2>
							<div class="pull-right">
								<a class="btn btn-info btn-lg" target="_blank" href="">Preview</a>
							</div>
						</div>
					</div>

					<!-- Lecture template Start -->
					<script type="text/ng-template" id="lectureTemplate">

							<div class="cfi-content" ng-class="isEditLectureTitleShown(subRow) ? 'closed' : ''">
								<span class="content ui-state-default">
									<span class="cfi-item-type">{{ subRow.type }}</span>
									<span class="cfi-item-number">1:</span>
									<span class="cfi-item-title" data-purpose="lecture-title-shown">{{ subRow.title }}</span>
									<i class="edit-handle fa fa-pencil" ng-click="toggleEditLectureTitle(subRow)"></i>
									<span class="js-delete-handle delete-handle fa fa-trash" ng-click="deleteLectureFunction(subRow, $event)"></span>
									<span class="action-buttons pull-right">
										<a href="javascript:void(0)" data-purpose="add-content" class="add-content btn btn-primary btn-sm container-switch floating m0-10 m5-30-xs m5-30-md">
											+ Add Content
										</a>
										<span class="cfi-item-previewable" style="display: none;">(Preview enabled)</span>
										<i class="fa  container-switch" ng-class="isIsHiddenShown(subRow) ? 'fa-chevron-up' : 'fa-chevron-down'" ng-click="toggleIsHidden(subRow)"></i>
										<i class="fa fa-bars sort-handle"></i>
									</span>
								</span>
							</div>
							<div class="gray-container " ng-if="isIsHiddenShown(subRow) && !isAddResourceTabShown(subRow)">
								<ul class="more">
									<li class="desc">
										<div class="lec-description" 
											ng-class="isShowEditContainerShown(subRow) ? 'hidden' : ''">
											<p ng-click="toggleShowEditContainer(subRow)">{{ subRow.description }}</p>
										</div>
										<div class="edit-container" 
											ng-class="!isShowEditContainerShown(subRow) ? 'hidden' : ''">
											<form class="clearfix">
												<div class="form-group">
													<textarea class="form-control" name="subRow.description" ng-model="subRow.description" placeholder="Add a description. Include what students will be able to do after completing the lecture."></textarea>
													<input type="hidden" ng-model="subRow.lectureID" value="{{ subRow.lectureID }}" />
												</div>
												<div class="form-group pull-right">
													<button type="button" class="btn btn-info" ng-click="toggleShowEditContainer(subRow)">Cancel</button>
													<button type="button" class="btn btn-info" ng-click="saveLectureDescription(subRow.lectureID, subRow.description, subRow)">Save</button>
												</div>
											</form>
										</div>
									</li>
									
									<li class="supplementary ">

										<div class="downloadable-materials" ng-if="subRow.haveDownloadableFile">
											<h4>Downloadable materials</h4>
											<ul class="sup downloadable">
												<li class="iconed File" data-type="asset" data-id="" 
													ng-repeat="downloadable in subRow.downloadablefile">
													<span class="fa fa-download"></span>
													<span class="title">{{ downloadable.downloadable_file }}</span><!-- <span> (8.4 kB)</span> -->
													<a href="javascript:void(0)" class="fa fa-trash-o delete-sup"></a>
												</li>
											</ul>
										</div>

										<div class="external-resources" ng-if="subRow.haveExternalLink">
											<h4>External Resources</h4>
											<ul class="sup external">
												<li class="iconed ExternalLink" data-type="asset" data-id="" 
													ng-repeat="external in subRow.externallink">
													<span class="udi udi-external-link"></span>
													<span class="title">{{ external.link }}</span>
													<a href="javascript:void(0)" class="fa fa-trash-o delete-sup"></a>
												</li>
											</ul>
										</div>

									</li>

								</ul>
								<p class="bottom-btns" ng-class="isShowEditContainerShown(subRow) ? 'hidden' : ''">
									<a class="btn btn-default desc-btn add" href="javascript:void(0)" 
										ng-if="!subRow.description" 
										ng-click="toggleShowEditContainer(subRow)">
										Add Description
									</a>

									<a class="btn btn-default add-supplementary" href="javascript:void(0)" 
										ng-click="openAddResourceTab(subRow)">
										Add Resources
									</a>
								</p>
								<div id="replace-with-video-confirm-6342600" class="modal fade replace-with-video-confirm" tabindex="-1" role="dialog">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span class="udi udi-close"></span>
												</button>
												<h4 class="modal-title" id="myModalLabel">Update this lecture to video?</h4>
											</div>
											<div class="modal-body">
												The content of this lecture will be added as a downloadable resource, which you may choose to remove. You will be prompted to add a video file and re-publish as a video lecture.
											</div>
											<div class="modal-footer">
												<div class="modal-footer--inner">
													<button type="button" class="btn btn-danger replace-with-video" data-dismiss="modal">
														Convert To Video
													</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="gray-container has-header tablet-m0" 
								ng-if="isAddResourceTabShown(subRow)">
								<div class="header">
									Add Resources
									<a class="close-btn container-switch fa fa-close" href="javascript:void(0)" 
										ng-click="toggleAddResourceTab(subRow)"></a>
								</div>

								<div class="asset-create-container" data-purpose="loader">
									<div class="main create-asset-container add-supplementary ud-assetcreator" data-type="File">
										
										<div my-tabs id="tabs">
											<ul>
												<li><a href="#downloadable-tabs">Downloadable File</a></li>
												<li><a href="#library-tabs">Add From Library</a></li>
												<li><a href="#external-tabs">External Resource</a></li>
												<li><a href="#sourcecode-tabs">Source Code</a></li>
											</ul>
											<div id="downloadable-tabs">
												<form>
													<div class="form-group">
														<input type="file" class="form-control" 
															data-postid="<?php echo $post_dtls['Post']['id']; ?>" 
															data-lectureid="{{ subRow.lectureID }}" 
															file-model="uploadDownloadableFile" 
															ng-model="downloadableFile" />
													</div>
												</form>

											</div>
											<div id="library-tabs">
												<table class="table table-striped">
													<thead>
														<tr>
															<th class="col-md-4">
																<span class="ng-scope">Filename</span>
															</th> 
															<th class="col-md-2">
																<span translate="">
																	<span class="ng-scope">Type</span>
																</span>
															</th> 
															<th class="col-md-3 orderable asc"> 
																<span>Date Uploaded</span>
															</th> 
															<th class="col-md-2">
																<span translate="">
																	<span>Status</span>
																</span>
															</th> 
															<th class="col-md-1"></th> 
														</tr>
													</thead>
													<tbody>
														<tr data-id="">
															<td>
																<div>External resource</div>
															</td>
															<td>ExternalLink</td>
															<td>01/17/2017</td>
															<td>
																<div ng-switch="" on="asset.status">
																	<div ng-switch-when="1" class="ng-scope">
																		<span translate="">
																			<span class="ng-scope">Success</span>
																		</span>
																	</div>
																</div>
															</td>
															<td class="center-align no-action">
															<i class="fa fa-trash"></i>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<div id="external-tabs">
												<form class="clearfix" ng-submit="uploadExternalLink(subRow)">
													<div class="form-group">
														<lable>Title</lable>
														<input type="text" name="externalLinkTitle" ng-model="subRow.externalLinkTitle" class="form-control" placeholder="Enter Title" />
													</div>
													<div class="form-group">
														<lable>URL</lable>
														<input type="text" name="externalLinkUrl" ng-model="subRow.externalLinkUrl" class="form-control" placeholder="Enter URL" />
													</div>
													<button type="submit" class="btn btn-default pull-right">Add Link</button>
												</form>
											</div>
											<div id="sourcecode-tabs">
												<form>
													<div class="form-group">
														<input type="file" class="form-control" />
													</div>
												</form>
											</div>
										</div>

									</div>
								</div>

							</div>

							<div class="cfi-lecture cfi-edit-form editing " ng-class="isEditLectureTitleShown(subRow) ? 'opened' : 'closed'">
					            <span class="content ui-state-default">
					                <span class="cfi-item-type">Lecture</span>
					                <span class="cfi-item-number">1:</span>
					                <span class="cfi-item-title">
										<form>
											<div class="form-group">
												<label class="col-md-2 lecture_label"></label>
												<span class="col-md-10">
													<input name="lecture_title" class="form-control form_part form_text" ng-model="subRow.title" value="" placeholder="Enter a title" type="text" />
													<div class="number">75</div>
												</span>
											</div>
												
											<div class="form-group pull-right cancel_button">
												<span class="col-md-12">
													<button type="button" class="btn btn-primary" ng-click="toggleEditLectureTitle(subRow)" style="margin-right: 7px;">Cancel</button>
													<button type="submit" class="btn btn-primary" ng-click="changeLectureTitle({id: subRow.lectureID, title: subRow.title}, subRow)">Save Lecture</button>
												</span>
											</div>
												
											<div class="clearfix"></div>
										</form>
					                </span>
					            </span>
					        </div>


					</script>
					<!-- Lecture template end -->

					<!-- Quiz template Start -->
					<script type="text/ng-template" id="quizTemplate">
						<div class="cfi-content" ng-class="isEditQuizTitleDescShown(subRow) ? 'closed' : ''">
							<span class="content ui-state-default">
								<span class="cfi-item-type">{{ subRow.type }}</span>
								<span class="cfi-item-number">1:</span>
								<span class="cfi-item-title" data-purpose="lecture-title-shown">{{ subRow.title }}</span>
								<i class="edit-handle fa fa-pencil" ng-click="toggleEditQuizTitleDesc(subRow)"></i>
								<span class="js-delete-handle delete-handle fa fa-trash" ng-click="deleteQuizFunction(subRow, $event)"></span>
								<span class="action-buttons pull-right">
									<a href="javascript:void(0)" 
										data-purpose="add-content" 
										class="add-content btn btn-primary btn-sm container-switch floating m0-10 m5-30-xs m5-30-md" 
										ng-class="isSelectQuestionTypeShown(subRow) || (subRow.haveQuestion != '') ? 'hidden' : ''" 
										ng-click="toggleSelectQuestionType(subRow)">
										+ Add Questions
									</a>
									<span class="cfi-item-previewable" style="display: none;">(Preview enabled)</span>
									<i class="fa  container-switch" 
										ng-class="isShowQuestionsShown(subRow) ? 'fa-chevron-up' : 'fa-chevron-down'" 
										ng-click="toggleShowQuestions(subRow)" 
										ng-if="subRow.haveQuestion != ''"></i>
									<i class="fa fa-bars sort-handle"></i>
								</span>
							</span>
						</div>

						<div class="gray-container has-header select-question-type tablet-m0" 
							ng-if="isSelectQuestionTypeShown(subRow) && !isAddMultiChoiceQuestShown(subRow)">
							<div class="header sub-menu">
							  	Select question type
							  	<i class="close-btn container-switch fa fa-close" 
							  		ng-click="toggleSelectQuestionType(subRow)"></i>
							</div>
							<div class="main types sub-menu">
								<a class="lecture-icons quiz-multi" 
									data-type="multiple-choice" href="javascript:void(0)" 
									ng-click="addQuizeQuestion(subRow)">
									<span>
										Multiple Choice
									</span>
								</a>
							</div>
						</div>

						<div class="gray-container has-header tablet-m0" 
							ng-if="isAddMultiChoiceQuestShown(subRow)">
							<div class="header">
								Add Multiple Choice
								<a class="fa fa-close close-btn container-switch" href="javascript:void(0)" 
								ng-click="closeAddMultiChoiceQuest(subRow)"></a>
							</div>
							<div class="assessment-create-container">
								<form class="">
									<div class="form-group" id="form-item-question">
										<label>Question:*:</label>
										<textarea class="form-control" placeholder="Add your question herer" 
											ng-model="subRow.questions[questionIndex].question"></textarea>
									</div>
									
									<div id="form-item-answers">
										<label>Answers:*:</label>

										<div class="form-group clearfix" ng-repeat="option in subRow.questions[questionIndex].options">
											<div class="col-xs-1">
												<input name="finalanswer" type="radio" ng-model="subRow.questions[questionIndex].answer" value="{{option.answer}}" />
											</div>
											<div class="col-xs-10">
												<div class="row">
													<textarea class="form-control" placeholder="Add an answer" 
														ng-model="option.answer"></textarea>
													<input type="text" class="form-control" placeholder="Explain why this is or isn't the best answer." 
														ng-model="option.explanation" />
												</div>
											</div>
											<div class="col-xs-1">
												<i class="fa fa-trash"></i>
											</div>
										</div>

									</div>
									
									<input type="hidden" name="questionid" ng-model="subRow.newQuestion.id" />

									<div class="form-group">
										<button type="button" class="btn btn-default" 
											ng-click="addEditQuestion(questionIndex, subRow)">Submit</button>

									</div>
								</form>
							</div>
							
						</div>

						<div class="gray-container quiz-instructor-view" 
							ng-if="isShowQuestionsShown(subRow) && !isSelectQuestionTypeShown(subRow)">
							<div class="question-list-wrapper ">
								<div class="top fxac fxdc-xs">
									<h4>Questions</h4>
									<a href="javascript:void(0)" class="btn btn-sm btn-default add mb10-xs mt10-xs add-question" 
										ng-click="toggleSelectQuestionType(subRow)">+ New Question</a>
								</div>

								<ul class="question-list ud-jq-sortable ui-sortable" data-placeholder="ui-state-highlight" data-handle="span.sort-handle" data-tolerance="pointer">
									<li data-id="{{ question.id }}" 
										data-type="assessment" 
										data-assessmenttype="multiple-choice" 
										ng-repeat="question in subRow.questions">
										<div class="ellipsis prompt">{{ question.question }}</div>

										<span>multiple-choice</span>
										<span class="edit-handle  fa fa-pencil" 
											ng-click="editQuizeQuestion($index, subRow)"></span>
										<span class="delete-handle  fa fa-trash" 
											ng-click="deleteQuizeQuestion($index, subRow)"></span>
										<!-- <span class="sort-handle fa fa-bars pull-right"></span> -->
									</li>
								</ul>

							</div>

						</div>

						<div class="cfi-quiz cfi-edit-form editing " ng-class="isEditQuizTitleDescShown(subRow) ? 'opened' : 'closed'">
				        	<span class="content ui-state-default">
				        		<span class="cfi-item-type">Quiz</span>
				        		<span class="cfi-item-number">1:</span>
				        		<span class="cfi-item-title">
				        			<form>
										<div class="form-group">
											<label class="col-md-2 lecture_label"></label>
											<span class="col-md-10">
												<input name="lecture_title" class="form-control form_part form_text" ng-model="subRow.title" value="" placeholder="Enter a title" type="text" />
												<div class="number">75</div>
											</span>
										</div>
										
										<div class="form-group">
											<label class="col-md-2 lecture_label"></label>
											<span class="col-md-10">
												<textarea name="lecture_title" class="form-control form_part form_text" ng-model="subRow.description" value="" placeholder="Enter Description" type="text"></textarea>
											</span>
										</div>

										<div class="form-group pull-right cancel_button">
											<span class="col-md-12">
												<button type="button" class="btn btn-primary" ng-click="toggleEditQuizTitleDesc(subRow)" style="margin-right: 7px;">Cancel</button>
												<button type="submit" class="btn btn-primary" 
													ng-click="changeQuizTitleDesc({id: subRow.lectureID, title: subRow.title, description: subRow.description}, subRow)">Save Quiz</button>
											</span>
										</div>
											
										<div class="clearfix"></div>
									</form>
				        		</span>
				        	</span>
				        </div>

					</script>
					<!-- Quiz template End -->

					<div style="padding: 45px 0px;">
						<div style="padding-bottom: 20px;"> Start putting together your course by creating sections, lectures and practice (quizzes and coding exercises) below. </div>
						
						<div>
							<ul>
								<li>
									<ul class="cfi-sortables-list">
										<li id="{{ row.lessonID }}" ng-repeat="row in totalStructure">
											<ul class="cfi-chapter-container ui-sortable" data-id="{{ row.lessonID }}">
												<li id="item-chapter-{{ row.lessonID }}" class="cfi-chapter cfi-sortable published" data-lessonid="{{ row.lessonID }}" data-class="chapter" data-canaddcontent="0">
													<div class="cfi-content" 
														ng-show="!isEditSectionTitleShown(row)">
														<span class="content ui-state-default">
															<span class="cfi-item-type">Section</span>
															<span class="cfi-item-number">1:</span>
															<span class="cfi-item-title fwb" data-purpose="chapter-title-shown">{{ row.title }}</span>
															<span class="edit-handle fa fa-pencil" 
																data-purpose="chapter-title-edit" 
																ng-show="!isEditSectionTitleShown(row)" 
																ng-click="toggleEditSectionTitle(row)"></span>
															<span class="js-delete-handle delete-handle fa fa-trash" 
																data-id="{% verbatim %}1539256{% endverbatim %}" 
																data-type="{% verbatim %}{% endverbatim %}"
																ng-show="!isEditSectionTitleShown(row)"></span>
															<span class="action-buttons">
																<span class="sort-handle outer"></span>
															</span>
														</span>
													</div>
													<div class="cfi-chapter cfi-edit-form editing " ng-class="isEditSectionTitleShown(row) ? 'opened' : 'closed'">
														<span class="content ui-state-default">
															<span class="cfi-item-type">Section</span>
															<span class="cfi-item-number">1:</span>
															<span class="cfi-item-title">
																<form>
																	<div class="form-group">
																		<label class="col-md-2 lecture_label"></label>
																		<span class="col-md-10">
																			<input name="section_title" class="form-control form_part form_text" ng-model="row.title" value="{{ row.title }}" placeholder="Enter a title" type="text">
																			<div class="number">75</div>
																		</span>
																	</div>
																	<div class="form-group">
																		<label class="col-md-2 lecture_label"></label>
																		<span class="col-md-10">
																			<textarea name="section_description" ng-model="row.description" class="form-control">{{ row.description }}</textarea>
																			<input type="hidden" ng-model="row.lessonID" name="lesson_id">
																		</span>
																	</div>
																	<div class="form-group pull-right cancel_button">
																		<button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleEditSectionTitle(row)">Cancel</button>
																		<button type="submit" class="btn btn-primary" ng-click="changeSectionTitleDesc(row.lessonID, row.title, row.description, row)">Save Section</button>
																	</div>
																	<div class="clearfix"></div>
																</form>
															</span>
														</span>
													</div>
												</li>
												<li id="item-lecture-{{subRow.lectureID}}" 
													ng-repeat="subRow in row.lectures" class="cfi-sortable published" 
													ng-class="cfi-{{subRow.type | lowercase}}" 
													data-class="chapter" data-canaddcontent="1" 
													ng-include="subRow.type == 'Quiz' ? 'quizTemplate' : 'lectureTemplate'">
												</li>
												
											</ul>
										</li>

									</ul>
								</li>
								<li>
									<ul class="cfi-forms-list">
										<li>
											<ul class="add-forms-container">
												<div class="df fx">

													<li class="cfi-lecture cfi-add" ng-class="isAddLectureShown() || isAddQuizShown() ? 'hidden' : ''">
														<a data-purpose="add-lecture-button" class="content ui-state-default add-lecture-btn" ng-click="addLecture = !addLecture">
															Add Lecture
														</a>
													</li>

													<li class="cfi-lecture cfi-add-form editing " ng-class="isAddLectureShown() ? 'opened' : 'closed'">
														<form method="post">
															<div class="row">
																<label class="col-md-2 lecture_label">New Lecture</label>
																<span class="col-md-10">
																	<input type="text" name="lecture_title" ng-model="newLecture.title" class="form-control form_part form_text" placeholder="Enter a title" />
																	<!-- <input type="hidden" name="post_id" ng-model="newLecture.post_id" value="<?php echo $post_dtls['Post']['id']; ?>" /> -->
																	<div class="number">75</div>
																</span>
															</div>
															<div class="form-group pull-right cancel_button">
																<button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleAddLecture()">Cancel</button>
																<button type="button" class="btn btn-primary" ng-click="addLectureFunction(newLecture.title)">Add Lecture</button>
															</div>
															<div class="clearfix"></div>
														</form>
													</li>

													<li class="cfi-quiz cfi-add" ng-class="isAddLectureShown() || isAddQuizShown() ? 'hidden' : ''">
														<a data-purpose="add-quiz-button" ng-click="toggleAddQuiz()" class="content ui-state-default add-quiz-btn">
															Add Quiz
														</a>
													</li>

													<li class="cfi-quiz cfi-add-form editing" ng-class="isAddQuizShown() ? 'opened' : 'closed'">
														<span class="content ui-state-default">
															<span class="cfi-item-type"></span>
															<span class="cfi-item-title fx">
																<form method="post">
																	<div class="form-group">
																		<label class="col-md-2 lecture_label">New Quiz</label>
																		<span class="col-md-10">
																			<input type="text" name="quiz_title" ng-model="quiz.title" class="form-control form_part form_text" placeholder="Enter a title" />
																			<div class="number">75</div>
																		</span>
																	</div>
																	<div class="form-group">
																		<span class="col-md-10 col-md-offset-2">
																			<textarea name="quiz_title" ng-model="quiz.description" class="form-control" placeholder="Enter Description"></textarea>
																		</span>
																	</div>
																	<div class="form-group pull-right cancel_button">
																		<span class="col-md-12">
																			<button type="button" class="btn btn-primary" style="margin-right: 7px;" ng-click="toggleAddQuiz()">Cancel</button>
																			<button type="button" class="btn btn-primary" ng-click="addQuizFunction({title: quiz.title, description: quiz.description})">Add Quiz</button>
																		</span>
																	</div>
																	<div class="clearfix"></div>
																</form>
															</span>
														</span>
													</li>

												</div>

												<div class="df fx">
													<li class="cfi-coding-exercise cfi-add" ng-class="isAddLectureShown() || isAddQuizShown() ? 'hidden' : ''">
														<a data-purpose="add-coding-exercise-button" class="content ui-state-default add-coding-exercise-btn" href="/course/1038814/manage/coding-exercise/">
															Add Coding Exercise
														</a>
													</li>
												</div>

											</ul>
										</li>
										<li class="cfi-chapter cfi-add">
											<a class="content ui-state-default add-chapter-btn" data-purpose="add-chapter-button"> Add Section </a>
										</li>
									</ul>
								</li>
							</ul>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	var serializeData = function(obj, prefix) {
	  var str = [];
	  for(var p in obj) {
	    if (obj.hasOwnProperty(p)) {
	      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
	      str.push(typeof v == "object" ?
	        serialize(v, k) :
	        encodeURIComponent(k) + "=" + encodeURIComponent(v));
	    }
	  }
	  return str.join("&");
	};

	var app = angular.module('myApp', []);
	
	app.config(function ($httpProvider) {
	    // send all requests payload as query string
	    $httpProvider.defaults.transformRequest = function(data){
	        if (data === undefined) {
	            return data;
	        }
	        return serializeData(data);
	    };

	    // set all post requests content type
	    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	});

	app.service('lessonService', ['$http', '$q', function ($http, $q) {
	    
	        this.updateLectures = function (data) {
                var request = $http({
                	method: 'POST',		                	
  					url: '<?php echo $this->webroot;?>lessons/sprint_status_order_new',
  					data:  data
            	});
            	return request.then( handleSuccess, handleError );
	        };

	        this.getAll = function(post_id) {
	        	var request = $http({
                	method: 'GET',		                	
  					url: '<?php echo $this->webroot;?>posts/getAllCurriculum/' + post_id
            	});

            	return request.then( handleSuccess, handleError );
	        };
	    	
	    	function handleSuccess( response ) {
                return response.data;
            }

            function handleError( response ) {
                if (
                    ! angular.isObject( response.data ) ||
                    ! response.data.message
                    ) {
                    return $q.reject( "An unknown error occurred." );
                }
                // Otherwise, use expected error message.
                return $q.reject( response.data.message );
            }
	}]);

	app.controller('myCtrl', ['$rootScope', '$scope', '$http', 'lessonService', '$filter', function($rootScope, $scope, $http, lessonService, $filter) {
		
		$scope.totalStructure = [];

		$scope.initializePage = function() {
			lessonService.getAll('<?php echo $post_dtls['Post']['id']; ?>')
			.then(function(res) {
				if (res.ack == '1') {
					$scope.totalStructure = res.lessonsArray;
				} else {
					$scope.totalStructure = [];
				}
			});
		};
		
		$scope.initializePage();

		$scope.changeSectionTitleDesc = function(lessonid, title, description, row) {
            $http({
            	url: '<?php echo $this->webroot;?>lessons/ajaxEditLesson/'+lessonid,
            	method: 'POST',
            	data: {
            		'data[Lesson][title]': title,
            		'data[Lesson][description]': description
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		console.log(res.res);
            		$scope.toggleEditSectionTitle(row);
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.saveLectureDescription = function(lectureid, description, subRow){
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxEditLectureNew/'+lectureid,
            	method: 'POST',
            	data: {
            		'data[Lecture][description]': description,
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		console.log(res.res);
            		$scope.toggleShowEditContainer(subRow);
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.addLectureFunction = function(title) {
			
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxAddLecture/<?php echo $post_dtls['Post']['id']; ?>',
            	method: 'POST',
            	data: {
            		'data[lecture][title]': title,
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		console.log(res.res);
            		$scope.toggleAddLecture();
            		$scope.initializePage();
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.deleteLectureFunction = function(subRow, $event){
			var el = $event.target;
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxDeleteLectureNew',
            	method: 'POST',
            	data: {
            		'data[lecture_id]': subRow.lectureID,
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.ack == '1') {
            		angular.element(el).closest('.cfi-sortable').remove();
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.deleteQuizFunction = function(subRow, $event){
			var el = $event.target;
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxDeleteLectureNew',
            	method: 'POST',
            	data: {
            		'data[lecture_id]': subRow.lectureID,
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.ack == '1') {
            		angular.element(el).closest('.cfi-sortable').remove();
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.changeLectureTitle = function(obj, subRow){
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxEditLecture/'+obj.id,
            	method: 'POST',
            	data: {
            		'data[Lecture][title]': obj.title
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		$scope.toggleEditLectureTitle(subRow);            		
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.addQuizFunction = function(obj) {
			
			$http({
            	url: '<?php echo $this->webroot; ?>lectures/ajaxAddQuiz/<?php echo $post_dtls['Post']['id']; ?>',
            	method: 'POST',
            	data: {
            		'data[quiz][name]': obj.title,
            		'data[quiz][objective]': obj.description
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		console.log(res.res);
            		$scope.toggleAddQuiz();
            		$scope.initializePage();
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.changeQuizTitleDesc = function(obj, subRow) {
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxEditLecture/'+obj.id,
            	method: 'POST',
            	data: {
            		'data[Lecture][title]': obj.title,
            		'data[Lecture][description]': obj.description
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		$scope.toggleEditQuizTitleDesc(subRow);            		
            	} else {
            		console.log(res.res);
            	}
            });
		};

		$scope.questionIndex = '';

		$scope.addQuizeQuestion = function(subRow) {
			
			if(subRow.questions.length > 0) {
				$scope.questionIndex = subRow.questions.length;
				var newQuestion = {
					id: '',
					question: '',
					options: [
						{
							answer: '',
							explanation: ''
						},
						{
							answer: '',
							explanation: ''
						},
						{
							answer: '',
							explanation: ''
						},
						{
							answer: '',
							explanation: ''
						}
					],
					answer: ''
				};

				subRow.questions.push(newQuestion);
				//console.log(subRow.questions[$scope.questionIndex]);
			} else {
				$scope.questionIndex = 0;
				var newQuestion = {
					id: '',
					question: '',
					options: [
						{
							answer: '',
							explanation: ''
						},
						{
							answer: '',
							explanation: ''
						},
						{
							answer: '',
							explanation: ''
						},
						{
							answer: '',
							explanation: ''
						}
					],
					answer: ''
				};

				subRow.questions.push(newQuestion);
			}
			$scope.toggleAddMultiChoiceQuest(subRow);
		};

		$scope.editQuizeQuestion = function(index, subRow) {
			$scope.questionIndex = index;
			$scope.toggleAddMultiChoiceQuest(subRow);
			$scope.toggleShowQuestions(subRow);
		};

		$scope.addEditQuestion = function($index, subRow) {
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxAddQuizQuestion',
            	method: 'POST',
            	data: {
            		'lecture_id': subRow.lectureID,
            		'data[Question][id]': subRow.questions[$index].id,
            		'data[Question][question]': subRow.questions[$index].question,
            		'data[Question][options]': angular.toJson(subRow.questions[$index].options),
            		'quiz_ans': subRow.questions[$index].answer
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		console.log(res.res);
            		$scope.initializePage();
            	} else{
            		console.log(res.res);
            		subRow.questions.splice(index, $index);
            	}
            	
            });

            $scope.closeAddMultiChoiceQuest(subRow);
		}

		$scope.deleteQuizeQuestion = function($index, subRow) {
			$http({
            	url: '<?php echo $this->webroot;?>lectures/ajaxDeleteQuizQuestion',
            	method: 'POST',
            	data: {
            		'data[Question][id]': subRow.questions[$index].id
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		$scope.initializePage();
            	} else{
            		subRow.questions.splice(index, $index);
            	}
            	
            });
		};

		$scope.uploadDownloadableFile = function(file, attrs) {
	        var fd = new FormData();
	        fd.append('code_file', file[0]);
	        fd.append('lecture_id', attrs.lectureid);
	        fd.append('post_id', attrs.postid);
	        
	        $http({
	            method: "POST",
	            url: '<?php echo $this->webroot; ?>lectures_libraries/ajax_add_downloadable_file',
	            data: fd,
	            transformRequest: angular.identity,
	            headers: { 'Content-Type': undefined }
	        })
	        .success(function(data){
	            console.log(data);
	        });
	    };

	    $scope.uploadExternalLink = function(subRow) {
	    	$http({
            	url: '<?php echo $this->webroot; ?>lectures_libraries/ajax_add_external_resource',
            	method: 'POST',
            	data: {
            		'lecture_id': subRow.lectureID,
            		'post_id': '<?php echo $post_dtls['Post']['id']; ?>',
            		'title': subRow.externalLinkTitle,
            		'url': subRow.externalLinkUrl
            	}
            })
            .then(function(response){
            	var res = response.data;
            	if(res.Ack == '1') {
            		$scope.initializePage();
            	} else {
            		
            	}
            });
	    };

	    $scope.openAddResourceTab = function(subRow) {
	    	$http({
	    		url: '<?php echo $this->webroot; ?>lectures_libraries/get_lectures_libraries',
            	method: 'GET',
	    	})
	    	.then(function(response){
            	var res = response.data;
            	console.log(res);
            });
	    };

	    $scope.toggleAddResourceTab = function(subRow) {
	    	subRow.addResourceTab = !subRow.addResourceTab;
	    };

	    $scope.isAddResourceTabShown = function(subRow) {
	    	return subRow.addResourceTab;
	    };

		$scope.toggleEditLectureTitle = function(subRow) {
			subRow.editLectureTitle = !subRow.editLectureTitle;
		};
		$scope.isEditLectureTitleShown = function(subRow) {
			return subRow.editLectureTitle;
		};
		$scope.toggleEditSectionTitle = function(row) {
		    row.editSectionTitle = !row.editSectionTitle;
	  	};
	  	$scope.isEditSectionTitleShown = function(row) {
	    	return row.editSectionTitle;
	  	};
	  	
	  	$scope.toggleIsHidden = function(subRow) {
	    	subRow.isHidden = !subRow.isHidden;
	  	};
	  	$scope.isIsHiddenShown = function(subRow) {
	    	return subRow.isHidden;
	  	};
	  	$scope.toggleShowEditContainer = function(subRow) {
	    	subRow.showEditContainer = !subRow.showEditContainer;
	  	};
	  	$scope.isShowEditContainerShown = function(subRow) {
	    	return subRow.showEditContainer;
	  	};
	  	$scope.toggleShowEditDeleteLecture = function(subRow) {
	  		subRow.showEditDeleteLecture = !subRow.showEditDeleteLecture;
	  	};
	  	$scope.isShowEditDeleteLectureShown = function(subRow) {
	    	return subRow.showEditDeleteLecture;
	  	};
	  	$scope.toggleEditQuizTitleDesc = function(subRow) {
	  		subRow.editQuizTitleDesc = !subRow.editQuizTitleDesc;
	  	};
	  	$scope.isEditQuizTitleDescShown = function(subRow) {
	  		return subRow.editQuizTitleDesc;
	  	};
	  	$scope.toggleSelectQuestionType = function(subRow) {
	  		subRow.selectQuestionType = !subRow.selectQuestionType;
	  	}
	  	$scope.isSelectQuestionTypeShown = function(subRow) {
	  		return subRow.selectQuestionType;
	  	};
	  	$scope.toggleAddMultiChoiceQuest = function(subRow) {
	  		subRow.addMultiChoiceQuest = !subRow.addMultiChoiceQuest;
	  	}
	  	$scope.closeAddMultiChoiceQuest = function(subRow) {
	  		subRow.addMultiChoiceQuest = false;
	  		subRow.selectQuestionType = false;
	  	}
	  	$scope.isAddMultiChoiceQuestShown = function (subRow) {
	  		return subRow.addMultiChoiceQuest;
	  	};
	  	$scope.toggleShowQuestions = function(subRow) {
	  		subRow.showQuestions = !subRow.showQuestions;
	  	};

	  	$scope.isShowQuestionsShown = function(subRow) {
	  		return subRow.showQuestions;
	  	};

	  	$scope.toggleAddLecture = function() {
	    	$scope.addLecture = !$scope.addLecture;
	  	};
	  	$scope.isAddLectureShown = function() {
	    	return $scope.addLecture;
	  	};
	  	$scope.toggleAddQuiz = function() {
	    	$scope.addQuiz = !$scope.addQuiz;
	  	};
	  	$scope.isAddQuizShown = function() {
	    	return $scope.addQuiz;
	  	};
	}]);

	app.directive("uiSortable", ['lessonService', function(lessonService) {
	    return {
	      restrict: "C",
	      link: function(scope, element, attrs) {
	        element.sortable({
				connectWith: ".ui-sortable",
				handle: ".sort-handle",
				//items: "> li:gt(0)",
				create: function(event, ui) {
					updatePosition();
				},
				update: function(event, ui) {
					var values = element.sortable( "toArray" );
					lessonService.updateLectures(values);
					updatePosition();
				}
			});

			function updatePosition() {
				var els = angular.element('.cfi-chapter-container.ui-sortable');
				angular.forEach(els, function( el, i ){
				   angular.element(el).find('.cfi-chapter .cfi-item-number').text(i + 1 + ':');
				});
			}
	      }
	    }
	}]);

	app.directive('myTabs', function() {
	    return {
	        restrict: 'A',
	        link: function(scope, elm, attrs) {
	            var jqueryElm = $(elm[0]);
	            elm.tabs({
	            	activate: function(evt, ui){
		            	if (ui.newPanel.attr('id') === 'library-tabs') {
		            		console.log('Hi');
		            	}
		            }
	            });
	        }
	    };
	});

	app.directive('fileModel', function() {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var uploadDownloadableFile = scope.$eval(attrs.fileModel);

				element.bind('change', function() {
					scope.$apply(function() {
						var files = element[0].files;
						if (files) {
							uploadDownloadableFile(files, attrs);
						}
					});
				});

			}
		};
	});


	

</script>