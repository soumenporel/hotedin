<style>
.header{
    position:relative;
}

.inner_header{
    background:#0c2440;
}


</style>

<section class="profileedit">
	<div class="container">
		<div class="row" style="background:#f6f6f6; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
			<div class="col-md-3 col-sm-3" style="padding:0;">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
			</div>
			<?php
			if(!empty($post_dtls['Post']['automatic_message'])){
				$data = json_decode($post_dtls['Post']['automatic_message']);
				$welcome_message = $data->welcome_message;
				$congratulation_message = $data->congratulation_message;
			}
			else{
				$welcome_message = '';
				$congratulation_message = '';
			} 
			?>
			<div class="col-lg-9 col-md-9 col-12" style="padding: 0; border-left:#ddd solid 1px; border-bottom:#ddd solid 1px; ">
				<div class="assignmentPart">
					<h2 class="mt-5">Assignment</h2>
                                        <form action="" method="post" enctype="multipart/form-data" >
					<div class="form-group">
    					<label>Subject</label>
                                        <input type="text" class="form-control" placeholder="Enter Subject" name="subject">
  					</div>
  					<div class="form-group">
    					<label>Total Marks</label>
                                        <input type="text" class="form-control" placeholder="Enter Total Marks" name="total_marks">
  					</div>
  					<div class="form-group" id="question_gr">
                                          
                                        <label>Q No</label>
    					<div class="input-group">
                                            <input type="text" class="form-control" placeholder="Enter Sl No"  name="ids[]">
                                        </div>
    					<label>Question</label>
    					<div class="input-group">
                                        <input type="text" class="form-control" placeholder="Enter Question" name="questions[]">
                                        </div>
                                        <label>Marks</label>
    					<div class="input-group">
                                        <input class="form-control" placeholder="Enter Marks" type="text" name="markss[]">
                                        </div>
  					</div>
  					</div>
  					
  					
  					<div class="form-group">
                                            <button type="button" class="btn btn-primary" onclick="addQuestion();">Add More Questions</button>
  					</div>
<!--  					<div class="form-group">
  						    <span class="btn btn-primary btn-file">
        						Attach File <input type="file">
    						</span>
  					</div>-->
  					
  					<div class="bulk_uploader">
                                            <button type="submit" class="btn btn-danger save_button">Submit</button>
					</div>
                                    </form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>


function addQuestion() {
    alert(1);
	$("#question_gr").append('<div class="form-group"><label>Q No</label><div class="input-group"><input type="text" class="form-control" placeholder="Enter Sl No" name="ids[]"></div><label>Question</label><div class="input-group"> <input type="text" class="form-control" placeholder="Enter Question" name="questions[]"></div><label>Marks</label><div class="input-group"><input class="form-control" placeholder="Enter Marks" type="text" name="markss[]"></div></div>');                                       
                                                                         
}
</script>