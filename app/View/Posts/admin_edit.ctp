<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
#content {
    overflow: inherit;
}
.select2-container--default .select2-selection--multiple{
    width: 400px;
}

.will_be_able .target_student .need_to_know{
    margin-right: 12px;
}

.fa-times{
    cursor: pointer;
    margin-left: 8px;
}
</style>
<?php //pr($users); ?>
<div class="categories form">
<?php echo $this->Form->create('Post',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Course'); ?></legend>
    <?php

        echo $this->Form->input('id');
       
        echo $this->Form->input('user_id', array('empty' => '(choose any user)','required'=>'required' ,'class'=>'selectbox2'));
        
        echo $this->Form->input('post_title', array('label' => 'Course Title','required'=>'required'));
        
        echo $this->Form->input('post_subtitle', array('label' => 'Course Subtitle','required'=>'required'));

        echo $this->Form->input('post_description', array('label' => 'Course Description', 'type' => 'textarea','id'=>'post_desc')); 

        $course_goals = json_decode($this->request->data['Post']['course_goals']);
        $automatic_message = json_decode($this->request->data['Post']['automatic_message']);

        ?>
        <div class="input select">
            <label for="PostSkillLevel">Skill Level</label>
            <select name="data[Post][skill_level]" required="required" class="selectbox2" id="PostSkillLevel">
                <option value="">(choose any Skill Level)</option>
                <option <?php if($this->request->data['Post']['skill_level']=='1'){ echo 'selected'; } ?> value="1">Basic</option>
                <option <?php if($this->request->data['Post']['skill_level']=='2'){ echo 'selected'; } ?> value="2">Intermediate</option>
                <option <?php if($this->request->data['Post']['skill_level']=='3'){ echo 'selected'; } ?> value="3">Advance</option>
                <option <?php if($this->request->data['Post']['skill_level']=='4'){ echo 'selected'; } ?> value="3">Basic & Intermediate</option>
                <option <?php if($this->request->data['Post']['skill_level']=='5'){ echo 'selected'; } ?> value="3">Intermediate & Advance</option>
                <option <?php if($this->request->data['Post']['skill_level']=='6'){ echo 'selected'; } ?> value="3">All Levels</option>
            </select>
        </div>

        <?php
        
        echo $this->Form->input('category_id',array('empty' => '(choose any category)', 'label' => 'Course Category','required'=>'required' ,'class'=>'selectbox2'));

        echo $this->Form->input('subcategory_id',array('empty' => '(choose any category)', 'label' => 'Course Subcategory','options'=>$subcategories ,'class'=>'selectbox2'));

        ?>

        <div class="input select">
            <label for="PostLanguage">Course Language</label>
            <select name="data[Post][language]" required="required" class="selectbox2" id="PostLanguage">
                <option value=""> Select Language</option>
                <option value="Es" <?php if($post_dtls['Post']['language']=='Es'){ echo 'selected'; } ?>>Bahasa Indonesia</option>
                                <option value="En" <?php if($post_dtls['Post']['language']=='En'){ echo 'selected'; } ?> >English</option>
                               <option value="Ot" <?php if($post_dtls['Post']['language']=='Ot'){ echo 'selected'; } ?> >Others</option>
            </select>
        </div>   

        <?php
                
        // echo $this->Form->input('short_summary', array('label' => 'Short Summary','required'=>'required'));
        // echo $this->Form->input('quantity', array('label' => 'Maximum Number','required'=>'required'));
        // echo $this->Form->input('who_should_attend',array('type' => 'textarea','id'=>'who_should_attend'));
        
        echo $this->Form->input('Tag', array('label' => 'Add Tags','class' => 'selectpicker','multiple'=>'multiple'));
        
        // echo $this->Form->input('Keyword');
        
        //echo $this->Form->input('old_price');
        
        //echo $this->Form->input('price',array('required'=>'required')); 
        ?>

        <!-- <div class="input select">
            <label for="PostCurrencyType">Currency Type</label>
            <select name="data[Post][currency_type]" required="required" class="selectbox2" id="PostCurrencyType">
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
                <option value="GBP">GBP</option>
                <option value="AUD">AUD</option>
                <option value="CAD">CAD</option>
                <option value="INR">INR</option>
            </select>
        </div> -->

        <div class="input textarea">
            <label for="PostWelcomeMessage">Welcome Message</label>
            <textarea name="data[Post][welcome_message]" required="required" cols="30" rows="6" id="PostWelcomeMessage">
            <?php echo $automatic_message->welcome_message;?></textarea>
        </div>

        <div class="input textarea">
            <label for="PostCongratulationsMessage">Congratulations Message</label>
            <textarea name="data[Post][congratulation_message]" required="required" cols="30" rows="6" id="PostCongratulationsMessage"><?php echo $automatic_message->congratulation_message;?>
            </textarea>
        </div>

        <label>Course Goal</label>
       <label for="PostPostSubtitle">WHAT ARE THE REQUIREMENTS?</label> 
    <?php
        foreach ($course_goals->need_to_know as $key1 => $value1) { ?>
        <div class="input text">
            <?php if($key1==0){?>
            
            <?php } ?>
                <input name="data[Post][need_to_know][]" value="<?php echo $value1;?>" class="need_to_know" required="required"  maxlength="256" id="need_to_know" type="text"><i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <?php } ?>
        <div class="input text">
                <input name="data[Post][need_to_know][]" value="" class="need_to_know" required="required"  maxlength="256" id="need_to_know" type="text"><i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div id="need_to_know-append"></div>

        <label for="PostPostSubtitle">WHO IS THE TARGET AUDIENCE?</label>
    <?php
        foreach ($course_goals->target_student as $key2 => $value3) { ?>
        <div class="input text">
            <?php if($key2==0){?>
            
            <?php } ?>
                <input name="data[Post][target_student][]" value="<?php echo $value3;?>" class="target_student" required="required"  maxlength="256"  type="text"><i class="fa fa-times" aria-hidden="true"></i>
        </div>
    <?php } ?>
        <div class="input text">
                <input name="data[Post][target_student][]" value="" class="target_student" required="required"  maxlength="256"  type="text"><i class="fa fa-times" aria-hidden="true"></i>
        </div>    
        <div id="target_student-append"></div>

        <label for="PostPostSubtitle">WHAT AM I GOING TO GET FROM THIS COURSE?</label>
        <?php
            foreach ($course_goals->will_be_able as $key3 => $value2) { ?>
        <div class="input text">
            <?php if($key3==0){?>
            
            <?php } ?>
                <input name="data[Post][will_be_able][]" value="<?php echo $value2;?>" class="will_be_able" required="required"  maxlength="256"  type="text"><i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <?php } ?>
        <div class="input text">
            <input name="data[Post][will_be_able][]" value="" class="will_be_able" required="required"  maxlength="256"  type="text"><i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div id="will_be_able-append"></div>
        

        <?php
        
        // echo $this->Form->input('type_of_course', array(
        //     'options' => array('Classroom','Online'),
        //     'empty' => '(choose one)'
        // ));
        
        // echo $this->Form->input('startdate',array('required'=>'required'));
        // echo $this->Form->input('enddate',array('required'=>'required'));
        
        // echo $this->Form->input('course_duration');
        echo $this->Form->input('is_approve', array(
                'options' => array('0'=>'In Review','1'=>'Approved','2'=>'Declined','3'=>'Disapproved','4'=>'On Hold'),
            ));
        ?>
        <label for="PostIsApprove">Course Tags</label>
        <?php 
        echo $this->Form->input('is_show_home_page');?>
        <label >(Check This to Show The Course in The Home Page)</label>
        <?php 
        echo $this->Form->input('course_of_the_day');
        echo $this->Form->input('featured');
        echo $this->Form->input('image', array('label'=>'Image/Video','type' => 'file'));
        echo $this->Form->input('audio', array('type' => 'file'));
        echo $this->Form->input('meta_keyword');
        echo $this->Form->input('meta_description',array('type' => 'textarea'));
        echo $this->Form->input('is_bestselling');
    ?>
    </fieldset>


<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>


<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>

<script type="text/javascript">
    CKEDITOR.replace('post_desc',
            {
                width: "95%"
            });
    CKEDITOR.replace('PostWelcomeMessage',
            {
                width: "95%"
            });
    CKEDITOR.replace('PostCongratulationsMessage',
            {
                width: "95%"
            });
    
   






$(document).ready(function() {
  
   $("#PostCategoryId").change(function () {
        var category_id = $(this).val();
        $.ajax({
           type: "POST",
           url: '/categories/ajaxSubCategory',
           dataType:'json',
           data:{
            category_id:category_id
           }, 
           success: function(data)
           {
                if(data.Ack==1){
                  $("#PostSubcategoryId").html('');
                  $("#PostSubcategoryId").html(data.res);
                }
                else{
                  $("#PostSubcategoryId").html('<option value="">(Sub-Category)</option>');  
                }
            }
        });    
    })  

    $(".selectpicker").select2();

    $(document).on('click','.will_be_able:last',function () {
      $("#will_be_able-append").append('<div class="input text"><input name="data[Post][will_be_able][]" class="will_be_able" required="required" maxlength="256"  type="text"><i class="fa fa-times" aria-hidden="true"></i></div>');
    });

    $(document).on('click','.target_student:last',function () {
      $("#target_student-append").append('<div class="input text"><input name="data[Post][target_student][]" class="target_student" required="required"  maxlength="256" type="text"><i class="fa fa-times" aria-hidden="true"></i></div>');
    });

    $(document).on('click','.need_to_know:last',function () {
      $("#need_to_know-append").append('<div class="input text"><input name="data[Post][need_to_know][]" class="need_to_know" required="required"  maxlength="256" id="need_to_know" type="text"><i class="fa fa-times" aria-hidden="true"></i></div>');
    });

    $(document).on('click','.fa-times',function () {
       $(this).closest("div.text").remove();
    });

});

</script>


