<style>
.header{
    position:relative;
}

.inner_header{
    background:#0c2440;
}
#addNewTag{
  padding: 6px;
}
.basicClass{
  margin-left: -21px;
}


</style>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
    .checkbox input[type="checkbox"] { margin: 0; }
    .checkbox { border: medium none !important; }
    .uploadimg .select2-container{ width: 264px !important; }
</style>
<?php
if(isset($post_dtls['PostImage']) && !empty($post_dtls['PostImage'])){
$img_array = end($post_dtls['PostImage']);
$img = $this->webroot.'img/post_img/'.$img_array['originalpath'];
}
else{
$img = $this->webroot.'img/placeholder.png';
} 
?> 
           
<section class="profileedit">
    <div class="container">
        <div class="row" style="background:#f6f6f6; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
            <div class="col-md-3 col-sm-3" style="padding:0">
                 <?php 
                                      /***Course Sidebar**/
                    echo $this->element('course_sidebar',$post_dtls,$course_status);
                  ?>
                
            </div>
            <div class="col-md-9 col-sm-9" style="padding:0; border-left:#ddd solid 1px;">
                <div class="profile_second_part">
                   <!-- <p class="pull-right course_setting">
                       <span>
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <a href="<?php echo $this->webroot;?>posts/course_setting/<?php echo $post_dtls['Post']['slug'];?>">Course Settings</a>
                      </span>
                      <span>
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                        <a href="#">help</a>
                      </span> 
                    </p>-->
                  <?php echo $topContent['CmsPage']['page_description']; ?>
                     <!-- <form class="form-horizontal form_part1213"> -->
                        <?php
                    echo $this->Form->create('Post', array('type' => 'file', 'class' => 'form-horizontal form_part1213'));
                    ?>
                          <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Course Title <span>*</span></label>
                            <div class="col-sm-9">
                              <!-- <input class="form-control form_part form_text" id="text" placeholder="Photoshop CC" type="text"> -->
                              <?php echo $this->Form->input('id', array('hidden'=>'hidden'));?>
                               <?php
                            echo $this->Form->input(
                                 'post_title', array(
                                'label' => FALSE,
                                'class' => 'form-control form_part form_text',
                                'id' => 'text',
                                'div' => FALSE,
                                'placeholder'=>'Photoshop CC',
                                'required' => 'required',
                                'maxlength' => 75
                                    )
                            );
                            ?>
                              <div class="number">75</div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Course Subtitle</label>
                            <div class="col-sm-9">
                              <!-- <input class="form-control form_part form_text" id="text" placeholder="Insert your course subtitle" type="text"> -->
                              <?php
                            echo $this->Form->input(
                                 'post_subtitle', array(
                                'label' => FALSE,
                                'class' => 'form-control form_part form_text',
                                'id' => 'text',
                                'div' => FALSE,
                                'placeholder'=>'Insert your course subtitle',
                                'required' => 'required',
                                'maxlength' => 75
                                    )
                            );
                            ?>
                              <div class="number">75</div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Course Description</label>
                            <div class="col-sm-9">
                              <!-- <textarea class="form-control textareabox" rows="5" id="comment"></textarea> -->
                              <?php
                            echo $this->Form->input(
                                    'post_description', array(
                                'label' => FALSE,
                                'class' => 'form-control textareabox',
                                'id' => 'comment',
                                'div' => FALSE
                                // ,
                                // 'required' => 'required'
                                    )
                            );
                            ?>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Basic Info</label>
                            <div class="col-sm-9">
                              <div class="row">
                                <!-- <div class="col-md-4">
                                    <select class="selectfield selectpart">
                                        <option>English (UK)</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>-->
                                <div class="col-md-4">
                                    <?php
                                      $levels = array('1'=>'Basic','2'=>'Intermediate','3'=>'Advance','4'=>'Basic & Intermediate','5'=>'Intermediate & Advance','6'=>'All Levels');
                                      echo $this->Form->input(
                                          'skill_level', array(
                                          'empty' => 'Select Level',
                                          'options'=>$levels,
                                          'label' => FALSE,
                                          'class' => 'selectfield selectpart w-100',
                                          'div' => FALSE,
                                          'required' => 'required'
                                        )
                                      );
                                    ?>
                                </div> 
                                <div class="col-md-4">
                                    <?php
                                      echo $this->Form->input(
                                          'category_id', array(
                                          'empty' => 'Select Category',
                                          'label' => FALSE,
                                          'class' => 'selectfield selectpart w-100',
                                          'div' => FALSE,
                                          'required' => 'required'
                                        )
                                      );
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                      echo $this->Form->input(
                                          'subcategory_id', array(
                                          'empty' => 'Select Sub-Category',
                                          'options'=>$subcategories,
                                          'label' => FALSE,
                                          'class' => 'selectfield selectpart w-100',
                                          'div' => FALSE
                                          
                                        )
                                      );
                                    ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Course Image</label>
                            <div class="col-sm-9">                        
                                <div class="uploadimg" id="divPreview">
                                    <img src="<?php echo $img; ?>" >
                                </div>
                                
                                <div class="reviewtext">
                                    <p class="font-15">Please read the course image quality standards before uploading the image. </p>

                                    <p class="second_part font-15">Dimensions: 2048x1152 pixels. Format: .jpg, .jpeg, .gif, .bmp or .png. </p>
                                    <div class="custom-file-upload">
                                        <!--<label for="file">File: </label>--> 
                                        <input type="file" id="image_file" class="media_input" name="data[Post][image][]"   multiple/>
                                    </div>
                                </div>                              
                              </div>
                            </div>
                            <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Promotional Video</label>
                            <div class="col-sm-9">                        
                                <div class="uploadimg" >
                                    <img src="<?php echo $this->webroot.'img/placeholder.png';?>" id="videoDiv">
                                </div>
                                <div class="reviewtext">
                                    <p class="font-15">Please read the course image quality standards before uploading the image. </p>

                                    <p class="second_part font-15">Dimensions: 2048x1152 pixels. Format: .jpg, .jpeg, .gif, .bmp or .png. </p>
                                    <div class="custom-file-upload">
                                        <!--<label for="file">File: </label>--> 
                                        <input type="file" id="video_file" class="media_input" name="data[Post][video][]"  multiple />
                                    </div>
                                </div>                              
                              </div>
                            </div>
                            
                            <div class="form-group row">
                            <label class="text-sm-right form-control-label col-sm-3" for="text">Course Language</label>
                            <div class="col-sm-9">                        
                                
                                
                                <div class="uploadimg">
                                    
                                    <select class="selectfield selectpart" name="data[Post][language]" required>
                                  
                                <option value="Es" <?php if($post_dtls['Post']['language']=='Es'){ echo 'selected'; } ?>>Bahasa Indonesia</option>
                                <option value="En" <?php if($post_dtls['Post']['language']=='En'){ echo 'selected'; } ?> >English</option>
                               <option value="Ot" <?php if($post_dtls['Post']['language']=='Ot'){ echo 'selected'; } ?> >Others</option>
                            
<!--                                        <option value='' > Select Language</option>
                                        <option <?php if($post_dtls['Post']['language']=='English'){ echo 'selected'; } ?> value='English'>English</option>
                                        <option <?php if($post_dtls['Post']['language']=='Espanol'){ echo 'selected'; } ?> value='Espanol'>Espanol</option>
                                        <option <?php if($post_dtls['Post']['language']=='French'){ echo 'selected'; } ?> value='French'>French</option>
                                        <option <?php if($post_dtls['Post']['language']=='German'){ echo 'selected'; } ?> value='German'>German</option>-->
                                    </select>
                                
                                </div>

                                    <!-- <p>Use a .vtt file no larger than 1GiB</p> -->
                                
                                <!-- <div class="reviewtext">
                                    <div class="custom-file-upload">
                                        <label for="file">File: </label>
                                        <input type="file" id="file" name="myfiles[]" multiple />
                                    </div>
                                    
                                </div> -->
                                                                
                              </div>
                            </div>
                            
                            <div class="form-group row">
                        <label class="text-sm-right form-control-label col-sm-3" for="text">Skill</label>
                        <div class="col-sm-9">                        
                                           
                        <div class="uploadimg">
                        <select name="skilldata[]" multiple="multiple" style="background: none !important;">
                            
                         <?php
                       foreach($skill as $key=>$val){ 
                           ?>
                            <option value="<?php echo $key; ?>">
                                <?php echo $val; ?>
                            </option>
                        <?php    
                       }
                          ?>   
                        </select> 
                                </div>
                        </div>
                       </div>
                            <div class="form-group row">
                              <label class="text-sm-right form-control-label col-sm-3" for="text">Course Tags</label>
                                <div class="col-sm-9">                        
                                    <div class="uploadimg">
                                      <?php
                                        echo $this->Form->input(
                                            'Tag', array(
                                            'label' => FALSE,
                                            'class' => 'selectpicker',
                                            'id' => 'PostTags',
                                            'div' => FALSE,
                                            'placeholder'=>'Select Tags'
                                                )
                                        );
                                      ?>
                                    </div>
                                    <button type="button" class="btn btn-default pt-2 pb-2 ml-2" data-toggle="modal" data-target="#addTagModal" id="addNewTag">Add New Tag</button>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                              <div class="col-sm-9 offset-sm-3">
                                <div class="btn_save">                        
                                    <input type="submit" name="save" class="btn btn-primary" value="Save">
                                    <!-- Save</button> -->
                                    <input type="submit" name="save-next" class="btn btn-primary" value="Save & Next" >
                                    <!-- Save</button> -->
                                    <!-- <a href="<?php echo $this->webroot.$this->request->data['Post']['slug']; ?>" ><button type="button" class="btn btn-success">Preview</button></a> -->
                                </div>                          
                              </div>
                            </div>
                          </form>
                          </div>
                </div>
        </div>
    </div>
</section>

<!--Add Tag Modal -->
  <div class="modal fade" id="addTagModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Tag</h4>
        </div>
        <div class="modal-body">
            <form>
                <div class="input text">
                    <label for="PostPostSubtitle">Tag Name</label> 
                    <input name="" class="" id="addTagName" required="required"  maxlength="256"  type="text">
                </div>
                <div class="submit">    
                  <button type="button" class="btn btn-info btn-sm"  id="saveTag" name="Submit" >Submit</button>
                </div>  
            </form>    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>

<script>

$(document).ready(function(){
  $(document).on('keyup', 'input', function(){
      var self = $(this);
      var attr = self.attr('maxlength');
      if (typeof attr !== typeof undefined && attr !== false) {
        var showThis = attr - self.val().length;
        self.next('.number').text(showThis);
      }
      
  });
  
  CalculateNumber();

})

function CalculateNumber() {
  $('input').each(function(){
    var self = $(this);
      var attr = self.attr('maxlength');
      if (typeof attr !== typeof undefined && attr !== false) {
        var showThis = attr - self.val().length;
        self.next('.number').text(showThis);
    }
  });
}


  
  (function($) {

          // Browser supports HTML5 multiple file?
          var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
              isIE = /msie/i.test( navigator.userAgent );

          $.fn.customFile = function() {

            return this.each(function() {

              var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
                  $wrap = $('<div class="file-upload-wrapper">'),
                  $input = $('<input type="text" class="file-upload-input1" />'),
                  // Button that will be used in non-IE browsers
                  $button = $('<button type="button" class="file-upload-button1">Select a File</button>'),
                  // Hack for IE
                  $label = $('<label class="file-upload-button1" for="'+ $file[0].id +'">Select a File</label>');

              // Hide by shifting to the left so we
              // can still trigger events
              $file.css({
                position: 'absolute',
                left: '-9999px'
              });

              $wrap.insertAfter( $file )
                .append( $file, $input, ( isIE ? $label : $button ) );

              // Prevent focus
              $file.attr('tabIndex', -1);
              $button.attr('tabIndex', -1);

              $button.click(function () {
                $file.focus().click(); // Open dialog
              });

              $file.change(function() {

                var files = [], fileArr, filename;

                // If multiple is supported then extract
                // all filenames from the file array
                if ( multipleSupport ) {
                  fileArr = $file[0].files;
                  for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                    files.push( fileArr[i].name );
                  }
                  filename = files.join(', ');

                // If not supported then just take the value
                // and remove the path to just show the filename
                } else {
                  filename = $file.val().split('\\').pop();
                }

                $input.val( filename ) // Set the value
                  .attr('title', filename) // Show filename in title tootlip
                  .focus(); // Regain focus

              });

              $input.on({
                blur: function() { $file.trigger('blur'); },
                keydown: function( e ) {
                  if ( e.which === 13 ) { // Enter
                    if ( !isIE ) { $file.trigger('click'); }
                  } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                    // On some browsers the value is read-only
                    // with this trick we remove the old input and add
                    // a clean clone with all the original events attached
                    $file.replaceWith( $file = $file.clone( true ) );
                    $file.trigger('change');
                    $input.val('');
                  } else if ( e.which === 9 ){ // TAB
                    return;
                  } else { // All other keys
                    return false;
                  }
                }
              });

            });

          };

          // Old browser fallback
          if ( !multipleSupport ) {
            $( document ).on('change', 'input.customfile', function() {

              var $this = $(this),
                  // Create a unique ID so we
                  // can attach the label to the input
                  uniqId = 'customfile_'+ (new Date()).getTime(),
                  $wrap = $this.parent(),

                  // Filter empty input
                  $inputs = $wrap.siblings().find('.file-upload-input')
                    .filter(function(){ return !this.value }),

                  $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

              // 1ms timeout so it runs after all other events
              // that modify the value have triggered
              setTimeout(function() {
                // Add a new input
                if ( $this.val() ) {
                  // Check for empty fields to prevent
                  // creating new inputs when changing files
                  if ( !$inputs.length ) {
                    $wrap.after( $file );
                    $file.customFile();
                  }
                // Remove and reorganize inputs
                } else {
                  $inputs.parent().remove();
                  // Move the input so it's always last on the list
                  $wrap.appendTo( $wrap.parent() );
                  $wrap.find('input').focus();
                }
              }, 1);

            });
          }

}(jQuery));

$('input[class="media_input"]').customFile();

$(function () {
    
$("#image_file").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#divPreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});

$("#PostCategoryId").change(function () {
  var category_id = $(this).val();
    $.ajax({
       type: "POST",
       url: '<?php echo $this->webroot;?>categories/ajaxSubCategory',
       dataType:'json',
       data:{category_id:category_id}, // serializes the form's elements.
       success: function(data)
       {
        console.log(data);
        if(data.Ack==1){
          $("#PostSubcategoryId").html('');
          $("#PostSubcategoryId").html(data.res);
        }
        else{
          $("#PostSubcategoryId").html('<option value="">(Sub-Category)</option>');  
        }
       }
    });    

});

$(document).ready(function () {
    $("#PostAddCourseForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[Post][image][]': {
                validators: {
                    notEmpty: {
                        message: 'Image is required and cannot be empty'
                    }
                }
            },
            'data[Post][video][]': {
                validators: {
                    notEmpty: {
                        message: 'Video is required and cannot be empty'
                    }
                }
            },
            'data[Post][post_title]': {
                validators: {
                    notEmpty: {
                        message: 'Title is required and cannot be empty'
                    }
                }
            },
            'data[Post][post_subtitle]': {
                validators: {
                    notEmpty: {
                        message: 'Subtitle is required and cannot be empty'
                    }
                }
            },
            // 'data[Post][post_description]': {
            //     validators: {
            //         notEmpty: {
            //             message: 'Description is required and cannot be empty'
            //         }
            //     }
            // },
            'data[Post][category_id]': {
                validators: {
                    notEmpty: {
                        message: 'Select a Category '
                    }
                }
            },
            'data[Post][language]': {
                validators: {
                    notEmpty: {
                        message: 'Select a Language '
                    }
                }
            }
        }
    })
});

$(document).ready(function() {
  $(".selectpicker").select2();
});

$(document).on('click','#saveTag',function(){
        var tag = $('#addTagName').val();

        $.ajax({
           type: "POST",
           url: '<?php echo $this->webroot; ?>tags/ajaxAddTag',
           dataType:'json',
           data:{
            tag:tag
           }, 
           success: function(data)
            {
                if(data.Ack==1){
                  $("#PostTags").append('<option value="'+data.id+'">'+tag+'</option>');
                  $('#addTagModal').modal('hide');
                  alert('New Tag Is Successfully Added');
                }
                
            }
        });
})

CKEDITOR.replace('comment');
</script>