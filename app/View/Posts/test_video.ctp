<?php //phpinfo(); exit; ?>
<style>
.header{
    position:relative;
}

.inner_header{
    background:#0c2440;
}
label{ font-weight: bold; }

</style>

<link href="<?php echo $this->webroot.'video/';?>css/video-js.css" rel="stylesheet" type="text/css">
<script>videojs.options.flash.swf = "video-js.swf";</script>
<link href="<?php echo $this->webroot.'video/';?>videoStyle.css" rel="stylesheet" type="text/css">

<section class="profileedit">
	<div class="container">
		<div class="row" style="background:#f6f6f6; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
			<div class="col-md-3 col-sm-3" style="padding:0;">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
			</div>
			<div class="col-md-9 col-sm-9" style="padding:0; border-left:solid 1px #ddd;">
				<div class="profile_second_part testvideo">
					<!--<p class="pull-right course_setting">
						<span>
							<i class="fa fa-cog" aria-hidden="true"></i>
 							<a href="<?php echo $this->webroot;?>posts/course_setting/<?php echo $post_dtls['Post']['slug'];?>">Course Settings</a>
 						</span>        
 						 <span>
 							<i class="fa fa-question-circle" aria-hidden="true"></i>
							<a href="#">help</a>
						</span> 
					</p>-->
		<?php echo $topContent['CmsPage']['page_description']; ?>
					<form class="form_parttestvideo" action="<?php echo $this->webroot; ?>posts/test_video/<?php echo $post_dtls['Post']['slug'];?>" method="post" enctype="multipart/form-data" >
							<?php if(!empty($testVideo)){
								$file = $testVideo['TestVideo']['video'];
								$ext = pathinfo($file, PATHINFO_EXTENSION);
								$file_name = trim($file, $ext);
							?>
							<div class="form-group row">
							    <div id="instructions" class="instructionsPart">
                                    <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-button" controls preload="none" width="450" height="330"
                                          
                                          data-setup='{ "playbackRates": [0.5, 1, 1.5, 2] }' >
                                        <source  src='<?php echo $this->webroot; ?>test_video/<?php echo $file; ?>' type='video/<?php echo $ext; ?>'>
                                        <source src='<?php echo $this->webroot . 'test_video/' . $file_name; ?>webm' type='video/webm'>
                                        <source src='<?php echo $this->webroot . 'test_video/' . $file_name; ?>ogv' type='video/ogg' />
                                        <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track>
                                        <!-- Tracks need an ending tag thanks to IE9 -->
                                        <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track>
                                        <!-- Tracks need an ending tag thanks to IE9 -->
                                        
                                        <!-- <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p> -->
                                          
                                    </video>
                                </div>
						  	</div>
							<?php } ?>

						  <div class="form-group row">
						    <label class="col-3 col-form-label" for="text">Video:<span>*</span></label>
						    <div class="col-9">
						      	<div class="custom-file-upload">
								    <!--<label for="file">File: </label>--> 
								    <input type="file" id="file" name="data[video]" required  />
								</div>
                                                        <p>Upload file size should be less than 1000MB</p>
						    </div>
						  </div>

					 	<?php if(!empty($testVideo)){ ?>
					 		<input type="hidden" name="data[last_video]" value="<?php echo $testVideo['TestVideo']['video'];?>">
					 		<input type="hidden" name="data[id]" value="<?php echo $testVideo['TestVideo']['id'];?>">
					 	<?php } ?>					 	
					 	<div class="form-group row">
						    <label class="col-form-label col-3" for="text">Microphone:</label>
						    <div class="col-9">
						      <input class="form-control form_part form_text" maxlength="75" id="text" placeholder="e.g.  Built in mic, blue yeti ( optional )" type="text" name="data[microphone]">
						      <div class="number">75</div>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label class="col-form-label col-3" for="text">Camera:</label>
						    <div class="col-9">
						      <input class="form-control form_part form_text" maxlength="75" id="text" placeholder="e.g.  Built in mic, blue yeti ( optional )" type="text" name="data[camera]">
						      <div class="number">75</div>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label class="col-form-label col-3" for="text">Lighting:</label>
						    <div class="col-9">
						      <input class="form-control form_part form_text" maxlength="75" id="text" placeholder="e.g.  Built in mic, blue yeti ( optional )" type="text" name="data[lighting]" >
						      <div class="number">75</div>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label class="col-form-label col-3 " for="text">Editing Software:</label>
						    <div class="col-9">
						      <input class="form-control form_part form_text" maxlength="75" id="text" placeholder="e.g.  Built in mic, blue yeti ( optional )" type="text" name="data[editing_software]">
						      <div class="number">75</div>
						    </div>
						  </div>
						  <?php if($TestVideo_condition>0){?>
						  <div class="form-group row">
						    <div class="col-9">
						      <span style="margin-left: 218px; color: red; display:block;"> *Your Video Is In Review.After Admin Approve,This Step Will Complete </span>
						    </div>
						  </div>
						  <?php } ?>
					 	<div class="bulk_uploader form-group row">
					 		<div class="col-sm-9 offset-sm-3">
					 			<input type="submit" name="save" class="btn btn-danger" value="Save" >
					 			<!-- Save</button> -->
					 			<input type="submit" name="save-next" class="btn btn-danger" value="Save & Next" >
					 			<!-- Save & Next</button> -->
					 		</div>
					 	</div>
					</form>
				</div>
						  
						  
					  
				</div>
		</div>
	</div>
</section>
<script>
	;(function($) {

		  // Browser supports HTML5 multiple file?
		  var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
		      isIE = /msie/i.test( navigator.userAgent );

		  $.fn.customFile = function() {

		    return this.each(function() {

		      var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
		          $wrap = $('<div class="file-upload-wrapper">'),
		          $input = $('<input type="text" class="file-upload-input" />'),
		          // Button that will be used in non-IE browsers
		          $button = $('<button type="button" class="file-upload-button">Upload Video</button>'),
		          // Hack for IE
		          $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Upload Video</label>');

		      // Hide by shifting to the left so we
		      // can still trigger events
		      $file.css({
		        position: 'absolute',
		        left: '-9999px'
		      });

		      $wrap.insertAfter( $file )
		        .append( $file, $input, ( isIE ? $label : $button ) );

		      // Prevent focus
		      $file.attr('tabIndex', -1);
		      $button.attr('tabIndex', -1);

		      $button.click(function () {
		        $file.focus().click(); // Open dialog
		      });

		      $file.change(function() {

		        var files = [], fileArr, filename;

		        // If multiple is supported then extract
		        // all filenames from the file array
		        if ( multipleSupport ) {
		          fileArr = $file[0].files;
		          for ( var i = 0, len = fileArr.length; i < len; i++ ) {
		            files.push( fileArr[i].name );
		          }
		          filename = files.join(', ');

		        // If not supported then just take the value
		        // and remove the path to just show the filename
		        } else {
		          filename = $file.val().split('\\').pop();
		        }

		        $input.val( filename ) // Set the value
		          .attr('title', filename) // Show filename in title tootlip
		          .focus(); // Regain focus

		      });

		      $input.on({
		        blur: function() { $file.trigger('blur'); },
		        keydown: function( e ) {
		          if ( e.which === 13 ) { // Enter
		            if ( !isIE ) { $file.trigger('click'); }
		          } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
		            // On some browsers the value is read-only
		            // with this trick we remove the old input and add
		            // a clean clone with all the original events attached
		            $file.replaceWith( $file = $file.clone( true ) );
		            $file.trigger('change');
		            $input.val('');
		          } else if ( e.which === 9 ){ // TAB
		            return;
		          } else { // All other keys
		            return false;
		          }
		        }
		      });

		    });

		  };

		  // Old browser fallback
		  if ( !multipleSupport ) {
		    $( document ).on('change', 'input.customfile', function() {

		      var $this = $(this),
		          // Create a unique ID so we
		          // can attach the label to the input
		          uniqId = 'customfile_'+ (new Date()).getTime(),
		          $wrap = $this.parent(),

		          // Filter empty input
		          $inputs = $wrap.siblings().find('.file-upload-input')
		            .filter(function(){ return !this.value }),

		          $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

		      // 1ms timeout so it runs after all other events
		      // that modify the value have triggered
		      setTimeout(function() {
		        // Add a new input
		        if ( $this.val() ) {
		          // Check for empty fields to prevent
		          // creating new inputs when changing files
		          if ( !$inputs.length ) {
		            $wrap.after( $file );
		            $file.customFile();
		          }
		        // Remove and reorganize inputs
		        } else {
		          $inputs.parent().remove();
		          // Move the input so it's always last on the list
		          $wrap.appendTo( $wrap.parent() );
		          $wrap.find('input').focus();
		        }
		      }, 1);

		    });
		  }

}(jQuery));

$('input[type=file]').customFile();

$(function () {
    
	$("#image_file").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#divPreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });

    $(document).on('keyup', 'input', function(){
    	var self = $(this);
    	var attr = self.attr('maxlength');
    	if (typeof attr !== typeof undefined && attr !== false) {
    		var showThis = attr - self.val().length;
		    self.next('.number').text(showThis);
		}
    	
    });
    CalculateNumber();
});

function CalculateNumber() {
	$('input').each(function(){
		var self = $(this);
    	var attr = self.attr('maxlength');
    	if (typeof attr !== typeof undefined && attr !== false) {
    		var showThis = attr - self.val().length;
		    self.next('.number').text(showThis);
		}
	});
}


</script>


<script type="text/javascript">
    videojs('example_video_1').ready(function(){
      // Create variables and new div, anchor and image for download icon
      var myPlayer = this,
        controlBar,
        newElement = document.createElement('div'),
        newLink = document.createElement('a'),
        newImage = document.createElement('img');
      // Assign id and classes to div for icon
      newElement.id = 'downloadButton';
      newElement.className = 'downloadStyle vjs-control';
      // Assign properties to elements and assign to parents
     <!-- newImage.setAttribute('src','images/file-download.png');-->
      newLink.setAttribute('href','#');
      newLink.appendChild(newImage);
      newElement.appendChild(newLink);
      // Get control bar and insert before elements
      // Remember that getElementsByClassName() returns an array
      controlBar = document.getElementsByClassName('vjs-control-bar')[0];
      // Change the class name here to move the icon in the controlBar
      insertBeforeNode = document.getElementsByClassName('vjs-volume-menu-button')[0];
      // Insert the icon div in proper location
      controlBar.insertBefore(newElement,insertBeforeNode);
    });
  </script>
    
    <script>
  var video = document.getElementById('example_video_1');
var intervalRewind;
$(video).on('play',function(){
    video.playbackRate = 1.0;
    clearInterval(intervalRewind);
});
$(video).on('pause',function(){
    video.playbackRate = 1.0;
    clearInterval(intervalRewind);
});
$("#speed").click(function() { // button function for 3x fast speed forward
    video.playbackRate = 3.0;
});
$("#negative").click(function() { // button function for rewind
   intervalRewind = setInterval(function(){
       video.playbackRate = 1.0;
       if(video.currentTime == 0){
           clearInterval(intervalRewind);
           video.pause();
       }
       else{
           video.currentTime += -.1;
       }
                },30);
});
</script>

<script>
function CenterPlayBT() {
   var playBT = $(".vjs-big-play-button");
   playBT.css({
      left:( (playBT.parent().outerWidth()-playBT.outerWidth())/2 )+"px",
      top:( (playBT.parent().outerHeight()-playBT.outerHeight())/2 )+"px"
   });
}
</script>

<script>
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    //$(".vjs-big-play-button").trigger("click");

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});

  jQuery('.header_cards').click(function(){
    $(this).siblings('.card_content').slideToggle();
    //alert('');
  });
</script>