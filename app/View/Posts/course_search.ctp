<style>
.rightparts {
    background: rgba(0, 0, 0, 0) url("<?php echo $this->webroot;?>/img/rightbanner.jpg") no-repeat scroll 0 0 / cover ;
    float: right;
    padding: 64px 0;
    width: 60%;
}
.list-group .course_bottom_part{
    min-height: 204px;
}
/** Paging **/
.paging {
	background:#fff;
	color: #ccc;
	margin-top: 1em;
	clear:both;
	padding-bottom: 15px;
}
.paging .current,
.paging .disabled,
.paging a {
	text-decoration: none;
	padding: 5px 8px;
	display: inline-block
}
.paging > span {
	display: inline-block;
	border: 1px solid #ccc;
	border-left: 0;
}
.paging > span:hover {
	background: #efefef;
}
.paging .prev {
	border-left: 1px solid #ccc;
	-moz-border-radius: 4px 0 0 4px;
	-webkit-border-radius: 4px 0 0 4px;
	border-radius: 4px 0 0 4px;
}
.paging .next {
	-moz-border-radius: 0 4px 4px 0;
	-webkit-border-radius: 0 4px 4px 0;
	border-radius: 0 4px 4px 0;
}
.paging .disabled {
	color: #ddd;
}
.paging .disabled:hover {
	background: transparent;
}
.paging .current {
	background: #efefef;
	color: #c73e14;
}
.view-as{
	margin-top: 5px;
}
#slider-range{
	width: 190px;
}
.slider-div{
	display: none;
}
.show1{
	margin-bottom: 13px;
}
.price_level{
	margin-left: 8px;
}
.filtersearch{
	margin-bottom: 30px;
}
.text{
	min-height: 146px;
}



</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<?php //pr($categories); ?>
<section class="category_banner">
	<div class="inner_banner">
		<div class="container">
          <div class="row">
            <div class="col-md-6">
			<p id="searchHeader">All Courses </p>
					<p class="inner_text">Explore your interests and find a course from myriads of professional courses perfectly suitable for you.</p>
					<a href="<?php echo $this->webroot;?>users/contact_us" class="btn btnPrimarys" style="padding:7px 20px 10px 20px !important; font-size:16px; margin-top:15px;">Enquire Now</a>
		</div>
        </div>
        </div>
	</div>
</section>
<?php // pr($courses); exit; ?>
<!-- <span>Skill Level:
		<select class="selectfield1" id = "skill_level" >
		<option value="all_level" > All Level </option>
		<option value="begineer" > Begineer </option>
		<option value="intermedite" > Intermedite </option>
		<option value="expert" > Expert </option>
	</select>
</span>
<span ></span><span class="show1">
	<label for="amount">Price range:</label>
		<input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold; background-color: rgb(235, 235, 235); ">
</span>
<span>Price:
		<select class="selectfield1" id = "price_level" >
		<option value="by_review" > All Level </option>
		<option value="by_rating" > Begineer </option>
	</select>
</span> -->

<section class="items_part">
	<div class="part_top">
		<div class="container">
			<div>
				<div class="hd_submenu">
					
						<span>
							Skill Level:
								<select class="selectfield1" id = "skill_level" >
								<option value="0" > All Level </option>
								<option value="1" > Begineer </option>
								<option value="2" > Intermedite </option>
								<option value="3" > Expert </option>
							</select>
						</span>
                        <span>
							 Price: 
								<select class="selectfield1" id = "price" >
								<option value="" > Select Price Level </option>
								<option value="0" > Paid  </option>
								<option value="1" > Free  </option>
							</select>
						</span>
                        <span>
							  Instructor : 
								<select class="selectfield1" id = "instructor" >
                             		<option value="" > Select Instructor </option>
							<?php foreach ($users as $key => $user) { 
								$count = 0;
									foreach ($user['Post'] as $key => $post) {
									    	if($post['is_approve']==1){
									    		$count = $count + 1;
									    	}
									    }   
								?>	
								<?php 
								if($user['User']['first_name']!='' && $user['User']['last_name']!=''){ ?>
									<option class="user_list" value="<?php echo $user['User']['id'];?>" ><a href="javascript:void(0)"><?php echo $user['User']['first_name'].' '.$user['User']['last_name'];?><span class="pull-right">
										<!-- (<?php echo $count;?>) -->
									</span></a></option>
							<?php 	}
							} ?>	
							
								
							</select>
						</span>
                        	<div class="pull-right viewpart"><p class="pull-left">
      				<span>Sort by:
      				<select class="selectfield1" id = "order_filter" >
						<option value="by_review" > Most Reviewed </option>
						<option value="by_rating" > Highest Rated </option>
						<option value="by_date" > Newest </option>
						<option value="by_low_price" > Price: Low to High </option>
						<option value="by_high_price" > Price: High to Low </option>
					</select></span>
					</p>
					<div class="btn-group" style="margin-top: 5px;">
							<span class="pull-left view-as">View as:</span>
				            <a href="#" id="grid" class="btn btn-default btn-sm"><i class="fa fa-th-large"></i></a>
				            <a href="#" id="list" class="btn btn-default btn-sm"><i class="fa fa-bars" aria-hidden="true"></i></a>
				            
				    </div>
					<input type='hidden' value="<?php echo $max_price;?>" id="max_price">
				</div>

				   </div>
				</div>
			</div>
		</div>
	</div>
	<div class="part_bottom">
		<div class="container courseFilterTab">
    
<!--end checkbox //-->			
    <div class="row">
		<div class="col-md-3">
					<div class="filtersearch">
						<h2>Filter Search</h2>
						<div class="software_business">
						<div class="business_class1">
							<h2>Course Categories</h2>
							<ul>
                            
                            <li>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

						      <?php
						      $cnt = 1; 
						      foreach ($categories as $key => $category) { 
								$count = 0;
									foreach ($category['Post'] as $key => $value) {
									    	if($value['is_approve']==1){
									    		$count = $count + 1;
									    	}
									    }    
							  ?>
							  
						     <div class="panel panel-default">
						        <div class="panel-heading" role="tab" id="<?php echo $category['Category']['id'];?>">
						         	<h4 class="panel-title">
						            <a role="button" class="category_list" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $category['Category']['category_name'];?>" data-catname="<?php echo $category['Category']['category_name'];?>" aria-expanded="true" aria-controls="<?php echo $category['Category']['category_name'];?>" data-id="<?php echo $category['Category']['id'];?>" >
						              <?php echo $category['Category']['category_name'];?>
						              <span class="pull-right">(<?php echo $count;?>)</span>
						            </a>
						          </h4>
						        </div>
						        <!-- <div id="<?php //echo $category['Category']['category_name'];?>" class="panel-collapse collapse <?php //echo ($cnt==1) ? 'in':''; ?>" role="tabpanel" aria-labelledby="<?php //echo $category['Category']['id'];?>">
						          <div class="panel-body">
						            <ul>
						            <?php 	
						            foreach ($category['Children'] as $key => $val) {	?>
						            <li class="sub_category" style="cursor: pointer;" data-subcat="<?php //echo $val['category_name'];?>" data-id="<?php //echo $val['id'];?>" ><?php //echo $val['category_name'];?></li> 
						            <?php } ?>
						            </ul>
						          </div>
						        </div> -->
						     </div>

						      <?php
						      	$cnt++;
						       } ?>
						    </li>
								
							</ul>
							</div>
							<!--<div class="business_class1">
							<h2>Instructor</h2>
							<ul>
							<?php foreach ($users as $key => $user) { 
								$count = 0;
									foreach ($user['Post'] as $key => $post) {
									    	if($post['is_approve']==1){
									    		$count = $count + 1;
									    	}
									    }   
								?>	
								<?php 
								if($user['User']['first_name']!='' && $user['User']['last_name']!=''){ ?>
									<li class="user_list" data-id="<?php echo $user['User']['id'];?>" ><a href="javascript:void(0)"><?php echo $user['User']['first_name'].' '.$user['User']['last_name'];?><span class="pull-right">(<?php echo $count;?>)</span></a></li>
							<?php 	}
							} ?>	
							</ul>
							</div>
							<div class="">
								<span class="price_level">Price:<br>
									<select class="selectfield1" id = "price_level" >
										<option value="" > Select Price </option>
										<option value="0" > Free </option>
										<option value="1" > Paid </option>
									</select>
								</span>
                                <span class="show1 slider-div" >
										<label for="amount">Price range:</label>
				  						<input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
				  						<div class="slider-div" id="slider-range"></div>
			  					</span>
							</div>-->	
						</div>
					</div>
				</div>
		<div class="col-md-9">
	         <!--start course list-->
	        <div id="product" class="flxwarp popular-courses infiniteScroll">
	    	     	<?php foreach ($courses as $key => $val) { ?>
	    	     	  	<div class="loop flxcontent">
	                      	<div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $val['Post']['slug']; ?>'">
                            <div class="img-holder"><img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $val['PostImage']['0']['originalpath']; ?>" style="height: 140px;" class="img-responsive" alt="" /></div>
                            <div class="text">
                                <h4><a href="javascript:void(0);"><?php 
                                	  
                                	  if(strlen($val['Post']['post_title'])<=30)
                                      {
                                          echo $val['Post']['post_title'];
                                      }
                                      else
                                      {
                                          $y=substr($val['Post']['post_title'],0,30) . '...';
                                          echo $y;
                                      }
                                
                                //echo $val['Post']['post_title']; 
                                ?></a></h4>
                                <!--<div class="img img-circle">
                                    <img src="<?php if (isset($val['User']['user_image']) && $val['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                                        echo $val['User']['user_image'];
                                    } else {
                                        echo $this->webroot;
                                        ?>img/profile_img.jpg<?php } ?>" alt="Image"></div>-->
                                <?php
                                if (!empty($val['User'])) {
                                ?>
                                <h6><?php echo $val['User']['first_name'] . ' ' . $val['User']['last_name']; ?></h6>
                                <?php
                                }
                                ?>
                                <ul>
                                    <?php 
                                            if(isset($val['Rating']) && $val['Rating']!=''){
                                                $b = count($val['Rating']);
                                            
                                                $a=0;
                                                foreach ($val['Rating'] as $value) {
                                                    $a=$a + $value['ratting'];
                                                }
                                            }   
                                            $finalrating='';
                                            if($b!=0){
                                                $finalrating = ($a / $b);
                                            }

                                             if(isset($finalrating) && $finalrating!='') {  
                                                for($x=1;$x<=$finalrating;$x++) { ?>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                <?php }
                                                if (strpos($finalrating,'.')) {  ?>
                                                    <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                                <?php  $x++;
                                                }
                                                while ($x<=5) { ?>
                                                    <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                <?php $x++;
                                                } 
                                              }else { ?>
                                                    <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                    <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <?php } ?>
                                   <!--  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li> -->                        
                                </ul>
                                <!--<p><?php echo $val['User']['email_address']; ?> </p>-->
                                <div class="clear-fix">
                                    <ul class="float-left">
                                       <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>-->
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $val['User']['address']; ?></li>
                                        <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                          <?php 
                                          if($val['Post']['price']==0){
                                            echo 'Free';  
                                          }else{
                                          	$price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($val['Post']['currency_type'],$val['Post']['price'])));
                                          	//echo '<pre>'; print_r($price); echo '</pre>';
                                            echo $price['symbol'].round($price['price']);  
                                            //echo '$'.round($val['Post']['price']);  
                                          }  
                                           ?>
                                        </a></li>
                                    </ul>

                                    
                                </div>
                            </div> 
                        </div>
	                  	</div>
	                <?php } ?> 
	                <?php echo $this->Paginator->next(); ?>  
	          </div>
	         <!--end course list-->         
		</div>
	</div>   
</section>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
});
</script>
<script>

$(document).ready(function(){
	var search = '<?php echo $courseSearch; ?>';
	
	$("#headerSearch,#searchHeader").val(search);
	if(search==''){
		$("#searchHeader").text('All Courses');	
	}else{
		$("#searchHeader").text(search);		
	}
})

$(window).scroll(function(){
  if ($(window).scrollTop() >= 130) {
      $('.header').addClass('fixed');
  }
  else {
     $('.header').removeClass('fixed');
  }

  var classlist =  jQuery('#products > div').attr('class');
	console.log(classlist);
    if (classlist == 'item  col-xs-4 col-lg-4')
        {
            jQuery('#grid').trigger('click');
        }
        else if (classlist == 'item  col-xs-4 col-lg-4 list-group-item')
        {
            jQuery('#list').trigger('click');
        }

});


$(".category_list").click(function(){
    var category_id = $(this).data('id');
    $("#searchHeader").text($(this).data('catname'));
    // var user_id = '';
    // var show = '';
    // var short_by = '';
    $.ajax({
        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
        type: 'post',
        dataType: 'json',
        data: {category_id:category_id},
        success: function(data) {
        	if(data.ack==1){
        		$("#product").html('');
        		$("#product").html(data.data);
        		$(".category_list").css("font-weight","300");
        		$('a[data-id="'+category_id+'"]').css("font-weight","600");
        		$("#headerSearch").val('');
        		$('#product').removeClass('infiniteScroll');
        	}
        	else{
        		$("#product").html('');
        		$("#product").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
        	}
        }
    });
});

$(".sub_category").click(function(){
    var sub_category_id = $(this).data('id');
    $("#searchHeader").text($(this).data('subcat'));
    // var user_id = '';
    // var show = '';
    // var short_by = '';
    $.ajax({
        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
        type: 'post',
        dataType: 'json',
        data: {sub_category_id:sub_category_id},
        success: function(data) {
        	if(data.ack==1){
        		$("#products").html('');
        		$("#products").html(data.data);
        		$('#product').removeClass('infiniteScroll');
        		//$(".category_list").css("font-weight","300");
        		//$('a[data-id="'+category_id+'"]').css("font-weight","600");
        	}
        	else{
        		$("#products").html('');
        		$("#products").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
        	}
        }
    });
});



$(".user_list").click(function(){
	//var category_id = '';
    var user_id = $(this).data('id');
    // var show = '';
    // var short_by = '';
    // var order_filter = '';
    // var price
    $.ajax({
        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
        type: 'post',
        dataType: 'json',
        data: {user_id:user_id},
        success: function(data) {
        	if(data.ack==1){
        		$("#products").html('');
        		$("#products").html(data.data);
        		$('#product').removeClass('infiniteScroll');
        	}
        	else{
        		$("#products").html('');
        		$("#products").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
        	}            
        }
    });
});
$(".selectfield").change(function(){
	var show = $(this).val();
    
    $.ajax({
        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
        type: 'post',
        dataType: 'json',
        data: {show:show},
        success: function(data) {
        	if(data.ack==1){
        		$("#products").html('');
        		$("#products").html(data.data);
        		$('#product').removeClass('infiniteScroll');
        	}
        	else{
        		$("#products").html('');
        		$("#products").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
        	}            
        }
    });
});

$("#order_filter,#skill_level,#price,#instructor").change(function(){
	console.log('hi....');
	var order_filter = $("#order_filter").val();
	var skl_lvl = $("#skill_level").val();
	var price = $("#price").val();
	var user_id = $("#instructor").val();
    $.ajax({
        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
        type: 'post',
        dataType: 'json',
        data: {
        	order_filter:order_filter,
        	skill_level:skl_lvl,
        	price_level:price,
        	user_id:user_id
        },
        success: function(data) {
        	if(data.ack==1){
        		$("#product").html('');
        		$("#product").html(data.data);
        		if(data.sort!=''){
        			$('#order_filter > option[value="'+data.sort+'"]').attr("selected", "selected")
        		}
        		$('#product').removeClass('infiniteScroll');
        	}
        	else{
        		$("#product").html('');
        		$("#product").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
        	}            
        }
    });
});

$( function() {
	var max_value = $("#max_price").val();
    $( "#slider-range" ).slider({
	  range: true,
      min: 0,
      max: max_value,
      values: [ 0, max_value ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      },
      change: function(event, ui) { 
        var slider_range = $( "#slider-range" ).slider( "values" );
	    $.ajax({
	        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
	        type: 'post',
	        dataType: 'json',
	        data: {slider_range:slider_range},
	        success: function(data) {
	        	if(data.ack==1){
	        		$("#products").html('');
	        		$("#products").html(data.data);
	        		$('#order_filter > option[value="'+data.sort+'"]').attr("selected", "selected");
	        		$('#product').removeClass('infiniteScroll');
	        	}
	        	else{
	        		$("#products").html('');
	        		$("#products").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
	        	}            
	        }
	    }); 
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
});


	//----Infinite Scrool---//
	$('.next').hide();
	var $container = $('.infiniteScroll');
	$container.infinitescroll({
	  	navSelector  : '.next',    // selector for the paged navigation
	  	nextSelector : '.next a',  // selector for the NEXT link (to page 2)
	  	itemSelector : '.loop',    // selector for all items you'll retrieve
	  	debug        : true,
	  	dataType     : 'html',
	  	loading		 : {
	      	finishedMsg: 'No More Courses to load.',
	      	//img: '<?php echo $this->webroot; ?>img/ajax-loader.gif'
	    }
	});	

	

$("#price_level").change(function(){
	var price = $(this).val();
    if(price==1){
    	$(".slider-div").css("display", "block");
    	//$(".price_level").css("display", "none");
    }
    if(price==0){
    	$(".slider-div").css("display", "none");
    	$.ajax({
	        url: '<?php echo $this->webroot;?>posts/ajaxCourseSearch',
	        type: 'post',
	        dataType: 'json',
	        data: {free_price:price},
	        success: function(data) {
	        	if(data.ack==1){
	        		$("#products").html('');
	        		$("#products").html(data.data);
	        		$('#order_filter > option[value="'+data.sort+'"]').attr("selected", "selected");
	        		$('#product').removeClass('infiniteScroll');
	        	}
	        	else{
	        		$("#products").html('');
	        		$("#products").html('<div class="row"><div class="col-md-12">No Search Result</div><div>');
	        	}            
	        }
	    });
    }    
});




</script>