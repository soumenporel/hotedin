<div class="contents form">
<?php 
//echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('WpPage'); ?>
    <fieldset>
        <legend><?php echo __('Edit Content'); ?></legend>
	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('title');
		echo $this->Form->input('heading');
        echo $this->Form->input('page_url');
		// echo $this->Form->input('show_in_header');
        // echo $this->Form->input('show_in_footer');
	?>
        <textarea name="data[WpPage][content]" id="Contentcontent" style="width:900px; height:600px;" class="validate[required]" ><?php echo $this->request->data['WpPage']['content']; ?></textarea>
        <!-- <textarea name="data[CmsPage][page_description_sp]" id="Contentcontent_sp" style="width:900px; height:600px;" class="validate[required]" ><?php //echo $this->request->data['CmsPage']['page_description_sp']; ?></textarea> -->
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('Contentcontent',
            {
                width: "95%"
            });
    // CKEDITOR.replace('Contentcontent_sp',
    //         {
    //             width: "95%"
    //         });
</script>

<?php //echo $this->element('admin_sidebar'); ?>
