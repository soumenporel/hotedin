<div class="contents form">
<?php
//echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('WpPage'); ?>
	<fieldset>
		<legend><?php echo __('Add New Page'); ?></legend>
	<?php
        echo $this->Form->input('category_id',array('required'=>'required','options'=>$categories));
		echo $this->Form->input('title',array('required'=>'required'));
		echo $this->Form->input('heading',array('required'=>'required'));
        echo $this->Form->input('page_url',array('required'=>'required'));
		// echo $this->Form->input('show_in_header');
        // echo $this->Form->input('show_in_footer');
        // echo $this->Form->input('status');
	?>
    <textarea name="data[WpPage][content]" id="Contentcontent" style="width:900px; height:600px;" class="validate[required]" ></textarea>
   </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
    <script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'Contentcontent',
        {
        width: "95%"
        });
    </script>
<?php //echo $this->element('admin_sidebar'); ?>