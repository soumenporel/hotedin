<div class="contents view">
<h2><?php echo __('CMS Page'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($content['WpPage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Page Name'); ?></dt>
		<dd>
			<?php echo h($content['WpPage']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Page Heading'); ?></dt>
		<dd>
			<?php echo h($content['WpPage']['heading']); ?>
			&nbsp;
		</dd>		
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo (nl2br($content['WpPage']['content'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php //echo $this->element('admin_sidebar'); ?>