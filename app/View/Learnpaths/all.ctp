<style>
.learnpaths-header {
    position: relative;
    overflow: hidden;
    box-shadow: 0 5px 10px 0 rgba(0,0,0,.2);
	color:#000;
}
.learnpaths-header .pathContent {
    position: relative;
    z-index: 2;
}
.learn-link{color:#000;}
.learn-link:hover{color:#444; text-decoration:underline;}
@media (min-width: 768px){
.learnpaths-header:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
background: #ffffff; /* Old browsers */
background: -moz-linear-gradient(left, #ffffff 36%, rgba(0, 172, 178, 0.8) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(left, #ffffff 36%,rgba(0, 172, 178, 0.8) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right, #ffffff 36%,rgba(0, 172, 178, 0.8) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='rgba(37, 198, 212, 0.48)',GradientType=1 );
    z-index: 1;
}
}
.pathStats {
    background-color: rgba(0,0,0,0.4);
    position: relative;
    z-index: 2;
    margin-top: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    font-size: 15px;
    color:#fff;
}
.developmentBoxTwo {
    float: right;
    width: 75%;
    background: #fafafa;
    padding: 8px 25px 20px 25px;
}
.developmentRow{transition:ease 0.5s;}
.developmentRow:hover{box-shadow:0 0 15px #ccc;}
</style>
<section class="container padding-top-40 padding-btm-40">
    
    <?php 
   // print_r($learningPaths);die;
    foreach ($learningPaths as $key => $learnpath) { 
        //pr($post);
    ?>
	    <div class="developmentRow" onclick="javascript:window.location.href = '<?php echo $this->webroot.'learnpaths/index/'.$learnpath['Learnpath']['slug']; ?>'" style="cursor:pointer;">
        	        <div class="developmentBoxOne">
        	           <img src="<?php if(isset($learnpath['Learnpath']['image']) && $learnpath['Learnpath']['image'] !='') { echo $this->webroot; ?>img/learnpath_img/<?php echo $learnpath['Learnpath']['image']; } else { echo $this->webroot.'noimage.png'; } ?>" style="height:152px;" alt="">
        	        </div>
        	        <div class="developmentBoxTwo">
        	            <div class="row">
        	                <div class="col-sm-9 col-md-10">
            		            <h3><?php echo $learnpath['Learnpath']['title']; ?></h3>
            		            <p class="hrs"><i class="fa fa-book"></i>&nbsp;<strong><?php
                      if(!empty($learnpath['Post'])){
                        echo count($learnpath['Post']);
                      }else {
                        echo '0';
                      }
                       ?> Learning Paths</strong> </p>
        		            </div>
                		   
        	            </div>
        	        </div>
        	    </div>
	<?php } ?>
	

</section>
