<style>
.learnpaths-header {
    position: relative;
    overflow: hidden;
    box-shadow: 0 5px 10px 0 rgba(0,0,0,.2);
	color:#000;
}
.learnpaths-header .pathContent {
    position: relative;
    z-index: 2;
}
.learn-link{color:#000;}
.learn-link:hover{color:#444; text-decoration:underline;}
@media (min-width: 768px){
.learnpaths-header:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
background: #ffffff; /* Old browsers */
background: -moz-linear-gradient(left, #ffffff 36%, rgba(0, 172, 178, 0.8) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(left, #ffffff 36%,rgba(0, 172, 178, 0.8) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right, #ffffff 36%,rgba(0, 172, 178, 0.8) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='rgba(37, 198, 212, 0.48)',GradientType=1 );
    z-index: 1;
}
}
.pathStats {
    background-color: rgba(0,0,0,0.4);
    position: relative;
    z-index: 2;
    margin-top: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    font-size: 15px;
    color:#fff;
}
.developmentBoxTwo {
    float: right;
    width: 75%;
    background: #fafafa;
    padding: 8px 25px 20px 25px;
}
.developmentRow{transition:ease 0.5s;}
.developmentRow:hover{box-shadow:0 0 15px #ccc;}
</style>
<section class="learnpaths-header">
 <div class="pathContent" style="background-size: cover !important; background: url(<?php echo $this->webroot.'img/learnpath_img/'.$learnpath['Learnpath']['image']; ?>) no-repeat right;">
  <div class="container">
 <div class="row">
  <div class="col-md-7" style="background:rgba(255, 255, 255, 0.9); padding:20px 30px">
    <p class="padding-top-10"><a href="#" class="learn-link">Learning Paths</a> <i class="fa fa-angle-right"></i></p>
	<!-- <h2><strong>Become a Manager</strong></h2>
	<p>As a manager, success is predicated on your ability to achieve goals working with your team. The good news is that management is a skill that can be learned. This path is designed to provide you with the key considerations, skills and competencies to help you become and succeed as a manager.</p>

	<div class="row">
	  <div class="col-sm-4">
	    <strong>Learn</strong> the critical skills and competencies for new managers.
	  </div>
	  <div class="col-sm-4">
	    <strong>Discover</strong>  essential interview techniques and questions.
	  </div>
	  <div class="col-sm-4">
	    <strong>Create</strong>  cultures of motivation and accountability.
	  </div>
	</div> -->
	<?php echo $learnpath['Learnpath']['description']; ?>
<!--	<p class="padding-top-20">
	   <a href="#" class="btn btnPrimarys" style="padding: 8px 15px !important; font-size: 16px;">Try for free</a>
	   <a href="#" class="btn btnPrimary" style="padding: 8px 15px !important; font-size: 16px; color:#000 !important;"><i class="fa fa-play"></i>  Preview the first video</a>
	</p>
	<p>Part of your Lynda.com membership.</p>	 -->
  </div>
  </div>
  </div>
  <div class="pathStats">
	<div class="container">
	  <div class="row">
	    <div class="col-sm-3">
		  <div class="media">
		    <div class="media-left mr-2"><i class="fa fa-clock-o"></i></div>
			<div class="media-body">
			  <strong>
			  <?php
			  	echo count($learnpath['Post']);
			  ?> Courses </strong> of expert-created tutorials
			</div>
		  </div>
		</div>
		<div class="col-sm-3">
		  <div class="media">
		    <div class="media-left mr-2"><i class="fa fa-file-text"></i></div>
			<div class="media-body">
			  <strong>Practice </strong> to reinforce what you learned

			</div>
		  </div>
		</div>
		<div class="col-sm-3">
		  <div class="media">
		    <div class="media-left mr-2"><i class="fa fa-certificate"></i></div>
			<div class="media-body">
			  <strong>Certify</strong> your achievement
			</div>
		  </div>
		</div>
		<div class="col-sm-3">
		  <div class="media">
		    <div class="media-left mr-2"><i class="fa fa-cogs"></i></div>
			<div class="media-body">
			  <strong>Advance</strong>  your career
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
</section>
<section class="container padding-top-40 padding-btm-40">

    <?php foreach ($learnpath['Post'] as $key => $post) {
        //pr($post);
    ?>
	    <div class="developmentRow" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?>posts/details/<?php echo $post['slug']; ?>'" style="cursor:pointer;">
        	        <div class="developmentBoxOne">
        	           <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $post['PostImage']['0']['originalpath']; ?>" style="height:152px;" alt="">
        	        </div>
        	        <div class="developmentBoxTwo">
        	            <div class="row">
        	                <div class="col-sm-9 col-md-10">
            		            <h3><?php echo $post['post_title']; ?></h3>
            		            <?php echo $post['User']['first_name'].' '.$post['User']['last_name']; ?> • <?php echo substr(strip_tags($post['User']['biography']), 0, 200).'...'; ?>
                                <p><?php echo $post['post_subtitle']; ?></p>
            		            <p>
            		                <i class="fa fa-play-circle"></i>
                                    <?php
                                        $noLecture = count($post['Lecture']);
                                        echo $noLecture;
                                    ?> lectures&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			            <!-- <i class="fa fa-clock-o"></i> 55 7 hours&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            			            <i class="fa fa-filter"></i> 55 lectures -->
            		            </p>
        		            </div>
                		    <div class="col-sm-3 col-md-2">

<!--                                <h2 style="margin-top-30">
                                    <?php if($post['price']!=0){ ?>
                                        <strong><?php $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($post['currency_type'],$post['price'])));
                                            echo $price['symbol'].round($price['price']);  ?>$<?php echo $post['price']; ?></strong>
                                    <?php }else{ ?>
                                        <strong>Free</strong>
                                    <?php } ?>
                                </h2>-->

                                <p>
                                    <!-- <i class="fa fa-star" aria-hidden="true" style="color: green; padding-right:1px !important;"></i>
                                    <i class="fa fa-star" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <i class="fa fa-star" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i> -->
                                    <?php
                                        if(isset($post['Rating']) && $post['Rating']!=''){
                                            $b = count($post['Rating']);

                                            $a=0;
                                            foreach ($post['Rating'] as $value) {
                                                $a=$a + $value['ratting'];
                                            }
                                        }
                                        $finalrating='';
                                        if($b!=0){
                                            $finalrating = ($a / $b);
                                        }

                                         if(isset($finalrating) && $finalrating!='') {
                                            for($x=1;$x<=$finalrating;$x++) { ?>
                                                <i class="fa fa-star" aria-hidden="true" style="color: green; padding-right:1px !important;"></i>
                                            <?php }
                                            if (strpos($finalrating,'.')) {  ?>
                                                <i class="fa fa-star-half-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                            <?php  $x++;
                                            }
                                            while ($x<=5) { ?>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                            <?php $x++;
                                            } ?>
                                            &nbsp;&nbsp;<?php //echo $finalrating; ?>
                                        <?php
                                          }else { ?>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <?php } ?>
                                </p>
                				<p class="light-gray">
                                    <small>(
                                    <?php
                                       $ratCount = count($post['Rating']);
                                       echo $ratCount;
                                    ?> ratings)</small>
                                </p>
                		    </div>
        	            </div>
        	        </div>
        	    </div>
	<?php } ?>


</section>
