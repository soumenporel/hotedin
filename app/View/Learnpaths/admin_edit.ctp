<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="categories form">
<?php echo $this->Form->create('Learnpath',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Learnpath'); ?></legend>

	<?php

		echo $this->Form->input('id');
		echo $this->Form->input('title',array('required'=>'required'));
		echo $this->Form->input('description',array('id'=>'cat_desc'));
		echo $this->Form->input('Post', array('label' => 'Add Courses','class' => 'selectpicker','multiple'=>'multiple'));
		echo $this->Form->input('show_in_homepage');
		echo $this->Form->input('image',array('type'=>'file'));
	?>
	<div style="display:none;"><input type="hidden" name="hidden_img" value="<?php echo $this->request->data['Learnpath']['image']; ?>"></div>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('cat_desc',
            {
                width: "95%"
            });

$(document).ready(function() {

	$(".selectpicker").select2();
});

</script>