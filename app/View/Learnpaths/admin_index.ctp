<div class="categories index">
    <h2><?php echo __('Learnpath'); ?></h2>
    <div>
       
        <form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">   
            <table style=" border:none;">
                <tr>
                    <td>Keyword</td>
                    <td><input type="text" name="keyword" value="<?php echo isset($keywords)?$keywords:'';?>" placeholder="Search by Keyword."></td>
                    
                    <td><input type="submit" name="search" value="Search"></td>
                </tr> 
                 <tr><a href="<?php echo($this->webroot);?>admin/learnpaths/add" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add Learnpath</a>
            </tr>      
            </table>
        </form>
        <?php //echo $this->Form->end();?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('image'); ?></th>
            <th><?php echo $this->Paginator->sort('title'); ?></th>
            <th><?php echo $this->Paginator->sort('description'); ?></th>
            <th><?php echo $this->Paginator->sort('show_in_homepage'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
	<?php 
        $CatCnt=0;
        foreach ($learnpaths as $category): 
            $CatCnt++;
	//pr($category);
	?>
        <tr>
            <td><?php echo $CatCnt;//echo h($category['Category']['id']); ?>&nbsp;</td>
            <td><img src="<?php echo $this->webroot; ?>img/learnpath_img/<?php echo $category['Learnpath']['image']; ?>" style="height:30px;" /></td>
            <td><?php echo $category['Learnpath']['title'];?></td>
            <td><?php echo $category['Learnpath']['description'];?></td>
            <td><?php echo h($category['Learnpath']['show_in_homepage']==1?'Yes':'No'); ?>&nbsp;</td>
            
            <td>
            <?php       
                        // echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')),
                        // array('action' => 'view', $category['Learnpath']['id']),
                        // array('class' => 'btn btn-info btn-xs', 'escape'=>false)); 
            ?>
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                        array('action' => 'edit', $category['Learnpath']['id']),
                        array('class' => 'btn btn-info btn-xs', 'escape'=>false)); ?>
            <?php echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                        array('action' => 'delete', $category['Learnpath']['id']),
                        array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
                        __('Are you sure you want to delete # %s?', $category['Learnpath']['id'])); ?>
        </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
