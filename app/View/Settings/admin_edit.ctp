<div class="sitesettings form">
<?php echo $this->Form->create('Setting'); ?>
    <fieldset>
        <legend><?php echo __('Edit Site Setting'); ?></legend>
	<?php
        echo $this->Form->input('hidpw', array('type' => 'hidden', 'value' => $this->request->data['Setting']['id']));
        echo $this->Form->input('id');
        echo $this->Form->input('site_name');
        // echo $this->Form->input('paypal_email');
        echo $this->Form->input('site_email');
        echo $this->Form->input('site_url');
        //echo $this->Form->input('bannertext');
        echo $this->Form->input('address');
        echo $this->Form->input('phone');
//        echo $this->Form->input('video_no',array('label' => 'Each instructor max video upload limit'));
//        echo $this->Form->input('video_size',array('label' => 'Video max size in MB'));
//        echo $this->Form->input('bank_name',array('label' => 'Bank Name'));
//        echo $this->Form->input('branch',array('label' => 'Branch Name'));
//        echo $this->Form->input('acc_no',array('label' => 'Account no'));
//        echo $this->Form->input('holders_name',array('label' => 'Account holders Name'));
	?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
