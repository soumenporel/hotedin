<?php //echo '<pre>'; print_r($vacancy); ?>
<div class="row-fluid">
    <div class="span12">
        <div class="categories index">
            <h2><?php echo __('View Category'); ?></h2>
            
            <div class="widget blue">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i>Category</h4>
                    <span class="tools">
                        <a href="javascript:;" class="icon-chevron-down"></a>
                        <a href="javascript:;" class="icon-remove"></a>
                    </span>
                </div>
                <div class="widget-body">
                    <table class="table table-striped table-bordered" id="sample">
                        <thead>
                            <tr>                           
                                <th>Id</th>
                                <th>Category Name</th>
                                <th>Category Description</th>
                                <th>Image</th>
                                <th>Is Principal</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                                <tr class="odd gradeX">
                                    
                                    <td><?php echo h($category['Category']['id']); ?></td>
                                    <td><?php echo h($category['Category']['category_name']); ?></td>
                                    <td><?php echo strip_tags($category['Category']['category_description']); ?></td>
                                    
                                    <td><?php
										if(isset($category['CategoryImage']['0']['originalpath']) and !empty($category['CategoryImage']['0']['originalpath']))
										{
										?>
										<img alt="" src="<?php echo $this->webroot;?>img/cat_img/<?php echo $category['CategoryImage']['0']['originalpath'];?>" style=" height:80px; width:80px;">
										<?php
										}
										else{
										?>
									   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

									   <?php } ?>
								   </td>
								   <td><?php echo h($category['Category']['is_principal']==1?'Yes':'No'); ?></td>
								   <td><?php echo h($category['Category']['status']==1?'Active':'Inactive'); ?></td>
                                    
                                </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
