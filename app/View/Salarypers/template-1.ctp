<?php

if(!empty($cat_details['Product']))
{
?>
<section class="sub-menu" id="subb_part">
    <div class="container">
        <div class="row">
            <div class="col-md-6 middle-div">

                <?php
                foreach($cat_details['Children'] as $child)
                {
                ?>
                <div class="one_sub text-center" style="cursor:pointer;">
                    <img src="<?php echo $this->webroot ?>img/cat_img/<?php echo $child['CategoryImage'][0]['originalpath']; ?>" style="margin: 5px auto;" class="img-responsive"
                         onclick="location.href = '<?php echo $this->webroot . 'categories/' . $child['slug']; ?>'"/>
                    <p class="sub_para" onclick="location.href = '<?php echo $this->webroot . 'categories/' . $child['slug']; ?>'"><?php echo $child['category_name']; ?></p>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
</section>
<?php
}
else
{
	echo "<section class='tvshow text-center' style='min-height:500px;margin-top:20px;'><div class='watchseries2 text-center'><span class='alert alert-danger'>Sorry No Product Found.</span></div></section>";
}
?>

<?php
if(!empty($cat_details))
{
$i=0;	
foreach($cat_details['Product'] as $cat_detail)
{	

if($i==6)
{
$i=0;
}

if($i==0)
{
?>
<section class="products product1" style="background: rgba(0, 0, 0, 0) url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat scroll center 200px;height: 810px;position: relative;">
    <div class="container">
        <div class="product-container text-center">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center pro-text">
                        <h2><?php echo $cat_detail['product_name']; ?></h2>
                        <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                        <span class="learn_more">
                            <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                            <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
		<?php
	}
	
if($i==1)
{
?>

<section class="products product2" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat right top; height: 577px; width: 100%;">
    <div class="container">
        <div class=" product-container text-center">
            <div class="row">
                <div class="col-md-12">
                    <div class="pro-text text-center">
                        <h2><?php echo $cat_detail['product_name']; ?></h2>
                        <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                        <span class="learn_more">
                            <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                            <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
		<?php
	}
	
if($i==2)
{
?>
<section class="products product3" style="background: rgba(0, 0, 0, 0) url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat scroll left top;height: 582px;margin-left: 0px;width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pro-text text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                </div>

            </div>
        </div>
    </div>
</section>

<?php
}

if($i==3)
{
?>

<section class="products product4" style="background: #f1f1f1 url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat scroll right 80px;height: 725px; margin-top: 40px;padding: 80px 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pro-text text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                </div>

            </div>
        </div>
    </div>
</section>
<?php 
} 
if($i==4)
{
?>
<div class="clearfix"></div>
<section class="products product5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="inner_prod1">
                    <img src="<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="pro-text pro-text1 text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
		<?php } 

if($i==5)
{
?>	
<section class="products product6" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat top left; margin-top:50px; height: 728px; margin-left: -300px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pro-text text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                </div>

            </div>
        </div>
    </div>
</section>
<?php 
} 
$i++;
  }
}
else
{
	echo "No Product Found.";
}
?>