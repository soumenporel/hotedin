<?php

if(!empty($cat_details['Product']))
{
?>
<section class="sub-menu" id="subb_part">
    <div class="container">
        <div class="row">
            <div class="col-md-6 middle-div">

                <?php
                foreach($cat_details['Children'] as $child)
                {
                ?>
                <div class="one_sub text-center" style="cursor:pointer;">
                    <img src="<?php echo $this->webroot ?>img/cat_img/<?php echo $child['CategoryImage'][0]['originalpath']; ?>" style="margin: 5px auto;" class="img-responsive"
                         onclick="location.href = '<?php echo $this->webroot . 'categories/' . $child['slug']; ?>'"/>
                    <p class="sub_para" onclick="location.href = '<?php echo $this->webroot . 'categories/' . $child['slug']; ?>'"><?php echo $child['category_name']; ?></p>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
</section>
<?php
}
else
{
	echo "<section class='tvshow text-center' style='min-height:500px;margin-top:20px;'><div class='watchseries2 text-center'><span class='alert alert-danger'>Sorry No Product Found.</span></div></section>";
}
?>


<?php
if(!empty($cat_details))
{
$i=0;
$j=0;	
foreach($cat_details['Product'] as $cat_detail)
{	

if($i==5)
{
$i=0;$j=0;
}

if($i==0)
{
?>
<section class="watchpart">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="watchseries2 text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <figure class="image hero-image" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat center bottom; height: 591px;"></figure>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
}

if($i>0)
{
	
	if($j==0)
	{
?>

<section class="section section-watch">
    <div class="container">
        <div class="row">

<?php 
}
if($i==1)
{
	?>
            <div class="col-md-6">
                <div class="watchpart1 watchseries2 text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <figure class="image second-image" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat center top; height: 550px; margin-top:30px;"></figure>
                </div>
            </div>
					<?php } ?>

                   <?php 
if($i==2)
{
	?>
            <div class="col-md-6">
                <div class="watchpart1 watchseries2 text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <figure class="image second-image1" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat center top; height: 550px; margin-top:30px;"></figure>
                </div>
            </div>
					<?php } ?>

					<?php if(count($cat_details['Product'])==2 || $i==2)
                     {
					 ?>

        </div>
    </div>
</section>
<?php
}
$j++;
}
?>

<?php
if($i==3)
{
?>
<section class="section section-watch">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="watchpart1 watchseries2 text-center">
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <figure class="image third-image" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat center top; height: 370px; margin-top:30px;"></figure>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
}
?>

<?php
if($i==4)
{
?>
<section class="section section-watch">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="watchpart1 watchseries2 text-center">
                    <figure class="image fourth-image" style="background: url('<?php echo $this->webroot ?>img/cat_img/<?php echo $cat_detail['ProductImage'][0]['originalpath']; ?>') no-repeat center top; height: 577px;"></figure>
                    <h2><?php echo $cat_detail['product_name']; ?></h2>
                    <p><?php echo substr($cat_detail['product_description'],0,150); ?></p>
                    <span class="learn_more">
                        <a href="#">Learn More <i class="fa fa-chevron-right"></i></a>
                        <a href="#">Buy <i class="fa fa-chevron-right"></i></a>
                    </span>

                </div>
            </div>
        </div>
    </div>
</section>
		<?php
	}
		?>


<?php 
$i++;
  }
}
?>