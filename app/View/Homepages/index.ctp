<section class="slider">
      <div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
        <?php $count = -1 ?>
        <?php foreach ($banners as $banner) { ?>
          <?php $count++; ?>
          <?php foreach ($banner as $bannerDetails) { ?>
          <?php if($count<1) { ?>
            <li data-target="#demo" data-slide-to="<?php echo $count ?>" class="active"></li>
          <?php }else { ?>
            <li data-target="#demo" data-slide-to="<?php echo $count ?>" ></li>
          <?php } ?>
          <?php }

          } ?>
        </ul>
        <!-- The slideshow -->

        <div class="carousel-inner">
        <?php $flag = -1 ?>
        <?php foreach ($banners as $banner) { ?>
        <?php $flag++; ?>
        <?php foreach ($banner as $bannerDetails) { ?>
        <?php if($flag<1)  {?>
          <div class="carousel-item active">
            <img src="<?php echo $this->Html->url('/');?>banner/<?php echo $bannerDetails['image']  ?>" alt="<?php echo $bannerDetails['title']  ?>">
          </div>
          <?php }else { ?>
          <div class="carousel-item">
            <img src="<?php echo $this->Html->url('/');?>banner/<?php echo $bannerDetails['image']  ?>" alt="<?php echo $bannerDetails['title']  ?>">
          </div>
          <?php } ?>
        <?php }

         } ?>

        </div>
      </div>
    </section>
    <!--end of slider-->

    <div class="form-banner">
      <div class="container">
        <div class="top-heading">
          <h1><a href="#">New to Hotedin.com?</a></h1>
          <h3>Save <strong>20%</strong> on your first job when you buy online</h3>
        </div>
        <div class="form-wrap">
          <div class="form-body clearfix">
              <form class="" action="<?php echo $this->webroot ?>jobs/jobsearch" method="post">
                  <input type="text" name="title" class="form-control pull-left" id="srch" placeholder="job title,skill or compnay">
                  <input type="text" name="country" class="form-control pull-left" id="cntry" placeholder="Country City or Postal Code">
                  <input type="number" name="location" class="form-control pull-left" id="miles" placeholder="10 miles">
                  <input type="submit" class="btn btn-primary pull-left" id="submit" value="Search">
              </form>
          </div>
          <div class="text-center">
            <a href="" class="btn btn-primary round-btn">More Option</a>
          </div>
        </div>
      </div>

      <div class="container-new">
        <div class="tabs-cont mt-2">
            <div class="tabs-body">
              <ul class="nav nav-tabs">
                <li><a class="active" data-toggle="tab" href="#home">Job By Specialism</a></li>
                <li><a class="" href="#menu" data-toggle="tab">Job By Location</a> </li>
                <li><a class="" href="" data-toggle="tab">Job By Company</a></li>
                <li><a class="" href="" data-toggle="tab"> Popular Searches</a></li>
                <li><a class="" href="" data-toggle="tab">Apprenticeships</a></li>
                <li><a class="" href="" data-toggle="tab">Ourly Paid Jobs</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="home">
                  <div class="row">
                    <div class="col-12 col-md-3">
                      <div class="tab-box">
                        <h2> <i class="icon ion-android-unlock"></i> Hotels</h2>
                        <ul>
                          <li><a href="#">Concierge <span>(278)</span></a></li>
                          <li><a href="#">Front Of House <span>(278)</span></a></li>
                          <li><a href="#">Hotel Management <span>(2,636)</span></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-12 col-md-3">
                      <div class="tab-box">
                        <h2><i class="icon ion-spoon"></i> Restaurants</h2>
                        <ul>
                          <li><a href="#">Kitchen Assistant <span>(278)</span></a></li>
                          <li><a href="#">Kitchen Manager <span>(278)</span></a></li>
                          <li><a href="#">Restaurant Management <span>(2,636)</span></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-12 col-md-3">
                      <div class="tab-box">
                        <h2> <i class="icon ion-wineglass"></i> Pubs & Bars</h2>
                        <ul>
                          <li><a href="#">Bar Attendant <span>(278)</span></a></li>
                          <li><a href="#">Bar General Manager <span>(278)</span></a></li>
                          <li><a href="#">Bar Management <span>(2,636)</span></a></li>
                        </ul>
                      </div>
                    </div>

                    <div class="col-12 col-md-3">
                      <div class="tab-box">
                        <h2><i class="icon ion-pizza"></i> Food Services</h2>
                        <ul>
                          <li><a href="#">Catering Management <span>(278)</span></a></li>
                          <li><a href="#">Catering Stuff<span>(278)</span></a></li>
                          <li><a href="#">Conference & Banqueting <span>(2,636)</span></a></li>
                        </ul>
                      </div>
                    </div>


                  </div>
                </div>

                <div class="tab-pane" id="menu">

                </div>
              </div>
            </div>
          </div>
      </div>

    </div>

    <!--cvs section-->
    <section class="video-cvs-section">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6 left-bg">
            <div class="cvsleft-box">
              <h1><?php echo $homecontent['ManageHomepage']['title'] ?></h1>
              <p><?php echo $homecontent['ManageHomepage']['description'] ?></p>
              <div class="cvsbtn"><a href="#">Know More</a></div>
            </div>
          </div>

          <div class="col-sm-6">
          <div class="video-box">
          <div class="row">
          <iframe width="100%" height="550" src="<?= $homecontent['ManageHomepage']['youtube_link'] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          </div>
          </div>



        </div>
      </div>
    </section>
    <section class="career-section">
      <div class="container">
          <div class="row">
          <h1>Find your next career</h1>
          <h2>move in 3 easy steps!</h2>
          <?php foreach ($howitworks as $howitwork) { ?>
              <div class="col-xs-4 col-sm-12 col-md-4 text-center hover-in">
                <img src="<?php echo $this->webroot; ?>howitworks_image/<?php echo $howitwork['Howitwork']['image'] ?>">
                <h3><?php echo $howitwork['Howitwork']['title'] ?></h3>
                <p><?php echo $howitwork['Howitwork']['description'] ?></p>
              </div>
          <?php } ?>
          <h5>Signing up is quick, easy and best of all – free!</h5>
          <div class="register-btn"><a href="#">REGISTER NOW</a></div>
          </div>
      </div>
    </section>
    <!--end csv section-->

 <!--about section-->

      <section class="video-cvs-section">
      <div class="container-fluid">
        <div class="row">

          <div class="col-sm-6">
          <div class="video-box">
          <div class="row">
          <iframe width="100%" height="550" src="<?php echo $homecontent1['ManageHomepage']['youtube_link'] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          </div>
          </div>

          <div class="col-sm-6 left-bg">
            <div class="cvsleft-box bottom-panle">
              <h1><?php echo $homecontent1['ManageHomepage']['title'] ?></h1>
              <p><?php echo $homecontent1['ManageHomepage']['description'] ?></p>
              <div class="cvsbtn"><a href="#">REGISTER TODAY</a></div>
            </div>
          </div>





        </div>
      </div>
    </section>
     <!--end about section-->

     <div class="employers-section">
      <div class="container">
        <div class="row">
          <h1><?php echo $homecontent2['ManageHomepage']['title'] ?></h1>
          <h2><?php echo $homecontent2['ManageHomepage']['subtitle'] ?></h2>
          <div class="clearfix"></div>

            <div class="col-sm-9 offset-sm-1 text-center">

              <p><?php echo $homecontent2['ManageHomepage']['description'] ?></p>

            </div>

        </div>
      </div>
     </div>
 <!--end employers section-->

 <section class="hotedin-section">
  <div class="container">
        <div class="row">
          <div class="col-sm-10 offset-sm-1 text-center">
            <div class="hotedin-box">
              <h1>New to Hotedin.com?</h1>
              <h2>Save <span>20%</span> on your first job when you buy online</h2>
              <div class="hotedin-btn"><a href="#">REGISTER TODAY</a></div>
            </div>
          </div>
        </div>
      </div>
 </section>
 <!--end hotedin section-->
<?php print_r(); ?>
 <section class="testimonial">
    <div class="container">
        <div class="row">
          <h2>What our clients say</h2>
            <h1>Testimonial</h1>

            <div class="clearfix"></div>
            <div class="owl-carousel owl-theme">
                <?php foreach ($Testimonials as $Testimonial) { ?>
                    <div class="item">
                      <div class="col-sm-10 offset-sm-1 text-center">
                      <?php echo $Testimonial['Testimonial']['description'] ?>
                      </div>
                      <div class="image-box"> <img src="<?php echo $this->webroot; ?>user_images/<?php echo $Testimonial['User']['user_image'] ?>"></div>
                      <div class="name-box"><?php echo $Testimonial['User']['first_name']; ?> <?php echo $Testimonial['User']['last_name']; ?></div>
                      <?php if($Testimonial['User']['company'] != '' && $Testimonial['User']['position'] != ''): ?>
                      <div class="designation"><?php echo $Testimonial['User']['position'] ?>, <?php echo $Testimonial['User']['company'] ?></div>
                  <?php endif; ?>
                    </div>
                <?php } ?>
          </div>
        </div>
      </div>
 </section>
  <!--end testimonial section-->
 <section class="clients-section">
  <div class="container">
        <div class="row">
          <h1> Our Clients</h1>
          <h2>what you need to know</h2>
          <ul class="clients">
            <?php foreach ($Clients as $Client) { ?>
                <li>
                  <div class="logo-box">
                    <img src="<?php echo $this->webroot; ?>client/<?php echo $Client['Client']['image'] ?>">
                  </div>
                  <p><?php echo $Client['Client']['name'] ?></p>
                </li>
            <?php } ?>
          </ul>
        </div>
      </div>
 </section>
   <!--end clients section-->
    <section class="blog-section">
  <div class="container">
        <div class="row">
          <h1><?php echo $homecontent3['ManageHomepage']['title'] ?></h1>
          <p><?php echo $homecontent3['ManageHomepage']['description'] ?></p>
          <?php foreach ($blogs as $blog) : ?>
              <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                <div class="blog-panel">
                  <div class="blog-panel-image">
                    <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['background_image'] ?>">
                  </div>
                  <div class="middle-image-box">
                    <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['image'] ?>">
                  </div>
                  <h1><?php echo $blog['Blog']['title'] ?></h1>
                  <h2><?php echo $blog['Blog']['short_description'] ?></h2>
                  <div class="Leaders-btn"><a href="#">Read More</a></div>
                </div>
              </div>
          <?php endforeach; ?>
        </div>
      </div>
    </section>

    <section class="hospitality">
       <div class="container-fluid">
        <div class="row">
        <?php foreach ($AppPromotions as $AppPromotion) { ?>
            <div class="col-xs-12 col-sm-12 col-md-6">
              <img class="img-responsive" src="<?php echo $this->webroot; ?>appPromotions_image/<?php echo $AppPromotion['AppPromotion']['image'] ?>">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
              <div class="hospitality-right">
                <h1><?php echo $AppPromotion['AppPromotion']['title'] ?></h1>
                <div class="hospitality-list">
                    <?php $AppPromotion['AppPromotion']['points'];
                        $points = explode(',',$AppPromotion['AppPromotion']['points']);
                     ?>
                  <ul>
                <?php foreach ($points as $point) { ?>
                    <li><i class="icon ion-ios-checkmark-empty"></i><?php echo $point ?></li>
                <?php } ?>

                  </ul>
                </div>
                <h2><?php echo $AppPromotion['AppPromotion']['description'] ?></h2>
                <ul class="star-listing">
                  <li><i class="icon ion-android-star"></i></li>
                  <li><i class="icon ion-android-star"></i></li>
                  <li><i class="icon ion-android-star"></i></li>
                  <li><i class="icon ion-android-star"></i></li>
                  <li><i class="icon ion-android-star"></i></li>
                </ul>
                <h3>Download the job app now</h3>
                <ul class="star-listing btn-listing">
                  <li><a href="#"><img src="<?php echo $this->webroot; ?>appPromotions_image/<?php echo $AppPromotion['AppPromotion']['android'] ?>"></a></li>
                  <li><a href="#"><img src="<?php echo $this->webroot; ?>appPromotions_image/<?php echo $AppPromotion['AppPromotion']['ios'] ?>"></a></li>
                </ul>
              </div>
            </div>
        <?php }  ?>

        </div>
      </div>

    </section>
<script>
      $(window).scroll(function(){
      var sticky = $('.sticky'),
          scroll = $(window).scrollTop();

      if (scroll >= 50) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
    </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                dots:true,
                nav: false,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          </script>
