<?php //echo '<pre>'; print_r($press_list); echo '</pre>'; ?>
<section class="pt-5 pb-5">
	<div class="container">
		<div class="row text-center">
      <div class="col-md-12">
        <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">
          <div class="">Studilmu</div>
          <span class="font-weight-bold" style="color:#f00; "> Press Releases</span>
          <em class="dvdr">
            <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
          </em>
        </h1>
      </div>
		</div>

		<div class="row">

			<?php if(!empty($press_list)){
				foreach ($press_list as $value) { ?>

				<div class="col-sm-12 margin-top-30">

					<div class="press-box">

						<a href="<?php echo $this->webroot.'press_releases/details/'.$value['PressRelease']['slug'] ?>">

							<span><?php echo date('F m, Y',strtotime($value['PressRelease']['creation_date'])); ?></span>
							<strong><?php echo $value['PressRelease']['name']; ?></strong>

						</a>

					</div>

				</div>
			<?php } } ?>


			<!-- <div class="col-sm-12 margin-top-30 margin-bottom-50">

				<div class="press-pagination">

          <p class="text-right mb-0 font-14">
            <?php
            echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
            ?>    </p>
					<ul class="paging text-right">
							<li><?php echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')); ?>
							<li><?php echo $this->Paginator->numbers(array('separator' => '')); ?>
							<li><?php echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')); ?>
						</ul>


					</div>

				</div> -->
			</div>

		</div>

	</div>

</section>
