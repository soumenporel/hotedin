<div class="categories form">
<?php 
//echo $this->Html->script('ckeditor/ckeditor'); 
echo $this->Form->create('PressRelease',array('enctype'=>'multipart/form-data'));   
?>
	<fieldset>
		<legend><?php echo __('Add Press'); ?></legend>
                
            <?php
              echo $this->Form->input('name',array('required'=>'required', 'label'=>'Press Title','type'=>'text'));
               echo $this->Form->input('category_id',array('empty' => '(choose any category)', 'label' => 'Category','required'=>'required' ,'class'=>'selectbox2'));
		      echo $this->Form->input('description',array('label'=>'Press Description'));
		      echo $this->Form->input('status', array('type'=>'checkbox', 'label'=>'Active'));
                       echo $this->Form->input('is_feature', array('type'=>'checkbox', 'label'=>'Feature'));
            ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'PressReleaseDescription',
        {
                width: "95%",
                filebrowserBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserImageBrowseUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserFlashBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserUploadUrl  :'<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',

                filebrowserImageUploadUrl : '<?php echo $this->webroot; ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',

                filebrowserFlashUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
        });
    </script>
<?php //echo $this->element('admin_sidebar'); ?>