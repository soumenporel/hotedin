<?php //pr($tags); ?>

<div class="roles index">
    <h2><?php echo __('Plan Items'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($plan_items as $plan_item): ?>
                <tr>
                    <td><?php echo h($plan_item['MembershipItem']['id']); ?>&nbsp;</td>
                    <td><?php echo h($plan_item['MembershipItem']['name']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link($this->Html->plan_item('i', '', array('class' => 'fa fa-eye')),
				                array('action' => 'view', $plan_item['MembershipItem']['id']),
				                array('class' => 'btn btn-success btn-xs', 'escape'=>false)); ?>
				                    
				        <?php echo $this->Html->link($this->Html->plan_item('i', '', array('class' => 'fa fa-edit')),
				                array('action' => 'edit', $plan_item['MembershipItem']['id']),
				                array('class' => 'btn btn-info btn-xs', 'escape'=>false)); ?>

				        <?php echo $this->Form->postLink($this->Html->plan_item('i', '', array('class' => 'fa fa-times')),
				                array('action' => 'delete', $plan_item['MembershipItem']['id']),
				                array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
				                __('Are you sure you want to delete # %s?', $plan_item['MembershipItem']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>