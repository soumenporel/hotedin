<link href="<?php echo $this->webroot;?>css/bootstrap-theme.css" rel="stylesheet">
<style>
#qid {
  padding: 10px 15px;
  -moz-border-radius: 50px;
  -webkit-border-radius: 50px;
  border-radius: 20px;
}
label.btn {
    padding: 0.4em;
    white-space: normal;
	/**-webkit-transform: scale(1.0);
    -moz-transform: scale(1.0);
    -o-transform: scale(1.0);
    -webkit-transition-duration: .3s;
    -moz-transition-duration: .3s;
    -o-transition-duration: .3s**/
}

label.btn:hover {
   /** text-shadow: 0 3px 2px rgba(0,0,0,0.4);
    -webkit-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -o-transform: scale(1.1)**/
}
label.btn-block {
    text-align: left;
    position: relative
}

label .btn-label {
    position: absolute;
    left: 0;
    top: 0;
    display: inline-block;
    padding: 0 10px;
    background: rgba(0,0,0,.15);
    height: 100%
}

label .glyphicon {
    top: 34%
}
.element-animation1 {
    animation: animationFrames ease .8s;
    animation-iteration-count: 1;
    transform-origin: 50% 50%;
    -webkit-animation: animationFrames ease .8s;
    -webkit-animation-iteration-count: 1;
    -webkit-transform-origin: 50% 50%;
    -ms-animation: animationFrames ease .8s;
    -ms-animation-iteration-count: 1;
    -ms-transform-origin: 50% 50%
}
.element-animation2 {
    animation: animationFrames ease 1s;
    animation-iteration-count: 1;
    transform-origin: 50% 50%;
    -webkit-animation: animationFrames ease 1s;
    -webkit-animation-iteration-count: 1;
    -webkit-transform-origin: 50% 50%;
    -ms-animation: animationFrames ease 1s;
    -ms-animation-iteration-count: 1;
    -ms-transform-origin: 50% 50%
}
.element-animation3 {
    animation: animationFrames ease 1.2s;
    animation-iteration-count: 1;
    transform-origin: 50% 50%;
    -webkit-animation: animationFrames ease 1.2s;
    -webkit-animation-iteration-count: 1;
    -webkit-transform-origin: 50% 50%;
    -ms-animation: animationFrames ease 1.2s;
    -ms-animation-iteration-count: 1;
    -ms-transform-origin: 50% 50%
}
.element-animation4 {
    animation: animationFrames ease 1.4s;
    animation-iteration-count: 1;
    transform-origin: 50% 50%;
    -webkit-animation: animationFrames ease 1.4s;
    -webkit-animation-iteration-count: 1;
    -webkit-transform-origin: 50% 50%;
    -ms-animation: animationFrames ease 1.4s;
    -ms-animation-iteration-count: 1;
    -ms-transform-origin: 50% 50%
}
@keyframes animationFrames {
    0% {
        opacity: 0;
        transform: translate(-1500px,0px)
    }

    60% {
        opacity: 1;
        transform: translate(30px,0px)
    }

    80% {
        transform: translate(-10px,0px)
    }

    100% {
        opacity: 1;
        transform: translate(0px,0px)
    }
}

@-webkit-keyframes animationFrames {
    0% {
        opacity: 0;
        -webkit-transform: translate(-1500px,0px)
    }
    60% {
        opacity: 1;
        -webkit-transform: translate(30px,0px)
    }

    80% {
        -webkit-transform: translate(-10px,0px)
    }

    100% {
        opacity: 1;
        -webkit-transform: translate(0px,0px)
    }
}

@-ms-keyframes animationFrames {
    0% {
        opacity: 0;
        -ms-transform: translate(-1500px,0px)
    }

    60% {
        opacity: 1;
        -ms-transform: translate(30px,0px)
    }
    80% {
        -ms-transform: translate(-10px,0px)
    }

    100% {
        opacity: 1;
        -ms-transform: translate(0px,0px)
    }
}

.modal-header {
    background-color: transparent;
    color: inherit
}

.modal-body {
    min-height: 205px
}
#loadbar {
    position: absolute;
    width: 62px;
    height: 77px;
    top: 2em
}
.blockG {
    position: absolute;
    background-color: #FFF;
    width: 10px;
    height: 24px;
    -moz-border-radius: 8px 8px 0 0;
    -moz-transform: scale(0.4);
    -moz-animation-name: fadeG;
    -moz-animation-duration: .8800000000000001s;
    -moz-animation-iteration-count: infinite;
    -moz-animation-direction: linear;
    -webkit-border-radius: 8px 8px 0 0;
    -webkit-transform: scale(0.4);
    -webkit-animation-name: fadeG;
    -webkit-animation-duration: .8800000000000001s;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-direction: linear;
    -ms-border-radius: 8px 8px 0 0;
    -ms-transform: scale(0.4);
    -ms-animation-name: fadeG;
    -ms-animation-duration: .8800000000000001s;
    -ms-animation-iteration-count: infinite;
    -ms-animation-direction: linear;
    -o-border-radius: 8px 8px 0 0;
    -o-transform: scale(0.4);
    -o-animation-name: fadeG;
    -o-animation-duration: .8800000000000001s;
    -o-animation-iteration-count: infinite;
    -o-animation-direction: linear;
    border-radius: 8px 8px 0 0;
    transform: scale(0.4);
    animation-name: fadeG;
    animation-duration: .8800000000000001s;
    animation-iteration-count: infinite;
    animation-direction: linear
}
#rotateG_01 {
    left: 0;
    top: 28px;
    -moz-animation-delay: .33s;
    -moz-transform: rotate(-90deg);
    -webkit-animation-delay: .33s;
    -webkit-transform: rotate(-90deg);
    -ms-animation-delay: .33s;
    -ms-transform: rotate(-90deg);
    -o-animation-delay: .33s;
    -o-transform: rotate(-90deg);
    animation-delay: .33s;
    transform: rotate(-90deg)
}
#rotateG_02 {
    left: 8px;
    top: 10px;
    -moz-animation-delay: .44000000000000006s;
    -moz-transform: rotate(-45deg);
    -webkit-animation-delay: .44000000000000006s;
    -webkit-transform: rotate(-45deg);
    -ms-animation-delay: .44000000000000006s;
    -ms-transform: rotate(-45deg);
    -o-animation-delay: .44000000000000006s;
    -o-transform: rotate(-45deg);
    animation-delay: .44000000000000006s;
    transform: rotate(-45deg)
}
#rotateG_03 {
    left: 26px;
    top: 3px;
    -moz-animation-delay: .55s;
    -moz-transform: rotate(0deg);
    -webkit-animation-delay: .55s;
    -webkit-transform: rotate(0deg);
    -ms-animation-delay: .55s;
    -ms-transform: rotate(0deg);
    -o-animation-delay: .55s;
    -o-transform: rotate(0deg);
    animation-delay: .55s;
    transform: rotate(0deg)
}
#rotateG_04 {
    right: 8px;
    top: 10px;
    -moz-animation-delay: .66s;
    -moz-transform: rotate(45deg);
    -webkit-animation-delay: .66s;
    -webkit-transform: rotate(45deg);
    -ms-animation-delay: .66s;
    -ms-transform: rotate(45deg);
    -o-animation-delay: .66s;
    -o-transform: rotate(45deg);
    animation-delay: .66s;
    transform: rotate(45deg)
}
#rotateG_05 {
    right: 0;
    top: 28px;
    -moz-animation-delay: .7700000000000001s;
    -moz-transform: rotate(90deg);
    -webkit-animation-delay: .7700000000000001s;
    -webkit-transform: rotate(90deg);
    -ms-animation-delay: .7700000000000001s;
    -ms-transform: rotate(90deg);
    -o-animation-delay: .7700000000000001s;
    -o-transform: rotate(90deg);
    animation-delay: .7700000000000001s;
    transform: rotate(90deg)
}
#rotateG_06 {
    right: 8px;
    bottom: 7px;
    -moz-animation-delay: .8800000000000001s;
    -moz-transform: rotate(135deg);
    -webkit-animation-delay: .8800000000000001s;
    -webkit-transform: rotate(135deg);
    -ms-animation-delay: .8800000000000001s;
    -ms-transform: rotate(135deg);
    -o-animation-delay: .8800000000000001s;
    -o-transform: rotate(135deg);
    animation-delay: .8800000000000001s;
    transform: rotate(135deg)
}
#rotateG_07 {
    bottom: 0;
    left: 26px;
    -moz-animation-delay: .99s;
    -moz-transform: rotate(180deg);
    -webkit-animation-delay: .99s;
    -webkit-transform: rotate(180deg);
    -ms-animation-delay: .99s;
    -ms-transform: rotate(180deg);
    -o-animation-delay: .99s;
    -o-transform: rotate(180deg);
    animation-delay: .99s;
    transform: rotate(180deg)
}

.btn-primary.correct{
    background: green !important;
    color: #000 !important;
}
.btn-primary.incorrect{
    background: red !important;
    color: #000 !important;
}

#rotateG_08 {
    left: 8px;
    bottom: 7px;
    -moz-animation-delay: 1.1s;
    -moz-transform: rotate(-135deg);
    -webkit-animation-delay: 1.1s;
    -webkit-transform: rotate(-135deg);
    -ms-animation-delay: 1.1s;
    -ms-transform: rotate(-135deg);
    -o-animation-delay: 1.1s;
    -o-transform: rotate(-135deg);
    animation-delay: 1.1s;
    transform: rotate(-135deg)
}
@-moz-keyframes fadeG {
    0% {
        background-color: #000
    }

    100% {
        background-color: #FFF
    }
}

@-webkit-keyframes fadeG {
    0% {
        background-color: #000
    }

    100% {
        background-color: #FFF
    }
}

@-ms-keyframes fadeG {
    0% {
        background-color: #000
    }

    100% {
        background-color: #FFF
    }
}

@-o-keyframes fadeG {
    0% {
        background-color: #000
    }
    100% {
        background-color: #FFF
    }
}

@keyframes fadeG {
    0% {
        background-color: #000
    }

    100% {
        background-color: #FFF
    }
}
#sortable{ width:100% !important;
}
</style>
 
  <script>
    $( function() {
    //$( "#sortable" ).sortable();
    //$( "#sortable" ).disableSelection();
    //$( "#sortable3" ).disableSelection();
    $("#sortable").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize"); 
                        console.log(order);
                        //alert(1);
			$.post("<?php echo $this->webroot; ?>learns/addordering/<?php echo $userid; ?>/<?php echo $questions['Question']['id'];?>/<?php echo $questions['Question']['quiz_id'];?>", order, function(theResponse){
                            console.log(theResponse);
	//alert(theResponse);			
        //$("#contentRight").html(theResponse);
			}); 															 
		}								  
		});
                
       //for matching type....................................
       $("#sortable2").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize"); 
                        console.log(order);
                        //alert(2);
			$.post("<?php echo $this->webroot; ?>learns/addmatching/<?php echo $userid; ?>/<?php echo $questions['Question']['id'];?>/<?php echo $questions['Question']['quiz_id'];?>", order, function(theResponse){
                            console.log(theResponse);
	//alert(theResponse);			
        //$("#contentRight").html(theResponse);
			}); 															 
		}								  
		});
                
    });

 
  </script>
<div class="container bg-info">
    <div class="modal-dialog modalQuestion">
      <div class="modal-content">
         <div class="modal-header mt-2">
            <h3 class="pt-3">
              <span class="label label-warning" id="qid"> Q </span> <?php echo $questions['Question']['question'];?> 
            </h3>
              <input type="hidden" id="user_id" value="<?php echo $userid; ?>" />
              <input type="hidden" id="quiz_id" value="<?php echo $questions['Question']['quiz_id'];?>" />
              <input type="hidden" id="question_id" value="<?php echo $questions['Question']['id'];?>" />
              <input type="hidden" id="Qtype" value="<?php echo $Qtype; ?>" />
        </div>
        <div class="modal-body">
            <div class="col-xs-3 col-xs-offset-5">
               <div id="loadbar" style="display: none;">
                  <div class="blockG" id="rotateG_01"></div>
                  <div class="blockG" id="rotateG_02"></div>
                  <div class="blockG" id="rotateG_03"></div>
                  <div class="blockG" id="rotateG_04"></div>
                  <div class="blockG" id="rotateG_05"></div>
                  <div class="blockG" id="rotateG_06"></div>
                  <div class="blockG" id="rotateG_07"></div>
                  <div class="blockG" id="rotateG_08"></div>
              </div>
          </div>

        <div class="quiz" id="quiz" data-toggle="buttons">
            <?php if($Qtype=='ordering'){ ?>
        <ul id="sortable">
        <?php foreach ($optionsDecode as $key => $option) { ?>  
           
                <li class="ui-state-default uisortableLi" id="recordsArray_<?php echo $option->id; ?>@<?php echo $option->order;?>">
                    <?php echo $option->answer;?>
                </li>
           
          

        <?php } ?>  
        </ul>
            <a href="javascript:void(0);">
            <button type="button" class="btn btn-primary save_order float-right">Save & Next <i class="fa fa-angle-right"></i></button>
            </a>  
            <?php 
           //for ordering ends here................................................... 
        } 
        elseif($Qtype=='matching')
        {
         ?>
           
<!--          <ul id="sortable2" class="row">
        <?php foreach ($optionsDecode as $key => $option) { ?>  
              <div class="col-lg-4">
               <li id="recordsArray3_<?php echo $option->match_ans;?>" class="firstLi">
                    <?php echo $option->answer;?>
                </li> 
              </div>
              <div class="col-lg-8">
              <li class="ui-state-default secondLi" id="recordsArray2_<?php echo $option->match_ans;?>">
                    <?php echo $option->match_ans;?>
                </li>
              </div>
          

        <?php } ?>  
        </ul>-->
<div class="col-lg-12">
	<div class="row">
     <div class="col-lg-4">
        <ul class="quizPart"> 
        <?php foreach ($optionsDecode as $key => $option) { ?>  
             
               <li class="firstLi">
                    <?php echo $option->answer;?>
                </li>
          

        <?php } ?> 
              </ul>
                 </div>
      
 
    <div class="col-lg-8">
      <ul id="sortable2"> 
          <?php foreach ($optionsDecode as $key => $option) { ?>  
             
          <li id="recordsArray3_<?php echo $option->match_ans;?>" class="firstLi" style="display: none;">
                    <?php echo $option->answer;?>
                </li>
          

        <?php } ?> 
        <?php 
        shuffle($optionsDecode);
        foreach ($optionsDecode as $key => $option) { ?>  
        <li class="ui-state-default secondLi" id="recordsArray2_<?php echo $option->match_ans;?>">
                    <?php echo $option->match_ans;?>
                </li>
                
                
        <?php } ?>
                 </ul>
    </div>
    </div>
    </div>
       
        <div class="clearfix"></div>
            <div><a href="javascript:void(0);">
            <button type="button" class="btn btn-primary float-right save_matching">Save & Next <i class="fa fa-angle-right"></i></button>
            </a>   </div> 
        
        <?php    
        }    
        else { ?>
           <?php foreach ($optionsDecode as $key => $option) { ?>  
           
              
         <label class="element-animation1 btn btn-lg btn-primary btn-block quesBtn">
             
            <!--<span class="btn-label">
              <i class="glyphicon glyphicon-chevron-right"></i>
            </span>--> 
            <input type="radio" name="q_answer" value="<?php echo $option->answer; ?>"><?php echo $option->answer; ?>
          </label>
        <?php } ?>   
            <?php } ?>
             
        <input type="hidden" id="quizAns" value="<?php echo $questions['Question']['answer'];?>">   
          
          <!-- <label class="element-animation2 btn btn-lg btn-primary btn-block">
            <span class="btn-label">
              <i class="glyphicon glyphicon-chevron-right"></i>
            </span> <input type="radio" name="q_answer" value="2">2 Two
          </label>
          
          <label class="element-animation3 btn btn-lg btn-primary btn-block">
            <span class="btn-label">
              <i class="glyphicon glyphicon-chevron-right"></i>
            </span>
            <input type="radio" name="q_answer" value="3">3 Three
          </label>
          
          <label class="element-animation4 btn btn-lg btn-primary btn-block">
            <span class="btn-label">
              <i class="glyphicon glyphicon-chevron-right"></i>
            </span>
            <input type="radio" name="q_answer" value="4">4 Four
          </label> -->
       </div>
   </div>
   <div class="modal-footer text-muted">
    <span id="answer"></span>
</div>
</div>
</div>
</div>
<div class="quiz-section1">
<div class="player-footer">
        <div class="player-footer-left"></div>
        
        <div class="player-footer-right">
            <a href="<?php echo $URL; ?>"><button type="button" class="btn btn-primary continue_next">Continue <i class="fa fa-angle-right"></i></button></a>
            <div class="btn-group">
                <button href="javascript:void(0)" class="btn btn-sm"> <i class="fa fa-cog fa-spin"></i></button>
                <button href="javascript:void(0)" class="btn btn-sm"> <i class="fa fa-arrows-alt"></i></button>
            </div>
        </div>
    </div>
</div>
<script>
$(function(){
    var loading = $('#loadbar').hide();
    $(document)
    .ajaxStart(function () {
        loading.show();
    }).ajaxStop(function () {
      loading.hide();
    });
     var quizAns = $("#quizAns").val();
    $ans = quizAns;
    
    $("label.btn").on('click',function () {
        //alert('1');
        var user_id = $('#user_id').val();
        var quiz_id = $('#quiz_id').val();
        var question_id = $('#question_id').val();
        var Qtype = $('#Qtype').val();
        //alert(user_id);
        //alert(quiz_id);
        //alert(question_id);
        //alert(Qtype); 
      var self = $(this);
      var choice = $(this).find('input:radio').val();
      console.log(choice);
      $('#loadbar').show();
      $('#quiz').fadeOut();
      $( "#answer" ).html(  $(this).checking(choice) );      
          $('#quiz').show();
          $('#loadbar').fadeOut();
           /* something else */
          if(choice==$ans){
            self.addClass('correct');
            var score=1;
          }else{
            self.addClass('incorrect');
            var score=0;
          }
          //alert(score);
          //alert($ans);
          $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>learns/addscore",
                    data: {
                        user_id: user_id,
                        quiz_id:quiz_id,
                        question_id:question_id,
                        Qtype:Qtype,
                        score:score,
                        your_ans:choice,
                        correct_ans:$ans
                    },
                    success: function (html) {
                        console.log(html);
                       $('.continue_next').click();
                    }
                });
          //$('.continue_next').click();
//      setTimeout(function(){
//          $( "#answer" ).html(  $(this).checking(choice) );      
//          $('#quiz').show();
//          $('#loadbar').fadeOut();
//           /* something else */
//          if(choice==$ans){
//            self.addClass('correct');
//          }else{
//            self.addClass('incorrect');
//          }
//          
//          $('.continue_next').click();
//      }, 1500);
      
    });
    
     $(".save_order").on('click',function () {
        //alert('1');
        var user_id = $('#user_id').val();
        var quiz_id = $('#quiz_id').val();
        var question_id = $('#question_id').val();
        var Qtype = $('#Qtype').val();
        
      $('#loadbar').show();
      $('#quiz').fadeOut();
      
          $('#quiz').show();
          $('#loadbar').fadeOut();
           /* something else */
       
          $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>learns/addscoreordering",
                    data: {
                        user_id: user_id,
                        quiz_id:quiz_id,
                        question_id:question_id,
                        Qtype:Qtype
                    },
                    success: function (html) {
                        console.log(html);
                       $('.continue_next').click();
                    }
                });
          //$('.continue_next').click();
//      setTimeout(function(){
//          $( "#answer" ).html(  $(this).checking(choice) );      
//          $('#quiz').show();
//          $('#loadbar').fadeOut();
//           /* something else */
//          if(choice==$ans){
//            self.addClass('correct');
//          }else{
//            self.addClass('incorrect');
//          }
//          
//          $('.continue_next').click();
//      }, 1500);
      
    });
    
    $(".save_matching").on('click',function () {
        //alert('1');
        var user_id = $('#user_id').val();
        var quiz_id = $('#quiz_id').val();
        var question_id = $('#question_id').val();
        var Qtype = $('#Qtype').val();
        
      $('#loadbar').show();
      $('#quiz').fadeOut();
      
          $('#quiz').show();
          $('#loadbar').fadeOut();
           /* something else */
       
          $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>learns/addscorematching",
                    data: {
                        user_id: user_id,
                        quiz_id:quiz_id,
                        question_id:question_id,
                        Qtype:Qtype
                    },
                    success: function (html) {
                        console.log(html);
                       $('.continue_next').click();
                    }
                });
          //$('.continue_next').click();
//      setTimeout(function(){
//          $( "#answer" ).html(  $(this).checking(choice) );      
//          $('#quiz').show();
//          $('#loadbar').fadeOut();
//           /* something else */
//          if(choice==$ans){
//            self.addClass('correct');
//          }else{
//            self.addClass('incorrect');
//          }
//          
//          $('.continue_next').click();
//      }, 1500);
      
    });

   
    $.fn.checking = function(ck) {
        if (ck != $ans)
            return 'INCORRECT';
        else 
            return 'CORRECT';
    }; 
}); 

</script>