<?php $vv = $postDetails['Post'];
//pr($announcements);
?>
<style>
  .header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

  .top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important;}
</style>
<!-- <link href="<?php echo $this->webroot; ?>css/course_dashsboard.css" rel="stylesheet" type="text/css"> -->
<div class="ud-angular-loaded" data-module-id="course-taking-v4" data-module-name="ng/apps/course-taking-v4/app">
   <!-- uiView:  -->
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom">
              <?php echo $this->element('course_midmenu'); ?>

<?php //echo '<pre>'; print_r($bookmarks); echo'</pre>';?>

<?php
if(empty($bookmarks)){ ?>
  <div class="tac" style="padding-top: 100px;padding-bottom: 100px;">
   <i class="fa fa-bookmark" aria-hidden="true" style="font-size: 50px;"></i>
   <div class="a1" translate=""><span><h1 style="font-size: 28px; margin-bottom10px; ">No bookmarks created yet</h1></span></div>
   <p translate=""><span style="font-size: 14px;">Add bookmarks while watching lectures to save moments of a course for later. Bookmarks you've</br> saved will show up here.</span></p>
   <p translate=""><span style="font-size: 14px;" >Hint: Use the keyboard shortcut "space" to create bookmarks while watching video lectures.</span></p>
</div>
<?php }else{
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="list-group mt-4 mb-4">
			  <?php foreach ($bookmarks as $key => $bookmark) { ?>
			    <a href="<?php echo $this->webroot.'learns/video_html/'.$postDetails['Post']['slug'].'/'.$bookmark['UserBookmark']['lecture_id'].'/'.$bookmark['UserBookmark']['id'];?>" class="list-group-item mb-2" style="display: block; padding:15px; width:100%">
			      <h4 class="list-group-item-heading"><?php echo $bookmark['Lecture']['title'];?></h4>
			      <p class="list-group-item-text">
			      	<i class="fa fa-play-circle-o" aria-hidden="true"></i>
			      	<span style="color:#08D6E8" ><?php echo round($bookmark['UserBookmark']['time'],2);?></span>
			      </p>
			    </a>
			  <?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>


<script>
    function handle(e){
        if(e.keyCode === 13){
            e.preventDefault(); // Ensure it is only this code that rusn
          alert($(this).val());
        }
    }

    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
</script>

<script>
  $(".show-more a").each(function() {
    var $link = $(this);
    var $content = $link.parent().prev("div.text-content");

    console.log($link);

    var visibleHeight = $content[0].clientHeight;
    var actualHide = $content[0].scrollHeight - 1;

    console.log(actualHide);
    console.log(visibleHeight);

    if (actualHide > visibleHeight) {
        $link.show();
    } else {
        $link.hide();
    }
});

$(".show-more a").on("click", function() {
    var $link = $(this);
    var $content = $link.parent().prev("div.text-content");
    var linkText = $link.text();

    $content.toggleClass("short-text, full-text");

    $link.text(getShowLinkText(linkText));

    return false;
});

function getShowLinkText(currentText) {
    var newText = '';

    if (currentText.toUpperCase() === "SHOW MORE") {
        newText = "Show less";
    } else {
        newText = "Show more";
    }

    return newText;
}
</script>
