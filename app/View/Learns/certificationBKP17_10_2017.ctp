<section class="certificate mt-5 pt-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="certifacatePart text-center">
					<img src="<?php echo $this->webroot;?>images/logoStudilmu.jpg">
					<div class="certificateContent">
						<p class="mt-5 pt-2">ONCERT/<?php echo date('Y'); ?>/<?php echo $number; ?></p>
						<h6 class="mt-3">CERTIFICATE OF COMPLETION</h6>
						<p class="mt-2">This is to certify that</p>
						<h2 class="fontEdwardion py-4"><?php echo $username; ?></h2>
						<p class="font-14 mt-2">has completed online course</p>
						<p class="font-16 mt-1"><strong><?php echo $course; ?></strong></p>
						<p class="mt-5 pt-2 font-14">Issued on <?php echo date('F j,Y'); ?></p>
						<p class="font-14 mt-5 pt-2"><strong>Berny Gomulya</strong> <br>CEO</p>
						<p>
                                                    <a href="<?php echo $this->webroot;?>certificate/<?php echo $fileName; ?>"  download class="btn btn-danger dangerBut">
                                                        Download Now</a>
<!--                                               <button type="button" class="btn btn-danger dangerBut">Download Now</button>-->
                                                </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>