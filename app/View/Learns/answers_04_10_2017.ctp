<?php //pr($question); pr($answers); exit;?>

<style>
img {

}
</style>

<?php echo $this->element('course_topmenu'); ?>

<input type="hidden" name="data[Rating][ratting]" id="review_val" >
<div class="course-dashboard__bottom"> 
	<nav class="navbar navbar-default bottom__nav">
	 <div class="container">
	  <!-- <div class="navbar-header"> 
	  	<div class="fx"> 
	  		<span class="navbar-header__title"> Overview </span> 
	  	</div> 
	  	<div>
	  	 <button class="btn btn-default btn-lg toggle-btn" type="button" ng-click="navbarCollapsed = !navbarCollapsed" data-target="#course-taking-bottom-navbar" ng-attr-aria-expanded="{{ navbarCollapsed ? 'false' : 'true' }}" aria-expanded="false"> 
	  	 	<span class="sr-only">Toggle navigation</span> 
	  	 	<i class="udi udi-reorder"></i> 
	  	 </button> 
	  	</div> 
	  </div>  -->
	  <div class="navbar-collapse collapse" id="course-taking-bottom-navbar" collapse="navbarCollapsed" style="height: 0px;" aria-expanded="false" aria-hidden="true"> 
	  	<ul class="nav navbar-nav nav-tabs nav-parts"> 
	  		<li ui-sref-active="active" class="menues" > 
	  			<a ui-sref="base.dashboard.overview" ng-click="navbarCollapsed = true" translate="" href="<?php echo $this->webroot.'learns/overview/'.$postDetails['Post']['slug']; ?>">
	  				<span>Overview</span>
	  			</a> 
	  		</li> 
	  		<li ui-sref-active="active" class="menues" > 
	  			<a ui-sref="base.dashboard.content" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-content-tab" href="<?php echo $this->webroot;?>learns/course_content/<?php echo $postDetails['Post']['slug'];?>">
	  				<span>Course Content</span>
	  			</a> 
	  		</li> 
	  		<li ng-class="{active: ('base.dashboard.questions' | includedByState) || ('base.dashboard.question-details' | includedByState)}" class="menues active" > 
	  			<a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/questions/'.$postDetails['Post']['slug']; ?>">
                         <span>Q&amp;A</span>
                    </a> 
               </li> 
               <li ui-sref-active="active" class="menues" > 
                    <a ui-sref="base.dashboard.bookmarks" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-bookmark-tab" href="javascript:void(0)">
                         <span>Bookmarks</span>
                    </a> 
               </li> 
               <li ui-sref-active="active" class="menues" > 
                    <a ui-sref="base.dashboard.announcements" ng-click="navbarCollapsed = true" translate="" href="<?php echo $this->webroot;?>learns/announcement/<?php echo $postDetails['Post']['slug'];?>">
                         <span>Announcements</span>
                    </a> 
               </li> 
               <li ui-sref-active="active" class="dropdown menues"> 
                    <a href="javascript:void(0)" id="dLabel" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                         <span class="mr5" translate="">
                              <span>Options</span>
                         </span> 
                         <i class="udi udi-caret-down"></i> 
                    </a> 
                    <!-- ngInclude: optionsDropdownTemplateUrl -->
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" ng-click="$event.stopPropagation();" ng-include="optionsDropdownTemplateUrl">
                         <li role="presentation">
                               <!-- ngIf: !course.favorite_time -->
                               <input type="hidden" id="hidden_user_id" value='<?php echo $userid?>'></input>
                               <a role="menuitem" tabindex="-1" ng-click="course.favoriteCourse()" ng-if="!course.favorite_time" translate="">
                                   <span id="<?php echo $favoritexist ? 'unfavorite_this_course' : 'favorite_this_course'; ?>" data-id="<?php echo $postDetails['Post']['id']; ?>" ><?php echo $favoritexist ? 'Unfavorite this course' : 'Favorite this course'; ?></span>
                               </a>
                               <!-- end ngIf: !course.favorite_time --> 
                               <!-- ngIf: course.favorite_time --> 
                            </li>
                            <li role="presentation">
                               <!-- ngIf: !course.archive_time -->
                               <a role="menuitem" tabindex="-1" ng-click="course.archiveCourse()" ng-if="!course.archive_time" translate="">
                                   <span id="<?php echo $archivexist ? 'unarchive_this_course' : 'archive_this_course'; ?>" data-id="<?php echo $postDetails['Post']['id']; ?>" ><?php echo $archivexist ? 'Unarchive this course' : 'Archive this course'; ?></span>
                              </a>
                              <!-- end ngIf: !course.archive_time --> 
                              <!-- ngIf: course.archive_time --> 
                            </li> 
                         <li role="presentation" class="divider"></li> 
                         <li role="presentation">
                              <a role="menuitem" tabindex="-1" ng-click="scrollToInstructor()" translate="">
                                   <span>Instructor profile</span>
                              </a>
                         </li> 
                         <li role="presentation">
                              <a role="menuitem" tabindex="-1" ng-show="isGiftCourseEnabled &amp;&amp; course.is_paid &amp;&amp; !course.is_private" ng-href="https://www.udemy.com/gift/introduction-to-social-media-in-education/" target="_blank" translate="" class="ng-hide" href="https://www.udemy.com/gift/introduction-to-social-media-in-education/">
                                   <span>Gift this course</span>
                              </a>
                         </li> 
                         <li role="presentation">
                              <a role="menuitem" tabindex="-1" ng-href="https://www.udemy.com/support/" target="_blank" translate="" href="https://www.udemy.com/support/">
                                   <span>Support</span>
                              </a>
                         </li> 
                         <!-- ngIf: reportAbusePopup.enabled -->
                         <li role="presentation" ng-if="reportAbusePopup.enabled"> 
                              <a role="menuitem" tabindex="-1" ng-click="reportAbusePopup.open=true"> <i class="udi udi-flag mr5"></i> 
                                   <span translate=""><span>Report abuse</span>
                              </span> 
                              <popup open="reportAbusePopup.open" ng-href="/feedback/report?related_object_type=course&amp;related_object_id=49220" style="display: block;" href="/feedback/report?related_object_type=course&amp;related_object_id=49220"> </popup> 
                         </a> 
                    </li>
                    <!-- end ngIf: reportAbusePopup.enabled --> <li role="presentation" class="divider">
               </li> 
               <!-- ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
               <li role="presentation" ng-repeat="(key, value) in course.notification_settings track by $id(key)" class="" style=""> 
                    <a role="menuitem" tabindex="-1" class="email-settings" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> 
                         <span class="checkbox1"> 
                              <label> 
                                   <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" type="checkbox"> 
                                   <span class="checkbox-label email-settings__title"> Promotional emails </span> 
                              </label> 
                         </span> 
                    </a> 
               </li>
               <!-- end ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
               <li role="presentation" ng-repeat="(key, value) in course.notification_settings track by $id(key)" class=""> 
                    <a role="menuitem" tabindex="-1" class="email-settings ng-hide" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> 
                         <span class="checkbox1"> 
                              <label> 
                                   <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" checked="checked" type="checkbox"> 
                                   <span class="checkbox-label email-settings__title">  
                                   </span> 
                              </label> 
                         </span> 
                    </a> 
               </li>
               <!-- end ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
               <li role="presentation" ng-repeat="(key, value) in course.notification_settings track by $id(key)" class=""> 
                    <a role="menuitem" tabindex="-1" class="email-settings" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> 
                         <span class="checkbox1"> 
                              <label> 
                                   <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" type="checkbox"> 
                                   <span class="checkbox-label email-settings__title"> New announcement emails 
                                   </span> 
                              </label> 
                         </span> 
                    </a> 
               </li>
               <!-- end ngRepeat: (key, value) in course.notification_settings track by $id(key) --> <li role="presentation" class="divider"></li> 
               <li role="presentation"> 
                    <!-- ngIf: course.canBeUnenrolled --> <!-- ngIf: course.refundFeatureEnabled -->
                    <div ng-if="course.refundFeatureEnabled"> 
                         <a target="_blank" ng-href="" ng-show="course.was_purchased_by_student &amp;&amp; !course.canBeUnenrolled" ng-class="{'unsubscribe': course.is_refundable, 'cannot-refund': !course.is_refundable}" class="pl10 pb10 ng-hide cannot-refund" translate="">
                              <span>Request a refund</span>
                         </a> 
                         <div ng-hide="course.is_refundable" class="cannot-refund-info"> 
                              <div ng-show="course.was_paid_by_student" translate="" class="ng-hide"><span>This course was purchased outside the 30-day refund policy and cannot be refunded.</span>
                              </div> 
                              <div ng-show="!course.was_paid_by_student" translate="" class="">
                                   <span>This course was a free enrollment and cannot be refunded.</span>
                              </div> 
                         </div> 
                    </div>
                    <!-- end ngIf: course.refundFeatureEnabled --> 
               </li> 
          </ul> 
     </li> 
</ul> 
</div> 
</div> 
</nav> 
<!-- uiView:  -->

<div class="bottom__content container" id="ans_tab" data-ui-view="">
   <a class="db mt20" href="<?php echo $this->webroot;?>learns/questions/<?php echo $postDetails['Post']['slug'];?>" ng-click="back()" translate=""><span id="to_all_question" >Back to All Questions</span></a> 
   <!-- ngIf: questions.loading --> 
   <!-- ngIf: questions.activeQuestion && !questions.loading -->
   <question-details show-new-tab="false" on-delete="onDelete" ng-if="questions.activeQuestion &amp;&amp; !questions.loading" class="">
      <div class="question-details questionsPart">
         <div class="original-question fx fx-lt">
            <img ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" ng-class="{'img-circle':!question.is_instructor}" src="<?php if (isset($question['User']['user_image']) && $question['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                       echo $question['User']['user_image'];
                      } else {
                      echo $this->webroot;
                      ?>img/profile_img.jpg<?php } ?>" style="width: 50px; height: 50px" class="img-circle"> 
            <div class="question__info a11 fx">
               <question-action-menu show-new-tab="showNewTab" instructor-view="instructorView" on-delete="onDelete" question="question" me="me" class="dib fr">
                  <!-- ngIf: showMenu -->
                  <div class="dropdown question-action-menu" ng-if="showMenu">
                     <button class="btn btn-tertiary btn-xs" data-deny-click-read="true" id="questionActionMenu1839212" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
 </button> 
                     <ul class="dropdown-menu dropdown-menu-right a11" role="menu" aria-labelledby="questionActionMenu1839212">
                        <!-- ngIf: showNewTab --> <!-- ngIf: instructorView && me.canEditCourse --> <!-- ngIf: instructorView --> <!-- ngIf: editQuestionButtonEnabled --> <!-- ngIf: deleteQuestionButtonEnabled --> <!-- ngIf: reportAbuseButtonEnabled -->
                        <li role="presentation" class="report-abuse-button" ng-if="reportAbuseButtonEnabled">
                           <report-abuse show-label="true" show-tooltip="false" object-type="'coursediscussion'" object-id="question.id" tabindex="-1">
                              <react-component props="props" name="ReportAbuse">
                                 <span data-reactroot="">
                                    <button type="button" class="btn btn-default">
                                       <i class="fa fa-flag" aria-hidden="true"></i>
<!-- react-text: 4 -->&nbsp;<!-- /react-text --><!-- react-text: 5 -->Report abuse<!-- /react-text -->
                                    </button>
                                    <span>
                                       <!-- react-empty: 7 --><!-- react-empty: 8 -->
                                    </span>
                                 </span>
                              </react-component>
                           </report-abuse>
                        </li>
                        <!-- end ngIf: reportAbuseButtonEnabled --> 
                     </ul>
                  </div>
                  <!-- end ngIf: showMenu --> 
               </question-action-menu>
               <!-- ngIf: !instructorView -->
               <div ng-if="!instructorView" ng-show="!questionEditorState.isEditing" class="a10 bold"><?php echo $question['CourseQuestion']['title'];?></div>
               <!-- end ngIf: !instructorView --> 
               <a ng-href="/user/carlos-eduardo-oliveida-do-nascimento/" target="_blank" href="/user/carlos-eduardo-oliveida-do-nascimento/"><?php echo $question['User']['first_name'].' '.$question['User']['last_name'];?></a> 
               <!-- ngIf: instructorView --> <!-- ngIf: instructorView --> <!-- ngIf: question.related_object._class == 'lecture' && question.related_object.object_index -->
               <a ng-if="question.related_object._class == 'lecture' &amp;&amp; question.related_object.object_index" ui-sref="base.taking.lecture({id: question.related_object.id})" ng-click="goToLecture(question.related_object)" translate="" href="/quickstart-angularjs/learn/v4/t/lecture/1881894"><span></span></a>
               <!-- end ngIf: question.related_object._class == 'lecture' && question.related_object.object_index --> 
               <!-- ngIf: question.related_object._class == 'quiz' && question.related_object.object_index --> 
               <!-- ngIf: question.related_object._class == 'practice' && question.related_object.object_index --> 
               <!-- ngIf: !instructorView -->
               <span ng-if="!instructorView"> · 2 months ago </span>
               <!-- end ngIf: !instructorView --> 
               <!-- ngIf: questionEditorState.isEditing --> 
               <!-- ngIf: !instructorView -->
               <div ng-if="!instructorView" ng-show="!questionEditorState.isEditing" class="question__body" ng-bind-html="question.body | openBlankTarget" prettify="">
                  <?php echo $question['CourseQuestion']['description'];?>
               </div>
               <!-- end ngIf: !instructorView --> 
               <!-- ngIf: instructorView --> 
          
               <!-- ngIf: !instructorView -->
               <question-follow disable="readOnly" ng-if="!instructorView" ng-show="!questionEditorState.isEditing" question="question" class="">
                  <!-- ngIf: !menuItem && question -->
                  <button type="button" class="btn btn-default question-following" ng-if="!menuItem &amp;&amp; question" ng-disabled="disable" ng-class="{'question-following': question.is_following}" ng-click="toggleFollow($event)">
                     <!-- ngIf: !question.is_following --> <!-- ngIf: question.is_following --><span translate="" class="text-link-blue bold" ng-if="question.is_following"><span>Following Responses</span></span><!-- end ngIf: question.is_following --> 
                  </button>
                  <!-- end ngIf: !menuItem && question --> <!-- ngIf: menuItem && question --> 
               </question-follow>
               <!-- end ngIf: !instructorView --> 
            </div>
         </div>
         <div class="answer-list">
            <div class="answers">
               <!-- ngRepeat: answer in answers -->
              <?php foreach ($answers as $key => $answer) { ?>
                 <div class="answer fx fx-lt" ng-repeat="answer in answers" ng-class="{'top-answer': answer.is_top_answer}">
                    <img ng-class="{'img-circle':!answer.is_instructor}" ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" src="<?php if (isset($answer['User']['user_image']) && $answer['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                         echo $answer['User']['user_image'];
                        } else {
                        echo $this->webroot;
                        ?>img/profile_img.jpg<?php } ?>" style="width: 50px; height: 50px" class="img-circle"> 
                    <div class="answer__info fx">
                       <answer-action-menu class="dib fr">
                          <!-- ngIf: showMenu -->
                          <div class="dropdown question-action-menu" ng-if="showMenu">
                     <button class="btn btn-tertiary btn-xs" data-deny-click-read="true" id="questionActionMenu1839212" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
 </button> 
                     <ul class="dropdown-menu dropdown-menu-right a11" role="menu" aria-labelledby="questionActionMenu1839212">
                        <!-- ngIf: showNewTab --> <!-- ngIf: instructorView && me.canEditCourse --> <!-- ngIf: instructorView --> <!-- ngIf: editQuestionButtonEnabled --> <!-- ngIf: deleteQuestionButtonEnabled --> <!-- ngIf: reportAbuseButtonEnabled -->
                        <li role="presentation" class="report-abuse-button" ng-if="reportAbuseButtonEnabled">
                           <report-abuse show-label="true" show-tooltip="false" object-type="'coursediscussion'" object-id="question.id" tabindex="-1">
                              <react-component props="props" name="ReportAbuse">
                                 <span data-reactroot="">
                                    <button type="button" class="btn btn-default">
                                       <i class="fa fa-flag" aria-hidden="true"></i>
<!-- react-text: 4 -->&nbsp;<!-- /react-text --><!-- react-text: 5 -->Report abuse<!-- /react-text -->
                                    </button>
                                    <span>
                                       <!-- react-empty: 7 --><!-- react-empty: 8 -->
                                    </span>
                                 </span>
                              </react-component>
                           </report-abuse>
                        </li>
                        <!-- end ngIf: reportAbuseButtonEnabled --> 
                     </ul>
                  </div>
                          <!-- end ngIf: showMenu --> 
                       </answer-action-menu>
                       <a ng-href="/user/soumya-sankar-mondal/" target="_blank" href="/user/soumya-sankar-mondal/">Soumya Sankar</a> <span class="instructor-tag ng-hide" ng-show="answer.is_instructor" translate=""><span>— Instructor</span></span> <small> · a month ago </small> <!-- ngIf: answer.is_top_answer --> <br> <!-- ngIf: instructorView --> <!-- ngIf: answer.user.id === me.id -->
                      <!--  -->
                       <!-- end ngIf: answer.user.id === me.id --> <!-- ngIf: !instructorView -->
                       <div ng-if="!instructorView" ng-show="!answerEditorStates[answer.id].isEditing" class="answer__body a11" ng-bind-html="answer.body | openBlankTarget" prettify="">
                          <?php echo $answer['CourseAnswer']['answers']?>
                       </div>
                       <!-- end ngIf: !instructorView --> <!-- ngIf: instructorView --> 
                       <div class="answer__actions a11">
                          <a class="mr10 upvote ng-hide not-upvoted owner" ng-class="{'upvoted': answer.is_upvoted, 'not-upvoted': !answer.is_upvoted, 'owner': userIsAnswerer, 'disabled': disable}" href="javascript:void(0)" ng-click="toggleUpvote($event)" ng-show="!userIsAnswerer || answer.num_upvotes > 0" disable="readOnly" question="question" answer="answer"> <span translate=""><span></span></span> </a> 
                          <a href="javascript:void(0)" ng-show="canUserMarkAnswerAsHelpful()" ng-class="{'disabled':disable }" ng-click="toggleMarkAsHelpful()" ng-disable="isMarkAsHelpfulSubmitting" answer="answer" me="me" on-success="markAsTopOnSuccess(answer)" class="ng-hide">
                             <!-- ngIf: !answer.is_top_answer --><span ng-if="!answer.is_top_answer" translate=""><span>Mark as top answer</span></span><!-- end ngIf: !answer.is_top_answer --> <!-- ngIf: answer.is_top_answer --> 
                          </a>
                       </div>
                    </div>
                 </div>
              <?php } ?>   
               <!-- end ngRepeat: answer in answers --> 
            </div>
          <!--  <div class="fx-c mb10"> <button class="btn btn-primary ng-hide" ng-click="loadAnswers()" ng-disabled="loadingAnswers" ng-show="hasMoreAnswers" translate=""><span>Load more answers</span></button> </div>-->
            <div class="add-answer">
               <!-- ngIf: isInstructor && !replyCreateEnabled --> <!-- ngIf: !isInstructor || replyCreateEnabled -->
               <div ng-if="!isInstructor || replyCreateEnabled">
                  <div ng-show="!answerFormVisible" ng-click="answerFormVisible = !readOnly" class="fx fx-lc imagePart"> <img ng-class="{'img-circle':!isInstructor}" class="add-answer__author img-circle" ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" src="https://udemy-images.udemy.com/user/50x50/anonymous.png"> <input class="ml15 form-control" id="add_question_ans" placeholder="Add an answer" ng-focus="onFocus()" ng-disabled="readOnly"> </div>
                  <answer-form class="answer-form" ng-class="{'visible': answerFormVisible}" ng-animate-disabled="" question="question" on-submit="submitAddAnswer(answer)">
                     <!-- ngIf: editorState.formError.getMessage() --> 
                     <ud-wysiwyg request-editor-init="editorState.initialize" request-reset-value="editorState.resetValue" placeholder-text="Write your response" redactor-theme="simple-image-link" editor-value="editorState.body">
                       <!--  <div class="redactor-box" role="application" dir="ltr">
                           <ul class="redactor-toolbar" id="redactor-toolbar-32" role="toolbar">
                              <li><a href="javascript:void(null);" class="re-button re-link redactor-toolbar-link-dropdown" title="Link" rel="link" role="button" aria-label="Link" tabindex="-1" aria-haspopup="true"><i class="udi udi-link"></i></a></li>
                              <li><a href="javascript:void(null);" class="re-button re-image" title="Image" rel="image" role="button" aria-label="Image" tabindex="-1"><i class="udi udi-picture-o"></i></a></li>
                              <li><a href="javascript:void(null);" class="re-button re-codeInline" title="Code" rel="codeInline" role="button" aria-label="Code" tabindex="-1"><i class="udi udi-curly-braces"></i></a></li>
                           </ul>
                           <span class="redactor-voice-label" id="redactor-voice-32" aria-hidden="false">Rich text editor</span>
                           <div class="redactor-editor redactor-editor-img-edit redactor-in redactor-relative redactor-placeholder" aria-labelledby="redactor-voice-32" role="presentation" id="redactor-uuid-32" contenteditable="true" dir="ltr" style="min-height: 40px;" placeholder="Write your response">
                              <p>&#8203;</p>
                           </div>
                           <textarea class="ud-wysiwyg-textarea" data-autoresize="true" data-theme="simple-image-link" ng-focus="requestEditorInit = true" placeholder="Write your response" id="undefined_876" name="content-32" style="display: none;"></textarea>
                        </div> -->
                     </ud-wysiwyg>
                     <div class="submit-row text-right"> <button type="submit" id="save_question_ans" class="btn btn-sm btn-primary" ng-disabled="editorState.submitDisabled" ng-click="submitForm()"> <span ng-show="editorState.formError.getType() === 'warning'" translate="" class="ng-hide"><span>Proceed</span></span> <span ng-show="editorState.formError.getType() !== 'warning'" translate="" class=""><span>Add an answer</span></span> </button> </div>
                  </answer-form>
               </div>
               <!-- end ngIf: !isInstructor || replyCreateEnabled --> 
            </div>
         </div>
      </div>
   </question-details>
   <!-- end ngIf: questions.activeQuestion && !questions.loading --> <!-- ngIf: !questions.activeQuestion && !questions.loading --> 
</div>


<input type="text" value="<?php echo $postDetails['Post']['id']; ?>" id="post_id" style='display:none'>
<input type="text" value="<?php echo $userid; ?>" id="user_id" style='display:none'>
<input type="text" value="<?php echo $question['CourseQuestion']['id'];?>" id="current_question_id"  style='display:none'>






    


<script>
$(document).ready(function(){
     $(".menues").click(function(){
       $(".menues").removeClass('active');   
       $(this).addClass('active');
     });
     $("#add_question_button").click(function(){
        $("#question_title").val('');
        $('#question_description').html('');
        $("#proceed_post_question").prop('disabled', true);  
        $("#question_listing").css('display','none');  
        $("#add_question").css('display','block');
     });
     $("#close_add_ans").click(function(){
        $("#question_listing").css('display','block');  
        $("#add_question").css('display','none');
     });
     $("#question_title").keyup(function(){
       var value = $(this).val();
       if(value!=''){
          $("#proceed_post_question").prop('disabled', false);
       }else{
          $("#proceed_post_question").prop('disabled', true);
       }
     });
     $('#question_description').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
               .attr('placeholder','');
     }).blur(function(){
          var value = $(this).html();
          if(value!=''){
               $(this).attr('placeholder',$(this).data('placeholder'));
          }
     });

     $("#question_description").keyup(function(){
       var value = $(this).html();
       if(value!=''){
          $(this).data('placeholder',$(this).attr('placeholder'))
               .attr('placeholder','');
       }
     });

     $(document).on("click","#proceed_post_question",function(){
          var q_title = $("#question_title").val();
          var q_description = $('#question_description').html();
          var post_id = $("#post_id").val();
          var user_id = $("#user_id").val();

          $.ajax({
               url: "<?php echo $this->webroot;?>course_questions/ajaxAddQuestion",
               data:{title:q_title,description:q_description,postID:post_id,userID:user_id},
               dataType:'json',
               type: 'POST',     
               success: function(result){
                 if(result.Ack==1){
                    $("#question_title").val('');
                    $('#question_description').html('');
                    $("#proceed_post_question").prop('disabled', true);  
                    $("#question_listing").css('display','block');  
                    $("#add_question").css('display','none');

                    var html = '';

                    html ='<div ng-repeat="question in questions.data" class="course-questions__question question_tab" ng-click="openDetails($event, question)" data-id="'+result.id+'" ><div class="course-questions__question__question-content a11 w100p"><div class="fx-jsb"><div> <img ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" ng-class="{'+ "'img-circle'"+': !question.is_instructor}" src="https://udemy-images.udemy.com/user/50x50/anonymous.png" class="img-circle"> </div><div class="fx pl10 question-content__content"><div class="bold ellipsis">'+q_title+'</div><div class="question-content__body" ng-bind-html="question.body | replaceImgTag | trimTags">'+q_description+'</div></div><div ng-if="showResponses"><div class="tac bold">'+0+'</div><div translate="" translate-n="question.num_replies" translate-plural="Responses"><span>Response</span></div></div></div></div></div>';
                    $("#question_div").append(html);

                 }
               }
          });
     });

     $(document).on('click','.question_tab',function(){
          var id = $(this).data('id');
          $("#current_question_id").val(id);
          $("#question_listing").css('display','none');
          $("#ans_tab").css('display','block');
     });

     $(document).on('click','#to_all_question',function(){
          $("#question_listing").css('display','block');
          $("#ans_tab").css('display','none');
     });

     $(document).on("click","#save_question_ans",function(){
          var data = $("#add_question_ans").val();
          var id = $("#current_question_id").val();
          var post_id = $("#post_id").val();
          var user_id = $("#user_id").val();
          
          $.ajax({
               url: "<?php echo $this->webroot;?>course_answers/ajaxAddAnswer",
               data:{ans:data,question_id:id,postID:post_id,userID:user_id},
               dataType:'json',
               type: 'POST',     
               success: function(result){
                    
                 if(result.Ack==1){
                    var url = window.location.href;
                    window.location.href = url;
                 }
               }
          });
     
     });
});
</script>

<script>
    function handle(e){
        if(e.keyCode === 13){
            e.preventDefault(); // Ensure it is only this code that rusn
          alert($(this).val());
        }
    }
    
    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course'); 
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course'); 
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course'); 
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course'); 
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });


</script>




