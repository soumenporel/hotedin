<section class="quizResult my-5 py-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="py-4">You have finished the quiz successfully</h3>
			</div>
			<div class="col-lg-4 offset-lg-4">
				<div class="resultPart">
					
					<h4 class="mt-4">
						<strong>Total Marks:</strong><span class="pl-3 color-blue"><?php echo $total_marks; ?></span>
					</h4>
					<h4 class="mt-4"><strong>Your Marks Obtain:</strong><span class="pl-3 color-blue"><strong><?php echo $your_score; ?></strong></span></h4>
					<h4 class="mt-4"><strong>Your Percentage of Marks:</strong><span class="pl-3 color-blue"><?php echo $yourPercentage; ?>%</span></h4>
					<?php if($certificate==1){ ?>
                                        <p class="pt-3">
                                            <a class="btn btn-primary certificateButton mt-5 " href="<?php echo $this->webroot;?>learns/certification/<?php echo $quiz_id;?>" target="_blank">Get your Certificate</a></p>
                                        <?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>