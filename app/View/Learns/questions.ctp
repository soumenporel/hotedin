<?php echo $this->element('course_topmenu'); ?>
<style>
  .header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

  .top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important;}
</style>
<input type="hidden" name="data[Rating][ratting]" id="review_val" >
<div class="course-dashboard__bottom">
	<?php echo $this->element('course_midmenu'); ?>
<!-- uiView:  -->


<div class="bottom__content container" id="ans_tab" data-ui-view="" style="display:none;">
   <a class="db mt20" href="javascript:void(null)" ng-click="back()" translate=""><span id="to_all_question" >Back to All Questions</span></a>
   <!-- ngIf: questions.loading -->
   <!-- ngIf: questions.activeQuestion && !questions.loading -->
   <question-details show-new-tab="false" on-delete="onDelete" ng-if="questions.activeQuestion &amp;&amp; !questions.loading" class="" style="">
      <div class="question-details">
         <div class="original-question fx fx-lt">
            <img ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" style="width: 50px; height: 50px;" ng-class="{'img-circle':!question.is_instructor}" src="https://udemy-images.udemy.com/user/50x50/anonymous.png" class="img-circle">
            <div class="question__info a11 fx">
               <question-action-menu show-new-tab="showNewTab" instructor-view="instructorView" on-delete="onDelete" question="question" me="me" class="dib fr">
                  <!-- ngIf: showMenu -->
                  <div class="dropdown question-action-menu" ng-if="showMenu">
                     <button class="btn btn-tertiary btn-xs" data-deny-click-read="true" id="questionActionMenu1839212" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i data-deny-click-read="true" class="udi udi-ellipsis-v"></i> </button>
                     <ul class="dropdown-menu dropdown-menu-right a11" role="menu" aria-labelledby="questionActionMenu1839212">
                        <!-- ngIf: showNewTab -->
                        <!-- ngIf: instructorView && me.canEditCourse -->
                        <!-- ngIf: instructorView -->
                        <!-- ngIf: editQuestionButtonEnabled -->
                        <!-- ngIf: deleteQuestionButtonEnabled -->
                        <!-- ngIf: reportAbuseButtonEnabled -->
<!--                        <li role="presentation" class="report-abuse-button" ng-if="reportAbuseButtonEnabled">
                           <report-abuse show-label="true" show-tooltip="false" object-type="'coursediscussion'" object-id="question.id" tabindex="-1">
                              <react-component props="props" name="ReportAbuse">
                                 <span data-reactroot="">
                                    <button type="button" class="btn btn-default">
                                       <span class="udi udi-flag"></span>
                                        react-text: 4 &nbsp; /react-text 
                                        react-text: 5 Report abuse /react-text 
                                    </button>
                                    <span>
                                        react-empty: 7 
                                        react-empty: 8 
                                    </span>
                                 </span>
                              </react-component>
                           </report-abuse>
                        </li>-->
                        <!-- end ngIf: reportAbuseButtonEnabled -->
                     </ul>
                  </div>
                  <!-- end ngIf: showMenu -->
               </question-action-menu>
               <!-- ngIf: !instructorView -->
               <div ng-if="!instructorView" ng-show="!questionEditorState.isEditing" class="a10 bold">Font in firefox and Chrome</div>
               <!-- end ngIf: !instructorView --> <a ng-href="/user/carlos-eduardo-oliveida-do-nascimento/" target="_blank" href="/user/carlos-eduardo-oliveida-do-nascimento/">Carlos Eduardo Oliveida do</a>
               <!-- ngIf: instructorView -->
               <!-- ngIf: instructorView -->
               <!-- ngIf: question.related_object._class == 'lecture' && question.related_object.object_index -->
               <a ng-if="question.related_object._class == 'lecture' &amp;&amp; question.related_object.object_index" ui-sref="base.taking.lecture({id: question.related_object.id})" ng-click="goToLecture(question.related_object)" translate="" href="/quickstart-angularjs/learn/v4/t/lecture/1881894"><span>· Lecture 8</span></a>
               <!-- end ngIf: question.related_object._class == 'lecture' && question.related_object.object_index -->
               <!-- ngIf: question.related_object._class == 'quiz' && question.related_object.object_index -->
               <!-- ngIf: question.related_object._class == 'practice' && question.related_object.object_index -->
               <!-- ngIf: !instructorView -->
               <span ng-if="!instructorView"> · a month ago </span>
               <!-- end ngIf: !instructorView -->
               <!-- ngIf: questionEditorState.isEditing -->
               <!-- ngIf: !instructorView -->
               <div ng-if="!instructorView" ng-show="!questionEditorState.isEditing" class="question__body" ng-bind-html="question.body | openBlankTarget" prettify="">
                  <p>The title and tagline look so much better in Chrome...why is that? Did you use a font that is specific for chrome? How do I know if a font is compatible with all the current browsers?</p>
                  <p>hanks in advance</p>
               </div>
               <!-- end ngIf: !instructorView -->
               <!-- ngIf: instructorView -->
               <div class="question__editor mt15 ng-hide" ng-show="questionEditorState.isEditing">
                  <!-- ngIf: questionEditorState.formError.getMessage() -->
                  <ud-wysiwyg request-editor-init="questionEditorState.initialize" request-reset-value="questionEditorState.resetValue" placeholder-text="Edit your question here." editor-value="questionEditorState.body" redactor-theme="simple-image-link-source">
                  <textarea data-autoresize="true" data-theme="simple-image-link-source" id="add_question_ans" ng-focus="requestEditorInit = true" placeholder="Edit your question here."></textarea> </ud-wysiwyg>
                  <button ng-click="submitEditQuestion()" ng-disabled="questionEditorState.submitDisabled" class="btn btn-sm btn-primary" type="button" id="save_question_ans" >
                    <span ng-show="questionEditorState.formError.getType() === 'warning'" translate="" class="ng-hide">
                         <span>Proceed</span>
                    </span> <span ng-show="questionEditorState.formError.getType() !== 'warning'" translate="" class=""><span>Save</span></span> </button> <button ng-click="cancelEdit()" class="btn btn-sm btn-tertiary" type="button" translate=""><span>Cancel</span></button>
               </div>
               <!-- ngIf: !instructorView -->
               <question-follow disable="readOnly" ng-if="!instructorView" ng-show="!questionEditorState.isEditing" question="question" class="">
                  <!-- ngIf: !menuItem && question -->
                  <button type="button" class="btn btn-default" ng-if="!menuItem &amp;&amp; question" ng-disabled="disable" ng-class="{'question-following': question.is_following}" ng-click="toggleFollow($event)">
                     <!-- ngIf: !question.is_following --><span translate="" class="text-medium-grey" ng-if="!question.is_following"><span>Follow Responses</span></span>
                     <!-- end ngIf: !question.is_following -->
                     <!-- ngIf: question.is_following -->
                  </button>
                  <!-- end ngIf: !menuItem && question -->
                  <!-- ngIf: menuItem && question -->
               </question-follow>
               <!-- end ngIf: !instructorView -->
            </div>
         </div>
         <div class="answer-list">
            <div class="answers">
               <!-- ngRepeat: answer in answers -->
            </div>
            <div class="fx-c mb10"> <button class="btn btn-primary ng-hide" ng-click="loadAnswers()" ng-disabled="loadingAnswers" ng-show="hasMoreAnswers" translate=""><span>Load more answers</span></button> </div>

         </div>
      </div>
   </question-details>
   <!-- end ngIf: questions.activeQuestion && !questions.loading -->
   <!-- ngIf: !questions.activeQuestion && !questions.loading -->
</div>




<div class="bottom__content container" data-ui-view="" style="">



  <div id="main_div" style="width: 85%; margin: auto;">
     <div ng-if="::questions.featureEnabled" class="content__questions" id="question_listing" style="display:block;">
        <question-title-form>
           <!-- ngIf: ::!createEnabled --> <!-- ngIf: ::is_owner_terms_banned -->
           <div ng-show="!editorState.isEditing" class="mb20">
              <!-- ngIf: displayType==='questionsPanel' --> <!-- ngIf: displayType!=='questionsPanel' -->
              <form ng-if="displayType!=='questionsPanel'" class="ng-pristine ng-valid">
                 <div class="ud-search clearfix fx fx-lc fx-wrap">
<!--                    <div class="fx"> <input ng-model="editorState.title" class="form-control ng-pristine ng-untouched ng-valid" ng-change="searchQuestions()" placeholder="Search for a question" type="text"> </div>
                    <span class="ml20 mr20 mt-30" translate="">
                         <span>or</span>
                    </span>-->
                    <span class="tooltip-container" id="add_question_button" >
                         <button ng-disabled="readOnly || !createEnabled" ng-click="addQuestion($event)" class="ml5 btn btn-primary ask-button" type="button" translate="">
                              <span class="ask-button__long-label" translate="">
                                   <span>Ask a new question</span>
                              </span>
                              <span class="ask-button__short-label" translate="">
                                   <span>Ask</span>
                              </span>
                         </button>
                    </span>
                 </div>
              </form>
              <!-- end ngIf: displayType!=='questionsPanel' -->
           </div>
        </question-title-form>

        <div ng-show="!editorState.isEditing" class="">
           <form class="ng-pristine ng-valid">
              <fieldset>
                 <div class="questions__filters row">
<!--                    <div class="filters__order col-sm-3">
                       <select ng-model="questions.params.ordering" class="form-control w100p ng-pristine ng-untouched ng-valid" ng-change="sortQuestions()" ng-options="option.value as option.name for option in orderOptions">
                          <option label="Sort by popular" value="string:-popularity,-created">Sort by popular</option>
                          <option label="Sort by recency" value="string:-created,-last_activity" selected="selected">Sort by recency</option>
                       </select>
                    </div>
                    <div class="filters__filter col-sm-9">
                    	<label class="checkbox-inline">
                    		<input ng-click="filterFollowedQuestions()" ng-checked="!!questions.params.followed" type="checkbox" style="margin-top: 7px">
                    		<span translate="" class="checkbox-label pr10"><span>See questions I'm following</span>
                    	</label>
                    	 <label class="checkbox-inline">
                    	 	<input ng-click="filterRespondedQuestions()" ng-checked="questions.params.num_replies === 0" type="checkbox" style="margin-top: 7px" >
                    	 	<span class="checkbox-label" translate="">See questions without responses</span>
                    	 </label>
                    </div>-->
                 </div>
              </fieldset>
           </form>
           <question-list questions="questions" show-responses="true" on-select="onSelect">
              <div>
                 <div class="questions-panel__content__course-questions" id="question_div" >
                    <?php foreach ($course_questions as $key => $course_question) { ?>
                    <!-- ngRepeat: question in questions.data -->
                    <div ng-repeat="question in questions.data" class="course-questions__question question_tab " onclick="window.location.href = '<?php echo $this->webroot; ?>learns/answers/<?php echo $postDetails['Post']['slug']; ?>/<?php echo $course_question['CourseQuestion']['id']; ?>'" ng-click="openDetails($event, question)" data-id="<?php echo $course_question['CourseQuestion']['id'];?>" >
                       <div class="course-questions__question__question-content a11 w100p">
                          <div class="fx-jsb">
                             <div> <img ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" ng-class="{'img-circle': !question.is_instructor}" src="https://udemy-images.udemy.com/user/50x50/anonymous.png" class="img-circle"> </div>
                             <div class="fx pl10 question-content__content">
                                <div class="bold ellipsis"><?php echo $course_question['CourseQuestion']['title'];?></div>
                                <div class="question-content__body" ng-bind-html="question.body | replaceImgTag | trimTags"><?php echo $course_question['CourseQuestion']['description'];?></div>
                             </div>
                             <!-- ngIf: showResponses -->
                             <div ng-if="showResponses">
                                <div class="tac bold"><?php echo $course_question['CourseQuestion']['course_answer_count'];?></div>
                                <div translate="" translate-n="question.num_replies" translate-plural="Responses"><span>Response</span></div>
                             </div>
                             <!-- end ngIf: showResponses -->
                          </div>

                       </div>
                    </div>
                    <!-- end ngRepeat: question in questions.data --> <!-- ngIf: questions.loading -->
                    <?php } ?>
                 </div>
                 <!-- <div class="fx-c p20 ng-hide" ng-show="showRaiseHand()">
                    <div class="fx tac">
                       <div class="bold pt10" translate="" ng-hide="questions.params.search"><span>No questions yet</span></div>
                       <div class="bold pt10 ng-hide" translate="" ng-show="questions.params.search"><span>No related questions found</span></div>
                       <div translate=""><span>Be the first to ask your question! You’ll be able to add details in the next step.</span></div>
                       <img src="/staticx/udemy/images/course-taking/raise_your_hand.png">
                    </div>
                 </div> -->
                 <!-- ngIf: questions.hasMore && !questions.loading -->
              </div>
           </question-list>
           <!--<div class="fx-c mt15" style="">
           		<button class="btn btn-primary" translate=""><span>Load more</span></button>
           </div>-->
        </div>
     </div>



<question id="add_question" style="display:none;">
   <div ng-show="editorState.isEditing" class="" style="">
      <p class="mb20 h4" translate="">
<!--          <span>Have a technical issue? Our </span>
          <a ng-href="https://www.udemy.com/support/requests/new/?ticket_form_id=" target="_blank" href="https://www.udemy.com/support/requests/new/?ticket_form_id=">Support Team</a>
          <span> can help.</span>-->
     </p>
      <!-- ngIf: !isInstructor && !replyCreateEnabled -->
      <div class="form-group">
      <input placeholder="Question title" ng-model="editorState.title" class="form-control mb10 ng-pristine ng-valid user-success ng-touched" style="" id="question_title" >
      </div>
      <!-- ngIf: editorState.formError.getMessage() -->
      <ud-wysiwyg request-editor-init="editorState.initialize" request-reset-value="editorState.resetValue" placeholder-text="Describe what you're trying to achieve and where you're getting stuck." editor-value="editorState.body" redactor-theme="simple-image-link">
         <div class="redactor-box" role="application" dir="ltr">
            <ul class="redactor-toolbar" id="redactor-toolbar-12" role="toolbar" style="display:none; position: relative; width: auto; top: 0px; left: 0px; visibility: visible;">
               <li>
                    <a href="javascript:void(null);" class="re-button re-link redactor-toolbar-link-dropdown" title="Link" rel="link" role="button" aria-label="Link" tabindex="-1" aria-haspopup="true"><i class="fa fa-link" aria-hidden="true"></i>

                    </a>
               </li>
               <li>
                    <a href="javascript:void(null);" class="re-button re-image" title="Image" rel="image" role="button" aria-label="Image" tabindex="-1">
                         <i class="fa fa-picture-o" aria-hidden="true"></i>

                    </a>
               </li>
               <li><a href="javascript:void(null);" class="re-button re-codeInline" title="Code" rel="codeInline" role="button" aria-label="Code" tabindex="-1">
                    <i class="fa fa-code" aria-hidden="true"></i>

               </a>
               </li>
            </ul>

            <div class="redactor-editor redactor-editor-img-edit redactor-in redactor-relative redactor-placeholder" aria-labelledby="redactor-voice-12" role="presentation"  dir="ltr" style="min-height: 40px; padding:10px"  placeholder="Describe what you're trying to achieve and where you're getting stuck." contenteditable="true" id="question_description" >
               <!--<p>&#8203;</p>-->
            </div>
            <textarea data-autoresize="true" data-theme="simple-image-link" ng-focus="requestEditorInit = true"  id="undefined_729" name="content-12" placeholder="Describe what you're trying to achieve and where you're getting stuck." style="display: none; width:100%; border: 0" rows="2"></textarea>
         </div>
      </ud-wysiwyg>
      <!--<div class="fx fx-rt"> -->
      	<p class="text-right" style="margin-bottom: 20px">
          <button ng-click="cancelQuestion()" class="mr20 btn btn-sm btn-tertiary" type="button" id="close_add_ans" translate="">
               <span>Cancel</span>
          </button>
          <button ng-click="submitQuestion($event)" ng-disabled="editorState.submitDisabled || editorState.title.length &lt; 1" class="btn btn-sm btn-primary" type="button" id="proceed_post_question">
               <span ng-show="editorState.formError.getType() === 'warning'" translate="" class="ng-hide"><span>Proceed</span>
          </span>
          <span ng-show="editorState.formError.getType() !== 'warning'" translate="" class="">
               <span>Post Question</span>
          </span>
          </button>
          </p>
     </div>
   <!--</div>-->
</question>





</div>
</div>
<input type="text" value="<?php echo $postDetails['Post']['id']; ?>" id="post_id" style='display:none'>
<input type="text" value="<?php echo $userid; ?>" id="user_id" style='display:none'>
<input type="text" id="current_question_id" style='display:none'>









<script>
$(document).ready(function(){
     $(".menues").click(function(){
       $(".menues").removeClass('active');
       $(this).addClass('active');
     });
     $("#add_question_button").click(function(){
        $("#question_title").val('');
        $('#question_description').html('');
        $("#proceed_post_question").prop('disabled', true);
        $("#question_listing").css('display','none');
        $("#add_question").css('display','block');
     });
     $("#close_add_ans").click(function(){
        $("#question_listing").css('display','block');
        $("#add_question").css('display','none');
     });
     $("#question_title").keyup(function(){
       var value = $(this).val();
       if(value!=''){
          $("#proceed_post_question").prop('disabled', false);
       }else{
          $("#proceed_post_question").prop('disabled', true);
       }
     });
     $('#question_description').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
               .attr('placeholder','');
     }).blur(function(){
          var value = $(this).html();
          if(value!=''){
               $(this).attr('placeholder',$(this).data('placeholder'));
          }
     });

     $("#question_description").keyup(function(){
       var value = $(this).html();
       if(value!=''){
          $(this).data('placeholder',$(this).attr('placeholder'))
               .attr('placeholder','');
       }
     });

     $(document).on("click","#proceed_post_question",function(){
          var q_title = $("#question_title").val();
          var q_description = $('#question_description').html();
          var post_id = $("#post_id").val();
          var user_id = $("#user_id").val();

          $.ajax({
               url: "<?php echo $this->webroot;?>course_questions/ajaxAddQuestion",
               data:{title:q_title,description:q_description,postID:post_id,userID:user_id},
               dataType:'json',
               type: 'POST',
               success: function(result){
                 if(result.Ack==1){
                    $("#question_title").val('');
                    $('#question_description').html('');
                    $("#proceed_post_question").prop('disabled', true);
                    $("#question_listing").css('display','block');
                    $("#add_question").css('display','none');

                    var html = '';

                    html ='<div ng-repeat="question in questions.data" class="course-questions__question question_tab" ng-click="openDetails($event, question)" data-id="'+result.id+'" ><div class="course-questions__question__question-content a11 w100p"><div class="fx-jsb"><div> <img ng-src="https://udemy-images.udemy.com/user/50x50/anonymous.png" ng-class="{'+ "'img-circle'"+': !question.is_instructor}" src="https://udemy-images.udemy.com/user/50x50/anonymous.png" class="img-circle"> </div><div class="fx pl10 question-content__content"><div class="bold ellipsis">'+q_title+'</div><div class="question-content__body" ng-bind-html="question.body | replaceImgTag | trimTags">'+q_description+'</div></div><div ng-if="showResponses"><div class="tac bold">'+0+'</div><div translate="" translate-n="question.num_replies" translate-plural="Responses"><span>Response</span></div></div></div></div></div>';
                    $("#question_div").append(html);

                 }
               }
          });
     });

     // $(document).on('click','.question_tab',function(){
     //      var id = $(this).data('id');
     //      $("#current_question_id").val(id);
     //      $("#question_listing").css('display','none');
     //      $("#ans_tab").css('display','block');
     // });

     // $(document).on('click','#to_all_question',function(){
     //      $("#question_listing").css('display','block');
     //      $("#ans_tab").css('display','none');
     // });

     // $(document).on("click","#save_question_ans",function(){
     //      var data = $("#add_question_ans").val();
     //      var id = $("#current_question_id").val();
     //      var post_id = $("#post_id").val();
     //      var user_id = $("#user_id").val();

     //      $.ajax({
     //           url: "<?php echo $this->webroot;?>course_answers/ajaxAddAnswer",
     //           data:{ans:data,question_id:id,postID:post_id,userID:user_id},
     //           dataType:'json',
     //           type: 'POST',
     //           success: function(result){

     //             if(result.Ack==1){
     //                $("#add_question_ans").val('');
     //                $("#question_listing").css('display','block');
     //                $("#ans_tab").css('display','none');
     //             }
     //           }
     //      });

     // });
});
</script>

<script>
    function handle(e){
        if(e.keyCode === 13){
            e.preventDefault(); // Ensure it is only this code that rusn
          alert($(this).val());
        }
    }

    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });


</script>
