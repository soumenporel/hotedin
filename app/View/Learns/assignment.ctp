<style type="text/css">
    .current {
        background: #1376d7;
        color: #ffffff;
    }

    .header {
        position: relative !important;
    }

    .inner_header {
        background: #0c2440 none repeat scroll 0 0;
    }

    .top--container {
        height: auto !important;
    }

    .progress {
        height: 9px;
        border-radius: 10px;
    }

    .filled-stars {
        *margin-top: 5px !important;
    }

    .ratingPart .rating-xs {
        font-size: 25px !important;
    }

    .panel {
        border-radius: 0 !important;
    }
</style>
<?php //echo '<pre>'; print_r($readList); echo '</pre>';?>
<div class="ud-angular-loaded" data-module-id="course-taking-v4" data-module-name="ng/apps/course-taking-v4/app">
    <?php //echo '<pre>'; print_r($userdetails); echo '</pre>';?>
    <!-- uiView:  -->
    <div>
        <!-- uiView: undefined -->
        <ui-view class="" style="">
            <div>
                <?php echo $this->element('course_topmenu'); ?>
                <div class="course-dashboard__bottom">

                    <?php echo $this->element('course_midmenu'); ?>
                    <!-- uiView:  -->

                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <link rel="stylesheet" href="/resources/demos/style.css">


                    <div class="container" style="padding-top:25px; padding-bottom:40px;">
                        <div class="row" style="margin: auto;">
                            <div class="col-md-12">
                                <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                    <?php
                                    $serial_no = 1;
                                    $count = count($assignments);

                                    foreach ($assignments as $key => $assignment) {
                                      
                                      $assignmentStatus = $this->requestAction('/learns/assignmentStatus/' . $assignment['Assignment']['id'] . '/' . $userid);
                                      //print_r($assignmentStatus);
                                      ?>
                                        <div class="panel panel-default custom-panel">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <div class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $serial_no; ?>" aria-expanded="true"
                                                        aria-controls="collapse<?php echo $serial_no; ?>">
                                                        <span class="sec-name pull-left">Assignment Subject</span>
                                                        <span class="item-no pull-right">
                                                            <?php echo $serial_no; ?>/
                                                            <?php echo $count; ?>
                                                        </span>
                                                        <div class="clearfix"></div>
                                                        <div class="customHeadPart">
                                                            <h4 class="customHeading">
                                                                <?php echo $assignment['Assignment']['subject']; ?>
                                                            </h4>
                                                            <span class="float-right color-red customNumber font-20">2</span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>

                                            </div>
                                            <div id="collapse<?php echo $serial_no; ?>" class="panel-collapse collapse custom-collapse" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    <?php if ($assignmentStatus == 0) {?>
                                                    <form action="" method="post" enctype="multipart/form-data">
                                                        <input type="hidden" class="form-control" name="assignment_id" value="<?php echo $assignment['Assignment']['id']; ?>">
                                                        <input type="hidden" class="form-control" name="total_marks" value="<?php echo $assignment['Assignment']['total_marks']; ?>">
                                                        <?php
                                                        $sl_no = 1;
                                                        $optionsDecode = json_decode($assignment['Assignment']['question']);
                                                        //print_r($optionsDecode);die;
                                                        foreach ($optionsDecode as $key => $value) {
                                                        ?>
                                                          <p class="customPara">
                                                              <?php echo $sl_no . ". " . $value->question . " -" . $value->marks; ?>
                                                          </p>
                                                          <span class="float-right color-red customNumber">2</span>
                                                          <div class="form-group question_gr questionTextPart">
                                                              <input type="hidden" class="form-control" name="ids[]" value="<?php echo $value->id; ?>">

                                                              <input type="hidden" class="form-control" name="questions[]" value="<?php echo $value->question; ?>">

                                                              <input type="hidden" class="form-control" name="markss[]" value="<?php echo $value->marks; ?>">

                                                              <textarea name="answers[]"></textarea>
                                                          </div>
                                                        <?php
                                                          $sl_no = $sl_no + 1;
                                                        }
                                                        ?>

                                                        <div class="bulk_uploader">
                                                            <button type="submit" class="btn btn-danger save_button">Submit</button>
                                                        </div>
                                                    </form>
                                                    <?php
                                                    } elseif ($assignmentStatus['UserAssignment']['status'] == 1) {
                                                      echo "Your assignment has submitted for evalution";
                                                    } elseif ($assignmentStatus['UserAssignment']['status'] == 2 && $assignmentStatus['UserAssignment']['total_score'] != '') {
                                                    ?>
                                                    <div class="resultPart">
                                                        <h4>
                                                            <strong>Total Marks:</strong>
                                                            <span class="pl-3 color-blue">
                                                                <strong>
                                                                    <?php echo $assignmentStatus['UserAssignment']['total_score']; ?>
                                                                </strong>
                                                            </span>
                                                        </h4>
                                                        <h4 class="my-4 font-22">
                                                            <strong class="font-22">Your Marks Obtain:</strong>
                                                            <span class="pl-3 color-blue">
                                                                <strong class="font-22">
                                                                    <?php echo $assignmentStatus['UserAssignment']['total_score']; ?>
                                                                </strong>
                                                            </span>
                                                        </h4>
                                                        <?php
                                                        $sl_no = 1;
                                                        $optionsScoreDecode = json_decode($assignmentStatus['UserAssignment']['answer']);
                                                        //print_r($optionsDecode);die;
                                                        foreach ($optionsScoreDecode as $key1 => $value1) { //print_r($value1);
                                                        ?>
                                                          <div style="overflow:hidden;">
                                                              <p class="customPara">
                                                                  <?php echo $value1->id . ". " . $value1->question . " -" . $value1->marks; ?>
                                                                  <span class="float-right color-red customNumber">2</span>
                                                                  <span class="d-block">Your Score -
                                                                      <?php echo $value1->score; ?>
                                                                  </span>
                                                              </p>
                                                          </div>
                                                        <?php
                                                        $sl_no = $sl_no + 1;
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>

                                            </div>
                                        </div>
                                      <?php
                                      $serial_no = $serial_no + 1;
                                    }
                                    ?>
                                </div>
                                <!-- <div class="panel panel-default custom-panel">
                                  <div class="panel-heading" role="tab" id="headingOne">
                                    <div class="panel-title">
                                      <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        <span class="sec-name pull-left">Section Name</span>
                                        <span class="item-no pull-right">0/2</span>
                                        <div class="clearfix"></div>
                                        <h4>Java</h4>
                                      </a>
                                    </div>
                                  </div>
                                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                      <ul class="panel-listing">
                                        <li>
                                          <a href="">
                                            <span class="vid_icon"><i class="fa fa-play-circle-o"></i></span>
                                            <span class="fx">
                                              <span class="no">1.</span>
                                              <span>Introduction</span>
                                            </span>
                                            <div class="right-part">
                                              <span class="time">3:23</span>
                                              <div class="content-box">
                                                <span class="checkboxFour">
                                                    <input type="checkbox" checked="" name="lecturecheckbox1" id="checkboxSixInput1" value="3">
                                                    <label for="checkboxSixInput1"></label>
                                                  </span>
                                              </div>
                                            </div>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="">
                                            <span class="vid_icon"><i class="fa fa-play-circle-o"></i></span>
                                            <span class="fx">
                                              <span class="no">1.</span>
                                              <span>Introduction</span>
                                            </span>
                                            <div class="right-part">
                                              <span class="time">3:23</span>
                                              <div class="content-box">
                                                <span class="checkboxFour">
                                                    <input type="checkbox" checked="" name="lecturecheckbox1" id="checkboxSixInput1" value="3">
                                                    <label for="checkboxSixInput1"></label>
                                                  </span>
                                              </div>
                                            </div>
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span open-modal="" auto-open="openSurvey" enable-loader="true" on-close="onGoalsSurveyClose()" has-header="true" backdrop="static"
                href="">
                <span system-message="" message-id="student_goals_survey" object-type="course" object-id="course.id"> </span>
            </span>
        </ui-view>
    </div>
    <div class="loading-screen" style="">
        <span class="preloader--center preloader--lg icon-spin">
            <i class="udi udi-circle-loader text-white"></i>
        </span>
    </div>
</div>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content"
        });
    });

    $('.video_preview').click(function () {
        var self = $(this);
        var lecture_id = self.data('id');
        $.ajax({
            url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideo',
            type: 'POST',
            dataType: 'json',
            data: { lecture_id: lecture_id },
            success: function (data) {
                if (data.Ack == 1) {
                    $(".modal-body").html('');
                    $(".modal-body").html(data.html);
                    /*$('#videoModal').modal('show');*/
                }
            }
        });
    });

    $('.lecture_resours').click(function () {

        var self = $(this);
        var lecture_id = self.data('id');
        var userid = $('#loginid').val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideostatus',
            type: 'POST',
            dataType: 'json',
            data: { lecture_id: lecture_id, userId: userid },
            success: function (data) {
                console.log(data);
            }
        });
    });
</script>

<script>
    //------ Favorite Unfavorite Section ---------//
    $(document).on('click', '#favorite_this_course', function () {
        var self = $(this);
        var post_id = $(this).data('id');
        var user_id = $('#hidden_user_id').val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
            type: 'POST',
            dataType: 'json',
            data: {
                postid: post_id,
                userid: user_id
            },
            success: function (data) {
                if (data.Ack == '1') {
                    self.html('Unfavorite this course');
                    self.attr("id", "unfavorite_this_course");
                } else {
                    alert(data.res);
                }
            }
        });
    });
    $(document).on('click', '#unfavorite_this_course', function () {
        var self = $(this);
        var post_id = $(this).data('id');
        var user_id = $('#hidden_user_id').val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
            type: 'POST',
            dataType: 'json',
            data: {
                postid: post_id,
                userid: user_id
            },
            success: function (data) {
                if (data.Ack == '1') {
                    self.html('Favorite this course');
                    self.attr("id", "favorite_this_course");
                } else {
                    alert(data.res);
                }
            }
        });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click', '#archive_this_course', function () {
        var self = $(this);
        var post_id = $(this).data('id');
        var user_id = $('#hidden_user_id').val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>archives/archive_this_course',
            type: 'POST',
            dataType: 'json',
            data: {
                postid: post_id,
                userid: user_id
            },
            success: function (data) {
                if (data.Ack == '1') {
                    self.html('Unarchive this course');
                    self.attr("id", "unarchive_this_course");
                } else {
                    alert(data.res);
                }
            }
        });
    });
    $(document).on('click', '#unarchive_this_course', function () {
        var self = $(this);
        var post_id = $(this).data('id');
        var user_id = $('#hidden_user_id').val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
            type: 'POST',
            dataType: 'json',
            data: {
                postid: post_id,
                userid: user_id
            },
            success: function (data) {
                if (data.Ack == '1') {
                    self.html('Archive this course');
                    self.attr("id", "archive_this_course");
                } else {
                    alert(data.res);
                }
            }
        });
    });
</script>


<head>
    <link href="http://vjs.zencdn.net/5.8.8/video-js.css" rel="stylesheet">

    <!-- If you'd like to support IE8 -->
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>

<!-- <video id="my-video" class="video-js" controls preload="auto" width="640" height="264"
  poster="MY_VIDEO_POSTER.jpg" data-setup='{"techOrder": ["html5"]}'>
    <source src="/learnfly/img/post_video/927018297_54515485.mp4" type="video/mp4">
    <source src="/learnfly/img/post_video/927018297_54515485.ogg" type="video/ogg">
    <p class="vjs-no-js">
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
  </video>

  <script src="http://vjs.zencdn.net/5.8.8/video.js"></script> -->