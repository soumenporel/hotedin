<?php //pr($announcements); //exit;?>
<style>
  .header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

  .top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important;}
</style>
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom">
               <?php echo $this->element('course_midmenu'); ?>
              <!-- uiView:  -->


<?php
if(!empty($announcements)){ ?>
<div class="bottom__content container" >
   <!-- ngIf: num_announcements > 0 -->
   <div class="content__announcements mt-5  pb-5">
      <!-- ngIf: announcementsLoading --> <!-- ngIf: !announcementsLoading -->
      <div>
         <!-- ngIf: announcements.length === 0 --> <!-- ngIf: announcements.length > 0 -->
         <div>


          <?php foreach ($announcements as $key => $announcement) {
            
              ?>
            <!-- ngRepeat: announcement in announcements -->
            <div class="panel">
              <div class="panel-heading announcement__details p-3 bg-white">
                    <?php
                    
                    if( isset($announcement['User']['user_image']) && $announcement['User']['user_image']!=''){
                         $img = $this->webroot.'user_images/'.$announcement['User']['user_image'];
                    }else{
                         $img = $this->webroot.'img/profile_img.jpg';
                    }
                    ?>
                  <img  alt="" src="<?php echo $img;?>" width="50" height="50" class="rounded-circle">
                  <div class="announcement__details__name">
                    <span>
                      <a href="javascript:void(0)" class="ellipsis" style="color: #104A80"> <?php echo $announcement['User']['first_name'].' '.$announcement['User']['last_name'] ?>
                      </a>
                      <span class="font-12"> posted an announcement</span>
                    </span>
                    <span class="font-12">‧<?php
                                        $start_date = new DateTime($announcement['Announcement']['post_time']);
                                        $your_date = date('Y-m-d H:i:s');
                                        $since_start = $start_date->diff(new DateTime($your_date));
                                        //echo $since_start = floor(($your_date - $start_date) / (60 * 60 * 24));
                                        //echo $since_start->days.' days total';
                                        $y = $since_start->y . ' years ago';
                                        $m = $since_start->m . ' months ago';
                                        $d = $since_start->d . ' days ago';
                                        $h = $since_start->h . ' hours ago';
                                        $i = $since_start->i . ' minutes ago';
                                        $s = $since_start->s . ' seconds ago';
                                        if ($y != 0) {
                                            echo $y;
                                        } else {
                                            if ($m != 0) {
                                                echo $m;
                                            } else {
                                                if ($d != 0) {
                                                    echo $d;
                                                } else {
                                                    if ($h != 0) {
                                                        echo $h;
                                                    } else {
                                                        if ($i != 0) {
                                                            echo $i;
                                                        } else {
                                                            if ($s != 0) {
                                                                echo $s;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                    </span>
                    <!-- ngIf: reportAbusePopup.enabled -->
                    <span class="button-group__report-abuse">
                        ‧
                        <report-abuse btn-class="'btn-link'" object-type="'courseannouncement'" object-id="announcement.id">
                          <react-component props="props" name="ReportAbuse">
                              <span>
                                 <!-- <button type="button" class="btn-link btn btn-default"> -->
                                  <span class="udi udi-flag"></span><!-- react-text: 4 -->&nbsp;<!-- /react-text --><!-- react-text: 5 --><!-- /react-text -->
                              </span>
                          </react-component>
                        </report-abuse>
                    </span>
                     <!-- end ngIf: reportAbusePopup.enabled -->
                  </div>
              </div>
              <div class="panel-body bg-white">
                  <!-- ngIf: !!announcement.title -->
                  <div class="announcement__body d-flex px-5">
                    <?php if(isset($announcement['Announcement']['media']) && $announcement['Announcement']['media']!=''){ ?>
                    <p class="mr-3">
                      <img src="<?php echo $this->webroot.'announcement_media/'.$announcement['Announcement']['media'];?>" class="">
                    </p>
                    <?php } ?>
                    <div class="">
                      <p class="mb-1">
                       <?php echo $announcement['Announcement']['announcement']; ?>
                      </p>
                    </div>
                  </div>
<!--                  <comment-thread comment-resource="commentResource" comment-resource-path-params="{announcementId: announcement.id}" comment-thread="announcement.comment_thread">
                    <div class="mt10 px-3 mb-3 bg-white">
                         ngIf: canReply && cannedComments 
                         ngIf: canReply 
                        <comment comment="placeholderComment" >
                            <div class="component-thread-head" style="display: block">

                              <?php foreach ($announcement['AnnouncementComment'] as $key => $comment) { ?>
                                <div class=" d-flex">
                                <div class="thread-head__avatar thread-head__avatar--stick-top">
                                  <?php
                                    if( isset($comment['User']['user_image']) && $comment['User']['user_image']!=''){
                                         $img = $this->webroot.'user_images/'.$comment['User']['user_image'];
                                    }else{
                                         $img = $this->webroot.'img/profile_img.jpg';
                                    }
                                  ?>
                                  <img class="round rounded-circle"  alt="Commenter" title="Commenter" src="<?php echo $img; ?>" width="40" height="40">
                                </div>

                                <div class="thread-head__basic-info" >
                                  <a ng-href="" class="inline inline--no-padding" ng-bind="comment.user.title" data-purpose="commenter-name"><?php echo $comment['User']['first_name'].' '.$comment['User']['last_name'];?>
                                  </a>
                                  <time class="inline inline--no-padding font-12">· <?php
                                        $start_date = new DateTime($comment['comment_time']);
                                        $your_date = gmdate('Y-m-d H:i:s');
                                        $since_start = $start_date->diff(new DateTime($your_date));
                                        //echo $since_start = floor(($your_date - $start_date) / (60 * 60 * 24));
                                        //echo $since_start->days.' days total';
                                        $y = $since_start->y . ' years ago';
                                        $m = $since_start->m . ' months ago';
                                        $d = $since_start->d . ' days ago';
                                        $h = $since_start->h . ' hours ago';
                                        $i = $since_start->i . ' minutes ago';
                                        $s = $since_start->s . ' seconds ago';
                                        if ($y != 0) {
                                            echo $y;
                                        } else {
                                            if ($m != 0) {
                                                echo $m;
                                            } else {
                                                if ($d != 0) {
                                                    echo $d;
                                                } else {
                                                    if ($h != 0) {
                                                        echo $h;
                                                    } else {
                                                        if ($i != 0) {
                                                            echo $i;
                                                        } else {
                                                            if ($s != 0) {
                                                                echo $s;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                  </time>
                                  <div class="annoucement_comment_div font-14">
                                    <?php if($comment['User']['id']==$userid){ ?>
                                       <button class="inline fr btn btn-link delete_comment" data-commentid="<?php echo $comment['id']; ?>" >
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                       </button>
                                       <button class="inline fr btn btn-link edit_comment" data-commentid="<?php echo $comment['id']; ?>" >
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                       </button>
                                      <?php } ?>
                                     <p class="no-compress user_announcement_comment" ><?php echo $comment['comment']; ?></p>
                                  </div>
                                </div>


                                </div>
                              <?php } ?>
                            </div>
                              <div class="thread-head__basic-info pb-3" data-purpose="edit-container" >
                                <textarea class="form-control ng-pristine ng-untouched ng-valid add_comment" data-userid="<?php echo $userid; ?>" data-annoucementid="<?php echo $announcement['Announcement']['id']; ?>"  placeholder="Enter your comment"  style="overflow-y: hidden; resize: none; height: 45px;"></textarea>
                                  <div  translate="" class="mt-2 font-12">
                                    <span>Press
                                    </span>
                                    <b>Enter</b>
                                    <span> to post,
                                    </span>
                                    <b>Esc</b>
                                    <span> to cancel,
                                    </span>
                                    <b>Shift + Enter</b>
                                    <span> to go to a new line
                                    </span>
                                  </div>
                              </div>
                        </comment>

                         <a>
                          <span>Show comments (1)</span>
                        </a> 

                    </div>
                  </comment-thread>-->
              </div>
            </div>
<?php } ?>

            <!-- end ngRepeat: announcement in announcements -->
         </div>
         <!-- end ngIf: announcements.length > 0 -->
      </div>
      <!-- end ngIf: !announcementsLoading -->
   </div>
</div>
<?php }else{ ?>
<div class="tac" style="margin-top: 100px;padding-bottom: 100px;">

   <div class="a1" translate=""><span><h1 style="font-size:28px; margin-bottom10px; ">No announcements posted yet.</h1></span></div>
   <p translate=""><span style="font-size: 14px;">The instructor hasn’t added any announcements to this course yet. Announcements are used to </br> inform you of updates or additions to the course.</span></p>
</div>
<?php } ?>

<script>
    function handle(e){
        if(e.keyCode === 13){
            e.preventDefault(); // Ensure it is only this code that rusn
          alert($(this).val());
        }
    }

    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });


    $(document).on('keyup','.add_comment',function(e){
        var self = $(this);
        var user_id = self.data('userid');
        var announcementid = self.data('annoucementid');
        var text = self.val();
        if(e.which === 13 && !e.shiftKey) {
          if(text!=''){
            $.ajax({
                url: '<?php echo $this->webroot; ?>announcement_comments/ajaxSaveAnnouncementComment',
                type: 'POST',
                dataType: 'json',
                data: {
                    a_id: announcementid,
                    userid: user_id,
                    a_text: text
                },
                success: function (data) {
                  if(data.Ack==1){
                    var URL = window.location.href;
                    window.location.href = URL;
                  }
                }
            });
          }
        }
        if(e.which == 27) {
          self.blur();
          self.val('');
        }
        if(e.which == 13 && e.shiftKey) {
          self.val(self.val() + "\n");// use the right id here
          return true;
        }
    });

    $(document).on('click','.delete_comment',function(){

      var self = $(this);
      var comment_id = self.data('commentid');
        $.ajax({
            url: '<?php echo $this->webroot; ?>announcement_comments/ajaxDeleteComment',
            type: 'POST',
            dataType: 'json',
            data: {
                c_id: comment_id,
            },
            success: function (data) {
              if(data.Ack==1){
                var URL = window.location.href;
                    window.location.href = URL;
              }
            }
        });
    });

    $(document).on('click','.edit_comment',function(){

      $("div").focusin();
      var selfe = $(this);
      var comment_id = selfe.data('commentid');
      var text = selfe.closest('.annoucement_comment_div').find('.user_announcement_comment').html();
      $('<textarea class="form-control ng-pristine ng-untouched ng-valid edit_added_comment" data-userid="<?php echo $userid; ?>" data-annoucementid="<?php echo $announcement["Announcement"]["id"]; ?>" data-commentid="'+comment_id+'" placeholder="Enter your comment"  style="overflow-y: hidden; resize: none; height: 45px;"></textarea>').insertAfter(selfe.closest('.annoucement_comment_div')).focus().val(text);
        selfe.closest('.annoucement_comment_div').hide();

        $('.edit_added_comment').on('keyup',function(e){
            var self = $(this);
            var user_id = self.data('userid');
            var c_id = self.data('commentid');
            var announcementid = self.data('annoucementid');
            var text = self.val();
            if(e.which === 13 && !e.shiftKey) {
              e.preventDefault();
              if(text!=''){
                $.ajax({
                    url: '<?php echo $this->webroot; ?>announcement_comments/ajaxEditComment',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        a_id: announcementid,
                        userid: user_id,
                        a_text: text,
                        c_id:c_id
                    },
                    success: function (data) {
                      if(data.Ack==1){
                        var URL = window.location.href;
                        window.location.href = URL;
                      }
                    }
                });
              }
            }
            if(e.which == 27) {
              self.hide();
              selfe.closest('.annoucement_comment_div').show();
            }
            if(e.which == 13 && e.shiftKey) {
              self.val(self.val() + "\n");// use the right id here
              return true;
            }
      });

      $('.edit_added_comment').on('keypress',function(e){
          if(e.which === 13 && !e.shiftKey) {
            e.preventDefault();
          }
      });

    });

</script>

<script>
  $( function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      heightStyle: "content"
    });
  } );

  $('.video_preview').click(function(){
       var self = $(this);
       var lecture_id = self.data('id');
       $.ajax({
           url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideo',
           type: 'POST',
           dataType: 'json',
           data: {lecture_id:lecture_id},
           success: function (data) {
               if(data.Ack==1){
                   $(".modal-body").html('');
                   $(".modal-body").html(data.html);
                   /*$('#videoModal').modal('show');*/
               }
           }
       });
   });

    $('.lecture_resours').click(function(){

        var self = $(this);
        var lecture_id = self.data('id');
        var userid = $('#loginid').val();
        $.ajax({
          url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideostatus',
            type: 'POST',
            dataType: 'json',
            data: {lecture_id:lecture_id,userId:userid},
            success: function (data) {
               console.log(data);
            }
        });
    });
  </script>

<script>
    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
</script>
