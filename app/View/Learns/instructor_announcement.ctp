<?php //pr($announcements); //exit;?>
<style>
  .header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

  .top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important;}
</style>
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom">
               <?php echo $this->element('course_midmenu'); ?>
              <!-- uiView:  -->


<?php
if(!empty($announcements)){ ?>
<div class="bottom__content container" >
   <!-- ngIf: num_announcements > 0 -->
   <div class="content__announcements mt-5  pb-5">
      <!-- ngIf: announcementsLoading --> <!-- ngIf: !announcementsLoading -->
      <div>
         <!-- ngIf: announcements.length === 0 --> <!-- ngIf: announcements.length > 0 -->
         <div>


          
            
<!--            for edit delete announcement...........................................-->
             <comment-thread comment-resource="commentResource" comment-resource-path-params="{announcementId: announcement.id}" comment-thread="announcement.comment_thread">
                    <div class="mt10 px-3 mb-3 bg-white">
                        <!-- ngIf: canReply && cannedComments -->
                        <!-- ngIf: canReply -->
                        <comment comment="placeholderComment" >
                            <div class="component-thread-head announcement_content" style="display: block">

                              <?php foreach ($announcements as $key => $comment) {?>
                                <div class=" d-flex">
                               
                                <div class="thread-head__basic-info " >
                                
                                  <div class="annoucement_comment_div font-14">
                                   
                                       <button class="inline fr btn btn-link delete_comment" data-commentid="<?php echo $comment['Announcement']['id']; ?>" >
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                       </button>
                                       <button class="inline fr btn btn-link edit_comment" data-commentid="<?php echo $comment['Announcement']['id']; ?>" >
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                       </button>
                                       <p class="mb-1"><strong><?php echo $comment['Announcement']['title']; ?></strong></p>
                                  
                                     <p class="no-compress user_announcement_comment" ><?php echo $comment['Announcement']['announcement']; ?></p>
                                  </div>
                                </div>


                                </div>
                              <?php } ?>
                            </div>
<!--                              <div class="thread-head__basic-info pb-3" data-purpose="edit-container" >
                                <textarea class="form-control ng-pristine ng-untouched ng-valid add_comment" data-userid="<?php echo $userid; ?>" data-annoucementid="<?php echo $comment['Announcement']['id']; ?>"  placeholder="Enter your comment"  style="overflow-y: hidden; resize: none; height: 45px;"></textarea>
                                  <div  translate="" class="mt-2 font-12">
                                    <span>Press
                                    </span>
                                    <b>Enter</b>
                                    <span> to post,
                                    </span>
                                    <b>Esc</b>
                                    <span> to cancel,
                                    </span>
                                    <b>Shift + Enter</b>
                                    <span> to go to a new line
                                    </span>
                                  </div>
                              </div>-->
                        </comment>

                        <!-- <a>
                          <span>Show comments (1)</span>
                        </a> -->

                    </div>
                  </comment-thread>

            <div class="panel" style="margin-top: 30px;">
             <div class="mt10 px-3 mb-3 bg-white">
                 <div class="panel-heading announcement__details p-3 bg-white"><h2>Post Announcement</h2></div>      
                           
                              <div class="thread-head__basic-info pb-3" data-purpose="edit-container" >
                                <input type="text" class="form-control add_comment" placeholder="Title"  style="overflow-y: hidden; resize: none; height: 45px;" id="announcement_title"/>
                                <p style="height: 20px;"></p>
                                <textarea class="form-control ng-pristine ng-untouched ng-valid add_comment" placeholder="Announcement Details"  style="overflow-y: hidden;" id="announcement_description"></textarea>
                                  <div  translate="" class="mt-2 font-12">
                                    <div class="post-btn">

                                    <button class="btn btn-danger" onclick="submitComment();">Submit</button>
                                    </div>  
                                  </div>
                              </div>
                      
                     
                    </div>
             </div>
            <!-- end ngRepeat: announcement in announcements -->
         </div>
         <!-- end ngIf: announcements.length > 0 -->
      </div>
      <!-- end ngIf: !announcementsLoading -->
   </div>
</div>
<?php }else{ ?>
<div class="tac" style="margin-top: 100px;padding-bottom: 100px;">

   <div class="a1" translate=""><span><h1 style="font-size:28px; margin-bottom10px; ">No announcements posted yet.</h1></span></div>
   <p translate=""><span style="font-size: 14px;">The instructor hasn’t added any announcements to this course yet. Announcements are used to </br> inform you of updates or additions to the course.</span></p>
</div>
<?php } ?>

<script>
    function handle(e){
        if(e.keyCode === 13){
            e.preventDefault(); // Ensure it is only this code that rusn
          alert($(this).val());
        }
    }

    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });


    $(document).on('keyup','.add_comment',function(e){
        var self = $(this);
        var user_id = self.data('userid');
        var announcementid = self.data('annoucementid');
        var text = self.val();
        if(e.which === 13 && !e.shiftKey) {
          if(text!=''){
            $.ajax({
                url: '<?php echo $this->webroot; ?>announcement_comments/ajaxSaveAnnouncementComment',
                type: 'POST',
                dataType: 'json',
                data: {
                    a_id: announcementid,
                    userid: user_id,
                    a_text: text
                },
                success: function (data) {
                  if(data.Ack==1){
                    var URL = window.location.href;
                    window.location.href = URL;
                  }
                }
            });
          }
        }
        if(e.which == 27) {
          self.blur();
          self.val('');
        }
        if(e.which == 13 && e.shiftKey) {
          self.val(self.val() + "\n");// use the right id here
          return true;
        }
    });

    $(document).on('click','.delete_comment',function(){

      var self = $(this);
      var comment_id = self.data('commentid');
      alert(comment_id);
        $.ajax({
            url: '<?php echo $this->webroot; ?>learns/ajaxDeleteComment',
            type: 'POST',
            dataType: 'json',
            data: {
                id: comment_id,
            },
            success: function (data) {
              if(data==1){
                var URL = window.location.href;
                    window.location.href = URL;
              }
            }
        });
    });

    $(document).on('click','.edit_comment',function(){

      $("div").focusin();
      var selfe = $(this);
      var comment_id = selfe.data('commentid');
      var text = selfe.closest('.annoucement_comment_div').find('.user_announcement_comment').html();
      $('<textarea class="form-control ng-pristine ng-untouched ng-valid edit_added_comment" data-annoucementid="'+comment_id+'" style="overflow-y: hidden; resize: none; height: 45px;"></textarea>').insertAfter(selfe.closest('.annoucement_comment_div')).focus().val(text);
        selfe.closest('.annoucement_comment_div').hide();

        $('.edit_added_comment').on('keyup',function(e){
            var self = $(this);
           
            var announcementid = self.data('annoucementid');
            var text = self.val();
            //alert(announcementid);
            //alert(text);
            if(e.which === 13 && !e.shiftKey) {
              e.preventDefault();
              if(text!=''){
                  //alert(1);
                $.ajax({
                    url: '<?php echo $this->webroot; ?>learns/ajaxEditAnnouncement',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        id: announcementid,
                        announcement: text
                     
                    },
                    success: function (data) {
                      if(data==1){
                        var URL = window.location.href;
                        window.location.href = URL;
                      }
                    }
                });
              }
            }
            if(e.which == 27) {
              self.hide();
              selfe.closest('.annoucement_comment_div').show();
            }
            if(e.which == 13 && e.shiftKey) {
              self.val(self.val() + "\n");// use the right id here
              return true;
            }
      });

      $('.edit_added_comment').on('keypress',function(e){
          if(e.which === 13 && !e.shiftKey) {
            e.preventDefault();
          }
      });

    });

</script>

<script>
  $( function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      heightStyle: "content"
    });
  } );

  $('.video_preview').click(function(){
       var self = $(this);
       var lecture_id = self.data('id');
       $.ajax({
           url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideo',
           type: 'POST',
           dataType: 'json',
           data: {lecture_id:lecture_id},
           success: function (data) {
               if(data.Ack==1){
                   $(".modal-body").html('');
                   $(".modal-body").html(data.html);
                   /*$('#videoModal').modal('show');*/
               }
           }
       });
   });

    $('.lecture_resours').click(function(){

        var self = $(this);
        var lecture_id = self.data('id');
        var userid = $('#loginid').val();
        $.ajax({
          url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideostatus',
            type: 'POST',
            dataType: 'json',
            data: {lecture_id:lecture_id,userId:userid},
            success: function (data) {
               console.log(data);
            }
        });
    });
  </script>

<script>
    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    

            function submitComment() {
            //alert(1);
            var title=$('#announcement_title').val();
            var description=$('#announcement_description').val();
           //alert(title);
           //alert(description);
           
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>learns/addAnnouncement",
                    data: {
                        title: title,
                        announcement: description,
                        post_id:<?php echo $post_id; ?>,
                        user_id:<?php echo $userid; ?>
                    },
                    success: function (html) {
                        console.log(html);
                        $('.announcement_content').append(html);
                        $('#announcement_title').val('');
                        $('#announcement_description').val('');
                    }
                });
            }
    
</script>
