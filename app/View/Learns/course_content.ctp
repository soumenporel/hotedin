<style type="text/css">
	.current{
		background: #1376d7;
    	color: #ffffff;
	}
  .header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

  .top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important;}
</style>
<?php //echo '<pre>'; print_r($readList); echo '</pre>';?>
<div class="ud-angular-loaded" data-module-id="course-taking-v4" data-module-name="ng/apps/course-taking-v4/app">
     <?php //echo '<pre>'; print_r($userdetails); echo '</pre>';?>
   <!-- uiView:  -->
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom">


<?php echo $this->element('course_midmenu'); ?>
                <?php
                if($userid>0){
                                                                
$isSubscribe = $this->requestAction('/users/isSubscribe/');
}
else
{
 $isSubscribe=0; 
}
?>     
<!-- uiView:  -->

                 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                 <link rel="stylesheet" href="/resources/demos/style.css">

                              
              <div class="container" style="padding-top:25px; padding-bottom:40px;">
               	<div class="row" style="margin: auto;">
               	 	<div class="col-md-12">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                      <?php 
                      if($userid>0){
                        $serial_no = 1;
                        $count = count($lessons);
                        foreach ($lessons as $key => $lesson) { ?>
                          <div class="panel panel-default custom-panel">
            								<div class="panel-heading" role="tab" id="headingOne">
            									<div class="panel-title">
            										<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $serial_no;?>" aria-expanded="true" aria-controls="collapse<?php echo $serial_no;?>">
            											<span class="sec-name pull-left">Section Name</span>
            											<span class="item-no pull-right"><?php echo $serial_no; ?>/<?php echo $count; ?></span>
            											<div class="clearfix"></div>
            											<h4> <?php echo $lesson['Lesson']['title'];?> </h4>
            										</a>
            									</div>
            								</div>
            								<div id="collapse<?php echo $serial_no;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            									<div class="panel-body">
            										<ul class="panel-listing">
            											<?php 
                                    $lecture_no = 1;
                                    
                                    foreach ($lesson['Lecture'] as $key => $value) {  
                                        
                                    if($value['media']==''){
                                    if($value['type']==0){
                                  ?>
                                      <li>
                                        <a href="<?php echo $this->webroot."learns/description/".$postDetails['Post']['slug']."/".$value['id'];?>">
                                          <span class="vid_icon"><i class="fa fa-file" ></i></span>
                                          <span class="fx">
                                            <span class="no"><?php echo $lecture_no; ?>.</span>
                                            <span><?php echo $value['title'];?></span>
                                          </span>
                                          <div class="right-part">
                                            <!-- <span class="time">3:23</span> -->
                                            <div class="content-box">
                                              <span class="checkboxFour">
                                                  <input type="checkbox"  name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3" <?php if( in_array($value['id'],$readList) ){ echo 'checked'; } ?>>
                                                  <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                                </span>
                                            </div>
                                          </div>
                                        </a>
                                      </li>
                                  <?php } 
                                    if($value['type']==1&&$userid>0){
                                        
                              if($_SESSION['usertype']==2&&$_SESSION['membership_plan_id']==6&&$isSubscribe==1){
                                        $isquiz=0;
                                      $quizMarks=0;
                                      $quizStatus = $this->requestAction('/learns/isQuiz/'.$value['id'].'/'.$userid);  
                                      if(!empty($quizStatus))
                                      {
                                        $isquiz=1;
                                          $quizMarks=$quizStatus['QuizScore']['percentage']; 
                                      }
                                  ?>
                                      <li>
                                          <?php 
                                          if($isquiz==1)
                                          {
                                              if($quizMarks>80)
                                              {
                                                  $quizLink=$this->webroot."learns/quiz_result/".$value['id'];
                                              }
                                              else
                                              {
                                                  $quizLink=$this->webroot."learns/previewquiz1/".$postDetails['Post']['slug']."/".$value['id'];
                                              }
                                          }
                                          else
                                          {
                                              $quizLink=$this->webroot."learns/previewquiz/".$postDetails['Post']['slug']."/".$value['id'];
                                          }
                                          ?>
                                          <a href="<?php echo $quizLink;?>" target="_blank">
                                          <span class="vid_icon"><i class="fa fa-question-circle-o" ></i></span>
                                          <span class="fx">
                                            <span class="no"><?php echo $lecture_no; ?>.</span>
                                            <span><?php echo $value['title'];?></span>
                                          </span>
                                          <div class="right-part">
                                            <!-- <span class="time">3:23</span> -->
                                            <div class="content-box">
                                              <span class="checkboxFour">
                                                  <input type="checkbox"  name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3" <?php if( in_array($value['id'],$readList) ){ echo 'checked'; } ?>>
                                                  <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                                </span>
                                            </div>
                                          </div>
                                        </a>
                                      </li>
                                  <?php 
                              } } } 
                                  ?>
                                  <?php 
                                        if($value['media']!=''){
                                        $file = $value['media'];
                                                $ext = pathinfo($file, PATHINFO_EXTENSION);
                                                $imageExt = array('jpeg' , 'jpg');
                                                $videoExt = array('mp4','flv','mkv');
                                                $pdfExt = array('pdf');
                                                $docExt = array('doc','docx','ppt');
                                        if(in_array(strtolower($ext), $videoExt)){ 
                                          $file_name = trim($value['media'],$ext);
                                          
                                          //for free .......................................
                                          if($value['status']==1){
                                  ?>
                                  <li>
                                    <a href="<?php echo $this->webroot.'learns/video_html/'.$postDetails['Post']['slug'].'/'.$value['id'];?>" data-id="<?php echo $value['id'];?>" >
                                            <span class="vid_icon"><i class="fa fa-play-circle-o" ></i></span>
                                            <span class="fx">
                                                    <span class="no"><?php echo $lecture_no; ?>.</span>
                                                    <span><?php echo $value['title'];?></span>
                                            </span>
                                            <div class="right-part">
                                                    <!-- <span class="time">3:23</span> -->
                                                    <div class="content-box">
                                                            <span class="checkboxFour">
                                                                    <input type="checkbox"  name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3" <?php if( in_array($value['id'],$readList) ){ echo 'checked'; } ?> >
                                                                    <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                                            </span>
                                                    </div>
                                            </div>
                                    </a>
                            </li>
                                          <?php  
                                          
                                           } 
                                          elseif($_SESSION['membership_plan_id']==6||$_SESSION['membership_plan_id']==5||$_SESSION['membership_plan_id']==4||$_SESSION['membership_plan_id']==3)
                                          {
                                              if($isSubscribe==1){
                                           ?>
                                           <li>
                                    <a href="<?php echo $this->webroot.'learns/video_html/'.$postDetails['Post']['slug'].'/'.$value['id'];?>" data-id="<?php echo $value['id'];?>" >
                                            <span class="vid_icon"><i class="fa fa-play-circle-o" ></i></span>
                                            <span class="fx">
                                                    <span class="no"><?php echo $lecture_no; ?>.</span>
                                                    <span><?php echo $value['title'];?></span>
                                            </span>
                                            <div class="right-part">
                                                    <!-- <span class="time">3:23</span> -->
                                                    <div class="content-box">
                                                            <span class="checkboxFour">
                                                                    <input type="checkbox"  name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3" <?php if( in_array($value['id'],$readList) ){ echo 'checked'; } ?> >
                                                                    <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                                            </span>
                                                    </div>
                                            </div>
                                    </a>
                            </li>
                                        <?php
                                              }
                                          }
                                          ?>
                                          <?php  }
                                          if(in_array(strtolower($ext), $pdfExt)){ ?>

                                  <li>
                                    <a href="<?php echo $this->webroot."learns/read_pdf/".$postDetails['Post']['slug']."/".$value['id'];?>">
                                      <span class="vid_icon"><i class="fa fa-file-pdf-o" ></i></span>
                                      <span class="fx">
                                        <span class="no"><?php echo $lecture_no; ?>.</span>
                                        <span><?php echo $value['title'];?></span>
                                      </span>
                                      <div class="right-part">
                                        <!-- <span class="time">3:23</span> -->
                                        <div class="content-box">
                                          <span class="checkboxFour">
                                              <input type="checkbox"  name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3" <?php if( in_array($value['id'],$readList) ){ echo 'checked'; } ?> >
                                              <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                            </span>
                                        </div>
                                      </div>
                                    </a>
                                  </li>        
                                          
                                  <?php }
                                          if(in_array(strtolower($ext), $imageExt)){ ?>

                                  <li>
                                    <a href="<?php echo $this->webroot."learns/view_image/".$postDetails['Post']['slug']."/".$value['id'];?>">
                                      <span class="vid_icon"><i class="fa fa-picture-o" ></i></span>
                                      <span class="fx">
                                        <span class="no"><?php echo $lecture_no; ?>.</span>
                                        <span><?php echo $value['title'];?></span>
                                      </span>
                                      <div class="right-part">
                                        <!-- <span class="time">3:23</span> -->
                                        <div class="content-box">
                                          <span class="checkboxFour">
                                              <input type="checkbox"  name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3" <?php if( in_array($value['id'],$readList) ){ echo 'checked'; } ?> >
                                              <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                            </span>
                                        </div>
                                      </div>
                                    </a>
                                  </li>        

                                  <?php }
                                          if(in_array(strtolower($ext), $docExt)){ ?>

                                  <li>
                                    <a href="<?php echo $this->webroot."learns/read_doc/".$postDetails['Post']['slug']."/".$value['id'];?>">
                                      <span class="vid_icon"><i class="fa fa-file" ></i></span>
                                      <span class="fx">
                                        <span class="no"><?php echo $lecture_no; ?>.</span>
                                        <span><?php echo $value['title'];?></span>
                                      </span>
                                      <div class="right-part">
                                        <!-- <span class="time">3:23</span> -->
                                        <div class="content-box">
                                          <span class="checkboxFour">
                                              <input type="checkbox" checked="" name="lecturecheckbox<?php echo $value['id'];?>" id="checkboxSixInput<?php echo $value['id'];?>" value="3">
                                              <label for="checkboxSixInput<?php echo $value['id'];?>"></label>
                                            </span>
                                        </div>
                                      </div>
                                    </a>
                                  </li>        

                                  <?php 
                                        } }  
                                        
                                        //downloadable files...............................................
                                       
                                         if($value['DownloadableFile']!=''){
                                             foreach ($value['DownloadableFile'] as $key1 => $value1) { //print_r($value['DownloadableFile']);?>
                                  <li>
                                      <div class="col-md-12"> 
                                          <div class="col-md-6"><?php echo $value1["downloadable_file"];?></div> 
                                       
                                       <div class="col-md-6"> 
                                           <a class="buttonDownload" href="<?php echo $this->webroot;?>lecture_asset/<?php echo $value1["downloadable_file"];?>" download> Download </a>
                                       </div>
                                       </div>
                                        </li>
                                                     <?php } 
                                         }
                                         
                                          //downloadable files...............................................
                                       
                                         if($value['ExternalLink']!=''){
                                             foreach ($value['ExternalLink'] as $key2 => $value2) { //print_r($value['DownloadableFile']);?>
                                  <li>
                                      <div class="col-md-12"> 
                                          <a class="text-center externalLink" href="<?php echo $value2["external_link"];?>" target="_blank"> <?php echo $value2["external_title"];?> </a>
                                       </div>
                                        </li>
                                                     <?php } 
                                         }
                                        $lecture_no = $lecture_no+1; 
                                        } 
                                  ?>                
                                  
                                 
            										</ul>
            									</div>
            								</div>
            							</div>
                      <?php $serial_no = $serial_no+1; } }?>  
        							<!-- <div class="panel panel-default custom-panel">
        								<div class="panel-heading" role="tab" id="headingOne">
        									<div class="panel-title">
        										<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        											<span class="sec-name pull-left">Section Name</span>
        											<span class="item-no pull-right">0/2</span>
        											<div class="clearfix"></div>
        											<h4>Java</h4>
        										</a>
        									</div>
        								</div>
        								<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        									<div class="panel-body">
        										<ul class="panel-listing">
        											<li>
        												<a href="">
        													<span class="vid_icon"><i class="fa fa-play-circle-o"></i></span>
        													<span class="fx">
        														<span class="no">1.</span>
        														<span>Introduction</span>
        													</span>
        													<div class="right-part">
        														<span class="time">3:23</span>
        														<div class="content-box">
        															<span class="checkboxFour">
        													  			<input type="checkbox" checked="" name="lecturecheckbox1" id="checkboxSixInput1" value="3">
        														  		<label for="checkboxSixInput1"></label>
        									  						</span>
        														</div>
        													</div>
        												</a>
        											</li>
        											<li>
        												<a href="">
        													<span class="vid_icon"><i class="fa fa-play-circle-o"></i></span>
        													<span class="fx">
        														<span class="no">1.</span>
        														<span>Introduction</span>
        													</span>
        													<div class="right-part">
        														<span class="time">3:23</span>
        														<div class="content-box">
        															<span class="checkboxFour">
        													  			<input type="checkbox" checked="" name="lecturecheckbox1" id="checkboxSixInput1" value="3">
        														  		<label for="checkboxSixInput1"></label>
        									  						</span>
        														</div>
        													</div>
        												</a>
        											</li>
        										</ul>
        									</div>
        								</div>
        							</div> -->
                    </div>
               	 		
               	 		
               	 	</div>
               	 </div>
               </div>


            </div>
            <span open-modal="" auto-open="openSurvey" enable-loader="true" on-close="onGoalsSurveyClose()" has-header="true" backdrop="static" href=""> <span system-message="" message-id="student_goals_survey" object-type="course" object-id="course.id"> </span> </span> 
         </div>
      </ui-view>
   </div>
   <div class="loading-screen" style="">
      <span class="preloader--center preloader--lg icon-spin">
      <i class="udi udi-circle-loader text-white"></i>
      </span>
   </div>
</div>

<!-- Video Modal -->
<div id="videoModal" class="modal fade" role="dialog modal-lg">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Video Preview</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      heightStyle: "content"
    });
  } );

  $('.video_preview').click(function(){
       var self = $(this);
       var lecture_id = self.data('id');
       $.ajax({
           url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideo',
           type: 'POST',
           dataType: 'json',
           data: {lecture_id:lecture_id},
           success: function (data) {
               if(data.Ack==1){
                   $(".modal-body").html('');
                   $(".modal-body").html(data.html);
                   /*$('#videoModal').modal('show');*/
               }
           }
       });
   });

  	$('.lecture_resours').click(function(){
  	
       	var self = $(this);
       	var lecture_id = self.data('id');
       	var userid = $('#loginid').val();
       	$.ajax({
        	url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideostatus',
           	type: 'POST',
           	dataType: 'json',
           	data: {lecture_id:lecture_id,userId:userid},
           	success: function (data) {
               console.log(data);
           	}
       	});
   	});
  </script>

<script>
    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course'); 
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course'); 
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course'); 
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course'); 
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
</script>


<head>
  <link href="http://vjs.zencdn.net/5.8.8/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 -->
  <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>  

<!-- <video id="my-video" class="video-js" controls preload="auto" width="640" height="264"
  poster="MY_VIDEO_POSTER.jpg" data-setup='{"techOrder": ["html5"]}'>
    <source src="/learnfly/img/post_video/927018297_54515485.mp4" type="video/mp4">
    <source src="/learnfly/img/post_video/927018297_54515485.ogg" type="video/ogg">
    <p class="vjs-no-js">
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
  </video>
 
  <script src="http://vjs.zencdn.net/5.8.8/video.js"></script> -->

