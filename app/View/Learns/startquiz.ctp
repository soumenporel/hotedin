<section class="quize-view">
    <div class="quize-view-header">
        <div class="pull-left">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <a href="javascript:void(0)"> <p><h3>Quiz : </h3> </p> <h3>Quiz List</h3> </a>
                        <?php $i = 1; foreach ($quizList as $list) { ?>
                        <?php if ($id != $list['Quiz']['id']) { ?>
                        <a href="<?php echo $this->webroot . 'users/previewquiz' . $list['Quiz']['id']; ?>"> 
                            <i class="fa fa-location-arrow" aria-hidden="true"></i> <?php echo $i . ". " . $list['Quiz']['title'] ?> 
                            <i class="fa fa-circle-thin icon-right" aria-hidden="true"></i> 
                        </a>
                        <?php } else { ?>
                        <a href="javascript:void(0)"> 
                            <i class="fa fa-location-arrow" aria-hidden="true"></i> <?php echo $i . ". " . $list['Quiz']['title'] ?> 
                            <!-- <i class="fa fa-circle-thin icon-right" aria-hidden="true"></i> -->
                            <i class="fa fa-check-circle icon-right" aria-hidden="true"></i>
                        </a>                           
                    <?php } ?>
                    <?php } ?>
            </div>
            <span style="font-size:20px;cursor:pointer" onclick="openNav()">&#9776;</span>
        </div>
        <div class="dashboard-button">
            <?php $url = $this->webroot . 'users/myquiz'; ?>
            <button class="btn btn-default" onclick="window.location = '<?php echo $url; ?>';" type="submit">Go To Quiz</button>
        </div>
        <div class="clearfix"></div>
    </div>
    
<?php $cnt = 1; foreach($questionlist as $qlist){ ?>    
    <div class="quize-view-body" id="questionSl<?php echo $cnt; ?>" style="display: <?php echo $cnt != 1? 'none':'block'?>" >
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-3">
                <p> <?php echo $qlist['Question']['question']?> </p>
                <div class="quize-view-body-text">                    
                    <?php $ansdt = 1; foreach($qlist['Answer'] as $ans){ ?>                   
                    <div id="<?php echo $cnt;?>disable<?php echo $ansdt ?>" class="try-after-box" onclick="proceedToWay(this,'<?php echo $cnt; ?>','<?php echo $ansdt; ?>','<?php echo $ans['is_answer']; ?>')">
                        <p> <?php echo $ans['answer']; ?> </p>
                    </div>
                    
                    <div id="<?php echo $cnt;?>enable<?php echo $ansdt ?>" class="try-again-box" style="display : none" >
                        <div style="float: left" id="<?php echo $cnt;?>answerSec<?php echo $ansdt;?>" ></div> <p style="float: left" > <?php echo $ans['answer']; ?> </p>
                        <?php if($ans['reasion'] != ""){ ?>
                        <p> Reasion Of Answer : <?php echo $ans['answer']; ?> </p>
                        <?php } ?>
                    </div>                    
                    <?php $ansdt++; } ?>
                </div>
            </div>
            <div>
            </div>
        </div>
    </div>
</div>
<?php $cnt ++; } ?>
    
  
    
    
    
    
    
    
    
    
    
<div class="clearfix"></div>
<div class="quize-view-footer">
    <div class="pull-right">
        <div class="footer-question">Question <span id="pageSlNo">1</span> of <?php echo count($questionlist); ?> </div>
        <div class="footer-question" id="pageRedirectionId"><button type="button" onclick="goNextTab('2')" class="btn btn-info"> Next </button></div>
        
            
        <?php $urlrs = $this->webroot . 'users/quizresult'; ?>
        <div style="display:none" id="nexturl"><?php echo $urlrs;?></div>
        
        <!--<div style="display:none" class="footer-question"><button type="button" onclick="window.location = '<?php echo $urlrs; ?>';" class="btn btn-info"> See Result </button></div>-->
        <div class="footer-right">
            <span class="footer-icon"><i class="fa fa-cog" aria-hidden="true"></i></span>
            <span class="pull-right"><i class="fa fa-arrows-alt" aria-hidden="true"></i></span>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<input type="hidden" id="score" value="0" >
</section>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "450px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
    
    function proceedToWay(thval,qcnt,anscnt,ans){
        //alert(qcnt); alert(anscnt); alert(ans);
        $("#"+qcnt+"disable" + anscnt).hide();
        $("#"+qcnt+"enable" + anscnt).show();
        $("#"+qcnt+"reasion"+anscnt).show();
        if( ans == 1){
            var rs = $('#score').val();
            var finrs = parseInt(rs) + parseInt(ans);
            $('#score').val(finrs);
            $("#" + qcnt + "answerSec" + anscnt).html('<img src="<?php echo $this->Html->url("/") ?>images/rightsign.jpg" width="25" height="25"  alt=""/>');
        } else {
            $("#" + qcnt + "answerSec" + anscnt).html('<img src="<?php echo $this->Html->url("/") ?>images/cross.png" width="25" height="25"  alt=""/>');
        }
    }
    
    
    
    function goNextTab(pageval){
    
        var pageCount = <?php echo count($questionlist); ?>; 
        var url = $("#nexturl").html();
        //alert(url); alert(pageCount); alert(pageval);
        //alert(pageCount); alert(pageval);
        postpageval = parseInt(pageval) + 1;
        if(pageCount == pageval){
            //$("#pageRedirectionId").html('<button type="button" onclick="window.location = \'' + url + '\';" class="btn btn-info"> See Result </button>');
            $("#pageRedirectionId").html('');
        } else {
            $("#pageRedirectionId").html('<button type="button" onclick="goNextTab(\'' + postpageval + '\')" class="btn btn-info"> Next </button>');
        }
        $("#pageSlNo").html(pageval);
        
        
        prepageval = parseInt(pageval) - 1;
        $("#questionSl" + prepageval).hide();
        $("#questionSl" + pageval).show();
        
        //if(pageval <  )

    }    
    
    
</script>