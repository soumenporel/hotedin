<?php $vv = $postDetails['Post'];
//pr($announcements);
?>

<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

.top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important; background: #fff; }
</style>
<link href="<?php echo $this->webroot; ?>css/course_dashsboard.css" rel="stylesheet" type="text/css">
<div class="ud-angular-loaded" data-module-id="course-taking-v4" data-module-name="ng/apps/course-taking-v4/app">
   <!-- uiView:  -->
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom" style="background: #f7f7f7;">
              <?php echo $this->element('course_midmenu'); ?>



<div class="bottom__content container" data-ui-view="">
	<div style="margin: auto; padding-bottom:40px; width: 85%;   " class="content__overview mt-5">
            <?php //print_r($course_discussions); ?>
      <h4 class="mt-3 font-20 mb-3" ><span>Discussions • <?php echo count($course_discussions); ?> </span></h4>
      <!-- ngRepeat: instructor in course.visible_instructors -->
      <div class="discussion_content">
      <?php foreach($course_discussions as $discussion) { ?>
      <div class="row course-section mt-0">
          <div class="col-1 section-title"> 
              
          <img src="<?php if (isset($discussion['User']['user_image']) && $discussion['User']['user_image'] != '') { ?> <?php echo $this->webroot; ?>user_images/<?php echo $discussion['User']['user_image'];} else {echo $this->webroot;?>img/profile_img.jpg<?php } ?>" class="instructor-avatar"> </div>
         <div class="col-11">
            <div class="instructor-title mb-2"> <a href="#" class="font-15" style="color: #d20000!important;"><?php echo $discussion['User']['first_name']." ".$discussion['User']['last_name']; ?></a> 
                <span class="time-gray-text">
                    <?php echo date('F j,Y g:ia',strtotime($discussion['CourseDiscussion']['post_time'])); ?>
                </span> </div>

            <!-- end ngIf: features.course_landing_page.instructor_bio.social_links -->
            <div class="instructor-description no-top-margine">
               <p class="font-14" style="margin-bottom: 0!important;"><?php echo $discussion['CourseDiscussion']['description']; ?></p>
<!--               <p class="add-on-star"><i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star-half"></i></p>-->
            </div>
         </div>
		<div class="separator-b"></div>
		
      </div>
      <?php } ?>
      </div>
      <div class="row post-comment">
      <h5 class="reply">Post Comment</h5>
      	<div class="col-12">
      		<form class="post-text-area"><textarea class="form-control" rows="2" id='comment'></textarea></form>      		
      		<div class="post-btn">
<!--      			<button class="btn btn-default" >Cancel</button>-->
                <button class="btn btn-danger" onclick="submitComment();">Submit</button>
      		</div>      		
      	</div>
      </div>
      <!-- end ngRepeat: instructor in course.visible_instructors -->
   </div>
</div>

<script type="text/javascript">

            function submitComment() {
                var comment = $('#comment').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>learns/addDiscussion",
                    data: {
                        description: comment,
                        post_id:<?php echo $post_id; ?>,
                        user_id:<?php echo $userid; ?>
                    },
                    success: function (html) {
                        $('.discussion_content').append(html);
                        $('#comment').val('');
                    }
                });
            }
        </script>