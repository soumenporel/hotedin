<?php $vv = $postDetails['Post'];
//pr($announcements);
?>

<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

.top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important; background: #fff; }
</style>
<link href="<?php echo $this->webroot; ?>css/course_dashsboard.css" rel="stylesheet" type="text/css">
<div class="ud-angular-loaded" data-module-id="course-taking-v4" data-module-name="ng/apps/course-taking-v4/app">
   <!-- uiView:  -->
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom" style="background: #f7f7f7;">
              <nav class="navbar navbar-toggleable-md navbar-light bg-faded my-navBar" style=" min-height: 60px; ">

                  <div class="container">
                     <div class="" id="course-taking-bottom-navbar" >
                        <ul class="navbar-nav mr-auto">
                              <li ui-sref-active="active" class="menues nav-item" >
                                   <a ui-sref="base.dashboard.overview" ng-click="navbarCollapsed = true" translate="" href="<?php echo $this->webroot.'learns/overview/'.$postDetails['Post']['slug']; ?>" class="nav-link active">
                                        <span>Overview</span>
                                   </a>
                              </li>
                              <li ui-sref-active="active" class="menues nav-item" >
                                   <a ui-sref="base.dashboard.content" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-content-tab" href="<?php echo $this->webroot;?>learns/course_content/<?php echo $postDetails['Post']['slug'];?>"  class="nav-link">
                                        <span>Course Content</span>
                                   </a>
                              </li>
                              <li ng-class="{active: ('base.dashboard.questions' | includedByState) || ('base.dashboard.question-details' | includedByState)}" class="menues nav-item" >
                                   <a ui-sref="base.dashboard.questions" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-question-tab" href="<?php echo $this->webroot.'learns/questions/'.$postDetails['Post']['slug']; ?>"  class="nav-link">
                                        <span>Q&amp;A</span>
                                   </a>
                              </li>
                           <li ui-sref-active="active" class="nav-item">
                              <a ui-sref="base.dashboard.bookmarks" ng-click="navbarCollapsed = true" translate="" course-taking-tracking="" tracking-category="dashboard" tracking-action="visit-bookmark-tab" href="<?php echo $this->webroot;?>learns/bookmarks/<?php echo $postDetails['Post']['slug'];?>"  class="nav-link">
                                   <span>Bookmarks</span>
                              </a>
                           </li>
                           <li ui-sref-active="active" class="nav-item">
                              <a ui-sref="base.dashboard.announcements" ng-click="navbarCollapsed = true" translate="" href="<?php echo $this->webroot;?>learns/announcement/<?php echo $postDetails['Post']['slug'];?>"  class="nav-link">
                                   <span>Announcements</span>
                              </a>
                           </li>
                           <li ui-sref-active="active" class="dropdown nav-item">
                              <a href="javascript:void(0)" id="dLabel" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span>Options</span>
                              </a>
                              <!-- ngInclude: optionsDropdownTemplateUrl -->
                              <div class="dropdown-menu" role="menu" aria-labelledby="dLabel" ng-click="$event.stopPropagation();" ng-include="optionsDropdownTemplateUrl" style="width:250px;">
                                    <!-- ngIf: !course.favorite_time -->
                                    <!-- <input type="hidden" id="hidden_user_id" value='<?php echo $userid?>'></input> -->
                                    <a role="menuitem" tabindex="-1" ng-click="course.favoriteCourse()" ng-if="!course.favorite_time" translate="" class="dropdown-item font-14">
                                        <span id="<?php echo $favoritexist ? 'unfavorite_this_course' : 'favorite_this_course'; ?>" data-id="<?php echo $postDetails['Post']['id']; ?>" ><?php echo $favoritexist ? 'Unfavorite this course' : 'Favorite this course'; ?></span>
                                    </a>
                                    <!-- end ngIf: !course.favorite_time -->
                                    <!-- ngIf: course.favorite_time -->

                                    <!-- ngIf: !course.archive_time -->
                                    <a role="menuitem" tabindex="-1" ng-click="course.archiveCourse()" ng-if="!course.archive_time" translate=""  class="dropdown-item font-14">
                                        <span id="<?php echo $archivexist ? 'unarchive_this_course' : 'archive_this_course'; ?>" data-id="<?php echo $postDetails['Post']['id']; ?>" ><?php echo $archivexist ? 'Unarchive this course' : 'Archive this course'; ?></span>
                                   </a>
                                   <!-- end ngIf: !course.archive_time -->
                                   <!-- ngIf: course.archive_time -->
                                 <div class="dropdown-divider"></div>
                                   <a role="menuitem" tabindex="-1" ng-click="scrollToInstructor()" translate="" class="dropdown-item font-14">
                                        <span>Instructor profile</span>
                                   </a>
                                   <a role="menuitem" tabindex="-1" ng-show="isGiftCourseEnabled &amp;&amp; course.is_paid &amp;&amp; !course.is_private" ng-href="https://www.udemy.com/gift/learn-wordpress-in-57-minutes/" target="_blank" translate="" href="https://www.udemy.com/gift/learn-wordpress-in-57-minutes/" class="ng-hide" class="dropdown-item font-14">
                                        <span>Gift this course</span>
                                   </a>
                                   <a role="menuitem" tabindex="-1" ng-href="https://www.udemy.com/support/" target="_blank" translate="" href="https://www.udemy.com/support/" class="dropdown-item font-14">
                                        <span>Support</span>
                                   </a>
                                 <!-- ngIf: reportAbusePopup.enabled -->
                                    <a role="menuitem" tabindex="-1" ng-click="reportAbusePopup.open=true" class="dropdown-item font-14">
                                       <i class="udi udi-flag mr5"></i>
                                       <span translate="">
                                        <span>Report abuse</span>
                                       </span>
                                       <popup open="reportAbusePopup.open" ng-href="/feedback/report?related_object_type=course&amp;related_object_id=552598" style="display: block;" href="/feedback/report?related_object_type=course&amp;related_object_id=552598"> </popup>
                                    </a>
                                 <!-- end ngIf: reportAbusePopup.enabled -->
                                 <div class="dropdown-divider"></div>
                                 <!-- ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
                                 <div class="dropdown-item font-14" ng-repeat="(key, value) in course.notification_settings track by $id(key)">
                                   <a role="menuitem" tabindex="-1" class="email-settings ng-hide" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> <span class="checkbox1"> <label> <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" checked="checked" type="checkbox"> <span class="checkbox-label email-settings__title">  </span> </label> </span> </a> </div>
                                 <!-- end ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
                                 <div class="dropdown-item font-14" ng-repeat="(key, value) in course.notification_settings track by $id(key)"> <a role="menuitem" tabindex="-1" class="email-settings" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> <span class="checkbox1"> <label> <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" type="checkbox"> <span class="checkbox-label email-settings__title"> New announcement emails </span> </label> </span> </a> </div>
                                 <!-- end ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
                                 <div ng-repeat="(key, value) in course.notification_settings track by $id(key)" class="dropdown-item font-14"> <a role="menuitem" tabindex="-1" class="email-settings" ng-click="updateCourseSetting()" course-id="course.id" disabled="disabled" setting-key="key" setting-value="value" ng-hide="key == 'disableAllEmails'"> <span class="checkbox1"> <label> <input autocomplete="off" ng-checked="settingValue" ng-disabled="disabled" disabled="disabled" type="checkbox"> <span class="checkbox-label email-settings__title"> Promotional emails </span> </label> </span> </a> </div>
                                 <!-- end ngRepeat: (key, value) in course.notification_settings track by $id(key) -->
                                 <div role="presentation" class="dropdown-divider"></div>
                                 <div class="dropdown-item font-14">
                                    <!-- ngIf: course.canBeUnenrolled --> <!-- ngIf: course.refundFeatureEnabled -->
                                    <div ng-if="course.refundFeatureEnabled">
                                       <a target="_blank" ng-href="" ng-show="course.was_purchased_by_student &amp;&amp; !course.canBeUnenrolled" ng-class="{'unsubscribe': course.is_refundable, 'cannot-refund': !course.is_refundable}" class="pl10 pb10 ng-hide cannot-refund" translate="" style="padding-left:0;"><span>Request a refund</span></a>
                                       <div ng-hide="course.is_refundable" class="cannot-refund-info">
                                          <div ng-show="course.was_paid_by_student" translate="" class="ng-hide"><span>This course was purchased outside the 30-day refund policy and cannot be refunded.</span></div>
                                          <div ng-show="!course.was_paid_by_student" translate="" class=""><span>This course was a free enrollment and cannot be refunded.</span></div>
                                       </div>
                                    </div>
                                    <!-- end ngIf: course.refundFeatureEnabled -->
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </nav>



<div class="bottom__content container" data-ui-view="">
	<div style="margin: auto; padding-bottom:40px; width: 85%;   " class="content__overview mt-5">
      <h4 class="mt-3 font-20 mb-3" translate-plural="Instructors" translate-n="course.visible_instructors.length" translate="" id="course-instructor"><span>Comments • 844 </span></h4>
      <!-- ngRepeat: instructor in course.visible_instructors -->
      <div ng-repeat="instructor in course.visible_instructors" class="row course-section mt-0">
         <div class="col-1 section-title"> <img src="/team4/studilmu/user_images/59c0e4669e726.jpg " ng-src="https://udemy-images.udemy.com/user/200_H/578322_54b9_3.jpg" class="instructor-avatar"> </div>
         <div class="col-11">
            <div class="instructor-title mb-2"> <a href="/Users/user_profile/165" class="font-15" style="color: #d20000!important;">Ritwik patra</a> <span class="time-gray-text">1 month ago</span> </div>

            <!-- end ngIf: features.course_landing_page.instructor_bio.social_links -->
            <div class="instructor-description no-top-margine">
               <p class="font-14" style="margin-bottom: 0!important;">Where is AB????﻿</p>
               <p class="add-on-star"><i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star-half"></i></p>
            </div>
         </div>
		<div class="separator-b"></div>
		
      </div>
      <div ng-repeat="instructor in course.visible_instructors" class="row course-section mt-0">
         <div class="col-1 section-title"> <img src="/team4/studilmu/user_images/59c0e4669e726.jpg " ng-src="https://udemy-images.udemy.com/user/200_H/578322_54b9_3.jpg" class="instructor-avatar"> </div>
         <div class="col-11">
            <div class="instructor-title mb-2"> <a href="/Users/user_profile/165" class="font-15" style="color: #d20000!important;">Ritwik patra</a> <span class="time-gray-text">1 month ago</span> </div>

            <!-- end ngIf: features.course_landing_page.instructor_bio.social_links -->
            <div class="instructor-description no-top-margine">
               <p class="font-14" style="margin-bottom: 0!important;">Where is AB????﻿</p>
               <p class="add-on-star"><i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star-half"></i></p>
            </div>
         </div>
		<div class="separator-b"></div>
		
      </div>
	  <div ng-repeat="instructor in course.visible_instructors" class="row course-section mt-0">
         <div class="col-1 section-title"> <img src="/team4/studilmu/user_images/59c0e4669e726.jpg " ng-src="https://udemy-images.udemy.com/user/200_H/578322_54b9_3.jpg" class="instructor-avatar"> </div>
         <div class="col-11">
            <div class="instructor-title mb-2"> <a href="/Users/user_profile/165" class="font-15" style="color: #d20000!important;">Ritwik patra</a> <span class="time-gray-text">1 month ago</span> </div>

            <!-- end ngIf: features.course_landing_page.instructor_bio.social_links -->
            <div class="instructor-description no-top-margine">
               <p class="font-14" style="margin-bottom: 0!important;">Where is AB????﻿</p>
               <p class="add-on-star"><i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star-half"></i></p>
            </div>
         </div>
		<div class="separator-b"></div>
		
      </div>
      <div ng-repeat="instructor in course.visible_instructors" class="row course-section mt-0">
         <div class="col-1 section-title"> <img src="/team4/studilmu/user_images/59c0e4669e726.jpg " ng-src="https://udemy-images.udemy.com/user/200_H/578322_54b9_3.jpg" class="instructor-avatar"> </div>
         <div class="col-11">
            <div class="instructor-title mb-2"> <a href="/Users/user_profile/165" class="font-15" style="color: #d20000!important;">Ritwik patra</a> <span class="time-gray-text">1 month ago</span> </div>

            <!-- end ngIf: features.course_landing_page.instructor_bio.social_links -->
            <div class="instructor-description no-top-margine">
               <p class="font-14" style="margin-bottom: 0!important;">Where is AB????﻿</p>
               <p class="add-on-star"><i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star"></i> <i class="icon ion-android-star-half"></i></p>
            </div>
         </div>		
		
      </div>
      <div class="row post-comment">
      <h5 class="reply">Reply</h5>
      	<div class="col-12">
      		<form class="post-text-area"><textarea class="form-control" rows="2"></textarea></form>      		
      		<div class="post-btn">
      			<button class="btn btn-default" >Cancel</button>
      			<button class="btn btn-danger">Submit</button>
      		</div>      		
      	</div>
      </div>
      <!-- end ngRepeat: instructor in course.visible_instructors -->
   </div>
</div>

