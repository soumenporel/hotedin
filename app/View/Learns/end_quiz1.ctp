<section class="quizResult my-5 py-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="py-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h3>
			</div>
			<div class="col-lg-4 offset-lg-4">
				<div class="resultPart">
					
					<h4 class="mt-4">
						<strong>Total Marks:</strong><span class="pl-3 color-blue">100</span>
					</h4>
					<h4 class="mt-4"><strong>Your Marks Obtain:</strong><span class="pl-3 color-blue"><strong>20</strong></span></h4>
					<h4 class="mt-4"><strong>Your Percentage of Marks:</strong><span class="pl-3 color-blue">20%</span></h4>
					<p class="pt-3"><a class="btn btn-primary certificateButton mt-5 " href="#">Get your Certificate</a></p>
				</div>
			</div>
		</div>
	</div>
</section>