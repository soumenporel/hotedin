<?php $vv = $postDetails['Post'];
//pr($announcements);
?>

<style>
.header{
    position: relative !important;
}

.inner_header {
    background: #0c2440 none repeat scroll 0 0;
}

.top--container{height: auto !important;}
.progress{height: 9px; border-radius:10px;}
.filled-stars{*margin-top:5px !important;}
.ratingPart .rating-xs{font-size:25px !important;}
.panel{border-radius:0 !important; background: #fff; }
</style>
<link href="<?php echo $this->webroot; ?>css/course_dashsboard.css" rel="stylesheet" type="text/css">
<div class="ud-angular-loaded" data-module-id="course-taking-v4" data-module-name="ng/apps/course-taking-v4/app">
   <!-- uiView:  -->
   <div >
      <!-- uiView: undefined -->
      <ui-view class="" style="">
         <div>
            <?php echo $this->element('course_topmenu'); ?>
            <div class="course-dashboard__bottom" style="background: #f7f7f7;">


<?php echo $this->element('course_midmenu'); ?>

<div class="bottom__content container" data-ui-view="">
   <div class="content__overview mt-5" style="margin: auto; padding-bottom:40px; width: 85%;   ">
      <h3 id="recent-activity" translate="" class="mb-3 font-22"><span>Recent Activity</span></h3>
      <div class="row mt30">
         <div class="col-sm-6">
            <div class="panel panel-default">
               <div class="recent-header" translate=""><span>Recent Questions</span></div>
               <div class="fx-c ng-hide" ng-show="questions.loading" style=""> <span class="udi udi-circle-loader udi-large m60"></span> </div>
               <div ng-hide="questions.loading" class="" style="">

                  <?php foreach ($questions as $key => $question) { ?>
                  <!-- ngRepeat: question in questions.data | limitTo:3 -->
                  <div onclick="window.location.href = '<?php echo $this->webroot; ?>learns/answers/<?php echo $postDetails['Post']['slug']; ?>/<?php echo $question['CourseQuestion']['id']; ?>'" ng-show="questions.data.length" ng-repeat="question in questions.data | limitTo:3" class="fx-lc recent-list" style="">
                     <img src="<?php if (isset($question['User']['user_image']) && $question['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                       echo $question['User']['user_image'];
                      } else {
                      echo $this->webroot;
                      ?>img/profile_img.jpg<?php } ?>" width="50" height="50"  class="img-circle">
                     <div class="fx pl10"> <a class="ellipsis text-dark-grey" ><?php echo $question['CourseQuestion']['title']; ?></a> </div>
                  </div>
                  <?php } ?>

                  <div class="recent-footer">
                    <a ui-sref="base.dashboard.questions" course-taking-tracking="" tracking-category="overview" tracking-action="dashboard.tab.recent-questions.visit-question-tab" href="<?php echo $this->webroot;?>learns/questions/<?php echo $postDetails['Post']['slug'];?>">
                      <span ng-show="questions.data.length" translate="" class="" style="">
                        <span>Browse all questions</span>
                      </span>
                    </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-sm-6">
            <div class="panel panel-default">
               <div class="recent-header" translate=""><span>Recent Instructor Announcements</span></div>
               <!-- <div class="fx-lc recent-list" style="">
               		<img src="<?php //echo $this->webroot; ?>images/learnflyImage.jpg">
               		<div class="fx pl10">
               			<a class="text-dark-grey" href="/quickstart-angularjs/learn/v4/announcements?ids=146514"> <div class="ellipsis ng-hide" ng-show="announcement.title">  </div>
               				<div class="ellipsis">Hello Students, First of all I wann</div> </a> </div> </div>

               	<div class="fx-lc recent-list">
               		<img src="<?php //echo $this->webroot; ?>images/learnflyImage.jpg">
               			<div class="fx pl10">
               				<a class="text-dark-grey"> <div class="ellipsis ng-hide">  </div>
               					<div class="ellipsis"> 	The New Year gives you fresh 365 days to play with
               					</div>
               				</a>
               			</div>
               	</div> -->
               <div class="fx-c ng-hide" ng-show="loadingAnnouncements" style=""> <span class="udi udi-circle-loader udi-large m60"></span> </div>
               <div ng-hide="loadingAnnouncements" class="" style="">
                  <!-- ngRepeat: announcement in announcements | limitTo:3 -->
                  <?php foreach ($announcements as $key => $announcement) { ?>
                  <div class="fx-lc recent-list" style="">
                     <img  src="<?php if (isset($announcement['User']['user_image']) && $announcement['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                       echo $announcement['User']['user_image'];
                      } else {
                      echo $this->webroot;
                      ?>img/profile_img.jpg<?php } ?>">
                     <div class="fx pl10">
                        <a class="text-dark-grey" href="javascript:void(0)">
                           <div class="ellipsis ng-hide" ng-show="announcement.title">  </div>
                           <div class="ellipsis" ng-hide="announcement.title" ng-bind-html="announcement.content | trimTags"><?php echo $announcement['Announcement']['announcement']; ?></div>
                        </a>
                     </div>
                  </div>
                  <?php } ?>

                  <div class="recent-footer"> <a ng-class="{'disabled':!announcements.length}" ui-sref="base.dashboard.announcements" href="<?php echo $this->webroot;?>learns/announcement/<?php echo $postDetails['Post']['slug'];?>" class="" style=""> <span translate=""><span>Browse all announcements</span></span> </a> </div>
               </div>
            </div>
         </div>
      </div>
      <div class="separator"></div>
      <h3 translate="" class="mt-4 font-22 mb-0"><span>Note</span></h3>
      <div class="row mt30">
         <div class="col-sm-12">
            <div class="panel panel-default add-on-area"> 
               <?php //print_r($user_courses); ?>
               	  <form>
               	  	<textarea class="form-control" rows="3" id="student_note"><?php echo $user_courses[0]['UserCourse']['note']; ?></textarea>
               	  </form>               		
               	               
            </div>
         </div>
          <div class="col-lg-12 text-center"><button class="btn btn-danger btn-lg add-on-btn-lg" onclick="addNote(<?php echo $user_courses[0]['UserCourse']['id']; ?>);">Save</button></div>
      </div>
      <div class="separator"></div>
      <h3 translate="" class="mt-4 font-22 mb-0"><span>About this course</span></h3>
      <div class="course-headline"> <?php echo $postDetails['Post']['short_summary']; ?> </div>
      <div class="separator"></div>
      <div class="row course-section">
         <div class="col-md-4 section-title" translate=""><span>By the numbers</span></div>
         <ul class="col-md-4">
            <li class="list-item"> <span translate=""><span>Lectures:</span></span> <?php echo $lectureCount; ?> </li>
            <!-- <li class="list-item"> <span translate=""><span>Video:</span></span> 1.5 hours </li> -->
            <li class="list-item"> <span translate=""><span>Skill level:</span></span>
              <?php
                if($postDetails['Post']['skill_level']==1){ echo 'Begineer'; }
                if($postDetails['Post']['skill_level']==2){ echo 'Intermedite'; }
                if($postDetails['Post']['skill_level']==3){ echo 'Expert'; }
              ?>
            </li>
         </ul>
         <ul class="col-md-4">
            <!-- <li class="list-item" translate="" translate-n="course.num_subscribers" translate-plural=" students"><span>47,795 students</span></li> -->
            <li class="list-item"> <span translate=""><span>Languages:</span></span> 
             <?php if($postDetails['Post']['language']=='Es'){ ?>
                              Bahasa Indonesia 
                                <?php } else { ?>
                              English 
                                <?php } ?></li>
            <!-- ngIf: course.has_closed_caption -->
         </ul>
      </div>
      <div class="separator"></div>
      <div class="row course-section">
      <div class="col-3 section-title" translate=""><span>Features</span></div>
         <ul class="col-4">

            <li ng-if="features.course_landing_page.incentives.lifetime_access" translate=""><span>Lifetime access</span></li>

            <li translate=""><span>Available on </span>iOS<span> and </span>Android</li>
         </ul>
         <ul class="col-4">

            <li ng-if="course.has_certificate" translate=""><span>Certificate of Completion</span></li>

         </ul>
      </div>
      <div class="separator"></div>
      <div class="row course-section">
         <div class="col-3 section-title" translate=""><span>Description</span></div>
         <view-more view-more-style="'simple'" class="col-9 course-description">
            <!-- ngIf: filterHtml --> <!-- ngIf: !filterHtml -->
            <div ng-if="!filterHtml" class="text-content short-text">
               <div ng-bind-html="text" class="view-more__text"></div>
               <div class="view-more__content view-more__content--collapsed" ng-class="{'view-more__content--collapsed': showToggle &amp;&amp; isCollapsed}" ng-transclude="" style="">
                  <div ng-bind-html="course.description">
                     <!-- <p><strong>EXCELLENT LEARNING, AWESOME DISCOUNTS, JOIN THIS COURSE &amp; GET MY ANGULARJS MASTERCLASS COURSE for $10.</strong></p> -->
                     <p><em><strong>Reviews</strong></em></p>
                     <?php foreach ($postDetails['Rating'] as $key => $review) { ?>
                     <p><em><?php echo $review['comment'];?></em></p>
                     <?php } ?>
                     <p>
                      <?php echo $postDetails['Post']['post_description'];?>
                     <p>
                  </div>
                  <!-- ngIf: course.prerequisites -->
                  <p class="course-description-title" ng-if="course.prerequisites"> <strong translate=""><span>What are the requirements?</span></strong> </p>
                  <!-- end ngIf: course.prerequisites --> <!-- ngIf: course.prerequisites -->
                  <ul ng-if="course.prerequisites">
                    <?php
                    $variable = json_decode($postDetails['Post']['course_goals']);
                    foreach ($variable->need_to_know as $key => $value) { ?>
                     <!-- ngRepeat: item in course.prerequisites track by $index -->
                     <li ng-repeat="item in course.prerequisites track by $index"><?php echo $value; ?></li>
                     <!-- end ngRepeat: item in course.prerequisites track by $index -->
                    <?php } ?>
                  </ul>
                  <!-- end ngIf: course.prerequisites --> <!-- ngIf: course.objectives -->
                  <p class="course-description-title" ng-if="course.objectives"> <strong translate=""><span>What am I going to get from this course?</span></strong> </p>
                  <!-- end ngIf: course.objectives --> <!-- ngIf: course.objectives -->
                  <ul ng-if="course.objectives">
                     <?php  foreach ($variable->will_be_able as $key => $val) { ?>
                     <!-- ngRepeat: item in course.objectives track by $index -->
                     <li ng-repeat="item in course.objectives track by $index"><?php echo $val; ?></li>
                     <!-- end ngRepeat: item in course.objectives track by $index -->
                     <?php } ?>
                  </ul>
                  <!-- end ngIf: course.objectives --> <!-- ngIf: course.target_audiences -->
                  <p class="course-description-title" ng-if="course.target_audiences"> <strong translate=""><span>What is the target audience?</span></strong> </p>
                  <!-- end ngIf: course.target_audiences --> <!-- ngIf: course.target_audiences -->
                  <ul ng-if="course.target_audiences">
                    <?php foreach ($variable->target_student as $key => $tr_st) { ?>
                     <!-- ngRepeat: item in course.target_audiences track by $index -->
                     <li ng-repeat="item in course.target_audiences track by $index"><?php echo $tr_st; ?></li>
                     <!-- end ngRepeat: item in course.target_audiences track by $index -->
                    <?php } ?>
                  </ul>
                  <!-- end ngIf: course.target_audiences -->
               </div>
            </div>
            <!-- end ngIf: !filterHtml --> <!-- ngIf: showToggle -->
            <!-- <div ng-if="showToggle" class="show-more" style="">
              <a href="#">Show more</a>
            </div> -->
            <!-- end ngIf: showToggle -->
         </view-more>
      </div>
      <div class="separator"></div>
      <h3 id="course-instructor" translate="" translate-n="course.visible_instructors.length" translate-plural="Instructors" class="mt-3 font-22 mb-3"><span>Instructor</span></h3>
      <!-- ngRepeat: instructor in course.visible_instructors -->
      <div class="row course-section mt-0" ng-repeat="instructor in course.visible_instructors">
         <div class="col-3 section-title"> <img class="instructor-avatar" src="<?php if (isset($postDetails['User']['user_image']) && $postDetails['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                echo $postDetails['User']['user_image'];
            } else {
                echo $this->webroot;
                ?>img/profile_img.jpg<?php } ?> "> </div>
         <div class="col-9">
            <div class="instructor-title mb-2"> <a href="/Users/user_profile/<?php echo $postDetails['User']['id']; ?>"><?php echo $postDetails['User']['first_name'].' '.$postDetails['User']['last_name']; ?></a> </div>

            <div class="fx-lc instructor-social">
              <?php if($postDetails['User']['facebook_link']!=null) { ?>
                <a target="_blank" class="iconed-btn" href="<?php echo $postDetails['User']['facebook_link']; ?>"> <i class="fa fa-facebook"></i> </a>
              <?php } ?>
                 <?php if($postDetails['User']['google_link']!=null) { ?>
                <a target="_blank" class="iconed-btn" href="<?php echo $postDetails['User']['google_link']; ?>"> <i class="fa fa-google-plus"></i> </a>
              <?php } ?>
                 <?php if($postDetails['User']['twitter_link']!=null) { ?>
                <a target="_blank" class="iconed-btn" href="<?php echo $postDetails['User']['twitter_link']; ?>"> <i class="fa fa-twitter"></i> </a>
              <?php } ?>
                 <?php if($postDetails['User']['linkedin_link']!=null) { ?>
                <a target="_blank" class="iconed-btn" href="<?php echo $postDetails['User']['linkedin_link']; ?>"> <i class="fa fa-linkedin"></i> </a>
              <?php } ?>
                 <?php if($postDetails['User']['youtube_link']!=null) { ?>
                <a target="_blank" class="iconed-btn" href="<?php echo $postDetails['User']['youtube_link']; ?>"> <i class="fa fa-youtube"></i> </a>
              <?php } ?>
            </div>
            <!-- end ngIf: features.course_landing_page.instructor_bio.social_links -->
            <div class="instructor-description" >
               <p class="font-14"><?php echo $postDetails['User']['biography']; ?></p>
            </div>
         </div>
      </div>
      <!-- end ngRepeat: instructor in course.visible_instructors -->
   </div>
</div>

<script>
    function handle(e){
        if(e.keyCode === 13){
            e.preventDefault(); // Ensure it is only this code that rusn
          alert($(this).val());
        }
    }

    //------ Favorite Unfavorite Section ---------//
    $(document).on('click','#favorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/favorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unfavorite this course');
                        self.attr("id","unfavorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unfavorite_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>favorities/unfavorite_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Favorite this course');
                        self.attr("id","favorite_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });

    //------ Archive Unarchive Section ---------//
    $(document).on('click','#archive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/archive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Unarchive this course');
                        self.attr("id","unarchive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
    $(document).on('click','#unarchive_this_course',function(){
          var self = $(this);
          var post_id = $(this).data('id');
          var user_id = $('#hidden_user_id').val();
          $.ajax({
                url: '<?php echo $this->webroot; ?>archives/unarchive_this_course',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: post_id,
                    userid: user_id
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.html('Archive this course');
                        self.attr("id","archive_this_course");
                    } else {
                        alert(data.res);
                    }
                }
          });
    });
</script>

<script>
  $(".show-more a").each(function() {
    var $link = $(this);
    var $content = $link.parent().prev("div.text-content");

    console.log($link);

    var visibleHeight = $content[0].clientHeight;
    var actualHide = $content[0].scrollHeight - 1;

    console.log(actualHide);
    console.log(visibleHeight);

    if (actualHide > visibleHeight) {
        $link.show();
    } else {
        $link.hide();
    }
});

$(".show-more a").on("click", function() {
    var $link = $(this);
    var $content = $link.parent().prev("div.text-content");
    var linkText = $link.text();

    $content.toggleClass("short-text, full-text");

    $link.text(getShowLinkText(linkText));

    return false;
});

function getShowLinkText(currentText) {
    var newText = '';

    if (currentText.toUpperCase() === "SHOW MORE") {
        newText = "Show less";
    } else {
        newText = "Show more";
    }

    return newText;
}

function addNote(id)
{
        var note=$('#student_note').val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>learns/addstudentNote',
            type: 'POST',
            dataType: 'json',
            data: {
                id: id,
                note: note
            },
            success: function (data) {
                if (data == '1') {
                   $('#ajaxFlashMessage').show();
                                
                                $('#ajaxFlashMessage').html('Note added successfully!');
                               $('#ajaxFlashMessage').removeClass('error');
                               $('#ajaxFlashMessage').addClass('success');
                                setTimeout(function () {
                                    $('#ajaxFlashMessage').hide();
                                    $('#ajaxFlashMessage').html('');
                                }, 3000);
                } else {
                                $('#ajaxFlashMessage').show();
                               
                                $('#ajaxFlashMessage').html('Please try again');
                                $('#ajaxFlashMessage').removeClass('success');
                                $('#ajaxFlashMessage').addClass('error');
                                setTimeout(function () {
                                    $('#ajaxFlashMessage').hide();
                                    $('#ajaxFlashMessage').html('');
                                }, 3000);
                }
            }
          });  
}
</script>
