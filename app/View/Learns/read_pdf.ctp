<!DOCTYPE html>
<html>
<head>
  <title>Video.js | HTML5 Video Player</title>

  <!-- Chang URLs to wherever Video.js files will be hosted -->
  <link href="<?php echo $this->webroot.'video/';?>css/video-js.css" rel="stylesheet" type="text/css">
   <link href="<?php echo $this->webroot.'video/';?>css/videojs.markers.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $this->webroot.'video/';?>css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo $this->webroot;?>css/bootstrap-theme.css" rel="stylesheet">
  <link href="<?php echo $this->webroot;?>css/bootstrap-theme3.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- video.js must be in the <head> for older IEs to work. -->
  <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
  <script src="<?php echo $this->webroot.'video/';?>js/video.js"></script>

  <script src="<?php echo $this->webroot.'video/';?>js/videojs-markers.js"></script>

  <!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
  <script>
    videojs.options.flash.swf = "video-js.swf";
  </script>
<style>


/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/

#wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  z-index: 100000;
  position: relative;
}

#wrapper.toggled {
    padding-left: 480px;
  position: absolute;
  height: 100%;
  top:0;
}

#sidebar-wrapper {
    z-index: 1000;
    left: 545px;
    width: 0;
    height: 100%;
    margin-left: -569px;
    overflow-y: auto;
    overflow-x: hidden;
    background: #1a1a1a;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
  background-color:#fff;
  color:#000;
  position: absolute;
}
.hamburger.is-open{
	width: 35px;
height: 35px;
background: #f00;
border-radius: 100%;
margin-left: -20px;
padding: 18px;
}
#sidebar-wrapper::-webkit-scrollbar {
  display: none;
}

#wrapper.toggled #sidebar-wrapper {
    width: 480px;
}

#page-content-wrapper {
    width: 100%;
    padding-top: 0px;
}

#wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -320px;
}

/*-------------------------------*/
/*     Sidebar nav styles        */
/*-------------------------------*/

.sidebar-nav {
    position: absolute;
    top: 0;
    width: 100%;
    margin: 0;
    padding: 0;
    list-style: none;
  height: 100%;
}

.sidebar-nav li {
    position: relative; 
    line-height: 20px;
    display: inline-block;
    width: 100%;
}


/*-------------------------------*/
/*       Hamburger-Cross         */
/*-------------------------------*/

.hamburger {
  position: fixed;
  top: 20px;  
  z-index: 999;
  display: block;
  width: 32px;
  height: 32px;
  margin-left: 15px;
  background: transparent;
  border: none;
  color:#fff !important;
}
.hamburger:hover,
.hamburger:focus,
.hamburger:active {
  outline: none;
}
.hamburger.is-closed:before {
  content: '';
  display: block;
  width: 100px;
  font-size: 14px;
  color: #fff;
  line-height: 32px;
  text-align: center;
  opacity: 0;
  -webkit-transform: translate3d(0,0,0);
  -webkit-transition: all .35s ease-in-out;
}
.hamburger.is-closed:hover:before {
  opacity: 1;
  display: block;
  -webkit-transform: translate3d(-100px,0,0);
  -webkit-transition: all .35s ease-in-out;
}

.hamburger.is-closed .hamb-top,
.hamburger.is-closed .hamb-middle,
.hamburger.is-closed .hamb-bottom,
.hamburger.is-open .hamb-top,
.hamburger.is-open .hamb-middle,
.hamburger.is-open .hamb-bottom {
  position: absolute;
  left: 0;
  height: 2px;
  width: 100%;
}
.hamburger.is-closed .hamb-top,
.hamburger.is-closed .hamb-middle,
.hamburger.is-closed .hamb-bottom {
  background-color: #000;
}
.hamburger.is-closed .hamb-top { 
  top: 0px; 
  -webkit-transition: all .35s ease-in-out;
}
.hamburger.is-closed .hamb-middle {
  top: 9px;
}
.hamburger.is-closed .hamb-bottom {
  top: 19px;  
  -webkit-transition: all .35s ease-in-out;
}

.hamburger.is-closed:hover .hamb-top {
  top: 0;
  -webkit-transition: all .35s ease-in-out;
}
.hamburger.is-closed:hover .hamb-bottom {
  bottom: 0;
  -webkit-transition: all .35s ease-in-out;
}
.hamburger.is-open .hamb-top,
.hamburger.is-open .hamb-middle,
.hamburger.is-open .hamb-bottom {
  background-color: #000;
}
.hamburger.is-open .hamb-top,
.hamburger.is-open .hamb-bottom {
  top: 50%;
  margin-top: -2px;  
}
.hamburger.is-open .hamb-top { 
  -webkit-transform: rotate(45deg);
  -webkit-transition: -webkit-transform .2s cubic-bezier(.73,1,.28,.08);
}
.hamburger.is-open .hamb-middle { display: none; }
.hamburger.is-open .hamb-bottom {
  -webkit-transform: rotate(-45deg);
  -webkit-transition: -webkit-transform .2s cubic-bezier(.73,1,.28,.08);
}
.hamburger.is-open:before {
  content: '';
  display: block;
  width: 100px;
  font-size: 14px;
  color: #000;
  line-height: 32px;
  text-align: center;
  opacity: 0;
  -webkit-transform: translate3d(0,0,0);
  -webkit-transition: all .35s ease-in-out;
}
.hamburger.is-open:hover:before {
  opacity: 1;
  display: block;
  -webkit-transform: translate3d(-100px,0,0);
  -webkit-transition: all .35s ease-in-out;
}

/*-------------------------------*/
/*            Overlay            */
/*-------------------------------*/

.overlay {
    position: fixed;
    display: none;
    width: auto;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,.5);
    z-index: 1;
}
.instructions{ position: relative; z-index: 0; top:-70px;}

/*****************************************/
@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700,900');
.class_cards{width:94%;margin:10px auto; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2);font-family: 'Lato', sans-serif;}
.class_cards .header_cards{padding: 20px;cursor: pointer;}
.class_cards .header_cards p{font-size:13px;color:#5a5858;}
.class_cards .header_cards span{float:right}
.class_cards .header_cards h4{ color: #5a5858;font-size: 18px;font-weight: 700;}

.card_content{display: none;}
.card_content ul{list-style: none;padding:0;}
.card_content li{width:100%;border-top:1px solid #ebebeb;padding: 15px 20px;position: relative;}
.card_content p{ color: #5a5858;
    font-size: 13px;
    margin: 0;
    padding: 0 30px 0 0;
    position: relative;}

.card_content li b{border: 1px solid #ddd;border-radius: 100%;height: 25px;position: absolute;right: 15px;top: 11px;width: 25px;}
.card_content li b input[type="checkbox"]{position: absolute;width:25px;height:25px;display: block;left:0;top:0;z-index: 9;margin: 0;opacity:0}
.card_content li b input[type="checkbox"]:checked + span{background: #08d6e8 none repeat scroll 0 0;
    border-radius: 100%;
    display: block;
    height: 21px;
    left: 1px;
    position: absolute;
    top: 1px;
    width: 21px;
    z-index: 6;}
    
.fx-lc, .fxac {align-items: center;display: flex;flex-direction: row;}

</style>

</head>
<body>
<input type='hidden' id="hidden_lectureID" value="<?php echo $lectureDetails['Lecture']['id'];?>" >
<input type='hidden' id="hidden_postID" value="<?php echo $postDetails['Post']['id'];?>" >
<input type='hidden' id="hidden_userID" value="<?php echo $userid;?>" >
<input type='hidden' id="hidden_bookmarkTime" value="0">



<?php
                if($userid>0){
                                                                
$isSubscribe = $this->requestAction('/users/isSubscribe/');
}
else
{
 $isSubscribe=0; 
}
?>   
  <div id="wrapper">
    <div class="overlay"></div>
    
    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
     <?php $serial_no = 1;
                     $count = count($lessons);
                        foreach ($lessons as $key => $lesson) { ?>  
      <div class="class_cards">
        <div class="header_cards"> 
          <p>Section: <span><?php echo $serial_no; ?> / <?php echo $count; ?></span></p>
          <h4><?php echo $lesson['Lesson']['title'];?> </h4>
        </div>
        <div class="card_content"> 
          <ul style="list-style: outside none none;" >
            <?php 
                              $lecture_no = 1;
                              foreach ($lesson['Lecture'] as $key => $value) { ?>
                                   <?php if($value['media']==''){
                                        if($value['type']==0){
                                        ?>
                                        
                                        <li>
                                          
                                          <p> <i class="fa fa-file" aria-hidden="true" style="font-size: 18px;" ></i> <?php echo $lecture_no; ?>. <?php echo $value['title'];?></p>
                                          <b>
                                            <input type="checkbox" />
                                            <span></span>
                                          </b>
                                        </li>
                                        <?php } 
                                        if($value['type']==1&&$userid>0){
                                            if($_SESSION['usertype']==2&&$_SESSION['membership_plan_id']==6&&$isSubscribe==1){
                                        $isquiz=0;
                                      $quizMarks=0;
                                      $quizStatus = $this->requestAction('/learns/isQuiz/'.$value['id'].'/'.$userid);  
                                      if(!empty($quizStatus))
                                      {
                                        $isquiz=1;
                                          $quizMarks=$quizStatus['QuizScore']['percentage']; 
                                      }
                                        ?>
                                        
                                         <?php 
                                          if($isquiz==1)
                                          {
                                              if($quizMarks>80)
                                              {
                                                  $quizLink=$this->webroot."learns/quiz_result/".$value['id'];
                                              }
                                              else
                                              {
                                                  $quizLink=$this->webroot."learns/previewquiz1/".$postDetails['Post']['slug']."/".$value['id'];
                                              }
                                          }
                                          else
                                          {
                                              $quizLink=$this->webroot."learns/previewquiz/".$postDetails['Post']['slug']."/".$value['id'];
                                          }
                                          ?>
                                         
                                          <a href="<?php echo $quizLink;?>" target="_blank">
                                            <li>
                                              <p> <i class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 18px;" ></i> Quiz <?php echo $lecture_no; ?>. <?php echo $value['title'];?></p>
                                            </li>
                                          </a>  
                                         
                                   <?php } } } ?>
                                   <?php if($value['media']!=''){
                                        $file = $value['media'];
                                                $ext = pathinfo($file, PATHINFO_EXTENSION);
                                                $imageExt = array('jpeg' , 'jpg');
                                                $videoExt = array('mp4','flv','mkv');
                                                $pdfExt = array('pdf');
                                                $docExt = array('doc','docx','ppt');
                                                if(in_array(strtolower($ext), $videoExt)){
                                                   
                                                    $file_name = trim($value['media'],$ext);
                                                     if($value['status']==1){
                                                        
                                                         //for free video...............................
                                                    ?>     

                                                       <a href="<?php echo $this->webroot.'learns/video_html/'.$postDetails['Post']['slug'].'/'.$value['id'];?>">

                                                            <li style="cursor: pointer;" >
                                                              
                                                              <p> <i class="fa fa-play-circle-o" style="font-size: 18px;" aria-hidden="true"></i> <?php echo $lecture_no; ?>. <?php echo $value['title'];?></p>
                                                            </li>
                                                       </a>
                                                
                                            
                                                <?php } 
                                               elseif($_SESSION['membership_plan_id']==6||$_SESSION['membership_plan_id']==5||$_SESSION['membership_plan_id']==4||$_SESSION['membership_plan_id']==3)
                                          {
                                                  
                                                   
                                                     if($isSubscribe==1){
                                           ?>
                                                <a href="<?php echo $this->webroot.'learns/video_html/'.$postDetails['Post']['slug'].'/'.$value['id'];?>">

                                                            <li style="cursor: pointer;" >
                                                              
                                                              <p> <i class="fa fa-play-circle-o" style="font-size: 18px;" aria-hidden="true"></i> <?php echo $lecture_no; ?>. <?php echo $value['title'];?></p>
                                                            </li>
                                                       </a> 
                                          <?php } } ?>
                                        
                                              <?php } 
                                                
                                                  if(in_array(strtolower($ext), $pdfExt)){ ?>
                                                    <a href="<?php echo $this->webroot;?>learns/read_pdf/<?php echo $postDetails['Post']['slug']."/".$value['id'];?>" >
                                                      
                                                      <li>
                                                        
                                                        <p> <i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size: 18px;" ></i> <?php echo $lecture_no; ?>. <?php echo $value['title'];?> </p>
                                                      </li>
                                                    </a>
                                           <?php }
                                                  if(in_array(strtolower($ext), $imageExt)){ ?>
                                                    <a href="<?php echo $this->webroot;?>learns/view_image/<?php echo $postDetails['Post']['slug']."/".$value['id'];?>" >
                                                       
                                                      <li>
                                                        
                                                        <p> <i class="fa fa-picture-o" aria-hidden="true" style="font-size: 18px;" ></i> <?php echo $lecture_no; ?>. <?php echo $value['title'];?> </p>
                                                      </li>
                                                    </a>
                                           <?php }
                                                  if(in_array(strtolower($ext), $docExt)){ ?>

                                                    <a href="<?php echo $this->webroot;?>learns/read_doc/<?php echo $postDetails['Post']['slug']."/".$value['id'];?>" >
                                                    <li>
                                                      
                                                      <p> <i class="fa fa-file" aria-hidden="true" style="font-size: 18px;" ></i> <?php echo $lecture_no; ?>. <?php echo $value['title'];?> </p>
                                                    </li>
                                                    </a>
                                           <?php } 
                                   } ?>     

                               <?php 
                               $lecture_no = $lecture_no+1; 
                              } ?>
          </ul>
        </div>
      </div>
      <?php $serial_no = $serial_no+1; } ?>
   
  </nav>
  <!-- /#sidebar-wrapper -->

  <!-- Page Content -->
  <div id="page-content-wrapper">
    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
      <span class="hamb-top"></span>
      <span class="hamb-middle"></span>
      <span class="hamb-bottom"></span>
    </button>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">

        </div>
      </div>
    </div>
  </div>
</div>


<?php 
  $file = $media;
  $ext = pathinfo($file, PATHINFO_EXTENSION);
  $file_name = trim($media,$ext); ?>

<section class="quiz-section pdf-section">
	<div class="quiz-header">
		<a href="<?php echo $this->webroot.'learns/course_content/'.$postDetails['Post']['slug'];?>" class="goto">Go to Dashboard</a>
	</div>
	<div class="player-container fullscreen scroll-wrapper">
		<div class="quiz-fullscreen">
			<div class="message-container message-container-quiz">
				<div class="pdf__info">
					<h1>Course Resources</h1>
					<h3> <?php echo 'Section '.$lesCount.', Lecture '.$lecCount;?> </h3>
					<p>Download and review the PDF provided by your instructor for this lecture.</p>
					<div class="form-group">
<!--						<button type="button" onclick="window.location.href='<?php echo $this->webroot;?>lecture_media/<?php echo $lectureDetails["Lecture"]["media"];?>'" class="btn btn-default btn-lg">Download PDF</button>-->
                                                <a class="btn btn-default btn-lg" href="<?php echo $this->webroot;?>lecture_media/<?php echo $lectureDetails["Lecture"]["media"];?>" download> Download </a>
                                        </div>
					<!-- <h4>Resources for this lecture</h4>
					<p>
            <a href="">
              <i class="fa fa-download"></i> Key Terms For Getting Started With Mobile Devices For Special Needs Course
            </a>
          </p>
					<p><a href=""><i class="fa fa-download"></i> Getting Started With Bridging Apps</a></p> -->
					
				</div>
			</div>
		</div>
	</div>
	<div class="player-footer">
		<div class="player-footer-left"></div>
		
		<div class="player-footer-right">
			<a href="<?php echo $URL; ?>" ><button type="button" class="btn btn-primary">Continue <i class="fa fa-angle-right"></i></button></a>
			<div class="btn-group">
				<button href="javascript:void(0)" class="btn btn-sm"> <i class="fa fa-cog fa-spin"></i></button>
				<button href="javascript:void(0)" class="btn btn-sm"> <i class="fa fa-arrows-alt"></i></button>
			</div>
		</div>
	</div>
</section>

  
  
<style>
    /* Style the download icon */
    #downloadButton {
        height: 28px;
        margin-top: 5px;
    width:100px;
    background:#9F0;
    left:0;
    border-radius:5px;
    cursor: pointer;
    font-size:12px;
    color:#fff;
    line-height: 28px;
    text-align: center;
    }
  #downloadButton:before {
      content: "Dashboard";
      color:#fff;
    line-height: 22px;
    text-align: center;
    text-shadow: none !important;
  }
  .vjs-time-controls{ display: none;}
  .vjs-live-controls{ display: none;}
  
  </style>

  <script type="text/javascript">
    videojs('example_video_1').ready(function(){
      // Create variables and new div, anchor and image for download icon
      this.on("ended", function(){
        var URL = '<?php echo $URL;?>';
        console.log(URL);
        window.location.href = URL;
      });

      var video = videojs("example_video_1");
      var bookmarkTime = '<?php echo $bookmarkTime; ?>';
      console.log(bookmarkTime);
      if(bookmarkTime!=''){
        video.currentTime(bookmarkTime); // 4 minutes into the video
        video.play();
      }
      
      var myPlayer = this,
        controlBar,
        newElement = document.createElement('div'),
        newLink = document.createElement('a'),
        newImage = document.createElement('img');
      // Assign id and classes to div for icon
      newElement.id = 'downloadButton';
      newElement.className = 'downloadStyle vjs-control';
      // Assign properties to elements and assign to parents
     <!-- newImage.setAttribute('src','images/file-download.png');-->
      newLink.setAttribute('href','#');
      newLink.appendChild(newImage);
      newElement.appendChild(newLink);
      // Get control bar and insert before elements
      // Remember that getElementsByClassName() returns an array
      controlBar = document.getElementsByClassName('vjs-control-bar')[0];
      // Change the class name here to move the icon in the controlBar
      insertBeforeNode = document.getElementsByClassName('vjs-volume-menu-button')[0];
      // Insert the icon div in proper location
      controlBar.insertBefore(newElement,insertBeforeNode);
    });
  </script>
    
    <script>
  var video = document.getElementById('example_video_1');
var intervalRewind;
$(video).on('play',function(){
    video.playbackRate = 1.0;
    video.currentTime = 5.0;
    clearInterval(intervalRewind);
});
$(video).on('pause',function(){
    video.playbackRate = 1.0;
    clearInterval(intervalRewind);
});
$("#speed").click(function() { // button function for 3x fast speed forward
    video.playbackRate = 3.0;
});
$("#negative").click(function() { // button function for rewind
   intervalRewind = setInterval(function(){
       video.playbackRate = 1.0;
       if(video.currentTime == 0){
           clearInterval(intervalRewind);
           video.pause();
       }
       else{
           video.currentTime += -.1;
       }
                },30);
});
</script>

<script>
function CenterPlayBT() {
   var playBT = $(".vjs-big-play-button");
   playBT.css({
      left:( (playBT.parent().outerWidth()-playBT.outerWidth())/2 )+"px",
      top:( (playBT.parent().outerHeight()-playBT.outerHeight())/2 )+"px"
   });
}
</script>
<script src="<?php echo $this->webroot.'video/';?>js/jquery.min.js"></script> 
<script src="<?php echo $this->webroot.'video/';?>js/bootstrap.min.js"></script>
<script>
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    $(".vjs-big-play-button").trigger("click");

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});

  jQuery('.header_cards').click(function(){
    $(this).siblings('.card_content').slideToggle();
    //alert('');
  });

  jQuery('#downloadButton').click(function(){
    var URL = '<?php echo $this->webroot;?>'+'learns/course_content/'+'<?php echo $postDetails["Post"]["slug"]; ?>';
    window.location.href= URL;
  });

$(window).keypress(function (e) {
  var video = videojs("example_video_1");
  if (e.keyCode === 0 || e.keyCode === 32) {
    e.preventDefault();
    video.pause(true);
    
    var time = video.currentTime();
    var userid = $('#hidden_userID').val();
    var lectureid = $('#hidden_lectureID').val();
    var postid = $('#hidden_postID').val();
    console.log(time);
    $.ajax({
        url: '<?php echo $this->webroot; ?>user_bookmarks/ajaxSaveBookmark',
        type: 'POST',
        dataType: 'json',
        data: {time:time,user_id:userid,lecture_id:lectureid,post_id:postid},
        success: function (data) {
            if(data.Ack==1){
              alert('Bookmark is saved');
            }
        }
    });
  }
})
</script>
<script>
   // initialize video.js
   var player = videojs('example_video_1');

   //load the marker plugin
   player.markers({

      markers: <?php echo json_encode($videoMarker)?>

   });
</script>   


</body>
</html>
