<?php //pr($post_lists); exit;?>
<section class="profileedit">
	<div class="container">
		<div class="row">
			<div class="profile_second_part">
				<div>
				  <h2>Instructor Dashboard</h2>

				 	<div class="dropdown pull-right">
					    <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					        Create a new...
					        <i class="udi udi-chevron-down"></i>
					    </button>
					    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					        <li>
					            <a role="menuitem" tabindex="-1" onclick="window.location.href = '<?php echo $this->webroot; ?>users/announcement'" href="javascript:void(0)">Announcement</a>
					        </li>
					        <li>
					            <a role="menuitem" tabindex="-1" onclick="window.location.href = '<?php echo $this->webroot; ?>posts/add_course'" href="javascript:void(0)">Course</a>
					        </li>
					    </ul>
					</div>

				</div>  
				  <ul class="nav nav-tabs navbar_part">
				    <li class="active"><a data-toggle="tab" href="#home">Courses</a></li>
				    <li><a data-toggle="tab" href="#menu1">Q & A</a></li>
				    <li><a data-toggle="tab" href="#menu2">Reviews</a></li>
				  </ul>

				  <div class="tab-content">
				    <div id="home" class="tab-pane fade in active">
				    <div class="flexbox">
					      <div class="fx mr20">
					      	<div>
					      		<span>Total Revenue</span>
					      	</div>
					      	<div class="a2">$0</div>
					      </div>
					      <div class="fx mr20">
					      	<div>
					      		<span>Recent Average Rating</span>
					      	</div>
					      	<div class="a2">$0.00</div>
					      </div>
					      <div class="fx mr20">
					      	<div>
					      		<span>Total Students</span>
					      	</div>
					      	<div class="a2">0</div>
					      </div>
					      <div class="fx mr20">
					      	<div>
					      		<span>Top Student Locations</span>
					      	</div>
					      	<div class="a2"></div>
					      </div>
					      <div class="fx">
					      	<div>
					      		<span>Countries With Students</span>
					      	</div>
					      	<div class="a2">0</div>
					      </div>
					    </div>
					    <div class="search_course">
					    	<div class="row">
					    		<div class="col-md-6 col-sm-6">
					    			<div class="your_course">
					    			<form name="sortfilter" method="post" action="" id="sort_filter">	
					    				<input type="text" name = "search" id="search_value" class="textfield" value="<?php if(isset($search)){ echo $search; } ?>" placeholder="Search Your Courses">
					    				<button id="search_button" type="button" class="btn btn-primary" style="height: 35px;" ><i class="fa fa-search"></i></button>
					    			</div>
					    		</div>
					    		<div class="col-md-6 col-sm-6">
					    			<div class="your_course">
					    				Sort by
						    			<select class="selectfield" name = "sorting" id="sort_option" >
						    				<option value="by_date" <?php echo (isset($sort) && $sort=='by_date')?'selected':'';?> > Date Created - Oldest </option>
						    				<option value="by_title_desc" <?php echo (isset($sort) && $sort=='by_title_desc')?'selected':'';?> > Title A-Z </option>
						    				<option value="by_title_asc" <?php echo (isset($sort) && $sort=='by_title_asc')?'selected':'';?> > Title Z-A </option>
						    				<option value="by_status_no" <?php echo (isset($sort) && $sort=='by_status_no')?'selected':'';?> > Published No-Yes </option>
						    				<option value="by_status_yes" <?php echo (isset($sort) && $sort=='by_status_yes')?'selected':'';?> > Published Yes-No </option>
						    			</select>
						    		</form>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					    <div class="row">
					    	<div class="col-md-12">
					    	<?php foreach ($post_lists as $key => $post_list) { 
					    		
								if(isset($post_list['PostImage']) && !empty($post_list['PostImage'])){
								$img_array = end($post_list['PostImage']);
								$img = $this->webroot.'img/post_img/'.$img_array['originalpath'];
								}
								else{
								$img = $this->webroot.'img/placeholder.png';
								} 
								?>
					    		<div class="card_teaching  draft">
					    			<img src="<?php echo $img; ?>" style="width: 140px; padding: 10px;">
					    			<div class="card_teaching_bodyplus" onclick="window.location.href = '<?php echo $this->webroot; ?>posts/course_goals/<?php echo $post_list['Post']['slug'];?>'">
					    			  <div class="fx-lt">
					    			<div class="col-100p">
					    				<div class="detailsname"><?php echo $post_list['Post']['post_title'];?></div>
					    				<div class="pb-30 teaching-label"><?php echo $post_list['User']['first_name'].' '.$post_list['User']['last_name'];?></div>
					    				<div class="detaisl_card_status all">
					    					<div class="fx-flex">
					    						<div class="status_label">
					    							<?php
					    								if($post_list['Post']['is_approve']==2){
					    									echo 'DRAFT';
					    								}
					    								if($post_list['Post']['is_approve']==0){
					    									echo 'IN REVIEWS';
					    								}
					    								if($post_list['Post']['is_approve']==1){
					    									echo 'APPROVED';
					    								}  
					    							?>	
					    						</div>
					    						<div class="pricelabel">
					    							<?php
					    							if($post_list['Post']['price']==0){
					    								echo 'Free';
					    							}
					    							else{ 
					    							echo $post_list['Post']['currency_type'].' '.$post_list['Post']['price'];
					    							}
					    							?>
					    							<span> Public</span>
					    						</div>
					    					</div>
					    				</div>
					    			</div>
					    			
					    		</div>
					    		      <div class="published_curriculumn">
					    			<span>Published Curriculum Items</span>
					    			<span class="blockspan">
					    				<?php
					    				echo count($post_list['Lecture']);
					    				?>
					    			</span>
					    			<span>Total 550(max)</span>
					    			<span class="blockspan">
					    				<?php
					    				echo count($post_list['Lecture']);
					    				?>
					    			</span>
					    		</div>
									  <a class="cardteaching_detail" href="#"><label>Go to Course Management</label></a>
					    			</div>
					    			
					    		</div>
					    	<?php } ?>	
					    	</div>	
					    </div>
				    </div>
				    <div id="menu1" class="tab-pane fade">
				      <div class="row">
				      	<div class="col-md-3">
				      		<div class="selectbox"><select class="selectpart">
				      			<option>1</option>
				      			<option>2</option>
				      			<option>3</option>
				      			<option>4</option>
				      			<option>5</option>
				      		</select>
				      		</div>
				      		 <div class="selectbox">
				      		 	<div class="control-group">
							    <label class="control control--checkbox">Unread (0)
							      <input type="checkbox" checked="checked"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  <div class="control-group">
							    <label class="control control--checkbox">No Top Answer (0)
							      <input type="checkbox"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  <div class="control-group">
							    <label class="control control--checkbox">No responce (0)
							      <input type="checkbox"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  
							</div>
							<div class="selectbox">
								Sort by:
								<select class="selectpart">
				      			<option>1</option>
				      			<option>2</option>
				      			<option>3</option>
				      			<option>4</option>
				      			<option>5</option>
				      		</select>
							</div>
				      	</div>
				      	<div class="col-md-9">No Question found</div>
				      </div>
				    </div>
				    <div id="menu2" class="tab-pane fade">
				      <div class="col-md-3">
				      		<div class="selectbox"><select class="selectpart">
				      			<option>1</option>
				      			<option>2</option>
				      			<option>3</option>
				      			<option>4</option>
				      			<option>5</option>
				      		</select>
				      		<div class="control-group">
							    <label class="control control--checkbox">Not responded
							      <input type="checkbox" checked="checked"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
				      		</div>
				      		 <div class="selectbox">
				      		 	<div class="control-group">
							    <label class="control control--checkbox">1 star
							      <input type="checkbox" checked="checked"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  <div class="control-group">
							    <label class="control control--checkbox">2 star
							      <input type="checkbox"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  <div class="control-group">
							    <label class="control control--checkbox">3 star
							      <input type="checkbox"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  <div class="control-group">
							    <label class="control control--checkbox">4 star
							      <input type="checkbox"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							  <div class="control-group">
							    <label class="control control--checkbox">5 star
							      <input type="checkbox"/>
							      <div class="control__indicator"></div>
							    </label>
							  </div>
							</div>
							<div class="selectbox">
								Sort by:
								<select class="selectpart">
				      			<option>1</option>
				      			<option>2</option>
				      			<option>3</option>
				      			<option>4</option>
				      			<option>5</option>
				      		</select>
							</div>
							<button type="button" class="btn btn-default btn-md btn-block form-group"><span>Export to CSV...</span></span> </button>
				      	</div>
				      	<div class="col-md-9">No Question found</div>
				    </div>
				  
				  
			</div>
		</div>
	</div>
</section>
<script>
  $(window).scroll(function(){
      if ($(window).scrollTop() >= 130) {
          $('.header').addClass('fixed');
      }
      else {
         $('.header').removeClass('fixed');
      }
  });
  $("#sort_option").change(function(){
 	  $("#sort_filter").submit();	     
  });
  $("#search_button").click(function(){
      var value = $("#search_value").val();
      if(value!=''){
      	$("#sort_filter").submit();
      }
  });

</script>

<!-- <div class="dropdown">
    <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Create a new...
        <i class="udi udi-chevron-down"></i>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <li>
            <a role="menuitem" tabindex="-1" onclick="UD.GoogleAnalytics.trackEvent('Teach Tab', 'create-announcement');" href="/announcement/">Announcement</a>
        </li>
        <li>
            <a role="menuitem" tabindex="-1" onclick="UD.GoogleAnalytics.trackEvent('Teach Tab', 'create-course');" href="https://teach.udemy.com/">Course</a>
        </li>
    </ul>
</div> -->