<?php //pr($coursedetail); ?>
<?php
    if(isset($coursedetail['PostImage'][0]['originalpath']) && !empty($coursedetail['PostImage'][0]['originalpath'])){
        $metaIMG = 'https://learnfly.com/img/post_img/'.$coursedetail['PostImage'][0]['originalpath'];
    }else{
        $metaIMG = 'https://learnfly.com/img/placeholder.png';
    }
    ?>

<meta property="og:title" content="<?php echo $coursedetail['Post']['post_title']; ?>"/>
<meta property="og:image" content="<?php echo $metaIMG; ?>"/>
<meta property="og:image:secure_url" content="<?php echo $metaIMG; ?>" />
<meta property="og:image:width" content="1280" />
<meta property="og:image:height" content="854" />
<meta property="og:site_name" content="LearnFly.com"/>
<meta property="og:description" content="<?php echo substr(strip_tags($coursedetail['Post']['post_description']),0,150).'...' ; ?>"/>

<link href="<?php echo $this->webroot.'video/';?>css/video-js.css" rel="stylesheet" type="text/css">
<!-- <link href="<?php echo $this->webroot.'video/';?>css/bootstrap.css" rel="stylesheet"> -->


<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
<script>videojs.options.flash.swf = "video-js.swf";</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="<?php echo $this->webroot.'video/';?>videoStyle.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style>


.inner_header{
    *background:#0c2440;
}
.inner-scroll-area {
    margin-top: -30px;
}
.big-title, .big-titles {
    display: block;
    position: relative;
    margin-bottom: 10px;
    margin-top:25px;
}
.big-titles {
    margin-bottom: 0px;
    margin-top:35px;
}
.related-title, .related-titles {
    background:url(https://35.154.208.200/learnfly/img/dots.png) 0 6px repeat-x;
    font-size: 17px;
    padding: 0;
    text-transform: uppercase;
    margin: 0;
    line-height: 1;
    font-weight: bold;
}
.related-titles {
    font-size: 28px;
    line-height: 30px;
    background:url(https://35.154.208.200/learnfly/img/dots.gif) 0 6px repeat-x;
}
.related-title span {
    padding-right: 10px;
    background: #fff;
}
.big-titles span {
    padding-right: 10px;
    background: #fff;
}
#instructor .media-body{padding-left:15px;}

.course-header{padding:40px 0;color: #fff !important;}
.course-header .vjs-default-skin .vjs-big-play-button{border-radius:6px !important; height: 55px !important; width: 55px !important; }
.course-header .vjs-default-skin .vjs-big-play-button::before{line-height: 1.5em;}
.course-header p{font-size: 16px !important;}
.course-header h2{font-size:26px !important; font-weight: 700;}
.course-header span.students{border-bottom:#666 solid 2px;}
.course-header span.students i{display: none;}
.stak{margin-top: 30px !important;}
#menu ul li a{line-height: 44px !important;}
.modal-content{border-radius: 0 !important}
.modal{top: 0 !important}
.curriculum-sec-left ul li .blue-text-li{position: relative; top: 2px;}
.curriculum-scrolling-part{padding: 5px 15px;}
.have-coupon{font-size: 17px; padding:10px; text-align: center;}
.play-img-area{
    position: relative;
}

.play-img-area .play-icon{
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    background: rgba(0,0,0,0.1);
}

.play-img-area .play-icon img{
    width: 70px;
    -webkit-transform: scale(1,1);
    -moz-transform: scale(1,1);
    -ms-transform: scale(1,1);
    -o-transform: scale(1,1);
    transform: scale(1,1);
    -webkit-transition: all ease 0.4s;
    -moz-transition: all ease 0.4s;
    -ms-transition: all ease 0.4s;
    -o-transition: all ease 0.4s;
    transition: all ease 0.4s;
}

.play-img-area .play-icon:hover img{
    -webkit-transform: scale(1.2,1.2);
    -moz-transform: scale(1.2,1.2);
    -ms-transform: scale(1.2,1.2);
    -o-transform: scale(1.2,1.2);
    transform: scale(1.2,1.2);
    -webkit-transition: all ease 0.4s;
    -moz-transition: all ease 0.4s;
    -ms-transition: all ease 0.4s;
    -o-transition: all ease 0.4s;
    transition: all ease 0.4s;
}
.owl-item{
    /*width: 203px !important;*/
}
.text{
    min-height: 146px;
}
.body-scrolling ul.angle-double-right{
    list-style-type: disc;
}

.doller25{display:inline-block; color: #0071b2 !important; font-weight: 700 !important; font-size: 20px !important; padding-top: 5px; text-decoration: line-through; position: relative; left: 10px; top: -2px;}
.discountClass{
    background: #ccc none repeat scroll 0 0;
    display: inline-block;
    font-size: 17px !important;
    font-weight: 700 !important;
    left: 10px;
    padding:4px 10px;
    position:relative;
    top: -2px;
    border-radius: 3px;
}

.auto-fixed .wishlist i{top: 15px; left:-50px;}

.wishlist {
    right: -2px !important;
}
.introduction_asset__text {
    position: absolute;
    bottom: 12px;
    left: 0;
    width: 100%;
    text-align: center;
    font-weight: 800;
    color: white;
    z-index: 99;

}
.divGradient {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: linear-gradient(transparent,transparent,#000);
}

.bestseller-badge__type{
  background: #f00;
  color: #fff;
}

</style>

<?php //echo '<pre>'; print_r($this->params); echo '</pre>';

// $currencyRates = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array('INR','40')));
//pr($currencyRates);

$cd = $this->Session->read['code'];
$sm = $this->Session->read['symbol'];
//echo $cd;
//echo $sm;


?>
<section style="background:url('https://learnfly.com/img/course-headerBg.jpg') #0C2440; ">

    <div class="inner_content" style="padding-bottom:0px; padding-top:30px;">

        <div class="container">

            <div class="row course-header">

                <div class="col-sm-4">

                    <div class="play-img-area">
                        <?php //if (empty($coursedetail['PostVideo'])) {
                                if(isset($coursedetail['PostImage'][0]['originalpath']) && !empty($coursedetail['PostImage'][0]['originalpath'])){
                                    $srcIMG = $this->webroot.'img/post_img/'.$coursedetail['PostImage'][0]['originalpath'];
                                ?>
                                <img style="width: 100%; height: 203px;" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $coursedetail['PostImage'][0]['originalpath']; ?>">
                            <?php  }
                            else{
                                    $srcIMG = $this->webroot.'img/placeholder.png';
                                ?>
                                <img style="width: 100%; height: 203px;" src="<?php echo $this->webroot; ?>img/placeholder.png">
                           <?php } ?>
                           <?php if($coursedetail['PostVideo'][0]['originalpath']!=''){?>
                            <a href="javascript:void(0)" id="modalVideoPlay" class="play-icon">
                                <img src="<?php echo $this->webroot; ?>img/play.svg">
                                <span class="introduction_asset__text" >Preview This Course</span>
                                <div class="divGradient" ></div>
                            </a>
                           <?php } ?>

                    </div>

                </div>
<!-- Post Video Modal Start -->

  <!-- <h2>Large Modal</h2> -->
  <!-- Trigger the modal with a button -->
  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Large Modal</button> -->
<?php //pr($coursedetail['PostVideo'][0]['originalpath']); ?>
  <!-- Modal -->
  <div class="modal fade" id="myVideoModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Course Preview Video</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <?php
                $file = $coursedetail['PostVideo'][0]['originalpath'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $videoExt = array('mp4', 'flv', 'mkv','mov');
                $isVideo=0;
                if (in_array(strtolower($ext), $videoExt)) {
                    $file_name = trim($coursedetail['PostVideo'][0]['originalpath'], $ext);
                    $isVideo=1;
                    ?>


                    <div id="instructions">
                        <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-button" controls preload="auto" width="100%" height="488px">
                              poster="<?php echo $srcIMG; ?>"
                              data-setup='{ "playbackRates": [0.5, 1, 1.5, 2] }' >
                            <source  src="<?php echo $this->webroot; ?>img/post_video/<?php echo $coursedetail['PostVideo'][0]['originalpath']; ?>" type="video/<?php echo $ext; ?>">
                            <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>webm" type='video/webm'>
                            <source src="<?php echo $this->webroot . 'img/post_video/' . $file_name; ?>ogv" type='video/ogg' />
                            <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track>
                            <!-- Tracks need an ending tag thanks to IE9 -->
                            <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track>
                            <!-- Tracks need an ending tag thanks to IE9 -->

                            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>

                        </video>
                    </div>
          <?php
                }
          ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default close" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<!-- Post Video Modal start -->

                <div class="col-sm-8" style="padding: 15px 35px 35px 35px;">
<?php print_r($coursedetail); die;?>
                        <div class="body-scrolling-sub-link" style="margin-top:20px;">
                            <div class="col-sm-12" style="padding-left: 0">
                                <!-- <span class="main-blocks" style="margin-left: 0"><strong style="color:#f00;"><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;Published</strong> <?php echo $coursedetail['Post']['post_date'];?></span> -->
                                <span class="main-blocks"  style="margin-left: 0"><strong style="color:#f00;">
                                <?php if($coursedetail['Post']['language']=='Es'){ ?>
                               <i class="fa fa-language" aria-hidden="true"></i> &nbsp;</strong>Bahasa Indonesia 
                                <?php } else { ?>
                               <i class="fa fa-globe" aria-hidden="true"></i> &nbsp;</strong>English </span>
                                <?php } ?>
                                </span>
                               
<!--                                <span class="main-blocks"><strong style="color:#f00;"><i class="fa fa-window-close" aria-hidden="true"></i> &nbsp;&nbsp;</strong>Closed captions available</span>-->
                            </div>



                        <div class="clearfix"></div>
                        </div>

                        <?php //pr($coursedetail); ?>
                            <h2 class="text-uppercase" style="margin: 15px 0 0 0;"><?php echo $coursedetail['Post']['post_title']; ?></h2>
                            <!-- <hr style="margin-top: 0px;margin-bottom: 10px;"> -->
                            <p style="padding-top:8px; line-height: 18px !important;"><?php echo $coursedetail['Post']['post_subtitle']; ?></p>
                            <p><?php echo $coursedetail['Post']['short_summary']; ?> </p>
                            <p>
                                <!--<img src="<?php echo $this->webroot; ?>img/star_image.jpg">-->
                                <span style="color: #f00">
                                    <!-- <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i> -->
                                <!-- </span> -->
                                <?php
                                    if(isset($coursedetail['Rating']) && $coursedetail['Rating']!=''){
                                        $b = count($coursedetail['Rating']);

                                        $a=0;
                                        foreach ($coursedetail['Rating'] as $value) {
                                            $a=$a + $value['ratting'];
                                        }
                                    }
                                    $finalrating='';
                                    if($b!=0){
                                        $finalrating = ($a / $b);
                                    }

                                     if(isset($finalrating) && $finalrating!='') {
                                        for($x=1;$x<=$finalrating;$x++) { ?>
                                            <i class="fa fa-star" aria-hidden="true" style="color: #f00; font-size:1.0em;" ></i>
                                        <?php }
                                        if (strpos($finalrating,'.')) {  ?>
                                            <i class="fa fa-star-half-o" aria-hidden="true" style="color: #f00; font-size:1.0em;" ></i>
                                        <?php  $x++;
                                        }
                                        while ($x<=5) { ?>
                                            <i class="fa fa-star-o" aria-hidden="true" style="color: #f00; font-size:1.0em;" ></i>
                                        <?php $x++;
                                        } } ?><?php echo $finalrating; ?> (<?php echo $b; ?> ratings) <span class="students"><i class="fa fa-circle"></i>
                                </span>
                                    <?php echo $totalEnrolled;?> students enrolled</span>
                                <?php if ($coursedetail['Post']['is_bestselling'] == '1') { ?>
                                    <span class="bestseller-badge__type"> Bestselling </span>
                                <?php } ?>
                            </p>
                            <p><strong style="font-size:18px;">Instructed by</strong>

                    <a href="#"style="color: #f00;font-weight: 400;"><?php echo $coursedetail['User']['first_name'].' '.$coursedetail['User']['last_name']; ?></a> <!-- <a href="#">Affiliate Marketing</a> --> </p>

                </div>

                <!-- <div class="col-sm-2">

                    <p><strong style="font-size:18px; display: block">Instructed by</strong>

                    <a href="#"style="color: #027b86;font-weight: 400;"><?php echo $coursedetail['User']['first_name'].' '.$coursedetail['User']['last_name']; ?></a> <!-- <a href="#">Affiliate Marketing</a> --> <!--</p>

                </div> -->

            </div>

        </div>

    </div>


</section>


<section>

<!-- Tab Button Start -->
<div id="menu">
            <div class="container sidebar">
                    <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <!-- <li class="hidden">
                        <a class="page-scroll" href="#page-top"></a>
                    </li> -->
                    <li>
                        <a href="#about"> About This Course</a>
                    </li>
                    <li>
                        <a  href="#curriculum">Curriculum</a>
                    </li>
                    <li>
                        <a  href="#instructor">Instructor</a>
                    </li>
                    <?php if(!empty($coursedetail['Rating'])){ ?>
                    <li>
                        <a  href="#reviews">Reviews</a>
                    </li>
                    <?php } ?>
                </ul>
                <!-- <div class="pull-right">
                <ul class="buttonsGroup"> -->

                        <!-- <a href="<?php echo $this->webroot.'learns/course_content/'.$coursedetail['Post']['slug'];?>" ><button type="button" class="btn btn-primary this_course learningButton"  > Start Learning</button></a> -->
                    <!-- <?php
                        if($userid!=$coursedetail['Post']['user_id']){
                            if ($userid) {
                                if (!$courseStatus) {
                                    if($coursedetail['Post']['price'] > 0)
                                    {
                                    ?>


                                    <li><button type="button" class="btn btn-primary cart_button learningButton" data-courseid="<?php echo $coursedetail['Post']['id']; ?>" id="learnflyAddToCart1" <?php echo $existOnCart ? 'disabled' : ''; ?>><?php echo $existOnCart ? 'Added to cart' : 'Add to cart'; ?></button></li>
                                    <?php }else{
                                        ?>
                                     <li><a href=""><button class="btn btn-primary this_course learningButton" id="takeThisCourseDirect" data-courseid="<?php echo $coursedetail['Post']['id']; ?>">Take This Course</button></a></li>
                                     <?php

                                    }

                                    } else {
                                    ?>
                                    <li><a href="<?php echo $this->webroot.'learns/course_content/'.$coursedetail['Post']['slug'];?>" ><button type="button" class="btn btn-primary this_course learningButton"  > Start Learning</button></a></li>


                                    <?php
                                }
                            } else {
                                ?>
                                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login'), true); ?>" class="btn btn-primary this_course learningButton">Take This Course</a>

                                <button type="button" class="btn btn-primary cart_button learningButton cartbutton1" data-courseid="<?php echo $coursedetail['Post']['id']; ?>" id="learnflyAddToCart1" <?php echo $existOnCart ? 'disabled' : ''; ?>><?php echo $existOnCart ? 'Added to cart' : 'Add to cart'; ?></button>
                                <?php
                            }
                        }
                        ?>

                    <?php
                    if($coursedetail['Post']['price'] == 0)
                    { ?>
                    <li><div class="flex-ac"><div class="price">Free</div></div></li>
                    <?php } ?>
                </ul>
            </div> -->
            </div>
</div>


</section>

<!-- Tab Button Closed -->

<div class="container">

<div class="row" style="margin-top: 10px; margin-bottom: 0px;" id="main-fixed">

<!-- Abouts Sections Start -->

<div class="col-sm-8 content">

<div id="about" class="aboutclass">
                <div style="margin-top: -25px;">
                    <div class="body-scrolling">
                        <!--<h2>About This Course </h2>-->

                        <?php  $course_goals = json_decode($coursedetail['Post']['course_goals']); ?>
                        <div class="big-title">
                            <h4 class="related-title">
                                <span>Course Description</span>
                            </h4>
                        </div>
                        <p><?php echo $coursedetail['Post']['post_description'] ;?></p>

                        <?php if($coursedetail['Post']['course_goals']!=''){ ?>
                        <div class="big-title">
                            <h4 class="related-title">
                                <span>What are the requirements?</span>
                            </h4>
                        </div>
                        <ul class="angle-double-right">
                            <?php
                            foreach ($course_goals->need_to_know as $key => $value1) { ?>
                            <li> <?php echo $value1;?></li>
                            <?php } ?>
                        </ul>
                        <div class="big-title">
                            <h4 class="related-title">
                                <span>What am I going to get from this course?</span>
                            </h4>
                        </div>
                        <ul class="angle-double-right">
                            <?php
                            foreach ($course_goals->will_be_able as $key => $value2) { ?>
                            <li><?php echo $value2;?></li>
                            <?php } ?>
                        </ul>
                         <div class="big-title">
                            <h4 class="related-title">
                                <span>Who is the target audience?</span>
                            </h4>
                        </div>
                        <ul class="angle-double-right">
                            <?php
                            foreach ($course_goals->target_student as $key => $value3) { ?>
                            <li> <?php echo $value3;?></li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </div>
            </div>

            </div>


 <!-- Curricullum Started -->

 <div class=" ">


<?php
            $serial_no = 1;
            $lectureTime = 0;
            foreach ($lessons as $key => $lesson) {

                foreach ($lesson['Lecture'] as $key => $value) {
                    //echo '<pre>'; print_r($value); echo '</pre>'; die;
                    if($value['type']==0){ $video = 0;
                        if(isset($value['media']) and !empty($value['media'])){
                            $file = $value['media'];
                            $ext = pathinfo($file, PATHINFO_EXTENSION);
                            $videoExt = array('mp4','flv','mkv','mov','avi');
                            if(in_array(strtolower($ext), $videoExt)){

                               $ffmpeg_path = 'ffmpeg'; //or: /usr/bin/ffmpeg , or /usr/local/bin/ffmpeg - depends on your installation (type which ffmpeg into a console to find the install path)
                                $vid =  '/var/www/html/team4/studilmu/app/webroot/lecture_media/'.$value['media']; //Replace here!

                                if (file_exists($vid)) {


                                    $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                    $mime_type = finfo_file($finfo, $vid); // check mime type
                                    finfo_close($finfo);

                                    if (preg_match('/video\/*/', $mime_type)) {

                                        $video_attributes = _get_video_attributes($vid, $ffmpeg_path);

                                        //print_r('Codec: ' . $video_attributes['codec'] . '<br/>');

                                        //print_r('Dimension: ' . $video_attributes['width'] . ' x ' . $video_attributes['height'] . ' <br/>');

                                       $lectureTime = $lectureTime + $video_attributes['ts'];

                                        //print_r('Size:  ' . _human_filesize(filesize($vid)));

                                    } else {
                                        print_r('File is not a video.');
                                    }
                                } else {
                                    print_r('File does not exist.');
                                }

                            }

                        }
                    }
                }
            }
?>



<div class="big-titles">
                <h4 class="related-titles font-20">
                    <span>Curriculum</span>
                </h4>
                <span id="total_time" style="float: right;margin-top: -28px; font-weight: 700;">Total Time : <?php echo gmdate("H:i:s",$lectureTime); ?></span>
            </div>
            <section id="curriculum">


                <script>

                    // String.prototype.toHHMMSS = function () {
                    //     var sec_num = parseInt(this, 10); // don't forget the second param
                    //     var hours   = Math.floor(sec_num / 3600);
                    //     var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                    //     var seconds = sec_num - (hours * 3600) - (minutes * 60);

                    //     if (hours   < 10) {hours   = "0"+hours;}
                    //     if (minutes < 10) {minutes = "0"+minutes;}
                    //     if (seconds < 10) {seconds = "0"+seconds;}
                    //     return hours+':'+minutes+':'+seconds;
                    // }

                    // function run(val){
                    //     var vid = document.getElementById(val);
                    //     var vDuration = vid.duration || 0;
                    //     if(vDuration!=0){
                    //         return vDuration.toString().toHHMMSS();
                    //     }else{

                    //         return '00:00:00';
                    //     }
                    // }
                </script>


                <div class="section-accordion">

                    <?php
                        $serial_no = 1;
                        $videoLecture = 0;
                        $lVideoTimeing = '';
                        foreach ($lessons as $key => $lesson) { ?>

                        <h3 class="accHdr">Section <?php echo $serial_no;?> : <?php echo $lesson['Lesson']['title'];?></h3>

                        <div>

                                <?php
                                $lecture_no = 1;

                                foreach ($lesson['Lecture'] as $key => $value) {
                                    // echo '<pre>'; print_r($value); echo '</pre>';
                                    if($value['type']==0){ $video = 0;
                                ?>

                                    <div class="curriculum-scrolling-part">
                                        <div class="curriculum-sec-left" style="width: 110px;">
                                            <ul>
                                                <li>
                                                    <?php
                                                    if(isset($value['media']) and !empty($value['media']))
                                                    {

                                                        $file = $value['media'];
                                                        $ext = pathinfo($file, PATHINFO_EXTENSION);
                                                        $imageExt = array('jpeg', 'jpg','png','gif');
                                                        $videoExt = array('mp4','flv','mkv','avi','mov');
                                                        $pdfExt = array('pdf');
                                                        $docExt = array('doc','docx','ppt');
                                                        if(in_array(strtolower($ext), $videoExt)){
                                                            $video = 1;

                                                            $file_name = trim($value['media'],$ext);
                                                            ?>
                                                            <i class="fa fa-play" aria-hidden="true"></i>
                                                        <?php }
                                                        if(in_array(strtolower($ext), $imageExt)){
                                                            $video = 0;

                                                            $file_name = trim($value['media'],$ext);
                                                            ?>
                                                            <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        <?php }
                                                        if(in_array(strtolower($ext), $pdfExt)){
                                                            $video = 0;

                                                            $file_name = trim($value['media'],$ext);
                                                            ?>
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                        <?php }
                                                        if(in_array(strtolower($ext), $docExt)){
                                                            $video = 0;

                                                            $file_name = trim($value['media'],$ext);
                                                            ?>
                                                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                                        <?php } ?>
                                                    <?php }else{ ?>
                                                        <i class="fa fa-file-o" aria-hidden="true"></i>
                                                    <?php }?>
                                                </li>
                                                <li>Lecture <?php echo $lecture_no;?> :</li>
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>
                                        <div class="curriculum-sec-left">
                                            <ul>
                                                <li>

                                                    <span class="blue-text-li"><?php echo $value['title'];?></span>
                                                <!-- <i class="fa fa-sort-desc" aria-hidden="true"></i> --> </li>
                                            </ul>
                                        </div>
                                        <div class="curriculum-sec-left" style="text-align: right;">
                                            <ul>

                                                <li>
                                                    <?php
                                                    
                                                    if(isset($value['media']) and !empty($value['media']))
                                                    {
                                                        $file = $value['media'];
                                                        $ext = pathinfo($file, PATHINFO_EXTENSION);
                                                        $imageExt = array('jpeg' , 'jpg','png','gif');
                                                        $videoExt = array('mp4','flv','mkv','mov','avi');
                                                        $pdfExt = array('pdf');
                                                        $docExt = array('doc','docx','ppt');
                                                        if(in_array(strtolower($ext), $videoExt)){
                                                            $videoLecture++;
                                                            $file_name = trim($value['media'],$ext);
                                                            $valID = "video_".$value['id'];
                                                            ?>
                                                                <script type="text/javascript">
                                                                    // $( window ).on( "load", function() {
                                                                    //     var id="<?php echo $valID ?>";
                                                                    //     var duration=run(id);
                                                                    //     $("#class"+id).append(duration);
                                                                    // });
                                                                </script>


                                                            <!-- <video style="display:none;" id="video_<?php echo $value['id']; ?>" width="400" controls>
                                                              <source src="<?php echo $this->webroot;?>lecture_media/<?php echo $value['media'];?>" type="video/<?php echo $ext; ?>">
                                                              <source src="<?php echo $this->webroot.'lecture_media/'.$file_name;?>ogg" type="video/ogg">
                                                              Your browser does not support HTML5 video.
                                                            </video> -->
                                                            <?php 
                                                            if($userid>0){
                                                            if( $videoLecture == 1 ||$value['status']==1){ ?>
<!--                                                            <a href="javascript:void();"
                                                            class="blue-text-li font-weight video_preview" data-toggle="modal" data-id="<?php echo $value['id'];?>" data-target="#videoModal">Preview The Course</a>-->
                                                            <a href="javascript:void();"
                                                               class="blue-text-li font-weight video_preview" onclick="view_video(<?php echo $value['id'];?>);">Preview</a>
                                                            <?php } }?>

                                                            <?php

                                                                $ffmpeg_path = 'ffmpeg'; //or: /usr/bin/ffmpeg , or /usr/local/bin/ffmpeg - depends on your installation (type which ffmpeg into a console to find the install path)
                                                                $vid =  '/var/www/html/team4/studilmu/app/webroot/lecture_media/'.$value['media']; //Replace here!

                                                                 if (file_exists($vid)) {

                                                                    $finfo = finfo_open(FILEINFO_MIME_TYPE);
                                                                    $mime_type = finfo_file($finfo, $vid); // check mime type
                                                                    finfo_close($finfo);

                                                                    if (preg_match('/video\/*/', $mime_type)) {

                                                                        $video_attributes = _get_video_attributes($vid, $ffmpeg_path);

                                                                        //print_r('Codec: ' . $video_attributes['codec'] . '<br/>');

                                                                        //print_r('Dimension: ' . $video_attributes['width'] . ' x ' . $video_attributes['height'] . ' <br/>');

                                                                        // $lVideoTimeing = $video_attributes['hours'] . ':' . $video_attributes['mins'] . ':'
                                                                        //         . $video_attributes['secs'] . '.' . $video_attributes['ms'] ;
                                                                        $lVideoTimeing = $video_attributes['hours'] . ':' . $video_attributes['mins'] . ':'
                                                                                . $video_attributes['secs'] ;

                                                                        //print_r('Size:  ' . _human_filesize(filesize($vid)));

                                                                    } else {
                                                                        $lVideoTimeing = 'File is not a video.';
                                                                    }
                                                                } else {
                                                                    $lVideoTimeing = 'File does not exist.';
                                                                }


                                                            ?>


                                                  <?php } ?>

                                                      <?php  ?>


                                              <?php }  ?>
                                                </li>
                                                <?php if($value['description']!=''){?>
                                                    <li>
                                                        <i class="fa fa-caret-down lectureDesc" aria-hidden="true"></i>
                                                    </li>
                                                <?php } ?>

                                                <div class="clearfix hideDesc descDiv"><?php echo $value['description'];?></div>
                                            </ul>
                                        </div>

                                        <div class="curriculum-sec-right">
                                            <ul style="margin-bottom:0">

                                                <li class="<?php if($video==1){ echo 'video_time'; } ?>" id="class<?php echo $valID ?>" style="list-style: none;" ><?php if($video==1){ echo $lVideoTimeing;} ?></li>
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>

                                    <div class="clearfix"></div>
                                    </div>
                                    <?php
                                    }
                                    if($value['type']==1){ ?>



                                    <div class="curriculum-scrolling-part">
                                        <div class="curriculum-sec-left">
                                            <ul style="position: relative; top: 0px;">
                                                <li><i class="fa fa-question-circle-o"></i></li>
                                                <li>Quiz <?php echo $lecture_no;?> :</li>
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>
                                        <div class="curriculum-sec-left">
                                            <ul>
                                                <li>
                                            <span class="blue-text-li"><?php echo $value['title'];?></span>
                                                <!-- <i class="fa fa-sort-desc" aria-hidden="true"></i> --> </li>
                                                <li>

                                                </li>
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>
                                        <div class="curriculum-sec-right">
                                            <ul>
                                                <!-- <li>00:21 </li> -->
                                                <div class="clearfix"></div>
                                            </ul>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>

                                <?php
                                   }
                                  $lecture_no = $lecture_no+1;
                                } ?>

                        </div>
                    <?php
                        $serial_no = $serial_no+1;
                        } ?>
                </div>
            </section>


</div>


 <!-- Curricullum Closed -->

 <div class="">

<div class="big-titles">
                <h4 class="related-titles font-20">
                    <span>Instructor Biography</span>
                </h4>
            </div>
                 <section class="instructor-class aboutclass mb-5" id="instructor" style="margin-top: 10px;">

        <div class="">
            <?php //print_r($coursedetail);die; ?>
            <div class="">
                <div class="media">
                <div class="media-left"><img src="<?php if (isset($coursedetail['User']['user_image']) && $coursedetail['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                echo $coursedetail['User']['user_image'];
            } else {
                echo $this->webroot;?>img/profile_img.jpg<?php } ?> "  alt="" class="img-circle" width="90" height="90"/></div>
                <div class="media-body">
                <p style="margin:0;">
                <a href="<?php echo $this->webroot;?>users/user_profile/<?php echo $coursedetail['User']['id'];?>" style="font-size:22px; color:red !important;"><span class="blue-text-li"><?php echo $coursedetail['User']['first_name'].' '.$coursedetail['User']['last_name'];?>,</span></a> </p>
                <!-- <p>Professional Photographer, Travel Journalist, TV Presenter.</p> -->
                <p class="ppSocial">
                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                    <i class="fa fa-globe" aria-hidden="true"></i>
                </p>
                </div>
                <div class="clearfix"></div>
                </div>
                <div class="instructor-content">

                <?php echo $coursedetail['User']['biography'];?>
               <!-- <p>Villiers Steyn's love for photography started when he was still a schoolboy and in 2008, after completing a Masters Degree in Nature Conservation, he stared his own photography business, Vision Photography.</p> -->

                </div>
            </div>
        </div>
    </section>

</div>



</div>


<div class="col-sm-4" style="margin-top: 20px;">

<div class="sticky-sidebar">

<div class="coupon">
                    <?php
                     /*   if($coursedetail['Post']['price']!=0){
                        $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($coursedetail['Post']['currency_type'],$coursedetail['Post']['price'])));

                    ?>
                            <h2 style="margin-bottom: 30px;" ><?php echo $price['symbol']; ?><?php echo number_format($price['price']); ?>
                                <?php if (isset($price['old_price']) && $price['old_price'] != '') {
                                    $oldPrice = $price['old_price'];
                                    ?> <span class="doller25"><?php echo $price['symbol']; ?><?php echo number_format($price['old_price']); ?></span><?php } ?>
                                <?php if (isset($price['old_price']) && $price['old_price'] != '' && isset($price['price']) && $price['price'] != '') { ?>
                                    <span class="discountClass" ><?php
                                        $discount = (($price['old_price'] - $price['price']) * 100) / $price['old_price'];
                                        echo $english_format_number = number_format($discount);
                                        ?>% off</span>
                                <?php } ?>
                            </h2>
                    <?php
                        }else{
                    ?>
                        <h2 style="margin-bottom: 20px;">Free</h2>
                    <?php
                        }*/
                    ?>
                            <!-- <p style="border: #ccc solid 1px;">Coupon valid until Nov 20, 2016 7:59 AM UTC</p> -->


                            <?php
                           if($userid!=$coursedetail['Post']['user_id']){
                            if ($userid) {

                                if (!$courseStatus) {
                                     if($Subscribestatus)
                                {
                              ?>
                        <button class="btn btnDarkBlue btn-block" id="takeThisCourseDirect" data-courseid="<?php echo $coursedetail['Post']['id']; ?>" onclick="take_course(<?php echo $coursedetail['Post']['id']; ?>);">Take This Course</button>
                        <?php
                            }
                            else {
                                ?>
                             <a href="<?php echo $this->webroot;?>" ><button type="button" class="btn btnDarkBlue btn-block"> Purchase Membership</button></a>
                           <?php
                             }
                                }
                            else {
                                ?>

                          <a href="<?php echo $this->webroot.'learns/course_content/'.$coursedetail['Post']['slug'];?>" ><button type="button" class="btn btnDarkBlue btn-block"  > Start Learning</button></a>
                          <?php
                            }

                          }
                           }
                            ?>
                            <!-- <ul>
                                <li><a href="#">Redeem a Coupon</a></li>
                                <li><a href="#">Gift This Course</a></li>

                            </ul> -->

                            <div class="clearfix"></div>
                        </div>

                        <div class="lectures course_lecture" style="margin-top: 25px;">
                            <table class="table table-responsive" style="width: 100%;">
                                <tr>
                                    <td width="40%;"><i class="fa fa-graduation-cap" style="padding-right: 5px;"></i>  Lectures</td>
                                    <td width="60%;"><span class="second_part"><?php echo $totalLecture; ?></span></td>
                                </tr>
                                <tr>
                                    <td width="40%;"><i class="fa fa-sliders" style="padding-right: 5px;"></i> Length   </td>
                                    <td width="60%;"><span class="second_part"><?php echo gmdate("H:i:s",$lectureTime); ?></span></td>
                                </tr>
                                <tr>
                                    <td width="40%;"><i class="fa fa-transgender-alt" style="padding-right: 5px;"></i> Skill Level </td>
                                    <td width="60%;"><span class="second_part"><?php
                                    if($coursedetail['Post']['skill_level']==1){ echo 'Beginner'; }
                                    if($coursedetail['Post']['skill_level']==2){ echo 'Intermedite'; }
                                    if($coursedetail['Post']['skill_level']==3){ echo 'Expert'; }
                                    ?></span></td>
                                </tr>
                                <tr>
                                    <td width="40%;" class="font-14"><i class="fa fa-language" style="padding-right: 5px;"></i> Languages </td>
                                    <td width="60%;">
                                      <span class="second_part">
                                          <?php if($coursedetail['Post']['language']=='Es'){ ?>
                              Bahasa Indonesia 
                                <?php } else { ?>
                              English 
                                <?php } ?>
                                       </span>
                                    </td>
                                </tr>
                               
                                <!-- <tr>
                                    <td>Includes</td>
                                    <td><span class="second_part"><?php echo $coursedetail['Post']['post_subtitle']; ?></span></td>
                                    <td><span class="second_part"><?php echo $coursedetail['Post']['short_summary']; ?></span></td>
                                </tr> -->
                                <!-- <tr>
                                    <td>Apply Coupon</td>
                                    <td><input type="text" ></td>
                                </tr> -->
                            </table>



                            <?php if ($userid) { ?>
                                <div class="wishlist">
                                    <i class="fa <?php echo $whishlistexist ? 'fa-heart' : 'fa-heart-o'; ?>" id="<?php echo $whishlistexist ? 'remove_from_wishlist' : 'add_to_wishlist'; ?>" style="cursor: pointer;" data-postid="<?php echo $coursedetail['Post']['id']; ?>" data-userid="<?php echo $userid; ?>"></i> <span class="wishlist"></span>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="wishlist" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?>users/login'" >
                                    <i class="fa fa-heart-o" style="cursor: pointer;"></i> <span class="wishlist" style="cursor:pointer;"></span>
                                </div>
                            <?php } ?>
                        </div>


<div class="clearfix"></div>
</div>

</div>

<!-- About Section Closed -->


</div>


<div class="clearfix"></div>
</div>

<?php if(!empty($coursedetail['Rating'])){ ?>
<section class="container">
    
    <div class="row">
       <div class="col-md-8">
       <div class="big-titles">
            <h4 class="related-titles">
                <span>Reviews</span>
            </h4>
        </div>
         <!--start-->

    <section id="reviews" class="aboutclass">

        <div class="">
            <div class="review-detail-area-part">
                <div>
                   <div class="col-md-6 col-sm-6 text-center">
                     <p class="h3" style="padding-top:10px;"><strong>Average Rating</strong></p>
                     <p class="nmbr"> <?php echo $finalrating; ?> </p>
                     <p class="rating-star">
                        <!-- <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                        <i class="fa fa-info-circle" aria-hidden="true"></i> -->
                        <?php
                            if(isset($coursedetail['Rating']) && $coursedetail['Rating']!=''){
                                $b = count($coursedetail['Rating']);

                                $a=0;
                                foreach ($coursedetail['Rating'] as $value) {
                                    $a=$a + $value['ratting'];
                                }
                            }
                            $finalrating='';
                            if($b!=0){
                                $finalrating = ($a / $b);
                            }

                            if(isset($finalrating) && $finalrating!='') {
                            for($x=1;$x<=$finalrating;$x++) { ?>
                                <i class="fa fa-star" aria-hidden="true" style="color: green; font-size:1.7em;" ></i>
                            <?php }
                            if (strpos($finalrating,'.')) {  ?>
                                <i class="fa fa-star-half-o" aria-hidden="true" style="color: green; font-size:1.7em;" ></i>
                            <?php  $x++;
                            }
                            while ($x<=5) { ?>
                                <i class="fa fa-star-o" aria-hidden="true" style="color: green; font-size:1.7em;" ></i>
                            <?php $x++;
                        } } ?>
                   </p>
               </div>
               <?php
                    if(isset($coursedetail['Rating']) && $coursedetail['Rating']!=''){
                        $fiveCount = 0;
                        $fourCount = 0;
                        $threeCount = 0;
                        $twoCount = 0;
                        $oneCount = 0;
                        foreach ($coursedetail['Rating'] as $val) {
                            if( $val['ratting']==0.5 ){ $oneCount = $oneCount + 1; }
                            if( $val['ratting']==1.5 || $val['ratting']==1 ){ $oneCount = $oneCount + 1; }
                            if( $val['ratting']==2.5 || $val['ratting']==2 ){ $twoCount = $twoCount + 1; }
                            if( $val['ratting']==3.5 || $val['ratting']==3 ){ $threeCount = $threeCount + 1; }
                            if( $val['ratting']==4.5 || $val['ratting']==4 ){ $fourCount = $fourCount + 1; }
                            if( $val['ratting']==5 ){ $fiveCount = $fiveCount + 1; }
                        }
                        $b = count($coursedetail['Rating']);
                        $fiveCountParse = 0;
                        $fourCountParse = 0;
                        $threeCountParse = 0;
                        $twoCountParse = 0;
                        $oneCountParse = 0;

                        if( $oneCount !=0 ){ $oneCountParse = ($oneCount/$b)*100; }
                        if( $twoCount !=0 ){ $twoCountParse = ($twoCount/$b)*100; }
                        if( $threeCount !=0 ){ $threeCountParse = ($threeCount/$b)*100; }
                        if( $fourCount !=0 ){ $fourCountParse = ($fourCount/$b)*100; }
                        if( $fiveCount !=0 ){ $fiveCountParse = ($fiveCount/$b)*100; }

                    }
               ?>
               <div class="col-md-6 col-sm-6">
                  <div class="review-detail">
                      <h4>Detail</h4>
                      <div class="detail-area">
                        <div class="detail-area-left">5 Stars</div>
                        <div class="detail-progress-left">
                            <div class="progress">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $fiveCountParse; ?>%">
                                <span class="sr-only"><?php echo $fiveCountParse; ?>% Complete (success)</span>
                            </div>
                        </div>
                    </div>
                    <div class="detail-area-right" style="width: 20px;" ><?php echo $fiveCountParse; ?>%</div>
                    <div class="clearfix"></div>

                </div>
                <div class="detail-area">
                    <div class="detail-area-left">4 Stars</div>
                    <div class="detail-progress-left">
                        <div class="progress">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $fourCountParse?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $fourCountParse; ?>%">
                            <span class="sr-only"><?php echo $fourCountParse; ?>% Complete (success)</span>
                        </div>
                    </div>
                </div>
                <div class="detail-area-right" style="width: 20px;" ><?php echo $fourCountParse; ?>%</div>
                <div class="clearfix"></div>
            </div>
            <div class="detail-area">
                <div class="detail-area-left">3 Stars</div>
                <div class="detail-progress-left">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $threeCountParse; ?>%">
                        <span class="sr-only"><?php echo $threeCountParse; ?>% Complete (success)</span>
                      </div>
                    </div>
                </div>
            <div class="detail-area-right" style="width: 20px;" ><?php echo $threeCountParse; ?>%</div>
            <div class="clearfix"></div>
        </div>

            <div class="detail-area">
                <div class="detail-area-left">2 Stars</div>
                <div class="detail-progress-left">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $twoCountParse; ?>%">
                        <span class="sr-only"><?php echo $twoCountParse; ?>% Complete (success)</span>
                    </div>
                </div>
            </div>
            <div class="detail-area-right" style="width: 20px;" ><?php echo $twoCountParse; ?>%</div>
            <div class="clearfix"></div>
        </div>

            <div class="detail-area">
                <div class="detail-area-left">1 Stars</div>
                <div class="detail-progress-left">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $oneCountParse; ?>%">
                        <span class="sr-only"><?php echo $oneCountParse; ?>% Complete (success)</span>
                    </div>
                </div>
            </div>
            <div class="detail-area-right" style="width: 20px;" ><?php echo $oneCountParse; ?>%</div>
            <div class="clearfix"></div>
        </div>


    </div>

</div>

<div class="clearfix"></div>
</div>
</div>
</div>

    <!-- <div class="people-review">
        <div class="people-review-left">
            <img src="<?php echo $this->webroot;?>images/anonymous.png">
            <div class="people-review-text">
               <p>Jean Christ</p>
               <p class="light-gray">posted 6 days ago</p>
            </div>
            <div class="clearfix"></div>
            <p class="rating-star">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
            </p>
            <p>easy to understand, starts with the basics</p>
        </div>
        <div class="people-review-right">
            <i class="fa fa-flag" aria-hidden="true"></i>
        </div>
        <div class="clearfix"></div>
    </div> -->
</div>
</section >
         <!--end //-->
         <div class="container">
          <!--start-->
          <div class="row">
          	<div class="col-md-8">
           <?php foreach ($coursedetail['Rating'] as $key => $vlu) { ?>
          <div class="people-review">
            <div class="people-review-left">
                <img src="<?php if (isset($vlu['User']['user_image']) && $vlu['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                echo $vlu['User']['user_image'];
            } else {
                echo $this->webroot;
                ?>img/profile_img.jpg<?php } ?>" class="img-circle">
                <div class="people-review-text">
                   <p><?php echo $vlu['User']['first_name'].' '.$vlu['User']['last_name']; ?></p>
                   <p class="light-gray" style="font-size:13px;">posted <?php echo postTime($vlu['ratting_date']); ?></p>
               </div>
               <div class="clearfix"></div>
                <p class="rating-star">
                    <!-- <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i> -->
                    <?php
                            $finalrating = $vlu['ratting'];
                            if(isset($finalrating) && $finalrating!='') {
                            for($x=1;$x<=$finalrating;$x++) { ?>
                                <i class="fa fa-star" aria-hidden="true" style="color: green; font-size:1.0em;" ></i>
                            <?php }
                            if (strpos($finalrating,'.')) {  ?>
                                <i class="fa fa-star-half-o" aria-hidden="true" style="color: green; font-size:1.0em;" ></i>
                            <?php  $x++;
                            }
                            while ($x<=5) { ?>
                                <i class="fa fa-star-o" aria-hidden="true" style="color: green; font-size:1.0em;" ></i>
                            <?php $x++;
                        } } ?>
                </p>
               <p><?php echo $vlu['comment'] ; ?></p>
            </div>
            <div class="people-review-right">
                <i class="fa fa-flag" aria-hidden="true"></i>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
          <!--end //-->
          </div>
        </div>


                </div>
            </div>
            </div>

        </div>
        </div>
    </div>
    </div>
</section>
<?php } ?>


<section class="tabpart theme-bg-color pt-5" id="student-fixed" >
    <div class="container">
        <h1 class="zilla font-weight-light text-center font-30 mb-5 p-relative">Students also may like
          <em class="dvdr">
            <img src="<?php echo $this->webroot; ?>images/divider.png" alt="">
          </em>
        </h1>
        <div class="row mt-3">
             <?php foreach ($courses as $key => $course) { ?>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3">
            <a href="<?php echo $this->webroot; ?>posts/details/<?php echo $course['Post']['slug']; ?>" style="text-decoration: none;color: #2e2e2e;">
               <figure class="course-box bg-white newCourse-box">
                  <div class="course-video p-relative">
                     <img width="100%" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $course['PostImage']['0']['originalpath']; ?>">
                     <div class="previewCourse">
                        <div class="text-center">
                           <span class="font-30"><i aria-hidden="true" class="fa fa-play"></i></span>
                           <div class="font-weight-bold font-12">Preview this course</div>
                        </div>
                     </div>
                  </div>
                  <figcaption class="bg-white p-3">
                     <h5><?php
                                      if(strlen($course['Post']['post_title'])<=30)
                                      {
                                        echo $course['Post']['post_title'];
                                      }
                                      else
                                      {
                                        $y=substr($course['Post']['post_title'],0,30) . '...';
                                        echo $y;
                                      }
                                //echo  $course['Post']['post_title'];
                                ?></h5>
                      <h6 class="font-weight-light font-12">
                          <?php
                                if (!empty($course['User'])) {
                                ?>
                                <h6><?php echo $course['User']['first_name'] . ' ' . $course['User']['last_name']; ?></h6>
                                <?php
                                }
                                ?>
                      </h6>
                     <p class="font-12 mb-2">
                        <span class="text-blue">
                            <?php
                                            if(isset($course['Rating']) && $course['Rating']!=''){
                                                $b = count($course['Rating']);

                                                $a=0;
                                                foreach ($course['Rating'] as $value) {
                                                    $a=$a + $value['ratting'];
                                                }
                                            }
                                            $finalrating='';
                                            if($b!=0){
                                                $finalrating = ($a / $b);
                                            }

                                             if(isset($finalrating) && $finalrating!='') {
                                                for($x=1;$x<=$finalrating;$x++) { ?>
                                                    <i class="fa fa-star" aria-hidden="true" style="color: #f00;"></i>
                                                <?php }
                                                if (strpos($finalrating,'.')) {  ?>
                                                   <i class="fa fa-star-half-o" aria-hidden="true" style="color: #f00;"></i>
                                                <?php  $x++;
                                                }
                                                while ($x<=5) { ?>
                                                    <i class="fa fa-star-o" aria-hidden="true" style="color: #f00;" ></i>
                                                <?php $x++;
                                                }
                                              }else { ?>
                                                    <i class="fa fa-star-o" aria-hidden="true" style="color: #f00;" ></i>
                                                    <i class="fa fa-star-o" aria-hidden="true" style="color: #f00;" ></i>
                                                    <i class="fa fa-star-o" aria-hidden="true" style="color: #f00;" ></i>
                                                    <i class="fa fa-star-o" aria-hidden="true" style="color: #f00;" ></i>
                                                   <i class="fa fa-star-o" aria-hidden="true" style="color: #f00;" ></i>
                                              <?php } ?>

                        </span>
                        <span><span class="font-weight-bold mr-1"><?php echo $finalrating; ?></span> ( <?php echo $course['Post']['click_count']; ?> views ) </span>
                     </p>
                  </figcaption>
               </figure>
            </a>
         </div>
             <?php } ?>
      </div>

        <div class="row">
            <!-- </div>
            <div class="row"> -->
            <!--<div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul id="mycarousel">
                        <?php
                        foreach ($otherCourses as $key => $otherCourse) {

                            if ($otherCourse['Post']['post_description'] != '') {
                                if (strlen($otherCourse['Post']['post_description']) > 100) {
                                    $stringCut = substr($otherCourse['Post']['post_description'], 0, 100);
                                    $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . "....";
                                } else {
                                    $string = $otherCourse['Post']['post_description'];
                                }
                            }
                            ?>
                            <li>
                                <div >
                                    <div class="coursename" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?>posts/course_details/<?php echo $otherCourse['Post']['slug']; ?>'">
                                        <div class="courseimage"><img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $otherCourse['PostImage'][0]['originalpath']; ?>"></div>
                                        <div class="course_bottom_part">
                                            <div class="name-p"><?php echo $otherCourse['Post']['post_title']; ?></div>
                                            <div class="course_para"><?php echo $string; ?></div>
                                            <div class="writer_name">By <?php echo $otherCourse['User']['first_name'] . ' ' . $otherCourse['User']['last_name']; ?></div>

                                            <div class="price">
                                                <div class="pull-left"><span class="pricenin">$<?php echo $otherCourse['Post']['price']; ?></span> <span class="staricon"><img src="<?php echo $this->webroot; ?>img/star.png"></span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <p class="jcarousel-pagination"></p>
            </div>-->

        </div>
    </div>
</section>
</section>

<!-- Video Modal -->
<div id="videoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="margin-top:130px;" >
        <div class="modal-header">
            <h4 class="modal-title">Video Preview</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="lectureModalVideo" >
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

  </div>
</div>

<?php

    function postTime($time = NULL){
        $start_date = new DateTime($time);
        $your_date = gmdate('Y-m-d H:i:s');
        $since_start = $start_date->diff(new DateTime($your_date));
        //echo $since_start = floor(($your_date - $start_date) / (60 * 60 * 24));
        //echo $since_start->days.' days total';
        $y = $since_start->y . ' years ago';
        $m = $since_start->m . ' months ago';
        $d = $since_start->d . ' days ago';
        $h = $since_start->h . ' hours ago';
        $i = $since_start->i . ' minutes ago';
        $s = $since_start->s . ' seconds ago';
        if ($y != 0) {
            return $y;
        } else {
            if ($m != 0) {
                return $m;
            } else {
                if ($d != 0) {
                    return $d;
                } else {
                    if ($h != 0) {
                        return $h;
                    } else {
                        if ($i != 0) {
                            return $i;
                        } else {
                            if ($s != 0) {
                                return $s;
                            }
                        }
                    }
                }
            }
        }
    }

?>

<?php


function _get_video_attributes($video, $ffmpeg) {

    $command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';
    $output = shell_exec($command);

    $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; // or : $regex_sizes = "/Video: ([^\r\n]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/"; (code from @1owk3y)
    if (preg_match($regex_sizes, $output, $regs)) {
        $codec = $regs [1] ? $regs [1] : null;
        $width = $regs [3] ? $regs [3] : null;
        $height = $regs [4] ? $regs [4] : null;
    }

    $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
    if (preg_match($regex_duration, $output, $regs)) {
        $hours = $regs [1] ? $regs [1] : null;
        $mins = $regs [2] ? $regs [2] : null;
        $secs = $regs [3] ? $regs [3] : null;
        $ms = $regs [4] ? $regs [4] : null;
        $ts = ($hours*60*60) + ($mins*60)+ $secs;
    }

    return array('codec' => $codec,
        'width' => $width,
        'height' => $height,
        'hours' => $hours,
        'mins' => $mins,
        'secs' => $secs,
        'ms' => $ms,
        'ts' => $ts
    );
}

function _human_filesize($bytes, $decimals = 2) {
    $sz = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
} ?>
<script src="<?php echo $this->webroot;?>js/jquery.stickybar.min.js"></script>
<script>
    function take_course(id){
       var postid = id;
            //alert(postid);
            $.ajax({
                url: '<?php echo $this->webroot; ?>user_courses/take_this_course_direct',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                success: function (data) {
                //alert(data.Ack);
                    if (data.Ack == '1') {
                         //alert('Courses added successfully');
                      window.location.href = '<?php echo Router::url(array('controller' => 'user_courses', 'action' => 'index'), true) ?>';
                    } else {
                        alert('Courses not added');
                    }
                }
            });
    }

    function view_video(id)
    {
            var lecture_id = id;
            //alert(lecture_id);
            $.ajax({
                url: '<?php echo $this->webroot; ?>lectures/ajaxLectureVideo',
                type: 'POST',
                dataType: 'json',
                data: {lecture_id:lecture_id},
                success: function (data) {
                    if(data.Ack==1){
                        $("#lectureModalVideo").html('');
                        $("#lectureModalVideo").html(data.html);
                        $('#videoModal').modal('show');
                    }
                }
            });

    }

    function onScroll(event){
    var scrollPos = $(document).scrollTop();

        $('#sidebar a').each(function () {
        var currLink = $(this);
                var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#sidebar ul li a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}
$( function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      heightStyle: "content"
    });
});

$(document).ready(function () {
    $(".lectureDesc").click(function(){
        $(this).toggleClass("fa-sort-asc");
        $(this).closest("div").find('.descDiv').toggleClass("hideDesc");
    });

    $("#modalVideoPlay").click(function(){
        $('#myVideoModal').modal('show');
    })
<?php if($isVideo==1){ ?>
    var myPlayer = videojs("example_video_1");
<?php } ?>
    $('.close').on('click', function () {
      // do something…
      myPlayer.pause();

    })

      var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();
    });

    // $(".vjs-big-play-button").trigger("click");

    function hamburger_cross() {

      if (isClosed == true) {
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }

  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });

    $('.header_cards').click(function(){
    $(this).siblings('.card_content').slideToggle();
    //alert('');
  });

$("#videoModal").on("hidden.bs.modal", function () {
    $('video').trigger('pause');
});

$(window).scroll(function(){

$(document).on("scroll", onScroll);
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
                $target = $(target);
       $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 600, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

   $('#menu').stickyBar();

        // Smooth scrolling
        $('a[href*=\\#]:not([href=\\#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 0);
                    return false;
                }
            }
        });

        $(document).on('click','#add_to_wishlist',function () {
            var self = $(this);
            var postid = self.data('postid');
            var userid = self.data('userid');
            $.ajax({
                url: '<?php echo $this->webroot; ?>wishlists/add_to_wishlist',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.removeClass().addClass('fa fa-heart');
                        self.attr("id","remove_from_wishlist");
                    } else {
                        alert(data.msg);
                    }
                }
            });
        });
        $(document).on('click','#remove_from_wishlist',function () {
            var self = $(this);
            var postid = self.data('postid');
            var userid = self.data('userid');
            $.ajax({
                url: '<?php echo $this->webroot; ?>wishlists/remove_from_wishlist',
                type: 'POST',
                dataType: 'json',
                data: {
                    postid: postid,
                    userid: '<?php echo $userid; ?>'
                },
                success: function (data) {
                    if (data.Ack == '1') {
                        self.removeClass().addClass('fa fa-heart-o');
                        self.attr("id","add_to_wishlist");
                    } else {
                        alert(data.msg);
                    }
                }
            });
        });
})

$('body').scrollspy({target: '#menu'})


</script>
<?php if($isVideo==1){ ?>
<script type="text/javascript">
    videojs('example_video_1').ready(function(){
      // Create variables and new div, anchor and image for download icon
      var myPlayer = this,
        controlBar,
        newElement = document.createElement('div'),
        newLink = document.createElement('a'),
        newImage = document.createElement('img');
      // Assign id and classes to div for icon
      newElement.id = 'downloadButton';
      newElement.className = 'downloadStyle vjs-control';
      // Assign properties to elements and assign to parents
     <!-- newImage.setAttribute('src','images/file-download.png');-->
      newLink.setAttribute('href','#');
      newLink.appendChild(newImage);
      newElement.appendChild(newLink);
      // Get control bar and insert before elements
      // Remember that getElementsByClassName() returns an array
      controlBar = document.getElementsByClassName('vjs-control-bar')[0];
      // Change the class name here to move the icon in the controlBar
      insertBeforeNode = document.getElementsByClassName('vjs-volume-menu-button')[0];
      // Insert the icon div in proper location
      controlBar.insertBefore(newElement,insertBeforeNode);

    // $('#modalVideoPlay').on('hidden', function () {
    //     myPlayer.pause();
    // });



    });
  </script>

    <script>
    var video = document.getElementById('example_video_1');
    var intervalRewind;
    $(video).on('play',function(){
        video.playbackRate = 1.0;
        clearInterval(intervalRewind);
    });
    $(video).on('pause',function(){
        video.playbackRate = 1.0;
        clearInterval(intervalRewind);
    });
    $("#speed").click(function() { // button function for 3x fast speed forward
        video.playbackRate = 3.0;
    });
    $("#negative").click(function() { // button function for rewind
       intervalRewind = setInterval(function(){
           video.playbackRate = 1.0;
           if(video.currentTime == 0){
               clearInterval(intervalRewind);
               video.pause();
           }
           else{
               video.currentTime += -.1;
           }
                    },30);
    });
</script>
<?php } ?>
<script>
function CenterPlayBT() {
   var playBT = $(".vjs-big-play-button");
   playBT.css({
      left:( (playBT.parent().outerWidth()-playBT.outerWidth())/2 )+"px",
      top:( (playBT.parent().outerHeight()-playBT.outerHeight())/2 )+"px"
   });
}
</script>

<style>
.hideDesc{
    display: none;
}
.popular-courses .item{
  max-width: 250px;
}

</style>


</div>


<!-- Rating Section Closed -->


</div>


</section>

<script>
$(window).on('load',function(){
var sidebar = $('.sticky-sidebar').offset().top;
var blueOffset = $('#student-fixed').offset().top;
var sidebarHeight = $('.sticky-sidebar').height();

$(window).scroll(function(){
  if ($(window).scrollTop()> sidebar) {
    $('.sticky-sidebar').addClass('auto-fixed');
  } else {
    $('.sticky-sidebar').removeClass('auto-fixed');
  }
  if ($(window).scrollTop() > blueOffset - sidebar) {
    $('.sticky-sidebar').removeClass('auto-fixed');
  }
});
});

</script>


<style type="text/css">

     #student-fixed{position: relative; z-index: 99;}
     #main-fixed{position: relative !important;}
    .auto-fixed{position: fixed; top: 54px; width: 27%; z-index: 1;}
    .ui-accordion .ui-accordion-header{padding: 10px !important; background: #f9f9f9; border:#eee solid 1px;}
    .curriculum-scrolling-part{padding: 10px;}
    .curriculum-sec-left{width: 35%;}
    .sticky-sidebar {
        border: 1px solid #ddd;
        border-radius: 3px;
        padding: 20px;
        margin-top: 10px;
    }
    @media (max-width: 767px){
        .auto-fixed{position: static; width: 100%;}
        .curriculum-sec-left{width: 34%;}
    }

    @media (max-width: 480px){
        .curriculum-sec-left{width: 100% !important; border-bottom:#ddd solid 1px; text-align: center !important; padding: 5px;}
        .curriculum-sec-right{width: 100%; text-align: center;  padding: 5px;}

    }
</style>
