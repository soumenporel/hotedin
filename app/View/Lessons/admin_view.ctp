<style>
.tools {
    margin: -28px -5px;
}
a:hover {
    text-decoration: none;
}
</style>
<div class="categories view">
	<h2><?php echo __('Lesson'); ?></h2>
	<a href="<?php echo $this->webroot.'admin/posts/view/'.$lesson['Post']['id']; ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Back to Course View</a>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Course Name'); ?></dt>
		<dd>
			<?php echo h($lesson['Post']['post_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lesson Title'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lesson Description'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['description']); ?>
			&nbsp;
		</dd>

	</dl>

	<?php
		$info = array('post_id'=>$lesson['Post']['id'],'lesson_id'=>$lesson['Lesson']['id']);
        $ids = implode(',', $info);
	?>

	<div class="row">
		<div class="col-md-6">
			<button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-lg btn-block" style="margin-top: 47px;margin-bottom: 50px;">Add Quiz</button>
		</div>
		<div class="col-md-6">
			<a href="<?php echo $this->webroot.'admin/lectures/add/'.base64_encode($ids);?>" ><button type="button" data-toggle="modal" class="btn btn-primary btn-lg btn-block" style="margin-top: 47px;margin-bottom: 50px;">Add Lecture</button></a>
		</div>
	</div>
	
</div>
<input type="hidden"  id="hidden_post_id" value="<?php echo $lesson['Lesson']['post_id']; ?>">
<input type="hidden"  id="hidden_lesson_id" value="<?php echo $lesson['Lesson']['id']; ?>">
<div class="bd-example">
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="exampleModalLabel">Add Quiz</h4>
				</div>
				<div class="modal-body">
					<form class="section_form">
						<div class="form-group">
							<label for="recipient-name" class="form-control-label">Quiz Title:</label>
							<input type="text" class="form-control" name="data[quiz][name]" id="recipient-name">
							<input type="hidden" id="course_id" name="data[quiz][lesson_id]" value="<?php echo $lesson['Lesson']['id']; ?>">
						</div>
						<div class="form-group">
							<label for="message-text" class="form-control-label">Quiz Description:</label>
							<textarea class="form-control" name="data[quiz][objective]" id="message-text"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" id="submit_quiz_add" class="btn btn-primary add-quiz">Add Quiz</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Add Questions Modal-->
<div class="modal fade" id="questions" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Quiz Question</h4>
			</div>
			<div class="modal-body">
				
				<form id="add_questions" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label class="form-control-label" for="email">Question :</label>
							<textarea name="data[Question][question]"  id="question" required></textarea>
							<input type="hidden" name='data[lecture_id]' id="quizID" >
							<input type="hidden" name="data[quiz_ans]" id="quiz_ans_option" >
					</div>
					<div class="form-group">
						<label class="form-control-label" for="email">Option :</label>
							<input type="radio" class="quiz_ans" name='ans[]' >
							<textarea name="data[Question][options][]" class="message45"></textarea>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="email">Option :</label>
							<input type="radio" class="quiz_ans" name='ans[]' >
							<textarea name="data[Question][options][]" class="message45"></textarea>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="email">Option :</label>
							<input type="radio" class="quiz_ans" name='ans[]' >
							<textarea name="data[Question][options][]" class="message45"></textarea>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="email">Option :</label>
							<input type="radio" class="quiz_ans" name='ans[]' >
							<textarea name="data[Question][options][]" class="message45"></textarea>
					</div>
					<div id="container-append"></div>

					<!-- <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default">Submit</button>
						</div>
					</div> -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-default">Submit</button>
			</div>
				</form>	
		</div>
	</div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm12">
		<div class="panel">
			<header class="panel-heading">
				<h2><?php echo __('Lectures'); ?></h2>
				<span class="tools pull-right">
					<a href="javascript:;" class="fa fa-chevron-down"></a>
					<!-- <a href="javascript:;" class="fa fa-times"></a> -->
				</span>
			</header>
			<div class="panel-body" style="display: block;">
				<div style="overflow-x: auto;">

					<table cellpadding="0" cellspacing="0">

						<tr>
							<th>Id</th>
							<th>Media</th>
							<th>Course Title</th>
							<th>Lesson Title</th>
							<th>Title</th>
							<th>Description</th>
							<th>Duration</th>


							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>

						<?php  
						$count = 1;
						foreach ($lecture as $value): ?>
						<tr>
							<td><?php echo h($count); ?></td>
							<td><?php
							if(isset($value['Lecture']['media']) and !empty($value['Lecture']['media']))
							{
								$file = $value['Lecture']['media'];
								$ext = pathinfo($file, PATHINFO_EXTENSION);
								$imageExt = array('jpeg' , 'jpg');
								$videoExt = array('mp4','flv','mkv');
								$pdfExt = array('pdf');
								$docExt = array('doc','docx','ppt');
								if(in_array(strtolower($ext), $videoExt)){ 
									$file_name = trim($value['Lecture']['media'],$ext);
									?>     
									<img alt="" src="<?php echo $this->webroot."img/videoImage/videoimage.png";?>" style=" height:80px; width:80px;">

									<?php }
									if(in_array(strtolower($ext), $imageExt)){ ?>		
									<img alt="" src="<?php echo $this->webroot;?>lecture_media/<?php echo $value['Lecture']['media'];?>" style=" height:80px; width:80px;">
									<?php

								}
								if(in_array(strtolower($ext), $pdfExt)){ ?>
								<a href="<?php echo $this->webroot;?>lecture_media/<?php echo $value['Lecture']['media'];?>" target="_blank">Read PDF in another tab</a>
								<?php }
								if(in_array(strtolower($ext), $docExt)){ ?>

								<a href="<?php echo $this->webroot;?>lecture_media/<?php echo $value['Lecture']['media'];?>" download>
									<button type="button" class="btn btn-primary">Download</button>
								</a>
								<?php }
							}
							else{
								?>
								<img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

								<?php } ?></td>
								<td><?php echo h($lesson['Lesson']['title']); ?></td>
								<td><?php echo h($lesson['Post']['post_title']); ?></td>
								<td><?php echo h($value['Lecture']['title']); ?></td>
								<td><?php echo h($value['Lecture']['description']); ?></td>
								<td><?php echo h($value['Lecture']['duration']); ?></td>
								<td >
									<?php
									echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'lectures' , 'action' => 'view', $value['Lecture']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
									?>
									<?php echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')),
					                array('controller'=>'lectures', 'action' => 'delete', $value['Lecture']['id']),
					                array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
					                __('Are you sure you want to delete # %s?', $value['Lecture']['id'])); ?>


								</td>

							</tr>
							<?php 
							$count++ ;
							endforeach; ?>
						</table>

						<div class="paging">
							<span class="prev disabled">&lt; previous</span><span class="next disabled">next &gt;</span>                        
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
			

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm12">
		<div class="panel">
			<header class="panel-heading">
				<h2><?php echo __('Quiz'); ?></h2>
				<span class="tools pull-right">
					<a href="javascript:;" class="fa fa-chevron-down"></a>
					<!-- <a href="javascript:;" class="fa fa-times"></a> -->
				</span>
			</header>
			<div class="panel-body" style="display: block;">
				<div style="overflow-x: auto;">
					<table >

						<tr>
							<th>Id</th>
							<th>Quiz Title</th>
							<th>Course Title</th>
							<th>Lesson Title</th>
							<th>Description</th>
							<!-- <th>Duration</th> -->


							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>

						<?php  
						$count = 1;
						foreach ($quiz as $val):?>
						<tr>
							<td><?php echo h($count); ?></td>
							<td><?php echo h($val['Lecture']['title']); ?></td>
							<td><?php echo h($lesson['Lesson']['title']); ?></td>
							<td><?php echo h($lesson['Post']['post_title']); ?></td>
							<td><?php echo h($val['Lecture']['description']); ?></td>
							<!-- <td><?php echo h($val['Lecture']['duration']); ?></td> -->
							<td >
								<?php
								// echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'lectures' , 'action' => 'view', $val['Lecture']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
								?>
								<button type="button" class="btn btn-secondary btn-sm add_question" data-toggle="modal" data-target="#questions" data-id="<?php echo $val['Lecture']['id'];?>" >Add Question</button>
							</td>

						</tr>
						<?php 
						$count++;
						endforeach; ?>
					</table>
					<div class="paging">
						<span class="prev disabled">&lt; previous</span><span class="next disabled">next &gt;</span>                        
					</div>
				</div>
			</div>
		</div>
	</div>
</div>			


<script>
//---------- Add Quiz ------------//
$(document).on('click','.add-quiz', function(){
	var course_id = $("#hidden_post_id").val();
	var form_data = $(".section_form").serialize();
	var lesson_id_link = $("#hidden_lesson_id").val();
	$(this).prop('disabled', true);
	$.ajax({
		type: "POST",
		url: '<?php echo $this->webroot;?>lectures/ajaxAdminAddQuiz/'+course_id,
		dataType:'json',
		data:form_data, // serializes the form's elements.
		success: function(data)
		{
			alert('Quiz Has Been Saved Successfully');
			//location.reload();
			window.location.href="<?php echo $this->webroot;?>admin/lessons/view/"+lesson_id_link;
		}
	});
	return false;
});


//---------- Add Questions ------------//

$(document).on('click','.add_question', function(e) {
	var lecture_id = $(this).data('id');
	$("#quizID").val(lecture_id);
});

$(document).on('click','.quiz_ans', function() {
	var ans = $(this).siblings("textarea.message45").val();
	console.log(ans);
	$("#quiz_ans_option").val(ans);
});

$(document).on('submit','#add_questions', function(e) {
	e.preventDefault();
	var post_id = $("#hidden_post_id").val();
	var data = new FormData(this); // <-- 'this' is your form element
    var values = [];
    var key = 0;
	$(".message45").each(function() {
		if($(this).val()){
			values[key] = $(this).val();
	    	key = key+1;
		}
	});
    var $checked = $('#add_questions').find(":radio:checked");
       if($checked.length==1){
       	var ans = $("#quiz_ans_option").val();
		var qus = $("#question").val();
		var lecture_id = $("#quizID").val();
       	$.ajax({
	            url: "<?php echo $this->webroot;?>lectures/ajaxAdminAddQuistion/"+post_id,
	            data:{ops:values,ans:ans,qus:qus,quiz_id:lecture_id},
	            dataType:'json',
	            type:'POST',     
	            success: function(result){
	            	if(result.Ack==1){
	            		alert(result.res);
	            		location.reload();
	            	}
	            }
	    });
	   }else{
	   	 alert('Please,select one of the options');
	   }	
	return false;    
});

$(document).on('click','.message45:last',function () {
  $("#container-append").append('<div class="form-group"><label class="form-control-label" for="email">Option :</label><input type="radio" class="quiz_ans" name="ans[]" ><textarea name="data[Question][options][]" class="message45"></textarea></div>');
});

$('#questions').on('hidden.bs.modal', function () {
    $(this).find("input,textarea").val('').end();
    $(this).find("input:radio:checked").prop('checked',false);
    $("#container-append").html('');
});

</script>