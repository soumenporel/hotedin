<section class="profileedit">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
				
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="profile_second_part">
					<p class="pull-right course_setting">
					<span>
 <button type="button" class="btn btn-default">Manage Email Notifications</button>
 	</span>        
 						<span><i class="fa fa-question-circle" aria-hidden="true"></i>
<a href="#">help</a></span></p>
					<h2>Course Settings </h2>
					
					<p>The first step to creating a great course is deciding who you are creating your course for and what those students are looking to accomplish. This is important information that will help students decide if your course is the right fit for their needs and will appear on your course landing page. </p>
					<form>
					 	<div class="students_need">
					 		<p class="studentsnees">What will students need to know or do before starting this course?
					 		<span class="material">What materials/software do the students need and what actions do they have to perform before the course begins?</span></p>		
					 		<ul id="sortable_1" style="padding-left: 0px;">
					 			
					 			<?php 
					 			if(isset($courseGoals) && !empty($courseGoals)){
					 			foreach ($courseGoals->need_to_know as $value) { ?>
					 			<li class="need_to_know" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-1" placeholder="" aria-describedby="basic-addon2" value="<?php echo $value;?>"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:20px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:20px; right:20px; cursor:move;" aria-hidden="true"></i></li>
					 			<?php } }?>


					 		</ul>
        					<div class="input-group">
					 		  <input type="text" id="goal_1" class="form-control" placeholder="What will students need to know or do before starting this course?" aria-describedby="basic-addon2">
							  <span class="input-group-addon" id="basic-addon1" style="cursor: pointer;">Add</span>
							</div>
							<div class="error_field" id="error_1">This field cannot be blank.
					 		  </div>
					 	</div>
					 	
					 	<div class="students_need">
					 		<p class="studentsnees">Who is your target student?
					 		<span class="material">Think about your students prior experience level, background or current position and their needs, desires or problems.</span></p>
					 		<ul id="sortable_2" style="padding-left: 0px;">
					 			<?php 
					 			if(isset($courseGoals) && !empty($courseGoals)){
					 			foreach ($courseGoals->target_student as $key => $value) { ?>
					 			<li class="target_student" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-2" placeholder="" aria-describedby="basic-addon2" value="<?php echo $value;?>"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:20px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:20px; right:20px; cursor:move;" aria-hidden="true"></i></li>
					 			<?php } }?>
					 		</ul>		
					 		<div class="input-group">
					 		  <input type="text" id="goal_2" class="form-control" placeholder="Who is your target student?" aria-describedby="basic-addon2">
							  <span class="input-group-addon" id="basic-addon2" style="cursor: pointer;">Add</span>
							</div>
							<div class="error_field" id="error_2">This field cannot be blank.
					 		  </div>
					 	</div>
					 	<div class="students_need">
					 		<p class="studentsnees">At the end of my course, students will be able to...
					 		<span class="material">Start with a verb. Include details on specific skills students will learn and where students can apply them.</span></p>		
					 		<ul id="sortable_3" style="padding-left: 0px;">
					 			<?php
					 			if(isset($courseGoals) && !empty($courseGoals)){ 
					 			foreach ($courseGoals->will_be_able as $key => $value) { ?>
					 			<li class="will_be_able" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-3" placeholder="" aria-describedby="basic-addon2" value="<?php echo $value;?>"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:20px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:20px; right:20px; cursor:move;" aria-hidden="true"></i></li>
					 			<?php } }?>
					 		</ul>
					 		<div class="input-group">
					 		  <input type="text" id="goal_3" class="form-control" placeholder="At the end of my course, students will be able to..." aria-describedby="basic-addon2">
					 		  <span class="input-group-addon" id="basic-addon3" style="cursor: pointer;">Add</span>
					 		</div>
					 		<div class="error_field" id="error_3">This field cannot be blank.
					 		  </div>
					 	</div>
					 	<input type='hidden' id='course_slug' value='<?php echo $course_slug;?>'>
					 	<input type='hidden' id='course_id' value='<?php echo $course_id;?>'>
					 	<div class="bulk_uploader">
					 		<button type="button" class="btn btn-danger" id="save_button">Save</button>
					 	</div>
					 </form>
				</div>
						  
						  
					  
				</div>
		</div>
	</div>
</section>

