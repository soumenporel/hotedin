<style>
.header{
    position:relative;
}

.inner_header{
    background:#0c2440;
}


</style>

<section class="profileedit">
	<div class="container">
		<div class="row" style="background:#f6f6f6; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
			<div class="col-md-3 col-sm-3" style="padding:0">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
			</div>
			<div class="col-md-9 col-sm-9" style="padding:0; border-left:#ddd solid 1px;">
				<div class="profile_second_part automatic_messages">
					<p class="pull-right course_setting"><span><i class="fa fa-cog" aria-hidden="true"></i>
 <a href="<?php echo $this->webroot;?>posts/course_setting/<?php echo $post_dtls['Post']['slug'];?>">Course Settings</a></span>        
 						<!-- <span>
 							<i class="fa fa-question-circle" aria-hidden="true"></i>
							<a href="#">help</a>
						</span> -->
					</p>
					<h2>Price & Coupons</h2>
					<?php //echo '</pre>'; print_r($post_dtls); echo '</pre>'; ?>
					<p class="font-15">Select the currency and price of your course below and click 'save'. </p>
					<form class="form-horizontal price_and_c" action="<?php echo $this->webroot;?>posts/price_coupons/<?php echo $post_dtls['Post']['slug'];?>" method="post">
						  <div class="form-group">
						    <label class="control-label col-sm-3" for="text">Course Price<span>*</span></label>
						    <div class="col-sm-9">
								  <div class="select_box">
								  	<select class="selectpart" name="data[currency_type]" id="currencytype">
								  		<!-- <option value="$" <?php if($post_dtls['Post']['currency_type']=='$'){ echo 'selected';} ?> >USD</option> -->
								  		<option value="USD" <?php if($post_dtls['Post']['currency_type'] =='USD' || $symbolAray[0]->code=='USD'){ echo 'selected';} ?> >USD ( $ )</option>
								  		<option value="GBP" <?php if($post_dtls['Post']['currency_type'] =='GBP' || $symbolAray[0]->code=='GBP'){ echo 'selected';} ?> >GBP ( £ )</option>
								  		<option value="EUR" <?php if($post_dtls['Post']['currency_type'] =='EUR' || $symbolAray[0]->code=='EUR'){ echo 'selected';} ?> >EUR ( ‎€ )</option>
								  		<option value="CAD" <?php if($post_dtls['Post']['currency_type'] =='CAD' || $symbolAray[0]->code=='CAD'){ echo 'selected';} ?> >CAD ( C$ )</option>
								  		<option value="AUD" <?php if($post_dtls['Post']['currency_type'] =='AUD' || $symbolAray[0]->code=='AUD'){ echo 'selected';} ?> >AUD ( A$ )</option>
								  		<option value="INR" <?php if($post_dtls['Post']['currency_type'] =='INR' || $symbolAray[0]->code=='INR'){ echo 'selected';} ?> >INR ( ₹ )</option>
								   	</select>
								   	
							  		<select class="selectpart" name="data[price]" id="indcur">
							  			<?php
							  				if($symbolAray[0]->code=='USD' || $symbolAray[0]->code=='GBP' || $symbolAray[0]->code=='EUR' || $symbolAray[0]->code=='CAD' || $symbolAray[0]->code=='AUD' || $symbolAray[0]->code=='INR'){
							  				foreach ($prices as $price) { ?>
							  					<option value='<?php echo $price; ?>' <?php if($post_dtls['Post']['price']==$price){ echo 'selected'; }?> ><?php echo $price; ?></option>
							  				<?php } } else { ?>
							  				<?php for ($i=20; $i <= 200; $i = $i+5) { ?>
								  			<option value='<?php echo $i;?>' <?php if($post_dtls['Post']['price']==$i){ echo 'selected'; }?> ><?php echo $i; ?></option>
								  		<?php } } ?>
							  				
								  		<option <?php if($post_dtls['Post']['price']==0){ echo 'selected';}?> value='0' >Free</option>
							  		</select>								  	
								  </div>
								  <div class="select_box"></div>
						    </div>
						  </div>
					 	<div class="bulk_uploader">
					 		<div class="col-sm-9 col-sm-offset-3">
					 			<input type="submit" name="save" class="btn btn-danger" value="Save" >
					 				<!-- Save</button> -->
					 			<input type="submit" name="save-name" class="btn btn-danger" value="Save & Next" >
					 				<!-- Save</button> -->
					 		</div>
					 	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('#currencytype').on('change',function(){
		var curr = $(this).val();
		//alert(curr);

		$.ajax({
            method:'POST',
            url:'<?php echo $this->webroot; ?>posts/ajax_currency',
            dataType:'json',
            data:{cur:curr},
            success:function(res){
            	//console.log(res);
                $('#indcur').empty();
	            $.each(res, function(key, value) {   
				    $('#indcur').append($("<option></option>").attr("value",value).text(value)); 
				});
            }
        })
	})
</script>