<style>
/** Paging **/
.paging { color: #ccc; margin-top: 1em; clear: both; text-align: center; }
.paging .current, .paging .disabled, .paging a { text-decoration: none; padding: 5px 8px; display: inline-block }
.paging > span { display: inline-block; border: 1px solid #ccc; border-left: 0; }
.paging > span:hover { background: #efefef; }
.paging .prev { border-left: 1px solid #ccc; -moz-border-radius: 4px 0 0 4px; -webkit-border-radius: 4px 0 0 4px; border-radius: 4px 0 0 4px; }
.paging .next { -moz-border-radius: 0 4px 4px 0; -webkit-border-radius: 0 4px 4px 0; border-radius: 0 4px 4px 0; }
.paging .disabled { color: #ddd; }
.paging .disabled:hover { background: transparent; }
.paging .current { background: #efefef; color: #c73e14; }
.text { min-height: 146px; }
.owl-wrapper { width: auto !important; }
.owl-item { width: 19% !important; margin-left: 10px !important; }
ul#feature li, ul#feature2 li, ul#feature3 li, ul#feature4 li, ul#feature5 li, ul#feature6 li { margin-left: 0 !important; margin-right: 0 !important }
</style>
<?php 
    function custom_echo($x, $length)
    {
      if(strlen($x)<=$length)
      {
        echo $x;
      }
      else
      {
        $y=substr($x,0,$length) . '...';
        echo $y;
      }
    }

   // echo $this->Session->read('code'); 
?>
<section class="category_banner">
	<div class="inner_banner" style="background: url(<?php echo $this->webroot; ?>img/cat_img/<?php echo $categoryDetails['CategoryImage']['0']['originalpath']; ?>) no-repeat center center #000; background-size: cover; " >
		<div class="container">
          <div class="row">
            <div class="col-md-6">
			<p id="searchHeader"><?php echo $categoryDetails['Category']['category_name']; ?></p>
                        <!--<?php if(!empty($allPosts)){ ?>
                        <p class="inner_text"><?php echo $allPosts['0']['Post']['post_title']; ?>: <?php echo $allPosts['0']['Post']['post_subtitle']; ?></p>
                        <p class="padding-top-10"><?php custom_echo($allPosts['0']['Post']['post_description'],200); ?></p>
                        <p>By 
                            <strong><?php echo $allPosts['0']['User']['first_name'].' '.$allPosts['0']['User']['last_name']; ?></strong> 
                        </p>
            
                        <a href='<?php echo $this->webroot; ?><?php echo $allPosts['0']['Post']['slug']; ?>' class="btn btnPrimarys" style="padding:7px 20px 10px 20px !important; font-size:16px; margin-top:15px;">Explore Course</a>
                        <?php } ?>-->
                        <?php echo $categoryDetails['Category']['category_description']; ?>
		</div>
        </div>
        </div>
	</div>
</section>

<section class="courseFilterTab">
<div class="part_top" style="margin-bottom:0 !important; -webkit-box-shadow: 0 1px 2px #cbcbcb; box-shadow: 0 1px 2px #cbcbcb;">
		<div class="container" style="padding-left:30px; padding-right:30px;">
			<div class="row">
				<div class="col-md-12">
						
                        <form action="<?php echo Router::url(array('controller'=>'posts','action'=>'subcat_filter',$categoryDetails['Category']['slug'])); ?>" method="post" id="filterForm" >
                            <span>
                                Skill Level:
                                <select class="selectfield1" name='skill_level' id = "skill_level" >
                                    <option <?php if($skill_filter==''){ echo 'selected'; }?> value="0" > All Level </option>
                                    <option <?php if($skill_filter==1){ echo 'selected'; }?> value="1" > Begineer </option>
                                    <option <?php if($skill_filter==2){ echo 'selected'; }?> value="2" > Intermedite </option>
                                    <option <?php if($skill_filter==3){ echo 'selected'; }?> value="3" > Expert </option>
                                </select>
                            </span>
                            <span>
                                 Price: 
                                <select class="selectfield1" name="price" id = "price" >
                                    <option <?php if($price_filter==0){ echo 'selected'; }?> value="0" > Paid  </option>
                                    <option <?php if($price_filter==1){ echo 'selected'; }?> value="1" > Free  </option>
                                </select>
                            </span>
                            
                            <div class="pull-right viewpart">
                                <p class="pull-left">
                                    <span>Sort by:
                                        <select class="selectfield1" name='order_filter' id = "order_filter" >
                                            <option <?php if($order_filter==''){ echo 'selected'; }?> value="" > Select Option </option>
                                            <option <?php if($order_filter=='by_review'){ echo 'selected'; }?> value="by_review" > Most Reviewed </option>
                                            <option <?php if($order_filter=='by_rating'){ echo 'selected'; }?> value="by_rating" > Highest Rated </option>
                                            <option <?php if($order_filter=='by_date'){ echo 'selected'; }?> value="by_date" > Newest </option>
                                            <option <?php if($order_filter=='by_low_price'){ echo 'selected'; }?> value="by_low_price" > Price: Low to High </option>
                                            <option <?php if($order_filter=='by_high_price'){ echo 'selected'; }?> value="by_high_price" > Price: High to Low </option>
                                        </select>
                                    </span>
                                </p>
                            </div>
                        </form> 
				   
                   </div>
				</div>
			</div>
		</div>
	</div>
<div class="secNav">
 <div class="container">
  <nav class="navbar navbar-default">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo $this->webroot; ?>courses/<?php echo $parentCategories['Category']['slug']; ?>">
        <strong><?php echo $parentCategories['Category']['category_name'] ; ?></strong>
    </a>
    </div>
    <ul class="nav navbar-nav">
        <li><a style="color: #000;background: #f8d24b !important;" href="javascript:void(0)"><?php echo $categoryDetails['Category']['category_name']; ?></a></li>      
        <?php foreach ($subcatList as $key => $subcat) { ?>
            <li><a href="<?php echo $this->webroot; ?>courses/<?php echo $parentCategories['Category']['slug']; ?>/<?php echo $subcat['Category']['slug']; ?>"><?php echo $subcat['Category']['category_name']; ?></a></li>
        <?php } ?>
    </ul>
  </nav>
</div>
</div>	

 <style>
    .owl-item{width:19% !important; margin-left:10px !important;}
    ul#feature li, ul#feature2 li, ul#feature3 li, ul#feature4 li, ul#feature5 li, ul#feature6 li{margin-left:0 !important; margin-right:0 !important}
 </style>


 <div class="container" style="padding-left:30px; padding-right:30px;">
	<h4 class="text-uppercase h3">Best Sellers in <?php echo $categoryDetails['Category']['category_name']; ?></h4>
        <hr style="margin-top:0;">
        <div class="partners-in popular-courses slider2">
            <?php foreach ($allPosts as $key => $val) { ?>
                <div class="slide">
                    <div class="item hvr-float-shadow" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $val['Post']['slug']; ?>'">
                        <div class="img-holder">
                            <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $val['PostImage']['0']['originalpath']; ?>" style="height: 140px;" class="img-responsive" alt="" />
                        </div>
                        <div class="text">
                            <h4><a href="javascript:void(0);"><?php 
                                      if(strlen($val['Post']['post_title'])<=30)
                                      {
                                          echo $val['Post']['post_title'];
                                      }
                                      else
                                      {
                                          $y=substr($val['Post']['post_title'],0,30) . '...';
                                          echo $y;
                                      }
                            // echo $val['Post']['post_title']; 
                            ?></a></h4>
                          <!--  <div class="img img-circle">
                                <img src="<?php if (isset($val['User']['user_image']) && $val['User']['user_image'] != '') { ?><?php echo $this->webroot; ?>user_images/<?php
                                    echo $val['User']['user_image'];
                                } else {
                                    echo $this->webroot;
                                    ?>img/profile_img.jpg<?php } ?>" alt="Image"></div>-->
                            <?php
                            if (!empty($val['User'])) {
                            ?>
                            <h6><?php echo $val['User']['first_name'] . ' ' . $val['User']['last_name']; ?></h6>
                            <?php
                            }
                            ?>
                            <ul>
                                <?php 
                                        if(isset($val['Rating']) && $val['Rating']!=''){
                                            $b = count($val['Rating']);
                                        
                                            $a=0;
                                            foreach ($val['Rating'] as $value) {
                                                $a=$a + $value['ratting'];
                                            }
                                        }   
                                        $finalrating='';
                                        if($b!=0){
                                            $finalrating = ($a / $b);
                                        }

                                         if(isset($finalrating) && $finalrating!='') {  
                                            for($x=1;$x<=$finalrating;$x++) { ?>
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                            <?php }
                                            if (strpos($finalrating,'.')) {  ?>
                                                <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
                                            <?php  $x++;
                                            }
                                            while ($x<=5) { ?>
                                                <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <?php $x++;
                                            } 
                                          }else { ?>
                                                <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                          <?php } ?>
                                                       
                            </ul>
                            <!-- <p><?php echo $val['User']['email_address']; ?> </p> -->
                            <div class="clear-fix">
                                <ul class="float-left">
                                    <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li> -->
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $val['User']['address']; ?></li>
                                    <li>
                                        <a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                          <?php 
                                          if($val['Post']['price']==0){
                                            echo 'Free';  
                                          }else{
                                            $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($val['Post']['currency_type'],$val['Post']['price'])));
                                            echo $price['symbol'].round($price['price']); 
                                            //echo '$'.round($val['Post']['price']);  
                                          }  
                                           ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div> 
                    </div>
                </div>
            <?php } ?>
        </div>
 </div>


</section>
<section class="tabpart theme-bg-color">
  <div class="container">
	<?php 
        if(!empty($posts)){
        
            foreach ($posts as $key => $post) { ?>
            
                <div class="developmentRow" style="cursor:pointer;" onclick="javascript:window.location.href = '<?php echo $this->webroot; ?><?php echo $post['Post']['slug']; ?>'">
                    <div class="developmentBoxOne">
                       <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $post['PostImage']['0']['originalpath']; ?>" style="height: 185px;" alt="">
                    </div>
                    <div class="developmentBoxTwo" >
                        <div class="row">
                            <div class="col-sm-9 col-md-10">
                                <h3><?php echo $post['Post']['post_title']; ?></h3>
                                <a href="#"><?php echo $post['User']['first_name'].' '.$post['User']['last_name']; ?> • <?php echo substr(strip_tags($post['User']['biography']), 0, 200).'...'; ?>  </a>  
                                <p><?php echo $post['Post']['post_subtitle']; ?>
                                </p>
                                <p>
                                    <i class="fa fa-play-circle"></i> 
                                    <?php
                                        $noLecture = count($post['Lecture']);
                                        echo $noLecture; 
                                    ?> lectures&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <!-- <i class="fa fa-clock-o"></i> 55 7 hours&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <i class="fa fa-filter"></i> 55 lectures -->
                                </p>
                            </div>
                            <div class="col-sm-3 col-md-2">
                                
                                <h2 style="margin-top-30">
                                    <?php if($post['Post']['price']!=0){ ?>
                                        <strong><?php $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($post['Post']['currency_type'],$post['Post']['price'])));
                                            //echo '<pre>'; print_r($price); echo '</pre>';
                                            echo $price['symbol'].round($price['price']);  ?><!--$<?php echo $post['Post']['price']; ?>--></strong>
                                    <?php }else{ ?>
                                        <strong>Free</strong>
                                    <?php } ?>    
                                </h2>
                              
                                <p>
                                    <!-- <i class="fa fa-star" aria-hidden="true" style="color: green; padding-right:1px !important;"></i>
                                    <i class="fa fa-star" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <i class="fa fa-star" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <i class="fa fa-star-half-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i> -->
                                    <?php 
                                        if(isset($post['Rating']) && $post['Rating']!=''){
                                            $b = count($post['Rating']);
                                        
                                            $a=0;
                                            foreach ($post['Rating'] as $value) {
                                                $a=$a + $value['ratting'];
                                            }
                                        }   
                                        $finalrating='';
                                        if($b!=0){
                                            $finalrating = ($a / $b);
                                        }

                                         if(isset($finalrating) && $finalrating!='') {  
                                            for($x=1;$x<=$finalrating;$x++) { ?>
                                                <i class="fa fa-star" aria-hidden="true" style="color: green; padding-right:1px !important;"></i>
                                            <?php }
                                            if (strpos($finalrating,'.')) {  ?>
                                                <i class="fa fa-star-half-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                            <?php  $x++;
                                            }
                                            while ($x<=5) { ?>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                            <?php $x++;
                                            } ?>
                                            &nbsp;&nbsp;<?php echo $finalrating; ?>
                                        <?php     
                                          }else { ?>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                                <i class="fa fa-star-o" aria-hidden="true" style="color: green;padding-right:1px !important;"></i>
                                    <?php } ?> 
                                </p>
                                <p class="light-gray">
                                    <small>(
                                    <?php 
                                       $ratCount = count($post['Rating']);
                                       echo $ratCount; 
                                    ?> ratings)</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
      <?php } 
        }else{ ?>

            <p> No Courses. </p>
       
       <?php } ?>
    <div class="paging">
        <?php
            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>   
	
  </div>
</section>  

<script>

$(document).ready(function(){
    $(".selectfield1").change(function(){
        $("#filterForm").submit();
    });
});

</script>  
<script>
$(document).ready(function(){
  $('.slider2').bxSlider({
    slideWidth: 200,
    minSlides: 1,
    maxSlides: 5,
    slideMargin: 10
  });
});
</script>