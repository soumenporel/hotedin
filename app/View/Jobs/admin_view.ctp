<style>
.modal-header .close {
    margin-top: -10px;
}
</style>
<div class="categories view">
<h2><?php echo __('Job'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($post['Post']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($post['Post']['post_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($post['Post']['post_description']); ?>
			&nbsp;
		</dd>

		
		<dt><?php echo __('Image'); ?></dt>
		<dd>
                    <?php
                    if(isset($post['PostImage']['0']['originalpath']) and !empty($post['PostImage']['0']['originalpath']))
                    {
                    	$file = $post['PostImage']['0']['originalpath'];
                    	$ext = pathinfo($file, PATHINFO_EXTENSION);
                    	$extensionValid = array('mp4','flv','mkv');
                    	if(in_array(strtolower($ext), $extensionValid)){ 
                    		$file_name = trim($post['PostImage']['0']['originalpath'],$ext);
                    		?>     
                            <video width="400" controls>
							  <source src="<?php echo $this->webroot;?>img/post_img/<?php echo $post['PostImage']['0']['originalpath'];?>" type="video/<?php echo $ext; ?>">
							  <source src="<?php echo $this->webroot.'img/post_img/'.$file_name;?>ogg" type="video/ogg"> 
							  Your browser does not support HTML5 video.
							</video>
					<?php }
					      else{ ?>		
		                    <img alt="" src="<?php echo $this->webroot;?>img/post_img/<?php echo $post['PostImage']['0']['originalpath'];?>" style=" height:80px; width:80px;">
		                    <?php
		                    }
		            }
		            else{
		                    ?>
		                   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                   <?php } ?>
		</dd>
		<dt><?php echo __('Test Video'); ?></dt>
		<dd>
                    <?php
                    if(isset($post['TestVideo']['video']) and !empty($post['TestVideo']['video']))
                    {
                    	$file = $post['TestVideo']['video'];
                    	$ext = pathinfo($file, PATHINFO_EXTENSION);
                    	$extensionValid = array('mp4','flv','mkv');
                    	if(in_array(strtolower($ext), $extensionValid)){ 
                    		$file_name = trim($post['TestVideo']['video'],$ext);
                    		?>     
                            <video width="400" controls>
							  <source src="<?php echo $this->webroot;?>test_video/<?php echo $post['TestVideo']['video'];?>" type="video/<?php echo $ext; ?>">
							  <source src="<?php echo $this->webroot.'img/post_img/'.$file_name;?>ogg" type="video/ogg"> 
							  Your browser does not support HTML5 video.
							</video>
					<?php }
					      else{ ?>		
		                    <img alt="" src="<?php echo $this->webroot;?>img/post_img/<?php echo $post['PostImage']['0']['originalpath'];?>" style=" height:80px; width:80px;">
		                    <?php
		                    }
		            }
		            else{
		                    ?>
		                   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                   <?php } ?>
		</dd>
		<?php if(!empty($post['TestVideo']['video'])){ ?>
		<dt><?php echo __('Review Test Video'); ?></dt>
		<?php
		if(!$post['TestVideo']['status']==1){
			$val = 0;
		}else{
			$val = 1;
		}
		?>
		<input type="hidden" value="<?php echo $post['TestVideo']['id'];?>" id="test_video_id" >
		<input type="hidden" value="<?php echo $val;?>" id="test_video_status" >
		<label>
			Active This Video <input type="radio" name="test_video" <?php if($post['TestVideo']['status']==1){ echo "checked"; }?> class="review_video" value="1" > <br> Deactive This Video <input type="radio" name="test_video" <?php if($post['TestVideo']['status']==0){ echo "checked"; }?> class="review_video" value="0" >
 		</label>
 		<?php } ?>
		<dt><?php echo __('Approve'); ?></dt>
		<dd>
			<?php echo h($post['Post']['is_approve']==1?'No':'Yes'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Featured'); ?></dt>
		<dd>
			<?php echo h($post['Post']['featured']==1?'Yes':'No'); ?>
			&nbsp;
		</dd>

<!--		<dt><?php echo __('Post Views'); ?></dt>
		<dd>
			<?php echo h($post['Post']['click_count']); ?>
			&nbsp;
		</dd>-->
                
                <dt><?php echo __('No of Views'); ?></dt>
		<dd>
			<?php 
                         echo $totalView;
                        ?>
			&nbsp;
		</dd>
                
                <dt><?php echo __('No of Applicants'); ?></dt>
		<dd>
			<?php echo $totalStudents; ?>
			&nbsp;
		</dd>

	</dl>
</div>
<?php //echo $this->element('admin_sidebar'); ?>


<div class="modal fade" id="resModal" role="dialog">
	<div class="modal-dialog modal-sm">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	    </div>
	    <div class="modal-body">
	      <p id="ajaxResponse"></p>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    </div>
	  </div>
	</div>
</div>

<script>
$(document).ready(function(){
    $(".review_video").click(function(){
        var current_stat = $(this).val();
        var stat = $("#test_video_status").val();
        var video_id = $("#test_video_id").val();
        if(stat!=current_stat){
        	$.ajax({
	            url: "<?php echo $this->webroot;?>test_videos/ajaxTestVideoStatus",
	            data:{video_id:video_id,status:current_stat},
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	if(result.Ack==1){
	            		$("#test_video_status").val(result.stat);
	            		$('#ajaxResponse').text(result.mas);
	            		$('#resModal').modal('show');

	            	}
	            }
	    	});
        }
    });
});
</script>

