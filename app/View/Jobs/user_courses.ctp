<style>
/*.coursename {
    min-height: 377px;*/
}
</style>
<section class="profileedit">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                 <!-- left panel !-->
                   <?php echo $this->element('leftpanel'); ?> 
                
            </div>
            <div class="col-md-9 col-sm-9">
                <div class="profile_second_part">
                    <p class="pull-right course_setting">
                      <span><i class="fa fa-cog" aria-hidden="true"></i><a href="#">Course Settings</a></span>        
                        <span><i class="fa fa-question-circle" aria-hidden="true"></i><a href="#">help</a></span></p>
                    <h2>Your Posted course </h2>
                    <div class="row">
                    <?php foreach ($courses as $key => $course) { ?>    
                        <div class="col-md-4 m-b-24">
                            <div class="coursename" style="cursor:pointer;" onclick="javascript:window. location.href='<?php echo $this->webroot; ?>posts/course_goals/<?php echo $course['Post']['slug']; ?>'">
                                <div class="courseimage"><img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $course['PostImage']['0']['originalpath']; ?>"></div>
                                <div class="course_bottom_part">
                                    <div class="name-p"><?php echo $course['Post']['post_title']; ?></div>
                                    <div class="course_para"><p>
                                        <?php
                                            if($course['Post']['post_description']!=''){
                                                if (strlen($course['Post']['post_description']) > 100)
                                                 {
                                                     $stringCut = substr($course['Post']['post_description'], 0, 100);
                                                     $string = substr($stringCut, 0, strrpos($stringCut, ' '))."....";
                                                 }
                                                 else
                                                 {
                                                     $string=$course['Post']['post_description'];
                                                 }

                                                     echo $string; 
                                            } 
                                        ?>
                                    </div>
                                    <?php
                                    if(!empty($course['User']))
                                    {
                                    ?>
                                      <div class="writer_name">By <?php echo $course['User']['first_name'].' '.$course['User']['last_name']; ?></div>
                                    <?php
                                    }
                                    ?>
                                    <div class="rating">Rating: <img src="<?php echo $this->webroot; ?>img/star.png"></div>
                               
                                    <div class="price">
                                        <div class="pull-left"><span class="pricenin">$<?php echo round($course['Post']['price']); ?></span> <?php if(isset($course['Post']['old_price']) && $course['Post']['old_price']!=''){ ?> <span class="doller25">$<?php echo $course['Post']['old_price']; ?></span><?php } ?>
                                        </div>
                                        <div class="pull-right"><span class="enrolled"><img src="<?php echo $this->webroot; ?>img/groupusers.png">57.2k Enrolled</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>    
                    </div>  
                </div>
            </div>
        </div>
    </div>    
</section>

