<?php //pr($course_status); ?>
<style>
.error_field{ 
	visibility: hidden;
	color: #d23837; 
}

.header{
    position:relative;
}

.inner_header{
    background:#0c2440;
}

</style>
<section class="profileedit">
	<div class="container">
		<div class="row" style="background: #f6f6f6; border-right: #ddd solid 1px; border-left: #ddd solid 1px;">
			<div class="col-md-3 col-sm-3" style="padding: 0;">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
				
			</div>
			<div class="col-md-9 col-sm-9" style="padding: 0; border-left:#ddd solid 1px; border-bottom:#ddd solid 1px; ">
				<div class="profile_second_part">
					<?php echo $topContent['CmsPage']['page_description']; ?>
						
					 		<ul id="sortable_1" style="padding-left: 0px;">
					 			
					 			<?php 
					 			if(isset($courseGoals) && !empty($courseGoals)){
					 			foreach ($courseGoals->need_to_know as $value) { ?>
					 			<li class="need_to_know" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-1 mb-2" placeholder="" aria-describedby="basic-addon2" value="<?php echo $value;?>"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:8px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:8px; right:20px; cursor:move;" aria-hidden="true"></i></li>
					 			<?php } }?>


					 		</ul>
        					<div class="input-group">
					 		  <input type="text" id="goal_1" class="form-control" placeholder=" " aria-describedby="basic-addon2">
							  <span class="input-group-addon" id="basic-addon1" style="cursor: pointer;">Add</span>
							</div>
							<div class="error_field" id="error_1">This field cannot be blank.
					 		  </div>
					 	</div>
					 	
					 	<div class="students_need">
					 		<?php echo $midContent['CmsPage']['page_description']; ?>
					 		<ul id="sortable_2" style="padding-left: 0px;">
					 			<?php 
					 			if(isset($courseGoals) && !empty($courseGoals)){
					 			foreach ($courseGoals->target_student as $key => $value) { ?>
					 			<li class="target_student" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-2 mb-2" placeholder="" aria-describedby="basic-addon2" value="<?php echo $value;?>"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:8px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:8px; right:20px; cursor:move;" aria-hidden="true"></i></li>
					 			<?php } }?>
					 		</ul>		
					 		<div class="input-group">
					 		  <input type="text" id="goal_2" class="form-control" placeholder=" " aria-describedby="basic-addon2">
							  <span class="input-group-addon" id="basic-addon2" style="cursor: pointer;">Add</span>
							</div>
							<div class="error_field" id="error_2">This field cannot be blank.
					 		  </div>
					 	</div>
					 	<div class="students_need">
					 		<?php echo $bottomContent['CmsPage']['page_description']; ?>	
					 		<ul id="sortable_3" style="padding-left: 0px;">
					 			<?php
					 			if(isset($courseGoals) && !empty($courseGoals)){ 
					 			foreach ($courseGoals->will_be_able as $key => $value) { ?>
					 			<li class="will_be_able" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-3 mb-2" placeholder="" aria-describedby="basic-addon2" value="<?php echo $value;?>"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:8px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:8px; right:20px; cursor:move;" aria-hidden="true"></i></li>
					 			<?php } }?>
					 		</ul>
					 		<div class="input-group">
					 		  <input type="text" id="goal_3" class="form-control" placeholder=" " aria-describedby="basic-addon2">
					 		  <span class="input-group-addon" id="basic-addon3" style="cursor: pointer;">Add</span>
					 		</div>
					 		<div class="error_field" id="error_3">This field cannot be blank.
					 		  </div>
					 	</div>
					 	<input type='hidden' id='course_slug' value='<?php echo $course_slug;?>'>
					 	<input type='hidden' id='course_id' value='<?php echo $course_id;?>'>
					 	<div class="bulk_uploader">
					 		<button type="button" class="btn btn-danger save_button" data-type="save" >Save</button>
					 		<button type="button" class="btn btn-danger save_button" data-type="save-next" >Save & Next</button>
					 	</div>
					 </form>
				</div>
						  
						  
					  
				</div>
		</div>
	</div>
</section>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#sortable_1,#sortable_2,#sortable_3" ).sortable({
    	cursor: "move"
    });
    $( "#sortable_1,#sortable_2,#sortable_3" ).disableSelection();
  } );

$("#basic-addon1").click(function () {
var data = $("#goal_1").val();
 if(data!='' && data!=' '){
   $("#sortable_1").append('<li class="need_to_know" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-1" placeholder="" aria-describedby="basic-addon2" value="'+data+'"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:20px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:20px; right:20px; cursor:move;" aria-hidden="true"></i></li>');
   var data = $("#goal_1").val('');
  }
});
$("#basic-addon2").click(function () {
var data = $("#goal_2").val();
 if(data!='' && data!=' '){
   $("#sortable_2").append('<li class="target_student" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-2" placeholder="" aria-describedby="basic-addon2" value="'+data+'"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:20px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:20px; right:20px; cursor:move;" aria-hidden="true"></i></li>');
   var data = $("#goal_2").val('');
  }
});
$("#basic-addon3").click(function () {
var data = $("#goal_3").val();
 if(data!='' && data!=' '){
   $("#sortable_3").append('<li class="will_be_able" style="list-style: none; position:relative;"><span class=""></span><input type="text" style="padding-right: 68px; margin-top: 9px;" class="form-control text-field-3" placeholder="" aria-describedby="basic-addon2" value="'+data+'"><i class="fa fa-trash-o delete" aria-hidden="true" style="position:absolute; top:20px; right:45px; cursor:pointer"></i><i class="fa fa-bars" style="position:absolute; top:20px; right:20px; cursor:move;" aria-hidden="true"></i></li>');
   var data = $("#goal_3").val('');
  }
});

$(document).on('click','.delete', function(){
	$(this).closest("li").remove();
});

$(document).on('click','.save_button', function(){
	var need_to_know = [];
	var target_student = [];
	var will_be_able = [];

	$(".need_to_know .text-field-1").each(function() {
	    need_to_know.push($(this).val());
	});

	$(".target_student .text-field-2").each(function() {
	    target_student.push($(this).val());
	});

	$(".will_be_able .text-field-3").each(function() {
	    will_be_able.push($(this).val());
	});

    var length_1 = need_to_know.length;
    var length_2 = target_student.length;
    var length_3 = will_be_able.length;
    
    if(length_1==0 || length_2==0 || length_3==0){
	    if(length_1==0){
	    	$("#error_1").css("visibility","visible");
	    }
	    else{
	    	$("#error_1").css("visibility","hidden");	
	    }

	    if(length_2==0){
	    	$("#error_2").css("visibility","visible");
	    }
	    else{
	    	$("#error_2").css("visibility","hidden");	
	    }

	    if(length_3==0){
	    	$("#error_3").css("visibility","visible");
	    }
	    else{
	    	$("#error_3").css("visibility","hidden");	
	    }
	}
	else{
		$("#error_1").css("visibility","hidden");
		$("#error_2").css("visibility","hidden");
		$("#error_3").css("visibility","hidden");
		var post_id = $("#course_id").val();
		var course_slug = $("#course_slug").val();
		
		var URL = '<?php echo $this->webroot;?>posts/course_goals/'+course_slug;
		
		var saveType = $(this).data('type'); 
		if(saveType=='save-next'){
			URL = '<?php echo $this->webroot;?>posts/test_video/'+course_slug;
		}
		
		$.ajax({
			url: "<?php echo $this->webroot;?>posts/ajaxAddGoals",
			method: "POST",
			dataType:'json',
			data:{
				  need_to_know:need_to_know, 
				  target_student:target_student,
				  will_be_able:will_be_able,
				  post_id:post_id 
				 }, 
			success: function(result){
				//console.log(result);
        		if(result.Ack==1){
        			window.location.href = URL;
        		}
    		}
    	}); 
	}    

});
</script>