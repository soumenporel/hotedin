<style>
#sortable1, #sortable2 {
	border: 1px solid #eee;

	list-style-type: none;
	margin: 0;

}
#sortable1 li, #sortable2 li {
	margin: 0 1px 1px 1px;
	padding: 10px;

}
.ui-sortable{
	/*display: none;*/
	list-style-type:none; 
}
.cancel_button .btn-primary {
    
    margin-right: 7px;
}
.fa-pencil{
	 margin-left: 6px;
    margin-right: 5px;
    display: none;
}
.fa-trash{
	display: none;
}

.fa-check-circle{
    color: green;
}
.lecturer {
    margin-top: 8px;
}
.fa-chevron-down{
	cursor: pointer;
}

</style>
<?php //pr($lessons); ?>
<section class="profileedit">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="profile_second_part curriculum_part">
					<p class="pull-right course_setting"><span><i class="fa fa-cog" aria-hidden="true"></i>
 <a href="#">Course Settings</a></span>        
 						<span><i class="fa fa-question-circle" aria-hidden="true"></i>
<a href="#">help</a></span></p>
					<h2>Curriculum</h2>
					<p>Add information about yourself to share on your profile.</p>
									
					<div class="cfi-sortables-list">
					<ul id="sortable">

						<?php 
						$serial_no = 1;
						foreach ($lessons as $key => $lesson) { ?>
						<li id="<?php echo $lesson['Lesson']['id'];?>" data-id="<?php echo $lesson['Lesson']['id'];?>" class="ui-state-default">
							<div id="id" class="ui-sortable-1">
								<div class="first_section">
							 		<span class="section">Section <?php echo $serial_no;?> : </span>
							 		<span class="sectiontext" id="lesson_title-<?php echo $lesson['Lesson']['id'];?>" ><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo $lesson['Lesson']['title'];?> <i class="fa fa-pencil edit-lesson" style="cursor: pointer;" > </i><i class="fa fa-trash" style="cursor: pointer;" ></i></span>	
							 		<span class="action_buttons pull-right"><i class="fa fa-bars" style="cursor: move;" ></i></span>
							 	</div>
								<!-- <div class="second_section"> -->
								<ul id="sortable1" class="connectedSortable">
									<?php 
									$lecture_no = 1;
									foreach ($lesson['Lecture'] as $key => $value) {
										if($value['type']==0){	
									?>
									<li id="<?php echo $value['id'];?>" data-id="<?php echo $lesson['Lesson']['id'];?>-<?php echo $value['id'];?>" class="ui-state-default"> 
								 		<div class="fileupload">
								 			<div class="row">
								 				<div class="col-md-6">	
								 					<div class="lecturer"><i class="fa fa-check-circle" aria-hidden="true"></i> Lecture <?php echo $lecture_no;?> :</label><span class="introduction" id="lecture_title-<?php echo $value['id'];?>" ><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo $value['title'];?><i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash delete-lecture" style="cursor: pointer;"></i></span>
								 					</div>
								 				</div>
								 				<div class="col-md-6">
										 			<div class="upload_part pull-right"><button id="add_lecture_media" type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-plus"></i>
													Add Content</button><i class="fa fa-chevron-down pull-right"></i>
													</div>

								 				</div>
								 			</div>
								 		</div>
								 	</li>
								 	<?php
								 		}
								 		if($value['type']==1){ ?>
								 	<li id="<?php echo $value['id'];?>" data-id="<?php echo $lesson['Lesson']['id'];?>-<?php echo $value['id'];?>" class="ui-state-default"> 
								 		<div class="fileupload"  >
								 			<div class="row" >
								 				<div class="col-md-6">	
								 					<div class="lecturer"><i class="fa fa-check-circle" aria-hidden="true"></i> Quiz <?php echo $lecture_no;?> :</label><span class="introduction" id="lecture_title-<?php echo $value['id'];?>" ><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo $value['title'];?><i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash delete-lecture" style="cursor: pointer;"></i></span>
								 					</div>
								 				</div>
								 				<?php if(empty($value['Question'])){ ?>
								 				<div class="col-md-6">
										 			<div class="upload_part pull-right">
										 				<button type="button" data-toggle="modal" data-target="#questions" class="btn btn-primary add_quiz_question"><i class="fa fa-plus"></i>
													 Add Questions</button>
													</div>
												<?php }
												else{ ?>
													<i class="fa fa-chevron-down pull-right"></i>
												<?php  }
												?>
											</div>
										
								 		</div>
								 	</li>	
								 	<?php 
								 		}
								 	$lecture_no = $lecture_no+1;
								 	} ?>
								</ul>  	
								<!-- </div> -->
							</div>
						</li>
						<?php
						$serial_no = $serial_no+1; 
						} ?>

					</ul>
					</div>
					<input type="hidden" id="course_id" value="<?php echo $post_dtls['Post']['id'];?>" >
					
					 <div class="row">
					 <ul class="playingarea" style="display:none;">
					 </ul>
					 		<ul class="lecture">
					 			<li class="add_menu" id="add_lecture"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Lecture</a></li>
					 			<li class="add_menu" id="add_quiz"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Quiz</a></li>
					 			<li class="add_menu" id="add_coding"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Coding</a></li>
					 			<li class="add_menu" id="add_section"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Section</a></li>
					 		</ul>
					 	</div>
					 	<div class="bulk_uploader">
					 		<button type="button" class="btn btn-primary">Bulk Uploader</button>
					 		<button type="button" class="btn btn-danger">Preview</button>
					 	</div>
				</div>
				
			</div>
		</div>
	</div>
</section>
<!--Model-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Lecture Content</h4>
        </div>
        <div class="modal-body">
	         <form class="form-horizontal" id="lecture_content" method="post" enctype="multipart/form-data">
				  <div class="form-group row">
				    <label class="control-label col-sm-2" for="email">Lecture :</label>
				    <div class="col-sm-10">
				      <input type="file" class="form-control" name="data[Lecture][media]" id="add_content" placeholder="Add Conten">
				       <input type="hidden" id="lectureID" >
				    </div>
				  </div>
				
				  <div class="form-group row"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-default">Submit</button>
				    </div>
				  </div>
			</form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
</div>

<!--Edit Section Modal-->
<div class="modal fade" id="sectionModal" role="dialog">
    <div class="modal-dialog">
    
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Edit Section Content</h4>
    </div>
    <div class="modal-body">
         <form class="form-horizontal" id="edit_lesson" method="post" enctype="multipart/form-data">
			  <div class="form-group row">
			    <label class="control-label col-sm-2" for="email">Section Title:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="data[Lesson][title]" id="lesson_title" placeholder="Add Conten">
			       <input type="hidden" id="lessonID" >
			    </div>
			  </div>
			  <div class="form-group row">
			    <label class="control-label col-sm-2" for="email">Section Description:</label>
			    <div class="col-sm-10">
			      <textarea name="data[Lesson][description]" id="lesson_description" ></textarea>
			    </div>
			  </div>
				
			  <div class="form-group row"> 
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Submit</button>
			    </div>
			  </div>
		</form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
	</div>
</div>

<!--Add Questions Modal-->
<div class="modal fade" id="questions" role="dialog">
    <div class="modal-dialog">
    
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Add Quiz Question</h4>
    </div>
    <div class="modal-body">
         <form class="form-horizontal" id="add_questions" method="post" enctype="multipart/form-data">
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email">Question :</label>
			    <div class="col-sm-10">
			      <textarea name="data[Question][question]" id="question" required ></textarea>
			       <input type="hidden" name='data[lecture_id]' id="quizID" >
			       <input type="hidden" name="data[quiz_ans]" id="quiz_ans_option" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email">Option 1:</label>
			    <div class="col-sm-10">
			      <input type="radio" class="quiz_ans" name='ans[]' >
			      <textarea name="data[Question][options][]" class="options" ></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email">Option 2:</label>
			    <div class="col-sm-10">
			      <input type="radio" class="quiz_ans" name='ans[]' >
			      <textarea name="data[Question][options][]" class="options" ></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email">Option 3:</label>
			    <div class="col-sm-10">
			      <input type="radio" class="quiz_ans" name='ans[]' >
			      <textarea name="data[Question][options][]" class="options" ></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email">Option 4:</label>
			    <div class="col-sm-10">
			      <input type="radio" class="quiz_ans" name='ans[]' >
			      <textarea name="data[Question][options][]" class="options" ></textarea>
			    </div>
			  </div>
				
			  <div class="form-group"> 
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Submit</button>
			    </div>
			  </div>
		</form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
	</div>
</div>

<!--Edit Lecture Modal-->
<div class="modal fade" id="lectureModal" role="dialog">
    <div class="modal-dialog">
    
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Edit  Content</h4>
    </div>
    <div class="modal-body">
         <form class="form-horizontal" id="edit_lecture" method="post" enctype="multipart/form-data">
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email"> Title:</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="data[Lecture][title]" id="lecture_title" placeholder="Add Conten">
			       <input type="hidden" id="lectureID" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label col-sm-2" for="email"> Description:</label>
			    <div class="col-sm-10">
			      <textarea name="data[Lecture][description]" id="lecture_description" ></textarea>
			    </div>
			  </div>
				
			  <div class="form-group"> 
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Submit</button>
			    </div>
			  </div>
		</form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
	</div>
</div>

<!-- Small Model -->
<div class="modal fade" id="resModal" role="dialog">
	<div class="modal-dialog modal-sm">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	    </div>
	    <div class="modal-body">
	      <p id="ajaxResponse"></p>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	    </div>
	  </div>
	</div>
</div>

  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
function helper(root) {
  var result = {};
         
  $('> ul > li > span', root).each(function () {
    result[$(this).text()] = $(this).hasClass('ui-state-default') ? helper($(this).parent()) : $(this).next('input').val();
  });

  return result;
}

console.log(helper('body'));
(function($) {
    $.fn.serial = function() {
        var array = [];
        var $elem = $(this);
        $elem.each(function(i) {
            var menu = this.id;
            $('li', this).each(function(e) {
                array.push(menu + '[' + e + ']=' + this.id);
            });
        });
        return array.join('&');
    }
})(jQuery);
$( function() {
	$( "#sortable1, #sortable2" ).sortable({
		placeholder: "ui-state-default",
	  	connectWith: ".connectedSortable",
	  update: function(event, ui) {
	    var ID = $( "#sortable" ).attr("id");
        update_sprint_status("progress", ID);
       }  
	}).disableSelection();
});
$( function() {
   $( "#sortable" ).sortable({
   	update: function(event, ui) {
         var ID = $( "#sortable" ).attr("id");
         update_sprint_status("progress", ID);
        } 
    });
   $( "#sortable" ).disableSelection();
});



$(document).on('click','#add_section', function(){
	$(".playingarea").html('');
	var tmp = '<div class="ui-sortable sortable_part"><form class="section_form" method="post"><div class="row"><label class="col-md-2 lecture_label">New Section</label><span class="col-md-10"><input id="text" name = data[section][name] class="form-control form_part form_text" required placeholder="Enter a title" type="text"><div class="number">75</div></span><label class="col-md-2 lecture_label"></label><label class="col-md-10 lecture_label">What will students be able to do at the end of this section?</label><label class="col-md-2 lecture_label"></label><span class="col-md-10"><input id="text" name = data[section][objective] required class="form-control form_part form_text" placeholder="Enter A Learning Objective" type="text"><div class="number">75</div></span></div><div class="pull-right cancel_button"><button type="button" class="btn btn-primary cancel-section">Cancel</button><button type="submit" class="btn btn-primary add-section" style="margin-left:10px;">Add Section</button></div><div class="clearfix"></div></form></div>';
	$(".playingarea").append(tmp);
	$('.playingarea').show();
	$("#add_section").hide();
});

$(document).on('click','.cancel-section', function(){
	//$(".playingarea").html('');
	//var HTML = '<li class="add_menu" id="add_lecture"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Lecture</a></li><li class="add_menu" id="add_quiz"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Quiz</a></li><li class="add_menu" id="add_coding"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Coding</a></li><li class="add_menu" id="add_section"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Section</a></li>';
	//$(".playingarea").append(HTML);
	$(".playingarea").html('');
	$('.playingarea').hide();
	$("#add_section").show();
});

$(document).on('submit','.section_form', function(){
	//console.log($(".section_form").serialize());
	var course_id = $("#course_id").val();
	var form_data = $(".section_form").serialize();
	$.ajax({
       type: "POST",
       url: '<?php echo $this->webroot;?>lessons/ajaxAddSection/'+course_id,
       dataType:'json',
       data:form_data, // serializes the form's elements.
       success: function(data)
       {
           if(data.Ack==1){
	   $("#sortable").html('');
           		//var HTML = '<li class="ui-state-default"><div id="id1" class="ui-sortable-1"><div class="first_section"><span class="section">Unpublished Section : </span><span class="sectiontext">New Class <i class="fa fa-pencil"> </i><i class="fa fa-trash"></i></span></span><span class="action_buttons pull-right"><i class="fa fa-bars"></i></span></div><ul id="sortable1" class="connectedSortable"></ul></div></li>';
           		$("#sortable").html(data.html);

           		$( "#sortable1, #sortable2" ).sortable({
					placeholder: "ui-state-default",
				  	connectWith: ".connectedSortable",
				  	update: function(event, ui) {
					    var ID = $( "#sortable" ).attr("id");
				        update_sprint_status("progress", ID);
			       	}  
				}).disableSelection();
				$( "#sortable" ).sortable({
				   	update: function(event, ui) {
				         var ID = $( "#sortable" ).attr("id");
				         update_sprint_status("progress", ID);
				   	} 
				});
				$( "#sortable" ).disableSelection();

           		$(".playingarea").html('');
	$('.playingarea').hide();
	$("#add_section").show();
           }
       }
    });
	return false;
});

$(document).on('click','#add_lecture', function(){
	$(".lecture").html('');
	var tmp = '<div class="ui-sortable sortable_part"><form class="lecture_form" method="post"><div class="row"><label class="col-md-2 lecture_label">New Lecture</label><span class="col-md-10"><input id="text" name=data[lecture][title] class="form-control form_part form_text" placeholder="Enter a title" type="text"><div class="number">75</div></span></div><div class="pull-right cancel_button"><button type="button" class="btn btn-primary cancel-lecture">Cancel</button><button type="submit" class="btn btn-primary">Add Lecture</button></div><div class="clearfix"></div></form></div>';
	$(".lecture").append(tmp);
});

$(document).on('click','.cancel-lecture', function(){
	$(".lecture").html('');
	var HTML = '<li class="add_menu" id="add_lecture"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Lecture</a></li><li class="add_menu" id="add_quiz"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Quiz</a></li><li class="add_menu" id="add_coding"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Coding</a></li><li class="add_menu" id="add_section"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Section</a></li>';
	$(".lecture").append(HTML);
});

$(document).on('submit','.lecture', function(){
	//console.log($(".lecture_form").serialize());
	var course_id = $("#course_id").val();
	var form_data = $(".lecture_form").serialize();
	$.ajax({
       type: "POST",
       url: '<?php echo $this->webroot;?>lectures/ajaxAddLecture/'+course_id,
       dataType:'json',
       data:form_data, // serializes the form's elements.
       success: function(data)
       {
           if(data.Ack==1){			
           		$("#sortable").html('');
				$("#sortable").html(data.html);

           		$( "#sortable1, #sortable2" ).sortable({
					placeholder: "ui-state-default",
				  	connectWith: ".connectedSortable",
				  	update: function(event, ui) {
					    var ID = $( "#sortable" ).attr("id");
				        update_sprint_status("progress", ID);
			       	}  
				}).disableSelection();
				$( "#sortable" ).sortable({
				   	update: function(event, ui) {
				         var ID = $( "#sortable" ).attr("id");
				         update_sprint_status("progress", ID);
				   	} 
				});
				$( "#sortable" ).disableSelection();

           		$(".lecture").html('');
				var HTML = '<li class="add_menu" id="add_lecture"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Lecture</a></li><li class="add_menu" id="add_quiz"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Quiz</a></li><li class="add_menu" id="add_coding"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Coding</a></li><li class="add_menu" id="add_section"><a href="#" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Section</a></li>';
				$(".lecture").append(HTML);
           }
       }
    });
	return false;
});

// $(function() {
// $('#sortable').sortable({
//     start : function(event, ui) {
//         var start_pos = ui.item.index();
//         ui.item.data('start_pos', start_pos);
//     },
//     update : function(event, ui) {
// 		var index = ui.item.index();
//         var start_pos = ui.item.data('start_pos');
        
//         //update the html of the moved item to the current index
// 		$('#sortable li:nth-child(' + (index + 1) + ')').html(index);
        
//         if (start_pos < index) {
//             //update the items before the re-ordered item
//             for(var i=index; i > 0; i--){
//                 $('#sortable li:nth-child(' + i + ')').html(i - 1);
//             }
//         }else {
//             //update the items after the re-ordered item
//             for(var i=index+2;i <= $("#sortable li").length; i++){
//                 $('#sortable li:nth-child(' + i + ')').html(i-1);
//             }
//         }
//     },
// 	axis : 'y'
// });
// });  
</script>


<script>
$(function() {

  function buildJSON($li) {
  	var res = $li;
  	var subObj = { "name": $li.contents().eq(0).text().trim() };
    $li.children('ul').children().each(function() {
      if (!subObj.children) { subObj.children = []; }
      subObj.children.push(buildJSON($(this)));
    });
    return subObj;
  }
    
  var obj = buildJSON($("#sortable").children());
  //$('body').append('<pre>').find('pre').append(JSON.stringify(obj, null, 2));

  console.log(JSON.stringify(obj, null, 2));
  
});
// $( "#id_sprint_name" ).sortable({
//        connectWith: ".connectedExchange",
//        cursor: 'move',
//        placeholder: "ui-state-highlight",
//        forcePlaceholderSize: true,
//        update: function(event, ui) {
//           console.log("sprint name sortable");
//           var ID = $( "#id_sprint_name" ).attr("id");
//           update_sprint_status("default", ID);
//         } 
//     }).disableSelection(); 
    
// $( "#id_sprint_progress" ).sortable({
//        connectWith: ".connectedExchange",
//        cursor: 'move',
//        placeholder: "ui-state-highlight",
//        forcePlaceholderSize: true,
//        update: function(event, ui) {
//          console.log("sprint progress sortable");
//          var ID = $( "#id_sprint_progress" ).attr("id");
//          update_sprint_status("progress", ID);
//         } 
//     }).disableSelection(); 

// $( "#id_sprint_verification" ).sortable({
//        connectWith: ".connectedExchange",
//        cursor: 'move',
//        placeholder: "ui-state-highlight",
//        forcePlaceholderSize: true,
//        update: function(event, ui) {
//           console.log("sprint verification sortable");
//           var ID = $( "#id_sprint_verification" ).attr("id");
//           update_sprint_status("verification", ID);
//         } 
//     }).disableSelection(); 

// $( "#id_sprint_completed" ).sortable({
//        connectWith: ".connectedExchange",
//        cursor: 'move',
//        placeholder: "ui-state-highlight",
//        forcePlaceholderSize: true,
//        update: function(event, ui) {
//          console.log("sprint completed sortable");
//          var ID = $( "#id_sprint_completed" ).attr("id");
//          update_sprint_status("completed", ID);
//         } 
//     }).disableSelection();     
    

function update_sprint_status(sprint_type, ulid) {
var arr_sprints = [];
var ns_this = $( "#"+ulid).find( ".ui-state-default" );
        ns_this.each(function( index ) {
               //alert($(this).attr('data-id')); 
               arr_sprints.push($(this).attr('data-id')); 
         });  
        
        console.log(arr_sprints) ;
        var checklisting_id = isArray(arr_sprints);
       if(checklisting_id === true) {  
              
       var arr_sprints_json = JSON.stringify(arr_sprints); 
       $.ajax({
         url : '<?php echo $this->webroot;?>lessons/sprint_status_order/',
       type: "POST",
      data : { arr_sprints_id : arr_sprints_json},
       success: function(data)
       {
        if(data) {
        console.log(data);
      }
       }
                });  
			 } 
        arr_sprints = [];

}

function isArray(x) {
    return x.constructor.toString().indexOf("Array") > -1;
}

//----------Adding lecture content-------//

$(document).on('click','#add_lecture_media', function(e) {
	var lecture_id = $(this).closest("li").data('id');
	$("#lectureID").val(lecture_id);
});
	
$(document).on('submit','#lecture_content', function(e) {
	e.preventDefault();
	var post_id = $("#course_id").val();
	var lecture_id = $("#lectureID").val();
	var res = lecture_id.split("-");
	var lesson_id = res['0'];
	var lecture_id = res['1'];
    var data = new FormData(this); // <-- 'this' is your form element

	    $.ajax({
	            url: "<?php echo $this->webroot;?>lectures/ajaxAddLectureContent/"+lecture_id,
	            data:data,
	            cache: false,
	            contentType: false,
	            processData: false,
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	if(result.Ack==1){
	            		$('#myModal').modal('hide');
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            	else{
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            }
	    });
	return false;    
});

//-------Edit Section Content -----//
$(document).on('click','.edit-lesson', function(e) {
	var lesson_id = $(this).closest("li").data('id');
	$("#lessonID").val(lesson_id);
		
		$.ajax({
	            url: "<?php echo $this->webroot;?>lessons/ajaxLessonDetails",
	            data:{lesson_id:lesson_id},
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	$("#lesson_title").val(result.title);
	            	$("#lesson_description").val(result.description);
	            }
	    });		
	
	$('#sectionModal').modal('show');
});

$(document).on('submit','#edit_lesson', function() {
	var post_id = $("#course_id").val();
	var lesson_id = $("#lessonID").val();
	
	    $.ajax({
	            url: "<?php echo $this->webroot;?>lessons/ajaxEditLesson/"+lesson_id,
	            data:$(this).serialize(),
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	console.log(result);
	            	if(result.Ack==1){
	            		$('#sectionModal').modal('hide');
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            		$("#lesson_title-"+lesson_id).html('<i class="fa fa-file-text-o" aria-hidden="true"></i> '+result.title+'<i class="fa fa-pencil edit-lesson" style="cursor: pointer;" > </i><i class="fa fa-trash" style="cursor: pointer;" ></i>');
	            	}
	            	else{
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            }
	    });
	return false;    
});

//-------Edit Lecture details -----//
$(document).on('click','.edit-lecture', function(e) {
	var lecture_id = $(this).closest("li").data('id');
	$("#lectureID").val(lecture_id);
	var res = lecture_id.split("-");
	var lesson_id = res['0'];
	var lecture_id = res['1'];
	$("#lectureID").val(lecture_id);	
		$.ajax({
	            url: "<?php echo $this->webroot;?>lectures/ajaxLectureDetails",
	            data:{lecture_id:lecture_id},
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	$("#lecture_title").val(result.title);
	            	$("#lecture_description").val(result.description);
	            }
	    });		
	
	$('#lectureModal').modal('show');
});

$(document).on('submit','#edit_lecture', function() {
	var post_id = $("#course_id").val();
	var lecture_id = $("#lectureID").val();
	
	    $.ajax({
	            url: "<?php echo $this->webroot;?>lectures/ajaxEditLecture/"+lecture_id,
	            data:$(this).serialize(),
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	console.log(result);
	            	if(result.Ack==1){
	            		$('#lectureModal').modal('hide');
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            		$("#lecture_title-"+lecture_id).html('<i class="fa fa-file-text-o" aria-hidden="true"></i>'+result.title+'<i class="fa fa-pencil edit-lecture" style="cursor: pointer;"> </i> <i class="fa fa-trash" style="cursor: pointer;"></i>');
	            	}
	            	else{
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            }
	    });
	return false;    
});

//------- Delete Lecture -----//
$(document).on('click','.delete-lecture', function(e) {
	var lecture_id = $(this).closest("li").data('id');
	var post_id = $("#course_id").val();
	var res = lecture_id.split("-");
	var lesson_id = res['0'];
	var lecture_id = res['1'];
	$("#lectureID").val(lecture_id);	
		$.ajax({
	            url: "<?php echo $this->webroot;?>lectures/ajaxDeleteLecture",
	            data:{lecture_id:lecture_id,post_id:post_id},
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	if(result.Ack == 1){
	            		$("#sortable").html('');
	            		$("#sortable").html(result.html);

	            		$( "#sortable1, #sortable2" ).sortable({
							placeholder: "ui-state-default",
						  	connectWith: ".connectedSortable",
						  	update: function(event, ui) {
							    var ID = $( "#sortable" ).attr("id");
						        update_sprint_status("progress", ID);
					       	}  
						}).disableSelection();
						$( "#sortable" ).sortable({
						   	update: function(event, ui) {
						         var ID = $( "#sortable" ).attr("id");
						         update_sprint_status("progress", ID);
						   	} 
						});
						$( "#sortable" ).disableSelection();

	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            }
	    });		
});

//------- Add Quiz ----------//

$(document).on('click','#add_quiz', function(){
	$(".lecture").html('');
	var tmp = '<div class="ui-sortable sortable_part"><form class="section_form" method="post"><div class="row"><label class="col-md-2 lecture_label">New Quiz</label><span class="col-md-10"><input id="text" name = data[quiz][name] class="form-control form_part form_text" required placeholder="Enter a title" type="text"><div class="number">75</div></span><label class="col-md-2 lecture_label"></label><label class="col-md-10 lecture_label"></label><label class="col-md-2 lecture_label"></label><span class="col-md-10"><input id="text" name = data[quiz][objective] required class="form-control form_part form_text" type="text"><div class="number">75</div></span></div><div class="pull-right cancel_button"><button type="button" class="btn btn-primary cancel-lecture">Cancel</button><button type="submit" class="btn btn-primary add-quiz" style="margin-left:10px;">Add Quiz</button></div><div class="clearfix"></div></form></div>';
	$(".lecture").append(tmp);
});

$(document).on('click','.cancel-lecture', function(){
	$(".lecture").html('');
	var HTML = '<li class="add_menu" id="add_lecture"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Lecture</a></li><li class="add_menu" id="add_quiz"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Quiz</a></li><li class="add_menu" id="add_coding"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Coding</a></li><li class="add_menu" id="add_section"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Section</a></li>';
	$(".lecture").append(HTML);
});

$(document).on('click','.add-quiz', function(){
	var course_id = $("#course_id").val();
	var form_data = $(".section_form").serialize();
	$.ajax({
       type: "POST",
       url: '<?php echo $this->webroot;?>lectures/ajaxAddQuiz/'+course_id,
       dataType:'json',
       data:form_data, // serializes the form's elements.
       success: function(data)
       {
       	    if(data.Ack==1){			
           		$("#sortable").html('');
				$("#sortable").html(data.html);

           		$( "#sortable1, #sortable2" ).sortable({
					placeholder: "ui-state-default",
				  	connectWith: ".connectedSortable",
				  	update: function(event, ui) {
					    var ID = $( "#sortable" ).attr("id");
				        update_sprint_status("progress", ID);
			       	}  
				}).disableSelection();
				$( "#sortable" ).sortable({
				   	update: function(event, ui) {
				         var ID = $( "#sortable" ).attr("id");
				         update_sprint_status("progress", ID);
				   	} 
				});
				$( "#sortable" ).disableSelection();

           		$(".lecture").html('');
				var HTML = '<li class="add_menu" id="add_lecture"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Lecture</a></li><li class="add_menu" id="add_quiz"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Quiz</a></li><li class="add_menu" id="add_coding"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Coding</a></li><li class="add_menu" id="add_section"><a href="javascript:void(0);" ><img src="<?php echo $this->webroot;?>img/plusicon.jpg"> Add Section</a></li>';
				$(".lecture").append(HTML);
           }
       }
    });
	return false;
});

//-------On Hover Show Icon ------// 

$(document).on({
    mouseenter: function () {
    $(this).find('.fa-pencil').show();    //stuff to do on mouse enter
    },
    mouseleave: function () {
    $(this).find('.fa-pencil').hide();    //stuff to do on mouse leave
    }
}, ".first_section");
$(document).on({
    mouseenter: function () {
    $(this).find('.fa-trash').show();    //stuff to do on mouse enter
    },
    mouseleave: function () {
    $(this).find('.fa-trash').hide();    //stuff to do on mouse leave
    }
}, ".first_section");
$(document).on({
    mouseenter: function () {
    $(this).find('.fa-pencil').show();    //stuff to do on mouse enter
    },
    mouseleave: function () {
    $(this).find('.fa-pencil').hide();    //stuff to do on mouse leave
    }
}, ".lecturer");
$(document).on({
    mouseenter: function () {
    $(this).find('.fa-trash').show();    //stuff to do on mouse enter
    },
    mouseleave: function () {
    $(this).find('.fa-trash').hide();    //stuff to do on mouse leave
    }
}, ".lecturer");
//---------- Add Questions ------------//

$(document).on('click','.add_quiz_question', function(e) {
	var lecture_id = $(this).closest("li").data('id');
	var res = lecture_id.split("-");
	var lesson_id = res['0'];
	var lecture_id = res['1'];
	$("#quizID").val(lecture_id);
});

$(document).on('click','.quiz_ans', function() {
	var ans = $(this).siblings("textarea.options").val();
	$("#quiz_ans_option").val(ans);
});

$(document).on('submit','#add_questions', function(e) {
	e.preventDefault();
	var post_id = $("#course_id").val();
	var lecture_id = $("#lectureID").val();
	var res = lecture_id.split("-");
	var lesson_id = res['0'];
	var lecture_id = res['1'];
    var data = new FormData(this); // <-- 'this' is your form element

    var $checked = $('#add_questions').find(":radio:checked");
       if($checked.length==1){
       		var ans = $("#quiz_ans_option").val();
			
	    $.ajax({
	            url: "<?php echo $this->webroot;?>lectures/ajaxAddQuizQuestion/"+post_id,
	            data:data,
	            cache: false,
	            contentType: false,
	            processData: false,
	            dataType:'json',
	            type: 'POST',     
	            success: function(result){
	            	if(result.Ack==1){
	            		$('#questions').modal('hide');
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            	else{
	            		$('#ajaxResponse').text(result.res);
	            		$('#resModal').modal('show');
	            	}
	            }
	    });
	   }else{
	   	 $('#ajaxResponse').text('Please Select Any of The Options');
	     $('#resModal').modal('show');
	   }	
	return false;    
});

</script>
