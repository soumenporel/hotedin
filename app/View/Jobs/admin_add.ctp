<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<style>
#content {
    overflow: inherit;
}
.select2-container--default .select2-selection--multiple{
    width: 400px;
}

.will_be_able .target_student .need_to_know{
    margin-right: 12px;
}

.fa-times{
    cursor: pointer;
    margin-left: 8px;
}
.addTagBtn{
    margin-bottom: 11px;
    margin-top: -73px;
    margin-left: 408px;
}
#addTagName{
    width: 100%;
}
#saveTag{
    float: right;
}
</style>
<?php //pr($users); ?>
<div class="categories form">
<?php echo $this->Form->create('Post',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Job'); ?></legend>
	<?php
       
        echo $this->Form->input('user_id', array('empty' => '(choose employer)','class'=>'selectbox2'));
        
        echo $this->Form->input('post_title', array('label' => 'Title','required'=>'required'));
     
        echo $this->Form->input('post_description', array('label' => 'Description', 'type' => 'textarea','id'=>'post_desc','required'=>'required')); 
        
        echo $this->Form->input('employer_question', array('label' => 'Employer Question', 'type' => 'textarea')); 
        
        echo $this->Form->input('category_id',array('empty' => '(choose job category)', 'label' => 'Job Category','class'=>'selectbox2'));

       // echo $this->Form->input('type_id',array('empty' => '(choose job type)', 'label' => 'Job Type','class'=>'selectbox2'));
        
       // echo $this->Form->input('salary_per',array('empty' => '(choose salary percent)', 'label' => 'Salary percent','class'=>'selectbox2'));
        
         ?>
        <div class="input select">
        <label for="JobType">Job Type</label>
        <select name="data[Post][type_id]" class="selectbox2">
            <option value="">Choose job type</option>
         <?php   foreach($jobtypes as $key=>$val)
        {
             ?>
        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>    
         <?php   
             }
        ?>
         
        </select>
        </div>
        <div class="input select">
        <label for="SalaryPer">Salary Per</label>
        <select name="data[Post][salary_per]" class="selectbox2">
            <option value="">Choose Salary Per</option>
         <?php   foreach($salarypers as $key=>$val)
        {
             ?>
        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>    
         <?php   
             }
        ?>
         
        </select>
        </div>
        <?php
        
        echo $this->Form->input('salary_rate', array('label' => 'Salary rate'));
        echo $this->Form->input('country',array('empty' => '(choose country)', 'label' => 'Country','class'=>'selectbox2'));
        echo $this->Form->input('state', array('label' => 'State'));
        echo $this->Form->input('city', array('label' => 'city'));
        
        echo $this->Form->input('zip_code', array('label' => 'Zip'));
       ?>
      
    </fieldset>

    <div class="submit"><input value="Submit" type="submit" onclick="$('#PostAdminAddForm').submit();"></div>
</form>
<?php //echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>




<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>

<script type="text/javascript">
    CKEDITOR.replace('post_desc',
            {
                width: "95%"
            });
   
</script>


