<!--login bg-->
<section class="candidate-section py-5 mb-2" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
  <div class="container">
      <div class="row">
          <div class="col-sm-12">
              <div class="search-page-bg search-page">
                  <div class="form-body clearfix search-page-bg">
                      <form class="" action="" method="post">
                          <input type="text" name="title" class="form-control pull-left" id="srch" placeholder="job title,skill or compnay">
                          <input type="text" name="country" class="form-control pull-left" id="cntry" placeholder="Country City or Postal Code">
                          <input type="number" name="location" class="form-control pull-left" id="miles" placeholder="10 miles">
                          <input type="submit" class="btn btn-primary pull-left bg-white color-0" id="submit" value="Search">
                      </form>

                  </div>
              </div>
          </div>
      </div>
  </div>
</section>

<div class="container py-4">
  <div class="row">

      <div class="col-sm-12 col-md-3">
          <div class="d-block px-4 left-gra-border">
              <h1 class="pt-4 pb-2 mb-0">Locations</h1>
              <div class="search-page-list">
                  <ul>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">UK (3,965)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">London (1,375)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">South East (1,170)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">Wales (43)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">North East (39)</a>
                      </li>
                  </ul>
                  <a href="#">More ...</a>
              </div>
              <h1 class="pt-4 pb-2 mb-0">Salaries</h1>
              <div class="search-page-list">
                  <ul>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">at least £10,000 (3,802)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">at least £20,000 (2,942)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">at least £90,000 (101)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">at least £100,000 (95)</a>
                      </li>

                  </ul>
                  <a href="#">More ...</a>
              </div>
              <h1 class="pt-4 pb-2 mb-0">Date posted</h1>
              <div class="search-page-list">
                  <ul>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">Last 24 hours (1,127)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">Last 3 days (1,571)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">Last 7 days (1,701)</a>
                      </li>
                      <li><label class="search-chk"><input type="checkbox">
                          <span class="checkmark1"></span>
                          </label><a href="#">Last 14 days (2,615)</a>
                      </li>

                  </ul>
                  <a href="#">More ...</a>
              </div>
          </div>
      </div>
      <div class="col-sm-12 col-md-9">
          <div class="d-block left-gra-border py-4 px-4">
              <?php foreach ($wholes as $whole) { ?>
                  <div class="row">
                      <div class="col-sm-12 col-md-10 col-lg-10">
                          <div class="media">
                              <?php if($whole['Job']['logo']) { ?>
                               <img class="mr-4" src="<?php echo $this->webroot ?>company_logo/<?php echo $whole['Job']['logo'] ?>" height="100px" width="75px" alt="Generic placeholder image">
                           <?php }else { ?>
                               <img class="mr-4" src="<?php echo $this->webroot ?>images/no_image.png" height="100px" width="75px" alt="No Image">
                           <?php } ?>
                               <div class="media-body">
                                  <h5 class="mt-0 searchlist-heading"><?php echo $whole['Job']['title'] ?> – <?php echo $whole['Jobtype']['name'] ?></h5>
                                  <h6 class="searchlist-sub-heading"><?php echo $whole['Job']['company_name'] ?></h6>
                                  <ul class="list-inline mb-0 py-2">
                                      <li class="list-inline-item mr-5"><i class="icon ion-briefcase"></i> 5 jobs</li>
                                      <li class="list-inline-item mr-5"><i class="icon ion-ios-location"></i><?php echo $whole['Job']['city'] ?>, <?php echo $whole['Job']['country'];?></li>
                                      <?php $date = date("jS F Y", strtotime($whole['Job']['created'])); ?>
                                      <li class="list-inline-item"><i class="icon ion-android-calendar"></i><?php echo $date; ?></li>
                                  </ul>
                                  <p class="searchlist-sub-para pb-0 mb-0"><?php echo $whole['Job']['description'] ?></p>
                               </div>

                          </div>
                      </div>

                      <div class="col-sm-12 col-md-2 col-lg-2">
                          <button type="button" class="btn btn-primary mb-3 searchlist-btn">Apply</button>
                          <button type="button" class="btn btn-success searchlist-btn">Save</button>
                      </div>
                      <div class="line-1 mt-4 mb-4 w-100"></div>
                  </div>
              <?php } ?>
          </div>
      </div>

  </div>
</div>
