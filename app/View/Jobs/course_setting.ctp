<style>
	.setting-header{border-bottom:#ddd solid 1px; padding:30px;}
	.setting-header p{font-size:16px; color:#000; padding-bottom:0; margin:15px 0;}

	.course-setting{border-bottom:#ddd solid 1px; padding:30px;}
	.course-setting h3{font-size:24px; font-weight:600; color:#444;}
	.course-setting table{width:100%; font-size:16px;}
	.course-setting table tr th{border-bottom:#ddd solid 1px; padding:10px 15px;}
	.course-setting table tr td{padding:10px 15px;}
	.course-setting table tr td span{padding:3px; border-radius:3px; background:#555; color:#fff; margin-left:5px; font-size:13px;}
</style>

<section class="profileedit">
	<div class="container">
		<div class="row" style="background: #f6f6f6; border-right: #ddd solid 1px; border-left: #ddd solid 1px;">
			<div class="col-md-3 col-sm-3" style="padding: 0;">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);

				//pr($accessUsers);
				?>
				
			</div>
			
			<div class="col-md-9 col-sm-9" style="padding:0; border-left: 1px solid #dedede;">
				<div class="profile_second_part" style="padding:0;">
					<div class="setting-header">

						<h2>Course Settings </h2>
					
						<p class="pull-right course_setting">
							<span>
		 						<button type="button" class="btn btn-default">Manage Email Notifications</button>
		 					</span>        
	 						<span>
	 							<i class="fa fa-question-circle" aria-hidden="true"></i>
								<a href="#">help</a>
							</span>
						</p>

					</div>


					<div class="setting-header">

						<div class="form-group">
							<label class="control-label col-sm-12" for="text">Basic:</label>
							<div class="col-sm-7">
								<select class="form-control form_part" id="basicSelect" placeholder="Designation">
									<option value="0" <?php if($post_dtls['Post']['privacy'] == 0){ echo 'selected'; } ?> >Public</option>
									<option value="1" <?php if($post_dtls['Post']['privacy'] == 1){ echo 'selected'; } ?> >Private (Invitation Only)</option>
									<option value="2" <?php if($post_dtls['Post']['privacy'] == 2){ echo 'selected'; } ?> >Private (Password Protected)</option>
								</select>
								<input type="password" value="<?php echo $post_dtls['Post']['post_password']; ?>" id="privatePassword" class="form-control" style="margin-top: 14px; height: 42px; display:none" ></input>
								<p id="basicText" > Public courses show up in search results and are available for anyone to take on Learnfly. </p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-9">
								<button class="btn btn-default" type="submit" id="basicSubmit">Save</button>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="course-setting">

						<h3>Permissions</h3>

						<div class="table">
							<table>

								<tr>
									<th>Instructor</th>
									<th class="text-center">Visible</th>
									<th class="text-center">Manage</th>
									<th class="text-center">Delete</th>
								</tr>
								<tr id="memberListing" >
									<td><?php echo $post_dtls['User']['first_name'].' '.$post_dtls['User']['last_name'] ?> <span>owner</span></td>
									<td class="text-center"><input type="checkbox"></td>
									<td class="text-center"><input type="checkbox"></td>
									<th class="text-center"><input type="checkbox"></th>
								</tr>
								<?php foreach ($accessUsers as $key => $accessUser) {?>
								<tr>
									<td><?php echo $accessUser['User']['first_name'].' '.$accessUser['User']['last_name'] ?></td>
									<td class="text-center">
										<input class="classCheckbox" 
										<?php
											if($accessUser['AccessInstructor']['visible']==1){
												echo "checked";
											}
										?> 
										data-type="visible" data-id="<?php echo $accessUser['User']['id']; ?>"  value="1" type="checkbox">
									</td>
									<td class="text-center">
										<input class="classCheckbox"
										<?php
											if($accessUser['AccessInstructor']['edit']==1){
												echo "checked";
											}
										?>
										 data-type="edit" data-id="<?php echo $accessUser['User']['id']; ?>" value="1" type="checkbox">
									</td>
									<th class="text-center">
										<input class="classCheckbox" 
										<?php
											if($accessUser['AccessInstructor']['delete_access']==1){
												echo "checked";
											}
										?>
										data-type="delete" data-id="<?php echo $accessUser['User']['id']; ?>" value="1" type="checkbox">
									</th>
								</tr>
								<?php } ?>
							</table>
						</div>
					</div>

					<div class="setting-header">

						<form action="#" method="post" id="accessForm" >	
							<div class="col-sm-10">
								<input class="form-control form_part" placeholder="Enter Email associated with a Learnfly Account" required="" name="userEmail"  type="text">
								<input type="hidden" name="post_id" id="course_id" value="<?php echo $post_dtls['Post']['id']; ?>" >
							</div>
							
							<div class="col-sm-2">
								<div class="submit_but" style="margin:0">
									<!-- <button class="btn btn-default" type="button" style="background: #E5E1E1;" >Add</button> -->
								</div>
							</div>

							<div class="clearfix"></div>

							<div class="form-group" style="margin-top:20px;">
								<div class="col-sm-9">
									<button class="btn btn-default" type="submit">Save</button>
								</div>
							</div>
						</form>
						
						<div class="clearfix"></div>
					</div>

					<div class="course-setting">

						<h3>Third Party Integration</h3>

						<div class="form-group" style="margin-top:20px">
							<label class="control-label col-sm-12" for="text">Google Analytics Tracking ID</label>
							<div class="col-sm-5" style="margin-bottom:20px;">
								<input id="text" class="form-control form_part"  placeholder="UA-######" type="text">
							</div>
						</div>

						<div class="form-group" style="margin-top:20px">
							<label class="control-label col-sm-12" for="text">Google Adwords</label>
							<div class="col-sm-5">
								<input class="form-control form_part"  placeholder="Coversion ID" type="text">

								<input class="form-control form_part" style="margin-top:20px;" placeholder="Landing Page Coversion Labe" type="text">

								<input class="form-control form_part" style="margin-top:20px;" placeholder="Checkout Page Coversion Labe" type="text">

								<input class="form-control form_part" style="margin-top:20px;" placeholder="Success Page Coversion Labe" type="text">
							</div>
							<div class="clearfix"></div>

							<div class="form-group" style="margin-top:20px;">
								<div class="col-sm-9">
									<button class="btn btn-default" type="submit">Save</button>
								</div>
							</div>

							<div class="clearfix"></div>
						</div>
					</div>
					
					<div class="setting-header" style="border-bottom:0;">

						<div class="row">

							<div class="col-sm-3">

								<div class="submit_but" style="margin:0">
									<button class="btn btn-default" type="button" style="background: #E5E1E1; width:100%;" disabled="">Unpublish</button>
								</div>
								<div class="form-group" style="margin:0">
									<button class="btn btn-danger" type="button" style="width:100%;">Delete</button>
								</div>
								
							</div>

							<div class="col-sm-9">

								<p> New students cannot find your course via search, but existing students can still access content. </p>
								
								<p> We promise students lifetime access, so courses cannot be deleted after students have enrolled. </p>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
</section>
<script>

$(document).ready(function(){
	var privacy = '<?php echo $post_dtls["Post"]["privacy"];?>';
	if(privacy == 2){
		$('#basicText').text(' Password Protected courses don\'t show up in search results on Learnfly. Instead, share the course URL and password directly with students you want to enroll in your course. ');
		$('#privatePassword').css('display','block');
	}

	$('#basicSelect').change(function(){
		
		var value = $(this).val();
		
		if( value == '0' ){
			$('#basicText').text(' Public courses show up in search results and are available for anyone to take on Learnfly. ');
			$('#privatePassword').css('display','none');
		}
		
		if( value == '1' ){
			$('#basicText').text(' Invitation Only courses don\'t show up in search results on Learnfly. Accept new student requests and send invitations from the \'Students\' page found under Course Management in the left navigation. ');
			$('#privatePassword').css('display','none');
		}
		
		if( value == '2' ){
			$('#basicText').text(' Password Protected courses don\'t show up in search results on Learnfly. Instead, share the course URL and password directly with students you want to enroll in your course. ');
			$('#privatePassword').css('display','block');
		}
	});

	$("#basicSubmit").click(function(){
		var value = $("#basicSelect").val();

		if( value == '2' ){
			var pasw = $("#privatePassword").val();

			$.ajax({
                type: "POST",
                url: "<?php echo $this->webroot; ?>posts/ajaxCoursePrivacy",
                dataType:"json",
                data: {
                    post_id: '<?php echo $post_dtls["Post"]["id"]; ?>',
                    password: pasw,
                    type: '2'
                },
                success: function (html) {
                    if(html.Ack==1){
                    	var URL = window.location.href;
                    	window.location.href = URL;
                    }
                }
            });

		}else{
			var value = $("#basicSelect").val();
			$.ajax({
                type: "POST",
                url: "<?php echo $this->webroot; ?>posts/ajaxCoursePrivacy",
                dataType:"json",
                data: {
                    post_id: '<?php echo $post_dtls["Post"]["id"]; ?>',
                    type: value
                },
                success: function (html) {
                    if(html.Ack==1){
                    	var URL = window.location.href;
                    	window.location.href = URL;
                    }
                }
            });
		}

	});

	$("#accessForm").submit(function(){

		$.ajax({
            type: "POST",
            url: "<?php echo $this->webroot; ?>posts/ajaxCourseAccess",
            dataType:"json",
            data: $("#accessForm").serialize(),
            success: function (html) {
            	if(html.Ack == 1){
            		$("#memberListing").after('<tr><td>'+html.name+'</td><td class="text-center"><input class="classCheckbox" data-type="visible" data-id="'+html.id+'"  value="1" type="checkbox"></td><td class="text-center"><input class="classCheckbox" data-type="edit" data-id="'+html.id+'" value="1" type="checkbox"></td><th class="text-center"><input class="classCheckbox" data-type="delete" data-id="'+html.id+'" value="1" type="checkbox"></th></tr>');
            	}
            }
        });

		return false;
	});

	$(document).on('click','.classCheckbox',function(){
		var post_id = $("#course_id").val();
		var type = $(this).data('type');
		var user_id = $(this).data('id');
		
		if($(this).is(':checked')){
			var val = $(this).val();	
		}else{
			var val = 0;
		}
		
		$.ajax({
            type: "POST",
            url: "<?php echo $this->webroot; ?>posts/ajaxUserAccess",
            dataType:"json",
            data: {
            	post_id:post_id,
            	user_id:user_id,
            	type:type,
            	value:val
            },
            success: function (html) {
            	console.log(html);
            }
        });
	});
});

</script>

