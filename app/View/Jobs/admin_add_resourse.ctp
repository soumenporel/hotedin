    <style type="text/css">
    .CL:after{ clear: both;
        content: '';
        display: block;}
    </style>
    <div class="categories form">
        <fieldset>
            <legend> Course Resourse </legend>
            <ul class="row" style="list-style-type:none">
                <li class="col-md-3">
                    <div class="CL">
                    <label for="PostIsApprove">Course Videos</label>
                    <form id="videoForm" action="#" method = "post" enctype="multipart/form-data" >
                        <input style="display:none" id="addVideo" type="file" name="data[Post][video][]" class="courseVideo">
                    </form>    
                    
                    <a href="javascript:void(0)" id="addMore" class="btn btn-info btn-sm pull-left"><i class="fa fa-plus"></i> Add More Video</a>
                    </div>
                    <ul style="list-style-type:none" >
                        <li><span>sas.jpg</span><span>X</span></li>
                        <li>ff</li>
                        <li>ggg</li>
                    </ul>
                </li>
                
                <li class="col-md-3">
                    <label for="PostIsApprove">Course Documents</label>
                    
                    <a href="javascript:void(0)" id="addMoreDoc" class="btn btn-info btn-sm pull-left"><i class="fa fa-plus"></i> Add More Docs</a>
                </li>
                <li class="col-md-3">
                    <label for="PostIsApprove">Course Assignments</label>
                   
                    <a href="javascript:void(0)" id="addMoreAssig" class="btn btn-info btn-sm pull-left"><i class="fa fa-plus"></i> Add More Assignments</a>
                </li>
                <li class="col-md-3">
                    <label for="PostIsApprove">Course Projects</label>
                    
                    <a href="javascript:void(0)" id="addMoreProject" class="btn btn-info btn-sm pull-left"><i class="fa fa-plus"></i> Add More Projects</a>
                </li>
            </ul>
        </fieldset>    
    </div>


<script type="text/javascript">
    

$(document).ready(function(){
    
    $(document).on('click',"#addMore",function(){
        $("#addVideo").trigger('click');  
    });

    $(document).on('change',"#addVideo",function(){

        var file_data = $(this).prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to
        form_data.append("post_id", "<?php echo $post_id; ?>") 

        $.ajax({
            url: "<?php echo $this->webroot; ?>posts/addVideo",
            type: "POST",             // Type of request to be send, called as method
            data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,
            dataType:'json',
            success: function(result){ }
        }); 
    });
    
});

</script>    