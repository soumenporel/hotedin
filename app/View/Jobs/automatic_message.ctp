<style>
.header{
    position:relative;
}

.inner_header{
    background:#0c2440;
}


</style>

<section class="profileedit">
	<div class="container">
		<div class="row" style="background:#f6f6f6; border-right:#ddd solid 1px; border-left:#ddd solid 1px;">
			<div class="col-md-3 col-sm-3" style="padding:0;">
				<?php 
							/***Course Sidebar**/
				echo $this->element('course_sidebar',$post_dtls,$course_status);
				?>
			</div>
			<?php
			if(!empty($post_dtls['Post']['automatic_message'])){
				$data = json_decode($post_dtls['Post']['automatic_message']);
				$welcome_message = $data->welcome_message;
				$congratulation_message = $data->congratulation_message;
			}
			else{
				$welcome_message = '';
				$congratulation_message = '';
			} 
			?>
			<div class="col-md-9 col-sm-9" style="padding:0; border-left:#ddd solid 1px;">
				<div class="profile_second_part automatic_messages">
					<!-- <p class="pull-right course_setting"><span><i class="fa fa-cog" aria-hidden="true"></i>
 <a href="<?php echo $this->webroot;?>posts/course_setting/<?php echo $post_dtls['Post']['slug'];?>">Course Settings</a></span>        
 						 <span>
 							<i class="fa fa-question-circle" aria-hidden="true"></i>
							<a href="#">help</a>
						</span> 
					</p>-->
					<?php echo $topContent['CmsPage']['page_description']; ?>
					<form class="form-horizontal form_parttestvideo" action="<?php echo $this->webroot;?>posts/automatic_message/<?php echo $post_dtls['Post']['slug'];?>" method="post">
						  <div class="form-group row">
						    <label class="form-control-label text-sm-right col-sm-3" for="text">Welcome Message</label>
						    <div class="col-sm-9">
								  <textarea class="form-control" maxlength="1000" name="data[welcome_message]" rows="5" id="comment-1" required><?php echo $welcome_message;?></textarea>
								  <div class="number">1000</div>

						    </div>
						  </div>
					 	
					 	<div class="form-group row">
						    <label class="form-control-label text-sm-right col-sm-3" for="text">Congratulations
Message</label>
						    <div class="col-sm-9">
								  <textarea class="form-control" maxlength="1000" name="data[congratulation_message]" rows="5" id="comment-2" required><?php echo $congratulation_message;?></textarea>
								  <div class="number">1000</div>

						    </div>
						  </div>
					 	<div class="bulk_uploader">
					 		<div class="col-sm-9 offset-sm-3">
					 			<button type="submit" class="btn btn-danger">Save</button>
					 		</div>
					 	</div>
					 </form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
$(function () {
	$(document).on('keyup', 'textarea', function(){
    	var self = $(this);
    	var attr = self.attr('maxlength');
    	if (typeof attr !== typeof undefined && attr !== false) {
    		var showThis = attr - self.val().length;
		    self.next('.number').text(showThis);
		}
    	
    });
    CalculateNumber();
});

function CalculateNumber() {
	$('textarea').each(function(){
		var self = $(this);
    	var attr = self.attr('maxlength');
    	if (typeof attr !== typeof undefined && attr !== false) {
    		var showThis = attr - self.val().length;
		    self.next('.number').text(showThis);
		}
	});
}
</script>