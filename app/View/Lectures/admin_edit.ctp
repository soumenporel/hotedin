<div class="categories form">
    <?php echo $this->Form->create('Lecture', array('enctype' => 'multipart/form-data')); ?>
    <a href="<?php echo $this->webroot.'admin/lessons/view/'.$lesson_id; ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Back to Lesson View</a>
    <fieldset>
        <legend><?php echo __('Edit Lecture',array('enctype'=>'multipart/form-data')); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('title');
        echo $this->Form->input('description');
        echo $this->Form->input('duration');
        echo $this->Form->input('media', array('label'=>'Image/Video','type' => 'file'));
        ?>
        <input name="data[Lecture][type]" maxlength="256" id="LectureType" value='0' type="hidden">
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>

<script type="text/javascript" src="<?php echo $this->webroot; ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('description',
            {
                width: "95%"
            });
</script>
