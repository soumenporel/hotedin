<style>
.nav > li > a:hover, .nav > li > a:focus {
    background-color: #f5f5f5;
}
</style>
<?php $assetNew = $asset; ?>
<?php $linkNew = $link; ?>
<?php $codeNew = $code; ?>
<?php //pr($lecture);?>
<div class="categories view">
<h2><?php echo __('Lesson'); ?></h2>
<a href="<?php echo $this->webroot.'admin/lessons/view/'.$lecture['Lecture']['lesson_id']; ?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Back to Lesson View</a>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lecture Title'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lecture Description'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Media'); ?></dt>
		<dd>
                    <?php
                    if(isset($lecture['Lecture']['media']) and !empty($lecture['Lecture']['media']))
                    {
                    	$file = $lecture['Lecture']['media'];
                    	$ext = pathinfo($file, PATHINFO_EXTENSION);
                    	$imageExt = array('jpeg' , 'jpg');
                    	$videoExt = array('mp4','flv','mkv');
                    	$pdfExt = array('pdf');
                    	$docExt = array('doc','docx','ppt');
                    	if(in_array(strtolower($ext), $videoExt)){ 
                    		$file_name = trim($lecture['Lecture']['media'],$ext);
                    		?>     
                            <video width="400" controls>
							  <source src="<?php echo $this->webroot;?>lecture_media/<?php echo $lecture['Lecture']['media'];?>" type="video/<?php echo $ext; ?>">
							  <source src="<?php echo $this->webroot.'lecture_media/'.$file_name;?>ogg" type="video/ogg"> 
							  Your browser does not support HTML5 video.
							</video>
					<?php }
					    if(in_array(strtolower($ext), $imageExt)){ ?>		
		                    <img alt="" src="<?php echo $this->webroot;?>lecture_media/<?php echo $lecture['Lecture']['media'];?>" style=" height:80px; width:80px;">
		                    <?php
		                    
		                    }
		                if(in_array(strtolower($ext), $pdfExt)){ ?>
		                	<a href="<?php echo $this->webroot;?>lecture_media/<?php echo $lecture['Lecture']['media'];?>" target="_blank">Read PDF in another tab</a>
		              <?php }
		                if(in_array(strtolower($ext), $docExt)){ ?>

							<a href="<?php echo $this->webroot;?>lecture_media/<?php echo $lecture['Lecture']['media'];?>" download>
		                	<button type="button" class="btn btn-primary">Download</button>
		                	</a>
		                <?php }
                    }
		            else{
		                    ?>
		                   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                   <?php } ?>
		</dd>
		<!-- <dt><?php echo __('Approve'); ?></dt>
		<dd>
			<?php echo h($post['Post']['is_approve']==1?'No':'Yes'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Featured'); ?></dt>
		<dd>
			<?php echo h($post['Post']['featured']==1?'Yes':'No'); ?>
			&nbsp;
		</dd> -->
	</dl>
</div>




<div class="row">
    <div class="col-md-12 col-xs-12 col-sm12">
        <div class="panel">
            <header class="panel-heading">
                Downloadable materials
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <!-- <a href="javascript:;" class="fa fa-times"></a> -->
                </span>
            </header>
            <div class="panel-body" style="display: block;">
                <div style="overflow-x: auto;">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th class="numeric">Name</th>
                                <th class="numeric">Date</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($lecture['DownloadableFile'])){ foreach ($lecture['DownloadableFile'] as $key => $file) { ?>
                            <tr>
                                <th><?php echo $file['downloadable_file']; ?></th>
                                <th><?php echo $file['date']; ?></th>
                                <th><i class="fa fa-times" ></i></th>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm12">
        <div class="panel">
            <header class="panel-heading">
                External Link
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <!-- <a href="javascript:;" class="fa fa-times"></a> -->
                </span>
            </header>
            <div class="panel-body" style="display: block;">
                <div style="overflow-x: auto;">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th class="numeric">Name</th>
                                <th class="numeric">Date</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($lecture['ExternalLink'])){ foreach ($lecture['ExternalLink'] as $key => $link) { ?>
                            <tr>
                                <th><?php echo $link['link']; ?></th>
                                <th><?php echo $link['date']; ?></th>
                                <th><i class="fa fa-times" ></th>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm12">
        <div class="panel">
            <header class="panel-heading">
                Source Code
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <!-- <a href="javascript:;" class="fa fa-times"></a> -->
                </span>
            </header>
            <div class="panel-body" style="display: block;">
                <div style="overflow-x: auto;">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th class="numeric">Name</th>
                                <th class="numeric">Date</th>
                                <th class="numeric">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($lecture['SourseCode'])){ foreach ($lecture['SourseCode'] as $key => $code) { ?>
                            <tr>
                                <th><?php echo $code['file']; ?></th>
                                <th><?php echo $code['date']; ?></th>
                                <th><i class="fa fa-times" ></th>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>    

    



<div class="container" style="width:100%;">
	<div class="row">
		<div class="col-md-12">
			<h2>Add Resources</h2>
  <ul class="nav nav-tabs">
    <li><a data-toggle="tab" href="#menu1">DOWNLOADABLE FILE</a></li>
    <li><a data-toggle="tab" href="#menu2">ADD FROM LIBRARY</a></li>
    <li><a data-toggle="tab" href="#menu3">EXTERNAL RESOURCE</a></li>
    <li><a data-toggle="tab" href="#menu4">SOURCE CODE</a></li>
  </ul>

  <div class="tab-content">
    
    <div id="menu1" class="tab-pane fade in active">
      <h3>Add Lecture Downloadable File</h3>
      <div class="">
        <form method="post" id="downloadable_file" enctype="multipart/form-data" >
        	<div class="">
            Upload file
        	<input type="file" method="post" name="code_file" id="lecture_code">
            <span style="color:red;font-size:12px;">*File Type Should be .DOCX,.DOC,.PDF,.JPEG,.JPG,.PNG .</span>
        	<input type="hidden" name="lacture_id" value="<?php echo $lecture['Lecture']['id']; ?>">
            <input type="hidden" name="post_id" value="<?php echo $lecture['Lecture']['post_id']; ?>">
        	</div>
        	<div class="form-actions">
		    		<div class="submit-row">
		    			<input type="submit" name="submit" value="Add link" class="btn btn-primary " id="submit_file">
		    		</div>
		    </div>
    	</form>
      </div>
    </div>


    <div id="menu2" class="tab-pane fade">
      <h3>Add From Library</h3>
      <span style="color:blue;font-size:12px;"> Click On the listing to add resources from library </span>
        <div class="panel-body" style="display: block;">
        <div style="overflow-x: auto;">

            <table cellpadding="0" cellspacing="0">

                <tr>
                    <th>Filename</th>
                    <th>Type</th>
                    <th>Date Uploaded</th>
                    <th>Status</th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>

                <?php if(isset($assetNew) && !empty($assetNew)){ foreach ($assetNew as $assetval){ ?>
                <tr style="cursor:pointer;" class="table_tr" >
                        <td class="td_name" ><?php echo h($assetval['LectureAsset']['downloadable_file']); ?></td>
                        <td>Downloadable File</td>
                        <td><?php echo h($assetval['LectureAsset']['date']); ?></td>
                        <td style="display:none" class="type">1</td>
                        <td>Success</td>
                        <td >
                            <?php
                            echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'lectures' , 'action' => 'view'), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                            ?>


                        </td>

                    </tr>
                <?php } } ?>
                <?php if(isset($linkNew) && !empty($linkNew)){ foreach ($linkNew as $linkval){?>
                <tr style="cursor:pointer;" class="link_table_tr" >
                        <td class="td_name" ><?php echo h($linkval['LectureLink']['link']); ?></td>
                        <td>External Link</td>
                        <td><?php echo h($linkval['LectureLink']['date']); ?></td>
                        <td style="display:none" class="type">2</td>
                        <td style="display:none" class="url"><?php echo h($linkval['LectureLink']['url']); ?></td>
                        <td>Success</td>
                        <td >
                            <?php
                            echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'lectures' , 'action' => 'view'), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                            ?>


                        </td>

                    </tr>
                <?php } } ?>
                <?php if(isset($codeNew) && !empty($codeNew)){ foreach ($codeNew as $codeval){?>
                <tr style="cursor:pointer;" class="table_tr" >
                        <td class="td_name" ><?php echo h($codeval['LectureCode']['file']); ?></td>
                        <td>Source Code</td>
                        <td><?php echo h($codeval['LectureCode']['date']); ?></td>
                        <td style="display:none" class="type">3</td>
                        <td>Success</td>
                        <td >
                            <?php
                            echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), array('controller'=>'lectures' , 'action' => 'view'), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                            ?>


                        </td>

                    </tr>
                <?php } } ?>
                </table>

                <div class="paging">
                    <span class="prev disabled">&lt; previous</span><span class="next disabled">next &gt;</span>                        
                </div>
            </div>
        </div>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Add Lecture External Resource</h3>
	      <div class="add-external-link-tab ud-assetcreator-externallink show" data-type="ExternalLink">
		    <form method="post" id="external_link" >
		    	<input type="hidden" name="lacture_id" id="hidden_lecture_id" value="<?php echo $lecture['Lecture']['id']; ?>">
                <input type="hidden" name="post_id" id="hidden_post_id" value="<?php echo $lecture['Lecture']['post_id']; ?>"> 
		    	<div class="">
		    		<div class="labeled " id="">
		    			<label class="">Title</label>
		    			<div id="" class="tooltip-reference form-field pos-r  ">
		    				<input class="textinput textInput form-control" id="id_title" name="title" placeholder="A descriptive title" type="text"> 
		    			</div>
		    		</div>
		    		<div class="labeled " id="form-item-url">
		    			<label class="">URL</label>
		    			<div id="" class="tooltip-reference form-field pos-r  ">
		    				<input class="urlinput form-control" id="id_url" name="url" placeholder="https://example.com" type="url"> 
		    			</div>
		    		</div>
		    	</div>
		    	<div class="form-actions">
		    		<div class="submit-row">
		    			<input type="submit" name="submit" value="Add link" class="btn btn-primary " id="submit_form">
		    		</div>
		    	</div>
		    </form>
		  </div>
    </div>
    <div id="menu4" class="tab-pane fade">
      <h3>Add Lecture Source Code</h3>
      <div class="">
        <form method="post" id="source_code" enctype="multipart/form-data" >
        	<div class="">
            Upload file
        	<input type="file" method="post" name="code_file" id="lecture_code">
            <span style="color:red;font-size:12px;">*Image Type Should be .DOCX,.DOC,.PDF .</span>
        	<input type="hidden" name="lacture_id" value="<?php echo $lecture['Lecture']['id']; ?>">
            <input type="hidden" name="post_id" value="<?php echo $lecture['Lecture']['post_id']; ?>">
        	</div>
        	<div class="form-actions">
		    		<div class="submit-row">
		    			<input type="submit" name="submit" value="Add link" class="btn btn-primary " id="submit_code">
		    		</div>
		    </div>
    	</form>
    </div>
    </div>
  </div>
		</div>
	</div>
  
</div>

<script>
$(document).ready(function(){
	
	//Add External Links    
    $("#submit_form").click(function(){
            
        	var data = $("#external_link").serialize();
            var title = $("#id_title").val();
            var url = $("#id_url").val();
            var lecture_id = $("#hidden_lecture_id").val();
		    if(title!='' && url!=''){
                $(this).prop('disabled', true);
                $.ajax({
                    url: '<?php echo $this->webroot; ?>lecture_links/ajaxAddLectureAsset',
                    type: 'POST',
                    dataType: 'json',
                    data:data,
                    success: function (data) {
                        if(data.Ack==1){
                        	alert('External Link Has Been Saved Successfully');
                            //$(this).prop('disabled', false);
                            window.location.href="<?php echo $this->webroot;?>admin/lectures/view/"+lecture_id;
                        }
                    }
                });
            }    
        return false;
    });

    //Add Source Code    
    $(document).on('submit','#source_code', function(e) {
		e.preventDefault();
    	var data = new FormData(this);
    	$.ajax({
            url: '<?php echo $this->webroot; ?>lecture_codes/ajaxAddLectureCode',
            data:data,
            cache: false,
            contentType: false,
            processData: false,
            dataType:'json',
            type: 'POST',
            success: function (data) {
                if(data.Ack==1){
                	alert('Source Code Has Been Saved Successfully');
                	location.reload();
                }
            }
        });
		return false;
	});

	//Add Downloadable File    
    $(document).on('submit','#downloadable_file', function(e) {
		e.preventDefault();
    	var data = new FormData(this);
    	$.ajax({
            url: '<?php echo $this->webroot; ?>lecture_assets/ajaxAddLectureAsset',
            data:data,
            cache: false,
            contentType: false,
            processData: false,
            dataType:'json',
            type: 'POST',
            success: function (data) {
                if(data.Ack==1){
                	alert('Downloadable File Has Been Saved Successfully');
                	location.reload();
                }
            }
        });
		return false;
	});

    //Add From Librery
    $(document).on('click','.table_tr', function(e) {
        var data = $(this).closest('tr').children('td.td_name').text();
        var post_id = $("#hidden_post_id").val();
        var lecture_id = $("#hidden_lecture_id").val();
        var type = $(this).closest('tr').children('td.type').text();
            if(type==1){
                var url = '<?php echo $this->webroot; ?>lecture_assets/ajaxAddLectureAssetLibrary';
            }
            if(type==3){
                var url = '<?php echo $this->webroot; ?>lecture_codes/ajaxAddLectureCodeLibrary';
            }
        $.ajax({
            url:url,
            data:{name:data,post_id:post_id,lecture_id:lecture_id},
            dataType:'json',
            type: 'POST',
            success: function (data) {
                if(data.Ack==1){
                    alert(data.res);
                    location.reload();
                }
            }
        });
    });

    $(document).on('click','.link_table_tr', function(e) {
        var data = $(this).closest('tr').children('td.td_name').text();
        var url = $(this).closest('tr').children('td.url').text();
        var type = $(this).closest('tr').children('td.type').text();
        var post_id = $("#hidden_post_id").val();
        var lecture_id = $("#hidden_lecture_id").val();
        $.ajax({
            url: '<?php echo $this->webroot; ?>lecture_links/ajaxAddLectureLink',
            data:{name:data,url:url,type:type,post_id:post_id,lecture_id:lecture_id},
            dataType:'json',
            type: 'POST',
            success: function (data) {
                if(data.Ack==1){
                    alert('External Link Has Been Saved Successfully');
                    location.reload();
                }
            }
        });
    });	

});
</script>








