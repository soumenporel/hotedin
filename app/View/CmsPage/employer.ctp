<!--login bg-->
<section class="employee-section" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
      <div class="employee-banner-part">
          <div class="container">
              <div class="row">
              <div class="col-sm-12 col-md-6">
                  <h1 class="employee-banner-part-heading"><?php echo $wppages2['WpPages']['heading'] ?></h1>
                  <h2 class="employee-banner-part-heading-sub"><?php echo $wppages2['WpPages']['content'] ?></h2>
                  <ul class="d-block list-unstyled employee-list">
                      <?php $points = $wppages2['WpPages']['points'];
                      $points = explode(',', $points);
                      foreach ($points as $point) { ?>
                          <li><i class="icon ion-checkmark-round"></i> <?php echo $point ?></li>
                      <?php } ?>
                  </ul>
              </div>

              <div class="col-sm-12 col-md-6">
                  <table class="table table-bordered employee-table">
                      <thead>
                          <tr>
                             <th>Plans</th>
                   <?php foreach ($plans as $plan) : ?>
                         <th><?php echo $plan['EmployerMembershipPlan']['title'] ?></th>
                   <?php endforeach; ?>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>No of Jobs</td>
                        <?php foreach ($plans as $plan) : ?>
                            <td><?php echo $plan['EmployerMembershipPlan']['jobs'] ?></td>
                        <?php endforeach; ?>
                        </tr>
                        <tr>
                          <td>Maximum User</td>
                          <?php foreach ($plans as $plan) : ?>
                              <td><?php echo $plan['EmployerMembershipPlan']['max_user'] ?></td>
                          <?php endforeach; ?>
                        </tr>
                        <tr>
                          <td>Duration</td>
                          <?php foreach ($plans as $plan) : ?>
                                   <td><?php echo $plan['EmployerMembershipPlan']['duration'] ?> <?php echo $plan['EmployerMembershipPlan']['duration_unit'] ?></td>
                          <?php endforeach; ?>
                        </tr>
                          <tr>
                          <td>Price</td>
                          <?php foreach ($plans as $plan) : ?>
                              <?php if($plan['EmployerMembershipPlan']['price'] == 0) { ?>
                                   <td>Free</td>
                              <?php }else {  ?>
                                   <td>$<?php echo $plan['EmployerMembershipPlan']['price']; ?></td>
                              <?php } ?>
                          <?php endforeach; ?>
                        </tr>
                        <tr>
                            <td></td>
                        <?php foreach ($plans as $plan) : ?>
                            <td><button type="button" class="btn btn-primary employee-btn">Buy Now</button></td>
                        <?php endforeach; ?>
                        </tr>
                      </tbody>
                    </table>
              </div>

          </div>
      </div>
     </div>
  <h3 class="text-center px-3 d-block employee-bottom-text">New to Hotedin.com? Save 20%
  on your first job when you buy online</h3>
</section>

<!--cvs section-->
<section class="video-cvs-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6 left-bg">
        <div class="cvsleft-box">
          <h1><?php echo $homecontent['ManageHomepage']['title'] ?></h1>
          <p><?php echo $homecontent['ManageHomepage']['description'] ?></p>
          <div class="cvsbtn"><a href="#">Know More</a></div>
        </div>
      </div>

      <div class="col-sm-6">
      <div class="video-box">
      <div class="row">
      <iframe width="100%" height="550" src="<?= $homecontent['ManageHomepage']['youtube_link'] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
      </div>
      </div>
    </div>
  </div>
</section>




<div class="container">
      <div class="row pb-5">
          <h1  class="dashboard-heading candidate-heading-color text-center w-100"><?php echo $wppages1['WpPages']['title'];  ?></h1>
          <p class="candidate-paragraph text-center py-3  w-100 pb-5">
              <?php echo $wppages1['WpPages']['heading'];  ?></p>

          <div class="col-sm-12 col-md-5 text-center">
              <img class="img-fluid" src="<?php echo $this->webroot ?>/cms_page/<?php echo $wppages1['WpPages']['image'];  ?>">
          </div>

          <div class="col-sm-12 col-md-7 mt-3">
                  <p class="employee-paragraph pb-4"><?php echo $wppages1['WpPages']['content'];  ?></p>
                  <div class="register-btn"><a href="#">Sign Up As a registered employer</a></div>
          </div>
      </div>
  </div>



<section class="hotedin-section">
<div class="container">
      <div class="row">
          <div class="col-sm-10 offset-sm-1 text-center">
              <div class="hotedin-box">
                  <h1>New to Hotedin.com?</h1>
                  <h2>Save <span>20%</span> on your first job when you buy online</h2>
                  <div class="hotedin-btn"><a href="#">REGISTER TODAY</a></div>
              </div>
          </div>
      </div>
  </div>
</section>
<!--end about section-->

<!--end hotedin section-->
<section class="clients-section">
 <div class="container">
       <div class="row">
         <h1> Our Clients</h1>
         <h2>what you need to know</h2>
         <ul class="clients">
           <?php foreach ($Clients as $Client) { ?>
               <li>
                 <div class="logo-box">
                   <img src="<?php echo $this->webroot; ?>client/<?php echo $Client['Client']['image'] ?>">
                 </div>
                 <p><?php echo $Client['Client']['name'] ?></p>
               </li>
           <?php } ?>
         </ul>
       </div>
     </div>
</section>
<!--end clients section-->

<section class="blog-section">
<div class="container">
    <div class="row">
      <h1><?php echo $homecontent3['ManageHomepage']['title'] ?></h1>
      <p><?php echo $homecontent3['ManageHomepage']['description'] ?></p>
      <?php foreach ($blogs as $blog) : ?>
          <div class="col-xs-12 col-sm-12 col-md-4 text-center">
            <div class="blog-panel">
              <div class="blog-panel-image">
                <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['background_image'] ?>">
              </div>
              <div class="middle-image-box">
                <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['image'] ?>">
              </div>
              <h1><?php echo $blog['Blog']['title'] ?></h1>
              <h2><?php echo $blog['Blog']['short_description'] ?></h2>
              <div class="Leaders-btn"><a href="#">Read More</a></div>
            </div>
          </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<!--end blog section-->
    <!-- Bootstrap core JavaScript -->
    <script>
      $(window).scroll(function(){
      var sticky = $('.sticky'),
          scroll = $(window).scrollTop();

      if (scroll >= 50) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
    </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                dots:true,
                nav: false,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          </script>
