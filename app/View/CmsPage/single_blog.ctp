
    <div class="container">

        <div class="row" style="padding:20px 0;">

            <!-- Blog Post Content Column -->
            <div class="col-md-9" style="margin-bottom:20px;">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>Blog Post Title</h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">Start Errand</a>
                </p>

                 <p> <div class="follow" style="overflow:hidden;">
                                       
                                        <a href="<?php echo $sitesetting['SiteSetting']['facebook_url'];?>" class="fa fa-facebook" style="background:#153892"></a>
                                        <a href="<?php echo $sitesetting['SiteSetting']['twitter_url'];?>" class="fa fa-twitter" style="background:#1eacfb"></a>
                                        <a href="<?php echo $sitesetting['SiteSetting']['twitter_url'];?>" class="fa fa-google-plus" style="background:red"></a>
                                        <a href="<?php echo $sitesetting['SiteSetting']['twitter_url'];?>" class="fa fa-pinterest-p" style="background:#820a0f"></a>
                                       
                                </div>
                            </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on August 29, 2016 at 11:00 PM</p>

                <hr>

                <!-- Preview Image -->
                <img style="margin:0 auto;" class="img-responsive" src="<?php echo $this->webroot?>images/single_blog.jpg" alt="">

                <hr>

                <!-- Post Content -->

                 
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

                <h3>Lorem Ipsum</h3>

                <img style="margin:0 auto;" class="img-responsive" src="<?php echo $this->webroot?>images/single_blog.jpg" alt="">
                <hr>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>


                <h3>Lorem Ipsum</h3>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

                <h3>Lorem Ipsum</h3>

                <img style="margin:0 auto;" class="img-responsive" src="<?php echo $this->webroot?>images/single_blog.jpg" alt="">
                <hr>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

                <h3>Lorem Ipsum</h3>

                <img style="margin:0 auto;" class="img-responsive" src="<?php echo $this->webroot?>images/single_blog.jpg" alt="">
                <hr>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

               

                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Start Errand
                            <small>August 29, 2016 at 11:30 PM</small>
                        </h4>
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                    </div>
                </div>

                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Start Errand
                            <small>August 29, 2016 at 11:30 PM</small>
                        </h4>
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        <!-- Nested Comment -->
                        <!-- <div class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="http://placehold.it/64x64" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Nested Start Errand
                                    <small>August 29, 2016 at 11:30 PM</small>
                                </h4>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div> -->
                        <!-- End Nested Comment -->
                    </div>
                </div>

            </div>

            <!-- Blog Sidebar Widgets Column -->

            <div class="col-md-3 text-center">

                <div class="alliswell">
                    <h4>Blog Categories</h4>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                           <!--  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Categories 1
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
       <ul class="list-unstyled">
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
           Categories 2
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <ul class="list-unstyled">
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
           Categories 3
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <ul class="list-unstyled">
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        </ul>
      </div>
    </div>
  </div>
</div>  -->


 
    <div class="accordion" id="accordion2">
<div class="accordion-group">
<div class="accordion-heading" style="">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
Categories 1
</a>
</div>
<div id="collapseOne" class="accordion-body collapse in">
<div class="accordion-inner">
<ul class="list-unstyled">
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        </ul>
</div>
</div>
</div>
<div class="accordion-group">
<div class="accordion-heading" >
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
Categories 2
</a>
</div>
<div id="collapseTwo" class="accordion-body collapse">
<div class="accordion-inner">
<ul class="list-unstyled">
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        <li>Cateogories</li>
        </ul>
</div>
</div>
</div>
</div>









                        </div>
                    </div>
                </div>
           <hr>

        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Related Blogs</h3>
                <hr>
            </div>
             <div class="col-md-4 text-center zoom-effect-container">
                <div class="gap">

                    <div class="image-card">
                      <div class="overlay">
                      <h3>Read more</h3></div>  
             <img  style="margin:0 auto;"  class="img-responsive"  src="<?php echo $this->webroot?>images/single_blog.jpg"> </div>


                <!-- Pager -->
                <h3>Blog Title Of the Errand Blog </h3>
               </div>

            </div>
             <div class="col-md-4 text-center zoom-effect-container">
                <div class="gap">

                    <div class="image-card">
                      <div class="overlay">
                      <h3>Read more</h3></div>  
             <img  style="margin:0 auto;"  class="img-responsive"  src="<?php echo $this->webroot?>images/single_blog.jpg"> </div>


                <!-- Pager -->
                <h3>Blog Title Of the Errand Blog </h3>
               </div>

            </div>
             <div class="col-md-4 text-center zoom-effect-container">
                <div class="gap">

                    <div class="image-card">
                      <div class="overlay">
                      <h3>Read more</h3></div>  
             <img  style="margin:0 auto;"  class="img-responsive"  src="<?php echo $this->webroot?>images/single_blog.jpg"> </div>


                <!-- Pager -->
                <h3>Blog Title Of the Errand Blog </h3>
               </div>

            </div>

        </div>

        <hr>

        <!-- Footer -->
       

    </div>
    <!-- /.container -->

    <style type="text/css">
    .alliswell {border: 1px solid #ccc; min-height: 30px; padding: 19px;}
    ul.list-unstyled > li {padding: 2px 0; text-align: left;}
   .panel-title {text-align:left;}


    .accordion-toggle:after {
    font-family: 'FontAwesome';
    content: "\f078";    
    float: right;
}
.accordion-opened .accordion-toggle:after {    
    content: "\f054";    
}

.accordion-heading {background-image: linear-gradient(to bottom, #f5f5f5 0%, #e8e8e8 100%);color: #333333;background-color: #f5f5f5;border-color: #dddddd; padding: 10px;
text-align: left;}
    </style>

    <script type="text/javascript">
    $(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });

    </script>