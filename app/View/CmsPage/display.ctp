<section class="inner_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="heading_part">
					<h2><?php
					echo $cmsPage['CmsPage']['page_title'];
					?></h2>
					<?php
					echo $cmsPage['CmsPage']['page_description'];
					?>
				</div>
			</div>
		</div>
	</div>
</section>
