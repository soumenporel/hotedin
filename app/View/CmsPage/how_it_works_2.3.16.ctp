<!--<section class="populer_service">
        <div class="container">
		<div class="row">
		        <div class="col-md-12" >
		    			<h2><u>How it works</u></h2>
			</div>
		</div>
		<div class="row" style="width: 75%;margin: 0 auto;">
		        <div class="col-md-12" >
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer.</p>
			</div>
		</div>
            
            
	</div>
</section>-->


<section class="how-baner-section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-7">
                <div class="intro"><div class="heading"><h1>How to assign an Errand Champion to my errand</h1></div><div class="sub-heading"><h5 style="font-size: 19px;color: #fff;">To assign an Errand Worker to your errand, simply log in to your Errand Champion account and go to your errand (you can find it in the <a href="www.Errand Champion.com/my-errands"> My Errands section</a>. link- www.Errand Champion.com/my-errands).</h5></div>
                <h5 style="font-size: 19px;
color: #fff;">Go to the 'Offers' section of the page where you will see a summary of offers and the offered prices. After selecting an offer you are happy with, hit the 'view offer' button of that Errand Worker and if you wish to assign the errand click the pink “Assign Errand” button at the bottom of the dialogue box.<br><br>
The errand is now assigned and you can communicate the finer details of the errand using the Private Message feature!</h5>
                <div class="buttons-heading">
                <!-- <h5>Find out more about:</h5> -->
            </div><!-- <div class="buttons"><a class="task-btn" href="#">Posting Tasks</a><a class="task-btn" href="#">Completing Tasks</a></div> --></div>
                </div>
                <div class="col-md-5">
                <div class="iphone-img"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section class="body-content">
    <div class="row">
    	<div class="container">
        	
           <!--  <h1>For Job Posters</h1> -->
            	<div class="col-md-8">
                	<div class="content" style="margin-bottom:25px;">

                        <h3>How to Use Us – Errand Poster </h3>
                        <div class="tagline">
                        <p>1. Create a free account using Errand Champion online or on your mobile device.</p>
                        <p>2. Post your <strong>free</strong> Ad and tell us about the errands you need completing.</p>
                        <p>3. Review multiple applications from our Errand workers. Receive applications and use our community review system to select someone you like.</p>
                        <p>4. Agree on a suitable price and deposit the Errand Funds via Errand Champion Safepay which uses Paypal.</p>
                        <p>5. Send messages via Errand Worker to arrange for errand completion.  Once you are happy the errand is done our contractor will request Payment and you will Release the money.</p>

                        <!-- <a href=""><p>See Task Suggestions</p></a> --></div>


                        <h3>How to Use Us – Errand Champion</h3>
                        <div class="tagline">
                        <p>1. Look through our database of errands and find an errand that’s right for you.  Refine your search based on your location and your skillset.</p>
                        <p>2. Once you have found an errand you like.  Make an offer and notify the errand owner of anything you would like them to know. </p>
                        <p>3. If your offer is accepted you will be notified via your Errand Champion notification and be assigned to the errand.  Talk to the owner via the message system to arrange suitable completion.  In the meantime we will collect your funds from the owner via our Errand Champion Safepay.</p>
                        <p>4. Once you have completed the errands - its pay time! Request payment and we will release money into your Paypal accounts. It’s that simple!</p>
                        

                        <!-- <a href=""><p>See Task Suggestions</p></a> --></div>

                    </div>
                </div>
                <div class="col-md-4">
                	<div class="post-1-img" style="margin-top: 200px;"><img src="<?php echo $this->webroot?>images/hiw-post-1.jpg"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="grey-bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-4">
                    	<div class="post-img"><img src="<?php echo $this->webroot?>images/mail.png" alt=""/></div>
                    </div>
                    <div class="col-md-8">
                    <div class="txt"><h3>How to send private message</h3><div class="tagline"><p>Private messaging is only possible between the poster and the worker once the errand is assigned. Private messages are prohibited bidding on an errand. <br>
Once an errand is assigned the two Errand Champion Members can communicate about the errand e.g. Exchanging personal details as phone numbers, addresses etc.<br><strong>Privacy protection is very important to us and our software ensuring that your information is safe and no spam can reach your account.</strong>

</p></div></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
        	<div class="row">
            	<div class="col-md-8">
                	<div class="content" style="margin-bottom:50px;">
                        <h3>How does an errand become overdue</h3>
                        <div class="tagline">
                        <p>Once an errand has passed the deadline date given by the Errand Poster and the Errand Worker has not yet <strong>completed/marked your errand as complete, the errand is said to be overdue.</strong>
<strong>As an Errand Poster</strong> ensure that your assigned Errand Worker completes the errand on time. Use private messaging to follow up with the assigned Errand Worker if the errand remains overdue.</p>

                        </div>

                      <h3>Best way to ensure your errand gets done</h3>
                        <div class="tagline">
                        <p>Provide as much information as possible about the errand and offer a price that is fair for the job.<br>
To get the best price and the best Errand Worker for your job, we suggest you to spread word about your errand on social networks. It will ensure more people as possible seeing your errand. It’s really easy just use the share button.</p>

                        <!-- <a href=""><p>See Task Suggestions</p>
                    </a> --></div>



                </div>
                </div>
                <div class="col-md-4">
                	<div class="post-1-img" style="margin-top:100px;"><img src="<?php echo $this->webroot?>images/post-img-3.png" alt=""/></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="grey-bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-4">
                    	<div class="post-img" style="margin-top:150px;"><img src="<?php echo $this->webroot?>images/tick.png" alt=""/></div>
                    </div>
                    <div class="col-md-8">
                    <div class="txt">
                        <h3>What will happens if no one offers to help me with the errand</h3>
                        <div class="tagline"><p>More people who see your errand, the more offers you’re likely to receive.<br>
Increase the number of people who see your errand by sharing it on social networks, just click on the share button.<br>
Provide as much info as possible about the errand you need done and make sure that you’ve offered a price that is fair for the job.</p>
                        </div>

                        <p><strong>If multiple Errand Worker offer to complete my errand, then what do I do?</strong>   </p>
                        <div class="tagline"><p>Your posted errand is gaining popularity among the Errand Worker. Congratulations!<br>
Assess each Errand Worker profile and assign the most suitable person to your errand. To know more about each person, ask them in the comment section, just be mindful not to ask for any private details. Check the Verification Badges, Reviews and References of your Errand Worker to make a wise decision. Happy choosing!</p>
                        </div>

                        <p><strong>What to do if my errand gets expired </strong>   </p>
                        <div class="tagline"><p>If the deadline date is passed and the errand has not been assigned to an Errand Worker, the errand will expire and will be removed from the errands feed. You can re-post your errand to find another Errand Worker in the meantime. All you have to do is edit errand and update the due date.</p>
                        </div>


                    </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>
        <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="content" style="margin-bottom: 60px;">
                                <h3>What happens if the Errand Champion worker doesn’t complete the errand.</h3>
                                <div class="tagline">
                                <p>We advise you find an alternative solution with the Errand Champion worker. The date of completion can be changed.<br>
Follow these steps if the work if alternate is not found:
<ul>
<li>IPlease contact us and inform us team if you have already released the fundsnform the Errand Champion worker via Errand Champion Private Messages that you are going to cancel the errand</li>
<li>Please contact us and inform us team if you have already released the funds</li>
<li>Re-post your errand:</li>

</ul>
Please contact our support team for any other questions.

</p>

                                </div>

                              <h3>How to edit or cancel/delete my Errand</h3>
                                <div class="tagline">
                                <p>If you want to cancel an errand that has been assigned, we advise you to follow the steps below:<br>
 Inform the Errand Champion worker via Errand Champion Private messaging that you will have to cancel the errand. If the errand has not been assigned, you can update or delete your errand by following the below Quick Steps:
<ol>
    <li>Log in to Errand Champion from a web browser and click on "My errands” tab</li>
     <li>Select the respective errand and click on the 'edit' icon below your errand title and cancel errand accordingly.</li>
</ol>

</p>

                                <!-- <a href=""><p>See Task Suggestions</p>
                            </a> --></div>



                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="post-1-img" style="margin-top: 175px;"><img src="<?php echo $this->webroot?>images/tick.png" alt=""/></div>
                        </div>
                    </div>
                </div>

        
    </section>
    <div class="clearfix"></div>
    <section class="task-section">
    <div style="margin-top:120px; margin-bottom: 120px" class="container"><h3 style="margin-top: 0; margin-bottom:20px; width:auto; display: block;">Ready to go?</h3><div class="txt"><a class="task-btn" href="#">Post A task</a></div>
    </section>

