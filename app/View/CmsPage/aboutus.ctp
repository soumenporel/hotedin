<!--login bg-->
<section class="about-page-section mb-5" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
  <div class="container">
      <div class="employee-banner-part">
          <h1 class="d-block text-center about-heading-color">New to Hotedin.com?</h1>
          <p class="d-block text-center about-small-heading">Save <strong>20%</strong> on your first job when you buy online</p>
      </div>
  </div>
  <h3 class="text-center px-3 d-block employee-bottom-text">New to Hotedin.com? Save 20%
  on your first job when you buy online</h3>
</section>
<div class="container">
      <div class="row pb-5">
          <h1  class="dashboard-heading candidate-heading-color text-center w-100"><?php echo $wppages1['WpPages']['heading'] ?></h1>
          <p class="candidate-paragraph text-center py-3  w-100 pb-5"><?php echo $wppages1['WpPages']['content'] ?></p>

          <div class="col-sm-12 col-md-6 text-left">
              <img class="img-fluid" src="<?php echo $this->webroot; ?>cms_page/<?php echo $wppages2['WpPages']['image'] ?>">
          </div>

          <?php echo $wppages2['WpPages']['content'] ?>

      </div>
</div>
<section class="clients-section">
<div class="container">
  <div class="row">
      <h1><?php echo $wppages3['WpPages']['heading'] ?></h1>
      <?php echo $wppages3['WpPages']['content'] ?>
      <div class="col-sm-12 col-md-6">
          <img class="img-fluid" src="<?php echo $this->webroot; ?>cms_page/<?php echo $wppages3['WpPages']['image'] ?>">
      </div>
          <p style="color: #fff;line-height: normal;" class="py-5 text-center">With HotedIn, you can count on a greater awareness of who it is that you are considering for your positions. That is going to create real results in your business, because you are going to end up with a curated staff that borders on perfect.<br><br>

          With an integrated marketing strategy, Hotedin have made its presence known across the Hospitality Industry. Hiring managers and other professionals all around the country are realizing what a game-changer HotedIn is in the world of hiring.</p>
  </div>
</div>
</section>

<section class="hotedin-section">
<div class="container">
      <div class="row">
          <div class="col-sm-10 offset-sm-1 text-center">
              <div class="hotedin-box">
                  <h1>New to Hotedin.com?</h1>
                  <h2>Save <span>20%</span> on your first job when you buy online</h2>
                  <div class="hotedin-btn"><a href="#">REGISTER TODAY</a></div>
              </div>
          </div>
      </div>
  </div>
</section>
<!--end about section-->

<!--end hotedin section-->
<section class="clients-section">
 <div class="container">
       <div class="row">
         <h1> Our Clients</h1>
         <h2>what you need to know</h2>
         <ul class="clients">
           <?php foreach ($Clients as $Client) { ?>
               <li>
                 <div class="logo-box">
                   <img src="<?php echo $this->webroot; ?>client/<?php echo $Client['Client']['image'] ?>">
                 </div>
                 <p><?php echo $Client['Client']['name'] ?></p>
               </li>
           <?php } ?>
         </ul>
       </div>
     </div>
</section>
<!--end clients section-->

<section class="blog-section">
<div class="container">
    <div class="row">
      <h1><?php echo $homecontent3['ManageHomepage']['title'] ?></h1>
      <p><?php echo $homecontent3['ManageHomepage']['description'] ?></p>
      <?php foreach ($blogs as $blog) : ?>
          <div class="col-xs-12 col-sm-12 col-md-4 text-center">
            <div class="blog-panel">
              <div class="blog-panel-image">
                <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['background_image'] ?>">
              </div>
              <div class="middle-image-box">
                <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['image'] ?>">
              </div>
              <h1><?php echo $blog['Blog']['title'] ?></h1>
              <h2><?php echo $blog['Blog']['short_description'] ?></h2>
              <div class="Leaders-btn"><a href="#">Read More</a></div>
            </div>
          </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<!--end blog section-->
    <!-- Bootstrap core JavaScript -->
    <script>
      $(window).scroll(function(){
      var sticky = $('.sticky'),
          scroll = $(window).scrollTop();

      if (scroll >= 50) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
    </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                dots:true,
                nav: false,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          </script>