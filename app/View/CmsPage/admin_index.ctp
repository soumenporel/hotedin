<?php
//pr($contents);
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<div class="contents index wp_pages_index">
    <h2 style="width:400px;float:left;"><?php echo __('Contents'); ?></h2>
<form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">   
            <table style=" border:none;" class="data_table">
                <tr>
                    <td width="10%">Keyword</td>
                    <td width="30%"><input type="text" name="keyword" value="<?php echo isset($keywords)?$keywords:'';?>" placeholder="Search by Keyword." class="textfield"></td>
                    <td width="10%">Activity Status</td>
                    <td><select name="search_is_active" class="selectbox" id="search_is_active">
                            <option value="" >Select Option</option>
                            <option value="1" <?php echo (isset($is_active) && $is_active=='1')?'selected':'';?>>Active</option>
                            <option value="0" <?php echo (isset($is_active) && $is_active=='0')?'selected':'';?>>Inactive</option>
                        </select></td>
                    <td><input type="submit" name="search" value="Search" class="search_button"></td>
                </tr> 
                 <tr><a href="<?php echo($this->webroot);?>admin/cms_page/add" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New CMS Page</a>
            </tr>      
            </table>
        </form>
<table style=" border:none;" class="bulk-action">
    <tr>
        <td width="9%">Bulk Action :</td> 
         <td width="27%"><select name="action_type" id="action_type" class="select_box1">
                <option value="" >Select Option</option>
                <option value="1" >Delete</option>
                <option value="2" >Approve</option>
                <option value="3" >Disapprove</option>
            </select>
            </td>
            <td>
     
            <button id="bulk_action" type="button" class="btn btn-primary">Action</button>
        </td>
    </tr> 
</table>

    <table cellpadding="0" cellspacing="0"  id="sortable">
        <thead>
            <tr>
                <th width="10%"><input type="checkbox" id="sel_all" value="1"/>Select All</th>
                <th>
                    <?php echo $this->Paginator->sort('id'); ?>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('page_name'); ?>
                </th>
                <th width="29%">
                    <?php echo $this->Paginator->sort('page_heading'); ?>
                </th>
                <!-- <th>
                    <?php echo __('Status'); ?>
                </th>
                <th>
                    <?php echo __('Show In Header'); ?>
                </th>
                <th>
                    <?php echo __('Show In Footer'); ?>
                </th> -->
                 <th>
                    <?php echo __('Preview'); ?>
                </th>
                <th>
                    <?php echo __('Actions'); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($contents as $key => $content) :
            ?>
            <tr data-id="<?php echo $content['CmsPage']['id']; ?>">
                <td>
                    <input type="checkbox" class="check" name="cmc_id[]" value="<?php echo ($content['CmsPage']['id']); ?>">
                </td>
                <td>
                    <?php echo ++$key; ?>&nbsp;
                </td>
                <td>
                    <?php echo h($content['CmsPage']['page_title']);?>
                </td>
                <td>
                    <?php echo substr(strip_tags($content['CmsPage']['page_description']), 0, 200).'...';?>
                </td>
               <!--  <td><?php if($content['CmsPage']['status']==1){ ?> <img src="<?php echo $this->webroot; ?>/img/success-01-128.png" style="height:30px;" /><?php }else{ ?><img src="<?php echo $this->webroot; ?>/img/cross-512.png" style="height:30px;" /><?php } ?>&nbsp;</td>
                <td>
                    <?php echo ($content['CmsPage']['show_in_header'] == '1') ? 'True': 'False'; ?>
                </td>
                <td>
                    <?php echo ($content['CmsPage']['page_title'] == '1') ? 'True': 'False'; ?>
                </td> -->
                <td>
                    <button class="btn btn-info btn-lg cms_preview" data-id="<?php echo $content['CmsPage']['id']; ?>" data-toggle="modal">Preview</button>
                </td>    
                <td>
                    <?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-plus')),
                    array('action' => 'add', $content['CmsPage']['id']),
                    array('class' => 'btn btn_circle btn-add', 'escape'=>false));
                    ?>
                    <?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                    array('action' => 'edit', $content['CmsPage']['id']),
                    array('class' => 'btn btn_circle btn-edit', 'escape'=>false));
                    ?>
                    <?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                    array('action' => 'delete', $content['CmsPage']['id']),
                    array('class' => 'btn btn_circle btn-del', 'escape'=>false));
                    ?>
                </td>
            </tr>
            <?php
            endforeach;
            ?>
        </tbody>
    </table>
    <p><?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?></p>
    <div class="paging">
	<?php
            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog modal-lg">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">CMS Page</h4>
    </div>
    <div class="modal-body" id="modal_body">
        <div class="row">
            <div class="col-md-12" id="modal_content" >
            </div>    
        </div>    
      
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  
</div>
</div>

<script>
(function($) {
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $("#sortable tbody").sortable({
        update: function(event, ui) {  
            var sort_order = [];
            $('#sortable tbody tr').each(function() {
                $(this).children('td:first-child').html($(this).index() + 1);
                //$(this).attr('data-order', $(this).index() + 1);
                sort_order.push($(this).attr('data-id'));
            });
            
            $.ajax({
                url: '<?php echo $this->webroot; ?>admin/cms_page/ajaxorder',
                type: 'post',
                //dataType: 'json',
                data: {
                    sort_order: sort_order
                },
                success: function(data) {
                    //console.log(data);
                }
            });
        },
        helper: fixHelper
    }).disableSelection();
    
})(jQuery);
</script>
<script>
    (function ($) {
        var total_check = $("input[name='cmc_id[]']").length;
        
        $('#sel_all').click(function(){
            if($(this).is(':checked')) {
                $('input[name="cmc_id[]"]').prop('checked', true);
            } else {
                $('input[name="cmc_id[]"]').prop('checked', false);
            }
            
        });
        
        $("input[name='cmc_id[]']").click(function(){
            var check_count = $("input[name='cmc_id[]']:checked").length;
            if(check_count == total_check) {
                $('#sel_all').prop('checked', true);
            } else {
                $('#sel_all').prop('checked', false);
            }
        });
        
        $('#bulk_action').click(function () {
            var action_type = $('#action_type').val();
            if(action_type!=''){

                var cms_ids = [];
                $("input[name='cmc_id[]']:checked").each(function () {
                    cms_ids.push($(this).val());
                });
                var size = cms_ids.length;
                if(size!=0){
                    
                    $.ajax({
                        url: "<?php echo $this->webroot; ?>cms_page/bulkAction",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            action_type:action_type,
                            cms_ids:cms_ids
                        },
                        success: function(result){
                            if(result.Ack==1){
                                //alert(result.res);
                                location.reload();
                            }
                        }
                    });
                
                }
                else{
                    alert('No CMS Page Is selected');
                }
            }
            else{
                alert('No Action selected');
            }
        });
    })(jQuery);
    $(document).ready(function(){
        $('input[name="cmc_id[]"]').prop('checked', false);
        $('#sel_all').prop('checked', false);
    });

$(document).ready(function(){
    $(".cms_preview").click(function(){
        var id = $(this).data('id');
        $.ajax({
            url: "<?php echo $this->webroot;?>cms_page/ajaxCMSPrewiew",
            method:'post',
            dataType:'json',
            data:{page_id:id},
             success: function(result){
                console.log(result);
                if(result.Ack==1)
                {
                    //$("#modal_content").html('');
                    $("#modal_content").html(result.html);
                    $('#myModal').modal('show');
                }
            }
        });
    });
});    

</script>