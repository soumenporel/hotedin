<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="blogs form">
<?php echo $this->Form->create('Tag', array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Tag'); ?></legend>
        <?php
        echo $this->Form->input('name',array('required'=>'required'));
        echo $this->Form->input('Post',array('required'=>'required','class'=>'selectpicker','multiple'=>'multiple','style'=>'width:499px; padding:5px 0'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script>
$(document).ready(function() {
  $(".selectpicker").select2();
});
</script>
