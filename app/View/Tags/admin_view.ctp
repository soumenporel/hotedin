<div class="roles view">
    <h2><?php echo __('Tag'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($tag['Tag']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($tag['Tag']['name']); ?>
            &nbsp;
        </dd>
    </dl>
</div>