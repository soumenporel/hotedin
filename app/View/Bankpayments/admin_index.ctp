<div class="users index">
    <h2 style="width:100%;float:left;"><?php echo __('Order'); ?></h2>


    <table cellpadding="0" cellspacing="0">
        <tr>

            <th>SN<?php //echo $this->Paginator->sort('id');       ?></th>
            <th><?php echo $this->Paginator->sort('first_name'); ?></th>
            <th style="width: 9%"><?php echo $this->Paginator->sort('last_name'); ?></th>
            <th><?php echo $this->Paginator->sort('email_address'); ?></th>
            <!-- <th>Country</th> -->
            <th style="width: 9%"><?php echo $this->Paginator->sort('Bank'); ?></th>
            <th><?php echo $this->Paginator->sort('Account No'); ?></th>
            <th><?php echo $this->Paginator->sort('payment_date', 'Payment Date'); ?></th>

            <th style="width: 9%"><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php
        $OrderCnt = 0;
        $uploadImgPath = WWW_ROOT . 'user_images';
        foreach ($users as $user):
            //pr($user);
            $OrderCnt++;
            ?>
            <tr>

                <td><?php echo $OrderCnt; //echo h($user['Order']['id']);       ?>&nbsp;</td>
                <td><?php echo h($user['User']['first_name']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['last_name']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['email_address']); ?>&nbsp;</td>
                <td><?php echo h($user['Bank']['bank_name']); ?>&nbsp;</td>
                <td><?php echo h($user['Order']['account_no']); ?>&nbsp;</td>
                <td><?php echo h($user['Order']['payment_date']); ?>&nbsp;</td>
                <td><?php echo h($user['Order']['status']); ?>&nbsp;</td>
                <td style="width: 120px">

                    <button class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Details" onclick="window.open('<?php echo $this->webroot;?>bank_docs/<?php echo $user['Order']['document'];?>','_blank')"><i class="fa fa-eye"></i></button>

                    <?php
                    echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $user['Order']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false));
                    ?>

                    <?php
                    echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')), array('action' => 'delete', $user['Order']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $user['Order']['id']));
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>

<style>
    .title a{
        color: #fff !important;
    }
</style>

<script>
    (function ($) {
        var total_check = $("input[name='user_id[]']").length;

        $('#sel_all').click(function () {
            if ($(this).is(':checked')) {
                $('input[name="user_id[]"]').prop('checked', true);
            } else {
                $('input[name="user_id[]"]').prop('checked', false);
            }

        });

        $("input[name='user_id[]']").click(function () {
            var check_count = $("input[name='user_id[]']:checked").length;
            if (check_count == total_check) {
                $('#sel_all').prop('checked', true);
            } else {
                $('#sel_all').prop('checked', false);
            }
        });

        $('#bulk_action').click(function () {
            var action_type = $('#action_type').val();
            if (action_type != '') {

                var user_ids = [];
                $("input[name='user_id[]']:checked").each(function () {
                    user_ids.push($(this).val());
                });
                var size = user_ids.length;
                if (size != 0) {

                    $.ajax({
                        url: "<?php echo $this->webroot; ?>users/bulkAction",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            action_type: action_type,
                            user_ids: user_ids
                        },
                        success: function (result) {
                            if (result.Ack == 1) {
                                //alert(result.res);
                                location.reload();
                            }
                        }
                    });

                }
                else {
                    alert('No Order Is selected');
                }
            }
            else {
                alert('No Action selected');
            }
        });
    })(jQuery);
    $(document).ready(function () {
        $('input[name="user_id[]"]').prop('checked', false);
        $('#sel_all').prop('checked', false);
    });
</script>
</div>
</div>
</div>
