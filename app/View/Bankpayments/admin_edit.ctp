<div class="row-fluid">
	<div class="span12">
		<div class="widget green">
			<!-- <div class="widget-title">
				<h4><i class="icon-reorder"></i>Edit Application</h4>
				<span class="tools">
				<a href="javascript:;" class="icon-chevron-down"></a>
				<a href="javascript:;" class="icon-remove"></a>
				</span>
			</div> -->
			<div class="widget-body">
<div class="users form">
    <?php echo $this->Form->create('Order', array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Order'); ?></legend>
        <div class="users view">
    <h2><?php echo __('User'); ?></h2>
    <dl>
       
        <dt><?php echo __('First Name'); ?></dt>
        <dd>
			<?php echo h($user['User']['first_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Last Name'); ?></dt>
        <dd>
			<?php echo h($user['User']['last_name']); ?>
            &nbsp;
        </dd>
       
        <dt><?php echo __('Email'); ?></dt>
        <dd>
			<?php echo h($user['User']['email_address']); ?>
            &nbsp;
        </dd>
       
        <dt><?php echo __('Bank'); ?></dt>
        <dd>
			<?php echo h($user['Bank']['bank_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Account No'); ?></dt>
        <dd>
			<?php echo h($user['Order']['account_no']); ?>
            &nbsp;
        </dd>
       
    </dl>
</div>
        <?php
        
        echo $this->Form->input('id');
       
       
        ?>
       <select name="data[Order][status]">
            <option value="">Change Status</option>
            <option value="Pending" <?php if($user['Order']['status']=='Pending'){ echo "selected"; }?>>Pending</option>
            <option value="Success" <?php if($user['Order']['status']=='Success'){ echo "selected"; }?>>Approved</option>
             <option value="Failure" <?php if($user['Order']['status']=='Failure'){ echo "selected"; }?>>Failure</option>
           
        </select>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script> -->
<script>
    // var autocomplete = new google.maps.places.Autocomplete($("#OrderAddress")[0], {});

    // google.maps.event.addListener(autocomplete, 'place_changed', function() {
    //     var place = autocomplete.getPlace();
    //     console.log(place.address_components);
    // });
</script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'OrderBiography',
    {
    width: "95%"
    });

$(document).ready(function(){
    $("#country_id").change(function(){
        var country_id = $(this).val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>states/ajaxStates",
            type: 'post',
            dataType: 'json',
            data: {
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#state_id').html(result.html);
                } else {
                    $('#state_id').html(result.html);
                }
            }
        });
    });

    $("#state_id").change(function(){
        var state_id = $(this).val();
        var country_id = $("#country_id").val();
        $.ajax({
            url: "<?php echo $this->webroot; ?>cities/ajaxCities",
            type: 'post',
            dataType: 'json',
            data: {
                s_id:state_id,
                c_id:country_id
            },
            success: function(result){
                if(result.ack == '1') {
                    $('#city_id').html(result.html);
                } else {
                    $('#city_id').html(result.html);
                }
            }
        });
    });
});
</script>
</div>
		</div>
	</div>
</div>
