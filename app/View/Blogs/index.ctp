<!--login bg-->
<section class="blog-page-section py-5 mb-5" style="background :url('<?php echo $this->webroot ?>banner/<?php echo $pagebanner['Pagebanner']['image'] ?>') no-repeat top center">
   <div class="container">
      <div class="row">
         <div class="dashboard-panel">
            <h1 class="dashboard-heading text-center mt-5">Blog</h1>
         </div>
      </div>
   </div>
</section>
<div class="container">
   <div class="row d-flex justify-content-center">
      <p class="blog-pragraph-text text-center py-3  w-100">Showing <?php echo $pageContent ?> Of  <?php echo $count ?> Post</p>
      <?php foreach ($blogs as $blog) : ?>
          <div class="col-xs-12 col-sm-12 col-md-4 text-center">
            <div class="blog-panel">
              <div class="blog-panel-image">
                <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['background_image'] ?>">
              </div>
              <div class="middle-image-box">
                <img src="<?php echo $this->webroot; ?>blogs_image/<?php echo $blog['Blog']['image'] ?>">
              </div>
              <h1><?php echo $blog['Blog']['title'] ?></h1>
              <h2><?php echo $blog['Blog']['short_description'] ?></h2>
              <div class="Leaders-btn"><a href="#">Read More</a></div>
            </div>
          </div>
      <?php endforeach; ?>
   </div>
</div>
<section class="hospitality">
   <div class="container-fluid">
    <div class="row">
    <?php foreach ($AppPromotions as $AppPromotion) { ?>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <img class="img-responsive" src="<?php echo $this->webroot; ?>appPromotions_image/<?php echo $AppPromotion['AppPromotion']['image'] ?>">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <div class="hospitality-right">
            <h1><?php echo $AppPromotion['AppPromotion']['title'] ?></h1>
            <div class="hospitality-list">
                <?php $AppPromotion['AppPromotion']['points'];
                    $points = explode(',',$AppPromotion['AppPromotion']['points']);
                 ?>
              <ul>
            <?php foreach ($points as $point) { ?>
                <li><i class="icon ion-ios-checkmark-empty"></i><?php echo $point ?></li>
            <?php } ?>

              </ul>
            </div>
            <h2><?php echo $AppPromotion['AppPromotion']['description'] ?></h2>
            <ul class="star-listing">
              <li><i class="icon ion-android-star"></i></li>
              <li><i class="icon ion-android-star"></i></li>
              <li><i class="icon ion-android-star"></i></li>
              <li><i class="icon ion-android-star"></i></li>
              <li><i class="icon ion-android-star"></i></li>
            </ul>
            <h3>Download the job app now</h3>
            <ul class="star-listing btn-listing">
              <li><a href="#"><img src="<?php echo $this->webroot; ?>appPromotions_image/<?php echo $AppPromotion['AppPromotion']['android'] ?>"></a></li>
              <li><a href="#"><img src="<?php echo $this->webroot; ?>appPromotions_image/<?php echo $AppPromotion['AppPromotion']['ios'] ?>"></a></li>
            </ul>
          </div>
        </div>
    <?php }  ?>

    </div>
  </div>
</section>

<script>
   $(window).scroll(function(){
   var sticky = $('.sticky'),
      scroll = $(window).scrollTop();

   if (scroll >= 50) sticky.addClass('fixed');
   else sticky.removeClass('fixed');
   });
</script>
<script>
   $(document).ready(function() {
     var owl = $('.owl-carousel');
     owl.owlCarousel({
       margin: 10,
       dots:true,
       nav: false,
       loop: true,
       responsive: {
         0: {
           items: 1
         },
         600: {
           items: 1
         },
         1000: {
           items: 1
         }
       }
     })
   })
</script>
