<div class="container post-details">

<div class="row pt-5">

  <!-- Post Content Column -->
  <div class="col-lg-9">
<?php //print_r($blog_details); ?>
    <!-- Title -->
    <h1><?php if(isset($blog_details['Blog']['title']) && $blog_details['Blog']['title'] !='') { echo $blog_details['Blog']['title']; } ?></h1>

    <!-- Author -->
    <p>
      by
      <a href="#"><?php echo $blog_details['User']['first_name'].' '.$blog_details['User']['last_name']; ?></a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p>Posted on <?php if(isset($blog_details['Blog']['creation_date']) && $blog_details['Blog']['creation_date'] !='') { echo date('M d, Y',strtotime($blog_details['Blog']['creation_date'])).' at '.date('h:i A',strtotime($blog_details['Blog']['creation_date']));  } ?></p>

    <hr>

    <!-- Preview Image -->
    <img class="img-fluid rounded" src="<?php if(isset($blog_details['Blog']['image']) && $blog_details['Blog']['image'] !='') {echo $this->webroot.'blogs_image/'.$blog_details['Blog']['image'];}else{echo $this->webroot.'noimage.png'; } ?>" alt="">

    <hr>

    <!-- Post Content -->
    <p><?php if(isset($blog_details['Blog']['description']) && $blog_details['Blog']['description'] !='') { echo $blog_details['Blog']['description']; } ?></p>

    <hr>

    <!-- Comments Form -->
      <?php if($userid !=''){ ?>
    <div class="card my-4">

      <h5 class="card-header">Leave a Comment:</h5>
      <div class="card-body">
          <form action="" method="post">
              <input type="hidden" name="blog_id" value="<?php echo $blog_details['Blog']['id'] ?>" class="inp" >
            <input type="hidden" name="user_id" value="<?php echo $userid; ?>" class="inp" >
             <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email Address" required>
          </div>
             <div class="form-group">
           <input type="text" name="website" class="form-control" placeholder="Website">
          </div>
          <div class="form-group">
           <textarea name="comment" class="form-control" placeholder="Write Your Comment" required></textarea>
          </div>
          <button type="submit" class="btn btn-primary">Post Comment</button>
        </form>
      </div>

    </div>
  <?php } ?>
    <?php if(!empty($blog_details['BlogComment'])) { ?>
    <h1 class="mt-4 mb-3">Popular Comments </h1>
    <?php foreach ($blog_details['BlogComment'] as $val) { ?>
    <!-- Single Comment -->
    <div class="media mb-4">
        <div class="">
          <img class="d-flex mr-3 rounded-circle" src="<?php if(isset($val['User']['user_image']) && $val['User']['user_image'] !='') {echo $this->webroot.'user_images/'.$val['User']['user_image'];}else{echo $this->webroot.'noimage.png'; } ?>" alt="">
        </div>

      <div class="media-body">
        <h5 class="mt-0"><?php echo $val['User']['first_name']; ?></h5>
        <p><?php echo $val['comment']; ?></p>
        <div class="date font-12"><?php echo date('M d, Y',strtotime($val['creation_date'])); ?> at <?php echo date('h:i A',strtotime($val['creation_date'])); ?></div>
      </div>
    </div>

    <?php } } ?>

  </div>
  <div class="col-lg-3">
                <?php //print_r($featured_blog); ?>
                 <?php //print_r($category_blog); ?>
                <?php foreach($category_blog as $cat){ ?>
              <div class="side-list-warp">
                <h4><?php echo $cat['BlogCategory']['category_name']; ?></h4>
                <ul class="side-list">
                    <?php foreach($cat['Blog'] as $blog){ ?>
                  <li>
                    <a href="<?php echo $this->webroot.'blogs/details/'.$blog['slug']; ?>">
                      <img src="<?php if(isset($blog['image']) && $blog['image'] !='') { echo $this->webroot.'blogs_image/'.$blog['image']; } else { echo $this->webroot.'noimage.png'; } ?>" alt="">
                      <div class="text"><?php echo $blog['title']; ?></div>
                    </a>
                  </li>
                    <?php } ?>
                </ul>
              </div>
                <?php } ?>
             
<!--                feature.........................................................-->
        <?php if(count($featured_blog)>0){ ?>
            <div class="side-list-warp">
                <h4>Featured Blogs</h4>
                <ul class="side-list">
                  <?php foreach($featured_blog as $bl){ ?>   
                  <li>
                    <a href="<?php echo $this->webroot.'blogs/details/'.$bl['Blog']['slug']; ?>">
                      <img src="<?php if(isset($bl['Blog']['image']) && $bl['Blog']['image'] !='') { echo $this->webroot.'blogs_image/'.$bl['Blog']['image']; } else { echo $this->webroot.'noimage.png'; } ?>" alt="">
                      <div class="text"><?php echo $bl['Blog']['title']; ?></div>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
              </div>
        <?php } ?>
            </div>
</div>
<!-- /.row -->

</div>
