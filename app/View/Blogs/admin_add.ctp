<div class="categories form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('Blog',array('enctype'=>'multipart/form-data'));
?>
	<fieldset>
		<legend><?php echo __('Add Blog'); ?></legend>

            <?php
              echo $this->Form->input('title',array('required'=>'required', 'label'=>'Blog Title'));

        echo $this->Form->input('category_id',array('empty' => '(choose any category)', 'label' => 'Category','required'=>'required' ,'class'=>'selectbox2'));


              echo $this->Form->input('description',array('label'=>'Blog Description'));
		      echo $this->Form->input('short_description',array('label'=>'Blog Short Description'));
		      echo $this->Form->input('image',array('type'=>'file'));
              echo $this->Form->input('background_image',array('type'=>'file'));
		      echo $this->Form->input('status', array('type'=>'checkbox', 'label'=>'Status'));
                      echo $this->Form->input('is_feature', array('type'=>'checkbox', 'label'=>'Feature'));
                ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'BlogDescription',
        {
                width: "95%",
                filebrowserBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserImageBrowseUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserFlashBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserUploadUrl  :'<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',

                filebrowserImageUploadUrl : '<?php echo $this->webroot; ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',

                filebrowserFlashUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
        });

        CKEDITOR.replace( 'BlogShortDescription',
        {
                width: "95%",
                filebrowserBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserImageBrowseUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserFlashBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserUploadUrl  :'<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',

                filebrowserImageUploadUrl : '<?php echo $this->webroot; ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',

                filebrowserFlashUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
        });
    </script>
<?php //echo $this->element('admin_sidebar'); ?>
