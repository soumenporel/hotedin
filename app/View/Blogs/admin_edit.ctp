<div class="categories form">
<?php
//echo $this->Html->script('ckeditor/ckeditor');
echo $this->Form->create('Blog',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Blog'); ?></legend>

	    <?php
    		echo $this->Form->input('id');
    		echo $this->Form->input('title',array('required'=>'required', 'label'=>'Blog Title'));
            echo $this->Form->input('category_id',array('empty' => '(choose any category)', 'label' => 'Category','required'=>'required' ,'class'=>'selectbox2'));
            echo $this->Form->input('description',array('label'=>'Blog Description'));
            echo $this->Form->input('short_description',array('label'=>'Blog Short Description'));
            echo $this->Form->input('hide_backimg',array('type' => 'hidden','value'=>isset($this->request->data['Blog']['background_image'])?$this->request->data['Blog']['background_image']:''));
    		echo $this->Form->input('image',array('type'=>'file'));
            echo $this->Form->input('background_image',array('type'=>'file'));
    		echo $this->Form->input('status', array('type'=>'checkbox', 'label'=>'Active'));
    		echo $this->Form->input('hide_img',array('type' => 'hidden','value'=>isset($this->request->data['Blog']['image'])?$this->request->data['Blog']['image']:''));
            echo $this->Form->input('is_feature', array('type'=>'checkbox', 'label'=>'Feature'));
            ?>
            <div>
                <?php
                $uploadImgPath = WWW_ROOT.'blogs_image';
                if( $this->request->data['Blog']['image']!='' && file_exists($uploadImgPath . '/' .  $this->request->data['Blog']['image'])){
                ?>
                <p>Image :</p><img alt="" src="<?php echo $this->webroot;?>blogs_image/<?php echo $this->request->data['Blog']['image'];?>" style=" height:80px; width:80px;">
                <?php
                }
                else{
                ?>
                    <p>Image :</p><img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                <?php } ?>
            </div>
            <div class="">
                <?php
                $uploadImgPath = WWW_ROOT.'blogs_image';
                if( $this->request->data['Blog']['background_image']!='' && file_exists($uploadImgPath . '/' .  $this->request->data['Blog']['background_image'])){
                ?>
                <p>Background Image :</p><img alt="" src="<?php echo $this->webroot;?>blogs_image/<?php echo $this->request->data['Blog']['background_image'];?>" style=" height:80px; width:80px;">
                <?php
                }
                else{
                ?>
                    <p>Background Image :</p><img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                <?php } ?>
            </div>

	</fieldset>

<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'BlogDescription',
    {
            width: "95%",
            filebrowserBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

            filebrowserImageBrowseUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

            filebrowserFlashBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

            filebrowserUploadUrl  :'<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',

            filebrowserImageUploadUrl : '<?php echo $this->webroot; ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',

            filebrowserFlashUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });

     CKEDITOR.replace( 'BlogShortDescription',
        {
                width: "95%",
                filebrowserBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserImageBrowseUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserFlashBrowseUrl :'<?php echo $this->webroot; ?>/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/connector.php',

                filebrowserUploadUrl  :'<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=File',

                filebrowserImageUploadUrl : '<?php echo $this->webroot; ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',

                filebrowserFlashUploadUrl : '<?php echo $this->webroot; ?>/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
        });
</script>
<?php //echo $this->element('admin_sidebar'); ?>
