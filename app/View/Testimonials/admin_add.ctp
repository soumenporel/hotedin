<div class="categories form">
<?php echo $this->Form->create('Testimonial',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Testimonial'); ?></legend>
	<?php
       
        echo $this->Form->input('user_id', array('empty' => '(Choose any User)','required'=>'required', 'class'=>'selectbox2' ));
		echo $this->Form->input('post_id',array('empty' => '(Choose any Job)','label' =>'Courses','required'=>'required', 'class'=>'selectbox2'));
        echo $this->Form->input('description', array('label' => 'Your Testimonial', 'type' => 'textarea','id'=>'description'));
        echo $this->Form->input('status');
	?>
    </fieldset>


<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('description',
            {
                width: "95%"
            });
</script>    