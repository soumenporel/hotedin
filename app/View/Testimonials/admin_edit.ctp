<div class="categories form">
<?php echo $this->Form->create('Testimonial',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Edit Testimonial'); ?></legend>
	<?php
        echo $this->Form->input('id');
        echo $this->Form->input('user_id', array('empty' => '(Choose any User)'));
		echo $this->Form->input('post_id',array('empty' => '(Choose any Course)','label' =>'Courses'));
        echo $this->Form->input('description', array('label' => 'Your Testimonial', 'type' => 'textarea','id'=>'description'));
        echo $this->Form->input('date');
        echo $this->Form->input('status');
	?>
    </fieldset>


<?php echo $this->Form->end(__('Submit')); ?>
</div>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('description',
            {
                width: "95%"
            });
</script>    