<div class="testimonials view">
    <h2><?php echo __('Testimonial'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($testimonial['Testimonial']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('User Name'); ?></dt>
        <dd>
            <?php echo h($testimonial['User']['first_name'].' '.$testimonial['User']['last_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Course Name'); ?></dt>
        <dd>
            <?php echo h($testimonial['Post']['post_title']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Testimonial'); ?></dt>
        <dd>
            <?php echo h($testimonial['Testimonial']['description']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Date'); ?></dt>
        <dd>
            <?php echo h($testimonial['Testimonial']['date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
            <?php echo $testimonial['Testimonial']['status']=='1' ? 'Active' : 'Inactive'; ?>
            &nbsp;
        </dd>
    </dl>
</div>