<?php
//pr($userdetails);
//echo $userpopdetails['UserImage'][0]['originalpath'];
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
//$userid = $this->Session->read('user_id');
$offset = $this->Session->read('timezone');
$cakeDescription = __d('cake_dev', 'Studilmu');
$sitepagefull = str_replace('http://', '', Router::url($this->here, true));
//if(isset($this->Session->read('language')) && $this->Session->read('language')!='Spanish')
//{
//}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $MetaTagskeywords; ?></title>
        <meta charset="UTF-8">
        <meta name="description" content="<?php echo strip_tags($MetaTagsdescripton); ?>">
        <meta name="keywords" content="<?php echo $MetaTagskeywords; ?>">
        <link rel="shortcut icon" href="<?php echo $fav_icon; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap core JavaScript -->
        <script src="<?php echo $this->webroot; ?>js/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo $this->webroot; ?>js/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!--     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

        <script src="<?php echo $this->webroot; ?>js/new/owl.carousel.js"></script>
        <script src="<?php echo $this->webroot; ?>js/formValidation.js"></script>



        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>js/vendor/bootstrap/css/bootstrap.min.css">

       <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

        <link rel="stylesheet" href="/resources/demos/style.css">
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/ionicons.min.css">
        <link href="<?php echo $this->webroot; ?>css/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>css/owl.theme.default.min.css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>css/custom.css" rel="stylesheet">



		</head>
    <body>
        <header class="fx sticky">

      <ul class="top-social">
                <li><a href="http://www.facebook.com" target="_blank"><img src="<?php echo $this->webroot; ?>images/face-book.png"></a></li>
                <li><a href="http://www.twitter.com" target="_blank"><img src="<?php echo $this->webroot; ?>images/twi.png"></a></li>
                <li><a href="http://www.linkedin.com" target="_blank"><img src="<?php echo $this->webroot; ?>images/linkdin.png"></a></li>
                <li><a href="https://plus.google.com/discover" target="_blank"><img src="<?php echo $this->webroot; ?>images/gplus.png"></a></li>
                <li><a href="http://www.instagram.com" target="_blank"><img src="<?php echo $this->webroot; ?>images/ins.png"></a></li>
      </ul>

      <!-- Navigation -->
      <nav class="navbar  navbar-expand-lg navbar-dark ">
        <div class="container">

             <a class="navbar-brand" href="<?php echo $this->webroot; ?>"><img src="<?php echo $this->webroot; ?>images/new/logo.png" class="img-fluid" alt=""></a>




             <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $this->webroot; ?>">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $this->webroot; ?>cms_page/aboutus">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $this->webroot; ?>cms_page/employer">Employers</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Job Seeker</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Online Trainings</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $this->webroot; ?>blogs/index">Blog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo $this->webroot; ?>contact_news/contactus">Contact Us</a>
              </li>
              <li class="nav-item">
              <?php $userid = $this->Session->read('userid');
                if ($userid) { ?>
                    <a class="nav-link" href="<?php echo $this->webroot; ?>users/dashboard"><strong>Dashboard</strong></a>
                <?php }else { ?>
                <a class="nav-link" href="<?php echo $this->webroot; ?>users/login"><strong>Sign In/Sign Up</strong></a>
                <?php } ?>
              </li>
            </ul>
          </div>

        </div>
      </nav>
    </header>
    <!--end of header-->
        <div style="margin-top: 0px;" >
        <div id="ajaxFlashMessage" class="error" style="display: none;"></div>
        <?php
        //echo $this->Session->flash();
        ?>
        </div>
        <?php echo $this->fetch('content'); ?>
    <!--   footer   -->

    <footer>
       <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-3">
            <div class="footer-box-left">
              <img src="<?php echo $this->webroot; ?>images/footer-logo.png">
              <p>Tel: 01234 567890</p>
              <p>Email: support@hotedin.co.uk</p>
              <ul class="social-icon">
                <li><a href="#"><img src="<?php echo $this->webroot; ?>images/face-book.png"></a></li>
                <li><a href="#"><img src="<?php echo $this->webroot; ?>images/twi.png"></a></li>
                <li><a href="#"><img src="<?php echo $this->webroot; ?>images/linkdin.png"></a></li>
                <li><a href="#"><img src="<?php echo $this->webroot; ?>images/gplus.png"></a></li>
                <li><a href="#"><img src="<?php echo $this->webroot; ?>images/ins.png"></a></li>
              </ul>
              <ul class="footer-listing-bottom">
                <li><a href="#"><img class="img-fluid" src="<?php echo $this->webroot; ?>images/app-btn1.png"></a></li>
                <li><a href="#"><img class="img-fluid" src="<?php echo $this->webroot; ?>images/app-btn2.png"></a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-12 col-md-9">
            <div class="row">
            <div class="col-sm-4">
              <div class="footer-right">
                <h1>Job Seekers</h1>
                <ul>
                  <li><a href="<?php echo $this->webroot; ?>jobs/jobsearch">Search Job</a></li>
                  <li><a href="#">Jobseeker Login </a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Inside job blog</a></li>
                  <li><a href="#">Salary Checker</a></li>
                  <li><a href="#">Help</a></li>

                </ul>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="footer-right">
                <h1>Employers</h1>
                <ul>
                  <li><a href="#">Advertise a Job</a></li>
                  <li><a href="#">Existing Customer Login</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">FAQ</a></li>
                  <li><a href="#">Membership Plans</a></li>
                </ul>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="footer-right">
                <h1>Quick Links</h1>
                <ul>
                  <li><a href="#">Home </a></li>
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Employers</a></li>
                  <li><a href="#">Job Seekers</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Work For Us</a></li>
                </ul>
              </div>
            </div>
            </div>

          </div>
        </div>
        <div class="col-sm-12 text-center powerd-txt">Powerd by</div>

        <div class="powed-listing">
          <ul class="powed-listing">
            <li><a href="#"><img src="<?php echo $this->webroot; ?>images/powerd1.png"></a></li>
            <li><a href="#"><img src="<?php echo $this->webroot; ?>images/powerd2.png"></a></li>
            <li><a href="#"><img src="<?php echo $this->webroot; ?>images/powerd3.png"></a></li>
            <li><a href="#"><img src="<?php echo $this->webroot; ?>images/powerd4.png"></a></li>
            <li><a href="#"><img src="<?php echo $this->webroot; ?>images/powerd5.png"></a></li>
            <li><a href="#"><img src="<?php echo $this->webroot; ?>images/powerd6.png"></a></li>
          </ul>
        </div>


      </div>
      <hr>
      <div class="container bottom-part">
        <div class="row">
          <div class="col-sm-6">
            <p>© 2018 HotedIn. Privacy Policy  |  Terms & Conditions</p>
          </div>
          <div class="col-sm-6 text-right">
            <p><a href="#">Powerd By Natitsolved.</a></p>
          </div>
        </div>
      </div>
    </footer>

<!--       <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );

  $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
  </script> -->

    </body>
</html>
