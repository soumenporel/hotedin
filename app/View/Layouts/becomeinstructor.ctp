<?php
//pr($userdetails);
//echo $userpopdetails['UserImage'][0]['originalpath'];
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
//$userid = $this->Session->read('user_id');
$offset = $this->Session->read('timezone');
$cakeDescription = __d('cake_dev', 'Studilmu');
$sitepagefull = str_replace('http://', '', Router::url($this->here, true));
//if(isset($this->Session->read('language')) && $this->Session->read('language')!='Spanish')
//{
//}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $MetaTagskeywords; ?></title>
        <meta charset="UTF-8">
        <meta name="description" content="<?php echo strip_tags($MetaTagsdescripton); ?>">
        <meta name="keywords" content="<?php echo $MetaTagskeywords; ?>">
        <link rel="shortcut icon" href="<?php echo $fav_icon; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap CSS -->


        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap.min.css">

        <!--  icon  css   -->

        <link rel="stylesheet" href="https://use.fontawesome.com/b245db073c.css">

        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>

        <!--  custom  link   -->
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>

        <script src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $this->webroot; ?>js/formValidation.js"></script>

          <script type="text/javascript" src="<?php echo $this->webroot; ?>video/js/video.js"></script>

        <script type="text/javascript" src="//platform.linkedin.com/in.js">
            api_key: 81m3hhbaqx8odm
            authorize: true
            onLoad: onLinkedInLoad
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

        <style>
          header {
            z-index:10;
          }


          .fixed-header {
            position: fixed;
            top:0; left:0;
            width: 100%;
          }

          nav {
            width:100%;
            background: #292f36;
            postion:fixed;
            z-index:999;
          }

          .lnLogin span {
            margin-top: 14px;
          }

          .lnLogin span span span a button {
            width: 367px;
          }

            .ui-autocomplete {
                max-height:200px;
                overflow-y: auto;
                /* prevent horizontal scrollbar */
                overflow-x: hidden;
                /* add padding to account for vertical scrollbar */
            }


        </style>

      <?php
        if ( $this->params['controller'] != 'homepages' && $this->params['action'] != 'signup' && $this->params['action']!= 'signuplanding' && $this->params['action']!= 'course_filter' && $this->params['action']!= 'dashboard' && $this->params['action']!= 'course_goals' && $this->params['action']!= 'test_video' && $this->params['action']!= 'curriculum_test' && $this->params['action']!= 'course_landing_page' && $this->params['action']!= 'automatic_message' && $this->params['action']!= 'editprofile' && $this->params['action']!= 'signupstudent' && $this->params['action']!= 'business' && $this->params['action']!= 'government' && $this->params['action']!= 'university' && $this->params['controller']!= 'blogs' ) { ?>

          <!--<link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap3.css">-->
          <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap-theme3.css">

      <?php  }

      if($this->Session->read('ssname')!=null)
      {
          $ssname= $this->Session->read('ssname');
      }
 else {
          $ssname= '';
      }
      ?>


		</head>
    <body>


        <div style="margin-top: 70px;" >
          <nav style="border-top: 6px solid #f00; box-sizing: border-box; z-index: 999; " class="navbar navbar-toggleable-md navbar-light fixed-header navbar-area">
             <div class="container">
                <a class="navbar-brand w-150" href="<?php echo $this->webroot; ?>"><img src="<?php echo $site_logo; ?>" class="img-fluid" alt="logo"></a>
             </div>
          </nav>
        <div id="ajaxFlashMessage" class="error" style="display: none;"></div>
        <?php
        echo $this->Session->flash();
        echo $this->fetch('content');
        ?>
        </div>

        <script>
            $(document).ready(function () {
                //carousel options
                $('#quote-carousel').carousel({
                    pause: true, interval: 10000,
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#subscribe").on('click', function () {
                    if ($('#email').val() == '') {
                        $('.errormsg').show();
                        $('.successmsg').hide();
                        $('.errormsg').html('');
                        $('.errormsg').html('This field can not be blank');
                        $('#email').focus();
                        setTimeout(function () {
                            $('.errormsg').hide();
                            $('.errormsg').html('');
                        }, 3000);
                    } else if (!validateEmail($('#email').val())) {
                        $('.errormsg').show();
                        $('.successmsg').hide();
                        $('.errormsg').html('');
                        $('.errormsg').html('Invalid Email Address!');
                        $('#email').focus();
                        setTimeout(function () {
                            $('.errormsg').hide();
                            $('.errormsg').html('');
                        }, 3000);
                    } else {
                        $.post("<?php echo $this->webroot; ?>homepages/postdatanewsletter/",
                                {
                                    email: $('#email').val()
                                },
                        function (data) {
                            if (data == 1) {
                                $('.successmsg').show();
                                $('.errormsg').hide();
                                $('.successmsg').html('');
                                $('.successmsg').html('Email send successmsgfully!');
                                $('#email').val('');
                                setTimeout(function () {
                                    $('.successmsg').hide();
                                    $('.successmsg').html('');
                                }, 3000);
                            } else {
                                $('.successmsg').hide();
                                $('.errormsg').show();
                                $('.errormsg').html('');
                                $('.errormsg').html('Email already exist!');
                                $('#email').val('');
                                setTimeout(function () {
                                    $('.errormsg').hide();
                                    $('.errormsg').html('');
                                }, 3000);
                            }
                        });
                    }
                });
            });
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            $(document).ready(function () {
                setTimeout(function () {
                    $('.message, .success, .error').fadeOut('slow');
                }, 2000);


            });
        </script>


        <script type="text/javascript">

            function changelan(language) {
                var lang = language;
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>users/ajaxlang",
                    data: {
                        language: lang
                    },
                    success: function (html) {
                        location.reload();
                    }
                });
            }
        </script>
<script>

$(document).ready(function(){
    $("#signinForm").formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        excluded: [':disabled'],
        fields: {
            'email': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address'
                    }
                }

            },
            'password': {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    }
                }
            }
        }
    })
    .on('success.form.fv', function (e) {
        // Prevent form submission
        e.preventDefault();

        var $form = $(e.target),
        fv = $form.data('formValidation');
        //console.log($form.serialize());
        // Use Ajax to submit form data
        $.ajax({
            url: '<?php echo $this->webroot; ?>users/loginModal',
            type: 'POST',
            data: $form.serialize(),
            dataType: 'json',
            beforeSend: function () {
                $("#verifyAgainTab").css('display', 'none');
                $("#userIDVerify").val('');
            },
            success: function (data) {
                if (data.Ack == 1) {
                    $("#alertLogin").removeClass();
                    $("#alertLogin").addClass("alert alert-success");
                    $("#alertLoginText").html('<strong>' + data.res + '</strong>');
                    $("#alertLogin").show().delay(1500).fadeOut();
                    $("#signupdiv").hide();
                    $('#signinForm')[0].reset();
                    setTimeout(function () {
                        $('#loginModal').modal('hide');
                        ;
                    }, 1510);
                    $("#siteSignin,#siteSignup,#siteSignupAdd").remove();
                    $("#siteBlog").after('<li class="nav-item"><a class="nav-link" href="<?php echo $this->webroot?>users/editprofile"><span>'+data.userImage+'</span> ' + data.user + '</a></li><li class="nav-item"><a class="nav-link btn btn-danger" href="<?php echo $this->webroot; ?>users/logout" > Log Out</a></li>');
                } else {
                    $("#alertLogin").removeClass();
                    $("#alertLogin").addClass("alert alert-danger");
                    $("#alertLoginText").html('<strong>' + data.res + '</strong>');
                    $("#alertLogin").show().delay(3000).fadeOut();
                }

            }
        });
    });

    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
          FB.init({
              appId: '110713409638185',
              status: true,
              xfbml: true,
              version: 'v2.9'
          });

          $('#fbsignin').click(function (e) {
              e.preventDefault();
              FB.login(function (response) {
                  console.log(response);
                  if (!response || response.status !== 'connected') {
                      alert('Failed');
                  } else {

                      FB.api('/me', {fields: 'id,first_name,last_name,email'}, function (response) {
                          //console.log(response);
                          var fb_user_id = response.id;
                          var fb_first_name = response.first_name;
                          var fb_last_name = response.last_name;
                          var fb_email = response.email;
                          //console.log(JSON.stringify(response));

                          $.ajax({
                              url: '<?php echo $this->webroot; ?>users/sociallogin',
                              type: 'post',
                              dataType: 'json',
                              data: {
                                  fbId: fb_user_id,
                                  email: fb_email,
                                  fname: fb_first_name,
                                  lname: fb_last_name,
                                  signupby: 'facebook'
                              },
                              beforeSend: function () {
                                  $("#alertLogin").removeClass();
                                  $("#alertLogin").addClass("alert alert-info");
                                  $("#alertLoginText").html('<strong> Sending please wait... </strong>');
                                  $("#alertLogin").show();
                              },
                              success: function (data) {
                                  if (data.Ack == 1) {
                                      $("#alertLogin").removeClass();
                                      $("#alertLogin").addClass("alert alert-success");
                                      $("#alertLoginText").html('<strong>' + data.res + '</strong>');
                                      $("#alertLogin").show().delay(1500).fadeOut();
                                      $("#signupdiv").hide();
                                      $('#signinForm')[0].reset();
                                      setTimeout(function () {
                                          $('#loginModal').modal('hide');
                                          ;
                                      }, 1510);
                                      $("#siteSignin,#siteSignup,#siteSignupAdd").remove();
                                      $("#siteBlog").after('<li class="nav-item"><a class="nav-link" href="<?php echo $this->webroot?>users/editprofile"><span>'+data.userImage+'</span> ' + data.user + '</a></li><li class="nav-item"><a class="nav-link btn btn-danger" href="<?php echo $this->webroot; ?>users/logout" > Log Out</a></li>');
                                  } else {
                                      $("#alertLogin").removeClass();
                                      $("#alertLogin").addClass("alert alert-danger");
                                      $("#alertLoginText").html('<strong>' + data.res + '</strong>');
                                      $("#alertLogin").show().delay(3000).fadeOut();
                                  }
                              }
                          });
                      });
                  }
              }, {scope: 'public_profile,email'});
          });

      });

               $('.search_user').autocomplete({
                        minLength: 1,
                        source: function (request, response) {
                            var keyword = $('.search_user').val();
                            var url = '<?php echo $this->webroot . 'homepages/autosuggest/' ?>' + keyword;
                            $.getJSON(url, response);

                        },
                        select: function (event, ui) {
                            var label = ui.item.label;
                            var value = ui.item.link;
                            //store in session
                            //alert(label);
                            console.log(value);
                            if (value != '') {
                                //window.location.href = '<?php //echo $this->webroot . 'searches/index/' ?>' + value;
                                window.location.href = '<?php echo $this->webroot; ?>' + value;
                            }
                        }
                    }).focus(function () {
                        //reset result list's pageindex when focus on
                        window.pageIndex = 0;
                        $(this).autocomplete("search");
                    });

                    //for image show.....................................
                    $('.search_user').data("ui-autocomplete")._renderItem = function (ul, item) {

                        var $li = $('<li>'),
                                $img = $('<img>');


                        $img.attr({
                            src: item.icon,
                            alt: item.label
                        });

                        $li.addClass('suggestrow');
                        $li.attr('data-value', item.label);
                        $li.append('<a href="' + item.link + '">');
                        $li.find('a').append($img).append('<div class="suggestDiv"><span class="suggestName">' + item.label + '</span></div>');

                        return $li.appendTo(ul);
                    };

});


    (function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = 'https://apis.google.com/js/client:plusone.js?onload=googleonLoadCallback1';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();

    function googleonLoadCallback1(){
        gapi.client.setApiKey('AIzaSyDZEj6I1ktLM3iojSui8y35cEDkjri5DcY'); //set your API KEY
        gapi.client.load('plus', 'v1', function () {
        });//Load Google + API
    }

    function google_login() {
        var myParams = {
            'clientid': '781797887038-sk9f5h5rujn8r0on8v1den33ifkanc9n.apps.googleusercontent.com',
            //You need to set client id
            'cookiepolicy': 'single_host_origin',
            'callback': 'googleloginCallback', //callback function
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
        };
        gapi.auth.signIn(myParams);
    }

    function googleloginCallback(result) {
        if (result['status']['signed_in'])
        {
            //console.log(result);
            //alert("Login Success");
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });

            request.execute(function (resp) {
                var email = resp.emails[0].value;
                var gpId = resp.id;
                var fname = resp.name.familyName;
                var lname = resp.name.givenName;

                $.ajax({
                    url: '<?php echo $this->webroot . 'users/sociallogingplus'; ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        gpId: gpId,
                        email: email,
                        fname: fname,
                        lname: lname,
                        signupby:'googleplus'
                    },
                    beforeSend: function () {
                      $("#alertLogin").removeClass();
                      $("#alertLogin").addClass("alert alert-info");
                      $("#alertLoginText").html('<strong> Sending please wait... </strong>');
                      $("#alertLogin").show();
                    },
                    success: function (data) {
                        if (data.Ack == 1) {
                            $("#alertLogin").removeClass();
                            $("#alertLogin").addClass("alert alert-success");
                            $("#alertLoginText").html('<strong>' + data.res + '</strong>');
                            $("#alertLogin").show().delay(1500).fadeOut();
                            $("#signupdiv").hide();
                            $('#signinForm')[0].reset();
                            setTimeout(function () {
                                $('#loginModal').modal('hide');
                                ;
                            }, 1510);
                            $("#siteSignin,#siteSignup,#siteSignupAdd").remove();
                            $("#siteBlog").after('<li class="nav-item"><a class="nav-link" href="<?php echo $this->webroot?>users/editprofile"> Howdy! ' + data.user + '</a></li><li class="nav-item"><a class="nav-link btn btn-danger" href="<?php echo $this->webroot; ?>users/logout" > Log Out</a></li>');
                        } else {
                            $("#alertLogin").removeClass();
                            $("#alertLogin").addClass("alert alert-danger");
                            $("#alertLoginText").html('<strong>' + data.res + '</strong>');
                            $("#alertLogin").show().delay(3000).fadeOut();
                        }
                    }
                });
            });
        }

    }

    // $(window).scroll(function(){
    //     if ($(window).scrollTop() >= 0) {
    //        $('nav').addClass('fixed-header');
    //     }
    //     else {
    //        $('nav').removeClass('fixed-header');
    //     }
    // });

    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        $('a[id*=li_ui_li_gen_]').html('<button class="btn btn-info"><i class="fa fa-linkedin"></i></button>');
        IN.Event.on(IN, "auth", getProfileData);
    }
     // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
        var ln_id           = data.values[0].id;
        var ln_email        = data.values[0].emailAddress;
        var ln_firstName    = data.values[0].firstName
        var ln_lastName     = data.values[0].lastName;

        $('#firstname').val(ln_firstName);
        $('#lastname').val(ln_lastName);
        $('#email').val(ln_email);
        $('#lnid').val(ln_id);

        // $.ajax({
        //     url: '<?php echo $this->webroot . 'users/linkedinLoginRegister' ?>',
        //     type: 'post',
        //     dataType: 'json',
        //     data: {
        //         ln_id: ln_id,
        //         ln_email: ln_email,
        //         ln_firstName: ln_firstName,
        //         ln_lastName: ln_lastName
        //     },
        //     success: function(data) {
        //         if(data.ack == 1) {
        //             if(data.url != '') {
        //                 window.location.href = data.url;
        //             } else {
        //                  onLinkedInLoad();
        //             }
        //         } else {

        //             onLinkedInLoad();
        //         }
        //     }
        // });

    }

    // Handle an error response from the API call
    function onError(error) {
        //console.log(error);
        $('#errMsg').html('');
        $('#errMsg').html('<div class="col-md-12"><div class="alert alert-danger"><strong>Error!</strong>'+error+'</div></div>');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        //IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me").fields("id","first-name", "last-name", "email-address").result(onSuccess).error(onError);

    }



</script>

<script type="text/javascript">

    function changelan(language) {
        var lang = language;
        $.ajax({
            type: "POST",
            url: "<?php echo $this->webroot; ?>users/ajaxlang",
            data: {
                language: lang
            },
            success: function (html) {
                location.reload();
            }
        });
    }

    $(document).ready(function(){
        $(".langName").click(function(){
            $("#langHeader").text($(this).text());

            var lang = $(this).data('lang');

            $.ajax({
                type: "POST",
                url: "<?php echo $this->webroot; ?>users/ajaxlang",
                data: {
                    language: lang
                },
                success: function (html) {
                    // location.reload();
                    // console.log(html);
                }
            });
        })
    })
function search_submit()
{
 var search_val=$('#search_val').val();
 //alert(search_val);
 window.location.href='<?php echo $this->webroot; ?>searches/index/'+search_val;
}
</script>


    </body>
</html>
