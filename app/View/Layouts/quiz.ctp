<?php
//pr($userdetails);
//echo $userpopdetails['UserImage'][0]['originalpath'];
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
//$userid = $this->Session->read('user_id');
$offset = $this->Session->read('timezone');
$cakeDescription = __d('cake_dev', 'Learnify');
$sitepagefull = str_replace('http://', '', Router::url($this->here, true));
//if(isset($this->Session->read('language')) && $this->Session->read('language')!='Spanish')
//{
//}
?>

<style>
.menu{
    position: relative;
    top: 33px;
    display: none;
}

.hoverzone{
    position: relative;
}

.hoverzone:hover .menu{
    display: block;
}
</style>

<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $MetaTagskeywords; ?></title>
        <meta charset="UTF-8">
        <meta name="description" content="<?php echo strip_tags($MetaTagsdescripton); ?>">
        <meta name="keywords" content="<?php echo $MetaTagskeywords; ?>">
        <link rel="shortcut icon" href="<?php echo $fav_icon; ?>" />
        <link href="<?php echo $this->webroot; ?>css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>css/bootstrap-theme.css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/jcarousel.responsive.css"> 

        <script src="<?php echo $this->webroot.'video/';?>js/video.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.jcarousel.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.infinitescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jcarousel.responsive.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/formValidation.js"></script>

        <style>
            #loading {
                background-color: #000;
                display: none;
                height: 100%;
                left: 0;
                opacity: .7;
                position: fixed;
                text-align: center;
                top: 0;
                width: 100%;
                z-index: 9999
            }
            #loading>img {
                position:absolute;
                top:0;
                right:0;
                bottom:0;
                left:0;
                margin:auto
            }
            
            .dropdown-toggle p {
                background: #08d6e8 none repeat scroll 0 0;
                border-radius: 53%;
                color: #fff;
                font-size: 11px;
                height: 18px;
                line-height: 17px;
                overflow: hidden;
                position: absolute;
                right: -13px;
                text-align: center;
                top: -3px;
                width: 19px;
            }   
        </style>
    </head>
    <body>
        <div id="loading">
            <img src="<?php echo $this->webroot . 'images/ajax-loader.gif'; ?>" />
        </div>
        <section class="header <?php
        if ($sitepagefull != $SITE_URL . 'team4/learnfly/') {
            echo "inner_header";
        }
        ?>" id="header_section" >
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <img src="<?php echo $ladder_logo; ?>" class="" onclick="window.location.href = '<?php echo $this->webroot; ?>'" style="cursor:pointer;margin:0px important!;">
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <nav class="navbar navbar-area">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>                        
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                            
                             
      
                                <ul class="nav navbar-nav">
          <!--<div class="dropdown" style="cursor:pointer;"><li class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i> Browse all Course <i class="fa fa-chevron-down" aria-hidden="true"></i></li>
                                  <ul class="dropdown-menu">
                                    <?php
                                    if (!empty($catgfirstdetail)) {
                                        foreach ($catgfirstdetail as $val) {
                                            ?>
                                     <?php
                                        }
                                    }
                                    ?>
                                        
                                      </ul>
                     </div>-->
                                    <li class="dropdown hoverzone" data-toggle="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i> Browse all Course <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                        <ul class="dropdown-menu menu" role="menu">
                                            <?php
                                            if (!empty($catgfirstdetail)) {
                                                foreach ($catgfirstdetail as $val) {
                                                    $blogdattas = $this->requestAction('/homepages/subcategory/' . $val['Category']['id']);
                                                    //pr($blogdattas);
                                                    ?>

                                                    <?php
                                                    //pr($val['Children']);
                                                    if (!empty($val['Children'])) {
                                                        ?>
                                                        <li class="dropdown-submenu">
                                                            <a tabindex="-1" href="javascript:void(0);" onclick="window.location.href = '<?php echo $this->webroot; ?>posts/course_search/<?php echo $val['Category']['slug']; ?>'" ><?php echo $val['Category']['category_name']; ?><i class="fa fa-chevron-right"></i></a>
                                                            <ul class="dropdown-menu">
                                                                <?php
                                                                foreach ($val['Children'] as $subcat) {
                                                                    ?>
                                                                    <li onclick="window.location.href = '<?php echo $this->webroot; ?>posts/course_search/<?php echo $subcat['slug']; ?>'" ><a tabindex="-1" href="javascript:void(0)" ><?php echo $subcat['category_name']; ?></a></li>	
                                                                    <?php
                                                                }
                                                                ?>
                                                            </ul>
                                                        </li>			            
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <li><a href="javascript:void(0);"><?php echo $val['Category']['category_name']; ?></a></li>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                   <!-- <li class="dropdown" data-toggle="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            <i class="fa fa-file" aria-hidden="true"></i> Pages <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                            foreach ($headerMenus as $headerMenu) {
                                                echo '<li><a href="javascript:void(0)" onclick="window.location.href=\'' . $this->webroot . 'users/cms_page/' . $headerMenu['CmsPage']['slug'] . '\'">' . $headerMenu['CmsPage']['page_title'] . '</a></li>';
                                            }
                                            ?>
                                            
                                        </ul>
                                    </li>-->
                                   
                                    <?php
                                    if (isset($userid) && $userid != '') {
                                        ?>
                                    <li>
                                        <i class="fa fa-envelope"></i>&nbsp;<?php echo $site_email; ?>
                                    </li> 
                                                                                <!-- <li><button type="button" class="btn btn-primary " onclick="window.location.href = 'http://<?php echo $SITE_URL; ?>users/logout'">Logout</button></li>
                                                                                <li onclick="window.location.href = 'http://<?php echo $SITE_URL; ?>users/editprofile'"><button type="button" class="btn btn-primary">Dashboard</button></li> -->
                                        <?php
                                        if ($userdetails['User']['admin_type'] == 1) {
                                            ?>                
                                            <li class="dropdown dropdown_menu1" data-toggle="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Instructor <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                                <ul class="dropdown-menu" role="menu">                          
                                                    <li onclick="window.location.href = '<?php echo $this->webroot; ?>users/dashboard'"><a href="#">Instructor Dashboard</a></li>
                                                    <li class="dropdown-submenu"><a tabindex="-1" href="#">News And Resources</a></li>                      
                                                    <li class="create_course" onclick="window.location.href = '<?php echo $this->webroot; ?>posts/add_course'" ><a href="#">Create a Course</a></li>
                                                </ul>
                                            </li>

                                            <li class="dropdown dropdown_menu1" data-toggle="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> My Course <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                                <ul class="dropdown-menu dropdown_part" role="menu">                        
                                                    <div class="instructor_dashboard">
                                                        <header class="dropdownheader">Quick Start</header>
                                                        <?php
                                                        if ($rootUserCourses) {
                                                            foreach ($rootUserCourses as $rootUserCourse) {
                                                                //pr($rootUserCourse);
                                                                ?>
                                                                <a href="javascript: void(0);" onclick="window.location.href = '<?php echo $this->webroot . 'posts/course_details/' . $rootUserCourse['Post']['slug']; ?>'">
                                                                    <div class="my_course">
                                                                        <div class="mycourse_leftpart">
                                                                            <img class="img-responsive" src="<?php echo $this->webroot . 'img/post_img/' . $rootUserCourse['Post']['PostImage'][0]['originalpath']; ?>">
                                                                        </div>
                                                                        <div class="mycourse_rightpart">
                                                                            <h2><?php echo $rootUserCourse['Post']['post_title']; ?></h2>
                                                                            <div class="metadata_instructor">By <?php echo $rootUserCourse['Post']['User']['first_name'] . ' ' . $rootUserCourse['Post']['User']['last_name']; ?></div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </a>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <a href="#" onclick="window.location.href = 'http://<?php echo $SITE_URL; ?>user_courses'" class="see_all pull-right">See all <i class="fa fa-chevron-right"></i>
                                                        </a>
                                                    </div>
                                                </ul>
                                            </li>
                                            <?php
                                        }
                                        if ($userdetails['User']['admin_type'] == 2) {
                                            ?>
                                            <li onclick="window.location.href = 'http://<?php echo $SITE_URL; ?>posts/add_course'" class="dropdown dropdown_menu1" data-toggle="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Become An Instructor </a>
                                            </li>
                                        <?php } ?>
                                        <li class="dropdown dropdown_menu1" data-toggle="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                <i class="fa fa-shopping-cart"></i>
                                                <!-- <span class="fa-stack">
                                                  <i class="fa fa-circle fa-stack-2x"></i>
                                                  <span class="fa-stack-1x text-primary" id="cart-item-count" ><?php echo $cartItemCount; ?></span>
                                                </span> -->
                                                <p data-original-title="" title="" id="cart-item-count"><?php echo $cartItemCount; ?></p>
                                            </a>

                                            <div class="dropdown-menu dropdown_part cart_part" role="menu">                       
                                                <div class="instructor_dashboard">
                                                    <header class="dropdownheader">Shopping Cart</header>
                                                    <div class="shoppingtable">
                                                        <table class="table_part" style="width: 100%;" id="headerCart">
                                                            <?php
                                                            $rootCartPrice = 0;
                                                            $rootCartOldPrice = 0;
                                                            if ($rootCarts) {
                                                                if ($rootCartItems) {
                                                                    foreach ($rootCartItems as $rootCartItem) {
                                                                        $rootCartPrice += $rootCartItem['Post']['price'];
                                                                        $rootCartOldPrice += $rootCartItem['Post']['old_price'];
                                                                        ?>
                                                                        <tr>
                                                                            <td class="table_part_img">
                                                                                <img src="<?php echo $this->webroot . 'img/post_img/' . $rootCartItem['Post']['PostImage'][0]['originalpath']; ?>">
                                                                            </td>
                                                                            <td class="table_text">
                                                                                <a href="#"><?php echo $rootCartItem['Post']['post_title']; ?></a>
                                                                                <span><?php echo $rootCartItem['Post']['User']['first_name'] . ' ' . $rootCartItem['Post']['User']['last_name']; ?></span>
                                                                            </td>
                                                                            <td class="table_price">$<?php echo $rootCartItem['Post']['price']; ?> <span>$<?php echo $rootCartItem['Post']['old_price']; ?></span></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table_text">
                                                                            <span>Not found</span>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <tr>
                                                                    <td class="table_text">
                                                                        Not found
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>


                                                            <tr>
                                                                <td colspan="3" class="total">
                                                                    Your total: <span class="total_price">$<?php echo $rootCartPrice; ?></span>
                                                                    <span class="cutoff"> $<?php echo $rootCartOldPrice; ?></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="cart_button">
                                                            <button onclick="window.location.href = '<?php echo $this->webroot . 'carts'; ?>'" class="btn btn-primary btn-block">Go to Cart</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="dropdown dropdown_menu1" data-toggle="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-bell"></i>
                                                <!-- <span class="fa-stack">
                                                  <i class="fa fa-circle fa-stack-2x"></i>
                                                  <span class="fa-stack-1x text-primary">5</span>
                                                </span> -->
                                                <p id="cart_quantity" data-original-title="" title="" >5</p>
                                            </a>
                                            <div class="dropdown-menu dropdown_part cart_part" role="menu">
                                                <div class="instructor_dashboard">
                                                    <header class="dropdownheader">Notification</header>
                                                    <div class="notification_part">
                                                        <h3>Instructor</h3>
                                                        <p>Sed ut perspiciatis unde omnis iste .......</p>
                                                        <!--<a href="#">Read More</a>-->
                                                    </div>
                                                    <div class="notification_part">
                                                        <h3>Student</h3>
                                                        <p>Sed ut perspiciatis unde omnis iste .......</p>
                                                        <a href="#">Read More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="dropdown dropdown_menu1" data-toggle="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i> My Account <i class="fa fa-chevron-down" ></i></a>
                                            <ul class="dropdown-menu" role="menu">                        
                                                <li onclick="window.location.href = 'http://<?php echo $SITE_URL; ?>users/editprofile'"><a href="#">My Profile</a></li>
                                                <li><a href="" onclick="window.location.href = '<?php echo $this->webroot . 'carts' ?>'">My Wishlist</a></li>                        
                                                <li><a href="#">My Messages</a></li>
                                                <li class="help_div"><a href="#">Help</a></li>
                                                <li><a href="#">Account Settings</a></li>
                                                <li><a href="#">LearnFly Credits</a></li>
                                                <li onclick="window.location.href = '<?php echo $this->webroot; ?>users/logout'"><a href="#">Logout</a></li>
                                            </ul>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li onclick="window.location.href = 'http://<?php echo $SITE_URL; ?>users/signup'" style="cursor:pointer;"><i class="fa fa-user" aria-hidden="true"></i> Become an instructor</li>
                                        <li><button type="button" class="btn btn-primary " onclick="window.location.href = '<?php echo $this->webroot; ?>users/login'">Login</button></li>
                                        <li onclick="window.location.href = '<?php echo $this->webroot; ?>users/signup'"><button type="button" class="btn btn-primary">Register</button></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        
        <?php
        echo $this->Session->flash();
        echo $this->fetch('content');
        ?>
        
        <footer>
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <?php
                        if (!empty($footerMenus)) {
                            foreach ($footerMenus as $val) {
                                ?>
                                <div class="col-md-3">
                                    <div class="coursesP">
                                        <h2><?php echo $val['CmsPage']['page_title']; ?></h2>
                                        <p><?php echo substr($val['CmsPage']['page_description'], 0, 150); ?>
                                        </p>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                                <div class="col-md-3">
                                    <div class="coursesP">
                                        <h2><a href="<?php echo $this->webroot.'faqs/faq_page';?>">FAQs</a></h2>
                                        <!-- <p><?php echo substr($val['CmsPage']['page_description'], 0, 150); ?>
                                        </p> -->
                                    </div>
                                </div>
                        <div class="col-md-3">
                            <div class="coursesP">
                                <h2>Subscribe</h2>
                                <p class="signup">Sign up for our newsletter and stay informed of our news 
                                </p>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" required/>
                                </div>
                                <button type="button" class="btn btn-default" id="subscribe">Subscribe</button>
                                <div class="errormsg" style="display:none;color:red;"></div>
                                <div class="successmsg" style="display:none;color:green;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer_bottom" style="position:relative;">
                <div style="position:absolute; top:-100; left:0;" >
                    <ul style="list-style: outside none none;" >    
                        <li>
                            <a href="javascript:void(0)" onclick="changelan('en')" class="language" style="padding: 6px 10px;">
                                <img src="<?php echo $this->webroot; ?>images/english.png" style="width:24px" />
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" onclick="changelan('sp')" class="language" style="display:block; padding: 6px 10px;">
                                <img src="<?php echo $this->webroot; ?>images/spanish.png" style="width:24px" />
                            </a>
                        </li>
                    </ul>    
                </div>    
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pull-left copyright">© <?php echo date('Y'); ?> <?php echo strip_tags($abouts['CmsPage']['page_description']); ?></div>
                            <div class="pull-left copyright">Designed & Developed by: <a href="http://www.natitsolved.com/" target="_blank" style="color:#ffffff;text-decoration:none;">NAT IT SOLVED PVT. LTD</a></div>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right social-list">
                                <ul>
                                    <li class="twitter"><a href="<?php echo $twitter['SocialMedia']['url']; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li class="facebook"><a href="<?php echo $facebook['SocialMedia']['url']; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li class="googleplus"><a href="<?php echo $googleplus['SocialMedia']['url']; ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="linkedin"><a href="<?php echo $linkln['SocialMedia']['url']; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="vimeo"><a href="<?php echo $vimeo['SocialMedia']['url']; ?>" target="_blank"><i class="fa fa-vimeo"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script>
            jQuery(document).ready(function () {

                var t = $(window).width();
                /* alert(t); // to see the actial width in pixel for debugging and fine-tuning purposes */

                if (t < 1280)
                    jQuery('#mycarousel').jcarousel({
                        visible: 2
                    });
                else
                    jQuery('#mycarousel').jcarousel({
                        visible: 4
                    });
            });
        </script>
        <script>
            $(document).ready(function () {
                //carousel options
                $('#quote-carousel').carousel({
                    pause: true, interval: 10000,
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#subscribe").on('click', function () {
                    if ($('#email').val() == '') {
                        $('.errormsg').show();
                        $('.successmsg').hide();
                        $('.errormsg').html('');
                        $('.errormsg').html('This field can not be blank');
                        $('#email').focus();
                        setTimeout(function () {
                            $('.errormsg').hide();
                            $('.errormsg').html('');
                        }, 3000);
                    } else if (!validateEmail($('#email').val())) {
                        $('.errormsg').show();
                        $('.successmsg').hide();
                        $('.errormsg').html('');
                        $('.errormsg').html('Invalid Email Address!');
                        $('#email').focus();
                        setTimeout(function () {
                            $('.errormsg').hide();
                            $('.errormsg').html('');
                        }, 3000);
                    } else {
                        $.post("<?php echo $this->webroot; ?>homepages/postdatanewsletter/",
                                {
                                    email: $('#email').val()
                                },
                        function (data) {
                            if (data == 1) {
                                $('.successmsg').show();
                                $('.errormsg').hide();
                                $('.successmsg').html('');
                                $('.successmsg').html('Email send successmsgfully!');
                                $('#email').val('');
                                setTimeout(function () {
                                    $('.successmsg').hide();
                                    $('.successmsg').html('');
                                }, 3000);
                            } else {
                                $('.successmsg').hide();
                                $('.errormsg').show();
                                $('.errormsg').html('');
                                $('.errormsg').html('Email already exist!');
                                $('#email').val('');
                                setTimeout(function () {
                                    $('.errormsg').hide();
                                    $('.errormsg').html('');
                                }, 3000);
                            }
                        });
                    }
                });
            });
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            $(document).ready(function () {
                $('.jcarousel').jcarousel({
                    autoPlay: 5000,
                    stopOnHover: false
                });
                setTimeout(function () {
                    $('.message, .success, .error').fadeOut('slow');
                }, 2000);
            });
        </script>
        <script>
            $(document).ready(function () {

                // For the Second level Dropdown menu, highlight the parent	
//                $(".dropdown-menu")
//                        .mouseenter(function () {
//                            $(this).parent('li').addClass('active');
//                        })
//                        .mouseleave(function () {
//                            $(this).parent('li').removeClass('active');
//                        });

            });
        </script>

        <script type="text/javascript">

            function changelan(language) {
                var lang = language;
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->webroot; ?>users/ajaxlang",
                    data: {
                        language: lang
                    },
                    success: function (html) {
                        location.reload();
                    }
                });
            }
        </script>
        
        
     
    </body>
</html>