<style>

.inner_header{background:#0C2440; position:relative;}
.text{
    min-height: 146px;
}
.rating ul{ padding-left:0px;}
.rating ul li{display:inline-block; list-style:none;}

</style>

<!-- <section class="inner_content" style="padding-top:40px; padding-bottom:20px;" >
    <div class="container">
        <div id="products" class="row list-group">
            <?php
            foreach ($user_courses as $course) {
                //print_r($course); exit();

            ?>
            <div class="home-cate" >
                <div class="coursename thumbnail" style="cursor:pointer;">
                    <div class="courseimage" onclick="javascript:window.location.href = '<?php echo $this->webroot . 'learns/course_content/'.$course['Post']['slug']; ?>'">
                        <img style="height:140px; width:140px;" class="img-responsive" src="<?php echo $this->webroot; ?>img/post_img/<?php echo $course['Post']['PostImage'][0]['originalpath']; ?>">
                    </div>
                    <div class="course_bottom_part">
                        <div class="name-p" onclick="javascript:window.location.href = '<?php echo $this->webroot . 'learns/course_content/'.$course['Post']['slug']; ?>'"><?php
                                if(strlen($course['Post']['post_title'])<=30)
                                {
                                  echo $course['Post']['post_title'];
                                }
                                else
                                {
                                  $y=substr($course['Post']['post_title'],0,30) . '...';
                                  echo $y;
                                }
                        // echo $course['Post']['post_title'];
                        ?></div>
                        <div class="writer_name">By <?php echo $course['Post']['User']['first_name'] . ' ' . $course['Post']['User']['last_name']; ?></div>
                        <div class="rating"> <span class="rateStarFirst starimg" style="color:gold" data-score="3.5" readonly="true"></span>


                        </div>
                        <div class="course-bottom-list ">
                        <ul class="float-left">

                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                        <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                          Free                                        </a></li>
                        </ul>

                        <div class="clearfix"></div>
                        </div>
                        <div class="price">
                            <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                            <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>

            <span class="next" style="display: none;">Next &gt;&gt;</span>
        </div>
    </div>
</section> -->

<section class="bootstrapTabberPart inner_content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="tabbable-panel">
				<div class="tabbable-line">
					<h2>My Courses</h2>
                                        <?php //print_r($wishlistPosts); ?>
					<ul class="nav nav-tabs "  role="tablist">
						<li class="nav-item">
							<a href="#tab_default_1" data-toggle="tab" class="nav-link active"  data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">
							All Courses </a>
						</li>
						<!-- <li>
							<a href="#tab_default_2" data-toggle="tab">
							Collections </a>
						</li> -->
						<li class="nav-item">
							<a href="#tab_default_3" data-toggle="tab" class="nav-link"  data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">
							Wishlist </a>
						</li>
						<li class="nav-item">
							<a href="#tab_default_4" data-toggle="tab" class="nav-link"  data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">
							Archived </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default_1">
							<!-- <div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3">
									<label for="sel1">Sort by:</label>
									  <select class="form-control selectPart1" id="sel1">
									    <option>1</option>
									    <option>2</option>
									    <option>3</option>
									    <option>4</option>
									  </select>

								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<label for="sel1">Filter by:</label>
									<div class="row">
										<div class="col-md-4 col-lg-4 col-sm-4">
											<select class="form-control selectPart1" id="sel1">
											    <option>1</option>
											    <option>2</option>
											    <option>3</option>
											    <option>4</option>
									  		</select>
										</div>
										<div class="col-md-4 col-lg-4 col-sm-4">
											<select class="form-control selectPart1" id="sel1">
											    <option>1</option>
											    <option>2</option>
											    <option>3</option>
											    <option>4</option>
									  		</select>
										</div>
										<div class="col-md-4 col-lg-4 col-sm-4">
											<select class="form-control selectPart1" id="sel1">
											    <option>1</option>
											    <option>2</option>
											    <option>3</option>
											    <option>4</option>
									  		</select>
										</div>
									</div>

								</div>
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="form-group formPart1">
									  <label for="usr">&nbsp;</label>
									  <input type="text" class="form-control textField1" id="usr" palceholder
									="Search my courses">
										<span class="searchbar"><a href="#"><i class="fa fa-search"></i></a></span>
									</div>
								</div>
							</div> -->
							 <div id="products" class="row">
            <?php
            foreach ($userPosts as $course) {
                //pr( $course['PostImage'] ); exit;
                ?>

                <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3" >
                  <a href="<?php echo $this->webroot . 'learns/course_content/'.$course['Post']['slug']; ?>" style="text-decoration: none;color: #2e2e2e;">
                    <figure class="course-box bg-white newCourse-box">
                      <div class="course-video p-relative">
                          <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $course['PostImage'][0]['originalpath']; ?>" style=" width: 100%; height:auto; " >
                          <div class="previewCourse">
                              <div class="text-center">
                                 <span class="font-30"><i class="fa fa-play" aria-hidden="true"></i></span>
                                 <div class="font-weight-bold font-12">Preview this course</div>
                              </div>
                           </div>
                      </div>
                      <figcaption class="bg-white p-3">
                          <h5><?php
                                  if(strlen($course['Post']['post_title'])<=30)
                                  {
                                    echo $course['Post']['post_title'];
                                  }
                                  else
                                  {
                                    $y=substr($course['Post']['post_title'],0,30) . '...';
                                    echo $y;
                                  }
                          // echo $course['Post']['post_title'];
                          ?></h5>
                          <h6 class="font-weight-light font-12">By <?php echo $course['User']['first_name'] . ' ' . $course['User']['last_name']; ?></h6>
                          <div class="rating font-12">
                              <ul>
                                  <?php
                                          if(isset($course['Rating']) && $course['Rating']!=''){
                                              $b = count($course['Rating']);

                                              $a=0;
                                              foreach ($course['Rating'] as $value) {
                                                  $a=$a + $value['ratting'];
                                              }
                                          }
                                          $finalrating='';
                                          if($b!=0){
                                              $finalrating = ($a / $b);
                                          }

                                           if(isset($finalrating) && $finalrating!='') {
                                              for($x=1;$x<=$finalrating;$x++) { ?>
                                                  <li><i class="fa fa-star" style="color: gold;" aria-hidden="true"></i></li>
                                              <?php }
                                              if (strpos($finalrating,'.')) {  ?>
                                                  <li><i class="fa fa-star-half-o" style="color: gold;" aria-hidden="true"></i></li>
                                              <?php  $x++;
                                              }
                                              while ($x<=5) { ?>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <?php $x++;
                                              }
                                            }else { ?>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <?php } ?>
                              </ul>
                          </div>

<!--                          <div class="course-bottom-list ">
                          <ul class="float-left mt-3">

                                          <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $course['User']['address']; ?></li>
                                          <li>
                                            <?php
                                        if($course['Post']['price']==0){
                                          echo 'Free';
                                        }else{
                                          $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($course['Post']['currency_type'],$course['Post']['price'])));
                                          echo $price['symbol'].round($price['price']);
                                          //echo '$'.round($course['Post']['price']);
                                        }
                                         ?>                                        </li>
                          </ul>

                          <div class="clearfix"></div>
                          </div>-->
<!--                          <div class="price">
                              <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                              <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                              <div class="clearfix"></div>
                          </div>-->
                      </figcaption>
                    </figure>
                    </a>
                </div>
            <?php } ?>

            <span class="next" style="display: none;">Next &gt;&gt;</span>
        </div>
	</div>


    <div class="tab-pane secndTab" id="tab_default_3">
            <?php if(!empty($wishlistPosts)){ //echo 1;?>
             
            <div id="products" class="row">
            <?php
            foreach ($wishlistPosts as $wishlist) { //print_r($wishlist); ?>
                
                 <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3" >
                  <a href="<?php echo $this->webroot.'learns/course_content/'.$wishlist['Post']['slug']; ?>" style="text-decoration: none;color: #2e2e2e;">
                    <figure class="course-box bg-white newCourse-box">
                      <div class="course-video p-relative">
                          <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $wishlist['PostImage'][0]['originalpath']; ?>" style=" width: 100%; height:auto; " >
                          <div class="previewCourse">
                              <div class="text-center">
                                 <span class="font-30"><i class="fa fa-play" aria-hidden="true"></i></span>
                                 <div class="font-weight-bold font-12">Preview this course</div>
                              </div>
                           </div>
                      </div>
                      <figcaption class="bg-white p-3">
                          <h5><?php
                                  if(strlen($wishlist['Post']['post_title'])<=30)
                                  {
                                    echo $wishlist['Post']['post_title'];
                                  }
                                  else
                                  {
                                    $y=substr($wishlist['Post']['post_title'],0,30) . '...';
                                    echo $y;
                                  }
                          // echo $course['Post']['post_title'];
                          ?></h5>
                          <h6 class="font-weight-light font-12">By <?php echo $wishlist['User']['first_name'] . ' ' . $wishlist['User']['last_name']; ?></h6>
                          <div class="rating font-12">
                              <ul>
                                  <?php
                                          if(isset($wishlist['Rating']) && $wishlist['Rating']!=''){
                                              $b = count($wishlist['Rating']);

                                              $a=0;
                                              foreach ($wishlist['Rating'] as $value) {
                                                  $a=$a + $value['ratting'];
                                              }
                                          }
                                          $finalrating='';
                                          if($b!=0){
                                              $finalrating = ($a / $b);
                                          }

                                           if(isset($finalrating) && $finalrating!='') {
                                              for($x=1;$x<=$finalrating;$x++) { ?>
                                                  <li><i class="fa fa-star" style="color: gold;" aria-hidden="true"></i></li>
                                              <?php }
                                              if (strpos($finalrating,'.')) {  ?>
                                                  <li><i class="fa fa-star-half-o" style="color: gold;" aria-hidden="true"></i></li>
                                              <?php  $x++;
                                              }
                                              while ($x<=5) { ?>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                              <?php $x++;
                                              }
                                            }else { ?>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                            <?php } ?>
                              </ul>
                          </div>

                          <div class="course-bottom-list ">
                          <ul class="float-left mt-3">

                                          <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $wishlist['User']['address']; ?></li>
<!--                                          <li>
                                            <?php
                                        if($wishlist['Post']['price']==0){
                                          echo 'Free';
                                        }else{
                                          $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($wishlist['Post']['currency_type'],$wishlist['Post']['price'])));
                                          echo $price['symbol'].round($price['price']);
                                          //echo '$'.round($course['Post']['price']);
                                        }
                                         ?>                                        </li>-->
                          </ul>

                          <div class="clearfix"></div>
                          </div>
<!--                          <div class="price">
                              <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                              <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                              <div class="clearfix"></div>
                          </div>-->
                      </figcaption>
                    </figure>
                    </a>
                </div>
                
<!--                <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3" >
                    <a href="<?php echo $this->webroot.$wishlist['Post']['slug']; ?>'">
                      <figure class="course-box bg-white newCourse-box">
                        <div class="course-video p-relative">
                            <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $wishlist['PostImage'][0]['originalpath']; ?>" style=" width: 100%; height:auto; ">
                            <div class="previewCourse">
                                <div class="text-center">
                                  <span class="font-30"><i aria-hidden="true" class="fa fa-play"></i></span>
                                  <div class="font-weight-bold font-12">Preview this course</div>
                                </div>
                            </div>
                        </div>
                        <figcaption class="bg-white p-3">
                            <h5><?php
                                    if(strlen($wishlist['Post']['post_title'])<=30)
                                    {
                                      echo $wishlist['Post']['post_title'];
                                    }
                                    else
                                    {
                                      $y=substr($wishlist['Post']['post_title'],0,30) . '...';
                                      echo $y;
                                    }
                            // echo $course['Post']['post_title'];
                            ?></h5>
                            <h6 class="font-weight-light font-12">By <?php echo $wishlist['User']['first_name'] . ' ' . $wishlist['User']['last_name']; ?></h6>

                            <div class="rating font-12">
                                <span class="rateStarFirst starimg" style="color:gold" data-score="3.5" readonly="true"></span>
                            </div>

                            <div class="course-bottom-list ">
                            <ul class="float-left mt-3">
                                            <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>
                                            <li><i class="fa fa-map-marker" aria-hidden="true"></i>  United States</li>
                                            <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                              <?php
                                          if($wishlist['Post']['price']==0){
                                            echo 'Free';
                                          }else{
                                            $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($wishlist['Post']['currency_type'],$wishlist['Post']['price'])));
                                            echo $price['symbol'].round($price['price']);
                                            //echo '$'.round($course['Post']['price']);
                                          }
                                           ?>                                         </a></li>
                            </ul>

                            <div class="clearfix"></div>
                            </div>
                            <div class="price">
                                <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                <div class="clearfix"></div>
                            </div>
                        </figcaption>
                      </figure>
                    </a>
                </div>-->
            <?php } ?>

            <span class="next" style="display: none;">Next &gt;&gt;</span>
        </div>
               
               <?php } ?>
	</div>
			<div class="tab-pane" id="tab_default_4">

                <?php if(empty($archivePosts)){ ?>
                <!-- <h2>
                    Focus on only the courses that matter to you.
                </h2> -->
                <?php } ?>

				<div id="products" class="row">
                <?php
                foreach ($archivePosts as $archive) {
                    //print_r($course); exit();
                ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 mt-3" >
                    <a style="text-decoration: none;color: #2e2e2e;" href="<?php echo $this->webroot . 'learns/course_content/'.$archive['Post']['slug']; ?>">
                        <figure class="course-box bg-white newCourse-box">
                          <div class="course-video p-relative">
                              <img src="<?php echo $this->webroot; ?>img/post_img/<?php echo $archive['PostImage'][0]['originalpath']; ?>" style=" width:100%; height: auto; ">
                              <div class="previewCourse">
                                  <div class="text-center">
                                      <span class="font-30"><i aria-hidden="true" class="fa fa-play"></i></span>
                                      <div class="font-weight-bold font-12">Preview this course</div>
                                  </div>
                              </div>
                          </div>
                          <figcaption  class="bg-white p-3" >
                              <h5 ><?php
                                      if(strlen($archive['Post']['post_title'])<=30)
                                      {
                                        echo $archive['Post']['post_title'];
                                      }
                                      else
                                      {
                                        $y=substr($archive['Post']['post_title'],0,30) . '...';
                                        echo $y;
                                      }
                              // echo $course['Post']['post_title'];
                              ?></h5>
                              <h6 class="font-weight-light font-12">By <?php echo $archive['User']['first_name'] . ' ' . $archive['User']['last_name']; ?></h6>
                              <div class="rating font-12">
                                  <ul>
                                      <?php
                                              if(isset($archive['Rating']) && $archive['Rating']!=''){
                                                  $b = count($archive['Rating']);

                                                  $a=0;
                                                  foreach ($archive['Rating'] as $value) {
                                                      $a=$a + $value['ratting'];
                                                  }
                                              }
                                              $finalrating='';
                                              if($b!=0){
                                                  $finalrating = ($a / $b);
                                              }

                                               if(isset($finalrating) && $finalrating!='') {
                                                  for($x=1;$x<=$finalrating;$x++) { ?>
                                                      <li><i class="fa fa-star" style="color: gold;" aria-hidden="true"></i></li>
                                                  <?php }
                                                  if (strpos($finalrating,'.')) {  ?>
                                                      <li><i class="fa fa-star-half-o" style="color: gold;" aria-hidden="true"></i></li>
                                                  <?php  $x++;
                                                  }
                                                  while ($x<=5) { ?>
                                                      <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                  <?php $x++;
                                                  }
                                                }else { ?>
                                                      <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                      <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                      <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                      <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                      <li><i class="fa fa-star-o" aria-hidden="true" style="color: gold;" ></i></li>
                                                <?php } ?>
                                     <!--  <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                      <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                      <li><i class="fa fa-star-o" aria-hidden="true"></i></li> -->
                                  </ul>
                              </div>
                              <div class="course-bottom-list ">
                              <ul class="float-left">
                                             <!-- <li><i class="fa fa-user" aria-hidden="true"></i> Tutor</li>-->
                                              <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php echo $archive['User']['address']; ?></li>
                                              <li><a href="javascript:void(0)" class="tran3s p-color-bg themehover">
                                                <?php
                                            if($archive['Post']['price']==0){
                                              echo 'Free';
                                            }else{
                                              $price = $this->requestAction(array('controller' => 'exchange_rates', 'action' => 'currencySet'),array('pass' => array($archive['Post']['currency_type'],$archive['Post']['price'])));
                                              echo $price['symbol'].round($price['price']);
                                              //echo '$'.round($archive['Post']['price']);
                                            }
                                             ?>                                        </a></li>
                              </ul>

                              <div class="clearfix"></div>
                              </div>
                              <div class="price">
                                  <div class="pull-left"><span class="pricenin">$25</span> <span class="doller25">$30</span></div>
                                  <div class="pull-right"><span class="enrolled"><img src="/team4/learnfly/img/groupusers.png">57.2k Enrolled</span></div>
                                  <div class="clearfix"></div>
                              </div>
                          </figcaption>
                        </figure>
                    </a>
                </div>
                <?php
                }
                ?>
                <span class="next" style="display: none;">Next &gt;&gt;</span>
                </div>
			</div>

                    </div>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>

<script src="<?php echo $this->webroot;?>js/jquery.raty-fa.1.js"></script>
<script>
    $(document).ready(function(){

        $(".rateStarFirst").raty({
            score: function() {
                return $(this).attr('data-score');
            },
            half: true,
            click: function(score, evt) {
               console.log(score);
            }
        });

    });
</script>
