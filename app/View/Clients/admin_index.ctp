<?php?>
<div class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <!--statistics start-->
            <div class="row state-overview">
                <div class="col-md-1 col-xs-6 col-sm-1">&nbsp;</div>
                
                
            </div>
            <!--statistics end-->
        </div>
    </div>
</div>
<div class="users index">
	<h2 style="width:400px;float:left;"><?php echo __('Clients'); ?></h2>
<!--	<a href="<?php echo $this->webroot.'admin/users/export'; ?>" style="float:right">Export Clients</a>-->
        <div>
       <?php //echo $this->Form->create("Client");?>
          
        <?php //echo $this->Form->end();?>
        </div>
        
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th>SN<?php //echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th>Image</th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
        $ClientCnt=0;
        
        foreach ($clients as $client): 
            $ClientCnt++;?>
	<tr>
		<td><?php echo $ClientCnt;//echo h($clients['Client']['id']); ?>&nbsp;</td>
		<td><?php echo h($client['Client']['name']); ?>&nbsp;</td>
                 <td width="11%">
                <?php
                    $uploadFolder = "client/";
                    $uploadPath = $this->webroot . $uploadFolder;
                    ?>
                    <img src="<?php echo ($client['Client']['image'] != '') ? $uploadPath . $client['Client']['image'] : ''; ?>" width="100" />
                 </td>
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $client['Client']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $client['Client']['id'])); ?>
			
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $client['Client']['id']), null, __('Are you sure you want to delete %s?', $client['Client']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<style>
.title a{
    color: #fff !important;
}
</style> 