<div class="users form">
<?php echo $this->Form->create('Client',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Client'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('feature', array('type'=>'checkbox', 'label'=>'Feature'));
	?>
         <?php
        echo $this->Form->input('image', array('type' => 'file', 'id'=>'BannerImageNew',array('required'=>'required')));?>
        <div class="input file" >
            <span style="color:red;font-size:12px;">* Image Type Should be .JPG,.JPEG,.PNG,.GIF.</span><br>
<!--            <span style="color:red;font-size:12px;">**The Image Resolution Should be 1920x750.</span>-->
        </div>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<div class="modal fade" id="alertModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header error-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Image Verification Error</h4>
        </div>
        <div class="modal-body">
          <p id="alertMessage"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">

var _URL = window.URL || window.webkitURL;

function isSupportedBrowser() {
    return window.File && window.FileReader && window.FileList && window.Image;
}

function getSelectedFile() {
    var fileInput = document.getElementById("BannerImageNew");
    var fileIsSelected = fileInput && fileInput.files && fileInput.files[0];
    if (fileIsSelected)
        return fileInput.files[0];
    else
        return false;
}


$("#BannerImageNew").change(function(event) {
    var form = this;

    if (isSupportedBrowser()) {
        event.preventDefault(); //Stop the submit for now

        var file = getSelectedFile();
        if (!file) {
            alert("Please select an image file.");
            return;
        }


    }
});

function validateImageBeforeUpload(el) {
    var types = ['image/jpeg', 'image/gif', 'image/png']
    var imageFile = el;

    if(types.indexOf(imageFile.files[0].type) != -1) {
        var fileSize = imageFile.files[0].size;
        fileSize = fileSize / (1024*1024);
        if (fileSize > 2) {
            imageFile.value = '';
            alert('Maximum filesize is 2MB');
        }
    } else {
        imageFile.value = '';
        alert('Invalid image type');
    }
}

</script>
