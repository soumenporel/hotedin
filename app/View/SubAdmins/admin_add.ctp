<div class="users form">
<?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Sub Admin'); ?></legend>
	<?php
        
        echo $this->Form->input('first_name',array('required'=>'required'));
        echo $this->Form->input('last_name',array('required'=>'required'));
        echo $this->Form->input('email_address', array('type' => 'email'));
        echo $this->Form->input('user_pass', array('type' => 'password'));
        
        echo $this->Form->input('country',array('required'=>'required','class'=>'selectbox2'));
        //echo $this->Form->input('role');
        echo $this->Form->input('state',array('required'=>'required'));
        echo $this->Form->input('city',array('required'=>'required'));
        echo $this->Form->input('zip',array('required'=>'required'));
        echo $this->Form->input('address',array('required'=>'required','label'=>'Street Address','id'=>'address'));
        
        echo $this->Form->input('Phone_number');
        //echo $this->Form->input('has_marketplace');
        echo $this->Form->input('status');
        echo $this->Form->input('image',array('type'=>'file'));
        echo $this->Form->input('role', array('type' => 'hidden', 'value' => 4));
	?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script>
    var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.address_components);
    });
</script>