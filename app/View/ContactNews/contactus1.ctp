<section class="contact-page-section py-5 mb-5">
	  	<div class="container">
	  	</div>
	  </section>

	  	<div class="container">
	  		<div class="row">
		  		<h1  class="dashboard-heading candidate-heading-color text-center w-100">Contact Us</h1>
		  		<p class="candidate-paragraph text-center py-3  w-100">If you have any questions regarding HotedIn, please do not 
		  		hesitate to ask:</p>
		  		<div class="contact-page-left-box mx-auto mb-5">
		  			<div class="row">
			  			<div class="col-sm-12 col-md-8 contact-page-shadow">
			  				<form action="contactus" method="post" id="UserSignupForm">
				  				<label>Name*</label>
				  				<div style="display:none;">hi</div>
				  				<input type="text"  name="data[ContactNew][name]" id="name" class="form-control" required>
				  				<label>Email Id*</label>
				  				<input type="email"  name="data[ContactNew][email_address]" id="email_address" class="form-control" required>
				  				<label>Message*</label>
				  				<textarea class="contact-page-textarea" name="data[ContactNew][message]" id="message" class="form-control" required> </textarea>	
				  				<input type="submit" class="candidate-register mb-4 float-left" placeholder="Register" name="">		  					
			  				</form>
			  			</div>

			  			<div class="col-sm-12 col-md-4 contact-rgt-bg">
			  				<i class="icon ion-ios-location d-block text-center pt-5"></i>
			  				<h1 class="d-block text-center pb-2">Address</h1>
					 		<p class="text-center d-block">London Kensington Olympia,
							Crown House, 72 Hammersmith
							Rd, Hammersmith, London,
							W14 8TH, UK</p>
							<i class="icon ion-ios-telephone d-block text-center pt-1"></i>
							<h1 class="d-block text-center pb-2">Phone Number</h1>
					 		<p class="text-center d-block"><?php echo $contactData['Contact']['phone_number']; ?></p>
					 		<i class="icon ion-ios-email d-block text-center pt-1"></i>
					 		<h1 class="d-block text-center pb-2">Email Id</h1>
					 		<p class="text-center d-block pb-5"><?php echo $contactData['Contact']['email_address']; ?></p>
			  			</div>
		  			</div>
		  		</div>
	  		</div>
	  	</div>
<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14735.59825088859!2d88.47519419999999!3d22.582859499999998!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1524754059454" width="100%" height="365 frameborder="0" style="border:0" allowfullscreen></iframe>
    <!-- Bootstrap core JavaScript -->
    <script>
    	$(window).scroll(function(){
		  var sticky = $('.sticky'),
		      scroll = $(window).scrollTop();

		  if (scroll >= 50) sticky.addClass('fixed');
		  else sticky.removeClass('fixed');
		});
    </script>
  <script>
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                dots:true,
                nav: false,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          </script>
 <script>
$(document).ready(function () {
    $("#UserSignupForm").formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'data[ContactNew][name]': {
                validators: {
                    notEmpty: {
                        message: 'Name is required and cannot be empty.'
                    }
                }
            },
            'data[ContactNew][email_address]': {
                /* Initially, the validators of this field are disabled */

                validators: {
                    notEmpty: {
                        message: 'Email Address is required and cannot be empty.'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address. '
                    }
                }

            },
            'data[ContactNew][message]': {
                validators: {
                    notEmpty: {
                        message: 'Message is required and cannot be empty.'
                    }
                }
            }
        }
    });


});

</script>